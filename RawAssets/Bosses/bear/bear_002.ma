//Maya ASCII 2014 scene
//Name: bear_002.ma
//Last modified: Mon, Jul 07, 2014 09:01:14 PM
//Codeset: UTF-8
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010035-864206";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.2552119718321864 65.709239799816956 123.58088204485568 ;
	setAttr ".r" -type "double3" -14.400000000006445 1.2000000000025166 2.485353423970101e-17 ;
	setAttr ".rp" -type "double3" 3.1086244689504383e-15 0 1.4210854715202004e-14 ;
	setAttr ".rpt" -type "double3" 1.1951965110615073e-14 -1.5466776643401578e-15 -1.0666866926210247e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".pze" yes;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 117.1098729108445;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.13521766662597656 48.667610168457031 5.0542886257171631 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.1346383955078352 100.93301348246804 8.9839001204365765 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 15.339141390150488;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.669164367284941 37.835320941869661 119.29730708422083 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 57.932158534123523;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 116.34533128146153 25.886860124687601 5.4034449482274312 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 85.404971928763842;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pPlane1";
	setAttr ".t" -type "double3" 25.587303870731105 0 12.169106372214047 ;
createNode transform -n "transform2" -p "pPlane1";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape1" -p "transform2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.17200440168380737 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 41 ".uvst[0].uvsp[0:40]" -type "float2" 0 0 0.086002201 0
		 0.1720044 0 0.2580066 0 0.3440088 0 0 0.25 0.086002201 0.25 0.1720044 0.25 0.2580066
		 0.25 0.3440088 0.25 0 0.5 0.086002201 0.5 0.1720044 0.5 0.2580066 0.5 0.3440088 0.5
		 0.3440088 0.375 0.2580066 0.375 0.1720044 0.375 0.086002201 0.375 0 0.375 0.3440088
		 0.125 0.2580066 0.125 0.1720044 0.125 0.086002201 0.125 0 0.125 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[25:29]" -type "float3"  3.9980412 1.0152473 -4.8439064 
		1.9990197 1.0152473 -4.8439064 -5.2050694e-07 1.0152473 -4.8439064 -1.9990208 1.0152473 
		-4.8439064 -3.9980412 1.0152473 -4.8439064;
	setAttr -s 30 ".vt[0:29]"  -7.32522392 13.80707932 7.92250538 -3.66261101 13.80707932 7.92250538
		 0 13.80707932 7.92250538 3.66261101 13.80707932 7.92250538 7.32522202 13.80707932 7.92250538
		 -7.32522392 0.40332985 9.61371708 -3.66261101 0.40332985 9.61371708 0 0.40332985 9.61371708
		 3.66261101 0.40332985 9.61371708 7.32522202 0.40332985 9.61371708 -7.32522392 -10.35816383 0
		 -3.66261101 -10.35816383 0 0 -10.35816383 0 3.66261101 -10.35816383 0 7.32522202 -10.35816383 0
		 7.32522202 -8.81809425 5.32342815 3.66261101 -8.81809425 5.32342815 0 -8.81809425 5.32342815
		 -3.66261101 -8.81809425 5.32342815 -7.32522392 -8.81809425 5.32342815 7.32522202 6.82258272 9.85189152
		 3.66261101 6.82258272 9.85189152 0 6.82258272 9.85189152 -3.66261101 6.82258272 9.85189152
		 -7.32522392 6.82258272 9.85189152 -7.32522392 24.48460007 4.97296619 -3.66261101 24.48460007 4.97296619
		 0 24.48460007 4.97296429 3.66261101 24.48460007 4.97296429 7.32522202 24.48460007 4.97296619;
	setAttr -s 49 ".ed[0:48]"  0 1 1 0 24 0 1 2 1 1 23 1 2 3 1 2 22 1 3 4 1
		 3 21 1 4 20 0 5 6 1 5 19 0 6 7 1 6 18 1 7 8 1 7 17 1 8 9 1 8 16 1 9 15 0 10 11 0
		 11 12 0 12 13 0 13 14 0 15 14 0 16 13 1 15 16 1 17 12 1 16 17 1 18 11 1 17 18 1 19 10 0
		 18 19 1 20 9 0 21 8 1 20 21 1 22 7 1 21 22 1 23 6 1 22 23 1 24 5 0 23 24 1 0 25 0
		 1 26 1 25 26 0 2 27 1 26 27 0 3 28 1 27 28 0 4 29 0 28 29 0;
	setAttr -s 20 -ch 80 ".fc[0:19]" -type "polyFaces" 
		f 4 0 3 39 -2
		mu 0 4 0 1 23 24
		f 4 2 5 37 -4
		mu 0 4 1 2 22 23
		f 4 4 7 35 -6
		mu 0 4 2 3 21 22
		f 4 6 8 33 -8
		mu 0 4 3 4 20 21
		f 4 9 12 30 -11
		mu 0 4 5 6 18 19
		f 4 11 14 28 -13
		mu 0 4 6 7 17 18
		f 4 13 16 26 -15
		mu 0 4 7 8 16 17
		f 4 15 17 24 -17
		mu 0 4 8 9 15 16
		f 4 -25 22 -22 -24
		mu 0 4 16 15 14 13
		f 4 -27 23 -21 -26
		mu 0 4 17 16 13 12
		f 4 -29 25 -20 -28
		mu 0 4 18 17 12 11
		f 4 -31 27 -19 -30
		mu 0 4 19 18 11 10
		f 4 -34 31 -16 -33
		mu 0 4 21 20 9 8
		f 4 -36 32 -14 -35
		mu 0 4 22 21 8 7
		f 4 -38 34 -12 -37
		mu 0 4 23 22 7 6
		f 4 -40 36 -10 -39
		mu 0 4 24 23 6 5
		f 4 -1 40 42 -42
		mu 0 4 25 26 27 28
		f 4 -3 41 44 -44
		mu 0 4 29 30 31 32
		f 4 -5 43 46 -46
		mu 0 4 33 34 35 36
		f 4 -7 45 48 -48
		mu 0 4 37 38 39 40;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane2";
	setAttr ".t" -type "double3" 78.734267471803435 33.688778067213505 1.8711753052752371 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode transform -n "transform4" -p "pPlane2";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape2" -p "transform4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 30 ".uvst[0].uvsp[0:29]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.1460738 0.25 0.1460738 0.5 0.1460738 0.75 0.1460738 1 0.1460738 0
		 0.29214761 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761 1 0.29214761 0 0.4382214
		 0.25 0.4382214 0.5 0.4382214 0.75 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521
		 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0.875 0.4382214 0.875
		 0.29214761 0.875 0.1460738 0.875 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 30 ".vt[0:29]"  -16.80016899 -2.89659071 6.7016201 -8.4000845 -2.1796472e-15 8.63739109
		 -2.86981964 -2.1796472e-15 10.732584 3.28050041 -2.1796472e-15 11.97109222 16.80016899 -2.97128391 9.83943367
		 -16.80016899 -2.89659071 3.35081005 -8.4000845 -1.0898236e-15 4.32637119 -2.92216492 -1.0898236e-15 5.35724258
		 3.27960777 -1.0898236e-15 5.98441887 16.80016899 -2.97128391 4.91982031 -16.80016899 -2.89659071 -4.3909764e-16
		 -8.4000845 0 0 -2.94003296 0 0 3.27931023 0 0 16.80016899 -2.97128391 -6.6132913e-16
		 -16.80016899 -2.89659071 -3.35081005 -8.4000845 1.0898237e-15 -4.32637167 -2.92216492 1.0898237e-15 -5.35724258
		 3.27960777 1.0898237e-15 -5.98441887 16.80016899 -2.97128391 -4.91982079 -16.80016899 -2.89659071 -6.7016201
		 -8.4000845 2.1796472e-15 -8.63739109 -2.86981964 2.1796472e-15 -10.732584 3.28050041 2.1796472e-15 -11.97109222
		 16.80016899 -2.97128391 -9.83943367 9.15307426 -0.93614626 -11.19478703 9.15183067 -0.9360013 -5.59701204
		 9.15142632 -0.93595552 -3.6548805e-16 9.15183067 -0.9360013 5.59701204 9.15307426 -0.93614626 11.19478703;
	setAttr -s 49 ".ed[0:48]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 29 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 28 1 8 13 1 9 14 0 10 11 1 10 15 0
		 11 12 1 11 16 1 12 13 1 12 17 1 13 27 1 13 18 1 14 19 0 15 16 1 15 20 0 16 17 1 16 21 1
		 17 18 1 17 22 1 18 26 1 18 23 1 19 24 0 20 21 0 21 22 0 22 23 0 23 25 0 25 24 0 25 26 1
		 26 27 1 27 28 1 28 29 1 29 4 0 28 9 1 27 14 1 26 19 1;
	setAttr -s 20 -ch 80 ".fc[0:19]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 44 45 8 -47
		mu 0 4 28 29 4 9
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -21 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -23 -15
		mu 0 4 7 8 13 12
		f 4 43 46 17 -48
		mu 0 4 27 28 9 14
		f 4 18 21 -28 -20
		mu 0 4 10 11 16 15
		f 4 20 23 -30 -22
		mu 0 4 11 12 17 16
		f 4 22 25 -32 -24
		mu 0 4 12 13 18 17
		f 4 42 47 26 -49
		mu 0 4 26 27 14 19
		f 4 27 30 -37 -29
		mu 0 4 15 16 21 20
		f 4 29 32 -38 -31
		mu 0 4 16 17 22 21
		f 4 31 34 -39 -33
		mu 0 4 17 18 23 22
		f 4 -41 41 48 35
		mu 0 4 24 25 26 19
		f 4 33 -42 -40 -35
		mu 0 4 18 26 25 23
		f 4 24 -43 -34 -26
		mu 0 4 13 27 26 18
		f 4 15 -44 -25 -17
		mu 0 4 8 28 27 13
		f 4 6 -45 -16 -8
		mu 0 4 3 29 28 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pPlane3";
	setAttr ".rp" -type "double3" 95.879229844570204 4.5404575312448099 2.553793890373933 ;
	setAttr ".sp" -type "double3" 95.879229844570204 4.5404575312448099 2.553793890373933 ;
createNode transform -n "transform6" -p "pPlane3";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape3" -p "transform6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 15 ".uvst[0].uvsp[0:14]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.13593556 0.25 0.13593556 0.5 0.13593556 0.75 0.13593556 1 0.13593556
		 0 0.27187112 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  5.9135447 -0.57689297 0 1.8729539 
		-2.5896893 0 -0.10658795 -0.7016086 0 -1.8385738 0.0087423846 0 -3.9799981 -7.0129342 
		0 3.0112891 -0.48400885 0 0.6525178 -2.5688682 0 -0.33931023 -1.0070237 0 -0.93973851 
		-0.41153604 0 -0.89328134 -3.9494143 0 0.10903601 -0.28550801 0 -0.0029686955 -1.7846785 
		0 -0.11322062 -0.29444179 0 -0.11342081 0 0;
	setAttr -s 15 ".vt[0:14]"  90.28933716 25.10125732 2.55379391 90.28933716 14.820858 2.55379391
		 90.28933716 4.54045773 2.55379391 90.28933716 -5.7399435 2.55379391 90.28933716 -16.020343781 2.55379391
		 95.87922668 25.10125732 2.55379391 95.87922668 14.820858 2.55379391 95.87922668 4.54045773 2.55379391
		 95.87922668 -5.7399435 2.55379391 95.87922668 -16.020343781 2.55379391 101.46911621 25.10125732 2.55379391
		 101.46911621 14.820858 2.55379391 101.46911621 4.54045773 2.55379391 101.46911621 -5.7399435 2.55379391
		 101.46911621 -16.020343781 2.55379391;
	setAttr -s 22 ".ed[0:21]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 4 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 9 1 8 13 1 9 14 0 10 11 1 11 12 1
		 12 13 1 13 14 1;
	setAttr -s 8 -ch 32 ".fc[0:7]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 6 8 -16 -8
		mu 0 4 3 4 9 8
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -20 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -21 -15
		mu 0 4 7 8 13 12
		f 4 15 17 -22 -17
		mu 0 4 8 9 14 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bowGRP";
	setAttr ".t" -type "double3" -87.612035823608394 8.7014997480500824 8.7992172316980781 ;
	setAttr ".r" -type "double3" -18.512846896075857 0 0 ;
	setAttr ".s" -type "double3" 0.12346704444718123 0.12346704444718123 0.12346704444718123 ;
	setAttr ".rp" -type "double3" 88.006568843010726 32.213752095030969 0.6121238511392626 ;
	setAttr ".sp" -type "double3" 88.006568843010726 32.213752095030969 0.6121238511392626 ;
createNode transform -n "polySurface4" -p "bowGRP";
	setAttr ".t" -type "double3" -26.958518879276038 8.6652709282360405 -1.5143874528764849 ;
	setAttr ".r" -type "double3" 0 0 -29.999999999999996 ;
	setAttr ".s" -type "double3" 0.76850662625953037 1 1 ;
	setAttr ".rp" -type "double3" 101.68158721923828 0.89123630523681652 2.5537939071655273 ;
	setAttr ".sp" -type "double3" 101.68158721923828 0.89123630523681652 2.5537939071655273 ;
createNode mesh -n "polySurfaceShape7" -p "polySurface4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:15]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 30 ".uvst[0].uvsp[0:29]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.13593556 0.25 0.13593556 0.5 0.13593556 0.75 0.13593556 1 0.13593556
		 0 0.27187112 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112 0 0 0.25
		 0 0.25 0.13593556 0 0.13593556 0.5 0 0.5 0.13593556 0.75 0 0.75 0.13593556 1 0 1
		 0.13593556 0.25 0.27187112 0 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[0]" -type "float3" 3.3141758 1.0794237 0 ;
	setAttr ".pt[1]" -type "float3" 1.0033834 0.2825768 0 ;
	setAttr ".pt[2]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".pt[5]" -type "float3" 1.6883709 1.0794237 0 ;
	setAttr ".pt[6]" -type "float3" 0.67646682 0.34491643 0 ;
	setAttr ".pt[9]" -type "float3" 0 1.4347146 0 ;
	setAttr ".pt[10]" -type "float3" 1.4210855e-14 0.71636426 0 ;
	setAttr ".pt[11]" -type "float3" -1.4210855e-14 -0.42108214 0 ;
	setAttr ".pt[12]" -type "float3" 0 -0.91088861 0 ;
	setAttr ".pt[13]" -type "float3" 0 -0.50051749 0 ;
	setAttr ".pt[15]" -type "float3" -3.3141758 1.0794237 0 ;
	setAttr ".pt[16]" -type "float3" -1.0033834 0.2825768 0 ;
	setAttr ".pt[17]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".pt[20]" -type "float3" -1.6883709 1.0794237 0 ;
	setAttr ".pt[21]" -type "float3" -0.67646682 0.34491643 0 ;
	setAttr ".pt[24]" -type "float3" 0 1.4347146 0 ;
	setAttr -s 25 ".vt[0:24]"  96.20288086 24.52436447 2.55379391 92.16229248 12.23116875 2.55379391
		 90.18274689 3.83884907 2.55379391 88.45075989 -5.73120117 2.55379391 86.30934143 -23.033277512 2.55379391
		 98.89051819 24.61724854 2.55379391 96.53174591 12.25198936 2.55379391 95.53991699 3.53343391 2.55379391
		 94.93949127 -6.15147972 2.55379391 94.98594666 -19.96975708 2.55379391 101.68158722 24.81575012 2.55379391
		 101.68158722 13.036179543 2.55379391 101.68158722 4.24601603 2.55379391 101.68158722 -5.7399435 2.55379391
		 101.68158722 -16.020343781 2.55379391 107.16029358 24.52436447 2.55379391 111.20088196 12.23116875 2.55379391
		 113.18042755 3.83884907 2.55379391 114.91241455 -5.73120117 2.55379391 117.053833008 -23.033277512 2.55379391
		 104.47265625 24.61724854 2.55379391 106.83142853 12.25198936 2.55379391 107.82325745 3.53343391 2.55379391
		 108.42368317 -6.15147972 2.55379391 108.37722778 -19.96975708 2.55379391;
	setAttr -s 40 ".ed[0:39]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 4 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 9 1 8 13 1 9 14 0 10 11 0 11 12 0
		 12 13 0 13 14 0 15 16 0 15 20 0 16 17 0 16 21 1 17 18 0 17 22 1 18 19 0 18 23 1 19 24 0
		 20 21 1 20 10 0 21 22 1 21 11 1 22 23 1 22 12 1 23 24 1 23 13 1 24 14 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 6 8 -16 -8
		mu 0 4 3 4 9 8
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -20 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -21 -15
		mu 0 4 7 8 13 12
		f 4 15 17 -22 -17
		mu 0 4 8 9 14 13
		f 4 23 31 -26 -23
		mu 0 4 15 18 17 16
		f 4 25 33 -28 -25
		mu 0 4 16 17 20 19
		f 4 27 35 -30 -27
		mu 0 4 19 20 22 21
		f 4 29 37 -31 -29
		mu 0 4 21 22 24 23
		f 4 32 18 -35 -32
		mu 0 4 18 26 25 17
		f 4 34 19 -37 -34
		mu 0 4 17 25 27 20
		f 4 36 20 -39 -36
		mu 0 4 20 27 28 22
		f 4 38 21 -40 -38
		mu 0 4 22 28 29 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "polySurface5" -p "bowGRP";
	setAttr ".t" -type "double3" -9.3788836897160142 -3.8090654815303968 -2.539441077288962 ;
	setAttr ".r" -type "double3" 4.444785074718042 181.28060464644139 20.559964218695171 ;
	setAttr ".s" -type "double3" 0.77068753959165781 0.77068753959165781 0.77068753959165781 ;
	setAttr ".rp" -type "double3" 78.734268188476562 33.688777923583984 -1.2920215129852295 ;
	setAttr ".sp" -type "double3" 78.734268188476562 33.688777923583984 -1.2920215129852295 ;
createNode mesh -n "polySurfaceShape8" -p "polySurface5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:39]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 60 ".uvst[0].uvsp[0:59]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.1460738 0.25 0.1460738 0.5 0.1460738 0.75 0.1460738 1 0.1460738 0
		 0.29214761 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761 1 0.29214761 0 0.4382214
		 0.25 0.4382214 0.5 0.4382214 0.75 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521
		 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0.875 0.4382214 0.875
		 0.29214761 0.875 0.1460738 0.875 0 0 0 0.25 0 0.25 0.1460738 0 0.1460738 0.5 0 0.5
		 0.1460738 0.75 0 0.75 0.1460738 0.875 0.1460738 0.875 0 1 0 1 0.1460738 0.25 0.29214761
		 0 0.29214761 0.5 0.29214761 0.75 0.29214761 0.875 0.29214761 1 0.29214761 0.25 0.4382214
		 0 0.4382214 0.5 0.4382214 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0.25 0.58429521
		 0 0.58429521 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 60 ".vt[0:59]"  61.93409729 26.98715782 -1.025415421 70.33418274 25.051387787 1.87117529
		 75.86444855 22.95619392 1.87117529 82.014770508 21.7176857 1.87117529 95.53443909 23.84934425 -1.10010862
		 61.93409729 30.33796692 -1.025415421 70.33418274 29.36240768 1.87117529 75.81210327 28.33153534 1.87117529
		 82.013877869 27.70435905 1.87117529 95.53443909 28.76895714 -1.10010862 61.93409729 33.68877792 -1.025415421
		 70.33418274 33.68877792 1.87117529 75.79423523 33.68877792 1.87117529 82.013580322 33.68877792 1.87117529
		 95.53443909 33.68877792 -1.10010862 61.93409729 37.039588928 -1.025415421 70.33418274 38.015148163 1.87117529
		 75.81210327 39.046020508 1.87117529 82.013877869 39.67319489 1.87117529 95.53443909 38.6085968 -1.10010862
		 61.93409729 40.39039612 -1.025415421 70.33418274 42.32616806 1.87117529 75.86444855 44.42136383 1.87117529
		 82.014770508 45.65987015 1.87117529 95.53443909 43.5282135 -1.10010862 87.88734436 44.883564 0.93502903
		 87.88610077 39.28578949 0.93517399 87.88569641 33.68877792 0.93521976 87.88610077 28.091766357 0.93517399
		 87.88734436 22.49399185 0.93502903 61.93409729 26.98715782 -1.55862761 70.33418274 25.051387787 -4.45521832
		 75.86444855 22.95619392 -4.45521832 82.014770508 21.7176857 -4.45521832 95.53443909 23.84934425 -1.4839344
		 61.93409729 30.33796692 -1.55862761 70.33418274 29.36240768 -4.45521832 75.81210327 28.33153534 -4.45521832
		 82.013877869 27.70435905 -4.45521832 95.53443909 28.76895714 -1.4839344 61.93409729 33.68877792 -1.55862761
		 70.33418274 33.68877792 -4.45521832 75.79423523 33.68877792 -4.45521832 82.013580322 33.68877792 -4.45521832
		 95.53443909 33.68877792 -1.4839344 61.93409729 37.039588928 -1.55862761 70.33418274 38.015148163 -4.45521832
		 75.81210327 39.046020508 -4.45521832 82.013877869 39.67319489 -4.45521832 95.53443909 38.6085968 -1.4839344
		 61.93409729 40.39039612 -1.55862761 70.33418274 42.32616806 -4.45521832 75.86444855 44.42136383 -4.45521832
		 82.014770508 45.65987015 -4.45521832 95.53443909 43.5282135 -1.4839344 87.88734436 44.883564 -3.51907206
		 87.88610077 39.28578949 -3.51921701 87.88569641 33.68877792 -3.51926279 87.88610077 28.091766357 -3.51921701
		 87.88734436 22.49399185 -3.51907206;
	setAttr -s 98 ".ed[0:97]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 29 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 28 1 8 13 1 9 14 0 10 11 1 10 15 0
		 11 12 1 11 16 1 12 13 1 12 17 1 13 27 1 13 18 1 14 19 0 15 16 1 15 20 0 16 17 1 16 21 1
		 17 18 1 17 22 1 18 26 1 18 23 1 19 24 0 20 21 0 21 22 0 22 23 0 23 25 0 25 24 0 25 26 1
		 26 27 1 27 28 1 28 29 1 29 4 0 28 9 1 27 14 1 26 19 1 30 31 0 30 35 0 31 32 0 31 36 1
		 32 33 0 32 37 1 33 59 0 33 38 1 34 39 0 35 36 1 35 40 0 36 37 1 36 41 1 37 38 1 37 42 1
		 38 58 1 38 43 1 39 44 0 40 41 1 40 45 0 41 42 1 41 46 1 42 43 1 42 47 1 43 57 1 43 48 1
		 44 49 0 45 46 1 45 50 0 46 47 1 46 51 1 47 48 1 47 52 1 48 56 1 48 53 1 49 54 0 50 51 0
		 51 52 0 52 53 0 53 55 0 55 54 0 55 56 1 56 57 1 57 58 1 58 59 1 59 34 0 58 39 1 57 44 1
		 56 49 1;
	setAttr -s 40 -ch 160 ".fc[0:39]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 44 45 8 -47
		mu 0 4 28 29 4 9
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -21 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -23 -15
		mu 0 4 7 8 13 12
		f 4 43 46 17 -48
		mu 0 4 27 28 9 14
		f 4 18 21 -28 -20
		mu 0 4 10 11 16 15
		f 4 20 23 -30 -22
		mu 0 4 11 12 17 16
		f 4 22 25 -32 -24
		mu 0 4 12 13 18 17
		f 4 42 47 26 -49
		mu 0 4 26 27 14 19
		f 4 27 30 -37 -29
		mu 0 4 15 16 21 20
		f 4 29 32 -38 -31
		mu 0 4 16 17 22 21
		f 4 31 34 -39 -33
		mu 0 4 17 18 23 22
		f 4 -41 41 48 35
		mu 0 4 24 25 26 19
		f 4 33 -42 -40 -35
		mu 0 4 18 26 25 23
		f 4 24 -43 -34 -26
		mu 0 4 13 27 26 18
		f 4 15 -44 -25 -17
		mu 0 4 8 28 27 13
		f 4 6 -45 -16 -8
		mu 0 4 3 29 28 8
		f 4 50 58 -53 -50
		mu 0 4 30 33 32 31
		f 4 52 60 -55 -52
		mu 0 4 31 32 35 34
		f 4 54 62 -57 -54
		mu 0 4 34 35 37 36
		f 4 95 -58 -95 -94
		mu 0 4 38 41 40 39
		f 4 59 67 -62 -59
		mu 0 4 33 43 42 32
		f 4 61 69 -64 -61
		mu 0 4 32 42 44 35
		f 4 63 71 -66 -63
		mu 0 4 35 44 45 37
		f 4 96 -67 -96 -93
		mu 0 4 46 47 41 38
		f 4 68 76 -71 -68
		mu 0 4 43 49 48 42
		f 4 70 78 -73 -70
		mu 0 4 42 48 50 44
		f 4 72 80 -75 -72
		mu 0 4 44 50 51 45
		f 4 97 -76 -97 -92
		mu 0 4 52 53 47 46
		f 4 77 85 -80 -77
		mu 0 4 49 55 54 48
		f 4 79 86 -82 -79
		mu 0 4 48 54 56 50
		f 4 81 87 -84 -81
		mu 0 4 50 56 57 51
		f 4 -85 -98 -91 89
		mu 0 4 58 53 52 59
		f 4 83 88 90 -83
		mu 0 4 51 57 59 52
		f 4 74 82 91 -74
		mu 0 4 45 51 52 46
		f 4 65 73 92 -65
		mu 0 4 37 45 46 38
		f 4 56 64 93 -56
		mu 0 4 36 37 38 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dsm" 2;
createNode transform -n "bear_armL";
	setAttr ".rp" -type "double3" 0.50595951080322266 24.240012407302856 5.3675669282674789 ;
	setAttr ".sp" -type "double3" 0.50595951080322266 24.240012407302856 5.3675669282674789 ;
createNode mesh -n "bear_armLShape" -p "bear_armL";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 93 ".uvst[0].uvsp[0:92]" -type "float2" 0.87136102 0.890625
		 0.846681 0.89495498 0.841905 0.86324298 0.865242 0.85918599 0.80940902 0.90010703
		 0.80527401 0.86829299 0.78459197 0.90268302 0.78182697 0.870682 0.84634501 0.76267302
		 0.82530397 0.76624501 0.81987399 0.73087901 0.839526 0.72765201 0.79306102 0.77050197
		 0.78883302 0.73465401 0.77179998 0.77257001 0.76864499 0.73655498 0.79466701 0.618837
		 0.81118298 0.621095 0.77015501 0.61282098 0.75425798 0.60928297 0.58030498 0.599635
		 0.58027899 0.58335298 0.60245299 0.595698 0.59024298 0.61762702 0.59247899 0.56141698
		 0.61465001 0.57375997 0.60632902 0.552854 0.62685198 0.55182099 0.62464201 0.60807002
		 0.61242503 0.62998599 0.63682199 0.58610803 0.64901799 0.56415999 0.646824 0.62042898
		 0.633044 0.62915897 0.65900099 0.59843498 0.65896201 0.58214098 0.64917701 0.625449
		 0.63340598 0.73921001 0.61326498 0.73772901 0.67300999 0.61724502 0.66482401 0.74020302
		 0.68850398 0.61225301 0.68518198 0.74028599 0.63150197 0.77499402 0.61031002 0.77348697
		 0.66398901 0.77626097 0.68537003 0.77642697 0.60049897 0.87142402 0.62427801 0.87312102
		 0.62282503 0.90511101 0.59841698 0.90332299 0.66123199 0.87474799 0.660119 0.90679002
		 0.68476599 0.87503302 0.68493903 0.90714097 0.96511698 0.885526 0.98074901 0.90271699
		 0.94543099 0.91093099 0.93737501 0.87602699 0.98891997 0.93759102 0.95363802 0.94584602
		 0.98252201 0.959939 0.96184301 0.980753 0.91011 0.91916603 0.90169299 0.88418102
		 0.91836703 0.95411402 0.92658597 0.98902798 0.874856 0.92756099 0.881145 0.90517497
		 0.88310897 0.96244198 0.89880198 0.97960299 0.71587002 0.90661699 0.71471697 0.87455201
		 0.75368398 0.90492302 0.75192201 0.87287998 0.71217602 0.77595502 0.71100301 0.73972899
		 0.74507397 0.77448499 0.74292099 0.73830301 0.70856303 0.607907 0.73388398 0.60676801
		 0.56862801 0.90120399 0.56987602 0.86856198 0.89481598 0.85286999 0.93176699 0.84395599
		 0.584153 0.77062702 0.58765298 0.73449802 0.90387499 0.74877501 0.87246197 0.75702798
		 0.864223 0.722206 0.89463699 0.71303099 0.832232 0.62027502 0.856498 0.61218601;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 272 ".vt";
	setAttr ".vt[0:165]"  25.77513123 36.4912796 7.16490078 25.75078773 37.90898514 7.95693493
		 25.70779228 40.41228867 7.95693493 25.68344307 41.8299942 7.16490078 16.69398117 36.63603973 6.82663822
		 16.67237473 37.89408112 7.52946901 16.63422585 40.11543655 7.52946901 16.6126194 41.37348175 6.82663822
		 14.17793846 36.59282303 6.82663822 14.15633011 37.85086823 7.52946901 14.11818123 40.072227478 7.52946901
		 14.096572876 41.33026505 6.82663822 6.57408714 37.76934814 6.26429081 6.20261383 38.68996811 6.8188262
		 5.54668999 40.31554413 6.8188262 5.17521906 41.23616409 6.26429081 6.8585391 37.064395905 5.047235966
		 6.20261383 38.68996811 5.047235966 5.54668999 40.31554413 5.047235966 4.8907671 41.94111633 5.047235966
		 6.8585391 37.064395905 3.27564096 6.20261383 38.68996811 3.27564096 5.54668999 40.31554413 3.27564096
		 4.8907671 41.94111633 3.27564096 6.57408714 37.76934814 2.058587074 6.20261383 38.68996811 1.50404894
		 5.54668999 40.31554413 1.50404894 5.17521906 41.23616409 2.058587074 14.17793846 36.59282303 1.49623704
		 14.15633106 37.85086823 0.79340702 14.11818123 40.072227478 0.79340702 14.09657383 41.33026505 1.49623704
		 16.69398117 36.63603973 1.49623704 16.67237854 37.89408112 0.79340702 16.63422585 40.11543655 0.79340702
		 16.6126194 41.37348175 1.49623704 25.77513123 36.4912796 1.15797603 25.75078773 37.90898514 0.36594301
		 25.70779228 40.41228867 0.36594301 25.68344307 41.8299942 1.15797603 25.79377937 35.40570068 2.89627194
		 25.75078773 37.90898514 2.89627194 25.70779228 40.41228867 2.89627194 25.66479683 42.91557693 2.89627194
		 25.79377937 35.40570068 5.42660522 25.75078773 37.90898514 5.42660522 25.70779228 40.41228867 5.42660522
		 25.66479683 42.91557693 5.42660522 16.59607315 42.33679962 3.038763046 16.59607315 42.33679962 5.28411722
		 14.08002758 42.29358292 3.038763046 14.08002758 42.29358292 5.28411722 16.71052933 35.67271423 3.038763046
		 16.71052933 35.67271423 5.28411722 14.1944828 35.62950516 3.038763046 14.1944828 35.62950516 5.28411722
		 23.45065689 41.9528656 7.34623909 23.47634888 40.44955444 8.18608761 23.52193642 37.79511642 8.18608761
		 23.54788399 36.29182053 7.34623909 23.56771088 35.14068985 5.50298977 23.56771088 35.14068985 2.81988597
		 23.54788399 36.29182053 0.97663701 23.52194405 37.79511642 0.13679001 23.47635269 40.44955444 0.13679001
		 23.45066071 41.9528656 0.97663701 23.43094063 43.10398865 2.81988597 23.43094063 43.10398865 5.50298977
		 9.80245018 5.37603617 1.92805898 7.87506104 5.37603617 0.95096201 4.4718051 5.37603617 0.95096201
		 2.54442 5.37603617 1.92805898 9.39373398 13.43475533 2.34535909 7.6834178 13.43475533 1.47830403
		 4.66345215 13.43475533 1.47830403 2.95313096 13.43475533 2.34535909 9.39373398 16.12099457 2.34535909
		 7.6834178 16.12099457 1.47830403 4.66345215 16.12099457 1.47830403 2.95313096 16.12099457 2.34535909
		 8.027686119 23.18336487 2.34535909 6.77939892 22.014196396 1.47830403 4.66345215 21.49346352 1.47830403
		 2.95313096 21.49346352 2.34535909 8.98354626 24.078636169 4.24830198 6.77939892 22.014196396 4.24830198
		 4.66345215 21.49346352 4.24830198 1.64348102 21.49346352 4.24830198 8.98354626 24.078636169 7.018302917
		 6.77939892 22.014196396 7.018302917 4.66345215 21.49346352 7.018302917 1.64348102 21.49346352 7.018302917
		 8.027686119 23.18336487 8.92124939 6.77939892 22.014196396 9.78830338 4.66345215 21.49346352 9.78830338
		 2.95313096 21.49346352 8.92124939 9.39373398 16.12099075 8.92124939 7.6834178 16.12099075 9.78830338
		 4.66345215 16.12099075 9.78830338 2.95313096 16.12099075 8.92124939 9.39373398 13.43475437 8.92124939
		 7.6834178 13.43475437 9.78830338 4.66345215 13.43475437 9.78830338 2.95313096 13.43475437 8.92124939
		 9.80245018 5.37603617 9.33854866 7.87506104 5.37603617 10.31564617 4.4718051 5.37603617 10.31564617
		 2.54442 5.37603617 9.33854866 11.27831554 5.37603617 7.19408321 7.87506104 5.37603617 7.19408321
		 4.4718051 5.37603617 7.19408321 1.068552017 5.37603617 7.19408321 11.27831554 5.37603617 4.07252121
		 7.87506104 5.37603617 4.07252121 4.4718051 5.37603617 4.07252121 1.068552017 5.37603617 4.07252121
		 1.64348102 13.43475533 7.018302917 1.64348102 13.43475533 4.24830198 1.64348102 16.12099457 7.018302917
		 1.64348102 16.12099457 4.24830198 10.70338631 13.43475533 7.018302917 10.70338631 13.43475533 4.24830198
		 10.70338631 16.12099457 7.018302917 10.70338631 16.12099457 4.24830198 2.32530904 7.75690508 1.70434904
		 4.36907101 7.75703907 0.66826302 7.97779799 7.75703907 0.66826302 10.021558762 7.75690508 1.70434904
		 11.58653736 7.75684404 3.97828603 11.58653736 7.75684404 7.28832006 10.021558762 7.75690413 9.56225872
		 7.97779799 7.75703812 10.59834385 4.36907101 7.75703812 10.59834385 2.32530904 7.75690413 9.56225872
		 0.76033098 7.75684404 7.28832006 0.76033098 7.75684404 3.97828603 -9.19085312 5.37603617 9.33854866
		 -7.26346493 5.37603617 10.31564617 -3.86020899 5.37603617 10.31564617 -1.93281996 5.37603617 9.33854866
		 -8.78213882 13.43475533 8.92125034 -7.071821213 13.43475533 9.78830338 -4.051854134 13.43475533 9.78830338
		 -2.34153605 13.43475533 8.92125034 -8.78213882 16.12099457 8.92125034 -7.071821213 16.12099457 9.78830338
		 -4.051854134 16.12099457 9.78830338 -2.34153605 16.12099457 8.92125034 -7.41609001 23.18336487 8.92125034
		 -6.16780186 22.014196396 9.78830338 -4.051854134 21.49346352 9.78830338 -2.34153605 21.49346352 8.92125034
		 -8.3719492 24.078636169 7.018302917 -6.16780186 22.014196396 7.018302917 -4.051854134 21.49346352 7.018302917
		 -1.031885982 21.49346352 7.018302917 -8.3719492 24.078636169 4.24830198 -6.16780186 22.014196396 4.24830198
		 -4.051854134 21.49346352 4.24830198 -1.031885982 21.49346352 4.24830198 -7.41609001 23.18336487 2.34535909
		 -6.16780186 22.014196396 1.47830403 -4.051854134 21.49346352 1.47830403 -2.34153605 21.49346352 2.34535909
		 -8.78213882 16.12099075 2.34535909 -7.071821213 16.12099075 1.47830403;
	setAttr ".vt[166:271]" -4.051854134 16.12099075 1.47830403 -2.34153605 16.12099075 2.34535909
		 -8.78213882 13.43475437 2.34535909 -7.071821213 13.43475437 1.47830403 -4.051854134 13.43475437 1.47830403
		 -2.34153605 13.43475437 2.34535909 -9.19085312 5.37603617 1.92805898 -7.26346493 5.37603617 0.95096201
		 -3.86020899 5.37603617 0.95096201 -1.93281996 5.37603617 1.92805898 -10.6667223 5.37603617 4.072525024
		 -7.26346493 5.37603617 4.072525024 -3.86020899 5.37603617 4.072525024 -0.45695201 5.37603617 4.072525024
		 -10.6667223 5.37603617 7.19408321 -7.26346493 5.37603617 7.19408321 -3.86020899 5.37603617 7.19408321
		 -0.45695201 5.37603617 7.19408321 -1.031885982 13.43475533 4.24830198 -1.031885982 13.43475533 7.018302917
		 -1.031885982 16.12099457 4.24830198 -1.031885982 16.12099457 7.018302917 -10.091789246 13.43475533 4.24830198
		 -10.091789246 13.43475533 7.018302917 -10.091789246 16.12099457 4.24830198 -10.091789246 16.12099457 7.018302917
		 -1.71371198 7.75690508 9.56225872 -3.75747395 7.75703907 10.59834385 -7.36619997 7.75703907 10.59834385
		 -9.40995884 7.75690508 9.56225872 -10.97493935 7.75684404 7.28832006 -10.97493935 7.75684404 3.97828603
		 -9.40995884 7.75690413 1.70434904 -7.36619997 7.75703812 0.66826397 -3.75747395 7.75703812 0.66826397
		 -1.71371198 7.75690413 1.70434904 -0.148735 7.75684404 3.97828603 -0.148735 7.75684404 7.28832006
		 -24.67152405 35.62486649 1.15797496 -24.69587135 37.042572021 0.36594301 -24.73886681 39.54586792 0.36594301
		 -24.7632122 40.96356964 1.15797496 -15.6006937 36.081378937 1.49623704 -15.62230206 37.3394165 0.79340702
		 -15.66045475 39.56078339 0.79340702 -15.68206215 40.81881332 1.49623704 -13.084650993 36.1245842 1.49623704
		 -13.10625839 37.38262939 0.79340702 -13.14440727 39.60398483 0.79340702 -13.16601658 40.86203766 1.49623704
		 -5.52568388 37.56153107 2.058587074 -5.18604612 38.49436951 1.50404894 -4.58633089 40.14151382 1.50404894
		 -4.24669313 41.074344635 2.058587074 -5.78575993 36.84723282 3.27564096 -5.18604612 38.49436951 3.27564096
		 -4.58633089 40.14151382 3.27564096 -3.98661804 41.78863907 3.27564096 -5.78575993 36.84723282 5.047235966
		 -5.18604612 38.49436951 5.047235966 -4.58633089 40.14151382 5.047235966 -3.98661804 41.78863907 5.047235966
		 -5.52568388 37.56153107 6.26429081 -5.18604612 38.49436951 6.8188262 -4.58633089 40.14151382 6.8188262
		 -4.24669313 41.074344635 6.26429081 -13.084650993 36.1245842 6.82663822 -13.10625839 37.38263321 7.52947187
		 -13.14440727 39.60398865 7.52947187 -13.16601658 40.86203766 6.82663822 -15.6006937 36.081378937 6.82663822
		 -15.62230206 37.3394165 7.52947187 -15.66045475 39.56078339 7.52947187 -15.68206215 40.81881332 6.82663822
		 -24.67152405 35.62486649 7.16490078 -24.69587135 37.042572021 7.95693493 -24.73886681 39.54586792 7.95693493
		 -24.7632122 40.96356964 7.16490078 -24.6528759 34.53926468 5.42660284 -24.69587135 37.042572021 5.42660284
		 -24.73886681 39.54586792 5.42660284 -24.78186035 42.049160004 5.42660284 -24.6528759 34.53926468 2.89627194
		 -24.69587135 37.042572021 2.89627194 -24.73886681 39.54586792 2.89627194 -24.78186035 42.049160004 2.89627194
		 -15.69860554 41.78214645 5.28411388 -15.69860554 41.78214645 3.038762093 -13.18256092 41.82535172 5.28411388
		 -13.18256092 41.82535172 3.038762093 -15.58415031 35.11805725 5.28411388 -15.58415031 35.11805725 3.038762093
		 -13.068105698 35.16126633 5.28411388 -13.068105698 35.16126633 3.038762093 -22.53595924 41.16304016 0.97663701
		 -22.5100174 39.65974426 0.13679001 -22.46442604 37.0052986145 0.13679001 -22.43873024 35.50199509 0.97663701
		 -22.41901779 34.35086823 2.81988597 -22.41901779 34.35086823 5.50298882 -22.43873215 35.50199509 7.34623909
		 -22.46442795 37.0052986145 8.18609047 -22.5100193 39.65974426 8.18609047 -22.53595924 41.16304016 7.34623909
		 -22.55578804 42.31417465 5.50298882 -22.55578804 42.31417465 2.81988597;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 58 1 58 59 0 59 0 0 1 2 0 2 57 1
		 57 58 0 2 3 0 3 56 0 56 57 0 4 5 0 5 9 1 9 8 1 8 4 0 5 6 0 6 10 1 10 9 1 6 7 0 7 11 0
		 11 10 1 9 13 1 13 12 0 12 8 0 10 14 1 14 13 0 11 15 0 15 14 0 13 17 1 17 16 1 16 12 0
		 14 18 1 18 17 1 15 19 0 19 18 1 17 21 1 21 20 1 20 16 0 18 22 1 22 21 1 19 23 0 23 22 1
		 21 25 1 25 24 0 24 20 0 22 26 1 26 25 0 23 27 0 27 26 0 25 29 1 29 28 1 28 24 0 26 30 1
		 30 29 1 27 31 0 31 30 1 29 33 1 33 32 1 32 28 0 30 34 1 34 33 1 31 35 0 35 34 1 62 63 1
		 63 37 1 37 36 0 36 62 0 63 64 1 64 38 1 38 37 0 64 65 1 65 39 0 39 38 0 37 41 1 41 40 1
		 40 36 0 38 42 1 42 41 1 39 43 0 43 42 1 41 45 1 45 44 1 44 40 0 42 46 1 46 45 1 43 47 0
		 47 46 1 45 1 1 0 44 0 46 2 1 47 3 0 65 66 1 66 43 1 66 67 1 67 47 1 67 56 1 48 35 1
		 31 50 1 50 48 1 49 48 1 50 51 1 51 49 1 7 49 1 51 11 1 23 50 1 19 51 1 40 61 1 61 62 1
		 44 60 1 60 61 1 59 60 1 32 52 1 52 54 1 54 28 1 52 53 1 53 55 1 55 54 1 53 4 1 8 55 1
		 54 20 1 55 16 1 56 7 0 6 57 1 5 58 1 4 59 0 53 60 1 52 61 1 32 62 0 33 63 1 34 64 1
		 35 65 0 48 66 1 49 67 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 13
		mu 0 4 8 9 10 11
		f 4 14 15 16 -12
		mu 0 4 9 12 13 10
		f 4 17 18 19 -16
		mu 0 4 12 14 15 13
		f 4 -13 20 21 22
		mu 0 4 11 10 16 17
		f 4 -17 23 24 -21
		mu 0 4 10 13 18 16
		f 4 -20 25 26 -24
		mu 0 4 13 15 19 18
		f 4 -22 27 28 29
		mu 0 4 20 21 22 23
		f 4 -25 30 31 -28
		mu 0 4 21 24 25 22
		f 4 -27 32 33 -31
		mu 0 4 24 26 27 25
		f 4 -29 34 35 36
		mu 0 4 23 22 28 29
		f 4 -32 37 38 -35
		mu 0 4 22 25 30 28
		f 4 -34 39 40 -38
		mu 0 4 25 27 31 30
		f 4 -36 41 42 43
		mu 0 4 29 28 32 33
		f 4 -39 44 45 -42
		mu 0 4 28 30 34 32
		f 4 -41 46 47 -45
		mu 0 4 30 31 35 34
		f 4 -43 48 49 50
		mu 0 4 33 36 37 38
		f 4 -46 51 52 -49
		mu 0 4 36 39 40 37
		f 4 -48 53 54 -52
		mu 0 4 39 41 42 40
		f 4 -50 55 56 57
		mu 0 4 38 37 43 44
		f 4 -53 58 59 -56
		mu 0 4 37 40 45 43
		f 4 -55 60 61 -59
		mu 0 4 40 42 46 45
		f 4 62 63 64 65
		mu 0 4 47 48 49 50
		f 4 66 67 68 -64
		mu 0 4 48 51 52 49
		f 4 69 70 71 -68
		mu 0 4 51 53 54 52
		f 4 -65 72 73 74
		mu 0 4 55 56 57 58
		f 4 -69 75 76 -73
		mu 0 4 56 59 60 57
		f 4 -72 77 78 -76
		mu 0 4 59 61 62 60
		f 4 -74 79 80 81
		mu 0 4 58 57 63 64
		f 4 -77 82 83 -80
		mu 0 4 57 60 65 63
		f 4 -79 84 85 -83
		mu 0 4 60 62 66 65
		f 4 -81 86 -1 87
		mu 0 4 64 63 67 68
		f 4 -84 88 -5 -87
		mu 0 4 63 65 69 67
		f 4 -86 89 -8 -89
		mu 0 4 65 66 70 69
		f 4 -78 -71 90 91
		mu 0 4 71 54 53 72
		f 4 -85 -92 92 93
		mu 0 4 73 71 72 74
		f 4 -90 -94 94 -9
		mu 0 4 6 73 74 7
		f 4 95 -61 96 97
		mu 0 4 75 46 42 76
		f 4 98 -98 99 100
		mu 0 4 77 75 76 78
		f 4 101 -101 102 -19
		mu 0 4 14 77 78 15
		f 4 -97 -54 -47 103
		mu 0 4 76 42 41 79
		f 4 -100 -104 -40 104
		mu 0 4 78 76 79 80
		f 4 -103 -105 -33 -26
		mu 0 4 15 78 80 19
		f 4 -75 105 106 -66
		mu 0 4 50 81 82 47
		f 4 -82 107 108 -106
		mu 0 4 58 64 83 84
		f 4 -88 -4 109 -108
		mu 0 4 64 0 3 83
		f 4 110 111 112 -58
		mu 0 4 44 85 86 38
		f 4 113 114 115 -112
		mu 0 4 87 88 89 90
		f 4 116 -14 117 -115
		mu 0 4 88 8 11 89
		f 4 -113 118 -44 -51
		mu 0 4 38 86 29 33
		f 4 -116 119 -37 -119
		mu 0 4 90 89 91 92
		f 4 -118 -23 -30 -120
		mu 0 4 89 11 17 91
		f 4 -10 120 -18 121
		mu 0 4 5 7 14 12
		f 4 -7 -122 -15 122
		mu 0 4 2 5 12 9
		f 4 -3 -123 -11 123
		mu 0 4 3 2 9 8
		f 4 -110 -124 -117 124
		mu 0 4 83 3 8 88
		f 4 -109 -125 -114 125
		mu 0 4 84 83 88 87
		f 4 -107 -126 -111 126
		mu 0 4 47 82 85 44
		f 4 -57 127 -63 -127
		mu 0 4 44 43 48 47
		f 4 -60 128 -67 -128
		mu 0 4 43 45 51 48
		f 4 -62 129 -70 -129
		mu 0 4 45 46 53 51
		f 4 -91 -130 -96 130
		mu 0 4 72 53 46 75
		f 4 -93 -131 -99 131
		mu 0 4 74 72 75 77
		f 4 -95 -132 -102 -121
		mu 0 4 7 74 77 14;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_legL";
	setAttr ".rp" -type "double3" 6.1734341681003571 14.727336168289185 5.6333034336566925 ;
	setAttr ".sp" -type "double3" 6.1734341681003571 14.727336168289185 5.6333034336566925 ;
createNode mesh -n "bear_legLShape" -p "bear_legL";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 93 ".uvst[0].uvsp[0:92]" -type "float2" 0.28965101 0.111173
		 0.32227799 0.116609 0.31569701 0.150362 0.28484699 0.145165 0.371833 0.12679701 0.36482999
		 0.16047101 0.403685 0.134462 0.395197 0.167742 0.27432799 0.22673699 0.302472 0.23165201
		 0.295277 0.26960999 0.268794 0.264696 0.34588 0.240748 0.33740899 0.278707 0.373676
		 0.247393 0.36419299 0.28508201 0.28875101 0.35484201 0.263818 0.36753899 0.319682
		 0.353811 0.34573701 0.35997099 0.057328999 0.46698099 0.032040998 0.456278 0.062351
		 0.430682 0.090249002 0.46372399 0.011913 0.43243399 0.042227 0.406838 0.0056019998
		 0.40571001 0.01433 0.37379599 0.092662998 0.405076 0.120554 0.43812799 0.072548002
		 0.381217 0.044668999 0.34820899 0.122978 0.37948301 0.129283 0.40621901 0.102853
		 0.355589 0.077564999 0.34473899 0.15916599 0.36166701 0.135709 0.34629199 0.139507
		 0.26086 0.166338 0.25894201 0.104764 0.341741 0.096684001 0.26499799 0.069468997
		 0.268053 0.136759 0.22226 0.16518 0.22057 0.092799 0.22630601 0.064772002 0.22957601
		 0.16381299 0.138317 0.132569 0.140001 0.129806 0.105747 0.162856 0.104001 0.082529999
		 0.144565 0.079466 0.110398 0.051217999 0.148413 0.047274001 0.11449 0.55150402 0.048519999
		 0.55649602 0.079044998 0.51387799 0.065563001 0.52862197 0.019104 0.54180199 0.125462
		 0.49913201 0.112029 0.52019203 0.14756601 0.48459399 0.15849701 0.47127101 0.052065
		 0.486031 0.0056019998 0.456462 0.098563999 0.44144401 0.14508399 0.42868301 0.038506001
		 0.45037499 0.016442999 0.41388199 0.084927998 0.41873601 0.115481 0.0096580004 0.119545
		 0.01274 0.154412 0.47605401 0.19231801 0.43175301 0.17786799 0.031761002 0.234671
		 0.036672 0.27203599 0.406618 0.25622699 0.44483501 0.26841101 0.43439701 0.30421701
		 0.396211 0.29354101 0.415288 0.37826699 0.37717199 0.36816201 0.20268799 0.103931
		 0.201676 0.13812999 0.25008899 0.106613 0.247247 0.140708 0.199424 0.22017699 0.198467
		 0.25811699 0.2403 0.22248399 0.23694 0.26028001 0.19068199 0.37411499 0.231152 0.376394;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 68 ".vt[0:67]"  9.80245018 5.37603617 1.92805898 7.87506104 5.37603617 0.95096201
		 7.97779799 7.75703907 0.66826302 10.021558762 7.75690508 1.70434904 4.4718051 5.37603617 0.95096201
		 4.36907101 7.75703907 0.66826302 2.54442 5.37603617 1.92805898 2.32530904 7.75690508 1.70434904
		 9.39373398 13.43475533 2.34535909 7.6834178 13.43475533 1.47830403 7.6834178 16.12099457 1.47830403
		 9.39373398 16.12099457 2.34535909 4.66345215 13.43475533 1.47830403 4.66345215 16.12099457 1.47830403
		 2.95313096 13.43475533 2.34535909 2.95313096 16.12099457 2.34535909 6.77939892 22.014196396 1.47830403
		 8.027686119 23.18336487 2.34535909 4.66345215 21.49346352 1.47830403 2.95313096 21.49346352 2.34535909
		 6.77939892 22.014196396 4.24830198 8.98354626 24.078636169 4.24830198 4.66345215 21.49346352 4.24830198
		 1.64348102 21.49346352 4.24830198 6.77939892 22.014196396 7.018302917 8.98354626 24.078636169 7.018302917
		 4.66345215 21.49346352 7.018302917 1.64348102 21.49346352 7.018302917 6.77939892 22.014196396 9.78830338
		 8.027686119 23.18336487 8.92124939 4.66345215 21.49346352 9.78830338 2.95313096 21.49346352 8.92124939
		 7.6834178 16.12099075 9.78830338 9.39373398 16.12099075 8.92124939 4.66345215 16.12099075 9.78830338
		 2.95313096 16.12099075 8.92124939 7.6834178 13.43475437 9.78830338 9.39373398 13.43475437 8.92124939
		 4.66345215 13.43475437 9.78830338 2.95313096 13.43475437 8.92124939 10.021558762 7.75690413 9.56225872
		 7.97779799 7.75703812 10.59834385 7.87506104 5.37603617 10.31564617 9.80245018 5.37603617 9.33854866
		 4.36907101 7.75703812 10.59834385 4.4718051 5.37603617 10.31564617 2.32530904 7.75690413 9.56225872
		 2.54442 5.37603617 9.33854866 7.87506104 5.37603617 7.19408321 11.27831554 5.37603617 7.19408321
		 4.4718051 5.37603617 7.19408321 1.068552017 5.37603617 7.19408321 7.87506104 5.37603617 4.07252121
		 11.27831554 5.37603617 4.07252121 4.4718051 5.37603617 4.07252121 1.068552017 5.37603617 4.07252121
		 0.76033098 7.75684404 7.28832006 0.76033098 7.75684404 3.97828603 1.64348102 13.43475533 7.018302917
		 1.64348102 16.12099457 7.018302917 1.64348102 13.43475533 4.24830198 1.64348102 16.12099457 4.24830198
		 11.58653736 7.75684404 7.28832006 11.58653736 7.75684404 3.97828603 10.70338631 13.43475533 7.018302917
		 10.70338631 16.12099457 7.018302917 10.70338631 13.43475533 4.24830198 10.70338631 16.12099457 4.24830198;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 1 2 3 1 3 0 0 1 4 0 4 5 1 5 2 1
		 4 6 0 6 7 0 7 5 1 8 9 1 9 10 1 10 11 1 11 8 0 9 12 1 12 13 1 13 10 1 12 14 1 14 15 0
		 15 13 1 10 16 1 16 17 0 17 11 0 13 18 1 18 16 0 15 19 0 19 18 0 16 20 1 20 21 1 21 17 0
		 18 22 1 22 20 1 19 23 0 23 22 1 20 24 1 24 25 1 25 21 0 22 26 1 26 24 1 23 27 0 27 26 1
		 24 28 1 28 29 0 29 25 0 26 30 1 30 28 0 27 31 0 31 30 0 28 32 1 32 33 1 33 29 0 30 34 1
		 34 32 1 31 35 0 35 34 1 32 36 1 36 37 1 37 33 0 34 38 1 38 36 1 35 39 0 39 38 1 40 41 1
		 41 42 1 42 43 0 43 40 0 41 44 1 44 45 1 45 42 0 44 46 1 46 47 0 47 45 0 42 48 1 48 49 1
		 49 43 0 45 50 1 50 48 1 47 51 0 51 50 1 48 52 1 52 53 1 53 49 0 50 54 1 54 52 1 51 55 0
		 55 54 1 52 1 1 0 53 0 54 4 1 55 6 0 46 56 1 56 51 1 56 57 1 57 55 1 57 7 1 58 39 1
		 35 59 1 59 58 1 60 58 1 59 61 1 61 60 1 14 60 1 61 15 1 27 59 1 23 61 1 49 62 1 62 40 1
		 53 63 1 63 62 1 3 63 1 37 64 1 64 65 1 65 33 1 64 66 1 66 67 1 67 65 1 66 8 1 11 67 1
		 65 25 1 67 21 1 7 14 0 12 5 1 9 2 1 8 3 0 66 63 1 64 62 1 37 40 0 36 41 1 38 44 1
		 39 46 0 58 56 1 60 57 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 13
		mu 0 4 8 9 10 11
		f 4 14 15 16 -12
		mu 0 4 9 12 13 10
		f 4 17 18 19 -16
		mu 0 4 12 14 15 13
		f 4 -13 20 21 22
		mu 0 4 11 10 16 17
		f 4 -17 23 24 -21
		mu 0 4 10 13 18 16
		f 4 -20 25 26 -24
		mu 0 4 13 15 19 18
		f 4 -22 27 28 29
		mu 0 4 20 21 22 23
		f 4 -25 30 31 -28
		mu 0 4 21 24 25 22
		f 4 -27 32 33 -31
		mu 0 4 24 26 27 25
		f 4 -29 34 35 36
		mu 0 4 23 22 28 29
		f 4 -32 37 38 -35
		mu 0 4 22 25 30 28
		f 4 -34 39 40 -38
		mu 0 4 25 27 31 30
		f 4 -36 41 42 43
		mu 0 4 29 28 32 33
		f 4 -39 44 45 -42
		mu 0 4 28 30 34 32
		f 4 -41 46 47 -45
		mu 0 4 30 31 35 34
		f 4 -43 48 49 50
		mu 0 4 36 37 38 39
		f 4 -46 51 52 -49
		mu 0 4 37 40 41 38
		f 4 -48 53 54 -52
		mu 0 4 40 35 42 41
		f 4 -50 55 56 57
		mu 0 4 39 38 43 44
		f 4 -53 58 59 -56
		mu 0 4 38 41 45 43
		f 4 -55 60 61 -59
		mu 0 4 41 42 46 45
		f 4 62 63 64 65
		mu 0 4 47 48 49 50
		f 4 66 67 68 -64
		mu 0 4 48 51 52 49
		f 4 69 70 71 -68
		mu 0 4 51 53 54 52
		f 4 -65 72 73 74
		mu 0 4 55 56 57 58
		f 4 -69 75 76 -73
		mu 0 4 56 59 60 57
		f 4 -72 77 78 -76
		mu 0 4 59 61 62 60
		f 4 -74 79 80 81
		mu 0 4 58 57 63 64
		f 4 -77 82 83 -80
		mu 0 4 57 60 65 63
		f 4 -79 84 85 -83
		mu 0 4 60 62 66 65
		f 4 -81 86 -1 87
		mu 0 4 64 63 67 68
		f 4 -84 88 -5 -87
		mu 0 4 63 65 69 67
		f 4 -86 89 -8 -89
		mu 0 4 65 66 70 69
		f 4 -78 -71 90 91
		mu 0 4 71 54 53 72
		f 4 -85 -92 92 93
		mu 0 4 66 62 73 74
		f 4 -90 -94 94 -9
		mu 0 4 6 66 74 7
		f 4 95 -61 96 97
		mu 0 4 75 46 42 76
		f 4 98 -98 99 100
		mu 0 4 77 78 79 80
		f 4 101 -101 102 -19
		mu 0 4 14 77 80 15
		f 4 -97 -54 -47 103
		mu 0 4 76 42 35 31
		f 4 -100 -104 -40 104
		mu 0 4 80 79 81 82
		f 4 -103 -105 -33 -26
		mu 0 4 15 80 82 19
		f 4 -75 105 106 -66
		mu 0 4 50 83 84 47
		f 4 -82 107 108 -106
		mu 0 4 83 85 86 84
		f 4 -88 -4 109 -108
		mu 0 4 85 0 3 86
		f 4 110 111 112 -58
		mu 0 4 44 87 88 39
		f 4 113 114 115 -112
		mu 0 4 87 89 90 88
		f 4 116 -14 117 -115
		mu 0 4 89 8 11 90
		f 4 -113 118 -44 -51
		mu 0 4 39 88 91 36
		f 4 -116 119 -37 -119
		mu 0 4 88 90 92 91
		f 4 -118 -23 -30 -120
		mu 0 4 90 11 17 92
		f 4 -10 120 -18 121
		mu 0 4 5 7 14 12
		f 4 -7 -122 -15 122
		mu 0 4 2 5 12 9
		f 4 -3 -123 -11 123
		mu 0 4 3 2 9 8
		f 4 -110 -124 -117 124
		mu 0 4 86 3 8 89
		f 4 -109 -125 -114 125
		mu 0 4 84 86 89 87
		f 4 -107 -126 -111 126
		mu 0 4 47 84 87 44
		f 4 -57 127 -63 -127
		mu 0 4 44 43 48 47
		f 4 -60 128 -67 -128
		mu 0 4 43 45 51 48
		f 4 -62 129 -70 -129
		mu 0 4 45 46 53 51
		f 4 -91 -130 -96 130
		mu 0 4 72 53 46 75
		f 4 -93 -131 -99 131
		mu 0 4 74 73 78 77
		f 4 -95 -132 -102 -121
		mu 0 4 7 74 77 14;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_legR";
	setAttr ".rp" -type "double3" -5.5618371739983559 14.727336168289185 5.6333039104938507 ;
	setAttr ".sp" -type "double3" -5.5618371739983559 14.727336168289185 5.6333039104938507 ;
createNode mesh -n "bear_legRShape" -p "bear_legR";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 93 ".uvst[0].uvsp[0:92]" -type "float2" 0.43127099 0.61491197
		 0.463727 0.62148601 0.45597601 0.65496403 0.425302 0.648718 0.51285398 0.63347298
		 0.50481403 0.66682202 0.54410398 0.64224601 0.53522998 0.67522699 0.41192201 0.72988498
		 0.43979201 0.73572099 0.43141699 0.77349401 0.40516201 0.76766998 0.48268801 0.74617499
		 0.47316599 0.783871 0.50993699 0.75353301 0.49963999 0.79090601 0.422638 0.85855597
		 0.39721701 0.87033802 0.45387501 0.85858297 0.48028699 0.86551601 0.41963899 0.91808897
		 0.43004099 0.89267498 0.455991 0.92268002 0.42327699 0.95096701 0.45363301 0.87223601
		 0.47958499 0.90224999 0.512263 0.87393302 0.48193499 0.952694 0.44921401 0.98097497
		 0.50553697 0.932275 0.53824002 0.903974 0.507873 0.98270899 0.48122099 0.98933297
		 0.53148401 0.96228802 0.54188699 0.93686801 0.29291901 0.86087197 0.27013001 0.84468299
		 0.27607599 0.75941002 0.30298701 0.758403 0.239704 0.83916903 0.233077 0.76228601
		 0.21305101 0.84148502 0.205659 0.76471001 0.274481 0.72080898 0.30304199 0.72004199
		 0.230212 0.723499 0.201748 0.72604197 0.304517 0.63780802 0.27322099 0.63845801 0.27162099
		 0.60410702 0.304719 0.60347903 0.223122 0.64131802 0.221084 0.60698402 0.192004 0.64410502
		 0.188425 0.60994399 0.158678 0.48632899 0.177058 0.51122099 0.13298801 0.51866299
		 0.124898 0.470588 0.185195 0.559264 0.14112601 0.56678599 0.17610601 0.58883703 0.14947601
		 0.614977 0.088907003 0.526039 0.080834001 0.47797099 0.096984997 0.57410997 0.104846
		 0.62213498 0.044817999 0.53339899 0.054046001 0.50388002 0.052846 0.581415 0.071139
		 0.60632902 0.15436099 0.64883399 0.108442 0.65676302 0.58057302 0.65276301 0.57241398
		 0.686804 0.16788 0.73001301 0.17276099 0.76844001 0.128306 0.73653698 0.133433 0.773467
		 0.54184502 0.76342499 0.53149199 0.79969901 0.180769 0.845034 0.14159299 0.84950298
		 0.34454799 0.604738 0.342383 0.63888299 0.39185601 0.60901201 0.387858 0.64299101
		 0.33732599 0.72079802 0.335136 0.75867897 0.378102 0.72448599 0.373505 0.76215601
		 0.323915 0.874394 0.36425099 0.87804598;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 68 ".vt[0:67]"  -9.19085312 5.37603617 9.33854866 -7.26346493 5.37603617 10.31564617
		 -7.36619997 7.75703907 10.59834385 -9.40995884 7.75690508 9.56225872 -3.86020899 5.37603617 10.31564617
		 -3.75747395 7.75703907 10.59834385 -1.93281996 5.37603617 9.33854866 -1.71371198 7.75690508 9.56225872
		 -8.78213882 13.43475533 8.92125034 -7.071821213 13.43475533 9.78830338 -7.071821213 16.12099457 9.78830338
		 -8.78213882 16.12099457 8.92125034 -4.051854134 13.43475533 9.78830338 -4.051854134 16.12099457 9.78830338
		 -2.34153605 13.43475533 8.92125034 -2.34153605 16.12099457 8.92125034 -6.16780186 22.014196396 9.78830338
		 -7.41609001 23.18336487 8.92125034 -4.051854134 21.49346352 9.78830338 -2.34153605 21.49346352 8.92125034
		 -6.16780186 22.014196396 7.018302917 -8.3719492 24.078636169 7.018302917 -4.051854134 21.49346352 7.018302917
		 -1.031885982 21.49346352 7.018302917 -6.16780186 22.014196396 4.24830198 -8.3719492 24.078636169 4.24830198
		 -4.051854134 21.49346352 4.24830198 -1.031885982 21.49346352 4.24830198 -6.16780186 22.014196396 1.47830403
		 -7.41609001 23.18336487 2.34535909 -4.051854134 21.49346352 1.47830403 -2.34153605 21.49346352 2.34535909
		 -7.071821213 16.12099075 1.47830403 -8.78213882 16.12099075 2.34535909 -4.051854134 16.12099075 1.47830403
		 -2.34153605 16.12099075 2.34535909 -7.071821213 13.43475437 1.47830403 -8.78213882 13.43475437 2.34535909
		 -4.051854134 13.43475437 1.47830403 -2.34153605 13.43475437 2.34535909 -9.40995884 7.75690413 1.70434904
		 -7.36619997 7.75703812 0.66826397 -7.26346493 5.37603617 0.95096201 -9.19085312 5.37603617 1.92805898
		 -3.75747395 7.75703812 0.66826397 -3.86020899 5.37603617 0.95096201 -1.71371198 7.75690413 1.70434904
		 -1.93281996 5.37603617 1.92805898 -7.26346493 5.37603617 4.072525024 -10.6667223 5.37603617 4.072525024
		 -3.86020899 5.37603617 4.072525024 -0.45695201 5.37603617 4.072525024 -7.26346493 5.37603617 7.19408321
		 -10.6667223 5.37603617 7.19408321 -3.86020899 5.37603617 7.19408321 -0.45695201 5.37603617 7.19408321
		 -0.148735 7.75684404 3.97828603 -0.148735 7.75684404 7.28832006 -1.031885982 13.43475533 4.24830198
		 -1.031885982 16.12099457 4.24830198 -1.031885982 13.43475533 7.018302917 -1.031885982 16.12099457 7.018302917
		 -10.97493935 7.75684404 3.97828603 -10.97493935 7.75684404 7.28832006 -10.091789246 13.43475533 4.24830198
		 -10.091789246 16.12099457 4.24830198 -10.091789246 13.43475533 7.018302917 -10.091789246 16.12099457 7.018302917;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 1 2 3 1 3 0 0 1 4 0 4 5 1 5 2 1
		 4 6 0 6 7 0 7 5 1 8 9 1 9 10 1 10 11 1 11 8 0 9 12 1 12 13 1 13 10 1 12 14 1 14 15 0
		 15 13 1 10 16 1 16 17 0 17 11 0 13 18 1 18 16 0 15 19 0 19 18 0 16 20 1 20 21 1 21 17 0
		 18 22 1 22 20 1 19 23 0 23 22 1 20 24 1 24 25 1 25 21 0 22 26 1 26 24 1 23 27 0 27 26 1
		 24 28 1 28 29 0 29 25 0 26 30 1 30 28 0 27 31 0 31 30 0 28 32 1 32 33 1 33 29 0 30 34 1
		 34 32 1 31 35 0 35 34 1 32 36 1 36 37 1 37 33 0 34 38 1 38 36 1 35 39 0 39 38 1 40 41 1
		 41 42 1 42 43 0 43 40 0 41 44 1 44 45 1 45 42 0 44 46 1 46 47 0 47 45 0 42 48 1 48 49 1
		 49 43 0 45 50 1 50 48 1 47 51 0 51 50 1 48 52 1 52 53 1 53 49 0 50 54 1 54 52 1 51 55 0
		 55 54 1 52 1 1 0 53 0 54 4 1 55 6 0 46 56 1 56 51 1 56 57 1 57 55 1 57 7 1 58 39 1
		 35 59 1 59 58 1 60 58 1 59 61 1 61 60 1 14 60 1 61 15 1 27 59 1 23 61 1 49 62 1 62 40 1
		 53 63 1 63 62 1 3 63 1 37 64 1 64 65 1 65 33 1 64 66 1 66 67 1 67 65 1 66 8 1 11 67 1
		 65 25 1 67 21 1 7 14 0 12 5 1 9 2 1 8 3 0 66 63 1 64 62 1 37 40 0 36 41 1 38 44 1
		 39 46 0 58 56 1 60 57 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 13
		mu 0 4 8 9 10 11
		f 4 14 15 16 -12
		mu 0 4 9 12 13 10
		f 4 17 18 19 -16
		mu 0 4 12 14 15 13
		f 4 -13 20 21 22
		mu 0 4 11 10 16 17
		f 4 -17 23 24 -21
		mu 0 4 10 13 18 16
		f 4 -20 25 26 -24
		mu 0 4 13 15 19 18
		f 4 -22 27 28 29
		mu 0 4 20 21 22 23
		f 4 -25 30 31 -28
		mu 0 4 21 24 25 22
		f 4 -27 32 33 -31
		mu 0 4 24 19 26 25
		f 4 -29 34 35 36
		mu 0 4 23 22 27 28
		f 4 -32 37 38 -35
		mu 0 4 22 25 29 27
		f 4 -34 39 40 -38
		mu 0 4 25 26 30 29
		f 4 -36 41 42 43
		mu 0 4 28 27 31 32
		f 4 -39 44 45 -42
		mu 0 4 27 29 33 31
		f 4 -41 46 47 -45
		mu 0 4 29 30 34 33
		f 4 -43 48 49 50
		mu 0 4 35 36 37 38
		f 4 -46 51 52 -49
		mu 0 4 36 39 40 37
		f 4 -48 53 54 -52
		mu 0 4 39 41 42 40
		f 4 -50 55 56 57
		mu 0 4 38 37 43 44
		f 4 -53 58 59 -56
		mu 0 4 37 40 45 43
		f 4 -55 60 61 -59
		mu 0 4 40 42 46 45
		f 4 62 63 64 65
		mu 0 4 47 48 49 50
		f 4 66 67 68 -64
		mu 0 4 48 51 52 49
		f 4 69 70 71 -68
		mu 0 4 51 53 54 52
		f 4 -65 72 73 74
		mu 0 4 55 56 57 58
		f 4 -69 75 76 -73
		mu 0 4 56 59 60 57
		f 4 -72 77 78 -76
		mu 0 4 59 61 62 60
		f 4 -74 79 80 81
		mu 0 4 58 57 63 64
		f 4 -77 82 83 -80
		mu 0 4 57 60 65 63
		f 4 -79 84 85 -83
		mu 0 4 60 62 66 65
		f 4 -81 86 -1 87
		mu 0 4 64 63 67 68
		f 4 -84 88 -5 -87
		mu 0 4 63 65 69 67
		f 4 -86 89 -8 -89
		mu 0 4 65 66 70 69
		f 4 -78 -71 90 91
		mu 0 4 62 54 53 71
		f 4 -85 -92 92 93
		mu 0 4 66 62 71 72
		f 4 -90 -94 94 -9
		mu 0 4 6 73 74 7
		f 4 95 -61 96 97
		mu 0 4 75 46 42 76
		f 4 98 -98 99 100
		mu 0 4 77 75 76 78
		f 4 101 -101 102 -19
		mu 0 4 14 79 80 15
		f 4 -97 -54 -47 103
		mu 0 4 76 42 41 81
		f 4 -100 -104 -40 104
		mu 0 4 78 76 81 82
		f 4 -103 -105 -33 -26
		mu 0 4 15 80 26 19
		f 4 -75 105 106 -66
		mu 0 4 50 83 84 47
		f 4 -82 107 108 -106
		mu 0 4 83 85 86 84
		f 4 -88 -4 109 -108
		mu 0 4 85 0 3 86
		f 4 110 111 112 -58
		mu 0 4 44 87 88 38
		f 4 113 114 115 -112
		mu 0 4 87 89 90 88
		f 4 116 -14 117 -115
		mu 0 4 89 8 11 90
		f 4 -113 118 -44 -51
		mu 0 4 38 88 91 35
		f 4 -116 119 -37 -119
		mu 0 4 88 90 92 91
		f 4 -118 -23 -30 -120
		mu 0 4 90 11 17 92
		f 4 -10 120 -18 121
		mu 0 4 5 7 14 12
		f 4 -7 -122 -15 122
		mu 0 4 2 5 12 9
		f 4 -3 -123 -11 123
		mu 0 4 3 2 9 8
		f 4 -110 -124 -117 124
		mu 0 4 86 3 8 89
		f 4 -109 -125 -114 125
		mu 0 4 84 86 89 87
		f 4 -107 -126 -111 126
		mu 0 4 47 84 87 44
		f 4 -57 127 -63 -127
		mu 0 4 44 43 48 47
		f 4 -60 128 -67 -128
		mu 0 4 43 45 51 48
		f 4 -62 129 -70 -129
		mu 0 4 45 46 53 51
		f 4 -91 -130 -96 130
		mu 0 4 71 53 46 75
		f 4 -93 -131 -99 131
		mu 0 4 72 71 75 77
		f 4 -95 -132 -102 -121
		mu 0 4 7 74 79 14;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_armR";
	setAttr ".rp" -type "double3" -14.384239196777344 38.332521438598633 4.1614402383565903 ;
	setAttr ".sp" -type "double3" -14.384239196777344 38.332521438598633 4.1614402383565903 ;
createNode mesh -n "bear_armRShape" -p "bear_armR";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 93 ".uvst[0].uvsp[0:92]" -type "float2" 0.96047199 0.35620901
		 0.93612599 0.35867801 0.933779 0.32674199 0.957497 0.32438201 0.89889801 0.36140001
		 0.896887 0.3294 0.87410498 0.362445 0.87337703 0.33034399 0.94493502 0.226761 0.92379498
		 0.228856 0.92089301 0.19314 0.94098401 0.191101 0.89135599 0.23103 0.88951302 0.19500899
		 0.86998999 0.231795 0.86916101 0.19566201 0.90199602 0.079856001 0.918239 0.083102003
		 0.87793797 0.072323002 0.86230499 0.067771003 0.90426302 0.074662998 0.926157 0.061788999
		 0.93887597 0.083402999 0.89159 0.05294 0.91348302 0.040109999 0.89126003 0.036649
		 0.90079302 0.018445 0.94805998 0.048911002 0.96076697 0.070552997 0.93536901 0.027256001
		 0.92267197 0.0056019998 0.96994799 0.036063999 0.97029299 0.052340999 0.95725399
		 0.014414 0.943214 0.0061690002 0.73984599 0.080081001 0.75630099 0.077352002 0.73425603
		 0.19005901 0.71451902 0.187396 0.78064501 0.070643 0.76539201 0.19295099 0.79645097
		 0.066653997 0.78562498 0.19428 0.72983402 0.225566 0.7087 0.222597 0.76218098 0.22890399
		 0.78349102 0.23037 0.69257599 0.319608 0.71600997 0.32299599 0.71214199 0.35483101
		 0.687365 0.35120901 0.75275898 0.327003 0.749529 0.35892099 0.77625501 0.32872701
		 0.77439499 0.36079299 0.67810398 0.365951 0.68511099 0.38812101 0.64960402 0.38086101
		 0.65689099 0.34563601 0.67798001 0.42324799 0.642474 0.41605201 0.66284502 0.44090101
		 0.63537598 0.45120999 0.61404002 0.37374699 0.620983 0.33858901 0.60695499 0.40891099
		 0.59987402 0.444065 0.57847798 0.36666599 0.59355497 0.34898099 0.57142901 0.40178999
		 0.57854003 0.423924 0.80534297 0.36216 0.80620098 0.33008 0.84318101 0.36278999 0.84342998
		 0.330704 0.81025898 0.231529 0.81138903 0.19530199 0.84318203 0.23207501 0.84333402
		 0.19583 0.816764 0.063565999 0.84212297 0.063991003 0.662835 0.31414801 0.62563801
		 0.30632299 0.99019003 0.35325599 0.98802501 0.32066301 0.68243003 0.217703 0.68967402
		 0.182658 0.650792 0.210356 0.65901297 0.17435101 0.97100198 0.22317199 0.966497 0.18716601
		 0.71877801 0.079862997 0.694291 0.072465003;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 68 ".vt[0:67]"  -24.67152405 35.62486649 1.15797496 -24.69587135 37.042572021 0.36594301
		 -22.46442604 37.0052986145 0.13679001 -22.43873024 35.50199509 0.97663701 -24.73886681 39.54586792 0.36594301
		 -22.5100174 39.65974426 0.13679001 -24.7632122 40.96356964 1.15797496 -22.53595924 41.16304016 0.97663701
		 -15.6006937 36.081378937 1.49623704 -15.62230206 37.3394165 0.79340702 -13.10625839 37.38262939 0.79340702
		 -13.084650993 36.1245842 1.49623704 -15.66045475 39.56078339 0.79340702 -13.14440727 39.60398483 0.79340702
		 -15.68206215 40.81881332 1.49623704 -13.16601658 40.86203766 1.49623704 -5.18604612 38.49436951 1.50404894
		 -5.52568388 37.56153107 2.058587074 -4.58633089 40.14151382 1.50404894 -4.24669313 41.074344635 2.058587074
		 -5.18604612 38.49436951 3.27564096 -5.78575993 36.84723282 3.27564096 -4.58633089 40.14151382 3.27564096
		 -3.98661804 41.78863907 3.27564096 -5.18604612 38.49436951 5.047235966 -5.78575993 36.84723282 5.047235966
		 -4.58633089 40.14151382 5.047235966 -3.98661804 41.78863907 5.047235966 -5.18604612 38.49436951 6.8188262
		 -5.52568388 37.56153107 6.26429081 -4.58633089 40.14151382 6.8188262 -4.24669313 41.074344635 6.26429081
		 -13.10625839 37.38263321 7.52947187 -13.084650993 36.1245842 6.82663822 -13.14440727 39.60398865 7.52947187
		 -13.16601658 40.86203766 6.82663822 -15.62230206 37.3394165 7.52947187 -15.6006937 36.081378937 6.82663822
		 -15.66045475 39.56078339 7.52947187 -15.68206215 40.81881332 6.82663822 -22.43873215 35.50199509 7.34623909
		 -22.46442795 37.0052986145 8.18609047 -24.69587135 37.042572021 7.95693493 -24.67152405 35.62486649 7.16490078
		 -22.5100193 39.65974426 8.18609047 -24.73886681 39.54586792 7.95693493 -22.53595924 41.16304016 7.34623909
		 -24.7632122 40.96356964 7.16490078 -24.69587135 37.042572021 5.42660284 -24.6528759 34.53926468 5.42660284
		 -24.73886681 39.54586792 5.42660284 -24.78186035 42.049160004 5.42660284 -24.69587135 37.042572021 2.89627194
		 -24.6528759 34.53926468 2.89627194 -24.73886681 39.54586792 2.89627194 -24.78186035 42.049160004 2.89627194
		 -22.55578804 42.31417465 5.50298882 -22.55578804 42.31417465 2.81988597 -15.69860554 41.78214645 5.28411388
		 -13.18256092 41.82535172 5.28411388 -15.69860554 41.78214645 3.038762093 -13.18256092 41.82535172 3.038762093
		 -22.41901779 34.35086823 5.50298882 -22.41901779 34.35086823 2.81988597 -15.58415031 35.11805725 5.28411388
		 -13.068105698 35.16126633 5.28411388 -15.58415031 35.11805725 3.038762093 -13.068105698 35.16126633 3.038762093;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 1 2 3 1 3 0 0 1 4 0 4 5 1 5 2 1
		 4 6 0 6 7 0 7 5 1 8 9 1 9 10 1 10 11 1 11 8 0 9 12 1 12 13 1 13 10 1 12 14 1 14 15 0
		 15 13 1 10 16 1 16 17 0 17 11 0 13 18 1 18 16 0 15 19 0 19 18 0 16 20 1 20 21 1 21 17 0
		 18 22 1 22 20 1 19 23 0 23 22 1 20 24 1 24 25 1 25 21 0 22 26 1 26 24 1 23 27 0 27 26 1
		 24 28 1 28 29 0 29 25 0 26 30 1 30 28 0 27 31 0 31 30 0 28 32 1 32 33 1 33 29 0 30 34 1
		 34 32 1 31 35 0 35 34 1 32 36 1 36 37 1 37 33 0 34 38 1 38 36 1 35 39 0 39 38 1 40 41 1
		 41 42 1 42 43 0 43 40 0 41 44 1 44 45 1 45 42 0 44 46 1 46 47 0 47 45 0 42 48 1 48 49 1
		 49 43 0 45 50 1 50 48 1 47 51 0 51 50 1 48 52 1 52 53 1 53 49 0 50 54 1 54 52 1 51 55 0
		 55 54 1 52 1 1 0 53 0 54 4 1 55 6 0 46 56 1 56 51 1 56 57 1 57 55 1 57 7 1 58 39 1
		 35 59 1 59 58 1 60 58 1 59 61 1 61 60 1 14 60 1 61 15 1 27 59 1 23 61 1 49 62 1 62 40 1
		 53 63 1 63 62 1 3 63 1 37 64 1 64 65 1 65 33 1 64 66 1 66 67 1 67 65 1 66 8 1 11 67 1
		 65 25 1 67 21 1 7 14 0 12 5 1 9 2 1 8 3 0 66 63 1 64 62 1 37 40 0 36 41 1 38 44 1
		 39 46 0 58 56 1 60 57 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 13
		mu 0 4 8 9 10 11
		f 4 14 15 16 -12
		mu 0 4 9 12 13 10
		f 4 17 18 19 -16
		mu 0 4 12 14 15 13
		f 4 -13 20 21 22
		mu 0 4 11 10 16 17
		f 4 -17 23 24 -21
		mu 0 4 10 13 18 16
		f 4 -20 25 26 -24
		mu 0 4 13 15 19 18
		f 4 -22 27 28 29
		mu 0 4 17 20 21 22
		f 4 -25 30 31 -28
		mu 0 4 20 23 24 21
		f 4 -27 32 33 -31
		mu 0 4 23 25 26 24
		f 4 -29 34 35 36
		mu 0 4 22 21 27 28
		f 4 -32 37 38 -35
		mu 0 4 21 24 29 27
		f 4 -34 39 40 -38
		mu 0 4 24 26 30 29
		f 4 -36 41 42 43
		mu 0 4 28 27 31 32
		f 4 -39 44 45 -42
		mu 0 4 27 29 33 31
		f 4 -41 46 47 -45
		mu 0 4 29 30 34 33
		f 4 -43 48 49 50
		mu 0 4 35 36 37 38
		f 4 -46 51 52 -49
		mu 0 4 36 39 40 37
		f 4 -48 53 54 -52
		mu 0 4 39 41 42 40
		f 4 -50 55 56 57
		mu 0 4 38 37 43 44
		f 4 -53 58 59 -56
		mu 0 4 37 40 45 43
		f 4 -55 60 61 -59
		mu 0 4 40 42 46 45
		f 4 62 63 64 65
		mu 0 4 47 48 49 50
		f 4 66 67 68 -64
		mu 0 4 48 51 52 49
		f 4 69 70 71 -68
		mu 0 4 51 53 54 52
		f 4 -65 72 73 74
		mu 0 4 55 56 57 58
		f 4 -69 75 76 -73
		mu 0 4 56 59 60 57
		f 4 -72 77 78 -76
		mu 0 4 59 61 62 60
		f 4 -74 79 80 81
		mu 0 4 58 57 63 64
		f 4 -77 82 83 -80
		mu 0 4 57 60 65 63
		f 4 -79 84 85 -83
		mu 0 4 60 62 66 65
		f 4 -81 86 -1 87
		mu 0 4 64 63 67 68
		f 4 -84 88 -5 -87
		mu 0 4 63 65 69 67
		f 4 -86 89 -8 -89
		mu 0 4 65 66 70 69
		f 4 -78 -71 90 91
		mu 0 4 71 54 53 72
		f 4 -85 -92 92 93
		mu 0 4 73 71 72 74
		f 4 -90 -94 94 -9
		mu 0 4 6 73 74 7
		f 4 95 -61 96 97
		mu 0 4 75 46 42 76
		f 4 98 -98 99 100
		mu 0 4 77 75 76 78
		f 4 101 -101 102 -19
		mu 0 4 14 77 78 15
		f 4 -97 -54 -47 103
		mu 0 4 76 42 41 79
		f 4 -100 -104 -40 104
		mu 0 4 78 76 79 80
		f 4 -103 -105 -33 -26
		mu 0 4 15 78 80 19
		f 4 -75 105 106 -66
		mu 0 4 50 58 81 47
		f 4 -82 107 108 -106
		mu 0 4 58 64 82 81
		f 4 -88 -4 109 -108
		mu 0 4 83 0 3 84
		f 4 110 111 112 -58
		mu 0 4 44 85 86 38
		f 4 113 114 115 -112
		mu 0 4 85 87 88 86
		f 4 116 -14 117 -115
		mu 0 4 89 8 11 90
		f 4 -113 118 -44 -51
		mu 0 4 38 86 91 35
		f 4 -116 119 -37 -119
		mu 0 4 86 88 92 91
		f 4 -118 -23 -30 -120
		mu 0 4 90 11 17 22
		f 4 -10 120 -18 121
		mu 0 4 5 7 14 12
		f 4 -7 -122 -15 122
		mu 0 4 2 5 12 9
		f 4 -3 -123 -11 123
		mu 0 4 3 2 9 8
		f 4 -110 -124 -117 124
		mu 0 4 84 3 8 89
		f 4 -109 -125 -114 125
		mu 0 4 81 82 87 85
		f 4 -107 -126 -111 126
		mu 0 4 47 81 85 44
		f 4 -57 127 -63 -127
		mu 0 4 44 43 48 47
		f 4 -60 128 -67 -128
		mu 0 4 43 45 51 48
		f 4 -62 129 -70 -129
		mu 0 4 45 46 53 51
		f 4 -91 -130 -96 130
		mu 0 4 72 53 46 75
		f 4 -93 -131 -99 131
		mu 0 4 74 72 75 77
		f 4 -95 -132 -102 -121
		mu 0 4 7 74 77 14;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "basePatch:pPlane4";
createNode mesh -n "basePatch:pPlane4Shape" -p "basePatch:pPlane4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 25 ".uvst[0].uvsp[0:24]" -type "float2" 0.0057799998 0.0057799998
		 0.25145301 0.0057799998 0.25145301 0.251454 0.0057799998 0.251454 0.497127 0.0057799998
		 0.497127 0.251454 0.74280101 0.0057799998 0.74280101 0.251454 0.988473 0.0057799998
		 0.988473 0.251454 0.25145301 0.497127 0.0057799998 0.497127 0.497127 0.497127 0.74280101
		 0.497127 0.988473 0.497127 0.25145301 0.74280101 0.0057799998 0.74280101 0.497127
		 0.74280101 0.74280101 0.74280101 0.988473 0.74280101 0.25145301 0.988473 0.0057799998
		 0.988473 0.497127 0.988473 0.74280101 0.988473 0.988473 0.988473;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 25 ".vt[0:24]"  23.5090847 17.083831787 2.92554808 25.19581223 17.083831787 2.92554808
		 26.88254738 17.083831787 2.92554808 28.56928062 17.083831787 2.92554808 30.25601006 17.083831787 2.92554808
		 23.5090847 18.77056885 2.92554808 25.19581223 18.77056885 2.92554808 26.88254738 18.77056885 2.92554808
		 28.56928062 18.77056885 2.92554808 30.25601006 18.77056885 2.92554808 23.5090847 20.45729637 2.92554808
		 25.19581223 20.45729637 2.92554808 26.88254738 20.45729637 2.92554808 28.56928062 20.45729637 2.92554808
		 30.25601006 20.45729637 2.92554808 23.5090847 22.14402962 2.92554808 25.19581223 22.14402962 2.92554808
		 26.88254738 22.14402962 2.92554808 28.56928062 22.14402962 2.92554808 30.25601006 22.14402962 2.92554808
		 23.5090847 23.83076477 2.92554808 25.19581223 23.83076477 2.92554808 26.88254738 23.83076477 2.92554808
		 28.56928062 23.83076477 2.92554808 30.25601006 23.83076477 2.92554808;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 6 1 6 5 1 5 0 0 1 2 0 2 7 1 7 6 1
		 2 3 0 3 8 1 8 7 1 3 4 0 4 9 0 9 8 1 6 11 1 11 10 1 10 5 0 7 12 1 12 11 1 8 13 1 13 12 1
		 9 14 0 14 13 1 11 16 1 16 15 1 15 10 0 12 17 1 17 16 1 13 18 1 18 17 1 14 19 0 19 18 1
		 16 21 1 21 20 0 20 15 0 17 22 1 22 21 0 18 23 1 23 22 0 19 24 0 24 23 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 -9
		mu 0 4 6 8 9 7
		f 4 -3 13 14 15
		mu 0 4 3 2 10 11
		f 4 -7 16 17 -14
		mu 0 4 2 5 12 10
		f 4 -10 18 19 -17
		mu 0 4 5 7 13 12
		f 4 -13 20 21 -19
		mu 0 4 7 9 14 13
		f 4 -15 22 23 24
		mu 0 4 11 10 15 16
		f 4 -18 25 26 -23
		mu 0 4 10 12 17 15
		f 4 -20 27 28 -26
		mu 0 4 12 13 18 17
		f 4 -22 29 30 -28
		mu 0 4 13 14 19 18
		f 4 -24 31 32 33
		mu 0 4 16 15 20 21
		f 4 -27 34 35 -32
		mu 0 4 15 17 22 20
		f 4 -29 36 37 -35
		mu 0 4 17 18 23 22
		f 4 -31 38 39 -37
		mu 0 4 18 19 24 23;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_body";
	setAttr ".rp" -type "double3" 0.37089014053344727 30.240166664123535 5.1618075370788574 ;
	setAttr ".sp" -type "double3" 0.37089014053344727 30.240166664123535 5.1618075370788574 ;
createNode mesh -n "bear_bodyShape" -p "bear_body";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 79 ".uvst[0].uvsp[0:78]" -type "float2" 0.22102199 0.43782699
		 0.32482499 0.40322399 0.346405 0.517533 0.25375801 0.51980197 0.49682701 0.41247001
		 0.48074201 0.52575803 0.60459697 0.45629501 0.57074702 0.53585601 0.33029199 0.65231597
		 0.269627 0.60762501 0.486296 0.66154402 0.54332203 0.61429101 0.35764301 0.73748899
		 0.344762 0.73579001 0.44193301 0.73614699 0.45663601 0.75146103 0.36261499 0.78099
		 0.29149401 0.78856099 0.43228501 0.779468 0.50070798 0.79366702 0.31150201 0.80870301
		 0.34844601 0.89300197 0.30585599 0.85378897 0.40863499 0.90250802 0.47220799 0.82180297
		 0.46129799 0.87828302 0.240153 0.81638098 0.18673199 0.83668298 0.16155601 0.838274
		 0.20372599 0.83499402 0.38825199 0.91816199 0.362975 0.91416502 0.59839702 0.87827802
		 0.56102598 0.83147401 0.59254098 0.86326802 0.61857003 0.89620697 0.74289 0.90345198
		 0.76755297 0.99021101 0.63606602 0.966268 0.63112301 0.92428499 0.71515101 0.83507103
		 0.67254001 0.75741202 0.7622 0.90043098 0.79279 0.978122 0.73119599 0.82311201 0.69919503
		 0.74359 0.94036299 0.828619 0.888753 0.93584001 0.90991801 0.75256503 0.79870898
		 0.70933098 0.24728701 0.0085340003 0.37150601 0.0057799998 0.366063 0.165584 0.23072
		 0.14517701 0.466299 0.006325 0.470144 0.16616701 0.59048998 0.010492 0.60556501 0.14743
		 0.368527 0.31665701 0.22583801 0.277237 0.47422901 0.32162601 0.60920799 0.27971601
		 0.25970799 0.34557101 0.56107903 0.343297 0.77207601 0.578686 0.69527799 0.64342302
		 0.67144197 0.50996399 0.66446 0.61950302 0.62119102 0.73015499 0.56567597 0.692922
		 0.0057799998 0.67102402 0.042599 0.54370999 0.113981 0.61557198 0.102703 0.71252
		 0.148297 0.48418 0.14461 0.59511501 0.182055 0.70912403 0.128823 0.73379499 0.239788
		 0.67596602;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 56 ".vt[0:55]"  -8.34742737 22.96451759 11.8078537 -4.64338112 22.39204216 14.83432388
		 4.38824081 22.48993683 15.12466812 8.068534851 22.8783989 11.42551899 -9.78660393 28.32244873 13.53318596
		 -4.39834595 29.4337368 17.59593964 5.32067823 29.43203163 17.61429214 9.82369518 27.94546127 12.88995838
		 -8.80706596 33.80234146 12.055464745 -4.38094187 35.91559219 11.87400818 5.38809109 35.91559219 11.87400818
		 8.7668438 32.80610275 11.71956825 -2.76336098 40.35173035 9.41383457 -2.084278107 39.76198578 8.57546616
		 2.82605696 39.76198578 9.32125378 3.63201499 41.13692856 9.51311111 -4.42000818 42.32611847 5.69385481
		 -1.51015604 42.32611847 7.76945114 2.25193501 42.32611847 8.51523781 5.16178608 42.32611847 6.43964291
		 -4.42000818 42.32611847 2.89004707 -1.51015604 42.32611847 0.81445098 2.25193501 42.32611847 0.81445098
		 5.16178608 42.32611847 2.89004707 -2.13227701 41.96360779 1.56829405 -0.41868499 41.96360779 -0.26761901
		 1.160465 41.96360779 -0.26761901 2.87405705 41.96360779 1.56829405 -6.99461317 35.65672302 0.018863
		 -2.084278107 35.65672302 -3.07355094 2.826056 35.65672302 -3.07355094 7.73639202 35.65672302 0.018866001
		 -6.99461317 34.025917053 -0.51998502 -2.084278107 34.90564728 -1.69891202 2.826056 34.90564728 -1.69891202
		 7.73639202 34.025917053 -0.51998502 -6.99461317 29.044416428 -4.52809381 -2.084278107 24.38182068 -7.29067707
		 2.82605696 24.38182068 -7.29067707 7.73639202 29.044416428 -4.52809381 -10.061608315 23.71173286 0.68049502
		 -3.10660911 18.3201561 0.81444901 3.8483901 18.3201561 0.81444901 10.8033886 23.71173286 0.68049502
		 -10.061608315 23.56178665 7.86221981 -2.9060781 18.15421486 10.5728054 4.037605762 18.47626114 10.80416679
		 10.8033886 23.56178665 7.86221981 10.37456036 29.61525536 2.55997896 10.37378597 29.61136055 4.93536711
		 9.39301968 36.51143646 3.31090307 9.39301968 36.51143646 7.76945114 -9.63277531 29.61525536 2.55997896
		 -9.63200378 29.61136055 4.93536711 -8.65124035 36.51143646 3.31090307 -8.65124035 36.51143646 7.76945114;
	setAttr -s 108 ".ed[0:107]"  0 1 0 1 5 1 5 4 1 4 0 0 1 2 0 2 6 1 6 5 1
		 2 3 0 3 7 0 7 6 1 5 9 1 9 8 1 8 4 0 6 10 1 10 9 1 7 11 0 11 10 1 9 13 1 13 12 0 12 8 0
		 10 14 1 14 13 0 11 15 0 15 14 0 13 17 1 17 16 1 16 12 0 14 18 1 18 17 1 15 19 0 19 18 1
		 17 21 1 21 20 1 20 16 0 18 22 1 22 21 1 19 23 0 23 22 1 21 25 1 25 24 0 24 20 0 22 26 1
		 26 25 0 23 27 0 27 26 0 29 28 1 28 24 0 25 29 1 30 29 1 26 30 1 31 30 1 27 31 0 29 33 1
		 33 32 1 32 28 0 30 34 1 34 33 1 31 35 0 35 34 1 33 37 1 37 36 0 36 32 0 34 38 1 38 37 0
		 35 39 0 39 38 0 37 41 1 41 40 1 40 36 0 38 42 1 42 41 1 39 43 0 43 42 1 41 45 1 45 44 1
		 44 40 0 42 46 1 46 45 1 43 47 0 47 46 1 45 1 1 0 44 0 46 2 1 47 3 0 35 48 1 48 43 1
		 48 49 1 49 47 1 49 7 1 31 50 1 50 48 1 50 51 1 51 49 1 51 11 1 23 50 1 19 51 1 40 52 1
		 52 32 1 44 53 1 53 52 1 4 53 1 52 54 1 54 28 1 53 55 1 55 54 1 8 55 1 54 20 1 55 16 1;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 -3 10 11 12
		mu 0 4 3 2 8 9
		f 4 -7 13 14 -11
		mu 0 4 2 5 10 8
		f 4 -10 15 16 -14
		mu 0 4 5 7 11 10
		f 4 -12 17 18 19
		mu 0 4 9 8 12 13
		f 4 -15 20 21 -18
		mu 0 4 8 10 14 12
		f 4 -17 22 23 -21
		mu 0 4 10 11 15 14
		f 4 -19 24 25 26
		mu 0 4 13 12 16 17
		f 4 -22 27 28 -25
		mu 0 4 12 14 18 16
		f 4 -24 29 30 -28
		mu 0 4 14 15 19 18
		f 4 -26 31 32 33
		mu 0 4 20 16 21 22
		f 4 -29 34 35 -32
		mu 0 4 16 18 23 21
		f 4 -31 36 37 -35
		mu 0 4 18 24 25 23
		f 4 -33 38 39 40
		mu 0 4 26 27 28 29
		f 4 -36 41 42 -39
		mu 0 4 21 23 30 31
		f 4 -38 43 44 -42
		mu 0 4 32 33 34 35
		f 4 45 46 -40 47
		mu 0 4 36 37 38 39
		f 4 48 -48 -43 49
		mu 0 4 40 36 39 35
		f 4 50 -50 -45 51
		mu 0 4 41 40 35 34
		f 4 -46 52 53 54
		mu 0 4 37 36 42 43
		f 4 -49 55 56 -53
		mu 0 4 36 40 44 42
		f 4 -51 57 58 -56
		mu 0 4 40 41 45 44
		f 4 -54 59 60 61
		mu 0 4 43 42 46 47
		f 4 -57 62 63 -60
		mu 0 4 42 44 48 46
		f 4 -59 64 65 -63
		mu 0 4 44 45 49 48
		f 4 -61 66 67 68
		mu 0 4 50 51 52 53
		f 4 -64 69 70 -67
		mu 0 4 51 54 55 52
		f 4 -66 71 72 -70
		mu 0 4 54 56 57 55
		f 4 -68 73 74 75
		mu 0 4 53 52 58 59
		f 4 -71 76 77 -74
		mu 0 4 52 55 60 58
		f 4 -73 78 79 -77
		mu 0 4 55 57 61 60
		f 4 -75 80 -1 81
		mu 0 4 59 58 1 62
		f 4 -78 82 -5 -81
		mu 0 4 58 60 4 1
		f 4 -80 83 -8 -83
		mu 0 4 60 61 63 4
		f 4 -72 -65 84 85
		mu 0 4 64 49 45 65
		f 4 -79 -86 86 87
		mu 0 4 66 64 65 67
		f 4 -84 -88 88 -9
		mu 0 4 6 66 67 7
		f 4 -85 -58 89 90
		mu 0 4 65 45 41 68
		f 4 -87 -91 91 92
		mu 0 4 67 65 68 69
		f 4 -89 -93 93 -16
		mu 0 4 7 67 69 11
		f 4 -90 -52 -44 94
		mu 0 4 68 41 34 33
		f 4 95 -92 -95 -37
		mu 0 4 19 69 68 33
		f 4 -94 -96 -30 -23
		mu 0 4 11 69 19 15
		f 4 -69 96 97 -62
		mu 0 4 70 71 72 73
		f 4 -76 98 99 -97
		mu 0 4 71 74 75 72
		f 4 -82 -4 100 -99
		mu 0 4 74 0 3 75
		f 4 -98 101 102 -55
		mu 0 4 73 72 76 77
		f 4 -100 103 104 -102
		mu 0 4 72 75 78 76
		f 4 -101 -13 105 -104
		mu 0 4 75 3 9 78
		f 4 -103 106 -41 -47
		mu 0 4 77 76 26 29
		f 4 -105 107 -34 -107
		mu 0 4 76 78 17 26
		f 4 -106 -20 -27 -108
		mu 0 4 78 9 13 17;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_bow";
	setAttr ".rp" -type "double3" 0.46309947967529297 39.008340835571289 10.05832052230835 ;
	setAttr ".sp" -type "double3" 0.46309947967529297 39.008340835571289 10.05832052230835 ;
createNode mesh -n "bear_bowShape" -p "bear_bow";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 554 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.30296201 0.89694899 0.248197
		 0.92545801 0.245691 0.90095103 0.30324399 0.888376 0.195265 0.95112801 0.19452199
		 0.91654003 0.121094 0.96716201 0.11885 0.928177 0.043804999 0.92378098 0.047336999
		 0.95620799 0.011946 0.945831 0.0084480001 0.91877401 0.244464 0.87555897 0.30384299
		 0.88009298 0.19314601 0.87688798 0.116654 0.88059402 0.041655 0.883757 0.006695 0.88516599
		 0.243931 0.85090101 0.30440801 0.871108 0.190348 0.838557 0.114467 0.83297998 0.040474001
		 0.84364599 0.0056980001 0.85145599 0.24311 0.82845598 0.305318 0.85835499 0.186928
		 0.80794901 0.11403 0.79364699 0.0071729999 0.82413399 0.041644 0.81096298 0.36942199
		 0.90775597 0.36681601 0.92946899 0.419269 0.92910701 0.41565299 0.96280301 0.49317399
		 0.94878203 0.48698899 0.98733002 0.56823999 0.95219201 0.60390103 0.950854 0.59766501
		 0.97739398 0.56141597 0.98406601 0.37271199 0.883205 0.424476 0.88994199 0.50020301
		 0.90172499 0.57448 0.91264099 0.609088 0.91764998 0.37584499 0.85840398 0.431225
		 0.85197198 0.50729603 0.85470802 0.57981098 0.87292099 0.61357403 0.88427699 0.37904
		 0.83563501 0.43786299 0.82170397 0.51184601 0.81585902 0.61496103 0.85697001 0.58205003
		 0.84033197 0.30987701 0.79865903 0.30956799 0.801956 0.27883399 0.76310903 0.292384
		 0.746481 0.24886499 0.72742403 0.26633599 0.70078403 0.198424 0.68367499 0.21831
		 0.65358001 0.14130899 0.65216601 0.112865 0.63979101 0.125545 0.61876398 0.157472
		 0.62696999 0.30924299 0.81143498 0.2669 0.78215498 0.230349 0.75590801 0.175037 0.71775901
		 0.121346 0.680547 0.09646 0.66316301 0.308799 0.81977099 0.256722 0.80015999 0.21095499
		 0.78250998 0.15097401 0.75153601 0.101432 0.70900798 0.080059998 0.68654799 0.308126
		 0.83045799 0.24550401 0.81620401 0.19217999 0.80413902 0.12892701 0.78083497 0.064560004
		 0.70561701 0.083154 0.73280698 0.34133899 0.74383003 0.35142201 0.76188999 0.37463301
		 0.70331198 0.38741001 0.731736 0.42847401 0.66356999 0.44363701 0.69612098 0.50474298
		 0.67302698 0.49240801 0.64577502 0.52516103 0.64226002 0.534639 0.664864 0.36088601
		 0.783876 0.40146101 0.76306099 0.461858 0.73321801 0.52035898 0.70393699 0.54744703
		 0.69026399 0.36880699 0.80429298 0.41707799 0.79255903 0.48087299 0.76995802 0.53601199
		 0.73483998 0.56031901 0.71560502 0.377958 0.82247502 0.43285099 0.81672901 0.49863499
		 0.80181998 0.57299602 0.73655099 0.550807 0.76086801 0.302481 0.74284601 0.30930299
		 0.803222 0.274313 0.69184899 0.223097 0.64179897 0.123915 0.60683697 0.158186 0.61461598
		 0.30722201 0.84267998 0.242117 0.82353401 0.18615299 0.80930698 0.118039 0.79017597
		 0.071693003 0.73762202 0.052761 0.70813102 0.33458999 0.73990798 0.36896399 0.69412398
		 0.425459 0.651223 0.49344701 0.63342202 0.52845699 0.630683 0.38060999 0.83093297
		 0.43829 0.822815 0.508183 0.81225502 0.584315 0.74070299 0.56149799 0.76724398 0.047626
		 0.80649298 0.01361 0.817711 0.57663703 0.83521599 0.60931802 0.84984797 0.194047
		 0.95472699 0.123935 0.96993202 0.248026 0.93456399 0.303563 0.90314102 0.36914399
		 0.93330097 0.41662499 0.965841 0.48387501 0.98989999 0.55495697 0.987441 0.59030497
		 0.98264199 0.053472999 0.96014303 0.018808 0.95172 0.59021097 0.183157 0.49595201
		 0.19205099 0.499461 0.17071 0.60639298 0.175266 0.40690899 0.19871099 0.41005999
		 0.16822299 0.314895 0.19886599 0.32684699 0.16651 0.229223 0.19545799 0.25380701
		 0.17172199 0.50372499 0.143057 0.60906398 0.163928 0.41767201 0.13092101 0.34711999
		 0.130721 0.279998 0.12836 0.596901 0.13777199 0.60791099 0.151447 0.50786102 0.115582
		 0.51089197 0.093956999 0.429326 0.095565997 0.436966 0.066055 0.373391 0.101423 0.381051
		 0.068053 0.29425299 0.094020002 0.29611501 0.058499001 0.7798 0.38818699 0.76723301
		 0.40159199 0.792597 0.297681 0.814174 0.29994401 0.81525999 0.210986 0.84547901 0.21627399
		 0.83689499 0.130582 0.87127399 0.12799101 0.862293 0.061843 0.89188999 0.044783 0.75527197
		 0.40078601 0.76476902 0.294054 0.77729303 0.207891 0.79685199 0.140048 0.81353498
		 0.075117998 0.73319101 0.381578 0.715505 0.28675699 0.73715597 0.290261 0.74320698
		 0.39634499 0.70967799 0.20799699 0.74011099 0.20912801 0.72695202 0.154717 0.76125598
		 0.156875 0.74167502 0.070308 0.77652001 0.078642003 0.81882799 0.29995701 0.77767098
		 0.399468 0.76507401 0.43963701 0.85187101 0.21742 0.878506 0.127608 0.89826 0.030924
		 0.86761302 0.047481 0.75960499 0.43899301 0.81639397 0.061131001 0.74059701 0.420219
		 0.732979 0.39234501 0.710733 0.28559801 0.70327401 0.207009 0.71930403 0.152657 0.738235
		 0.055295002 0.77884799 0.059967998 0.60398799 0.186225 0.49568301 0.196638 0.64690399
		 0.186161 0.64828998 0.18005501 0.633636 0.15686899 0.608603 0.141238 0.51213801 0.089327
		 0.43879101 0.059826002 0.38192701 0.060231999 0.28275001 0.050996002 0.275711 0.091712996
		 0.26578799 0.12795199 0.23899101 0.173407 0.21438 0.19797 0.312729 0.205762 0.40655401
		 0.205172 0.67212301 0.94652599 0.634206 0.85535002 0.655927 0.853926 0.68517798 0.95791501
		 0.60901898 0.77346802 0.63915098 0.76898098 0.59684199 0.70551002 0.63328701 0.69449103
		 0.56624597 0.63094801 0.60440499 0.60280699 0.68380398 0.85196 0.696329 0.95691401
		 0.677607 0.76367402 0.67685097 0.68503201 0.67329901 0.60910499 0.71732497 0.94357997
		 0.707452 0.95646101 0.71170402 0.85030502 0.73341298 0.84888101 0.716474 0.760656
		 0.74720001 0.76016402;
	setAttr ".uvst[0].uvsp[250:499]" 0.72105098 0.67876202 0.75838101 0.67350203
		 0.73472297 0.61334401 0.773772 0.60367501 0.83760798 0.60794502 0.84989399 0.59553802
		 0.82897598 0.70127702 0.80713099 0.70182902 0.81881303 0.78778797 0.78834599 0.78596598
		 0.81777197 0.86414403 0.78043902 0.85634702 0.795825 0.95743698 0.755288 0.93318099
		 0.86111498 0.59570998 0.85701299 0.70101798 0.85774702 0.78970701 0.86286998 0.86823201
		 0.86537898 0.942249 0.88334101 0.60725999 0.90683901 0.70036399 0.88503599 0.700459
		 0.87226599 0.595204 0.92760903 0.78784102 0.89686602 0.78955501 0.94523603 0.87342799
		 0.90764397 0.87081999 0.96574801 0.941948 0.92618799 0.93476498 0.80229801 0.70226902
		 0.83984703 0.59684801 0.854716 0.57288402 0.78194302 0.78566301 0.77280903 0.85400802
		 0.74628001 0.93952298 0.78961402 0.978562 0.862207 0.57322001 0.86514097 0.95966399
		 0.86961299 0.57266003 0.88213199 0.59626502 0.91166902 0.70067602 0.93407202 0.78749901
		 0.95310903 0.87412298 0.97378999 0.95257902 0.93040502 0.94847101 0.67510402 0.958857
		 0.62954301 0.85625702 0.69176501 0.98180902 0.69934601 0.98094898 0.706909 0.98080498
		 0.71734399 0.95602697 0.738177 0.84912097 0.75365102 0.76039702 0.766258 0.672692
		 0.781142 0.593126 0.73834598 0.60109901 0.67294699 0.59462303 0.59660298 0.58244598
		 0.556575 0.62461501 0.58916098 0.70751297 0.60279101 0.77455401 0.075342998 0.184349
		 0.090744004 0.18463901 0.090744004 0.200574 0.071676001 0.200581 0.106144 0.184349
		 0.109813 0.200581 0.090744004 0.21652199 0.073319003 0.216793 0.108169 0.216793 0.076842003
		 0.22913601 0.090742998 0.229587 0.090742998 0.234936 0.072416 0.233527 0.104645 0.22913601
		 0.10907 0.233527 0.113908 0.13566101 0.092364997 0.13838699 0.092364997 0.13236099
		 0.110691 0.130952 0.070822001 0.135662 0.074037999 0.130953 0.109789 0.114217 0.092363998
		 0.113947 0.092363998 0.097997002 0.111432 0.098003998 0.074940003 0.114218 0.073294997
		 0.098007001 0.092362002 0.082062997 0.107763 0.081772 0.076962002 0.081773996 0.106131
		 0.064740002 0.092361003 0.063881002 0.092361003 0.057895001 0.108601 0.059884001
		 0.078590997 0.064740002 0.076121002 0.059882998 0.076972999 0.167316 0.090742998
		 0.16645899 0.090742998 0.17167699 0.080112003 0.17196999 0.104513 0.167318 0.101374
		 0.17197099 0.062797002 0.080953002 0.068439998 0.081573002 0.063279003 0.097972997
		 0.057071999 0.097857997 0.114664 0.184148 0.119828 0.20054699 0.065566003 0.114237
		 0.059588 0.114543 0.117542 0.216812 0.116284 0.081570998 0.121927 0.080949999 0.127653
		 0.097854003 0.121447 0.097970001 0.066822998 0.184148 0.061661001 0.20054699 0.12514099
		 0.11454 0.119163 0.114235 0.063946001 0.216812 0.070612997 0.230655 0.118169 0.12901799
		 0.112496 0.128078 0.106266 0.126561 0.092364997 0.127012 0.078463003 0.126561 0.072232999
		 0.12808 0.066560999 0.129021 0.110874 0.230654 0.092361003 0.069100998 0.102993 0.069394
		 0.108169 0.067585997 0.113376 0.066175997 0.074936002 0.17016301 0.106549 0.170164
		 0.071346 0.066178001 0.076553002 0.067587003 0.081730001 0.069394 0.112286 0.238236
		 0.090742998 0.240962 0.069200002 0.238236 0.057968002 0.217117 0.064939998 0.23159499
		 0.055454999 0.200432 0.061177999 0.18352699 0.069729 0.168752 0.074502997 0.162459
		 0.090742998 0.16047201 0.106983 0.162462 0.111756 0.168753 0.120308 0.18352699 0.12603401
		 0.200432 0.12352 0.217118 0.116548 0.23159499 0.013787 0.31238601 0.101741 0.29394999
		 0.099955 0.31586701 0.011934 0.320317 0.16144399 0.269586 0.159123 0.303561 0.235897
		 0.25707799 0.23526201 0.29610899 0.30909899 0.305491 0.308972 0.27275401 0.342042
		 0.28641301 0.34232801 0.31322601 0.098493002 0.339582 0.0089050001 0.325057 0.158097
		 0.34343201 0.23483101 0.34361899 0.30917501 0.34580201 0.34267199 0.346111 0.096042
		 0.359725 -0.00428 0.322359 0.159609 0.38319001 0.23575699 0.390504 0.31001899 0.38645199
		 0.343009 0.37984401 0.09821 0.381477 -0.0023409999 0.329523 0.16256499 0.417 0.23683199
		 0.427531 0.34329599 0.409255 0.31012699 0.42003301 0.65923399 0.30789301 0.66132802
		 0.31575501 0.58402699 0.31075299 0.58169699 0.28894001 0.525262 0.29973599 0.52217102
		 0.26578501 0.44896799 0.29391801 0.44746801 0.25489399 0.37536901 0.30482 0.374791
		 0.27209401 0.66514099 0.320301 0.58580202 0.33439201 0.52715099 0.33967301 0.45040801
		 0.341425 0.376136 0.34511599 0.67938298 0.31370601 0.58908498 0.355216 0.52645802
		 0.37955701 0.45041701 0.38833299 0.37610599 0.38576201 0.67745399 0.320898 0.58727199
		 0.37693101 0.524041 0.413463 0.45001701 0.425394 0.37664801 0.41932401 0.050638001
		 0.56606001 0.051215999 0.559425 0.130291 0.55143499 0.13227101 0.57097399 0.183423
		 0.55423301 0.187904 0.58495498 0.250072 0.55263001 0.25406501 0.58850998 0.313806
		 0.53744602 0.34437701 0.52940601 0.344605 0.55295599 0.31557 0.56773901 0.050000001
		 0.55384803 0.12757801 0.53103799 0.178983 0.52030098 0.246325 0.511774 0.31251201
		 0.50221902 0.344096 0.49971801 0.049422 0.55049801 0.12546501 0.51120001 0.17582101
		 0.48644099 0.24212 0.47176799 0.30992299 0.46681401 0.343797 0.46803999 0.053222001
		 0.538109 0.124576 0.490417 0.172924 0.45590401 0.23749299 0.43817401 0.34351301 0.43778601
		 0.30571499 0.43675199 0.62739801 0.559443 0.55614501 0.56685197 0.55782998 0.54728597
		 0.62660003 0.55276901 0.50178599 0.58194 0.505642 0.55115598 0.43577 0.58680201 0.439033
		 0.55083901 0.37506899 0.53687501 0.37389201 0.56720901;
	setAttr ".uvst[0].uvsp[500:553]" 0.56026298 0.52686501 0.62786001 0.54716903
		 0.50945801 0.51716799 0.44199601 0.50991201 0.37569299 0.50161803 0.56189299 0.50699002
		 0.62831599 0.54378802 0.51195103 0.483266 0.44545799 0.46982199 0.377621 0.46616399
		 0.56217498 0.48614201 0.62286502 0.53175902 0.51431501 0.45270401 0.44950899 0.43612999
		 0.38128099 0.436028 0.129522 0.57819003 0.044142 0.57026303 0.18814901 0.59575498
		 0.25799999 0.60070199 0.34468001 0.559331 0.322559 0.57757699 0.036910001 0.53531301
		 0.118153 0.484366 0.17054901 0.44549099 0.2362 0.42822701 0.306694 0.424523 0.34336299
		 0.419752 0.634978 0.56153101 0.55975699 0.573834 0.50195998 0.59278601 0.43209299
		 0.59909201 0.36710599 0.57720703 0.56918103 0.48069701 0.63942802 0.53329402 0.51673198
		 0.44226599 0.45064899 0.42612299 0.38011 0.42381901 0.65408897 0.33708 0.57848299
		 0.382083 0.104705 0.389429 0.015619 0.351623 0.164353 0.41986799 0.522183 0.416318
		 0.162994 0.26662701 0.233419 0.25408599 0.105177 0.28998199 0.020584 0.30729499 0.57753003
		 0.285081 0.65116102 0.30538499 0.52036101 0.26282701 0.449853 0.251825 0.37984201
		 0.26750201 0.34192699 0.27978599 0.30375901 0.26835299;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 438 ".vt";
	setAttr ".vt[0:165]"  0.37987599 40.81322861 8.41886139 -0.75403702 40.1700325 9.01417923
		 -1.014554977 39.67264938 8.99684143 -1.87809598 39.1367569 9.25017262 -3.38951492 39.24058151 9.49641418
		 0.38483101 40.80111313 8.42406845 -0.88552898 40.49791718 8.79194641 -1.16549504 40.24254227 9.012536049
		 -2.094608068 39.67879105 9.015416145 -3.51690507 39.55202866 9.36253262 0.18253 41.081977844 8.4220047
		 -0.98851103 40.65909195 8.55247402 -1.30845499 40.55261612 8.60920238 -2.31212711 40.21706772 8.78360367
		 -3.64429903 39.86348343 9.22864723 0.187491 41.069847107 8.42721939 -1.10323501 40.93066788 8.43818569
		 -1.49890494 40.95993042 8.43591595 -2.53437996 40.75401688 8.5532341 -3.77169204 40.17495728 9.094762802
		 0.19244701 41.057731628 8.43242836 -1.24726796 41.24955368 8.31651211 -1.72049701 41.39860153 8.25492954
		 -2.76045108 41.29404068 8.32286072 -3.89908409 40.48641586 8.96088409 -3.5658741 40.75072861 8.61988735
		 -3.40550494 40.33347321 8.79915905 -3.24004698 39.92051315 8.97664165 -3.06855607 39.50964737 9.15327644
		 -2.89016604 39.098686218 9.33002186 -0.768453 40.30593491 9.37735271 -1.018381953 39.90904617 9.55044937
		 -1.88160396 39.35357285 9.75790787 -0.88463402 40.6232605 9.22555447 -1.16410005 40.36841583 9.34540653
		 -2.098185062 39.89577484 9.52357006 -0.99233699 40.89549637 9.10608292 -1.31227803 40.78902054 9.16281128
		 -2.31571698 40.4341011 9.29192829 -1.10705996 41.16708374 8.99179459 -1.50273097 41.19633484 8.98952484
		 -2.537956 40.97097015 9.061390877 -1.25109303 41.48595047 8.87012196 -1.72432196 41.6350174 8.80854225
		 -2.76395702 41.51085281 8.83059502 -3.56856704 40.91716385 9.0096530914 -3.40820003 40.49991608 9.18895435
		 -3.24273896 40.086959839 9.36644173 -3.071245909 39.67610931 9.54306984 -2.89285898 39.26512146 9.71979141
		 0.67230803 40.90272141 8.33707237 -0.52404898 39.91747665 8.82234478 -0.75137901 40.33303452 8.55892563
		 0.67802602 40.88873672 8.34308147 -0.90442801 39.54197693 9.0049257278 -1.072523952 40.071891785 8.76839733
		 -1.90057504 38.92380524 9.29715919 -2.15033603 39.54908371 9.026355743 -3.2738471 39.35396957 9.18538475
		 -3.068063021 38.87988663 9.38927078 -3.64409304 39.043563843 9.5812149 -3.7910471 39.40284729 9.42677498
		 -0.87438399 40.67991257 8.49232006 0.68374801 40.87474442 8.34909439 -1.24345803 40.55708313 8.55776024
		 -2.40125608 40.17001343 8.7589426 -3.47167301 39.82791901 8.9816246 -3.93800306 39.76214218 9.27232933
		 -1.0067249537 40.99319839 8.36048031 0.68947202 40.86075211 8.35510921 -1.46315706 41.026943207 8.35786438
		 -2.65764093 40.78941727 8.49319935 -3.66253901 40.30429077 8.7768898 -4.084960938 40.12144089 9.11788559
		 -1.17287695 41.36104965 8.22012329 0.69518799 40.84677505 8.36111832 -1.71877599 41.53298187 8.149086
		 -2.91842604 41.41235733 8.22744846 -3.84753489 40.78561783 8.57008266 -4.23191309 40.48072433 8.96344662
		 -0.75455397 40.63859177 9.26876068 -0.620534 40.27253723 9.44386959 -1.076936007 40.34460068 9.40702248
		 -0.90884203 39.81469345 9.64354992 -2.15445995 39.79938126 9.61254311 -1.90462303 39.17391586 9.88286114
		 -3.27695298 39.54598236 9.63503838 -3.071170092 39.07188797 9.83889389 -0.87879801 40.95262146 9.1309433
		 -1.24787104 40.82979584 9.19638348 -2.40539908 40.42037582 9.34532642 -3.47477889 40.019927979 9.43128681
		 -1.011136055 41.26591492 8.99910355 -1.46756899 41.29966354 8.99648476 -2.66176605 41.03969574 9.079388618
		 -3.66564703 40.49630737 9.22654247 -1.17728901 41.63375092 8.85874557 -1.723189 41.80570221 8.78771114
		 -2.92247295 41.66246796 8.81315231 -3.85064006 40.9776268 9.019708633 -1.21419203 41.55986023 8.86443424
		 0.340188 41.098751068 8.39313412 -1.21007299 41.30530167 8.26831818 -1.71963704 41.46579742 8.2020092
		 -2.83943796 41.35319901 8.27515411 -3.70670295 40.76817703 8.59498596 -4.065495968 40.48357773 8.96216488
		 -3.70960212 40.94739914 9.014681816 -2.84321308 41.58666229 8.82187271 -1.72375405 41.72034836 8.79812717
		 -1.88933599 39.030281067 9.27366638 -0.95949298 39.60731506 9.00088405609 -0.654962 40.070953369 8.96459866
		 0.52609199 40.85797119 8.37796593 -0.694493 40.28924179 9.41061115 -0.96361202 39.86186981 9.59700012
		 -1.89311194 39.26374435 9.82038498 -2.98201489 39.16850281 9.7793417 -3.51680493 39.14207077 9.53881168
		 -2.97911501 38.9892807 9.35964584 0.102185 40.88238525 8.83041573 -1.22782004 39.84088898 9.82672119
		 -2.0084700584 39.09551239 10.076306343 -2.74158502 38.20308685 10.37512398 -3.89433789 36.69197845 11.010937691
		 0.195172 40.84403229 8.82669067 -0.89050001 39.66744232 9.88479233 -1.587111 38.82353973 10.16737175
		 -2.23433208 37.86857605 10.48714066 -3.08632493 37.23945999 10.82761574 0.27562499 40.77774048 8.84889221
		 -0.52178597 39.46805191 9.95156193 -1.094668984 38.52713776 10.26662159 -1.68580401 37.86243057 10.59172344
		 -2.2842021 36.96272659 10.88508797 0.45791599 40.6876297 8.89562798 0.17173401 39.074672699 10.083286285
		 -0.118681 38.060909271 10.42273998 -0.56715202 38.061611176 10.77373981 -1.45930302 36.21097946 11.3934164
		 0.37639499 40.74481583 8.85991478 -0.15531901 39.26495361 10.019565582 -0.57775003 38.27093506 10.35240746
		 -1.12629604 38.3109169 10.69026279 -1.89459097 37.057407379 11.10999393 0.102185 40.90699768 8.90391445
		 -1.22782004 39.86548615 9.90021992 -0.89050001 39.69206238 9.95829201 0.195172 40.86864471 8.90019131
		 -2.0084700584 39.120121 10.14980412 -1.587111 38.84815216 10.24087238 -2.74158502 38.22771072 10.4486227
		 -2.23433208 37.89318466 10.56063938 -3.89433789 36.71658325 11.084438324 -3.08632493 37.26408005 10.90111542
		 -0.52178597 39.49266434 10.025061607 0.27562499 40.80235291 8.92238998 -1.094668984 38.55174255 10.34012222
		 -1.68580401 37.88704681 10.66522217 -2.28405595 36.98361969 10.95935822 0.45791599 40.71223831 8.96912384
		 0.37639499 40.76942825 8.93341446 -0.15531901 39.28957748 10.093065262 0.17173401 39.099277496 10.15678596
		 -0.57775003 38.2955513 10.42590618 -0.118681 38.085502625 10.49623585;
	setAttr ".vt[166:331]" -1.12629604 38.33552933 10.76376247 -0.56715202 38.086227417 10.84723759
		 -1.89459097 37.082019806 11.18349361 -1.45930302 36.23558426 11.46691418 -1.22782004 39.85318375 9.86347008
		 0.102185 40.89468765 8.86476421 0.195172 40.57838821 8.030907631 0.27562499 40.51209259 8.053107262
		 0.37639499 40.75712967 8.89422131 0.45791599 40.69993591 8.92997646 0.17173401 39.086971283 10.12003613
		 -0.118681 38.073207855 10.45948982 -0.56715202 38.073917389 10.8104887 -1.45930302 36.22327423 11.43016624
		 -1.89459097 37.069713593 11.14674377 -2.28417802 36.9744339 10.92196083 -3.08632493 37.2517662 10.86436558
		 -3.89433789 36.70428467 11.047687531 -2.74158502 38.21540833 10.41187286 -2.0084700584 39.10781097 10.11305523
		 0.37948701 40.6876297 8.81493282 0.66566902 39.07466507 10.083286285 0.95608401 38.060901642 10.42273998
		 1.40455401 37.4060173 10.72939682 1.47824895 36.42575073 12.048472404 0.46100801 40.74481583 8.77715588
		 0.99272197 39.26495361 10.019565582 1.41515303 38.27093124 10.35240746 1.96369898 37.53583527 10.50078297
		 2.4591651 36.27186584 12.029338837 0.561777 40.77774048 8.76613426 1.35918999 39.46805191 9.95156193
		 1.93207502 38.52713013 10.26662159 2.523211 37.65594864 10.43860245 3.17901397 36.7560997 11.014185905
		 0.73521799 40.88238525 8.74972057 2.065222025 39.84088516 9.82672119 2.84587193 39.09551239 10.076306343
		 3.57898688 38.20308685 10.37512398 4.82307291 37.36124039 10.93028641 0.64222902 40.84403229 8.74393845
		 1.72790301 39.66744232 9.88479233 2.42451096 38.82353592 10.1673727 3.071733952 37.86856842 10.48714066
		 3.83239794 37.42697525 10.90827084 0.37948701 40.7122345 8.88843346 0.66566902 39.099273682 10.15678596
		 0.99272197 39.28957367 10.093065262 0.46100801 40.76942825 8.8506546 0.95608401 38.08549881 10.49623966
		 1.41515303 38.2955513 10.42590714 1.40455401 37.47341537 10.76766968 1.963696 37.60324478 10.53905582
		 1.48949206 36.49277878 12.085733414 2.47040606 36.33889771 12.0666008 1.35918999 39.49266052 10.025061607
		 0.561777 40.80234909 8.83963394 1.93207502 38.55174255 10.34012222 2.523211 37.69557953 10.50521946
		 3.20053792 36.72272491 11.12378216 0.73521799 40.90699768 8.82322121 0.64222902 40.8686409 8.81743813
		 1.72790301 39.69206238 9.95829201 2.065222025 39.86548233 9.90021992 2.42451096 38.84814453 10.24087238
		 2.84587193 39.12011719 10.14980412 3.071733952 37.89318085 10.56063938 3.57898688 38.22770691 10.4486227
		 3.83239794 37.45159149 10.98176861 4.82307291 37.385849 11.0037841797 0.66566902 39.086971283 10.12003613
		 0.37948701 40.69993591 8.8489809 0.461007 40.75712585 8.81116009 0.561777 40.79004669 8.80013561
		 0.64222902 40.8563385 8.77793789 0.73521799 40.89468765 8.78376961 2.065222025 39.85318375 9.86347008
		 2.84587193 39.10781097 10.11305523 3.57898688 38.21540833 10.41187286 4.82307291 37.37354279 10.96703625
		 3.83239794 37.43928909 10.94501972 3.1904161 36.74155045 11.067715645 2.46478605 36.30538177 12.047968864
		 1.48387003 36.45925903 12.067103386 1.40455401 37.43971634 10.7485342 0.95608401 38.073204041 10.45948982
		 0.23354501 40.5421524 9.053377151 0.38989201 40.56583023 9.12412357 0.54624099 40.5421524 9.053377151
		 0.070435002 40.97224808 8.90936089 0.38989201 40.99594116 8.98010826 0.70934999 40.97224808 8.90936089
		 0.167261 41.40234756 8.76534843 0.38989201 41.42603683 8.83609486 0.61252397 41.40234756 8.76534843
		 0.028890001 41.36259842 8.64667702 0.38989201 41.36259842 8.64667702 0.75089502 41.36259842 8.64667702
		 0.167261 41.32287598 8.52800465 0.38989201 41.29918289 8.45725822 0.61252397 41.32287598 8.52800465
		 0.070435002 40.89277649 8.67202091 0.38989201 40.8690834 8.60127068 0.70934999 40.89277649 8.67202091
		 0.23354501 40.46268082 8.81603432 0.38989201 40.43899155 8.74528694 0.54624099 40.46268082 8.81603432
		 0.13637 40.5024147 8.93470478 0.38989201 40.5024147 8.93470478 0.64341402 40.5024147 8.93470478
		 0.90789902 40.93251038 8.79069042 -0.128115 40.93251038 8.79069042 0.38989201 41.21098328 8.90810204
		 0.118847 41.18728638 8.83735371 -0.04961 41.14756393 8.7186842 0.118847 41.1078186 8.60001183
		 0.38989201 41.084129333 8.52926636 0.66093802 41.1078186 8.60001183 0.82939702 41.14756393 8.7186842
		 0.66093802 41.18728638 8.83735371 0.38989201 40.65404129 8.67327976 0.151989 40.67772675 8.74402714
		 0.0041279998 40.71746445 8.8626976 0.151989 40.75719833 8.98136997 0.38989201 40.7808876 9.052116394
		 0.62779498 40.75719833 8.98136997 0.775657 40.71746445 8.8626976 0.62779498 40.67772675 8.74402714
		 0.68170899 41.38248062 8.70601177 0.38989201 41.39432907 8.74138546 0.098076001 41.38248062 8.70601177
		 0.034618001 41.16742325 8.77801895 -0.028837999 40.95238113 8.85002708 0.078059003 40.73733521 8.92203522
		 0.184958 40.52227783 8.99404335 0.38989201 40.5341301 9.029415131 0.59482598 40.52227783 8.99404335
		 0.70172602 40.73733521 8.92203522 0.80862302 40.95238113 8.85002708 0.74516702 41.16742325 8.77801895
		 0.68170899 41.34274292 8.58734131 0.38989201 41.33089828 8.55196667 0.098076001 41.34274292 8.58734131
		 0.034618001 41.12768555 8.65934658 -0.028837999 40.91264725 8.73135376 0.078059003 40.69758987 8.8033638
		 0.184958 40.48254776 8.87537098 0.38989201 40.47070313 8.83999729 0.59482598 40.48254776 8.87537098
		 0.70172602 40.69758987 8.8033638 0.80862302 40.91264725 8.73135376 0.74516702 41.12768555 8.65934658
		 0.32971901 40.86143875 8.28640938 1.67333603 40.69441605 9.83593464 1.851156 40.36841583 10.17068291
		 2.60966206 39.86956406 10.69303608 4.1132431 39.56334305 10.7287941 0.32273701 40.85253143 8.29479694
		 1.84295905 40.94313431 9.62005806 2.074321032 40.71658325 9.84931183 2.91682196 40.27029037 10.31770515
		 4.29266024 39.79241943 10.51319218 0.315846 40.84418869 8.30292606 1.99622595 41.14593124 9.43199539
		 2.29312205 41.029453278 9.5553093 3.2243011 40.66681671 9.94487286;
	setAttr ".vt[332:437]" 4.47208405 40.021530151 10.29758167 0.15244401 40.76347351 8.37119484
		 2.15622497 41.34313583 9.24361229 2.55125308 41.32555771 9.27619267 3.53620601 41.061161041 9.57340336
		 4.65150785 40.2506218 10.081973076 0.536686 40.75632477 8.3803091 2.35309196 41.56238556 9.019085884
		 2.84541106 41.63793945 8.97590828 3.85238409 41.45634079 9.19968891 4.83092499 40.47972107 9.86637211
		 4.55166292 40.91874313 9.7037859 4.3214488 40.61111069 9.99417973 4.086959839 40.30712891 10.28056431
		 3.84688592 40.0051689148 10.56445408 3.59998608 39.70359039 10.84739113 1.70751798 40.26854706 9.41185379
		 1.88533795 39.94254684 9.74660206 2.6410141 39.4789772 10.30409813 1.87713802 40.51725769 9.19597721
		 2.1085031 40.29070282 9.42523384 2.94826007 39.87936401 9.92847633 2.030407906 40.72005081 9.0079174042
		 2.32730508 40.60358429 9.13122845 3.25575495 40.27576065 9.55552578 2.19040704 40.9172554 8.81952953
		 2.58543205 40.89968109 8.85211563 3.56764007 40.67023087 9.18417931 2.38727498 41.13650513 8.59500504
		 2.87959099 41.21207047 8.55182743 3.88373303 41.065750122 8.81075001 4.57572699 40.61890793 9.4052124
		 4.34551811 40.31125641 9.69558716 4.11102676 40.0072593689 9.98196602 3.87095308 39.70530319 10.2658596
		 3.624053 39.40375137 10.54881668 0.071235001 41.026981354 8.12831974 1.51563203 40.6972847 9.88470078
		 1.71130395 40.98419189 9.63567448 0.063193999 41.016777039 8.13796234 1.72076094 40.3212204 10.270854
		 1.97819495 40.72284317 9.90013409 2.59574604 39.74575043 10.87342167 2.95007396 40.20801544 10.44045258
		 4.022962093 39.90218353 10.72509384 3.73814893 39.55428696 11.05147934 4.33022213 39.3924942 10.91466808
		 4.53718996 39.6567688 10.66596127 1.88810802 41.21812439 9.41872978 0.055201001 41.0068855286 8.14745998
		 2.23059607 41.083766937 9.56097889 3.30476689 40.66543579 10.01036644 4.29990292 40.25051498 10.3976078
		 4.74416685 39.92105484 10.41723919 2.072679043 41.44561768 9.20141792 -0.58226401 40.8730545 8.29677677
		 2.52836704 41.42533493 9.23900509 3.66457391 41.12033081 9.58185387 4.57040215 40.60119247 10.067247391
		 4.95114517 40.18533707 10.16852093 2.29977703 41.69852829 8.94241333 -0.59014499 40.86381531 8.30596924
		 2.8677001 41.78569412 8.89260578 4.029303074 41.57619858 9.15075016 4.83596611 40.95605087 9.73225689
		 5.15811205 40.44960785 9.9198122 1.75337195 40.46006393 9.11375618 1.55770004 40.17315292 9.36278534
		 2.017627001 40.23156357 9.41093063 1.76019204 39.8299408 9.78164768 2.986341 39.75705719 9.99145317
		 2.63191199 39.29518127 10.424757 4.050725937 39.55627441 10.38064861 3.7659111 39.20841599 10.70705605
		 1.93017495 40.69399261 8.89681435 2.27002811 40.59248734 9.071775436 3.34105706 40.21432877 9.56122875
		 4.32766724 39.90459061 10.053156853 2.11210895 40.95433426 8.71221542 2.56779695 40.93405533 8.74980259
		 3.70083499 40.66936874 9.13285828 4.59816504 40.25527191 9.72280025 2.33920789 41.20724869 8.45320988
		 2.90712905 41.29441452 8.40340137 4.065466881 41.12562561 8.70208263 4.86372709 40.61016846 9.3878336
		 2.36324096 41.17187119 8.52410507 0.166879 40.90891266 8.26604366 2.32643509 41.63044739 8.98074913
		 2.85655499 41.7118187 8.9342556 3.94084406 41.51626587 9.17521858 4.69381094 40.93740082 9.71802139
		 4.99451923 40.46466064 9.89309311 4.71972609 40.61453629 9.39652061 3.97459888 41.095695496 8.75641727
		 2.89336109 41.25324631 8.47761536 2.602705 39.80765533 10.78322887 1.78595901 40.34482574 10.22076797
		 1.59448397 40.69585419 9.86031818 0.196566 40.94492722 8.2310009 1.63129103 40.23726654 9.40367603
		 1.82276499 39.88624954 9.76412678 2.63646197 39.38708115 10.36442661 3.6949811 39.30607986 10.62793636
		 4.22173405 39.47791672 10.82173061 3.66906691 39.62893677 10.94943523;
	setAttr -s 864 ".ed";
	setAttr ".ed[0:165]"  50 51 0 51 52 1 52 53 1 53 50 0 51 54 0 54 55 1 55 52 1
		 54 56 0 56 57 1 57 55 1 58 59 1 59 60 0 60 61 0 61 58 1 52 62 1 62 63 1 63 53 0 55 64 1
		 64 62 1 57 65 1 65 64 1 66 58 1 61 67 0 67 66 1 62 68 1 68 69 1 69 63 0 64 70 1 70 68 1
		 65 71 1 71 70 1 72 66 1 67 73 0 73 72 1 68 74 1 74 75 0 75 69 0 70 76 1 76 74 0 71 77 1
		 77 76 0 79 78 0 78 72 1 73 79 0 71 72 1 78 77 0 65 66 1 57 58 1 56 59 0 53 80 1 80 81 1
		 81 50 0 80 82 1 82 83 1 83 81 0 82 84 1 84 85 1 85 83 0 86 61 1 60 87 0 87 86 1 63 88 1
		 88 80 1 88 89 1 89 82 1 89 90 1 90 84 1 91 67 1 86 91 1 69 92 1 92 88 1 92 93 1 93 89 1
		 93 94 1 94 90 1 95 73 1 91 95 1 75 96 0 96 92 1 96 97 0 97 93 1 97 98 0 98 94 1 95 99 1
		 99 79 0 98 99 0 95 94 1 91 90 1 86 84 1 87 85 0 0 5 0 5 6 1 6 1 1 1 0 0 6 7 1 7 2 1
		 2 1 0 7 8 1 8 3 1 3 2 0 28 9 1 9 4 0 4 29 0 29 28 1 5 10 0 10 11 1 11 6 1 11 12 1
		 12 7 1 12 13 1 13 8 1 27 14 1 14 9 0 28 27 1 10 15 0 15 16 1 16 11 1 16 17 1 17 12 1
		 17 18 1 18 13 1 26 19 1 19 14 0 27 26 1 15 20 0 20 21 0 21 16 1 21 22 0 22 17 1 22 23 0
		 23 18 1 24 19 0 26 25 1 25 24 0 23 25 0 26 18 1 27 13 1 28 8 1 29 3 0 0 30 0 30 33 1
		 33 5 1 30 31 0 31 34 1 34 33 1 31 32 0 32 35 1 35 34 1 48 49 1 49 4 0 9 48 1 33 36 1
		 36 10 1 34 37 1 37 36 1 35 38 1 38 37 1 47 48 1 14 47 1 36 39 1 39 15 1 37 40 1 40 39 1
		 38 41 1 41 40 1 46 47 1;
	setAttr ".ed[166:331]" 19 46 1 39 42 1 42 20 0 40 43 1 43 42 0 41 44 1 44 43 0
		 24 45 0 45 46 1 41 46 1 45 44 0 38 47 1 35 48 1 32 49 0 1 112 0 112 113 1 113 0 0
		 2 111 0 111 112 1 3 110 0 110 111 1 4 118 0 118 119 1 119 29 0 20 101 0 101 102 1
		 102 21 0 102 103 1 103 22 0 103 104 1 104 23 0 25 105 0 105 106 1 106 24 0 104 105 1
		 119 110 1 113 114 1 114 30 0 114 115 1 115 31 0 115 116 1 116 32 0 49 117 0 117 118 1
		 42 100 0 100 101 1 43 109 0 109 100 1 44 108 0 108 109 1 106 107 1 107 45 0 107 108 1
		 116 117 1 100 96 0 75 101 0 74 102 0 76 103 0 77 104 0 78 105 0 79 106 0 99 107 0
		 98 108 0 97 109 0 110 56 0 54 111 0 51 112 0 50 113 0 81 114 0 83 115 0 85 116 0
		 87 117 0 60 118 0 59 119 0 145 146 0 146 147 1 147 148 1 148 145 0 146 149 0 149 150 1
		 150 147 1 149 151 0 151 152 1 152 150 1 151 153 0 153 154 0 154 152 1 147 155 1 155 156 0
		 156 148 0 150 157 1 157 155 0 152 158 1 158 157 0 154 159 0 159 158 0 160 161 0 161 162 1
		 162 163 1 163 160 0 162 164 1 164 165 1 165 163 0 164 166 1 166 167 1 167 165 0 166 168 1
		 168 169 0 169 167 0 161 156 0 155 162 1 157 164 1 158 166 1 159 168 0 120 125 0 125 126 1
		 126 121 1 121 120 0 126 127 1 127 122 1 122 121 0 127 128 1 128 123 1 123 122 0 128 129 1
		 129 124 0 124 123 0 125 130 0 130 131 0 131 126 1 131 132 0 132 127 1 132 133 0 133 128 1
		 133 134 0 134 129 0 135 136 0 136 141 1 141 140 1 140 135 0 136 137 0 137 142 1 142 141 1
		 137 138 0 138 143 1 143 142 1 138 139 0 139 144 0 144 143 1 141 131 1 130 140 0 142 132 1
		 143 133 1 144 134 0 121 170 1 170 171 1 171 120 0 171 172 1 172 125 1 122 185 1 185 170 1
		 123 184 1 184 185 1 124 183 0 183 184 1 129 182 1;
	setAttr ".ed[332:497]" 182 183 1 172 173 1 173 130 1 134 181 0 181 182 1 140 174 1
		 174 175 1 175 135 0 175 176 1 176 136 1 176 177 1 177 137 1 177 178 1 178 138 1 139 179 0
		 179 180 1 180 144 1 178 179 1 173 174 1 180 181 1 170 146 1 145 171 0 148 172 1 156 173 1
		 161 174 1 160 175 0 163 176 1 165 177 1 167 178 1 169 179 0 168 180 1 159 181 0 154 182 1
		 153 183 0 151 184 1 149 185 1 211 212 0 212 213 1 213 214 1 214 211 0 212 215 0 215 216 1
		 216 213 1 215 217 0 217 218 1 218 216 1 217 219 0 219 220 0 220 218 1 213 221 1 221 222 0
		 222 214 0 216 223 1 223 221 0 218 224 1 224 223 0 220 225 0 225 224 0 226 227 0 227 228 1
		 228 229 1 229 226 0 228 230 1 230 231 1 231 229 0 230 232 1 232 233 1 233 231 0 232 234 1
		 234 235 0 235 233 0 227 222 0 221 228 1 223 230 1 224 232 1 225 234 0 186 191 0 191 192 1
		 192 187 1 187 186 0 192 193 1 193 188 1 188 187 0 193 194 1 194 189 1 189 188 0 194 195 1
		 195 190 0 190 189 0 191 196 0 196 197 0 197 192 1 197 198 0 198 193 1 198 199 0 199 194 1
		 199 200 0 200 195 0 201 202 0 202 207 1 207 206 1 206 201 0 202 203 0 203 208 1 208 207 1
		 203 204 0 204 209 1 209 208 1 204 205 0 205 210 0 210 209 1 207 197 1 196 206 0 208 198 1
		 209 199 1 210 200 0 187 236 1 236 237 1 237 186 0 237 238 1 238 191 1 188 251 1 251 236 1
		 189 250 1 250 251 1 190 249 0 249 250 1 195 248 1 248 249 1 238 239 1 239 196 1 200 247 0
		 247 248 1 206 240 1 240 241 1 241 201 0 241 242 1 242 202 1 242 243 1 243 203 1 243 244 1
		 244 204 1 205 245 0 245 246 1 246 210 1 244 245 1 239 240 1 246 247 1 236 212 1 211 237 0
		 214 238 1 222 239 1 227 240 1 226 241 0 229 242 1 231 243 1 233 244 1 235 245 0 234 246 1
		 225 247 0 220 248 1 219 249 0 217 250 1 215 251 1 289 290 1 290 256 1;
	setAttr ".ed[498:663]" 256 255 1 255 289 0 290 291 1 291 257 0 257 256 1 256 278 1
		 278 279 1 279 255 0 257 285 0 285 278 1 258 259 0 259 295 1 295 296 1 296 258 0 259 260 0
		 260 294 0 294 295 1 261 262 1 262 307 1 307 308 1 308 261 0 262 263 1 263 306 0 306 307 1
		 281 282 1 282 268 1 268 267 1 267 281 0 282 283 1 283 269 0 269 268 1 268 286 1 286 287 1
		 287 267 0 269 293 0 293 286 1 312 313 1 313 274 1 274 273 1 273 312 0 313 314 1 314 275 0
		 275 274 1 300 301 1 301 253 1 253 252 0 252 300 0 301 302 1 302 254 0 254 253 0 292 315 1
		 315 316 1 316 276 1 276 292 1 291 303 1 303 304 1 304 257 1 316 317 1 317 284 1 284 276 1
		 304 305 1 305 285 1 311 288 1 288 277 1 277 310 1 310 311 1 299 289 1 255 298 1 298 299 1
		 277 280 1 280 309 1 309 310 1 279 297 1 297 298 1 278 259 1 258 279 0 296 297 1 280 261 1
		 308 309 1 264 265 0 265 282 1 281 264 0 265 266 0 266 283 0 317 306 1 263 284 1 305 294 1
		 260 285 0 286 271 1 271 270 0 270 287 0 273 288 1 311 312 1 252 289 0 299 300 1 253 290 1
		 254 291 0 302 303 1 314 315 1 292 275 1 293 272 0 272 271 0 294 263 0 262 295 1 261 296 0
		 280 297 1 277 298 1 288 299 1 273 300 0 274 301 1 275 302 0 292 303 1 276 304 1 284 305 1
		 306 266 0 265 307 1 264 308 0 281 309 1 267 310 1 287 311 1 270 312 0 271 313 1 272 314 0
		 293 315 1 269 316 1 283 317 1 368 369 0 369 370 1 370 371 1 371 368 0 369 372 0 372 373 1
		 373 370 1 372 374 0 374 375 1 375 373 1 376 377 1 377 378 0 378 379 0 379 376 1 370 380 1
		 380 381 1 381 371 0 373 382 1 382 380 1 375 383 1 383 382 1 384 376 1 379 385 0 385 384 1
		 380 386 1 386 387 1 387 381 0 382 388 1 388 386 1 383 389 1 389 388 1 390 384 1 385 391 0
		 391 390 1 386 392 1 392 393 0 393 387 0 388 394 1 394 392 0 389 395 1;
	setAttr ".ed[664:829]" 395 394 0 397 396 0 396 390 1 391 397 0 389 390 1 396 395 0
		 383 384 1 375 376 1 374 377 0 371 398 1 398 399 1 399 368 0 398 400 1 400 401 1 401 399 0
		 400 402 1 402 403 1 403 401 0 404 379 1 378 405 0 405 404 1 381 406 1 406 398 1 406 407 1
		 407 400 1 407 408 1 408 402 1 409 385 1 404 409 1 387 410 1 410 406 1 410 411 1 411 407 1
		 411 412 1 412 408 1 413 391 1 409 413 1 393 414 0 414 410 1 414 415 0 415 411 1 415 416 0
		 416 412 1 413 417 1 417 397 0 416 417 0 413 412 1 409 408 1 404 402 1 405 403 0 318 323 0
		 323 324 1 324 319 1 319 318 0 324 325 1 325 320 1 320 319 0 325 326 1 326 321 1 321 320 0
		 346 327 1 327 322 0 322 347 0 347 346 1 323 328 0 328 329 1 329 324 1 329 330 1 330 325 1
		 330 331 1 331 326 1 345 332 1 332 327 0 346 345 1 328 333 0 333 334 1 334 329 1 334 335 1
		 335 330 1 335 336 1 336 331 1 344 337 1 337 332 0 345 344 1 333 338 0 338 339 0 339 334 1
		 339 340 0 340 335 1 340 341 0 341 336 1 342 337 0 344 343 1 343 342 0 341 343 0 344 336 1
		 345 331 1 346 326 1 347 321 0 318 348 0 348 351 1 351 323 1 348 349 0 349 352 1 352 351 1
		 349 350 0 350 353 1 353 352 1 366 367 1 367 322 0 327 366 1 351 354 1 354 328 1 352 355 1
		 355 354 1 353 356 1 356 355 1 365 366 1 332 365 1 354 357 1 357 333 1 355 358 1 358 357 1
		 356 359 1 359 358 1 364 365 1 337 364 1 357 360 1 360 338 0 358 361 1 361 360 0 359 362 1
		 362 361 0 342 363 0 363 364 1 359 364 1 363 362 0 356 365 1 353 366 1 350 367 0 319 430 0
		 430 431 1 431 318 0 320 429 0 429 430 1 321 428 0 428 429 1 322 436 0 436 437 1 437 347 0
		 338 419 0 419 420 1 420 339 0 420 421 1 421 340 0 421 422 1 422 341 0 343 423 0 423 424 1
		 424 342 0 422 423 1 437 428 1 431 432 1 432 348 0 432 433 1 433 349 0;
	setAttr ".ed[830:863]" 433 434 1 434 350 0 367 435 0 435 436 1 360 418 0 418 419 1
		 361 427 0 427 418 1 362 426 0 426 427 1 424 425 1 425 363 0 425 426 1 434 435 1 418 414 0
		 393 419 0 392 420 0 394 421 0 395 422 0 396 423 0 397 424 0 417 425 0 416 426 0 415 427 0
		 428 374 0 372 429 0 369 430 0 368 431 0 399 432 0 401 433 0 403 434 0 405 435 0 378 436 0
		 377 437 0;
	setAttr -s 432 -ch 1728 ".fc[0:431]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 13
		mu 0 4 8 9 10 11
		f 4 -3 14 15 16
		mu 0 4 3 2 12 13
		f 4 -7 17 18 -15
		mu 0 4 2 5 14 12
		f 4 -10 19 20 -18
		mu 0 4 5 7 15 14
		f 4 21 -14 22 23
		mu 0 4 16 8 11 17
		f 4 -16 24 25 26
		mu 0 4 13 12 18 19
		f 4 -19 27 28 -25
		mu 0 4 12 14 20 18
		f 4 -21 29 30 -28
		mu 0 4 14 15 21 20
		f 4 31 -24 32 33
		mu 0 4 22 16 17 23
		f 4 -26 34 35 36
		mu 0 4 19 18 24 25
		f 4 -29 37 38 -35
		mu 0 4 18 20 26 24
		f 4 -31 39 40 -38
		mu 0 4 20 21 27 26
		f 4 41 42 -34 43
		mu 0 4 28 29 22 23
		f 4 44 -43 45 -40
		mu 0 4 21 22 29 27
		f 4 46 -32 -45 -30
		mu 0 4 15 16 22 21
		f 4 47 -22 -47 -20
		mu 0 4 7 8 16 15
		f 4 48 -11 -48 -9
		mu 0 4 6 9 8 7
		f 4 -4 49 50 51
		mu 0 4 0 3 30 31
		f 4 -51 52 53 54
		mu 0 4 31 30 32 33
		f 4 -54 55 56 57
		mu 0 4 33 32 34 35
		f 4 58 -13 59 60
		mu 0 4 36 37 38 39
		f 4 -17 61 62 -50
		mu 0 4 3 13 40 30
		f 4 -63 63 64 -53
		mu 0 4 30 40 41 32
		f 4 -65 65 66 -56
		mu 0 4 32 41 42 34
		f 4 67 -23 -59 68
		mu 0 4 43 44 37 36
		f 4 -27 69 70 -62
		mu 0 4 13 19 45 40
		f 4 -71 71 72 -64
		mu 0 4 40 45 46 41
		f 4 -73 73 74 -66
		mu 0 4 41 46 47 42
		f 4 75 -33 -68 76
		mu 0 4 48 49 44 43
		f 4 -37 77 78 -70
		mu 0 4 19 25 50 45
		f 4 -79 79 80 -72
		mu 0 4 45 50 51 46
		f 4 -81 81 82 -74
		mu 0 4 46 51 52 47
		f 4 -44 -76 83 84
		mu 0 4 53 49 48 54
		f 4 -83 85 -84 86
		mu 0 4 47 52 54 48
		f 4 -75 -87 -77 87
		mu 0 4 42 47 48 43
		f 4 -67 -88 -69 88
		mu 0 4 34 42 43 36
		f 4 -57 -89 -61 89
		mu 0 4 35 34 36 39
		f 4 90 91 92 93
		mu 0 4 55 56 57 58
		f 4 -93 94 95 96
		mu 0 4 58 57 59 60
		f 4 -96 97 98 99
		mu 0 4 60 59 61 62
		f 4 100 101 102 103
		mu 0 4 63 64 65 66
		f 4 104 105 106 -92
		mu 0 4 56 67 68 57
		f 4 -107 107 108 -95
		mu 0 4 57 68 69 59
		f 4 -109 109 110 -98
		mu 0 4 59 69 70 61
		f 4 111 112 -101 113
		mu 0 4 71 72 64 63
		f 4 114 115 116 -106
		mu 0 4 67 73 74 68
		f 4 -117 117 118 -108
		mu 0 4 68 74 75 69
		f 4 -119 119 120 -110
		mu 0 4 69 75 76 70
		f 4 121 122 -112 123
		mu 0 4 77 78 72 71
		f 4 124 125 126 -116
		mu 0 4 73 79 80 74
		f 4 -127 127 128 -118
		mu 0 4 74 80 81 75
		f 4 -129 129 130 -120
		mu 0 4 75 81 82 76
		f 4 131 -122 132 133
		mu 0 4 83 78 77 84
		f 4 -131 134 -133 135
		mu 0 4 76 82 84 77
		f 4 -121 -136 -124 136
		mu 0 4 70 76 77 71
		f 4 -111 -137 -114 137
		mu 0 4 61 70 71 63
		f 4 -99 -138 -104 138
		mu 0 4 62 61 63 66
		f 4 139 140 141 -91
		mu 0 4 55 85 86 56
		f 4 142 143 144 -141
		mu 0 4 85 87 88 86
		f 4 145 146 147 -144
		mu 0 4 87 89 90 88
		f 4 148 149 -102 150
		mu 0 4 91 92 93 94
		f 4 -142 151 152 -105
		mu 0 4 56 86 95 67
		f 4 -145 153 154 -152
		mu 0 4 86 88 96 95
		f 4 -148 155 156 -154
		mu 0 4 88 90 97 96
		f 4 157 -151 -113 158
		mu 0 4 98 91 94 99
		f 4 -153 159 160 -115
		mu 0 4 67 95 100 73
		f 4 -155 161 162 -160
		mu 0 4 95 96 101 100
		f 4 -157 163 164 -162
		mu 0 4 96 97 102 101
		f 4 165 -159 -123 166
		mu 0 4 103 98 99 104
		f 4 -161 167 168 -125
		mu 0 4 73 100 105 79
		f 4 -163 169 170 -168
		mu 0 4 100 101 106 105
		f 4 -165 171 172 -170
		mu 0 4 101 102 107 106
		f 4 173 174 -167 -132
		mu 0 4 108 109 103 104
		f 4 175 -175 176 -172
		mu 0 4 102 103 109 107
		f 4 177 -166 -176 -164
		mu 0 4 97 98 103 102
		f 4 178 -158 -178 -156
		mu 0 4 90 91 98 97
		f 4 179 -149 -179 -147
		mu 0 4 89 92 91 90
		f 4 -94 180 181 182
		mu 0 4 55 58 110 111
		f 4 -97 183 184 -181
		mu 0 4 58 60 112 110
		f 4 -100 185 186 -184
		mu 0 4 60 62 113 112
		f 4 -103 187 188 189
		mu 0 4 66 65 114 115
		f 4 -126 190 191 192
		mu 0 4 80 79 116 117
		f 4 -128 -193 193 194
		mu 0 4 81 80 117 118
		f 4 -130 -195 195 196
		mu 0 4 82 81 118 119
		f 4 -134 197 198 199
		mu 0 4 83 84 120 121
		f 4 -135 -197 200 -198
		mu 0 4 84 82 119 120
		f 4 -139 -190 201 -186
		mu 0 4 62 66 115 113
		f 4 -140 -183 202 203
		mu 0 4 85 55 111 122
		f 4 -143 -204 204 205
		mu 0 4 87 85 122 123
		f 4 -146 -206 206 207
		mu 0 4 89 87 123 124
		f 4 -150 208 209 -188
		mu 0 4 93 92 125 126
		f 4 -169 210 211 -191
		mu 0 4 79 105 127 116
		f 4 -171 212 213 -211
		mu 0 4 105 106 128 127
		f 4 -173 214 215 -213
		mu 0 4 106 107 129 128
		f 4 -174 -200 216 217
		mu 0 4 109 108 130 131
		f 4 -177 -218 218 -215
		mu 0 4 107 109 131 129
		f 4 -180 -208 219 -209
		mu 0 4 92 89 124 125
		f 4 -212 220 -78 221
		mu 0 4 116 127 50 25
		f 4 -192 -222 -36 222
		mu 0 4 117 116 25 24
		f 4 -194 -223 -39 223
		mu 0 4 118 117 24 26
		f 4 -196 -224 -41 224
		mu 0 4 119 118 26 27
		f 4 -201 -225 -46 225
		mu 0 4 132 119 27 29
		f 4 -199 -226 -42 226
		mu 0 4 133 132 29 28
		f 4 -217 -227 -85 227
		mu 0 4 134 135 53 54
		f 4 -219 -228 -86 228
		mu 0 4 129 134 54 52
		f 4 -216 -229 -82 229
		mu 0 4 128 129 52 51
		f 4 -214 -230 -80 -221
		mu 0 4 127 128 51 50
		f 4 -187 230 -8 231
		mu 0 4 136 137 6 4
		f 4 -185 -232 -5 232
		mu 0 4 138 136 4 1
		f 4 -182 -233 -1 233
		mu 0 4 139 138 1 0
		f 4 -203 -234 -52 234
		mu 0 4 140 139 0 31
		f 4 -205 -235 -55 235
		mu 0 4 141 140 31 33
		f 4 -207 -236 -58 236
		mu 0 4 142 141 33 35
		f 4 -220 -237 -90 237
		mu 0 4 143 142 35 39
		f 4 -210 -238 -60 238
		mu 0 4 144 143 39 38
		f 4 -189 -239 -12 239
		mu 0 4 145 146 10 9
		f 4 -202 -240 -49 -231
		mu 0 4 137 145 9 6
		f 4 240 241 242 243
		mu 0 4 147 148 149 150
		f 4 244 245 246 -242
		mu 0 4 148 151 152 149
		f 4 247 248 249 -246
		mu 0 4 151 153 154 152
		f 4 250 251 252 -249
		mu 0 4 153 155 156 154
		f 4 -243 253 254 255
		mu 0 4 150 149 157 158
		f 4 -247 256 257 -254
		mu 0 4 149 152 159 157
		f 4 -250 258 259 -257
		mu 0 4 152 154 160 159
		f 4 -253 260 261 -259
		mu 0 4 154 156 161 160
		f 4 262 263 264 265
		mu 0 4 162 163 164 165
		f 4 -265 266 267 268
		mu 0 4 165 164 166 167
		f 4 -268 269 270 271
		mu 0 4 167 166 168 169
		f 4 -271 272 273 274
		mu 0 4 169 168 170 171
		f 4 275 -255 276 -264
		mu 0 4 163 158 157 164
		f 4 -277 -258 277 -267
		mu 0 4 164 157 159 166
		f 4 -278 -260 278 -270
		mu 0 4 166 159 160 168
		f 4 -279 -262 279 -273
		mu 0 4 168 160 161 170
		f 4 280 281 282 283
		mu 0 4 172 173 174 175
		f 4 -283 284 285 286
		mu 0 4 175 174 176 177
		f 4 -286 287 288 289
		mu 0 4 177 176 178 179
		f 4 -289 290 291 292
		mu 0 4 179 178 180 181
		f 4 293 294 295 -282
		mu 0 4 173 182 183 174
		f 4 -296 296 297 -285
		mu 0 4 174 183 184 176
		f 4 -298 298 299 -288
		mu 0 4 176 184 185 178
		f 4 -300 300 301 -291
		mu 0 4 178 185 186 180
		f 4 302 303 304 305
		mu 0 4 187 188 189 190
		f 4 306 307 308 -304
		mu 0 4 188 191 192 189
		f 4 309 310 311 -308
		mu 0 4 191 193 194 192
		f 4 312 313 314 -311
		mu 0 4 193 195 196 194
		f 4 -305 315 -295 316
		mu 0 4 190 189 183 182
		f 4 -309 317 -297 -316
		mu 0 4 189 192 184 183
		f 4 -312 318 -299 -318
		mu 0 4 192 194 185 184
		f 4 -315 319 -301 -319
		mu 0 4 194 196 186 185
		f 4 -284 320 321 322
		mu 0 4 172 175 197 198
		f 4 -281 -323 323 324
		mu 0 4 173 172 198 199
		f 4 -287 325 326 -321
		mu 0 4 175 177 200 197
		f 4 -290 327 328 -326
		mu 0 4 177 179 201 200
		f 4 -293 329 330 -328
		mu 0 4 179 181 202 201
		f 4 -292 331 332 -330
		mu 0 4 181 180 203 202
		f 4 -294 -325 333 334
		mu 0 4 182 173 199 204
		f 4 -302 335 336 -332
		mu 0 4 180 186 205 203
		f 4 -306 337 338 339
		mu 0 4 187 190 206 207
		f 4 -303 -340 340 341
		mu 0 4 188 187 207 208
		f 4 -307 -342 342 343
		mu 0 4 191 188 208 209
		f 4 -310 -344 344 345
		mu 0 4 193 191 209 210
		f 4 -314 346 347 348
		mu 0 4 196 195 211 212
		f 4 -313 -346 349 -347
		mu 0 4 195 193 210 211
		f 4 -317 -335 350 -338
		mu 0 4 190 182 204 206
		f 4 -320 -349 351 -336
		mu 0 4 186 196 212 205
		f 4 -322 352 -241 353
		mu 0 4 213 214 148 147
		f 4 -324 -354 -244 354
		mu 0 4 215 213 147 150
		f 4 -334 -355 -256 355
		mu 0 4 216 215 150 158
		f 4 -351 -356 -276 356
		mu 0 4 217 216 158 163
		f 4 -339 -357 -263 357
		mu 0 4 218 217 163 162
		f 4 -341 -358 -266 358
		mu 0 4 219 218 162 165
		f 4 -343 -359 -269 359
		mu 0 4 220 219 165 167
		f 4 -345 -360 -272 360
		mu 0 4 221 220 167 169
		f 4 -350 -361 -275 361
		mu 0 4 222 221 169 171
		f 4 -348 -362 -274 362
		mu 0 4 223 222 171 170
		f 4 -352 -363 -280 363
		mu 0 4 224 223 170 161
		f 4 -337 -364 -261 364
		mu 0 4 225 224 161 156
		f 4 -333 -365 -252 365
		mu 0 4 226 225 156 155
		f 4 -331 -366 -251 366
		mu 0 4 227 226 155 153
		f 4 -329 -367 -248 367
		mu 0 4 228 227 153 151
		f 4 -327 -368 -245 -353
		mu 0 4 214 228 151 148
		f 4 368 369 370 371
		mu 0 4 229 230 231 232
		f 4 372 373 374 -370
		mu 0 4 230 233 234 231
		f 4 375 376 377 -374
		mu 0 4 233 235 236 234
		f 4 378 379 380 -377
		mu 0 4 235 237 238 236
		f 4 -371 381 382 383
		mu 0 4 232 231 239 240
		f 4 -375 384 385 -382
		mu 0 4 231 234 241 239
		f 4 -378 386 387 -385
		mu 0 4 234 236 242 241
		f 4 -381 388 389 -387
		mu 0 4 236 238 243 242
		f 4 390 391 392 393
		mu 0 4 244 245 246 247
		f 4 -393 394 395 396
		mu 0 4 247 246 248 249
		f 4 -396 397 398 399
		mu 0 4 249 248 250 251
		f 4 -399 400 401 402
		mu 0 4 251 250 252 253
		f 4 403 -383 404 -392
		mu 0 4 245 240 239 246
		f 4 -405 -386 405 -395
		mu 0 4 246 239 241 248
		f 4 -406 -388 406 -398
		mu 0 4 248 241 242 250
		f 4 -407 -390 407 -401
		mu 0 4 250 242 243 252
		f 4 408 409 410 411
		mu 0 4 254 255 256 257
		f 4 -411 412 413 414
		mu 0 4 257 256 258 259
		f 4 -414 415 416 417
		mu 0 4 259 258 260 261
		f 4 -417 418 419 420
		mu 0 4 261 260 262 263
		f 4 421 422 423 -410
		mu 0 4 255 264 265 256
		f 4 -424 424 425 -413
		mu 0 4 256 265 266 258
		f 4 -426 426 427 -416
		mu 0 4 258 266 267 260
		f 4 -428 428 429 -419
		mu 0 4 260 267 268 262
		f 4 430 431 432 433
		mu 0 4 269 270 271 272
		f 4 434 435 436 -432
		mu 0 4 270 273 274 271
		f 4 437 438 439 -436
		mu 0 4 273 275 276 274
		f 4 440 441 442 -439
		mu 0 4 275 277 278 276
		f 4 -433 443 -423 444
		mu 0 4 272 271 265 264
		f 4 -437 445 -425 -444
		mu 0 4 271 274 266 265
		f 4 -440 446 -427 -446
		mu 0 4 274 276 267 266
		f 4 -443 447 -429 -447
		mu 0 4 276 278 268 267
		f 4 -412 448 449 450
		mu 0 4 254 257 279 280
		f 4 -409 -451 451 452
		mu 0 4 255 254 280 281
		f 4 -415 453 454 -449
		mu 0 4 257 259 282 279
		f 4 -418 455 456 -454
		mu 0 4 259 261 283 282
		f 4 -421 457 458 -456
		mu 0 4 261 263 284 283
		f 4 -420 459 460 -458
		mu 0 4 263 262 285 284
		f 4 -422 -453 461 462
		mu 0 4 264 255 281 286
		f 4 -430 463 464 -460
		mu 0 4 262 268 287 285
		f 4 -434 465 466 467
		mu 0 4 269 272 288 289
		f 4 -431 -468 468 469
		mu 0 4 270 269 289 290
		f 4 -435 -470 470 471
		mu 0 4 273 270 290 291
		f 4 -438 -472 472 473
		mu 0 4 275 273 291 292
		f 4 -442 474 475 476
		mu 0 4 278 277 293 294
		f 4 -441 -474 477 -475
		mu 0 4 277 275 292 293
		f 4 -445 -463 478 -466
		mu 0 4 272 264 286 288
		f 4 -448 -477 479 -464
		mu 0 4 268 278 294 287
		f 4 -450 480 -369 481
		mu 0 4 295 296 230 229
		f 4 -452 -482 -372 482
		mu 0 4 297 295 229 232
		f 4 -462 -483 -384 483
		mu 0 4 298 297 232 240
		f 4 -479 -484 -404 484
		mu 0 4 299 298 240 245
		f 4 -467 -485 -391 485
		mu 0 4 300 299 245 244
		f 4 -469 -486 -394 486
		mu 0 4 301 300 244 247
		f 4 -471 -487 -397 487
		mu 0 4 302 301 247 249
		f 4 -473 -488 -400 488
		mu 0 4 303 302 249 251
		f 4 -478 -489 -403 489
		mu 0 4 304 303 251 253
		f 4 -476 -490 -402 490
		mu 0 4 305 304 253 252
		f 4 -480 -491 -408 491
		mu 0 4 306 305 252 243
		f 4 -465 -492 -389 492
		mu 0 4 307 306 243 238
		f 4 -461 -493 -380 493
		mu 0 4 308 307 238 237
		f 4 -459 -494 -379 494
		mu 0 4 309 308 237 235
		f 4 -457 -495 -376 495
		mu 0 4 310 309 235 233
		f 4 -455 -496 -373 -481
		mu 0 4 296 310 233 230
		f 4 496 497 498 499
		mu 0 4 311 312 313 314
		f 4 500 501 502 -498
		mu 0 4 312 315 316 313
		f 4 -499 503 504 505
		mu 0 4 314 313 317 318
		f 4 -503 506 507 -504
		mu 0 4 313 316 319 317
		f 4 508 509 510 511
		mu 0 4 320 321 322 323
		f 4 512 513 514 -510
		mu 0 4 321 324 325 322
		f 4 515 516 517 518
		mu 0 4 326 327 328 329
		f 4 519 520 521 -517
		mu 0 4 327 330 331 328
		f 4 522 523 524 525
		mu 0 4 332 333 334 335
		f 4 526 527 528 -524
		mu 0 4 333 336 337 334
		f 4 -525 529 530 531
		mu 0 4 335 334 338 339
		f 4 -529 532 533 -530
		mu 0 4 334 337 340 338
		f 4 534 535 536 537
		mu 0 4 341 342 343 344
		f 4 538 539 540 -536
		mu 0 4 342 345 346 343
		f 4 541 542 543 544
		mu 0 4 347 348 349 350
		f 4 545 546 547 -543
		mu 0 4 348 351 352 349
		f 4 548 549 550 551
		mu 0 4 353 354 355 356
		f 4 552 553 554 -502
		mu 0 4 315 357 358 316
		f 4 -551 555 556 557
		mu 0 4 356 355 359 360
		f 4 -555 558 559 -507
		mu 0 4 316 358 361 319
		f 4 560 561 562 563
		mu 0 4 362 363 364 365
		f 4 564 -500 565 566
		mu 0 4 366 311 314 367
		f 4 -563 567 568 569
		mu 0 4 365 364 368 369
		f 4 -566 -506 570 571
		mu 0 4 367 314 318 370
		f 4 -505 572 -509 573
		mu 0 4 318 317 321 320
		f 4 -571 -574 -512 574
		mu 0 4 370 318 320 371
		f 4 -569 575 -519 576
		mu 0 4 369 368 372 373
		f 4 577 578 -523 579
		mu 0 4 374 375 333 332
		f 4 580 581 -527 -579
		mu 0 4 375 376 336 333
		f 4 -557 582 -521 583
		mu 0 4 360 359 377 378
		f 4 -560 584 -514 585
		mu 0 4 319 361 379 324
		f 4 -508 -586 -513 -573
		mu 0 4 317 319 324 321
		f 4 -531 586 587 588
		mu 0 4 339 338 380 381
		f 4 -538 589 -561 590
		mu 0 4 382 383 363 362
		f 4 -545 591 -565 592
		mu 0 4 384 350 311 366
		f 4 -544 593 -497 -592
		mu 0 4 350 349 312 311
		f 4 -548 594 -501 -594
		mu 0 4 349 352 315 312
		f 4 -547 595 -553 -595
		mu 0 4 352 385 357 315
		f 4 -540 596 -549 597
		mu 0 4 386 387 354 353
		f 4 -534 598 599 -587
		mu 0 4 338 340 388 380
		f 4 -515 600 -520 601
		mu 0 4 322 325 389 390
		f 4 -511 -602 -516 602
		mu 0 4 323 322 390 391
		f 4 603 -575 -603 -576
		mu 0 4 392 370 371 393
		f 4 604 -572 -604 -568
		mu 0 4 394 367 370 392
		f 4 605 -567 -605 -562
		mu 0 4 395 366 367 394
		f 4 606 -593 -606 -590
		mu 0 4 396 384 366 395
		f 4 -537 607 -542 -607
		mu 0 4 397 398 348 347
		f 4 -541 608 -546 -608
		mu 0 4 398 399 351 348
		f 4 -596 -609 -598 609
		mu 0 4 357 385 400 401
		f 4 -554 -610 -552 610
		mu 0 4 358 357 401 402
		f 4 -559 -611 -558 611
		mu 0 4 361 358 402 403
		f 4 -585 -612 -584 -601
		mu 0 4 379 361 403 404
		f 4 -522 612 -581 613
		mu 0 4 328 331 376 375
		f 4 -518 -614 -578 614
		mu 0 4 329 328 375 374
		f 4 615 -577 -615 -580
		mu 0 4 332 369 373 374
		f 4 616 -570 -616 -526
		mu 0 4 335 365 369 332
		f 4 617 -564 -617 -532
		mu 0 4 339 362 365 335
		f 4 618 -591 -618 -589
		mu 0 4 381 382 362 339
		f 4 -588 619 -535 -619
		mu 0 4 381 380 342 341
		f 4 -600 620 -539 -620
		mu 0 4 380 388 345 342
		f 4 -597 -621 -599 621
		mu 0 4 354 387 388 340
		f 4 -550 -622 -533 622
		mu 0 4 355 354 340 337
		f 4 -556 -623 -528 623
		mu 0 4 359 355 337 336
		f 4 -583 -624 -582 -613
		mu 0 4 377 359 336 376
		f 4 624 625 626 627
		mu 0 4 405 406 407 408
		f 4 628 629 630 -626
		mu 0 4 406 409 410 407
		f 4 631 632 633 -630
		mu 0 4 409 411 412 410
		f 4 634 635 636 637
		mu 0 4 413 414 415 416
		f 4 -627 638 639 640
		mu 0 4 408 407 417 418
		f 4 -631 641 642 -639
		mu 0 4 407 410 419 417
		f 4 -634 643 644 -642
		mu 0 4 410 412 420 419
		f 4 645 -638 646 647
		mu 0 4 421 413 416 422
		f 4 -640 648 649 650
		mu 0 4 418 417 423 424
		f 4 -643 651 652 -649
		mu 0 4 417 419 425 423
		f 4 -645 653 654 -652
		mu 0 4 419 420 426 425
		f 4 655 -648 656 657
		mu 0 4 427 421 422 428
		f 4 -650 658 659 660
		mu 0 4 424 423 429 430
		f 4 -653 661 662 -659
		mu 0 4 423 425 431 429
		f 4 -655 663 664 -662
		mu 0 4 425 426 432 431
		f 4 665 666 -658 667
		mu 0 4 433 434 427 428
		f 4 668 -667 669 -664
		mu 0 4 426 427 434 432
		f 4 670 -656 -669 -654
		mu 0 4 420 421 427 426
		f 4 671 -646 -671 -644
		mu 0 4 412 413 421 420
		f 4 672 -635 -672 -633
		mu 0 4 411 414 413 412
		f 4 -628 673 674 675
		mu 0 4 435 436 437 438
		f 4 -675 676 677 678
		mu 0 4 438 437 439 440
		f 4 -678 679 680 681
		mu 0 4 440 439 441 442
		f 4 682 -637 683 684
		mu 0 4 443 416 415 444
		f 4 -641 685 686 -674
		mu 0 4 436 445 446 437
		f 4 -687 687 688 -677
		mu 0 4 437 446 447 439
		f 4 -689 689 690 -680
		mu 0 4 439 447 448 441
		f 4 691 -647 -683 692
		mu 0 4 449 422 416 443
		f 4 -651 693 694 -686
		mu 0 4 445 450 451 446
		f 4 -695 695 696 -688
		mu 0 4 446 451 452 447
		f 4 -697 697 698 -690
		mu 0 4 447 452 453 448
		f 4 699 -657 -692 700
		mu 0 4 454 428 422 449
		f 4 -661 701 702 -694
		mu 0 4 450 455 456 451
		f 4 -703 703 704 -696
		mu 0 4 451 456 457 452
		f 4 -705 705 706 -698
		mu 0 4 452 457 458 453
		f 4 -668 -700 707 708
		mu 0 4 433 428 454 459
		f 4 -707 709 -708 710
		mu 0 4 453 458 459 454
		f 4 -699 -711 -701 711
		mu 0 4 448 453 454 449
		f 4 -691 -712 -693 712
		mu 0 4 441 448 449 443
		f 4 -681 -713 -685 713
		mu 0 4 442 441 443 444
		f 4 714 715 716 717
		mu 0 4 460 461 462 463
		f 4 -717 718 719 720
		mu 0 4 463 462 464 465
		f 4 -720 721 722 723
		mu 0 4 465 464 466 467
		f 4 724 725 726 727
		mu 0 4 468 469 470 471
		f 4 728 729 730 -716
		mu 0 4 461 472 473 462
		f 4 -731 731 732 -719
		mu 0 4 462 473 474 464
		f 4 -733 733 734 -722
		mu 0 4 464 474 475 466
		f 4 735 736 -725 737
		mu 0 4 476 477 469 468
		f 4 738 739 740 -730
		mu 0 4 472 478 479 473
		f 4 -741 741 742 -732
		mu 0 4 473 479 480 474
		f 4 -743 743 744 -734
		mu 0 4 474 480 481 475
		f 4 745 746 -736 747
		mu 0 4 482 483 477 476
		f 4 748 749 750 -740
		mu 0 4 478 484 485 479
		f 4 -751 751 752 -742
		mu 0 4 479 485 486 480
		f 4 -753 753 754 -744
		mu 0 4 480 486 487 481
		f 4 755 -746 756 757
		mu 0 4 488 483 482 489
		f 4 -755 758 -757 759
		mu 0 4 481 487 489 482
		f 4 -745 -760 -748 760
		mu 0 4 475 481 482 476
		f 4 -735 -761 -738 761
		mu 0 4 466 475 476 468
		f 4 -723 -762 -728 762
		mu 0 4 467 466 468 471
		f 4 763 764 765 -715
		mu 0 4 490 491 492 493
		f 4 766 767 768 -765
		mu 0 4 491 494 495 492
		f 4 769 770 771 -768
		mu 0 4 494 496 497 495
		f 4 772 773 -726 774
		mu 0 4 498 499 470 469
		f 4 -766 775 776 -729
		mu 0 4 493 492 500 501
		f 4 -769 777 778 -776
		mu 0 4 492 495 502 500
		f 4 -772 779 780 -778
		mu 0 4 495 497 503 502
		f 4 781 -775 -737 782
		mu 0 4 504 498 469 477
		f 4 -777 783 784 -739
		mu 0 4 501 500 505 506
		f 4 -779 785 786 -784
		mu 0 4 500 502 507 505
		f 4 -781 787 788 -786
		mu 0 4 502 503 508 507
		f 4 789 -783 -747 790
		mu 0 4 509 504 477 483
		f 4 -785 791 792 -749
		mu 0 4 506 505 510 511
		f 4 -787 793 794 -792
		mu 0 4 505 507 512 510
		f 4 -789 795 796 -794
		mu 0 4 507 508 513 512
		f 4 797 798 -791 -756
		mu 0 4 488 514 509 483
		f 4 799 -799 800 -796
		mu 0 4 508 509 514 513
		f 4 801 -790 -800 -788
		mu 0 4 503 504 509 508
		f 4 802 -782 -802 -780
		mu 0 4 497 498 504 503
		f 4 803 -773 -803 -771
		mu 0 4 496 499 498 497
		f 4 -718 804 805 806
		mu 0 4 460 463 515 516
		f 4 -721 807 808 -805
		mu 0 4 463 465 517 515
		f 4 -724 809 810 -808
		mu 0 4 465 467 518 517
		f 4 -727 811 812 813
		mu 0 4 471 470 519 520
		f 4 -750 814 815 816
		mu 0 4 485 484 521 522
		f 4 -752 -817 817 818
		mu 0 4 486 485 522 523
		f 4 -754 -819 819 820
		mu 0 4 487 486 523 524
		f 4 -758 821 822 823
		mu 0 4 488 489 525 526
		f 4 -759 -821 824 -822
		mu 0 4 489 487 524 525
		f 4 -763 -814 825 -810
		mu 0 4 467 471 520 518
		f 4 -764 -807 826 827
		mu 0 4 491 490 527 528
		f 4 -767 -828 828 829
		mu 0 4 494 491 528 529
		f 4 -770 -830 830 831
		mu 0 4 496 494 529 530
		f 4 -774 832 833 -812
		mu 0 4 470 499 531 519
		f 4 -793 834 835 -815
		mu 0 4 511 510 532 533
		f 4 -795 836 837 -835
		mu 0 4 510 512 534 532
		f 4 -797 838 839 -837
		mu 0 4 512 513 535 534
		f 4 -798 -824 840 841
		mu 0 4 514 488 526 536
		f 4 -801 -842 842 -839
		mu 0 4 513 514 536 535
		f 4 -804 -832 843 -833
		mu 0 4 499 496 530 531
		f 4 -836 844 -702 845
		mu 0 4 537 538 456 455
		f 4 -816 -846 -660 846
		mu 0 4 539 540 430 429
		f 4 -818 -847 -663 847
		mu 0 4 541 539 429 431
		f 4 -820 -848 -665 848
		mu 0 4 524 541 431 432
		f 4 -825 -849 -670 849
		mu 0 4 525 524 432 434
		f 4 -823 -850 -666 850
		mu 0 4 526 525 434 433
		f 4 -841 -851 -709 851
		mu 0 4 536 526 433 459
		f 4 -843 -852 -710 852
		mu 0 4 535 536 459 458
		f 4 -840 -853 -706 853
		mu 0 4 542 535 458 457
		f 4 -838 -854 -704 -845
		mu 0 4 538 542 457 456
		f 4 -811 854 -632 855
		mu 0 4 543 544 411 409
		f 4 -809 -856 -629 856
		mu 0 4 545 543 409 406
		f 4 -806 -857 -625 857
		mu 0 4 546 545 406 405
		f 4 -827 -858 -676 858
		mu 0 4 547 548 435 438
		f 4 -829 -859 -679 859
		mu 0 4 549 547 438 440
		f 4 -831 -860 -682 860
		mu 0 4 550 549 440 442
		f 4 -844 -861 -714 861
		mu 0 4 551 550 442 444
		f 4 -834 -862 -684 862
		mu 0 4 552 551 444 415
		f 4 -813 -863 -636 863
		mu 0 4 553 552 415 414
		f 4 -826 -864 -673 -855
		mu 0 4 544 553 414 411;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "facialFeatures:polySurface1";
createNode transform -n "bear_eye" -p "facialFeatures:polySurface1";
	setAttr ".rp" -type "double3" -1.1094734966754913 48.983343124389648 9.6061296463012695 ;
	setAttr ".sp" -type "double3" -1.1094734966754913 48.983343124389648 9.6061296463012695 ;
createNode mesh -n "bear_eyeShape" -p "bear_eye";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:47]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:47]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 66 ".uvst[0].uvsp[0:65]" -type "float2" 0.729478 0.89929199
		 0.76739198 0.88371599 0.777022 0.89854699 0.73369098 0.91651702 0.79307401 0.86046898
		 0.80753303 0.86953199 0.80241603 0.83219701 0.81945097 0.83323801 0.79658997 0.80299699
		 0.81204498 0.79576403 0.77393198 0.776793 0.78529698 0.763246 0.738199 0.75671202
		 0.744479 0.74013001 0.69438201 0.748155 0.69547701 0.73024797 0.64984798 0.75130898
		 0.64563602 0.73408502 0.61193502 0.76688498 0.602305 0.75205398 0.58625197 0.79013199
		 0.57179302 0.78106999 0.57691097 0.81840497 0.55987501 0.81736201 0.58273602 0.84760499
		 0.56728101 0.85483801 0.60539502 0.87380898 0.59403002 0.88735598 0.641128 0.893888
		 0.634848 0.91047102 0.68494499 0.90244699 0.68384898 0.92035401 0.68966299 0.82529998
		 0.773242 0.609375 0.82587302 0.54387701 0.86031502 0.56610203 0.88133001 0.59364003
		 0.885364 0.62314099 0.87431401 0.65079099 0.84726 0.67242599 0.808465 0.68565798
		 0.76382399 0.68608803 0.72061002 0.67487198 0.68616998 0.65264797 0.66515303 0.62510902
		 0.66112 0.595608 0.67216998 0.56796002 0.69922501 0.54632401 0.73801798 0.53309101
		 0.78266102 0.53266102 0.673998 0.66547501 0.64928502 0.63138598 0.71332902 0.69103998
		 0.76163697 0.70389402 0.81161898 0.70310801 0.85596597 0.68781698 0.888192 0.66071898
		 0.90230298 0.62522101 0.89719802 0.58736402 0.872486 0.55327398 0.83315402 0.52770901
		 0.78484702 0.51485401 0.73486501 0.51564199 0.69051802 0.53093302 0.65829098 0.55803102
		 0.64418101 0.59352899;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 50 ".vt[0:49]"  -0.67571902 49.24973297 9.49502087 -0.77749002 49.47555923 9.49502087
		 -0.92980802 49.62644958 9.49502087 -1.10947502 49.67943954 9.49502087 -1.28914201 49.62644958 9.49502087
		 -1.44145501 49.47555923 9.49502087 -1.54322898 49.24973297 9.49502087 -1.57896602 48.98334503 9.49502087
		 -1.54322898 48.71695328 9.49502087 -1.44145501 48.49113083 9.49502087 -1.28914201 48.34023285 9.49502087
		 -1.10947502 48.2872467 9.49502087 -0.92980802 48.34023285 9.49502087 -0.77749002 48.49113083 9.49502087
		 -0.67571902 48.71695328 9.49502087 -0.63998097 48.98334503 9.49502087 -0.67571902 49.24973297 9.71723461
		 -0.77749002 49.47555923 9.71723461 -0.92980802 49.62644958 9.71723461 -1.10947502 49.67943954 9.71723461
		 -1.28914201 49.62644958 9.71723461 -1.44145501 49.47555923 9.71723461 -1.54322898 49.24973297 9.71723461
		 -1.57896602 48.98334503 9.71723461 -1.54322898 48.71695328 9.71723461 -1.44145501 48.49113083 9.71723461
		 -1.28914201 48.34023285 9.71723461 -1.10947502 48.2872467 9.71723461 -0.92980802 48.34023285 9.71723461
		 -0.77749002 48.49113083 9.71723461 -0.67571902 48.71695328 9.71723461 -0.63998097 48.98334503 9.71723461
		 -1.10947502 48.98334503 9.39272404 -1.10947502 48.98334503 9.81953526 -1.28914201 48.34023285 9.60612869
		 -1.44145501 48.49113083 9.60612869 -1.54322898 48.71695328 9.60612869 -1.57896602 48.98334503 9.60612869
		 -1.54322898 49.24973297 9.60612869 -1.44145501 49.47555923 9.60612869 -1.28914201 49.62644958 9.60612869
		 -1.10947502 49.67943954 9.60612869 -0.92980802 49.62644958 9.60612869 -0.77749002 49.47555923 9.60612869
		 -0.67571902 49.24973297 9.60612869 -0.63998097 48.98334503 9.60612869 -0.67571902 48.71695328 9.60612869
		 -0.77749002 48.49113083 9.60612869 -0.92980802 48.34023285 9.60612869 -1.10947502 48.2872467 9.60612869;
	setAttr -s 96 ".ed[0:95]"  0 1 0 1 43 1 43 44 0 44 0 1 1 2 0 2 42 1
		 42 43 0 2 3 0 3 41 1 41 42 0 3 4 0 4 40 1 40 41 0 4 5 0 5 39 1 39 40 0 5 6 0 6 38 1
		 38 39 0 6 7 0 7 37 1 37 38 0 7 8 0 8 36 1 36 37 0 8 9 0 9 35 1 35 36 0 9 10 0 10 34 1
		 34 35 0 10 11 0 11 49 1 49 34 0 11 12 0 12 48 1 48 49 0 12 13 0 13 47 1 47 48 0 13 14 0
		 14 46 1 46 47 0 14 15 0 15 45 1 45 46 0 15 0 0 44 45 0 0 32 1 32 2 1 32 4 1 32 6 1
		 32 8 1 32 10 1 32 12 1 32 14 1 33 16 1 16 17 0 17 18 0 18 33 1 18 19 0 19 20 0 20 33 1
		 20 21 0 21 22 0 22 33 1 22 23 0 23 24 0 24 33 1 24 25 0 25 26 0 26 33 1 26 27 0 27 28 0
		 28 33 1 28 29 0 29 30 0 30 33 1 30 31 0 31 16 0 34 26 1 25 35 1 24 36 1 23 37 1 22 38 1
		 21 39 1 20 40 1 19 41 1 18 42 1 17 43 1 16 44 1 31 45 1 30 46 1 29 47 1 28 48 1 27 49 1;
	setAttr -s 48 -ch 192 ".fc[0:47]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 -9
		mu 0 4 6 8 9 7
		f 4 13 14 15 -12
		mu 0 4 8 10 11 9
		f 4 16 17 18 -15
		mu 0 4 10 12 13 11
		f 4 19 20 21 -18
		mu 0 4 12 14 15 13
		f 4 22 23 24 -21
		mu 0 4 14 16 17 15
		f 4 25 26 27 -24
		mu 0 4 16 18 19 17
		f 4 28 29 30 -27
		mu 0 4 18 20 21 19
		f 4 31 32 33 -30
		mu 0 4 20 22 23 21
		f 4 34 35 36 -33
		mu 0 4 22 24 25 23
		f 4 37 38 39 -36
		mu 0 4 24 26 27 25
		f 4 40 41 42 -39
		mu 0 4 26 28 29 27
		f 4 43 44 45 -42
		mu 0 4 28 30 31 29
		f 4 46 -4 47 -45
		mu 0 4 30 0 3 31
		f 4 -1 48 49 -5
		mu 0 4 1 0 32 4
		f 4 -8 -50 50 -11
		mu 0 4 6 4 32 8
		f 4 -14 -51 51 -17
		mu 0 4 10 8 32 12
		f 4 -20 -52 52 -23
		mu 0 4 14 12 32 16
		f 4 -26 -53 53 -29
		mu 0 4 18 16 32 20
		f 4 -32 -54 54 -35
		mu 0 4 22 20 32 24
		f 4 -38 -55 55 -41
		mu 0 4 26 24 32 28
		f 4 -44 -56 -49 -47
		mu 0 4 30 28 32 0
		f 4 56 57 58 59
		mu 0 4 33 34 35 36
		f 4 -60 60 61 62
		mu 0 4 33 36 37 38
		f 4 -63 63 64 65
		mu 0 4 33 38 39 40
		f 4 -66 66 67 68
		mu 0 4 33 40 41 42
		f 4 -69 69 70 71
		mu 0 4 33 42 43 44
		f 4 -72 72 73 74
		mu 0 4 33 44 45 46
		f 4 -75 75 76 77
		mu 0 4 33 46 47 48
		f 4 -78 78 79 -57
		mu 0 4 33 48 49 34
		f 4 -31 80 -71 81
		mu 0 4 50 51 44 43
		f 4 -28 -82 -70 82
		mu 0 4 52 50 43 42
		f 4 -25 -83 -68 83
		mu 0 4 53 52 42 41
		f 4 -22 -84 -67 84
		mu 0 4 54 53 41 40
		f 4 -19 -85 -65 85
		mu 0 4 55 54 40 39
		f 4 -16 -86 -64 86
		mu 0 4 56 55 39 38
		f 4 -13 -87 -62 87
		mu 0 4 57 56 38 37
		f 4 -10 -88 -61 88
		mu 0 4 58 57 37 36
		f 4 -7 -89 -59 89
		mu 0 4 59 58 36 35
		f 4 -3 -90 -58 90
		mu 0 4 60 59 35 34
		f 4 -48 -91 -80 91
		mu 0 4 61 60 34 49
		f 4 -46 -92 -79 92
		mu 0 4 62 61 49 48
		f 4 -43 -93 -77 93
		mu 0 4 63 62 48 47
		f 4 -40 -94 -76 94
		mu 0 4 64 63 47 46
		f 4 -37 -95 -74 95
		mu 0 4 65 64 46 45
		f 4 -34 -96 -73 -81
		mu 0 4 51 65 45 44;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_nose" -p "facialFeatures:polySurface1";
	setAttr ".rp" -type "double3" 0.33350953459739685 46.501867294311523 11.743663311004639 ;
	setAttr ".sp" -type "double3" 0.33350953459739685 46.501867294311523 11.743663311004639 ;
createNode mesh -n "bear_noseShape" -p "bear_nose";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 74 ".uvst[0].uvsp[0:73]" -type "float2" 0.51215899 0.221607
		 0.53935301 0.185931 0.55994803 0.21596999 0.52474201 0.27004001 0.572267 0.127625
		 0.60862899 0.129737 0.588763 0.08591 0.636733 0.071658999 0.58444601 0.23638999 0.54925698
		 0.29909599 0.63876897 0.14016099 0.67427701 0.077633999 0.61120403 0.25167701 0.58218098
		 0.31878901 0.66568297 0.15517201 0.708148 0.095647 0.653781 0.27447799 0.58968598
		 0.36942899 0.707205 0.17984401 0.75538301 0.075907998 0.684591 0.29168999 0.632846
		 0.39373201 0.73786002 0.19733 0.79848999 0.100303 0.72668701 0.31665599 0.68041497
		 0.373384 0.780985 0.22047199 0.80564803 0.151545 0.75498903 0.33253399 0.71327102
		 0.39090499 0.809201 0.23650099 0.837623 0.170623 0.78725803 0.345227 0.75007898 0.39926901
		 0.83674097 0.257572 0.86380202 0.197816 0.82585597 0.35163999 0.80021298 0.39483401
		 0.86217397 0.28730601 0.88590699 0.24303199 0.44584301 0.18767899 0.45392099 0.140625
		 0.48321399 0.15654901 0.45382899 0.220275 0.489337 0.077886999 0.51810497 0.094740003
		 0.52544999 0.046659999 0.55748701 0.036657002 0.51148802 0.172039 0.48578599 0.23839299
		 0.54597503 0.110946 0.58950901 0.054657999 0.90640903 0.213874 0.88400298 0.161598
		 0.93853599 0.19922499 0.91710299 0.14264201 0.64954698 0.031420998 0.86232799 0.13335501
		 0.892465 0.108871 0.69121498 0.035190001 0.836564 0.111609 0.86676002 0.082948998
		 0.72451699 0.050000001 0.785842 0.427452 0.72950703 0.435278 0.78989899 0.46252701
		 0.73038101 0.47341201 0.496912 0.301801 0.69412702 0.43131199 0.68873501 0.46976599
		 0.52166802 0.33552799 0.66219801 0.42049 0.65325999 0.46114901 0.55155599 0.35638899;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".vt[0:55]"  -0.145724 46.080299377 11.97045612 0.123719 46.080299377 12.049408913
		 0.54329801 46.080299377 12.049408913 0.81274301 46.080299377 11.97045612 -0.359642 46.29058838 12.0099439621
		 0.030076999 46.29058838 12.10264683 0.63694203 46.29058838 12.10264683 1.026661992 46.29058838 12.0099439621
		 -0.42757601 46.50088501 12.0099439621 0.000336 46.50088501 12.10264683 0.66667998 46.50088501 12.10264683
		 1.094598055 46.50088501 12.0099439621 -0.42757601 46.71116257 12.0099439621 0.000336 46.71116257 12.10264683
		 0.66667998 46.71116257 12.10264683 1.094598055 46.71116257 12.0099439621 -0.66600198 46.71116257 11.86332607
		 0.000336 46.92343521 11.86332607 0.66667998 46.92343521 11.86332607 1.33302104 46.71116257 11.86332607
		 -0.66600198 46.71116257 11.62400436 0.000336 46.92343521 11.62400436 0.66667998 46.92343521 11.62400436
		 1.33302104 46.71116257 11.62400436 -0.42757601 46.71116257 11.47738171 0.000336 46.71116257 11.38467979
		 0.66667998 46.71116257 11.38467979 1.094598055 46.71116257 11.47738171 -0.42757601 46.50088501 11.47738171
		 0.000336 46.50088501 11.38467979 0.66667998 46.50088501 11.38467979 1.094598055 46.50088501 11.47738171
		 -0.359642 46.29058838 11.47738171 0.030076999 46.29058838 11.38467979 0.63694203 46.29058838 11.38467979
		 1.026661992 46.29058838 11.47738171 -0.145724 46.080299377 11.51687336 0.123719 46.080299377 11.43791771
		 0.54329801 46.080299377 11.43791771 0.81274301 46.080299377 11.51687336 -0.295854 46.080299377 11.64175034
		 0.123719 46.080299377 11.64175034 0.54329801 46.080299377 11.64175034 0.96287298 46.080299377 11.64175034
		 -0.295854 46.080299377 11.84557915 0.123719 46.080299377 11.84557915 0.54329801 46.080299377 11.84557915
		 0.96287298 46.080299377 11.84557915 1.243806 46.29058838 11.62400436 1.243806 46.29058838 11.86332607
		 1.33302104 46.50088501 11.62400436 1.33302104 46.50088501 11.86332607 -0.57678598 46.29058838 11.62400436
		 -0.57678598 46.29058838 11.86332607 -0.66600198 46.50088501 11.62400436 -0.66600198 46.50088501 11.86332607;
	setAttr -s 108 ".ed[0:107]"  0 1 0 1 5 1 5 4 1 4 0 0 1 2 0 2 6 1 6 5 1
		 2 3 0 3 7 0 7 6 1 5 9 1 9 8 1 8 4 0 6 10 1 10 9 1 7 11 0 11 10 1 9 13 1 13 12 0 12 8 0
		 10 14 1 14 13 0 11 15 0 15 14 0 13 17 1 17 16 1 16 12 0 14 18 1 18 17 1 15 19 0 19 18 1
		 17 21 1 21 20 1 20 16 0 18 22 1 22 21 1 19 23 0 23 22 1 21 25 1 25 24 0 24 20 0 22 26 1
		 26 25 0 23 27 0 27 26 0 25 29 1 29 28 1 28 24 0 26 30 1 30 29 1 27 31 0 31 30 1 29 33 1
		 33 32 1 32 28 0 30 34 1 34 33 1 31 35 0 35 34 1 33 37 1 37 36 0 36 32 0 34 38 1 38 37 0
		 35 39 0 39 38 0 37 41 1 41 40 1 40 36 0 38 42 1 42 41 1 39 43 0 43 42 1 41 45 1 45 44 1
		 44 40 0 42 46 1 46 45 1 43 47 0 47 46 1 45 1 1 0 44 0 46 2 1 47 3 0 35 48 1 48 43 1
		 48 49 1 49 47 1 49 7 1 31 50 1 50 48 1 50 51 1 51 49 1 51 11 1 23 50 1 19 51 1 40 52 1
		 52 32 1 44 53 1 53 52 1 4 53 1 52 54 1 54 28 1 53 55 1 55 54 1 8 55 1 54 20 1 55 16 1;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 -3 10 11 12
		mu 0 4 3 2 8 9
		f 4 -7 13 14 -11
		mu 0 4 2 5 10 8
		f 4 -10 15 16 -14
		mu 0 4 5 7 11 10
		f 4 -12 17 18 19
		mu 0 4 9 8 12 13
		f 4 -15 20 21 -18
		mu 0 4 8 10 14 12
		f 4 -17 22 23 -21
		mu 0 4 10 11 15 14
		f 4 -19 24 25 26
		mu 0 4 13 12 16 17
		f 4 -22 27 28 -25
		mu 0 4 12 14 18 16
		f 4 -24 29 30 -28
		mu 0 4 14 15 19 18
		f 4 -26 31 32 33
		mu 0 4 17 16 20 21
		f 4 -29 34 35 -32
		mu 0 4 16 18 22 20
		f 4 -31 36 37 -35
		mu 0 4 18 19 23 22
		f 4 -33 38 39 40
		mu 0 4 21 20 24 25
		f 4 -36 41 42 -39
		mu 0 4 20 22 26 24
		f 4 -38 43 44 -42
		mu 0 4 22 23 27 26
		f 4 -40 45 46 47
		mu 0 4 25 24 28 29
		f 4 -43 48 49 -46
		mu 0 4 24 26 30 28
		f 4 -45 50 51 -49
		mu 0 4 26 27 31 30
		f 4 -47 52 53 54
		mu 0 4 29 28 32 33
		f 4 -50 55 56 -53
		mu 0 4 28 30 34 32
		f 4 -52 57 58 -56
		mu 0 4 30 31 35 34
		f 4 -54 59 60 61
		mu 0 4 33 32 36 37
		f 4 -57 62 63 -60
		mu 0 4 32 34 38 36
		f 4 -59 64 65 -63
		mu 0 4 34 35 39 38
		f 4 -61 66 67 68
		mu 0 4 40 41 42 43
		f 4 -64 69 70 -67
		mu 0 4 41 44 45 42
		f 4 -66 71 72 -70
		mu 0 4 44 46 47 45
		f 4 -68 73 74 75
		mu 0 4 43 42 48 49
		f 4 -71 76 77 -74
		mu 0 4 42 45 50 48
		f 4 -73 78 79 -77
		mu 0 4 45 47 51 50
		f 4 -75 80 -1 81
		mu 0 4 49 48 1 0
		f 4 -78 82 -5 -81
		mu 0 4 48 50 4 1
		f 4 -80 83 -8 -83
		mu 0 4 50 51 6 4
		f 4 -72 -65 84 85
		mu 0 4 52 39 35 53
		f 4 -79 -86 86 87
		mu 0 4 54 52 53 55
		f 4 -84 -88 88 -9
		mu 0 4 6 51 56 7
		f 4 -85 -58 89 90
		mu 0 4 53 35 31 57
		f 4 -87 -91 91 92
		mu 0 4 55 53 57 58
		f 4 -89 -93 93 -16
		mu 0 4 7 56 59 11
		f 4 -90 -51 -44 94
		mu 0 4 57 31 27 60
		f 4 -92 -95 -37 95
		mu 0 4 58 57 60 61
		f 4 -94 -96 -30 -23
		mu 0 4 11 59 62 15
		f 4 -69 96 97 -62
		mu 0 4 37 63 64 33
		f 4 -76 98 99 -97
		mu 0 4 63 65 66 64
		f 4 -82 -4 100 -99
		mu 0 4 49 0 3 67
		f 4 -98 101 102 -55
		mu 0 4 33 64 68 29
		f 4 -100 103 104 -102
		mu 0 4 64 66 69 68
		f 4 -101 -13 105 -104
		mu 0 4 67 3 9 70
		f 4 -103 106 -41 -48
		mu 0 4 29 68 71 25
		f 4 -105 107 -34 -107
		mu 0 4 68 69 72 71
		f 4 -106 -20 -27 -108
		mu 0 4 70 9 13 73;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_browR" -p "facialFeatures:polySurface1";
	setAttr ".rp" -type "double3" -1.1015084981918335 49.9091796875 9.3020696640014648 ;
	setAttr ".sp" -type "double3" -1.1015084981918335 49.9091796875 9.3020696640014648 ;
createNode mesh -n "bear_browRShape" -p "bear_browR";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 76 ".uvst[0].uvsp[0:75]" -type "float2" 0.152344 0.31540701
		 0.16103201 0.34634101 0.148353 0.349832 0.13967299 0.31894299 0.16953599 0.377415
		 0.156928 0.38069001 0.176066 0.40843901 0.16491 0.411935 0.13567799 0.35336 0.126991
		 0.32248801 0.14447699 0.38409099 0.15371799 0.41486499 0.123027 0.35696101 0.114324
		 0.32605401 0.13199 0.387795 0.142673 0.417303 0.054088 0.38803899 0.084686004 0.39716199
		 0.065329999 0.553186 0.033702999 0.54505098 0.114251 0.40744099 0.097542003 0.56187701
		 0.130504 0.57051402 0.059615001 0.72326899 0.027979 0.72038198 0.091429003 0.72561598
		 0.123548 0.72845 0.061785001 0.87417197 0.030055 0.87378901 0.092820004 0.87364697
		 0.122914 0.87304598 0.13620199 0.96776098 0.131193 0.93616003 0.144187 0.93414497
		 0.149196 0.96571398 0.126331 0.90451199 0.13925099 0.90264398 0.13481501 0.87094301
		 0.15718 0.93209499 0.16219901 0.96365601 0.15204901 0.900657 0.14657301 0.869048
		 0.17016099 0.92998701 0.175191 0.96158201 0.16489901 0.89843899 0.158021 0.86780399
		 0.247223 0.87605101 0.216636 0.872518 0.231411 0.722076 0.26387101 0.72245902 0.186905
		 0.86945599 0.19871999 0.721645 0.165665 0.72216201 0.24020199 0.55154198 0.272192
		 0.54556 0.207513 0.55745697 0.174161 0.56265998 0.234896 0.394086 0.26592001 0.38674
		 0.204946 0.401822 0.151371 0.72365701 0.15973 0.56579202 0.137317 0.725797 0.14518
		 0.568304 0.27708301 0.725941 0.25948301 0.879273 0.284547 0.54834402 0.27871901 0.38869101
		 0.29008999 0.729002 0.27206001 0.88226497 0.29697701 0.55128002 0.291583 0.391491
		 0.30289799 0.73200297 0.28494501 0.88450801 0.30952701 0.55461299 0.30443799 0.394407;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".vt[0:55]"  -2.52193999 50.4037323 8.89678288 -2.4921751 50.21167374 8.89678288
		 -2.46239209 50.019504547 8.89678288 -2.43265009 49.82758331 8.89678288 -2.52193999 50.4037323 8.97633171
		 -2.4921751 50.21167374 8.97633171 -2.46239209 50.019504547 8.97633171 -2.43265009 49.82758331 8.97633171
		 -2.52193999 50.4037323 9.055879593 -2.4921751 50.21167374 9.055879593 -2.46239209 50.019504547 9.055879593
		 -2.43265009 49.82758331 9.055879593 -2.52193999 50.4037323 9.13542938 -2.4921751 50.21167374 9.13542938
		 -2.46239209 50.019504547 9.13542938 -2.43265009 49.82758331 9.13542938 -1.58404803 50.42763138 9.37421608
		 -1.56802201 50.23236084 9.37362957 -1.551934 50.037082672 9.37304401 -1.53578901 49.84183502 9.37246799
		 -0.57578498 50.22588348 9.6986618 -0.60336202 50.035793304 9.69535255 -0.63061303 49.84591675 9.69152069
		 -0.65749002 49.65553665 9.68863297 0.318923 49.95426178 9.70735645 0.27196801 49.76641464 9.70735645
		 0.225012 49.57857132 9.70735645 0.178055 49.390728 9.70735645 0.318923 49.95426178 9.62780857
		 0.27196801 49.76641464 9.62780857 0.225012 49.57857132 9.62780857 0.178055 49.390728 9.62780857
		 0.318923 49.95426178 9.54826069 0.27196801 49.76641464 9.54826069 0.225012 49.57857132 9.54826069
		 0.178055 49.390728 9.54826069 0.318923 49.95426178 9.46871185 0.27196801 49.76641464 9.46871185
		 0.225012 49.57857132 9.46871185 0.178055 49.390728 9.46871185 -0.57578498 50.22588348 9.46001816
		 -0.60336 50.035793304 9.45670795 -0.63061303 49.84591675 9.45287704 -0.65749002 49.65553665 9.44998741
		 -1.58404803 50.42763138 9.13556767 -1.56802201 50.23236084 9.13498402 -1.551934 50.037082672 9.13439846
		 -1.53578901 49.84183502 9.13382053 -0.65749002 49.65553665 9.52953625 -1.53578901 49.84183502 9.21337128
		 -0.65749002 49.65553665 9.60908604 -1.53578901 49.84183502 9.29291916 -0.57578498 50.22588348 9.53956604
		 -1.58404803 50.42763138 9.21511745 -0.57578498 50.22588348 9.61911392 -1.58404803 50.42763138 9.29466629;
	setAttr -s 108 ".ed[0:107]"  0 1 0 1 5 1 5 4 1 4 0 0 1 2 0 2 6 1 6 5 1
		 2 3 0 3 7 0 7 6 1 5 9 1 9 8 1 8 4 0 6 10 1 10 9 1 7 11 0 11 10 1 9 13 1 13 12 0 12 8 0
		 10 14 1 14 13 0 11 15 0 15 14 0 13 17 1 17 16 1 16 12 0 14 18 1 18 17 1 15 19 0 19 18 1
		 17 21 1 21 20 1 20 16 0 18 22 1 22 21 1 19 23 0 23 22 1 21 25 1 25 24 0 24 20 0 22 26 1
		 26 25 0 23 27 0 27 26 0 25 29 1 29 28 1 28 24 0 26 30 1 30 29 1 27 31 0 31 30 1 29 33 1
		 33 32 1 32 28 0 30 34 1 34 33 1 31 35 0 35 34 1 33 37 1 37 36 0 36 32 0 34 38 1 38 37 0
		 35 39 0 39 38 0 37 41 1 41 40 1 40 36 0 38 42 1 42 41 1 39 43 0 43 42 1 41 45 1 45 44 1
		 44 40 0 42 46 1 46 45 1 43 47 0 47 46 1 45 1 1 0 44 0 46 2 1 47 3 0 35 48 1 48 43 1
		 48 49 1 49 47 1 49 7 1 31 50 1 50 48 1 50 51 1 51 49 1 51 11 1 23 50 1 19 51 1 40 52 1
		 52 32 1 44 53 1 53 52 1 4 53 1 52 54 1 54 28 1 53 55 1 55 54 1 8 55 1 54 20 1 55 16 1;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 -3 10 11 12
		mu 0 4 3 2 8 9
		f 4 -7 13 14 -11
		mu 0 4 2 5 10 8
		f 4 -10 15 16 -14
		mu 0 4 5 7 11 10
		f 4 -12 17 18 19
		mu 0 4 9 8 12 13
		f 4 -15 20 21 -18
		mu 0 4 8 10 14 12
		f 4 -17 22 23 -21
		mu 0 4 10 11 15 14
		f 4 -19 24 25 26
		mu 0 4 16 17 18 19
		f 4 -22 27 28 -25
		mu 0 4 17 20 21 18
		f 4 -24 29 30 -28
		mu 0 4 20 15 22 21
		f 4 -26 31 32 33
		mu 0 4 19 18 23 24
		f 4 -29 34 35 -32
		mu 0 4 18 21 25 23
		f 4 -31 36 37 -35
		mu 0 4 21 22 26 25
		f 4 -33 38 39 40
		mu 0 4 24 23 27 28
		f 4 -36 41 42 -39
		mu 0 4 23 25 29 27
		f 4 -38 43 44 -42
		mu 0 4 25 26 30 29
		f 4 -40 45 46 47
		mu 0 4 31 32 33 34
		f 4 -43 48 49 -46
		mu 0 4 32 35 36 33
		f 4 -45 50 51 -49
		mu 0 4 35 30 37 36
		f 4 -47 52 53 54
		mu 0 4 34 33 38 39
		f 4 -50 55 56 -53
		mu 0 4 33 36 40 38
		f 4 -52 57 58 -56
		mu 0 4 36 37 41 40
		f 4 -54 59 60 61
		mu 0 4 39 38 42 43
		f 4 -57 62 63 -60
		mu 0 4 38 40 44 42
		f 4 -59 64 65 -63
		mu 0 4 40 41 45 44
		f 4 -61 66 67 68
		mu 0 4 46 47 48 49
		f 4 -64 69 70 -67
		mu 0 4 47 50 51 48
		f 4 -66 71 72 -70
		mu 0 4 50 45 52 51
		f 4 -68 73 74 75
		mu 0 4 49 48 53 54
		f 4 -71 76 77 -74
		mu 0 4 48 51 55 53
		f 4 -73 78 79 -77
		mu 0 4 51 52 56 55
		f 4 -75 80 -1 81
		mu 0 4 54 53 57 58
		f 4 -78 82 -5 -81
		mu 0 4 53 55 59 57
		f 4 -80 83 -8 -83
		mu 0 4 55 56 6 59
		f 4 -72 -65 84 85
		mu 0 4 52 45 41 60
		f 4 -79 -86 86 87
		mu 0 4 56 52 60 61
		f 4 -84 -88 88 -9
		mu 0 4 6 56 61 7
		f 4 -85 -58 89 90
		mu 0 4 60 41 37 62
		f 4 -87 -91 91 92
		mu 0 4 61 60 62 63
		f 4 -89 -93 93 -16
		mu 0 4 7 61 63 11
		f 4 -90 -51 -44 94
		mu 0 4 62 37 30 26
		f 4 -92 -95 -37 95
		mu 0 4 63 62 26 22
		f 4 -94 -96 -30 -23
		mu 0 4 11 63 22 15
		f 4 -69 96 97 -62
		mu 0 4 46 49 64 65
		f 4 -76 98 99 -97
		mu 0 4 49 54 66 64
		f 4 -82 -4 100 -99
		mu 0 4 54 58 67 66
		f 4 -98 101 102 -55
		mu 0 4 65 64 68 69
		f 4 -100 103 104 -102
		mu 0 4 64 66 70 68
		f 4 -101 -13 105 -104
		mu 0 4 66 67 71 70
		f 4 -103 106 -41 -48
		mu 0 4 69 68 72 73
		f 4 -105 107 -34 -107
		mu 0 4 68 70 74 72
		f 4 -106 -20 -27 -108
		mu 0 4 70 71 75 74;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_browL" -p "facialFeatures:polySurface1";
	setAttr ".rp" -type "double3" 2.8405844569206238 49.711399078369141 8.9626312255859375 ;
	setAttr ".sp" -type "double3" 2.8405844569206238 49.711399078369141 8.9626312255859375 ;
createNode mesh -n "bear_browLShape" -p "bear_browL";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 76 ".uvst[0].uvsp[0:75]" -type "float2" 0.425789 0.32434401
		 0.41322699 0.32825199 0.422811 0.358872 0.43538201 0.35500801 0.43228599 0.38946301
		 0.44478899 0.38580999 0.44120401 0.42044801 0.45224601 0.41659701 0.40065601 0.33217201
		 0.410247 0.36277699 0.419943 0.39324 0.43010199 0.42373401 0.38809901 0.33611101
		 0.397708 0.36675 0.407574 0.39731699 0.41913101 0.42652401 0.32963401 0.40018201
		 0.314363 0.55777597 0.346241 0.564879 0.360513 0.40830401 0.37871799 0.57251197 0.39039701
		 0.41760999 0.411944 0.58006603 0.314338 0.73320401 0.34605101 0.73505998 0.37792599
		 0.73637301 0.410117 0.738159 0.32142499 0.88646501 0.35315201 0.88581401 0.384161
		 0.88428497 0.41423601 0.88273197 0.43145201 0.97692299 0.44435 0.97433698 0.43801999
		 0.943003 0.42511901 0.94555402 0.43175799 0.91172701 0.41892201 0.91411 0.426056
		 0.88021201 0.45725799 0.971744 0.45091799 0.94042403 0.44446799 0.90924102 0.43772599
		 0.87789398 0.470153 0.96913099 0.463801 0.93778098 0.45721599 0.906506 0.449101 0.87624598
		 0.53848499 0.88164598 0.55016798 0.727597 0.51771301 0.72825801 0.50780803 0.879098
		 0.48502699 0.728881 0.47800499 0.87699097 0.452005 0.73046702 0.552746 0.55052102
		 0.52096701 0.55753303 0.488489 0.564502 0.455322 0.57078499 0.54134798 0.391987 0.51057601
		 0.40033099 0.48089299 0.40903601 0.437765 0.732436 0.44099799 0.57438803 0.42379001
		 0.735048 0.42653799 0.57737601 0.55083799 0.884471 0.56348401 0.73065102 0.56518298
		 0.55290502 0.55420297 0.39352399 0.56350201 0.88705802 0.57658201 0.73329097 0.57770002
		 0.555439 0.56715202 0.39590901 0.576451 0.88888401 0.589481 0.73587799 0.59034997
		 0.55836499 0.58009601 0.398408;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".vt[0:55]"  4.17371702 50.20594406 8.3265152 4.14436722 50.013889313 8.33146954
		 4.11500311 49.82172394 8.33642769 4.085672855 49.6298027 8.34138298 4.18696308 50.20594406 8.404953
		 4.15761185 50.013889313 8.4099102 4.12824583 49.82172394 8.41486931 4.098917007 49.6298027 8.41982079
		 4.20020723 50.20594406 8.48339462 4.170856 50.013889313 8.48834801 4.14148903 49.82172394 8.49330711
		 4.11216021 49.6298027 8.49825764 4.21344995 50.20594406 8.56183243 4.1840992 50.013889313 8.56678486
		 4.15473413 49.82172394 8.57174492 4.12540483 49.6298027 8.5766964 3.32840109 50.22986221 8.95343399
		 3.31250095 50.034576416 8.95552254 3.29654408 49.83930588 8.95762253 3.28052592 49.6440506 8.95974445
		 2.38822794 50.028102875 9.44121361 2.41486907 49.83802032 9.43335915 2.44110203 49.64813995 9.42504597
		 2.46712399 49.45775223 9.4177227 1.50745201 49.75648117 9.59874725 1.55375195 49.56863022 9.59092999
		 1.60005403 49.38078308 9.58311176 1.64635396 49.19293594 9.57529259 1.49420905 49.75648117 9.52030945
		 1.54051006 49.56863022 9.51249027 1.58680904 49.38078308 9.50467396 1.633111 49.19293594 9.49685383
		 1.48096502 49.75648117 9.44187069 1.52726305 49.56863022 9.43405437 1.57356596 49.38078308 9.42623711
		 1.61986697 49.19293594 9.41841793 1.46771896 49.75648117 9.36343288 1.51402199 49.56863022 9.35561466
		 1.560323 49.38078308 9.34779835 1.60662198 49.19293594 9.33997917 2.34849405 50.028102875 9.20590115
		 2.3751359 49.83802032 9.19804382 2.401371 49.64813995 9.18972969 2.42739105 49.45775223 9.18240833
		 3.28867006 50.22986221 8.71811867 3.27277088 50.034576416 8.72021008 3.25681305 49.83930588 8.72231007
		 3.24079609 49.6440506 8.72442913 2.44063711 49.45775223 9.260849 3.25403595 49.6440506 8.80286503
		 2.45388007 49.45775223 9.33928585 3.26728201 49.6440506 8.88130474 2.36174107 50.028102875 9.28433895
		 3.30191493 50.22986221 8.79655743 2.37498498 50.028102875 9.36277485 3.31515908 50.22986221 8.87499619;
	setAttr -s 108 ".ed[0:107]"  0 1 0 1 5 1 5 4 1 4 0 0 1 2 0 2 6 1 6 5 1
		 2 3 0 3 7 0 7 6 1 5 9 1 9 8 1 8 4 0 6 10 1 10 9 1 7 11 0 11 10 1 9 13 1 13 12 0 12 8 0
		 10 14 1 14 13 0 11 15 0 15 14 0 13 17 1 17 16 1 16 12 0 14 18 1 18 17 1 15 19 0 19 18 1
		 17 21 1 21 20 1 20 16 0 18 22 1 22 21 1 19 23 0 23 22 1 21 25 1 25 24 0 24 20 0 22 26 1
		 26 25 0 23 27 0 27 26 0 25 29 1 29 28 1 28 24 0 26 30 1 30 29 1 27 31 0 31 30 1 29 33 1
		 33 32 1 32 28 0 30 34 1 34 33 1 31 35 0 35 34 1 33 37 1 37 36 0 36 32 0 34 38 1 38 37 0
		 35 39 0 39 38 0 37 41 1 41 40 1 40 36 0 38 42 1 42 41 1 39 43 0 43 42 1 41 45 1 45 44 1
		 44 40 0 42 46 1 46 45 1 43 47 0 47 46 1 45 1 1 0 44 0 46 2 1 47 3 0 35 48 1 48 43 1
		 48 49 1 49 47 1 49 7 1 31 50 1 50 48 1 50 51 1 51 49 1 51 11 1 23 50 1 19 51 1 40 52 1
		 52 32 1 44 53 1 53 52 1 4 53 1 52 54 1 54 28 1 53 55 1 55 54 1 8 55 1 54 20 1 55 16 1;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 -4 -3 -2 -1
		mu 0 4 0 1 2 3
		f 4 1 -7 -6 -5
		mu 0 4 3 2 4 5
		f 4 5 -10 -9 -8
		mu 0 4 5 4 6 7
		f 4 -13 -12 -11 2
		mu 0 4 1 8 9 2
		f 4 10 -15 -14 6
		mu 0 4 2 9 10 4
		f 4 13 -17 -16 9
		mu 0 4 4 10 11 6
		f 4 -20 -19 -18 11
		mu 0 4 8 12 13 9
		f 4 17 -22 -21 14
		mu 0 4 9 13 14 10
		f 4 20 -24 -23 16
		mu 0 4 10 14 15 11
		f 4 -27 -26 -25 18
		mu 0 4 16 17 18 19
		f 4 24 -29 -28 21
		mu 0 4 19 18 20 21
		f 4 27 -31 -30 23
		mu 0 4 21 20 22 15
		f 4 -34 -33 -32 25
		mu 0 4 17 23 24 18
		f 4 31 -36 -35 28
		mu 0 4 18 24 25 20
		f 4 34 -38 -37 30
		mu 0 4 20 25 26 22
		f 4 -41 -40 -39 32
		mu 0 4 23 27 28 24
		f 4 38 -43 -42 35
		mu 0 4 24 28 29 25
		f 4 41 -45 -44 37
		mu 0 4 25 29 30 26
		f 4 -48 -47 -46 39
		mu 0 4 31 32 33 34
		f 4 45 -50 -49 42
		mu 0 4 34 33 35 36
		f 4 48 -52 -51 44
		mu 0 4 36 35 37 30
		f 4 -55 -54 -53 46
		mu 0 4 32 38 39 33
		f 4 52 -57 -56 49
		mu 0 4 33 39 40 35
		f 4 55 -59 -58 51
		mu 0 4 35 40 41 37
		f 4 -62 -61 -60 53
		mu 0 4 38 42 43 39
		f 4 59 -64 -63 56
		mu 0 4 39 43 44 40
		f 4 62 -66 -65 58
		mu 0 4 40 44 45 41
		f 4 -69 -68 -67 60
		mu 0 4 46 47 48 49
		f 4 66 -71 -70 63
		mu 0 4 49 48 50 51
		f 4 69 -73 -72 65
		mu 0 4 51 50 52 45
		f 4 -76 -75 -74 67
		mu 0 4 47 53 54 48
		f 4 73 -78 -77 70
		mu 0 4 48 54 55 50
		f 4 76 -80 -79 72
		mu 0 4 50 55 56 52
		f 4 -82 0 -81 74
		mu 0 4 53 57 58 54
		f 4 80 4 -83 77
		mu 0 4 54 58 59 55
		f 4 82 7 -84 79
		mu 0 4 55 59 7 56
		f 4 -86 -85 64 71
		mu 0 4 52 60 41 45
		f 4 -88 -87 85 78
		mu 0 4 56 61 60 52
		f 4 8 -89 87 83
		mu 0 4 7 6 61 56
		f 4 -91 -90 57 84
		mu 0 4 60 62 37 41
		f 4 -93 -92 90 86
		mu 0 4 61 63 62 60
		f 4 15 -94 92 88
		mu 0 4 6 11 63 61
		f 4 -95 43 50 89
		mu 0 4 62 26 30 37
		f 4 -96 36 94 91
		mu 0 4 63 22 26 62
		f 4 22 29 95 93
		mu 0 4 11 15 22 63
		f 4 61 -98 -97 68
		mu 0 4 46 64 65 47
		f 4 96 -100 -99 75
		mu 0 4 47 65 66 53
		f 4 98 -101 3 81
		mu 0 4 53 66 67 57
		f 4 54 -103 -102 97
		mu 0 4 64 68 69 65
		f 4 101 -105 -104 99
		mu 0 4 65 69 70 66
		f 4 103 -106 12 100
		mu 0 4 66 70 71 67
		f 4 47 40 -107 102
		mu 0 4 68 72 73 69
		f 4 106 33 -108 104
		mu 0 4 69 73 74 70
		f 4 107 26 19 105
		mu 0 4 70 74 75 71;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_head";
	setAttr ".rp" -type "double3" 0.13521766662597656 48.667610168457031 5.0542886257171631 ;
	setAttr ".sp" -type "double3" 0.13521766662597656 48.667610168457031 5.0542886257171631 ;
createNode mesh -n "bear_headShape" -p "bear_head";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 155 ".uvst[0].uvsp[0:154]" -type "float2" 0.82635301 0.238479
		 0.84484398 0.247565 0.80874902 0.29883301 0.78910398 0.261884 0.86295801 0.25791499
		 0.83586103 0.31421 0.88008898 0.269319 0.87793797 0.31294 0.67591602 0.300883 0.75843501
		 0.360742 0.74670398 0.39728701 0.678666 0.36126199 0.80934602 0.386325 0.79206598
		 0.41925499 0.793118 0.96965599 0.77113098 0.977889 0.768116 0.96981502 0.79881501
		 0.96188003 0.70875198 0.47628 0.67082602 0.44755799 0.75248897 0.49744099 0.86251003
		 0.45233199 0.79828203 0.50879902 0.41831499 0.67266399 0.369688 0.664096 0.37279701
		 0.62166399 0.42989099 0.61920202 0.32026601 0.66442102 0.31671199 0.62204802 0.27172101
		 0.67355001 0.25958899 0.62025201 0.44064999 0.56671202 0.375644 0.56593502 0.374286
		 0.515266 0.43788099 0.51356 0.31337801 0.56643403 0.314284 0.51592499 0.248375 0.56804001
		 0.25065699 0.51526701 0.36967999 0.47308001 0.42137799 0.470128 0.318398 0.473932
		 0.26666 0.47234899 0.41244501 0.43025401 0.36612201 0.43046799 0.38566199 0.36727399
		 0.41999099 0.36603901 0.32094601 0.43152699 0.315911 0.36830699 0.274883 0.43364599
		 0.26264101 0.37066501 0.384491 0.31425399 0.419278 0.311492 0.31394801 0.31654799
		 0.25951499 0.316531 0.35337001 0.270327 0.38589701 0.25931299 0.32213399 0.27134401
		 0.28892401 0.262348 0.41585001 0.21366701 0.36037499 0.200312 0.38731799 0.156937
		 0.476807 0.174742 0.31012201 0.20198201 0.28011799 0.160616 0.25575599 0.219037 0.192113
		 0.184671 0.39073601 0.096450001 0.45166701 0.103567 0.272403 0.100545 0.21212 0.111924
		 0.436896 0.043060999 0.383564 0.039088 0.37252101 0.0057970001 0.435256 0.0076819998
		 0.27551001 0.042831 0.28420001 0.0088539999 0.222582 0.050524998 0.221743 0.015112
		 0.180874 0.23628999 0.21084701 0.299218 0.162801 0.28858599 0.109477 0.211321 0.114999
		 0.29503599 0.013813 0.21048599 0.048448998 0.205052 0.066087998 0.309358 0.0057970001
		 0.32672501 0.215279 0.38525799 0.170488 0.39278099 0.121166 0.36481801 0.074101999
		 0.365596 0.0060879998 0.388699 0.24107 0.462717 0.192256 0.48069999 0.13501801 0.48161599
		 0.079457 0.48608401 0.024834 0.47510001 0.49186999 0.225237 0.51396298 0.27596799
		 0.46661299 0.29047799 0.56115198 0.194469 0.56251901 0.27831399 0.62171501 0.1831
		 0.65685701 0.18549301 0.613051 0.288331 0.51465702 0.38049299 0.46883601 0.37678599
		 0.56191599 0.34833401 0.609182 0.344789 0.500871 0.47002599 0.44970101 0.45623401
		 0.55904502 0.46591899 0.61608899 0.46514899 0.80376601 0.123465 0.87128699 0.13059901
		 0.86122102 0.19538 0.81606901 0.165838 0.73268598 0.304995 0.77862298 0.328217 0.70164502
		 0.18302999 0.72878897 0.232279 0.95140803 0.17692401 0.90024197 0.217914 0.82585299
		 0.35361901 0.99125302 0.23193599 0.94836599 0.24232601 0.990219 0.350178 0.93392497
		 0.35104999 0.901618 0.42668501 0.86941499 0.38125199 0.81943899 0.209353 0.764166
		 0.24690101 0.76180202 0.27594799 0.798244 0.30924201 0.83210701 0.32797799 0.87942201
		 0.342547 0.90351498 0.32752401 0.90888798 0.260968 0.87692201 0.24226999 0.85161799
		 0.227705 0.87121999 0.82970202 0.967134 0.80597401 0.88894898 0.853872 0.90989798
		 0.843813 0.85133499 0.93333298 0.83010697 0.98881501 0.92763603 0.89946097 0.85560399
		 0.92697501 0.89250398 0.87772697 0.86659801 0.88532501 0.83505797 0.85549998 0.82892501
		 0.81858999 0.86932701 0.81154197 0.871674 0.80355;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 244 ".vt";
	setAttr ".vt[0:165]"  -3.62198496 42.40184784 7.62927008 -1.47448206 40.947052 7.64252615
		 2.13505697 40.947052 7.64252615 4.28256416 42.40184784 7.62927008 -4.62700987 47.059753418 8.66557217
		 -0.809071 48.48849106 9.59270191 1.46964705 48.48849106 9.59270191 5.28758717 47.059753418 8.66557217
		 -3.849509 49.44572449 8.78936672 -0.75238901 50.059440613 9.86735439 1.41296196 50.059440613 9.86735439
		 4.51008606 49.44572449 8.78936672 -2.70130706 52.88076782 8.26881313 -0.69137299 53.25577164 7.96332884
		 1.35194802 53.25577164 7.96332884 3.36188197 52.88076782 8.26881313 -3.64355612 53.7389679 4.025265217
		 -0.98668897 54.45624924 4.0070271492 1.647264 54.45624924 4.0070271492 4.30413103 53.7389679 4.025265217
		 -3.60073805 53.97964859 1.89344299 -0.96116298 54.61458588 1.83337498 1.62173903 54.61458588 1.83337498
		 4.26131392 53.97964859 1.89344299 -2.28376794 52.14920807 -0.619183 -0.54106498 52.17361832 -0.85763103
		 1.20163798 52.17361832 -0.85763103 2.94434404 52.14920807 -0.619183 -3.10099196 49.73489761 -1.37454295
		 -1.59123003 49.73489761 -1.89566898 1.47404802 49.73489761 -2.16412902 3.76156712 49.73489761 -1.37454295
		 -3.10099196 47.44738007 -1.37454295 -1.59123003 47.44738007 -1.89566898 1.47404802 47.44738007 -2.16412902
		 3.76156712 47.44738007 -1.37454295 -1.77416098 45.13894272 -1.69814801 -0.37119299 45.4192009 -2.20058393
		 1.031769991 45.4192009 -2.20058393 2.43473411 45.13894272 -1.69814801 -5.15431786 44.10963058 1.15830898
		 -1.96616697 41.88098526 1.17072999 2.62673998 41.88098526 1.17072999 5.81489182 44.10963058 1.15830898
		 -4.16967583 43.032855988 3.77657604 -2.15464497 41.42626572 3.78364396 2.81521893 41.42626572 3.78364396
		 4.83025408 43.032855988 3.77657604 6.4906292 46.22083282 1.97080505 6.46673822 46.22946548 4.053399086
		 6.4906292 50.73278427 1.97080505 6.46850014 49.24108887 4.052708149 -5.8300519 46.22083282 1.97080505
		 -5.80616188 46.22946548 4.053399086 -5.8300519 50.73278427 1.97080505 -5.80792522 49.24108887 4.052708149
		 -3.23973489 53.68365097 6.19067621 -5.1355958 48.98313141 6.023650169 -5.1346879 46.63033676 6.088323116
		 -3.7091341 42.41312027 6.1685071 -1.96945906 40.94572449 6.22379923 2.63003397 40.94572449 6.22379923
		 4.36971092 42.41312027 6.1685071 5.79526281 46.63033676 6.088323116 5.7961731 48.98313141 6.023650169
		 3.90031099 53.68365097 6.19067621 1.51592398 54.055511475 6.35161495 -0.85535002 54.055511475 6.35161495
		 3.50356889 53.27865601 0.35057801 1.37629998 53.60535812 0.32388201 -0.715725 53.60535812 0.32388201
		 -2.84299397 53.27865601 0.35057801 -4.57867384 50.33948898 0.267441 -4.57715416 46.78367996 0.267441
		 -2.69377303 43.64168167 -0.38960201 -0.66801602 42.71884918 -0.57273799 1.32859099 42.71884918 -0.57273799
		 3.35434794 43.64168167 -0.38960201 5.23773003 46.78367996 0.267441 5.23925114 50.33948898 0.267441
		 -1.05952704 43.68160629 11.79500866 -0.132984 43.60797119 11.8554039 -0.336622 46.3278923 12.30916119
		 -1.66715801 45.38793564 11.77056122 0.79356003 43.60797119 11.8554039 0.99719501 46.3278923 12.30916119
		 1.72010303 43.68160629 11.79500866 2.32773399 45.38793564 11.77056122 -2.8095789 42.46319962 9.39399147
		 -2.724576 47.090358734 10.07983017 -0.809071 47.66585922 10.96519375 1.46964705 47.66585922 10.96519375
		 3.38515091 47.090358734 10.07983017 3.470155 42.46319962 9.39399147 1.34566903 41.79312897 10.22915554
		 -0.68509501 41.79312897 10.22915554 -2.25328302 45.99208832 11.096043587 -0.477005 46.83747482 11.7772913
		 1.13758004 46.83747482 11.7772913 2.91385794 45.99208832 11.096043587 2.53312111 43.18826294 10.86975384
		 0.97040701 42.90061569 11.20685101 -0.30983201 42.90061569 11.20685101 -1.872545 43.18826294 10.86975384
		 2.90920305 48.72124863 8.6310463 1.99952805 49.11910629 8.62926292 1.85355902 48.78347778 8.64367008
		 3.13247705 48.38049698 8.64535522 -4.80640221 48.98691559 2.89414096 -1.613819 53.4822731 3.3383379
		 -7.95883512 50.30711365 2.95087004 -3.9146781 56.016296387 3.3383379 -7.082129002 51.053554535 1.856933
		 -3.3289659 55.82442093 1.87700498 -4.51060486 49.71966553 2.019808054 -1.37318397 53.70016479 2.19383311
		 -3.098510027 55.12982559 3.3383379 -6.84466124 49.8531723 2.95087004 -6.38067722 50.76775742 1.856933
		 -2.81513 55.26633072 1.87700498 -2.34745407 54.31406403 3.3383379 -5.8193779 49.43544388 2.95087004
		 -5.23852396 50.30241776 1.856933 -1.97846603 54.35758972 1.87700498 -6.061272144 55.50986862 3.3383379
		 -5.99875593 55.60172653 1.87700498 -4.56629896 54.045864105 1.398507 -3.53439498 52.9250679 1.398507
		 -2.927809 52.27245712 2.21020699 -3.01631093 52.20036697 3.38291407 -8.020442009 53.40091324 4.015853882
		 -7.6935811 53.9052887 2.45409894 -6.077123165 52.20935059 1.25069106 -4.58877897 52.19058228 1.57512105
		 -4.13794279 50.89188004 3.51493692 -4.46910095 50.35483932 4.36156988 -8.23635769 54.38042831 3.082386971
		 -8.068369865 50.65234375 2.31399393 -6.97258806 50.20588684 2.31399393 -5.83043098 49.74053574 2.31399393
		 -4.68828011 49.27518845 2.31399393 -4.31537914 50.49324799 3.9942131 -2.92994905 52.27368164 2.79688406
		 -1.37318397 53.70016479 2.7660861 -2.20985103 54.60890961 2.7660861 -3.046514988 55.51764679 2.7660861
		 -3.84921288 56.38949585 2.7660861 -6.46416712 56.10722351 2.7660861 -4.92070103 49.46555328 3.65260911
		 -4.60434103 50.74550247 4.51019621 -5.69392681 49.63529587 3.42241406 -6.41721582 49.92999649 3.42241406
		 -7.56365299 53.48861313 4.38308716 -7.71710396 50.42194748 3.44384909 -3.453264 52.22024536 3.63565493
		 -2.42344594 53.12321854 3.37632108 -2.73244596 53.90201187 3.37632108 -3.26227999 54.47748947 3.37632108
		 -6.03907299 55.072952271 3.75867105 -4.20500994 55.50142288 3.4180069 -4.95033312 50.29292679 3.64263201
		 -4.65568399 51.27385712 4.164958 -5.16462708 51.71663284 2.042954922 -5.56170321 50.43429565 3.21475196
		 -6.09247303 51.89632034 1.50982904 -6.53640223 51.1007843 2.4014411;
	setAttr ".vt[166:243]" -6.93946409 53.22005844 3.789994 -7.56381702 50.96768188 3.084110975
		 -3.77456689 52.41999054 3.23438311 -4.13877296 52.91157532 2.053067923 -2.92943096 53.11292648 2.70709491
		 -3.037369013 53.85787201 2.57486796 -5.16627502 53.67155075 1.50650203 -3.74409699 54.62548065 2.24205208
		 -5.90274906 54.70595551 2.71675205 -4.46253395 55.4058075 2.8941071 2.24071908 53.93017578 3.71544194
		 5.97300196 48.36981583 2.58916903 5.10142612 55.70177078 3.71544194 7.88538694 50.57988739 1.57317102
		 4.66246223 56.23075104 2.27905607 6.87961817 50.59671783 0.33872399 2.09551692 54.53041458 2.27905607
		 4.85009623 48.7872467 1.78345001 8.39604759 52.60391998 1.79761803 6.7846961 52.77891541 0.478504
		 3.80247688 50.095779419 2.704036 5.46757603 50.2336998 3.92132998 7.10238791 55.26337433 3.73887897
		 6.67369318 55.031047821 1.70678902 3.4104569 52.69419479 1.70678902 3.72535706 53.20375061 3.11265111
		 3.039515018 55.20641708 2.27905607 2.98038912 55.052383423 3.71544194 6.57719898 48.88048172 2.13989997
		 5.62120104 49.43897629 1.21007204 5.17159224 51.22141647 1.72126305 4.60830784 53.55199432 1.33084595
		 3.98351192 55.88242722 2.27905607 3.83080912 55.54191589 3.71544194 7.21356297 49.49990082 1.72249603
		 6.39231205 50.090698242 0.63669097 6.24082279 51.98711777 1.057091951 5.80615807 54.40978241 1.33084595
		 7.64039612 50.83819199 0.73765701 8.50679302 52.87432861 0.61450303 7.48473215 56.024925232 2.86767197
		 5.22622299 56.2057457 2.97027612 3.73911905 55.96429825 2.96004295 2.80040503 55.26039124 2.94321394
		 1.85511601 53.98094559 2.93799996 3.26346397 52.94141769 2.79647112 5.069219112 50.24741745 3.77245307
		 5.24449921 48.31175232 2.11843204 6.025379181 48.95969009 1.53889203 6.82004499 49.59277725 0.95180601
		 3.93345308 54.87486267 3.76702905 6.69812918 54.71113205 3.79463005 5.11270523 55.07302475 3.76702905
		 7.89945507 52.42844009 2.23818803 6.97397995 49.75128555 2.30349898 7.62179995 50.74016953 2.12358689
		 2.69494009 54.03730011 3.76702905 3.9800539 53.051704407 3.16840196 3.28673792 54.46109772 3.76702905
		 5.46418095 50.52726364 3.99135089 6.4698019 49.2914505 2.65534496 5.98638678 48.88288879 3.014799118
		 4.30238819 54.49449921 3.082612991 5.89270115 54.2478714 2.58656812 6.84183311 54.63130569 3.32857895
		 5.23810196 54.71986771 3.082612991 6.77183723 52.14385223 1.47367799 7.70426178 52.86307144 2.32464004
		 6.45623207 50.44129562 1.75583005 7.0035190582 51.20753479 2.033245087 3.16202497 53.80794144 3.082612991
		 4.6145339 53.2734375 1.90224004 5.51697016 53.7183609 1.83448303 3.65097404 54.15808487 3.082612991
		 6.59749699 51.58301544 1.63109601 5.84967899 51.30984116 1.99209201 5.987607 49.95641708 2.043553114
		 5.5882082 49.61884689 2.81725311;
	setAttr -s 212 ".ed";
	setAttr ".ed[0:165]"  80 81 0 81 82 1 82 83 0 83 80 0 81 84 0 84 85 1 85 82 0
		 84 86 0 86 87 0 87 85 0 4 5 0 5 9 1 9 8 1 8 4 0 5 6 0 6 10 0 10 9 1 104 105 0 105 106 0
		 106 107 0 107 104 0 9 13 1 13 12 0 12 8 0 10 14 1 14 13 0 10 11 0 11 15 0 15 14 0
		 13 67 1 67 56 1 56 12 0 14 66 1 66 67 1 15 65 0 65 66 1 16 17 1 17 21 1 21 20 1 20 16 0
		 17 18 1 18 22 1 22 21 1 18 19 1 19 23 0 23 22 1 21 70 1 70 71 1 71 20 0 22 69 1 69 70 1
		 23 68 0 68 69 1 24 25 0 25 29 1 29 28 1 28 24 0 25 26 0 26 30 1 30 29 1 26 27 0 27 31 0
		 31 30 1 29 33 1 33 32 1 32 28 0 30 34 1 34 33 1 31 35 0 35 34 1 33 37 1 37 36 0 36 32 0
		 34 38 1 38 37 0 35 39 0 39 38 0 74 75 1 75 41 1 41 40 1 40 74 0 75 76 1 76 42 1 42 41 1
		 76 77 1 77 43 0 43 42 1 41 45 1 45 44 1 44 40 0 42 46 1 46 45 1 43 47 0 47 46 1 59 60 1
		 60 1 1 1 0 1 0 59 0 60 61 1 61 2 1 2 1 1 61 62 1 62 3 0 3 2 1 77 78 1 78 48 1 48 43 1
		 48 49 1 49 47 1 62 63 1 63 7 1 7 3 0 78 79 1 79 50 1 50 48 1 50 51 1 51 49 1 63 64 1
		 64 11 1 11 7 0 79 68 1 23 50 1 19 51 1 64 65 1 40 52 1 52 73 1 73 74 1 44 53 1 53 52 1
		 0 4 0 4 58 1 58 59 1 52 54 1 54 72 1 72 73 1 53 55 1 55 54 1 8 57 1 57 58 1 54 20 1
		 71 72 1 55 16 1 56 57 1 55 57 1 56 16 0 53 58 1 44 59 0 45 60 1 46 61 1 47 62 0 49 63 1
		 51 64 1 19 65 0 18 66 1 17 67 1 68 27 0 26 69 1 25 70 1 24 71 0 28 72 1 32 73 1 36 74 0
		 37 75 1 38 76 1 39 77 0 35 78 1;
	setAttr ".ed[166:211]" 31 79 1 1 95 1 95 88 1 88 0 0 4 89 0 89 90 1 90 5 1
		 88 89 1 2 94 1 94 95 1 90 91 1 91 6 1 3 93 0 93 94 1 7 92 0 92 93 1 7 6 0 91 92 1
		 88 103 0 103 96 1 96 89 0 96 97 1 97 90 1 97 98 1 98 91 1 98 99 1 99 92 0 99 100 1
		 100 93 0 100 101 1 101 94 1 101 102 1 102 95 1 102 103 1 96 83 0 82 97 1 85 98 1
		 87 99 0 86 100 0 84 101 1 81 102 1 80 103 0 7 105 0 104 6 0 11 106 0 10 107 0;
	setAttr -s 106 -ch 424 ".fc[0:105]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -2
		mu 0 4 1 4 5 2
		f 4 7 8 9 -6
		mu 0 4 4 6 7 5
		f 4 10 11 12 13
		mu 0 4 8 9 10 11
		f 4 14 15 16 -12
		mu 0 4 9 12 13 10
		f 4 17 18 19 20
		mu 0 4 14 15 16 17
		f 4 -13 21 22 23
		mu 0 4 11 10 18 19
		f 4 -17 24 25 -22
		mu 0 4 10 13 20 18
		f 4 26 27 28 -25
		mu 0 4 13 21 22 20
		f 4 -23 29 30 31
		mu 0 4 23 24 25 26
		f 4 -26 32 33 -30
		mu 0 4 24 27 28 25
		f 4 -29 34 35 -33
		mu 0 4 27 29 30 28
		f 4 36 37 38 39
		mu 0 4 31 32 33 34
		f 4 40 41 42 -38
		mu 0 4 32 35 36 33
		f 4 43 44 45 -42
		mu 0 4 35 37 38 36
		f 4 -39 46 47 48
		mu 0 4 34 33 39 40
		f 4 -43 49 50 -47
		mu 0 4 33 36 41 39
		f 4 -46 51 52 -50
		mu 0 4 36 38 42 41
		f 4 53 54 55 56
		mu 0 4 43 44 45 46
		f 4 57 58 59 -55
		mu 0 4 44 47 48 45
		f 4 60 61 62 -59
		mu 0 4 47 49 50 48
		f 4 -56 63 64 65
		mu 0 4 46 45 51 52
		f 4 -60 66 67 -64
		mu 0 4 45 48 53 51
		f 4 -63 68 69 -67
		mu 0 4 48 50 54 53
		f 4 -65 70 71 72
		mu 0 4 52 51 55 56
		f 4 -68 73 74 -71
		mu 0 4 51 53 57 55
		f 4 -70 75 76 -74
		mu 0 4 53 54 58 57
		f 4 77 78 79 80
		mu 0 4 59 60 61 62
		f 4 81 82 83 -79
		mu 0 4 60 63 64 61
		f 4 84 85 86 -83
		mu 0 4 63 65 66 64
		f 4 -80 87 88 89
		mu 0 4 62 61 67 68
		f 4 -84 90 91 -88
		mu 0 4 61 64 69 67
		f 4 -87 92 93 -91
		mu 0 4 64 66 70 69
		f 4 94 95 96 97
		mu 0 4 71 72 73 74
		f 4 98 99 100 -96
		mu 0 4 72 75 76 73
		f 4 101 102 103 -100
		mu 0 4 75 77 78 76
		f 4 -86 104 105 106
		mu 0 4 79 65 80 81
		f 4 -93 -107 107 108
		mu 0 4 82 79 81 83
		f 4 -103 109 110 111
		mu 0 4 84 85 86 87
		f 4 -106 112 113 114
		mu 0 4 81 80 88 89
		f 4 -108 -115 115 116
		mu 0 4 83 81 89 90
		f 4 -111 117 118 119
		mu 0 4 87 86 91 92
		f 4 -114 120 -52 121
		mu 0 4 89 88 93 94
		f 4 -116 -122 -45 122
		mu 0 4 90 89 94 95
		f 4 -119 123 -35 -28
		mu 0 4 92 91 96 97
		f 4 -81 124 125 126
		mu 0 4 59 98 99 100
		f 4 -90 127 128 -125
		mu 0 4 98 101 102 99
		f 4 -98 129 130 131
		mu 0 4 103 104 8 105
		f 4 -126 132 133 134
		mu 0 4 100 99 106 107
		f 4 -129 135 136 -133
		mu 0 4 99 102 108 106
		f 4 -131 -14 137 138
		mu 0 4 105 8 11 109
		f 4 -134 139 -49 140
		mu 0 4 107 106 110 111
		f 4 -137 141 -40 -140
		mu 0 4 106 108 112 110
		f 4 -138 -24 -32 142
		mu 0 4 109 11 19 113
		f 4 143 -143 144 -142
		mu 0 4 108 109 113 112
		f 4 145 -139 -144 -136
		mu 0 4 102 105 109 108
		f 4 146 -132 -146 -128
		mu 0 4 101 103 105 102
		f 4 -89 147 -95 -147
		mu 0 4 68 67 72 71
		f 4 -92 148 -99 -148
		mu 0 4 67 69 75 72
		f 4 -94 149 -102 -149
		mu 0 4 69 70 77 75
		f 4 -110 -150 -109 150
		mu 0 4 86 85 82 83
		f 4 -118 -151 -117 151
		mu 0 4 91 86 83 90
		f 4 -124 -152 -123 152
		mu 0 4 96 91 90 95
		f 4 -36 -153 -44 153
		mu 0 4 28 30 37 35
		f 4 -34 -154 -41 154
		mu 0 4 25 28 35 32
		f 4 -31 -155 -37 -145
		mu 0 4 26 25 32 31
		f 4 -53 155 -61 156
		mu 0 4 41 42 49 47
		f 4 -51 -157 -58 157
		mu 0 4 39 41 47 44
		f 4 -48 -158 -54 158
		mu 0 4 40 39 44 43
		f 4 159 -141 -159 -57
		mu 0 4 46 107 111 43
		f 4 160 -135 -160 -66
		mu 0 4 52 100 107 46
		f 4 161 -127 -161 -73
		mu 0 4 56 59 100 52
		f 4 -72 162 -78 -162
		mu 0 4 56 55 60 59
		f 4 -75 163 -82 -163
		mu 0 4 55 57 63 60
		f 4 -77 164 -85 -164
		mu 0 4 57 58 65 63
		f 4 -105 -165 -76 165
		mu 0 4 80 65 58 54
		f 4 -113 -166 -69 166
		mu 0 4 88 80 54 50
		f 4 -121 -167 -62 -156
		mu 0 4 93 88 50 49
		f 4 -97 167 168 169
		mu 0 4 114 115 116 117
		f 4 -11 170 171 172
		mu 0 4 9 8 118 119
		f 4 -130 -170 173 -171
		mu 0 4 120 114 117 121
		f 4 -101 174 175 -168
		mu 0 4 115 122 123 116
		f 4 -15 -173 176 177
		mu 0 4 12 9 119 124
		f 4 -104 178 179 -175
		mu 0 4 122 125 126 123
		f 4 -112 180 181 -179
		mu 0 4 125 127 128 126
		f 4 182 -178 183 -181
		mu 0 4 129 12 124 130
		f 4 -174 184 185 186
		mu 0 4 121 117 131 132
		f 4 -172 -187 187 188
		mu 0 4 119 118 133 134
		f 4 -177 -189 189 190
		mu 0 4 124 119 134 135
		f 4 -184 -191 191 192
		mu 0 4 130 124 135 136
		f 4 -182 -193 193 194
		mu 0 4 126 128 137 138
		f 4 -180 -195 195 196
		mu 0 4 123 126 138 139
		f 4 -176 -197 197 198
		mu 0 4 116 123 139 140
		f 4 -169 -199 199 -185
		mu 0 4 117 116 140 131
		f 4 -188 200 -3 201
		mu 0 4 134 133 3 2
		f 4 -190 -202 -7 202
		mu 0 4 135 134 2 5
		f 4 -192 -203 -10 203
		mu 0 4 136 135 5 7
		f 4 -194 -204 -9 204
		mu 0 4 138 137 7 6
		f 4 -196 -205 -8 205
		mu 0 4 139 138 6 4
		f 4 -198 -206 -5 206
		mu 0 4 140 139 4 1
		f 4 -200 -207 -1 207
		mu 0 4 131 140 1 0
		f 4 -186 -208 -4 -201
		mu 0 4 132 131 0 3
		f 4 -183 208 -18 209
		mu 0 4 141 142 143 144
		f 4 -120 210 -19 -209
		mu 0 4 145 146 16 15
		f 4 -27 211 -20 -211
		mu 0 4 147 148 149 150
		f 4 -16 -210 -21 -212
		mu 0 4 151 152 153 154;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_earR";
	setAttr ".rp" -type "double3" -4.804770827293396 52.688205718994141 2.8804436326026917 ;
	setAttr ".sp" -type "double3" -4.804770827293396 52.688205718994141 2.8804436326026917 ;
createNode mesh -n "bear_earRShape" -p "bear_earR";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 105 ".uvst[0].uvsp[0:104]" -type "float2" 0.344859 0.81076002
		 0.37095401 0.79837799 0.378755 0.84640598 0.341351 0.82982099 0.64940798 0.63683999
		 0.69587702 0.701662 0.67707199 0.71924001 0.63131601 0.64647901 0.55738199 0.68077302
		 0.56741399 0.72864097 0.51305902 0.71184701 0.538517 0.67496002 0.53078002 0.66456503
		 0.49981499 0.70326298 0.49034101 0.69939202 0.51637501 0.65990001 0.77849102 0.87772697
		 0.793863 0.88011599 0.78926402 0.90896302 0.77462697 0.90366602 0.533997 0.663113
		 0.53046203 0.64832997 0.55585903 0.64215797 0.56240398 0.657924 0.349388 0.86574602
		 0.37238899 0.86520499 0.375332 0.925834 0.330172 0.88789999 0.59067398 0.65143901
		 0.58124501 0.63649601 0.60877299 0.62964898 0.61779398 0.64409399 0.60663599 0.66835099
		 0.65727103 0.715976 0.59720701 0.70874399 0.58755898 0.67366701 0.77049398 0.92955601
		 0.78465801 0.93781 0.78022301 0.96548498 0.76598698 0.95769 0.686131 0.77316701 0.66754103
		 0.82357901 0.64598101 0.82460099 0.668935 0.77071702 0.597619 0.76519501 0.64591402
		 0.77013499 0.61136597 0.82069302 0.58913702 0.81689602 0.56333101 0.76078999 0.55807602
		 0.81211197 0.53590202 0.76045102 0.53566998 0.81049699 0.50812602 0.75669903 0.52204102
		 0.75970501 0.52218097 0.80986202 0.50896901 0.80205297 0.41501901 0.84346801 0.41155699
		 0.813945 0.43922701 0.82500201 0.44991499 0.83880103 0.411309 0.87307101 0.448991
		 0.86453998 0.42246199 0.90884101 0.45656499 0.89221901 0.80722702 0.88224697 0.81129903
		 0.90429699 0.806696 0.93314499 0.979949 0.79710102 0.98956102 0.81657898 0.96811599
		 0.823654 0.48225001 0.70508498 0.499881 0.67247099 0.533216 0.62725699 0.55233198
		 0.63009501 0.66447401 0.63816297 0.70687199 0.705814 0.57003999 0.62625802 0.60180098
		 0.61844599 0.50192899 0.78270602 0.49803799 0.75078899 0.94680798 0.93844903 0.923217
		 0.93821102 0.92346698 0.93549901 0.938703 0.92002201 0.94877303 0.92334503 0.86258698
		 0.88701397 0.83696699 0.89947999 0.83495998 0.885526 0.86461502 0.87121999 0.69849801
		 0.76865 0.68159401 0.81509602 0.32603601 0.80549097 0.36191401 0.78849399 0.32237199
		 0.82624602 0.37072599 0.94451302 0.31566301 0.88654798 0.318239 0.84848499 0.41321799
		 0.80128998 0.45033199 0.80936003 0.46680799 0.82911301 0.47407299 0.85463798 0.46870899
		 0.89520198 0.42977199 0.93115401 0.979949 0.79710102 0.94680798 0.93844903;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 68 ".vt[0:67]"  -4.95033312 50.29292679 3.64263201 -4.65568399 51.27385712 4.164958
		 -5.16462708 51.71663284 2.042954922 -5.56170321 50.43429565 3.21475196 -7.95883512 50.30711365 2.95087004
		 -8.020442009 53.40091324 4.015853882 -8.23635769 54.38042831 3.082386971 -8.068369865 50.65234375 2.31399393
		 -5.23852396 50.30241776 1.856933 -4.58877897 52.19058228 1.57512105 -4.13794279 50.89188004 3.51493692
		 -4.51060486 49.71966553 2.019808054 -4.68828011 49.27518845 2.31399393 -4.31537914 50.49324799 3.9942131
		 -4.46910095 50.35483932 4.36156988 -4.80640221 48.98691559 2.89414096 -1.613819 53.4822731 3.3383379
		 -1.37318397 53.70016479 2.7660861 -2.20985103 54.60890961 2.7660861 -2.34745407 54.31406403 3.3383379
		 -5.8193779 49.43544388 2.95087004 -5.83043098 49.74053574 2.31399393 -6.53640223 51.1007843 2.4014411
		 -6.09247303 51.89632034 1.50982904 -6.93946409 53.22005844 3.789994 -7.56381702 50.96768188 3.084110975
		 -6.97258806 50.20588684 2.31399393 -6.84466124 49.8531723 2.95087004 -7.082129002 51.053554535 1.856933
		 -7.6935811 53.9052887 2.45409894 -6.077123165 52.20935059 1.25069106 -6.38067722 50.76775742 1.856933
		 -3.098510027 55.12982559 3.3383379 -3.046514988 55.51764679 2.7660861 -3.84921288 56.38949585 2.7660861
		 -3.9146781 56.016296387 3.3383379 -6.061272144 55.50986862 3.3383379 -6.46416712 56.10722351 2.7660861
		 -4.56629896 54.045864105 1.398507 -5.99875593 55.60172653 1.87700498 -3.3289659 55.82442093 1.87700498
		 -2.81513 55.26633072 1.87700498 -3.53439498 52.9250679 1.398507 -1.97846603 54.35758972 1.87700498
		 -2.927809 52.27245712 2.21020699 -1.37318397 53.70016479 2.19383311 -3.01631093 52.20036697 3.38291407
		 -2.92994905 52.27368164 2.79688406 -4.13877296 52.91157532 2.053067923 -3.77456689 52.41999054 3.23438311
		 -2.92943096 53.11292648 2.70709491 -3.037369013 53.85787201 2.57486796 -5.16627502 53.67155075 1.50650203
		 -3.74409699 54.62548065 2.24205208 -5.90274906 54.70595551 2.71675205 -4.46253395 55.4058075 2.8941071
		 -4.60434103 50.74550247 4.51019621 -4.92070103 49.46555328 3.65260911 -5.69392681 49.63529587 3.42241406
		 -7.71710396 50.42194748 3.44384909 -7.56365299 53.48861313 4.38308716 -6.41721582 49.92999649 3.42241406
		 -2.42344594 53.12321854 3.37632108 -3.453264 52.22024536 3.63565493 -2.73244596 53.90201187 3.37632108
		 -3.26227999 54.47748947 3.37632108 -4.20500994 55.50142288 3.4180069 -6.03907299 55.072952271 3.75867105;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 1 2 3 1 3 0 0 4 5 0 5 6 1 6 7 1
		 7 4 0 8 9 1 9 10 1 10 11 0 11 8 0 12 13 1 13 14 1 14 15 0 15 12 0 16 17 0 17 18 1
		 18 19 1 19 16 0 15 20 0 20 21 1 21 12 1 22 23 1 23 24 1 24 25 0 25 22 0 26 27 1 27 4 0
		 7 26 1 28 29 0 29 30 1 30 31 1 31 28 0 32 33 1 33 34 1 34 35 0 35 32 0 2 23 1 22 3 0
		 20 27 0 26 21 1 30 9 1 8 31 0 18 33 1 32 19 0 36 35 0 34 37 1 37 36 1 38 39 1 39 40 0
		 40 41 0 41 38 1 42 38 1 41 43 0 43 42 1 44 42 1 43 45 0 45 44 0 46 47 1 47 17 1 16 46 0
		 48 49 1 49 50 0 50 51 0 51 48 1 52 48 1 51 53 0 53 52 1 54 52 1 53 55 0 55 54 0 5 36 0
		 37 6 1 29 39 0 38 30 1 42 9 1 44 10 0 13 47 1 46 14 0 1 49 0 48 2 1 52 23 1 54 24 0
		 6 29 1 28 7 0 31 26 1 8 21 1 11 12 0 10 13 1 44 47 1 45 17 0 43 18 1 41 33 1 40 34 0
		 39 37 1 14 56 0 56 57 0 57 15 0 57 58 0 58 20 0 4 59 0 59 60 0 60 5 0 27 61 0 61 59 0
		 58 61 0 16 62 0 62 63 0 63 46 0 19 64 0 64 62 0 32 65 0 65 64 0 35 66 0 66 65 0 36 67 0
		 67 66 0 63 56 0 60 67 0 56 1 1 0 57 0 3 58 1 59 25 0 24 60 0 61 22 1 62 50 0 49 63 1
		 64 51 1 65 53 1 66 55 0 67 54 0;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 7
		mu 0 4 4 5 6 7
		f 4 8 9 10 11
		mu 0 4 8 9 10 11
		f 4 12 13 14 15
		mu 0 4 12 13 14 15
		f 4 16 17 18 19
		mu 0 4 16 17 18 19
		f 4 -16 20 21 22
		mu 0 4 20 21 22 23
		f 4 23 24 25 26
		mu 0 4 24 25 26 27
		f 4 27 28 -8 29
		mu 0 4 28 29 30 31
		f 4 30 31 32 33
		mu 0 4 32 33 34 35
		f 4 34 35 36 37
		mu 0 4 36 37 38 39
		f 4 -3 38 -24 39
		mu 0 4 3 2 25 24
		f 4 -22 40 -28 41
		mu 0 4 23 22 29 28
		f 4 -33 42 -9 43
		mu 0 4 35 34 9 8
		f 4 -19 44 -35 45
		mu 0 4 19 18 37 36
		f 4 46 -37 47 48
		mu 0 4 40 41 42 43
		f 4 49 50 51 52
		mu 0 4 44 45 46 47
		f 4 53 -53 54 55
		mu 0 4 48 44 47 49
		f 4 56 -56 57 58
		mu 0 4 50 48 49 51
		f 4 59 60 -17 61
		mu 0 4 52 53 54 55
		f 4 62 63 64 65
		mu 0 4 56 57 58 59
		f 4 66 -66 67 68
		mu 0 4 60 56 59 61
		f 4 69 -69 70 71
		mu 0 4 62 60 61 63
		f 4 72 -49 73 -6
		mu 0 4 5 40 43 6
		f 4 -32 74 -50 75
		mu 0 4 34 33 45 44
		f 4 -43 -76 -54 76
		mu 0 4 9 34 44 48
		f 4 -10 -77 -57 77
		mu 0 4 10 9 48 50
		f 4 -14 78 -60 79
		mu 0 4 14 13 53 52
		f 4 -2 80 -63 81
		mu 0 4 2 1 57 56
		f 4 -39 -82 -67 82
		mu 0 4 25 2 56 60
		f 4 -25 -83 -70 83
		mu 0 4 26 25 60 62
		f 4 -7 84 -31 85
		mu 0 4 7 6 33 32
		f 4 86 -30 -86 -34
		mu 0 4 35 28 31 32
		f 4 87 -42 -87 -44
		mu 0 4 8 23 28 35
		f 4 88 -23 -88 -12
		mu 0 4 11 20 23 8
		f 4 -11 89 -13 -89
		mu 0 4 11 10 13 12
		f 4 -79 -90 -78 90
		mu 0 4 53 13 10 50
		f 4 -61 -91 -59 91
		mu 0 4 54 53 50 51
		f 4 -18 -92 -58 92
		mu 0 4 18 17 64 65
		f 4 -45 -93 -55 93
		mu 0 4 37 18 65 66
		f 4 -36 -94 -52 94
		mu 0 4 67 68 69 103
		f 4 -48 -95 -51 95
		mu 0 4 43 42 46 45
		f 4 -74 -96 -75 -85
		mu 0 4 6 43 45 33
		f 4 -15 96 97 98
		mu 0 4 15 14 70 71
		f 4 -21 -99 99 100
		mu 0 4 22 21 72 73
		f 4 -5 101 102 103
		mu 0 4 5 4 74 75
		f 4 -29 104 105 -102
		mu 0 4 30 29 76 77
		f 4 -41 -101 106 -105
		mu 0 4 29 22 73 76
		f 4 -62 107 108 109
		mu 0 4 52 55 78 79
		f 4 -20 110 111 -108
		mu 0 4 80 81 82 83
		f 4 -46 112 113 -111
		mu 0 4 81 84 104 82
		f 4 -38 114 115 -113
		mu 0 4 85 86 87 88
		f 4 -47 116 117 -115
		mu 0 4 41 40 89 90
		f 4 -80 -110 118 -97
		mu 0 4 14 52 79 70
		f 4 -73 -104 119 -117
		mu 0 4 40 5 75 89
		f 4 -98 120 -1 121
		mu 0 4 91 92 1 0
		f 4 -100 -122 -4 122
		mu 0 4 93 91 0 3
		f 4 -103 123 -26 124
		mu 0 4 94 95 27 26
		f 4 -106 125 -27 -124
		mu 0 4 95 96 24 27
		f 4 -107 -123 -40 -126
		mu 0 4 96 93 3 24
		f 4 -109 126 -64 127
		mu 0 4 97 98 58 57
		f 4 -112 128 -65 -127
		mu 0 4 98 99 59 58
		f 4 -114 129 -68 -129
		mu 0 4 99 100 61 59
		f 4 -116 130 -71 -130
		mu 0 4 100 101 63 61
		f 4 -118 131 -72 -131
		mu 0 4 101 102 62 63
		f 4 -119 -128 -81 -121
		mu 0 4 92 97 57 1
		f 4 -120 -125 -84 -132
		mu 0 4 102 94 26 62;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bear_earL";
	setAttr ".rp" -type "double3" 5.1809545159339905 52.271251678466797 2.1650374382734299 ;
	setAttr ".sp" -type "double3" 5.1809545159339905 52.271251678466797 2.1650374382734299 ;
createNode mesh -n "bear_earLShape" -p "bear_earL";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 92 ".uvst[0].uvsp[0:91]" -type "float2" 0.55320799 0.89830703
		 0.576105 0.93105298 0.56862599 0.95750201 0.55729002 0.92110503 0.23566701 0.96116501
		 0.189834 0.97888499 0.187951 0.95772398 0.228117 0.93350601 0.226045 0.90530002 0.17180599
		 0.92490399 0.16532899 0.90132999 0.22340401 0.88537902 0.212975 0.81027502 0.170359
		 0.80673099 0.17013399 0.79238302 0.21077099 0.79155099 0.026831999 0.86796898 0.046755001
		 0.86908698 0.045898002 0.90427101 0.023947001 0.897946 0.238268 0.87881398 0.25749901
		 0.87304598 0.26712799 0.90148401 0.24683 0.91282499 0.106532 0.96236098 0.057753
		 0.94928902 0.063851997 0.92171901 0.106568 0.93866199 0.108623 0.88274598 0.115003
		 0.90392298 0.064139001 0.892474 0.062743001 0.872585 0.094072998 0.75950497 0.097759001
		 0.768682 0.047375001 0.80261999 0.043019 0.78195202 0.62206 0.95963597 0.63413 0.92878503
		 0.67149198 0.915658 0.65807599 0.93422502 0.23364601 0.81476098 0.255445 0.81939101
		 0.25409001 0.85073298 0.23504201 0.851924 0.559071 0.866983 0.592933 0.89647901 0.59140199
		 0.91783601 0.55457997 0.88050598 0.64551198 0.92334801 0.64707297 0.903409 0.68022197
		 0.90180898 0.68705201 0.882828 0.021238999 0.82390398 0.040453002 0.81421 0.043834999
		 0.84172398 0.024378 0.84512198 0.101312 0.80429202 0.106653 0.84970498 0.060736999
		 0.84390998 0.058274001 0.81527299 0.160937 0.83060801 0.16213299 0.86737102 0.21955401
		 0.85651898 0.216793 0.828866 0.23314799 0.97891802 0.189081 0.99021 0.27177301 0.86619997
		 0.28094399 0.89415097 0.0089440001 0.89419103 0.011334 0.86518401 0.106347 0.97654098
		 0.061161999 0.96652597 0.164516 0.78854299 0.20415699 0.78371799 0.26329201 0.82828599
		 0.268794 0.84590799 0.099868998 0.756522 0.055769999 0.77260399 0.0084960004 0.84605598
		 0.0057970001 0.82867301 0.55957699 0.96217102 0.53934699 0.924582 0.53337401 0.89375401
		 0.69375199 0.92209899 0.672436 0.94534999 0.63276601 0.96241999 0.54539502 0.85309899
		 0.58887202 0.86427897 0.53694201 0.87241203 0.66450101 0.85501599 0.70608401 0.88119
		 0.70320398 0.90145499;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 68 ".vt[0:67]"  4.30238819 54.49449921 3.082612991 5.89270115 54.2478714 2.58656812
		 6.84183311 54.63130569 3.32857895 5.23810196 54.71986771 3.082612991 5.10142612 55.70177078 3.71544194
		 7.10238791 55.26337433 3.73887897 7.48473215 56.024925232 2.86767197 5.22622299 56.2057457 2.97027612
		 4.66246223 56.23075104 2.27905607 6.67369318 55.031047821 1.70678902 5.80615807 54.40978241 1.33084595
		 3.98351192 55.88242722 2.27905607 1.85511601 53.98094559 2.93799996 3.26346397 52.94141769 2.79647112
		 3.72535706 53.20375061 3.11265111 2.24071908 53.93017578 3.71544194 7.21356297 49.49990082 1.72249603
		 6.82004499 49.59277725 0.95180601 7.64039612 50.83819199 0.73765701 7.88538694 50.57988739 1.57317102
		 3.73911905 55.96429825 2.96004295 3.83080912 55.54191589 3.71544194 8.39604759 52.60391998 1.79761803
		 8.50679302 52.87432861 0.61450303 6.24082279 51.98711777 1.057091951 6.7846961 52.77891541 0.478504
		 6.87961817 50.59671783 0.33872399 6.39231205 50.090698242 0.63669097 5.46757603 50.2336998 3.92132998
		 5.069219112 50.24741745 3.77245307 5.24449921 48.31175232 2.11843204 5.97300196 48.36981583 2.58916903
		 7.70426178 52.86307144 2.32464004 6.77183723 52.14385223 1.47367799 6.45623207 50.44129562 1.75583005
		 7.0035190582 51.20753479 2.033245087 2.98038912 55.052383423 3.71544194 2.80040503 55.26039124 2.94321394
		 3.16202497 53.80794144 3.082612991 4.6145339 53.2734375 1.90224004 5.51697016 53.7183609 1.83448303
		 3.65097404 54.15808487 3.082612991 6.59749699 51.58301544 1.63109601 5.84967899 51.30984116 1.99209201
		 5.987607 49.95641708 2.043553114 5.5882082 49.61884689 2.81725311 6.025379181 48.95969009 1.53889203
		 6.57719898 48.88048172 2.13989997 3.80247688 50.095779419 2.704036 5.17159224 51.22141647 1.72126305
		 5.62120104 49.43897629 1.21007204 4.85009623 48.7872467 1.78345001 3.4104569 52.69419479 1.70678902
		 4.60830784 53.55199432 1.33084595 3.039515018 55.20641708 2.27905607 2.09551692 54.53041458 2.27905607
		 5.11270523 55.07302475 3.76702905 6.69812918 54.71113205 3.79463005 3.93345308 54.87486267 3.76702905
		 7.62179995 50.74016953 2.12358689 6.97397995 49.75128555 2.30349898 7.89945507 52.42844009 2.23818803
		 3.9800539 53.051704407 3.16840196 2.69494009 54.03730011 3.76702905 3.28673792 54.46109772 3.76702905
		 5.46418095 50.52726364 3.99135089 5.98638678 48.88288879 3.014799118 6.4698019 49.2914505 2.65534496;
	setAttr -s 132 ".ed[0:131]"  0 1 1 1 2 1 2 3 0 3 0 0 4 5 0 5 6 1 6 7 1
		 7 4 0 8 9 0 9 10 1 10 11 1 11 8 0 12 13 1 13 14 1 14 15 0 15 12 0 16 17 1 17 18 1
		 18 19 0 19 16 0 20 21 1 21 4 0 7 20 1 22 19 0 18 23 1 23 22 1 24 25 1 25 26 0 26 27 0
		 27 24 1 28 29 1 29 30 1 30 31 0 31 28 0 32 33 1 33 34 1 34 35 0 35 32 0 5 22 0 23 6 1
		 9 25 0 24 10 1 13 29 1 28 14 0 1 33 1 32 2 0 15 36 0 36 37 1 37 12 1 38 39 0 39 40 1
		 40 41 1 41 38 0 42 40 1 39 43 0 43 42 1 44 42 1 43 45 0 45 44 0 30 46 1 46 47 1 47 31 0
		 48 49 1 49 50 1 50 51 0 51 48 0 52 53 1 53 49 1 48 52 0 54 53 1 52 55 0 55 54 0 36 21 0
		 20 37 1 40 1 1 0 41 0 42 33 1 44 34 0 46 17 1 16 47 0 49 24 1 27 50 0 53 10 1 54 11 0
		 18 26 0 25 23 1 9 6 1 8 7 0 11 20 1 54 37 1 55 12 0 52 13 1 48 29 1 51 30 0 50 46 1
		 27 17 1 4 56 0 56 57 0 57 5 0 21 58 0 58 56 0 19 59 0 59 60 0 60 16 0 22 61 0 61 59 0
		 57 61 0 14 62 0 62 63 0 63 15 0 63 64 0 64 36 0 28 65 0 65 62 0 31 66 0 66 65 0 47 67 0
		 67 66 0 64 58 0 60 67 0 56 3 0 2 57 1 58 0 1 59 35 0 34 60 1 61 32 1 62 39 1 38 63 0
		 41 64 1 65 43 1 66 45 0 67 44 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 7
		mu 0 4 4 5 6 7
		f 4 8 9 10 11
		mu 0 4 8 9 10 11
		f 4 12 13 14 15
		mu 0 4 12 13 14 15
		f 4 16 17 18 19
		mu 0 4 16 17 18 19
		f 4 20 21 -8 22
		mu 0 4 20 21 22 23
		f 4 23 -19 24 25
		mu 0 4 24 25 26 27
		f 4 26 27 28 29
		mu 0 4 28 29 30 31
		f 4 30 31 32 33
		mu 0 4 32 33 34 35
		f 4 34 35 36 37
		mu 0 4 36 37 38 39
		f 4 38 -26 39 -6
		mu 0 4 5 24 27 6
		f 4 -10 40 -27 41
		mu 0 4 10 9 29 28
		f 4 -14 42 -31 43
		mu 0 4 14 13 33 32
		f 4 -2 44 -35 45
		mu 0 4 2 1 37 36
		f 4 -16 46 47 48
		mu 0 4 40 41 42 43
		f 4 49 50 51 52
		mu 0 4 44 45 46 47
		f 4 53 -51 54 55
		mu 0 4 48 46 45 49
		f 4 56 -56 57 58
		mu 0 4 50 48 49 51
		f 4 -33 59 60 61
		mu 0 4 52 53 54 55
		f 4 62 63 64 65
		mu 0 4 56 57 58 59
		f 4 66 67 -63 68
		mu 0 4 60 61 57 56
		f 4 69 -67 70 71
		mu 0 4 62 61 60 63
		f 4 -48 72 -21 73
		mu 0 4 43 42 21 20
		f 4 -52 74 -1 75
		mu 0 4 47 46 1 0
		f 4 -45 -75 -54 76
		mu 0 4 37 1 46 48
		f 4 -36 -77 -57 77
		mu 0 4 38 37 48 50
		f 4 -61 78 -17 79
		mu 0 4 55 54 17 16
		f 4 80 -30 81 -64
		mu 0 4 57 28 31 58
		f 4 82 -42 -81 -68
		mu 0 4 61 10 28 57
		f 4 -11 -83 -70 83
		mu 0 4 11 10 61 62
		f 4 -25 84 -28 85
		mu 0 4 27 26 30 29
		f 4 -40 -86 -41 86
		mu 0 4 6 27 29 9
		f 4 -7 -87 -9 87
		mu 0 4 7 6 9 8
		f 4 88 -23 -88 -12
		mu 0 4 11 20 23 8
		f 4 89 -74 -89 -84
		mu 0 4 62 43 20 11
		f 4 90 -49 -90 -72
		mu 0 4 63 40 43 62
		f 4 -71 91 -13 -91
		mu 0 4 63 60 13 12
		f 4 -43 -92 -69 92
		mu 0 4 33 13 60 56
		f 4 -32 -93 -66 93
		mu 0 4 34 33 56 59
		f 4 -60 -94 -65 94
		mu 0 4 54 53 59 58
		f 4 -79 -95 -82 95
		mu 0 4 17 54 58 31
		f 4 -18 -96 -29 -85
		mu 0 4 18 17 31 30
		f 4 -5 96 97 98
		mu 0 4 5 4 64 65
		f 4 -22 99 100 -97
		mu 0 4 22 21 66 67
		f 4 -20 101 102 103
		mu 0 4 16 19 68 69
		f 4 -24 104 105 -102
		mu 0 4 25 24 70 71
		f 4 -39 -99 106 -105
		mu 0 4 24 5 65 70
		f 4 -15 107 108 109
		mu 0 4 15 14 72 73
		f 4 -47 -110 110 111
		mu 0 4 42 41 74 75
		f 4 -44 112 113 -108
		mu 0 4 14 32 76 72
		f 4 -34 114 115 -113
		mu 0 4 32 35 77 76
		f 4 -62 116 117 -115
		mu 0 4 52 55 78 79
		f 4 -73 -112 118 -100
		mu 0 4 21 42 75 66
		f 4 -80 -104 119 -117
		mu 0 4 55 16 69 78
		f 4 -98 120 -3 121
		mu 0 4 80 81 3 2
		f 4 -101 122 -4 -121
		mu 0 4 81 82 0 3
		f 4 -103 123 -37 124
		mu 0 4 83 84 39 38
		f 4 -106 125 -38 -124
		mu 0 4 84 85 36 39
		f 4 -107 -122 -46 -126
		mu 0 4 85 80 2 36
		f 4 -109 126 -50 127
		mu 0 4 86 87 45 44
		f 4 -111 -128 -53 128
		mu 0 4 88 86 44 47
		f 4 -114 129 -55 -127
		mu 0 4 87 89 49 45
		f 4 -116 130 -58 -130
		mu 0 4 89 90 51 49
		f 4 -118 131 -59 -131
		mu 0 4 90 91 50 51
		f 4 -119 -129 -76 -123
		mu 0 4 82 88 47 0
		f 4 -120 -125 -78 -132
		mu 0 4 91 83 38 50;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 7;
	setAttr -s 9 ".dli[1:8]"  1 2 3 0 4 5 6 7;
	setAttr -s 7 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 0\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 0\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n"
		+ "                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n"
		+ "                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n"
		+ "            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"perPolygonSort\" \n"
		+ "                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 0\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n"
		+ "            -transparencyAlgorithm \"perPolygonSort\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 0\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n"
		+ "                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n"
		+ "            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n"
		+ "                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n"
		+ "                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n"
		+ "                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n"
		+ "                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n"
		+ "                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n"
		+ "                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n"
		+ "                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"perPolygonSort\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"perPolygonSort\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode displayLayer -n "head";
	setAttr ".c" 30;
	setAttr ".do" 1;
createNode displayLayer -n "body";
	setAttr ".c" 31;
	setAttr ".do" 2;
createNode displayLayer -n "limbs";
	setAttr ".c" 30;
	setAttr ".do" 3;
createNode displayLayer -n "bow";
	setAttr ".c" 12;
	setAttr ".do" 4;
createNode shadingEngine -n "lambert2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode file -n "file1";
	setAttr ".ftn" -type "string" "/Users/oneanimefan/Desktop/brownn.png";
createNode place2dTexture -n "place2dTexture1";
createNode file -n "file2";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "/Users/oneanimefan/Desktop/Fur0037_1_S.jpg";
createNode place2dTexture -n "place2dTexture2";
createNode bump2d -n "bump2d1";
	setAttr ".bi" 1;
	setAttr ".vc2" -type "float3" 9.9999997e-06 9.9999997e-06 0 ;
createNode objectSet -n "facialFeatures:pCylinder1";
	setAttr ".ihi" 0;
createNode objectSet -n "facialFeatures:pCube10";
	setAttr ".ihi" 0;
createNode objectSet -n "facialFeatures:pCube11";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr -s 2 ".gn";
createNode objectSet -n "facialFeatures:pCube12";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr -s 2 ".gn";
createNode displayLayer -n "face";
	setAttr ".c" 12;
	setAttr ".do" 5;
createNode groupId -n "groupId13";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	setAttr ".ihi" 0;
createNode displayLayer -n "patch";
	setAttr ".c" 12;
	setAttr ".do" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :initialShadingGroup;
	setAttr -s 14 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 4 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "bow.di" "polySurface4.do";
connectAttr "bow.di" "polySurface5.do";
connectAttr "limbs.di" "bear_armL.do";
connectAttr "limbs.di" "bear_legL.do";
connectAttr "limbs.di" "bear_legR.do";
connectAttr "limbs.di" "bear_armR.do";
connectAttr "patch.di" "basePatch:pPlane4.do";
connectAttr "body.di" "bear_body.do";
connectAttr "face.di" "bear_bow.do";
connectAttr "face.di" "bear_eye.do";
connectAttr "groupId13.id" "bear_eyeShape.iog.og[0].gid";
connectAttr "facialFeatures:pCylinder1.mwc" "bear_eyeShape.iog.og[0].gco";
connectAttr "groupId14.id" "bear_eyeShape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "bear_eyeShape.iog.og[1].gco";
connectAttr "face.di" "bear_nose.do";
connectAttr "groupId15.id" "bear_noseShape.iog.og[0].gid";
connectAttr "facialFeatures:pCube10.mwc" "bear_noseShape.iog.og[0].gco";
connectAttr "groupId16.id" "bear_noseShape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "bear_noseShape.iog.og[1].gco";
connectAttr "face.di" "bear_browR.do";
connectAttr "groupId17.id" "bear_browRShape.iog.og[0].gid";
connectAttr "facialFeatures:pCube11.mwc" "bear_browRShape.iog.og[0].gco";
connectAttr "groupId18.id" "bear_browRShape.iog.og[1].gid";
connectAttr "facialFeatures:pCube12.mwc" "bear_browRShape.iog.og[1].gco";
connectAttr "groupId19.id" "bear_browRShape.iog.og[2].gid";
connectAttr ":initialShadingGroup.mwc" "bear_browRShape.iog.og[2].gco";
connectAttr "face.di" "bear_browL.do";
connectAttr "groupId20.id" "bear_browLShape.iog.og[0].gid";
connectAttr "facialFeatures:pCube11.mwc" "bear_browLShape.iog.og[0].gco";
connectAttr "groupId21.id" "bear_browLShape.iog.og[1].gid";
connectAttr "facialFeatures:pCube12.mwc" "bear_browLShape.iog.og[1].gco";
connectAttr "groupId22.id" "bear_browLShape.iog.og[2].gid";
connectAttr ":initialShadingGroup.mwc" "bear_browLShape.iog.og[2].gco";
connectAttr "head.di" "bear_head.do";
connectAttr "head.di" "bear_earR.do";
connectAttr "head.di" "bear_earL.do";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "layerManager.dli[2]" "head.id";
connectAttr "layerManager.dli[3]" "body.id";
connectAttr "layerManager.dli[5]" "limbs.id";
connectAttr "layerManager.dli[6]" "bow.id";
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr "file2.oa" "bump2d1.bv";
connectAttr "groupId13.msg" "facialFeatures:pCylinder1.gn" -na;
connectAttr "bear_eyeShape.iog.og[0]" "facialFeatures:pCylinder1.dsm" -na;
connectAttr "groupId15.msg" "facialFeatures:pCube10.gn" -na;
connectAttr "bear_noseShape.iog.og[0]" "facialFeatures:pCube10.dsm" -na;
connectAttr "groupId17.msg" "facialFeatures:pCube11.gn" -na;
connectAttr "groupId20.msg" "facialFeatures:pCube11.gn" -na;
connectAttr "bear_browRShape.iog.og[0]" "facialFeatures:pCube11.dsm" -na;
connectAttr "bear_browLShape.iog.og[0]" "facialFeatures:pCube11.dsm" -na;
connectAttr "groupId18.msg" "facialFeatures:pCube12.gn" -na;
connectAttr "groupId21.msg" "facialFeatures:pCube12.gn" -na;
connectAttr "bear_browRShape.iog.og[1]" "facialFeatures:pCube12.dsm" -na;
connectAttr "bear_browLShape.iog.og[1]" "facialFeatures:pCube12.dsm" -na;
connectAttr "layerManager.dli[7]" "face.id";
connectAttr "layerManager.dli[8]" "patch.id";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "bear_armLShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_legLShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_legRShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_armRShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "basePatch:pPlane4Shape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_bodyShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_bowShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_headShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_earRShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_earLShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "bear_eyeShape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "bear_noseShape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "bear_browRShape.iog.og[2]" ":initialShadingGroup.dsm" -na;
connectAttr "bear_browLShape.iog.og[2]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId14.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId16.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId19.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId22.msg" ":initialShadingGroup.gn" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of bear_002.ma
