//Maya ASCII 2014 scene
//Name: bear_001.ma
//Last modified: Sat, Jul 05, 2014 11:26:48 PM
//Codeset: UTF-8
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010035-864206";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.8216135368701041 64.749943932316768 102.94356681234439 ;
	setAttr ".r" -type "double3" -15.000000000007489 2.0000000000000018 9.9452917826146688e-17 ;
	setAttr ".rp" -type "double3" 3.1086244689504383e-15 0 1.4210854715202004e-14 ;
	setAttr ".rpt" -type "double3" 1.1951965110615073e-14 -1.5466776643401578e-15 -1.0666866926210247e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".pze" yes;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 110.437624744316;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.3708899808742725 30.240172573200848 5.1618059860566756 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.1346383955078352 100.93301348246804 8.9839001204365765 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 15.339141390150488;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.669164367284941 37.835320941869661 119.29730708422083 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 57.932158534123523;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 116.34533128146153 25.886860124687601 5.4034449482274312 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 85.404971928763842;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	setAttr ".t" -type "double3" 0.33028732307709063 48.619648912343017 3.5014775100484901 ;
	setAttr ".s" -type "double3" 1.3537328726025699 1.3537328726025699 1.3537328726025699 ;
	setAttr ".rp" -type "double3" 0 -0.83949124324993263 0.85081186034728939 ;
	setAttr ".sp" -type "double3" 0 -0.56285881996154785 0.57588911056518555 ;
	setAttr ".spt" -type "double3" 0 -0.27663242328838472 0.2749227497821039 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[104:107]" -type "float3"  1.0633974 0.1719372 -0.71037483 
		-2.4288828 1.5212389 -0.026822574 -1.9623717 -0.48919293 -0.10762582 1.2702032 -1.2402346 
		-0.90268731;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode mesh -n "polySurfaceShape1" -p "pCube1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 132 ".uvst[0].uvsp[0:131]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.33333334 0.25 0.375 0.29166669 0.33333334 0.16666667
		 0.33333334 0.083333336 0.33333334 0 0.375 0.95833325 0.45833334 0.95833325 0.54166669
		 0.95833325 0.625 0.95833325 0.66666669 0 0.66666669 0.083333336 0.66666669 0.16666667
		 0.625 0.29166669 0.66666669 0.25 0.54166669 0.29166669 0.45833334 0.29166669 0.625
		 0.45833334 0.83333337 0.25 0.54166669 0.45833334 0.45833334 0.45833334 0.16666667
		 0.25 0.375 0.45833334 0.16666667 0.16666667 0.16666667 0.083333336 0.16666667 0 0.375
		 0.79166663 0.45833334 0.79166663 0.54166669 0.79166663 0.625 0.79166663 0.83333337
		 0 0.83333337 0.083333336 0.83333337 0.16666667 0.375 0 0.45833334 0 0.45833334 0.083333336
		 0.375 0.083333336 0.54166669 0 0.54166669 0.083333336 0.625 0 0.625 0.083333336 0.375
		 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336 0.625 0.083333336
		 0.625 0 0.54166669 0 0.45833334 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669
		 0.083333336 0.625 0.083333336 0.625 0 0.54166669 0 0.45833334 0 0.375 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 36 ".pt";
	setAttr ".pt[0]" -type "float3" 2.9802322e-08 -7.4505806e-09 0.071456477 ;
	setAttr ".pt[1]" -type "float3" 7.4505806e-09 0 0.0741909 ;
	setAttr ".pt[2]" -type "float3" -1.4901161e-08 0 0.0741909 ;
	setAttr ".pt[3]" -type "float3" -2.9802322e-08 -7.4505806e-09 0.071456477 ;
	setAttr ".pt[4]" -type "float3" 5.9604645e-08 2.2351742e-08 0.2852923 ;
	setAttr ".pt[5]" -type "float3" -7.4505806e-09 -2.9802322e-08 0.47660172 ;
	setAttr ".pt[6]" -type "float3" 0 -2.9802322e-08 0.47660172 ;
	setAttr ".pt[7]" -type "float3" -5.9604645e-08 -1.4901161e-08 0.2852923 ;
	setAttr ".pt[12]" -type "float3" -0.17635417 0.26297379 -0.68066406 ;
	setAttr ".pt[13]" -type "float3" -0.06813544 0.33230209 -0.6696949 ;
	setAttr ".pt[14]" -type "float3" 0.0681355 0.33230209 -0.6696949 ;
	setAttr ".pt[15]" -type "float3" 0.17635417 0.26297379 -0.68066406 ;
	setAttr ".pt[80]" -type "float3" 0.72730023 0.85023093 0.95316583 ;
	setAttr ".pt[81]" -type "float3" 0.24243337 0.90062374 0.93584269 ;
	setAttr ".pt[82]" -type "float3" 0.34899813 -0.96071386 0.80569202 ;
	setAttr ".pt[83]" -type "float3" 1.0452771 -0.31747016 0.96017849 ;
	setAttr ".pt[84]" -type "float3" -0.24243337 0.90062374 0.93584269 ;
	setAttr ".pt[85]" -type "float3" -0.34899813 -0.96071386 0.80569202 ;
	setAttr ".pt[86]" -type "float3" -0.72730041 0.85023093 0.95316583 ;
	setAttr ".pt[87]" -type "float3" -1.0452771 -0.31747016 0.96017849 ;
	setAttr ".pt[88]" -type "float3" 1.0430813e-07 -2.2351742e-08 0.43559816 ;
	setAttr ".pt[89]" -type "float3" -8.9406967e-08 -2.2351742e-08 0.57711828 ;
	setAttr ".pt[90]" -type "float3" -1.4901161e-08 2.9802322e-08 0.75980937 ;
	setAttr ".pt[91]" -type "float3" 1.4901161e-08 2.9802322e-08 0.75980937 ;
	setAttr ".pt[92]" -type "float3" 8.9406967e-08 2.2351742e-08 0.57711828 ;
	setAttr ".pt[93]" -type "float3" -1.1920929e-07 -2.2351742e-08 0.43559816 ;
	setAttr ".pt[94]" -type "float3" -1.4901161e-08 -4.4703484e-08 0.6079309 ;
	setAttr ".pt[95]" -type "float3" 7.4505806e-09 -4.4703484e-08 0.6079309 ;
	setAttr ".pt[96]" -type "float3" 0.48022619 -0.34123594 0.89485335 ;
	setAttr ".pt[97]" -type "float3" 0.24529621 -0.59810597 0.88625079 ;
	setAttr ".pt[98]" -type "float3" -0.24529621 -0.59810597 0.88625079 ;
	setAttr ".pt[99]" -type "float3" -0.48022619 -0.34123591 0.89485335 ;
	setAttr ".pt[100]" -type "float3" -0.40945569 0.51070517 0.8977111 ;
	setAttr ".pt[101]" -type "float3" -0.19450055 0.59810585 0.89345413 ;
	setAttr ".pt[102]" -type "float3" 0.19450054 0.59810585 0.89345413 ;
	setAttr ".pt[103]" -type "float3" 0.40945569 0.51070517 0.8977111 ;
	setAttr -s 104 ".vt[0:103]"  -2.91953874 -4.53580475 2.92513514 -1.33318043 -5.61046267 2.9321897
		 1.33318079 -5.61046267 2.9321897 2.91953897 -4.53580475 2.92513514 -3.66194773 -1.095014453 3.47681189
		 -0.84164268 -0.03960463 3.97037268 0.84164286 -0.03960463 3.97037268 3.66194797 -1.095014691 3.47681189
		 -3.087609768 0.66749138 3.85355043 -0.79977131 1.1208514 4.64985752 0.79977155 1.1208514 4.64985752
		 3.087610245 0.66749167 3.85355043 -2.063080311 2.94198298 4.14968395 -0.68656349 3.14967346 3.91305423
		 0.68656379 3.14967346 3.91305423 2.063080549 2.94198298 4.14968395 -2.93547058 3.83891392 0.33431649
		 -0.97284859 4.36876154 0.32084572 0.97284895 4.36876154 0.32084572 2.93547106 3.83891392 0.33431649
		 -2.90384197 4.016702652 -1.24045539 -0.95399338 4.48572493 -1.28482735 0.95399374 4.48572493 -1.28482735
		 2.90384245 4.016702652 -1.24045539 -1.93099856 2.66455078 -3.096529484 -0.64366609 2.68259048 -3.27266932
		 0.64366639 2.68259048 -3.27266932 1.93099916 2.66455078 -3.096529484 -2.53468108 0.88110352 -3.65451217
		 -1.41942132 0.88110352 -4.039468288 0.84489381 0.88110352 -4.23777819 2.53468132 0.88110352 -3.65451217
		 -2.53468108 -0.80868149 -3.65451217 -1.41942132 -0.80868149 -4.039468288 0.84489381 -0.80868149 -4.23777819
		 2.53468132 -0.80868149 -3.65451217 -1.55455184 -2.5139184 -3.89355874 -0.51818377 -2.30689359 -4.26470757
		 0.51818407 -2.30689359 -4.26470757 1.55455208 -2.5139184 -3.89355874 -4.051468849 -3.27426887 -1.78349781
		 -1.69638705 -4.92056608 -1.77432203 1.69638729 -4.92056608 -1.77432203 4.051468849 -3.27426887 -1.78349781
		 -3.32411671 -4.069679737 0.15061104 -1.83561587 -5.25646687 0.1558342 1.83561635 -5.25646687 0.1558342
		 3.32411695 -4.069679737 0.15061104 4.55063295 -1.71472812 -1.18330789 4.53298473 -1.7083472 0.35510004
		 4.55063295 1.61824131 -1.18330789 4.53428698 0.51632452 0.35459042 -4.55063295 -1.71472812 -1.18330789
		 -4.53298473 -1.7083472 0.35510004 -4.55063295 1.61824131 -1.18330789 -4.53428698 0.51632452 0.35459042
		 -2.63717055 3.7980485 1.93390262 -4.037639618 0.32578138 1.81052136 -4.036968708 -1.41222513 1.85829592
		 -2.98391414 -4.52747583 1.91752613 -1.69881868 -5.61144257 1.95836973 1.69881892 -5.61144257 1.95836973
		 2.98391461 -4.52747583 1.91752613 4.036969185 -1.41222489 1.85829592 4.037640095 0.3257812 1.81052136
		 2.63717079 3.7980485 1.9339025 0.87582898 4.07273531 2.05278945 -0.87582868 4.07273531 2.05278945
		 2.34409857 3.49887562 -2.38016653 0.77268755 3.74020815 -2.3998878 -0.77268726 3.74020815 -2.3998878
		 -2.34409809 3.49887562 -2.38016653 -3.62624145 1.32771301 -2.4415803 -3.62511849 -1.29895401 -2.4415803
		 -2.2338686 -3.61994505 -2.9269383 -0.73744571 -4.30163765 -3.062220573 0.73744595 -4.30163765 -3.062220573
		 2.23386908 -3.61994505 -2.9269383 3.62511849 -1.29895401 -2.4415803 3.62624192 1.32771301 -2.4415803
		 -1.75395453 -4.44067764 5.12064791 -0.58465147 -4.54546738 5.18258524 -0.84164262 -0.67492771 5.64792395
		 -2.52078772 -2.012514114 5.095575333 0.58465177 -4.54546738 5.18258524 0.84164286 -0.67492771 5.64792395
		 1.75395513 -4.44067764 5.12064791 2.52078795 -2.012514114 5.095575333 -2.31941485 -4.49048424 3.86458731
		 -2.25662327 -1.072408795 4.2296958 -0.84164262 -0.64728725 4.70102262 0.84164286 -0.64728725 4.70102262
		 2.25662351 -1.072408915 4.2296958 2.31941509 -4.49048424 3.86458731 0.75006139 -4.98546076 4.3091898
		 -0.75006109 -4.98546076 4.3091898 -2.38870549 -1.5424614 4.6626358 -0.84164262 -0.66110748 5.17447329
		 0.84164286 -0.66110748 5.17447329 2.38870573 -1.54246151 4.6626358 2.03668499 -4.46558094 4.49261761
		 0.66735661 -4.76546383 4.74588776 -0.66735625 -4.76546383 4.74588776 -2.036684752 -4.46558094 4.49261761;
	setAttr -s 204 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 4 5 0 5 6 0 6 7 0 8 9 1 9 10 1 10 11 1
		 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1 24 25 0 25 26 0
		 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0 38 39 0 40 41 1
		 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 4 0 3 7 0 4 8 0 5 9 1 6 10 1 7 11 0 8 12 0
		 9 13 1 10 14 1 11 15 0 12 56 0 13 67 1 14 66 1 15 65 0 16 20 0 17 21 1 18 22 1 19 23 0
		 20 71 0 21 70 1 22 69 1 23 68 0 24 28 0 25 29 1 26 30 1 27 31 0 28 32 0 29 33 1 30 34 1
		 31 35 0 32 36 0 33 37 1 34 38 1 35 39 0 36 74 0 37 75 1 38 76 1 39 77 0 40 44 0 41 45 1
		 42 46 1 43 47 0 44 59 0 45 60 1 46 61 1 47 62 0 35 78 1 48 49 1 49 63 1 31 79 1 50 51 1
		 51 64 1 43 48 1 47 49 1 48 50 1 49 51 1 50 23 1 51 19 1 32 73 1 52 53 1 53 58 1 28 72 1
		 54 55 1 55 57 1 40 52 1 44 53 1 52 54 1 53 55 1 54 20 1 55 16 1 56 16 0 57 8 1 56 57 1
		 58 4 1 57 58 1 59 0 0 58 59 1 60 1 1 59 60 1 61 2 1 60 61 1 62 3 0 61 62 1 63 7 1
		 62 63 1 64 11 1 63 64 1 65 19 0 64 65 1 66 18 1 65 66 1 67 17 1 66 67 1 67 56 1 68 27 0
		 69 26 1 68 69 1 70 25 1 69 70 1 71 24 0 70 71 1 72 54 1 71 72 1 73 52 1 72 73 1 74 40 0
		 73 74 1 75 41 1 74 75 1 76 42 1 75 76 1 77 43 0 76 77 1 78 48 1 77 78 1 79 50 1 78 79 1
		 79 68 1 0 88 0 1 95 1 80 81 0 5 90 1 81 82 1 4 89 0 83 82 0 80 83 0 2 94 1 81 84 0
		 6 91 1 84 85 1;
	setAttr ".ed[166:203]" 82 85 0 3 93 0 84 86 0 7 92 0 86 87 0 85 87 0 88 103 0
		 89 96 0 88 89 1 90 97 1 89 90 1 91 98 1 90 91 1 92 99 0 91 92 1 93 100 0 92 93 1
		 94 101 1 93 94 1 95 102 1 94 95 1 95 88 1 96 83 0 97 82 1 96 97 1 98 85 1 97 98 1
		 99 87 0 98 99 1 100 86 0 99 100 1 101 84 1 100 101 1 102 81 1 101 102 1 103 80 0
		 102 103 1 103 96 1;
	setAttr -s 102 -ch 408 ".fc[0:101]" -type "polyFaces" 
		f 4 156 158 -161 -162
		mu 0 4 108 109 110 111
		f 4 163 165 -167 -159
		mu 0 4 109 112 113 110
		f 4 168 170 -172 -166
		mu 0 4 112 114 115 113
		f 4 3 39 -7 -39
		mu 0 4 4 5 9 8
		f 4 4 40 -8 -40
		mu 0 4 5 6 10 9
		f 4 5 41 -9 -41
		mu 0 4 6 7 11 10
		f 4 6 43 -10 -43
		mu 0 4 8 9 13 12
		f 4 7 44 -11 -44
		mu 0 4 9 10 14 13
		f 4 8 45 -12 -45
		mu 0 4 10 11 15 14
		f 4 9 47 129 -47
		mu 0 4 12 13 91 77
		f 4 10 48 128 -48
		mu 0 4 13 14 90 91
		f 4 11 49 126 -49
		mu 0 4 14 15 88 90
		f 4 12 51 -16 -51
		mu 0 4 16 17 21 20
		f 4 13 52 -17 -52
		mu 0 4 17 18 22 21
		f 4 14 53 -18 -53
		mu 0 4 18 19 23 22
		f 4 15 55 136 -55
		mu 0 4 20 21 95 97
		f 4 16 56 134 -56
		mu 0 4 21 22 94 95
		f 4 17 57 132 -57
		mu 0 4 22 23 92 94
		f 4 18 59 -22 -59
		mu 0 4 24 25 29 28
		f 4 19 60 -23 -60
		mu 0 4 25 26 30 29
		f 4 20 61 -24 -61
		mu 0 4 26 27 31 30
		f 4 21 63 -25 -63
		mu 0 4 28 29 33 32
		f 4 22 64 -26 -64
		mu 0 4 29 30 34 33
		f 4 23 65 -27 -65
		mu 0 4 30 31 35 34
		f 4 24 67 -28 -67
		mu 0 4 32 33 37 36
		f 4 25 68 -29 -68
		mu 0 4 33 34 38 37
		f 4 26 69 -30 -69
		mu 0 4 34 35 39 38
		f 4 144 143 -31 -142
		mu 0 4 101 102 41 40
		f 4 146 145 -32 -144
		mu 0 4 102 103 42 41
		f 4 148 147 -33 -146
		mu 0 4 103 104 43 42
		f 4 30 75 -34 -75
		mu 0 4 40 41 45 44
		f 4 31 76 -35 -76
		mu 0 4 41 42 46 45
		f 4 32 77 -36 -77
		mu 0 4 42 43 47 46
		f 4 114 113 -1 -112
		mu 0 4 81 82 49 48
		f 4 116 115 -2 -114
		mu 0 4 82 83 50 49
		f 4 118 117 -3 -116
		mu 0 4 83 84 51 50
		f 4 -148 150 149 -89
		mu 0 4 53 105 106 56
		f 4 -78 88 83 -90
		mu 0 4 54 53 56 57
		f 4 -118 120 119 -38
		mu 0 4 3 85 86 7
		f 4 -150 152 151 -91
		mu 0 4 56 106 107 59
		f 4 -84 90 86 -92
		mu 0 4 57 56 59 60
		f 4 -120 122 121 -42
		mu 0 4 7 86 87 11
		f 4 -152 153 -58 -93
		mu 0 4 59 107 93 62
		f 4 -87 92 -54 -94
		mu 0 4 60 59 62 63
		f 4 -122 124 -50 -46
		mu 0 4 11 87 89 15
		f 4 141 100 -140 142
		mu 0 4 100 65 68 99
		f 4 74 101 -96 -101
		mu 0 4 65 66 69 68
		f 4 111 36 -110 112
		mu 0 4 80 0 4 79
		f 4 139 102 -138 140
		mu 0 4 99 68 71 98
		f 4 95 103 -99 -103
		mu 0 4 68 69 72 71
		f 4 109 38 -108 110
		mu 0 4 79 4 8 78
		f 4 137 104 54 138
		mu 0 4 98 71 74 96
		f 4 98 105 50 -105
		mu 0 4 71 72 75 74
		f 4 107 42 46 108
		mu 0 4 78 8 12 76
		f 4 99 -109 106 -106
		mu 0 4 72 78 76 75
		f 4 96 -111 -100 -104
		mu 0 4 69 79 78 72
		f 4 78 -113 -97 -102
		mu 0 4 66 80 79 69
		f 4 33 79 -115 -79
		mu 0 4 44 45 82 81
		f 4 34 80 -117 -80
		mu 0 4 45 46 83 82
		f 4 35 81 -119 -81
		mu 0 4 46 47 84 83
		f 4 -121 -82 89 84
		mu 0 4 86 85 54 57
		f 4 -123 -85 91 87
		mu 0 4 87 86 57 60
		f 4 -125 -88 93 -124
		mu 0 4 89 87 60 63
		f 4 -127 123 -15 -126
		mu 0 4 90 88 19 18
		f 4 -129 125 -14 -128
		mu 0 4 91 90 18 17
		f 4 -130 127 -13 -107
		mu 0 4 77 91 17 16
		f 4 -133 130 -21 -132
		mu 0 4 94 92 27 26
		f 4 -135 131 -20 -134
		mu 0 4 95 94 26 25
		f 4 -137 133 -19 -136
		mu 0 4 97 95 25 24
		f 4 97 -139 135 58
		mu 0 4 70 98 96 73
		f 4 94 -141 -98 62
		mu 0 4 67 99 98 70
		f 4 70 -143 -95 66
		mu 0 4 64 100 99 67
		f 4 27 71 -145 -71
		mu 0 4 36 37 102 101
		f 4 28 72 -147 -72
		mu 0 4 37 38 103 102
		f 4 29 73 -149 -73
		mu 0 4 38 39 104 103
		f 4 -151 -74 -70 82
		mu 0 4 106 105 52 55
		f 4 -153 -83 -66 85
		mu 0 4 107 106 55 58
		f 4 -154 -86 -62 -131
		mu 0 4 93 107 58 61
		f 4 0 155 187 -155
		mu 0 4 0 1 123 116
		f 4 -4 159 176 -158
		mu 0 4 5 4 117 118
		f 4 -37 154 174 -160
		mu 0 4 4 0 116 117
		f 4 1 162 186 -156
		mu 0 4 1 2 122 123
		f 4 -5 157 178 -165
		mu 0 4 6 5 118 119
		f 4 2 167 184 -163
		mu 0 4 2 3 121 122
		f 4 37 169 182 -168
		mu 0 4 3 7 120 121
		f 4 -6 164 180 -170
		mu 0 4 7 6 119 120
		f 4 -175 172 203 -174
		mu 0 4 117 116 131 124
		f 4 -177 173 190 -176
		mu 0 4 118 117 124 125
		f 4 -179 175 192 -178
		mu 0 4 119 118 125 126
		f 4 -181 177 194 -180
		mu 0 4 120 119 126 127
		f 4 -183 179 196 -182
		mu 0 4 121 120 127 128
		f 4 -185 181 198 -184
		mu 0 4 122 121 128 129
		f 4 -187 183 200 -186
		mu 0 4 123 122 129 130
		f 4 -188 185 202 -173
		mu 0 4 116 123 130 131
		f 4 -191 188 160 -190
		mu 0 4 125 124 111 110
		f 4 -193 189 166 -192
		mu 0 4 126 125 110 113
		f 4 -195 191 171 -194
		mu 0 4 127 126 113 115
		f 4 -197 193 -171 -196
		mu 0 4 128 127 115 114
		f 4 -199 195 -169 -198
		mu 0 4 129 128 114 112
		f 4 -201 197 -164 -200
		mu 0 4 130 129 112 109
		f 4 -203 199 -157 -202
		mu 0 4 131 130 109 108
		f 4 -204 201 161 -189
		mu 0 4 124 131 108 111;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "frontOrtho";
	setAttr ".t" -type "double3" 0 30.688496776363269 -30.325125328615105 ;
	setAttr ".s" -type "double3" 17.495255765028976 17.495255765028976 17.495255765028976 ;
createNode imagePlane -n "frontOrthoShape" -p "frontOrtho";
	setAttr -k off ".v";
	setAttr ".fc" 76;
	setAttr ".imn" -type "string" "/Users/oneanimefan/Desktop/Game Dev Workshop I_II/GMAP 378/orthos/bearFront.jpg";
	setAttr ".cov" -type "short2" 240 312 ;
	setAttr ".dlc" no;
	setAttr ".w" 2.4;
	setAttr ".h" 3.12;
createNode transform -n "sideOrtho";
	setAttr ".t" -type "double3" -30.325125328615108 29.587592884473739 6.8207642578299978 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 18.8168949043102 18.816894904310193 18.8168949043102 ;
	setAttr ".rp" -type "double3" -2.7832336401867733e-30 0 -8.3563799898867862e-15 ;
	setAttr ".rpt" -type "double3" -8.3563799898867846e-15 0 8.3563799898867877e-15 ;
	setAttr ".sp" -type "double3" -1.4791141972893978e-31 0 -4.4408920985006262e-16 ;
	setAttr ".spt" -type "double3" -2.6353222204578336e-30 0 -7.9122907800367235e-15 ;
createNode imagePlane -n "sideOrthoShape" -p "sideOrtho";
	setAttr -k off ".v";
	setAttr ".fc" 76;
	setAttr ".imn" -type "string" "/Users/oneanimefan/Desktop/Game Dev Workshop I_II/GMAP 378/orthos/bearSide.jpg";
	setAttr ".cov" -type "short2" 160 312 ;
	setAttr ".dlc" no;
	setAttr ".w" 1.6;
	setAttr ".h" 3.12;
createNode transform -n "pCube2";
	setAttr ".t" -type "double3" 5.4189834349141162 52.743600245038273 2.8743926357683649 ;
	setAttr ".r" -type "double3" 0 0 -54.393 ;
	setAttr ".s" -type "double3" 0.64012935452673814 0.58972057704888703 0.58972057704888703 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 84 ".uvst[0].uvsp[0:83]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.54166663 0.25 0.54166663 0.5 0.54166663 0.75 0.54166663
		 0 0.54166663 1 0.45833331 0.25 0.45833331 0.5 0.45833331 0.75 0.45833331 0 0.45833331
		 1 0.125 0.083333343 0.375 0.66666663 0.375 0.083333343 0.45833331 0.083333343 0.54166663
		 0.083333343 0.625 0.083333343 0.625 0.66666663 0.875 0.083333343 0.54166663 0.66666663
		 0.45833331 0.66666663 0.125 0.16666667 0.375 0.58333331 0.375 0.16666667 0.45833331
		 0.16666667 0.54166663 0.16666667 0.625 0.16666667 0.625 0.58333331 0.875 0.16666667
		 0.54166663 0.58333331 0.45833331 0.58333331 0.625 0.375 0.75 0.25 0.54166663 0.375
		 0.45833331 0.375 0.25 0.25 0.375 0.375 0.25 0.16666667 0.25 0.083333343 0.25 0 0.375
		 0.875 0.45833331 0.875 0.54166663 0.875 0.625 0.875 0.75 0 0.75 0.083333343 0.75
		 0.16666667 0.45833331 0.25 0.375 0.25 0.375 0.16666667 0.625 0.16666667 0.625 0.25
		 0.54166663 0.25 0.375 0 0.45833331 0 0.375 0.083333343 0.54166663 0 0.625 0 0.625
		 0.083333343 0.45833331 0.25 0.375 0.25 0.375 0.16666667 0.625 0.16666667 0.625 0.25
		 0.54166663 0.25 0.375 0 0.45833331 0 0.375 0.083333343 0.54166663 0 0.625 0 0.625
		 0.083333343;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt[0:67]" -type "float3"  1.6146227 0.61245537 -0.082485899 
		0.046589248 0.26838443 -1.9923246 1.9664326 -0.40492561 -0.082485899 -1.0210358 -1.6235292 
		-3.7151737 1.7690835 -0.54655397 -0.76352763 -2.8309553 -3.0522358 -4.0537834 1.5939605 
		1.0249484 -0.76352763 -2.3786726 -0.84754437 -1.6039349 0.58974123 -0.93787277 -3.3345721 
		-1.0981011 -3.095403 -2.5540826 -0.40269142 -0.99995363 1.2197924 0.93661934 1.4318202 
		0.2666468 0.62615848 -0.095769703 -0.042741299 0.53132015 -1.0249485 -0.47125557 
		0.53132015 1.0249484 -0.47125557 0.17053683 1.962171 -1.1046451 1.5939605 0.43194866 
		-0.76352763 0.86206985 0.50333953 -0.082485899 -0.052456699 -0.63126701 -2.7541592 
		-2.5050797 -1.7028742 -2.5762243 -0.58709824 -1.2518748 0.38278481 0.53132015 0.27248386 
		-0.27925014 1.5939605 -0.16105111 -0.76352763 1.0137993 -0.077705182 -0.082485899 
		-0.26038569 -1.3791978 -3.4619594 -2.6314867 -2.5582054 -3.5485137 -0.58709824 -2.2725976 
		-0.74345952 0.53132015 -0.47998169 -0.27925014 -2.0088146 -2.1838353 -4.2546372 0.34703898 
		-1.7174795 -3.832135 0.0066671395 -0.016028652 -0.011393883 1.876739 -0.21287559 
		-0.46874326 0.83080667 -0.10212513 -0.48609641 0.87105817 0.30807158 -0.51463145 
		1.6362994 0.14100227 -0.52347565 0.083625115 1.0663759 -0.13213331 0.55686772 0.89616686 
		1.5228572 -0.97915471 -0.78326565 -1.9132288 -1.0918453 -1.4663427 -2.8959699 -1.1731603 
		-2.1450591 -3.8914983 0.99866682 0.081500925 -0.13180374 0.59498841 0.15406762 -0.085003719 
		1.8195552 0.021689648 -0.13180374 0.72582865 -0.44336757 -2.7242913 0.1581322 -0.78503996 
		-2.6135421 -0.50861925 -0.79686731 -2.9186223 0.93596733 1.309599 -0.13180375 0.23045067 
		2.125154 -1.1469076 0.93596739 0.66264784 -0.13180378 0.92553586 1.6789486 0.24858159 
		0.28359687 -0.052869905 -2.0169108 0.36284238 0.75853819 -1.4073763 1.8173232 0.21461146 
		0.13180377 0.45081696 0.47491139 -0.70934993 0.82709455 0.27336735 0.54889506 2.3821471 
		-0.15409091 0.13180377 0.061670177 -0.39031571 -2.5964983 -0.0037268773 -0.28337583 
		-1.1535075 -1.1891656 -0.81760627 -2.1180475 -1.6645805 -1.1878389 -1.6476303 1.6521202 
		1.7271036 0.13180377 0.52591044 3.218811 -1.8697788 0.78161103 2.1681802 -1.984675 
		1.6521204 0.86564112 0.13180377 0.6154325 1.5496725 -2.3295624 0.28222477 2.9830322 
		-1.7174107 -0.99953258 -0.061160844 -1.6301489 -0.93405902 0.93617743 -0.31817389;
	setAttr -s 68 ".vt[0:67]"  -6.012454033 -3.82273865 1.50866795 6.012454987 -3.82273865 1.50866795
		 -6.012454033 2.88768768 1.50866795 6.012454987 2.88768768 1.50866795 -6.88621902 2.94638443 -0.24599552
		 6.88621902 2.94638443 -0.24599552 -6.88621902 -3.84281158 -0.24599552 6.88621902 -3.84281158 -0.24599552
		 2.29540634 4.90436935 1.50866795 2.29540634 5.013141632 -1.50866795 2.29540634 -3.84281158 -1.50866795
		 2.29540634 -3.84281158 1.50866795 -2.29540634 4.90436935 1.50866795 -2.29540634 5.013141632 -1.50866795
		 -2.29540634 -3.84281158 -1.50866795 -2.29540634 -3.84281158 1.50866795 -6.88621902 -1.28093719 -0.24599552
		 -6.012454033 -1.58592987 1.50866795 6.012454987 -1.58592987 1.50866795 6.88621902 -1.28093719 -0.24599552
		 2.29540634 -0.59201813 -2.33816814 -2.29540634 -0.59201813 -2.33816814 -6.88621902 1.28093719 -0.24599552
		 -6.012454033 0.65087891 1.50866795 6.012454987 0.65087891 1.50866795 6.88621902 1.28093719 -0.24599552
		 2.29540634 2.65877914 -2.33816814 -2.29540634 2.65877914 -2.33816814 6.44933701 3.36524963 0.63133621
		 2.29540634 6.10361481 0 -2.29540634 6.10361481 0 -6.44933701 3.36524963 0.63133621
		 -6.44933701 0.96590805 0.63133621 -6.44933701 -1.43343353 0.63133621 -6.44933701 -3.83277512 0.63133621
		 -2.29540634 -3.84281158 0 2.29540634 -3.84281158 0 6.44933701 -3.83277512 0.63133621
		 6.44933701 -1.43343353 0.63133621 6.44933701 0.96590805 0.63133621 -5.05673027 -0.025390625 1.64546871
		 -1.93053436 3.55197525 1.64546871 -5.05673027 1.85586166 1.64546871 1.93053436 3.55197525 1.64546871
		 5.056732178 -0.025390625 1.64546871 5.056732178 1.85586166 1.64546871 -5.05673027 -3.7878952 1.64546871
		 -1.93053436 -3.80477524 1.64546871 -5.05673027 -1.90664291 1.64546871 1.93053436 -3.80477524 1.64546871
		 5.056732178 -1.90664291 1.64546871 5.056732178 -3.7878952 1.64546871 -5.05673027 -0.025390625 0.22128105
		 -1.93053436 1.66333771 0.22128105 -1.93053436 3.55197525 0.22128105 -5.05673027 1.85586166 0.22128105
		 1.93053436 1.66333771 0.22128105 1.93053436 3.55197525 0.22128105 5.056732178 -0.025390625 0.22128105
		 5.056732178 1.85586166 0.22128105 -5.05673027 -3.7878952 0.22128105 -1.93053436 -3.80477524 0.22128105
		 -1.93053436 -1.070720673 0.22128105 -5.05673027 -1.90664291 0.22128105 1.93053436 -1.070720673 0.22128105
		 1.93053436 -3.80477524 0.22128105 5.056732178 -1.90664291 0.22128105 5.056732178 -3.7878952 0.22128105;
	setAttr -s 132 ".ed[0:131]"  0 15 0 2 12 0 4 13 0 6 14 0 0 17 0 1 18 0
		 2 31 0 3 28 0 4 22 0 5 25 0 6 34 0 7 37 0 8 3 0 9 5 0 8 29 1 10 7 0 9 26 1 11 1 0
		 10 36 1 12 8 0 13 9 0 12 30 1 14 10 0 13 27 1 15 11 0 14 35 1 16 6 0 17 23 0 16 33 1
		 18 24 0 19 7 0 18 38 1 20 10 1 19 20 1 21 14 1 20 21 1 21 16 1 22 16 0 23 2 0 22 32 1
		 24 3 0 25 19 0 24 39 1 26 20 1 25 26 1 27 21 1 26 27 1 27 22 1 28 5 0 29 9 1 28 29 1
		 30 13 1 29 30 1 31 4 0 30 31 1 32 23 1 31 32 1 33 17 1 32 33 1 34 0 0 33 34 1 35 15 1
		 34 35 1 36 11 1 35 36 1 37 1 0 36 37 1 38 19 1 37 38 1 39 25 1 38 39 1 39 28 1 23 40 0
		 12 41 0 2 42 0 42 41 0 40 42 0 8 43 0 24 44 0 3 45 0 44 45 0 43 45 0 41 43 0 0 46 0
		 15 47 0 46 47 0 17 48 0 46 48 0 11 49 0 47 49 0 18 50 0 1 51 0 49 51 0 51 50 0 48 40 0
		 50 44 0 40 52 1 52 53 1 41 54 1 53 54 1 42 55 0 55 54 0 52 55 0 43 57 1 56 57 1 44 58 1
		 56 58 1 45 59 0 58 59 0 57 59 0 53 56 1 54 57 0 46 60 0 47 61 1 60 61 0 61 62 1 48 63 1
		 63 62 1 60 63 0 62 64 1 49 65 1 61 65 0 65 64 1 50 66 1 64 66 1 51 67 0 65 67 0 67 66 0
		 62 53 1 63 52 0 64 56 1 66 58 0;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 97 99 -102 -103
		mu 0 4 74 37 72 73
		f 4 1 21 54 -7
		mu 0 4 2 19 47 49
		f 4 2 23 47 -9
		mu 0 4 4 20 43 35
		f 4 62 61 -1 -60
		mu 0 4 53 54 23 8
		f 4 42 71 -8 -41
		mu 0 4 39 59 45 3
		f 4 55 38 6 56
		mu 0 4 50 36 2 48
		f 4 12 7 50 -15
		mu 0 4 14 3 44 46
		f 4 -17 13 9 44
		mu 0 4 42 15 5 40
		f 4 -64 66 65 -18
		mu 0 4 18 55 56 9
		f 4 -105 106 108 -110
		mu 0 4 77 38 75 76
		f 4 19 14 52 -22
		mu 0 4 19 14 46 47
		f 4 -24 20 16 46
		mu 0 4 43 20 15 42
		f 4 -62 64 63 -25
		mu 0 4 23 54 55 18
		f 4 -100 110 104 -112
		mu 0 4 72 37 38 77
		f 4 59 4 -58 60
		mu 0 4 52 0 26 51
		f 4 114 115 -118 -119
		mu 0 4 78 79 27 80
		f 4 -120 -116 121 122
		mu 0 4 28 27 79 81
		f 4 -125 -123 126 127
		mu 0 4 83 28 81 82
		f 4 -66 68 -32 -6
		mu 0 4 1 57 58 29
		f 4 -33 -34 30 -16
		mu 0 4 16 32 30 7
		f 4 -35 -36 32 -23
		mu 0 4 21 33 32 16
		f 4 -37 34 -4 -27
		mu 0 4 25 33 21 6
		f 4 57 27 -56 58
		mu 0 4 51 26 36 50
		f 4 117 128 -98 -130
		mu 0 4 80 27 37 74
		f 4 -111 -129 119 130
		mu 0 4 38 37 27 28
		f 4 -107 -131 124 131
		mu 0 4 75 38 28 83
		f 4 31 70 -43 -30
		mu 0 4 29 58 59 39
		f 4 -44 -45 41 33
		mu 0 4 32 42 40 30
		f 4 -46 -47 43 35
		mu 0 4 33 43 42 32
		f 4 -48 45 36 -38
		mu 0 4 35 43 33 25
		f 4 -51 48 -14 -50
		mu 0 4 46 44 5 15
		f 4 -53 49 -21 -52
		mu 0 4 47 46 15 20
		f 4 -55 51 -3 -54
		mu 0 4 49 47 20 4
		f 4 39 -57 53 8
		mu 0 4 34 50 48 13
		f 4 28 -59 -40 37
		mu 0 4 24 51 50 34
		f 4 10 -61 -29 26
		mu 0 4 12 52 51 24
		f 4 3 25 -63 -11
		mu 0 4 6 21 54 53
		f 4 -65 -26 22 18
		mu 0 4 55 54 21 16
		f 4 -67 -19 15 11
		mu 0 4 56 55 16 7
		f 4 -69 -12 -31 -68
		mu 0 4 58 57 10 31
		f 4 -71 67 -42 -70
		mu 0 4 59 58 31 41
		f 4 -72 69 -10 -49
		mu 0 4 45 59 41 11
		f 4 -2 74 75 -74
		mu 0 4 19 2 61 60
		f 4 -39 72 76 -75
		mu 0 4 2 36 62 61
		f 4 40 79 -81 -79
		mu 0 4 39 3 64 63
		f 4 -13 77 81 -80
		mu 0 4 3 14 65 64
		f 4 -20 73 82 -78
		mu 0 4 14 19 60 65
		f 4 0 84 -86 -84
		mu 0 4 0 22 67 66
		f 4 -5 83 87 -87
		mu 0 4 26 0 66 68
		f 4 24 88 -90 -85
		mu 0 4 22 17 69 67
		f 4 17 91 -93 -89
		mu 0 4 17 1 70 69
		f 4 5 90 -94 -92
		mu 0 4 1 29 71 70
		f 4 -28 86 94 -73
		mu 0 4 36 26 68 62
		f 4 29 78 -96 -91
		mu 0 4 29 39 63 71
		f 4 -76 100 101 -99
		mu 0 4 60 61 73 72
		f 4 -77 96 102 -101
		mu 0 4 61 62 74 73
		f 4 80 107 -109 -106
		mu 0 4 63 64 76 75
		f 4 -82 103 109 -108
		mu 0 4 64 65 77 76
		f 4 -83 98 111 -104
		mu 0 4 65 60 72 77
		f 4 85 113 -115 -113
		mu 0 4 66 67 79 78
		f 4 -88 112 118 -117
		mu 0 4 68 66 78 80
		f 4 89 120 -122 -114
		mu 0 4 67 69 81 79
		f 4 92 125 -127 -121
		mu 0 4 69 70 82 81
		f 4 93 123 -128 -126
		mu 0 4 70 71 83 82
		f 4 -95 116 129 -97
		mu 0 4 62 68 80 74
		f 4 95 105 -132 -124
		mu 0 4 71 63 75 83;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube3";
	setAttr ".t" -type "double3" -4.9620748466534952 52.914492938800642 2.7660860489410579 ;
	setAttr ".r" -type "double3" 0 0 42.635346382329715 ;
	setAttr ".s" -type "double3" 0.23519172975898775 0.23721122251055424 0.24154981405487555 ;
	setAttr ".rp" -type "double3" -0.39913775464548645 0.18568367563162599 0.11435852124974398 ;
	setAttr ".rpt" -type "double3" -0.020268773421029007 -0.31942797941575951 0 ;
	setAttr ".sp" -type "double3" -1.6970739364624023 0.78277778625488292 0.47343659400939941 ;
	setAttr ".spt" -type "double3" 1.2979361818169155 -0.59709411062325679 -0.35907807275965542 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 84 ".uvst[0].uvsp[0:83]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.16666666 0.375 0.16666666 0.125 0.16666666
		 0.375 0.58333337 0.625 0.58333337 0.875 0.16666666 0.625 0.083333328 0.375 0.083333328
		 0.125 0.083333328 0.375 0.66666669 0.625 0.66666669 0.875 0.083333328 0.54166663
		 0.25 0.54166663 0.5 0.54166663 0.58333337 0.54166663 0.66666669 0.54166663 0.75 0.54166663
		 0 0.54166663 1 0.54166663 0.083333328 0.54166663 0.16666666 0.45833331 0.25 0.45833331
		 0.5 0.45833331 0.58333337 0.45833331 0.66666669 0.45833331 0.75 0.45833331 0 0.45833331
		 1 0.45833331 0.083333328 0.45833331 0.16666666 0.45833331 0.375 0.25 0.25 0.375 0.375
		 0.25 0.16666666 0.25 0.083333328 0.25 0 0.375 0.875 0.45833331 0.875 0.54166663 0.875
		 0.625 0.875 0.75 0 0.75 0.083333328 0.75 0.16666666 0.625 0.375 0.75 0.25 0.54166663
		 0.375 0.375 0 0.45833331 0 0.375 0.083333328 0.45833331 0.25 0.375 0.25 0.375 0.16666666
		 0.54166663 0 0.625 0 0.625 0.083333328 0.625 0.16666666 0.625 0.25 0.54166663 0.25
		 0.375 0 0.45833331 0 0.375 0.083333328 0.45833331 0.25 0.375 0.25 0.375 0.16666666
		 0.54166663 0 0.625 0 0.625 0.083333328 0.625 0.16666666 0.625 0.25 0.54166663 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 66 ".pt";
	setAttr ".pt[0]" -type "float3" 1.2843963 -4.8256798 -1.8389417 ;
	setAttr ".pt[1]" -type "float3" 0 -5.9604645e-08 -4.6566129e-10 ;
	setAttr ".pt[2]" -type "float3" -4.7743568 -5.7533627 -1.6040829 ;
	setAttr ".pt[3]" -type "float3" 0.10067102 0.40525675 -4.6566129e-10 ;
	setAttr ".pt[4]" -type "float3" 1.4978535 -5.519618 -1.3947401 ;
	setAttr ".pt[5]" -type "float3" 0 -1.4401139 -1.3116382 ;
	setAttr ".pt[6]" -type "float3" 5.7001171 -3.3864074 -0.72044742 ;
	setAttr ".pt[7]" -type "float3" 5.9604645e-08 0 0 ;
	setAttr ".pt[8]" -type "float3" 0.10067102 0 -4.6566129e-10 ;
	setAttr ".pt[9]" -type "float3" -2.5965548 -5.6681018 -1.6040834 ;
	setAttr ".pt[10]" -type "float3" 2.8689373 -5.2108021 -1.3947401 ;
	setAttr ".pt[11]" -type "float3" 0 -1.4401139 -1.3116382 ;
	setAttr ".pt[12]" -type "float3" 0.10067102 0 -4.6566129e-10 ;
	setAttr ".pt[13]" -type "float3" -0.59249431 -5.2167172 -1.6040834 ;
	setAttr ".pt[14]" -type "float3" 5.1014347 -4.7079663 -1.3947409 ;
	setAttr ".pt[15]" -type "float3" 5.9604645e-08 -1.4401139 -1.3116382 ;
	setAttr ".pt[16]" -type "float3" 2.9802322e-08 -0.3893939 -4.6566129e-10 ;
	setAttr ".pt[17]" -type "float3" 0 -0.24379089 -1.3116382 ;
	setAttr ".pt[18]" -type "float3" 0 -0.22495773 -1.3116382 ;
	setAttr ".pt[19]" -type "float3" 0 -1.4401139 -1.3116382 ;
	setAttr ".pt[20]" -type "float3" 0.017963467 0.011229761 0.067784771 ;
	setAttr ".pt[21]" -type "float3" -0.0064292699 0.029006166 0.18453977 ;
	setAttr ".pt[22]" -type "float3" -4.1295619 -1.3358631 2.8048675 ;
	setAttr ".pt[23]" -type "float3" -1.1944944 -0.66565132 1.0774862 ;
	setAttr ".pt[24]" -type "float3" -1.0223634 -1.6066668 -1.9235909 ;
	setAttr ".pt[25]" -type "float3" 3.579107 -0.70733953 -0.58048052 ;
	setAttr ".pt[26]" -type "float3" 1.2491742 -0.81506234 5.4692869 ;
	setAttr ".pt[27]" -type "float3" -1.793368 -1.5463581 4.2361131 ;
	setAttr ".pt[28]" -type "float3" -1.5239714 0.55987895 1.3094687 ;
	setAttr ".pt[29]" -type "float3" -2.7425785 -5.7456903 -1.8716252 ;
	setAttr ".pt[30]" -type "float3" -0.60071534 -5.2632666 -1.8716252 ;
	setAttr ".pt[31]" -type "float3" 1.6317816 -4.7604299 -1.8716252 ;
	setAttr ".pt[32]" -type "float3" 3.8642786 -4.2575946 -1.8716252 ;
	setAttr ".pt[33]" -type "float3" -0.45383242 -1.5446943 5.0843759 ;
	setAttr ".pt[34]" -type "float3" 0.014757765 0.021104492 0.12750058 ;
	setAttr ".pt[35]" -type "float3" 5.9604645e-08 0 0 ;
	setAttr ".pt[36]" -type "float3" 5.9604645e-08 0 0 ;
	setAttr ".pt[39]" -type "float3" 0 0.85503006 0 ;
	setAttr ".pt[40]" -type "float3" 0.22545817 -3.0235667 0.95331538 ;
	setAttr ".pt[41]" -type "float3" -1.7846017 0.042710923 4.5036607 ;
	setAttr ".pt[42]" -type "float3" -1.7043223 -4.1608214 0.00032861531 ;
	setAttr ".pt[43]" -type "float3" -3.1180885 -5.0532012 0.0003286358 ;
	setAttr ".pt[44]" -type "float3" -3.1414306 0.95145255 3.9774375 ;
	setAttr ".pt[45]" -type "float3" -5.7673421 -3.6873512 0.089052595 ;
	setAttr ".pt[46]" -type "float3" -0.62269342 1.329654 0.88313091 ;
	setAttr ".pt[47]" -type "float3" -1.4867208 1.1895821 -0.19050486 ;
	setAttr ".pt[48]" -type "float3" -0.210436 0.61563021 -0.19050486 ;
	setAttr ".pt[49]" -type "float3" -0.21043602 0.041678131 -0.19050486 ;
	setAttr ".pt[50]" -type "float3" -0.49557331 1.5118084 1.3924009 ;
	setAttr ".pt[51]" -type "float3" -0.21043602 2.0375772 -0.017918505 ;
	setAttr ".pt[52]" -type "float3" 2.5155239 -0.37296385 3.4821095 ;
	setAttr ".pt[53]" -type "float3" -0.42360556 1.8279228 5.6444969 ;
	setAttr ".pt[54]" -type "float3" -0.74039924 0.78280705 -3.140455 ;
	setAttr ".pt[55]" -type "float3" 1.0103006 -2.0603919 1.7107041 ;
	setAttr ".pt[56]" -type "float3" -3.1252325 0.11783922 -5.3475599 ;
	setAttr ".pt[57]" -type "float3" -0.11911379 -1.0818067 -1.6563463 ;
	setAttr ".pt[58]" -type "float3" -1.9623834 -1.6637419 4.092176 ;
	setAttr ".pt[59]" -type "float3" -3.7162294 -2.4325619 1.1698622 ;
	setAttr ".pt[60]" -type "float3" -1.0524764 2.8665934 1.7919825 ;
	setAttr ".pt[61]" -type "float3" -0.77601004 1.5595238 -3.0985851 ;
	setAttr ".pt[62]" -type "float3" -3.0990877 2.6024358 -0.39095205 ;
	setAttr ".pt[63]" -type "float3" -1.2913311 1.3494198 -0.93836826 ;
	setAttr ".pt[64]" -type "float3" -1.8013738 2.9788129 -5.3613358 ;
	setAttr ".pt[65]" -type "float3" -1.2913311 1.8764583 -2.3162024 ;
	setAttr ".pt[66]" -type "float3" -1.1260583 -0.015630756 -0.35097831 ;
	setAttr ".pt[67]" -type "float3" -1.2913311 2.4763608 0.3832562 ;
	setAttr -s 68 ".vt[0:67]"  -12.10848999 -7.79967499 2.36908841 12.10848808 -7.79967499 2.36908841
		 -12.10848999 6.22385788 2.36908841 12.10848808 6.22385788 2.36908841 -13.48869896 5.80175018 -2.36908865
		 13.48869896 5.80175018 -2.36908865 -13.48869896 -7.81101608 -2.36908865 13.48869896 -7.81101608 -2.36908865
		 12.10848808 1.54934692 2.36908841 -12.10848999 1.54934692 2.36908841 -13.48869896 2.60367203 -2.36908865
		 13.48869896 2.60367203 -2.36908865 12.10848808 -3.12516403 2.36908841 -12.10848999 -3.12516403 2.36908841
		 -13.48869896 -2.60367203 -2.36908865 13.48869896 -2.60367203 -2.36908865 4.036161423 11.57725906 2.36908841
		 4.49623299 11.53801727 -2.36908865 4.49623299 2.60367203 -4.35003519 4.49623299 -2.60367203 -4.35003519
		 4.49623299 -7.81101608 -2.36908865 4.036161423 -7.79967499 2.36908841 -4.03616333 11.57725906 2.36908841
		 -4.49623299 11.53801727 -2.36908865 -4.49623299 2.60367203 -4.35003519 -4.49623299 -2.60367203 -4.35003519
		 -4.49623299 -7.81101608 -2.36908865 -4.03616333 -7.79967499 2.36908841 -4.49623299 13.33588028 0
		 -13.48869896 7.59961319 0 -13.48869896 2.60367203 0 -13.48869896 -2.60367203 0 -13.48869896 -7.81101608 0
		 -4.49623299 -7.81101608 0 4.49623299 -7.81101608 0 13.48869896 -7.81101608 0 13.48869896 -2.60367203 0
		 13.48869896 2.60367203 0 13.48869896 7.59961319 0 4.49623299 13.33588028 0 -10.028661728 -7.79100418 2.71683884
		 -3.34288788 -7.79100418 2.71683884 -10.028661728 -3.91941833 2.71683884 -10.028661728 -0.047828674 2.71683884
		 -3.34288788 8.25762558 2.71683884 -10.028661728 3.82375717 2.71683884 3.34288597 -7.79100418 2.71683884
		 10.028661728 -7.79100418 2.71683884 10.028661728 -3.91941833 2.71683884 10.028661728 -0.047828674 2.71683884
		 3.34288597 8.25762558 2.71683884 10.028661728 3.82375717 2.71683884 -10.028661728 -7.79100418 0.14674139
		 -3.34288788 -7.79100418 0.14674139 -3.34288788 -3.91941833 0.14674139 -10.028661728 -3.91941833 0.14674139
		 -3.34288788 -0.047828674 0.14674139 -10.028661728 -0.047828674 0.14674139 -3.34288788 8.25762558 0.14674139
		 -10.028661728 3.82375717 0.14674139 3.34288597 -7.79100418 0.14674139 3.34288597 -3.91941833 0.14674139
		 10.028661728 -7.79100418 0.14674139 10.028661728 -3.91941833 0.14674139 3.34288597 -0.047828674 0.14674139
		 10.028661728 -0.047828674 0.14674139 3.34288597 8.25762558 0.14674139 10.028661728 3.82375717 0.14674139;
	setAttr -s 132 ".ed[0:131]"  0 27 0 2 22 0 4 23 0 6 26 0 0 13 0 1 12 0
		 2 29 0 3 38 0 4 10 0 5 11 0 6 32 0 7 35 0 8 3 0 9 2 0 10 14 0 9 30 1 11 15 0 10 24 1
		 11 37 1 12 8 0 13 9 0 14 6 0 13 31 1 15 7 0 14 25 1 15 36 1 16 3 0 17 5 0 16 39 1
		 18 11 1 17 18 1 19 15 1 18 19 1 20 7 0 19 20 1 21 1 0 20 34 1 22 16 0 23 17 0 22 28 1
		 24 18 1 23 24 1 25 19 1 24 25 1 26 20 0 25 26 1 27 21 0 26 33 1 28 23 1 29 4 0 28 29 1
		 30 10 1 29 30 1 31 14 1 30 31 1 32 0 0 31 32 1 33 27 1 32 33 1 34 21 1 33 34 1 35 1 0
		 34 35 1 36 12 1 35 36 1 37 8 1 36 37 1 38 5 0 37 38 1 39 17 1 38 39 1 39 28 1 0 40 0
		 27 41 0 40 41 0 13 42 0 40 42 0 9 43 0 22 44 0 2 45 0 45 44 0 43 45 0 42 43 0 21 46 0
		 1 47 0 46 47 0 12 48 0 47 48 0 8 49 0 48 49 0 16 50 0 3 51 0 49 51 0 50 51 0 41 46 0
		 44 50 0 40 52 0 41 53 1 52 53 0 53 54 1 42 55 1 54 55 1 52 55 0 43 57 1 56 57 1 44 58 0
		 56 58 1 45 59 0 59 58 0 57 59 0 54 56 1 55 57 0 46 60 1 60 61 1 47 62 0 60 62 0 48 63 1
		 62 63 0 63 61 1 61 64 1 49 65 1 63 65 0 65 64 1 50 66 0 64 66 1 51 67 0 65 67 0 66 67 0
		 53 60 0 61 54 1 64 56 1 58 66 0;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 98 99 101 -103
		mu 0 4 72 73 42 74
		f 4 1 39 50 -7
		mu 0 4 2 35 44 46
		f 4 24 45 -4 -22
		mu 0 4 23 38 39 6
		f 4 58 57 -1 -56
		mu 0 4 50 51 41 8
		f 4 -62 64 63 -6
		mu 0 4 1 54 55 20
		f 4 55 4 22 56
		mu 0 4 49 0 21 48
		f 4 -105 106 -109 -110
		mu 0 4 77 43 75 76
		f 4 -16 13 6 52
		mu 0 4 47 15 2 45
		f 4 2 41 -18 -9
		mu 0 4 4 36 37 17
		f 4 -66 68 -8 -13
		mu 0 4 14 56 58 3
		f 4 -102 110 104 -112
		mu 0 4 74 42 43 77
		f 4 -23 20 15 54
		mu 0 4 48 21 15 47
		f 4 17 43 -25 -15
		mu 0 4 17 37 38 23
		f 4 -64 66 65 -20
		mu 0 4 20 55 56 14
		f 4 26 7 70 -29
		mu 0 4 26 3 57 59
		f 4 -31 27 9 -30
		mu 0 4 28 27 5 18
		f 4 -33 29 16 -32
		mu 0 4 29 28 18 24
		f 4 -35 31 23 -34
		mu 0 4 30 29 24 7
		f 4 -60 62 61 -36
		mu 0 4 32 52 53 9
		f 4 -114 115 117 118
		mu 0 4 33 78 79 80
		f 4 -120 -119 121 122
		mu 0 4 34 33 80 81
		f 4 -125 -123 126 -128
		mu 0 4 83 34 81 82
		f 4 37 28 71 -40
		mu 0 4 35 26 59 44
		f 4 -42 38 30 -41
		mu 0 4 37 36 27 28
		f 4 -44 40 32 -43
		mu 0 4 38 37 28 29
		f 4 -46 42 34 -45
		mu 0 4 39 38 29 30
		f 4 -58 60 59 -47
		mu 0 4 41 51 52 32
		f 4 -100 128 113 129
		mu 0 4 42 73 78 33
		f 4 -111 -130 119 130
		mu 0 4 43 42 33 34
		f 4 -107 -131 124 -132
		mu 0 4 75 43 34 83
		f 4 -51 48 -3 -50
		mu 0 4 46 44 36 4
		f 4 -52 -53 49 8
		mu 0 4 16 47 45 13
		f 4 -54 -55 51 14
		mu 0 4 22 48 47 16
		f 4 10 -57 53 21
		mu 0 4 12 49 48 22
		f 4 3 47 -59 -11
		mu 0 4 6 39 51 50
		f 4 -61 -48 44 36
		mu 0 4 52 51 39 30
		f 4 -63 -37 33 11
		mu 0 4 53 52 30 7
		f 4 -65 -12 -24 25
		mu 0 4 55 54 10 25
		f 4 -67 -26 -17 18
		mu 0 4 56 55 25 19
		f 4 -69 -19 -10 -68
		mu 0 4 58 56 19 11
		f 4 -71 67 -28 -70
		mu 0 4 59 57 5 27
		f 4 -72 69 -39 -49
		mu 0 4 44 59 27 36
		f 4 0 73 -75 -73
		mu 0 4 0 40 61 60
		f 4 -5 72 76 -76
		mu 0 4 21 0 60 62
		f 4 -2 79 80 -79
		mu 0 4 35 2 64 63
		f 4 -14 77 81 -80
		mu 0 4 2 15 65 64
		f 4 -21 75 82 -78
		mu 0 4 15 21 62 65
		f 4 35 84 -86 -84
		mu 0 4 31 1 67 66
		f 4 5 86 -88 -85
		mu 0 4 1 20 68 67
		f 4 19 88 -90 -87
		mu 0 4 20 14 69 68
		f 4 12 91 -93 -89
		mu 0 4 14 3 70 69
		f 4 -27 90 93 -92
		mu 0 4 3 26 71 70
		f 4 46 83 -95 -74
		mu 0 4 40 31 66 61
		f 4 -38 78 95 -91
		mu 0 4 26 35 63 71
		f 4 74 97 -99 -97
		mu 0 4 60 61 73 72
		f 4 -77 96 102 -101
		mu 0 4 62 60 72 74
		f 4 -81 107 108 -106
		mu 0 4 63 64 76 75
		f 4 -82 103 109 -108
		mu 0 4 64 65 77 76
		f 4 -83 100 111 -104
		mu 0 4 65 62 74 77
		f 4 85 114 -116 -113
		mu 0 4 66 67 79 78
		f 4 87 116 -118 -115
		mu 0 4 67 68 80 79
		f 4 89 120 -122 -117
		mu 0 4 68 69 81 80
		f 4 92 125 -127 -121
		mu 0 4 69 70 82 81
		f 4 -94 123 127 -126
		mu 0 4 70 71 83 82
		f 4 94 112 -129 -98
		mu 0 4 61 66 78 73
		f 4 -96 105 131 -124
		mu 0 4 71 63 75 83;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube4";
	setAttr ".t" -type "double3" 0.3708903563442858 30.443111111161969 4.2919512290179709 ;
	setAttr ".s" -type "double3" 0.78741768751847296 0.78741768751847296 0.78741768751847296 ;
	setAttr ".rp" -type "double3" -3.7547001243518494e-07 -0.20293853796111799 0.86985475703870485 ;
	setAttr ".sp" -type "double3" -4.76837158203125e-07 -0.25772666931152344 1.1046929359436035 ;
	setAttr ".spt" -type "double3" 1.0136714576794004e-07 0.054788131350405431 -0.23483817890489864 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 76 ".uvst[0].uvsp[0:75]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 48 ".pt";
	setAttr ".pt[0]" -type "float3" -1.0332357 0.12373174 -2.2311838 ;
	setAttr ".pt[1]" -type "float3" -3.8681819 0 0 ;
	setAttr ".pt[2]" -type "float3" 3.0970895 0 0 ;
	setAttr ".pt[3]" -type "float3" 1.4263548 0.21099307 -3.2535343 ;
	setAttr ".pt[4]" -type "float3" -2.1590011 2.6051674 -0.94832659 ;
	setAttr ".pt[5]" -type "float3" -3.687892 2.7720337 -2.3754854 ;
	setAttr ".pt[6]" -type "float3" 3.7825682 2.7698612 -2.3521795 ;
	setAttr ".pt[7]" -type "float3" 2.9491293 2.2748857 -2.3121188 ;
	setAttr ".pt[8]" -type "float3" -0.91501874 0.49704736 -2.5425489 ;
	setAttr ".pt[9]" -type "float3" -3.6657872 0.6787793 -3.1152575 ;
	setAttr ".pt[10]" -type "float3" 3.8681819 0.6787793 -3.1152575 ;
	setAttr ".pt[11]" -type "float3" 1.6069568 1.2793652 -3.5348804 ;
	setAttr ".pt[12]" -type "float3" 1.9067851 0.74897027 1.0647085 ;
	setAttr ".pt[15]" -type "float3" -1.7456605 1.746153 0.24365327 ;
	setAttr ".pt[16]" -type "float3" -0.76863712 0 0 ;
	setAttr ".pt[17]" -type "float3" -0.038639545 0 0 ;
	setAttr ".pt[18]" -type "float3" 0.038639534 0 0 ;
	setAttr ".pt[19]" -type "float3" 0.768637 0 0 ;
	setAttr ".pt[20]" -type "float3" -0.76863712 0 0 ;
	setAttr ".pt[21]" -type "float3" -0.038639545 0 0 ;
	setAttr ".pt[22]" -type "float3" 0.038639534 0 0 ;
	setAttr ".pt[23]" -type "float3" 0.768637 0 0 ;
	setAttr ".pt[24]" -type "float3" -0.17073312 0 0 ;
	setAttr ".pt[27]" -type "float3" 0.17073303 0 0 ;
	setAttr ".pt[28]" -type "float3" 0 0 3.9272902 ;
	setAttr ".pt[29]" -type "float3" 0 0 -2.3841858e-07 ;
	setAttr ".pt[30]" -type "float3" 0 0 -2.3841858e-07 ;
	setAttr ".pt[31]" -type "float3" 0 0 3.9272902 ;
	setAttr ".pt[32]" -type "float3" 0 7.6680698 3.2429624 ;
	setAttr ".pt[33]" -type "float3" 0 7.1290021 3.9117088 ;
	setAttr ".pt[34]" -type "float3" 0 7.1290021 3.9117088 ;
	setAttr ".pt[35]" -type "float3" 0 7.6680698 3.2429628 ;
	setAttr ".pt[36]" -type "float3" 0 7.5776916 -0.77381533 ;
	setAttr ".pt[37]" -type "float3" 0 3.5527137e-15 -2.116272 ;
	setAttr ".pt[38]" -type "float3" 0 3.5527137e-15 -2.116272 ;
	setAttr ".pt[39]" -type "float3" 0 7.5776916 -0.77381533 ;
	setAttr ".pt[40]" -type "float3" 5.9604645e-08 0.19042674 -0.1701187 ;
	setAttr ".pt[43]" -type "float3" 0 0.19042678 -0.1701187 ;
	setAttr ".pt[44]" -type "float3" 6.7055225e-08 -4.4703484e-08 0.11781749 ;
	setAttr ".pt[47]" -type "float3" -7.4505806e-09 0 0.11781749 ;
	setAttr ".pt[48]" -type "float3" -0.54460621 3.3649807 2.2167764 ;
	setAttr ".pt[49]" -type "float3" -0.54558659 3.360033 -3.5992119 ;
	setAttr ".pt[50]" -type "float3" -1.7911333 0 3.1704302 ;
	setAttr ".pt[51]" -type "float3" -1.7911333 0 0 ;
	setAttr ".pt[52]" -type "float3" 0.54460621 3.3649807 2.2167768 ;
	setAttr ".pt[53]" -type "float3" 0.54558635 3.360033 -3.5992115 ;
	setAttr ".pt[54]" -type "float3" 1.7911333 0 3.1704302 ;
	setAttr ".pt[55]" -type "float3" 1.7911333 0 0 ;
	setAttr -s 56 ".vt[0:55]"  -10.038804054 -9.62135029 11.77618599 -2.4998126 -10.22464752 13.38854122
		 2.004843235 -10.10032463 13.75727081 8.34945774 -9.81797504 12.3129797 -10.74075127 -5.29835415 12.68445396
		 -2.3689177 -4.053906918 19.2712059 2.50353289 -4.053906918 19.2712059 9.055687904 -5.44682884 13.2313652
		 -10.74075127 3.76909184 12.40200996 -2.3689177 6.2711463 12.74427414 2.50353289 6.2711463 12.74427414
		 9.055687904 1.72157979 12.96776295 -5.88720655 11.83472252 5.4399519 -3.11799955 11.83472252 5.4399519
		 3.1179986 11.83472252 6.38708544 5.88720512 11.83472252 6.38708544 -5.3156786 15.091119766 1.78038263
		 -2.35024023 15.091119766 4.41633511 2.35023928 15.091119766 5.36346769 5.31567764 15.091119766 2.72751451
		 -5.3156786 15.091119766 -1.78038168 -2.35024023 15.091119766 -4.41633415 2.35023928 15.091119766 -4.41633415
		 5.31567764 15.091119766 -1.78038168 -3.0082259178 14.63074589 -3.45897102 -1.0027422905 14.63074589 -5.79053736
		 1.0027406216 14.63074589 -5.79053736 3.0082240105 14.63074589 -3.45897102 -9.35399914 6.62115335 -9.35399914
		 -3.11799955 6.62115335 -9.35399818 3.11799836 6.62115288 -9.35399818 9.35399723 6.62115335 -9.35399628
		 -9.35399914 -3.1179986 -9.35399818 -3.11799955 -1.46168184 -11.51995087 3.11799812 -1.46168184 -11.51995087
		 9.35399723 -3.1179986 -9.35399818 -9.35399914 -9.35399818 -10.42741489 -3.11799955 -7.69768143 -12.59336758
		 3.1179986 -7.69768143 -12.59336758 9.35399723 -9.35399818 -10.42741489 -13.24900436 -8.73910332 -4.41633511
		 -4.41633511 -15.39583778 -4.41633511 4.41633415 -15.39583778 -4.41633511 13.24900341 -8.73910332 -4.41633511
		 -13.24900436 -8.73910332 4.41633415 -4.16166687 -15.6065731 7.97652054 4.65663433 -15.19758129 8.27034569
		 13.24900341 -8.73910332 4.41633415 13.24900436 -4.41633511 -4.41633463 13.24900436 -4.41633511 4.41633415
		 13.24900436 7.70661736 -4.41633511 13.24900436 7.70661783 4.41633415 -13.24900436 -4.41633511 -4.41633511
		 -13.24900436 -4.41633511 4.41633415 -13.24900436 7.70661783 -4.41633415 -13.24900436 7.70661783 4.41633415;
	setAttr -s 108 ".ed[0:107]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1
		 9 10 1 10 11 1 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1
		 24 25 0 25 26 0 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0
		 38 39 0 40 41 1 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 4 0 1 5 1 2 6 1 3 7 0 4 8 0
		 5 9 1 6 10 1 7 11 0 12 16 0 13 17 1 14 18 1 15 19 0 16 20 0 17 21 1 18 22 1 19 23 0
		 20 24 0 21 25 1 22 26 1 23 27 0 24 28 0 25 29 1 26 30 1 27 31 0 28 32 0 29 33 1 30 34 1
		 31 35 0 32 36 0 33 37 1 34 38 1 35 39 0 36 40 0 37 41 1 38 42 1 39 43 0 40 44 0 41 45 1
		 42 46 1 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1 48 49 1 49 7 1 31 50 1 50 51 1
		 51 11 1 43 48 1 47 49 1 48 50 1 49 51 1 51 19 1 32 52 1 52 53 1 53 4 1 28 54 1 54 55 1
		 55 8 1 40 52 1 44 53 1 52 54 1 53 55 1 11 15 0 10 14 1 9 13 1 8 12 0 55 16 1 54 20 1
		 50 23 1;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 37 -4 -37
		mu 0 4 0 1 5 4
		f 4 1 38 -5 -38
		mu 0 4 1 2 6 5
		f 4 2 39 -6 -39
		mu 0 4 2 3 7 6
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 103 -10 -105
		mu 0 4 8 9 13 12
		f 4 7 102 -11 -104
		mu 0 4 9 10 14 13
		f 4 8 101 -12 -103
		mu 0 4 10 11 15 14
		f 4 9 45 -13 -45
		mu 0 4 12 13 17 16
		f 4 10 46 -14 -46
		mu 0 4 13 14 18 17
		f 4 11 47 -15 -47
		mu 0 4 14 15 19 18
		f 4 12 49 -16 -49
		mu 0 4 16 17 21 20
		f 4 13 50 -17 -50
		mu 0 4 17 18 22 21
		f 4 14 51 -18 -51
		mu 0 4 18 19 23 22
		f 4 15 53 -19 -53
		mu 0 4 20 21 25 24
		f 4 16 54 -20 -54
		mu 0 4 21 22 26 25
		f 4 17 55 -21 -55
		mu 0 4 22 23 27 26
		f 4 -22 -57 18 57
		mu 0 4 29 28 24 25
		f 4 -23 -58 19 58
		mu 0 4 30 29 25 26
		f 4 -24 -59 20 59
		mu 0 4 31 30 26 27
		f 4 21 61 -25 -61
		mu 0 4 28 29 33 32
		f 4 22 62 -26 -62
		mu 0 4 29 30 34 33
		f 4 23 63 -27 -63
		mu 0 4 30 31 35 34
		f 4 24 65 -28 -65
		mu 0 4 32 33 37 36
		f 4 25 66 -29 -66
		mu 0 4 33 34 38 37
		f 4 26 67 -30 -67
		mu 0 4 34 35 39 38
		f 4 27 69 -31 -69
		mu 0 4 36 37 41 40
		f 4 28 70 -32 -70
		mu 0 4 37 38 42 41
		f 4 29 71 -33 -71
		mu 0 4 38 39 43 42
		f 4 30 73 -34 -73
		mu 0 4 40 41 45 44
		f 4 31 74 -35 -74
		mu 0 4 41 42 46 45
		f 4 32 75 -36 -75
		mu 0 4 42 43 47 46
		f 4 33 77 -1 -77
		mu 0 4 44 45 49 48
		f 4 34 78 -2 -78
		mu 0 4 45 46 50 49
		f 4 35 79 -3 -79
		mu 0 4 46 47 51 50
		f 4 -72 -68 80 -87
		mu 0 4 53 52 55 56
		f 4 -76 86 81 -88
		mu 0 4 54 53 56 57
		f 4 -80 87 82 -40
		mu 0 4 3 54 57 7
		f 4 -81 -64 83 -89
		mu 0 4 56 55 58 59
		f 4 -82 88 84 -90
		mu 0 4 57 56 59 60
		f 4 -83 89 85 -44
		mu 0 4 7 57 60 11
		f 4 -84 -60 -56 -108
		mu 0 4 59 58 61 62
		f 4 -91 -85 107 -52
		mu 0 4 63 60 59 62
		f 4 -86 90 -48 -102
		mu 0 4 11 60 63 15
		f 4 68 97 -92 64
		mu 0 4 64 65 68 67
		f 4 72 98 -93 -98
		mu 0 4 65 66 69 68
		f 4 76 36 -94 -99
		mu 0 4 66 0 4 69
		f 4 91 99 -95 60
		mu 0 4 67 68 71 70
		f 4 92 100 -96 -100
		mu 0 4 68 69 72 71
		f 4 93 40 -97 -101
		mu 0 4 69 4 8 72
		f 4 94 106 52 56
		mu 0 4 70 71 74 73
		f 4 95 105 48 -107
		mu 0 4 71 72 75 74
		f 4 96 104 44 -106
		mu 0 4 72 8 12 75;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube5";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 -7.1054273576010019e-15 -1.4718650860341462 ;
	setAttr ".rp" -type "double3" 18.181160686045317 39.031001219787953 5.6333046228546451 ;
	setAttr ".sp" -type "double3" 18.181160686045317 39.031001219787953 5.6333046228546451 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 90 ".uvst[0].uvsp[0:89]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.625 0.041666668 0.54166669 0.041666668 0.45833334
		 0.041666668 0.375 0.041666668 0.29166669 0.041666668 0.20833334 0.041666668 0.125
		 0.041666668 0.375 0.70833325 0.45833334 0.70833325 0.54166669 0.70833325 0.625 0.70833325
		 0.875 0.041666668 0.79166669 0.041666668 0.70833337 0.041666668;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".vt[0:67]"  25.77513695 36.49128723 8.63676739 25.75078773 37.90899277 9.42879963
		 25.70779419 40.41228867 9.42879963 25.68344498 41.8299942 8.63676739 16.69398499 36.63603973 8.29850483
		 16.67237663 37.89408112 9.0013360977 16.63422585 40.11544418 9.0013360977 16.6126194 41.37348175 8.29850483
		 14.17793941 36.59282684 8.29850483 14.15633202 37.85086823 9.0013360977 14.11818123 40.072227478 9.0013360977
		 14.09657383 41.33026886 8.29850483 6.57408714 37.76934814 7.73615742 6.20261478 38.68997192 8.29069328
		 5.54669094 40.31554794 8.29069328 5.17521954 41.23617172 7.73615742 6.85853863 37.064395905 6.51910067
		 6.20261478 38.68997192 6.51910067 5.54669094 40.31554794 6.51910067 4.89076805 41.94112396 6.51910067
		 6.85853863 37.064395905 4.74750805 6.20261478 38.68997192 4.74750805 5.54669094 40.31554794 4.74750805
		 4.89076805 41.94112396 4.74750805 6.57408714 37.76934814 3.53045201 6.20261478 38.68997192 2.97591591
		 5.54669094 40.31554794 2.97591591 5.17521954 41.23617172 3.53045201 14.17794037 36.59282684 2.96810412
		 14.15633297 37.85086823 2.26527309 14.11818123 40.072227478 2.26527309 14.096574783 41.33026886 2.96810412
		 16.69398499 36.63603973 2.96810412 16.67237854 37.89408112 2.26527309 16.63422585 40.11544418 2.26527309
		 16.6126194 41.37348175 2.96810412 25.77513695 36.49128723 2.6298418 25.75078773 37.90899277 1.83780932
		 25.70779419 40.41228867 1.83780932 25.68344498 41.8299942 2.6298418 25.79378128 35.40570068 4.36813927
		 25.75078773 37.90899277 4.36813927 25.70779419 40.41228867 4.36813927 25.66480064 42.91558075 4.36813927
		 25.79378128 35.40570068 6.89846992 25.75078773 37.90899277 6.89846992 25.70779419 40.41228867 6.89846992
		 25.66480064 42.91558075 6.89846992 16.59607315 42.33680344 4.51062775 16.59607315 42.33680344 6.75598192
		 14.080028534 42.29359055 4.51062775 14.080028534 42.29359055 6.75598192 16.71052933 35.67271805 4.51062775
		 16.71052933 35.67271805 6.75598192 14.19448471 35.62950516 4.51062775 14.19448471 35.62950516 6.75598192
		 23.45065689 41.95286179 8.8181057 23.47634888 40.44955444 9.65795422 23.52193832 37.79512405 9.65795422
		 23.54788589 36.29182053 8.8181057 23.56771088 35.14068985 6.97485733 23.56771088 35.14068985 4.29175234
		 23.54788589 36.29182053 2.44850302 23.52194214 37.79512405 1.60865498 23.47635269 40.44955444 1.60865498
		 23.4506588 41.95286179 2.44850302 23.43094063 43.10399628 4.29175234 23.43094063 43.10399628 6.97485733;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1
		 9 10 1 10 11 1 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1
		 24 25 0 25 26 0 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0
		 38 39 0 40 41 1 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 59 0 1 58 1 2 57 1 3 56 0
		 4 8 0 5 9 1 6 10 1 7 11 0 8 12 0 9 13 1 10 14 1 11 15 0 12 16 0 13 17 1 14 18 1 15 19 0
		 16 20 0 17 21 1 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 28 0 25 29 1 26 30 1
		 27 31 0 28 32 0 29 33 1 30 34 1 31 35 0 32 62 0 33 63 1 34 64 1 35 65 0 36 40 0 37 41 1
		 38 42 1 39 43 0 40 44 0 41 45 1 42 46 1 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1
		 48 49 1 49 7 1 31 50 1 50 51 1 51 11 1 43 66 1 47 67 1 48 50 1 49 51 1 50 23 1 51 19 1
		 32 52 1 52 53 1 53 4 1 28 54 1 54 55 1 55 8 1 40 61 1 44 60 1 52 54 1 53 55 1 54 20 1
		 55 16 1 56 7 0 57 6 1 56 57 1 58 5 1 57 58 1 59 4 0 58 59 1 60 53 1 59 60 1 61 52 1
		 60 61 1 62 36 0 61 62 1 63 37 1 62 63 1 64 38 1 63 64 1 65 39 0 64 65 1 66 48 1 65 66 1
		 67 49 1 66 67 1 67 56 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 37 114 -37
		mu 0 4 0 1 78 79
		f 4 1 38 112 -38
		mu 0 4 1 2 77 78
		f 4 2 39 110 -39
		mu 0 4 2 3 76 77
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 -10 -45
		mu 0 4 8 9 13 12
		f 4 7 46 -11 -46
		mu 0 4 9 10 14 13
		f 4 8 47 -12 -47
		mu 0 4 10 11 15 14
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 18 61 -22 -61
		mu 0 4 24 25 29 28
		f 4 19 62 -23 -62
		mu 0 4 25 26 30 29
		f 4 20 63 -24 -63
		mu 0 4 26 27 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 122 121 -28 -120
		mu 0 4 83 84 37 36
		f 4 124 123 -29 -122
		mu 0 4 84 85 38 37
		f 4 126 125 -30 -124
		mu 0 4 85 86 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 31 78 -35 -78
		mu 0 4 41 42 46 45
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -126 128 -91
		mu 0 4 53 52 87 88
		f 4 -80 90 130 -92
		mu 0 4 54 53 88 89
		f 4 -84 91 131 -40
		mu 0 4 3 54 89 76
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -64 -60 -95
		mu 0 4 59 58 61 62
		f 4 -89 94 -56 -96
		mu 0 4 60 59 62 63
		f 4 -90 95 -52 -48
		mu 0 4 11 60 63 15
		f 4 72 102 120 119
		mu 0 4 64 65 81 82
		f 4 76 103 118 -103
		mu 0 4 65 66 80 81
		f 4 80 36 116 -104
		mu 0 4 66 0 79 80
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 56 60
		mu 0 4 70 71 74 73
		f 4 100 107 52 -107
		mu 0 4 71 72 75 74
		f 4 101 44 48 -108
		mu 0 4 72 8 12 75
		f 4 -111 108 -6 -110
		mu 0 4 77 76 7 6
		f 4 -113 109 -5 -112
		mu 0 4 78 77 6 5
		f 4 -115 111 -4 -114
		mu 0 4 79 78 5 4
		f 4 -117 113 -99 -116
		mu 0 4 80 79 4 69
		f 4 -119 115 -98 -118
		mu 0 4 81 80 69 68
		f 4 -121 117 -97 68
		mu 0 4 82 81 68 67
		f 4 24 69 -123 -69
		mu 0 4 32 33 84 83
		f 4 25 70 -125 -70
		mu 0 4 33 34 85 84
		f 4 26 71 -127 -71
		mu 0 4 34 35 86 85
		f 4 -129 -72 84 -128
		mu 0 4 88 87 55 56
		f 4 -131 127 85 -130
		mu 0 4 89 88 56 57
		f 4 -132 129 86 -109
		mu 0 4 76 89 57 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube7";
	setAttr ".t" -type "double3" 0.71803434543372902 0 0 ;
	setAttr ".rp" -type "double3" -6.279872406307911 14.727339950824234 5.6333046228546504 ;
	setAttr ".sp" -type "double3" -6.279872406307911 14.727339950824234 5.6333046228546504 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 90 ".uvst[0].uvsp[0:89]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.625 0.041666668 0.54166669 0.041666668 0.45833334
		 0.041666668 0.375 0.041666668 0.29166669 0.041666668 0.20833334 0.041666668 0.125
		 0.041666668 0.375 0.70833325 0.45833334 0.70833325 0.54166669 0.70833325 0.625 0.70833325
		 0.875 0.041666668 0.79166669 0.041666668 0.70833337 0.041666668;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt[0:67]" -type "float3"  -4.9697666 16.441423 4.0681953 
		-5.6655707 16.441423 3.6554651 -6.8941741 16.441423 3.6554651 -7.5899773 16.441423 
		4.0681953 -4.5610528 17.123217 3.6508958 -5.4739261 17.123217 3.1281214 -7.0858188 
		17.123217 3.1281214 -7.998692 17.123217 3.6508958 -4.5610528 12.432531 3.6508958 
		-5.4739261 12.432531 3.1281214 -7.0858188 12.432531 3.1281214 -7.998692 12.432531 
		3.6508958 -3.1950037 12.11798 3.6508958 -4.5699067 10.948809 3.1281214 -7.0858188 
		10.428084 3.1281214 -7.998692 10.428084 3.6508958 -2.1421938 13.013254 4.7982435 
		-4.5699067 10.948809 4.7982435 -7.0858188 10.428084 4.7982435 -8.697711 10.428084 
		4.7982435 -2.1421938 13.013254 6.4683661 -4.5699072 10.948809 6.4683661 -7.0858188 
		10.428084 6.4683661 -8.697711 10.428084 6.4683661 -3.1950037 12.11798 7.6157136 -4.5699072 
		10.948809 8.1384878 -7.0858188 10.428084 8.1384878 -7.998692 10.428084 7.6157136 
		-4.5610528 12.432531 7.6157136 -5.4739261 12.432531 8.1384878 -7.0858188 12.432531 
		8.1384878 -7.998692 12.432531 7.6157136 -4.5610528 17.123217 7.6157136 -5.4739261 
		17.123217 8.1384878 -7.0858188 17.123217 8.1384878 -7.998692 17.123217 7.6157136 
		-4.9697666 16.441423 7.1984138 -5.6655707 16.441423 7.6111441 -6.8941741 16.441423 
		7.6111441 -7.5899773 16.441423 7.1984138 -4.4369664 16.441423 6.2925849 -5.6655707 
		16.441423 6.2925849 -6.8941741 16.441423 6.2925849 -8.1227779 16.441423 6.2925849 
		-4.4369664 16.441423 4.9740248 -5.6655707 16.441423 4.9740248 -6.8941741 16.441423 
		4.9740248 -8.1227779 16.441423 4.9740248 -8.697711 17.123217 6.4683657 -8.697711 
		17.123217 4.7982435 -8.697711 12.432531 6.4683657 -8.697711 12.432531 4.7982435 -3.8620341 
		17.123217 6.4683657 -3.8620341 17.123217 4.7982435 -3.8620341 12.432531 6.4683657 
		-3.8620341 12.432531 4.7982435 -7.3708692 15.133828 4.2919054 -6.7914381 15.133965 
		3.9381633 -5.7683067 15.133965 3.9381633 -5.1888752 15.133828 4.2919054 -4.7451854 
		15.133771 5.0682607 -4.7451854 15.133771 6.198349 -5.1888752 15.133828 6.9747038 
		-5.7683067 15.133964 7.3284464 -6.7914381 15.133964 7.3284464 -7.3708692 15.133828 
		6.9747038 -7.8145599 15.133771 6.198349 -7.8145599 15.133771 5.0682607;
	setAttr -s 68 ".vt[0:67]"  -4.93912172 -11.065385818 5.27035427 -2.31592989 -11.065385818 6.66018152
		 2.31592989 -11.065385818 6.66018152 4.93912172 -11.065385818 5.27035427 -4.93912172 -3.68846178 5.27035427
		 -2.31592989 -3.68846178 6.66018152 2.31592989 -3.68846178 6.66018152 4.93912172 -3.68846178 5.27035427
		 -4.93912172 3.68846226 5.27035427 -2.31592989 3.68846226 6.66018152 2.31592989 3.68846226 6.66018152
		 4.93912172 3.68846226 5.27035427 -4.93912172 11.065385818 5.27035427 -2.31592989 11.065385818 6.66018152
		 2.31592989 11.065385818 6.66018152 4.93912172 11.065385818 5.27035427 -6.94778967 11.065385818 2.22006035
		 -2.31592989 11.065385818 2.22006035 2.31592989 11.065385818 2.22006035 6.94778967 11.065385818 2.22006035
		 -6.94778967 11.065385818 -2.22006083 -2.31592989 11.065385818 -2.22006083 2.31592989 11.065385818 -2.22006083
		 6.94778967 11.065385818 -2.22006083 -4.93912172 11.065385818 -5.27035427 -2.31592989 11.065385818 -6.66018152
		 2.31592989 11.065385818 -6.66018152 4.93912172 11.065385818 -5.27035427 -4.93912172 3.68846178 -5.27035427
		 -2.31592989 3.68846178 -6.66018152 2.31592989 3.68846178 -6.66018152 4.93912172 3.68846178 -5.27035427
		 -4.93912172 -3.68846226 -5.27035427 -2.31592989 -3.68846226 -6.66018152 2.31592989 -3.68846226 -6.66018152
		 4.93912172 -3.68846226 -5.27035427 -4.93912172 -11.065385818 -5.27035427 -2.31592989 -11.065385818 -6.66018152
		 2.31592989 -11.065385818 -6.66018152 4.93912172 -11.065385818 -5.27035427 -6.94778967 -11.065385818 -2.22006035
		 -2.31592989 -11.065385818 -2.22006035 2.31592989 -11.065385818 -2.22006035 6.94778967 -11.065385818 -2.22006035
		 -6.94778967 -11.065385818 2.22006083 -2.31592989 -11.065385818 2.22006083 2.31592989 -11.065385818 2.22006083
		 6.94778967 -11.065385818 2.22006083 6.94778967 -3.68846178 -2.22006035 6.94778967 -3.68846178 2.22006083
		 6.94778967 3.68846226 -2.22006035 6.94778967 3.68846226 2.22006083 -6.94778967 -3.68846178 -2.22006035
		 -6.94778967 -3.68846178 2.22006083 -6.94778967 3.68846226 -2.22006035 -6.94778967 3.68846226 2.22006083
		 4.93912172 -7.37692356 5.27035427 2.31592989 -7.37692356 6.66018152 -2.31592989 -7.37692356 6.66018152
		 -4.93912172 -7.37692356 5.27035427 -6.94778967 -7.37692356 2.22006083 -6.94778967 -7.37692356 -2.22006035
		 -4.93912172 -7.37692404 -5.27035427 -2.31592989 -7.37692404 -6.66018152 2.31592989 -7.37692404 -6.66018152
		 4.93912172 -7.37692404 -5.27035427 6.94778967 -7.37692356 -2.22006035 6.94778967 -7.37692356 2.22006083;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1
		 9 10 1 10 11 1 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1
		 24 25 0 25 26 0 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0
		 38 39 0 40 41 1 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 59 0 1 58 1 2 57 1 3 56 0
		 4 8 0 5 9 1 6 10 1 7 11 0 8 12 0 9 13 1 10 14 1 11 15 0 12 16 0 13 17 1 14 18 1 15 19 0
		 16 20 0 17 21 1 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 28 0 25 29 1 26 30 1
		 27 31 0 28 32 0 29 33 1 30 34 1 31 35 0 32 62 0 33 63 1 34 64 1 35 65 0 36 40 0 37 41 1
		 38 42 1 39 43 0 40 44 0 41 45 1 42 46 1 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1
		 48 49 1 49 7 1 31 50 1 50 51 1 51 11 1 43 66 1 47 67 1 48 50 1 49 51 1 50 23 1 51 19 1
		 32 52 1 52 53 1 53 4 1 28 54 1 54 55 1 55 8 1 40 61 1 44 60 1 52 54 1 53 55 1 54 20 1
		 55 16 1 56 7 0 57 6 1 56 57 1 58 5 1 57 58 1 59 4 0 58 59 1 60 53 1 59 60 1 61 52 1
		 60 61 1 62 36 0 61 62 1 63 37 1 62 63 1 64 38 1 63 64 1 65 39 0 64 65 1 66 48 1 65 66 1
		 67 49 1 66 67 1 67 56 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 37 114 -37
		mu 0 4 0 1 78 79
		f 4 1 38 112 -38
		mu 0 4 1 2 77 78
		f 4 2 39 110 -39
		mu 0 4 2 3 76 77
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 -10 -45
		mu 0 4 8 9 13 12
		f 4 7 46 -11 -46
		mu 0 4 9 10 14 13
		f 4 8 47 -12 -47
		mu 0 4 10 11 15 14
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 18 61 -22 -61
		mu 0 4 24 25 29 28
		f 4 19 62 -23 -62
		mu 0 4 25 26 30 29
		f 4 20 63 -24 -63
		mu 0 4 26 27 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 122 121 -28 -120
		mu 0 4 83 84 37 36
		f 4 124 123 -29 -122
		mu 0 4 84 85 38 37
		f 4 126 125 -30 -124
		mu 0 4 85 86 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 31 78 -35 -78
		mu 0 4 41 42 46 45
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -126 128 -91
		mu 0 4 53 52 87 88
		f 4 -80 90 130 -92
		mu 0 4 54 53 88 89
		f 4 -84 91 131 -40
		mu 0 4 3 54 89 76
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -64 -60 -95
		mu 0 4 59 58 61 62
		f 4 -89 94 -56 -96
		mu 0 4 60 59 62 63
		f 4 -90 95 -52 -48
		mu 0 4 11 60 63 15
		f 4 72 102 120 119
		mu 0 4 64 65 81 82
		f 4 76 103 118 -103
		mu 0 4 65 66 80 81
		f 4 80 36 116 -104
		mu 0 4 66 0 79 80
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 56 60
		mu 0 4 70 71 74 73
		f 4 100 107 52 -107
		mu 0 4 71 72 75 74
		f 4 101 44 48 -108
		mu 0 4 72 8 12 75
		f 4 -111 108 -6 -110
		mu 0 4 77 76 7 6
		f 4 -113 109 -5 -112
		mu 0 4 78 77 6 5
		f 4 -115 111 -4 -114
		mu 0 4 79 78 5 4
		f 4 -117 113 -99 -116
		mu 0 4 80 79 4 69
		f 4 -119 115 -98 -118
		mu 0 4 81 80 69 68
		f 4 -121 117 -97 68
		mu 0 4 82 81 68 67
		f 4 24 69 -123 -69
		mu 0 4 32 33 84 83
		f 4 25 70 -125 -70
		mu 0 4 33 34 85 84
		f 4 26 71 -127 -71
		mu 0 4 34 35 86 85
		f 4 -129 -72 84 -128
		mu 0 4 88 87 55 56
		f 4 -131 127 85 -130
		mu 0 4 89 88 56 57
		f 4 -132 129 86 -109
		mu 0 4 76 89 57 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube6";
	setAttr ".t" -type "double3" 0.71803434543372902 0 0 ;
	setAttr ".rp" -type "double3" 5.4554011346631768 14.727339950824229 5.6333046228546451 ;
	setAttr ".sp" -type "double3" 5.4554011346631768 14.727339950824229 5.6333046228546451 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 90 ".uvst[0].uvsp[0:89]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.625 0.041666668 0.54166669 0.041666668 0.45833334
		 0.041666668 0.375 0.041666668 0.29166669 0.041666668 0.20833334 0.041666668 0.125
		 0.041666668 0.375 0.70833325 0.45833334 0.70833325 0.54166669 0.70833325 0.625 0.70833325
		 0.875 0.041666668 0.79166669 0.041666668 0.70833337 0.041666668;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt[0:67]" -type "float3"  14.023539 16.441423 -3.3422949 
		9.4729595 16.441423 -5.709219 1.4378434 16.441423 -5.709219 -3.1127362 16.441423 
		-3.3422949 13.614825 17.123217 -2.9249949 9.2813148 17.123217 -5.1818752 1.6294875 
		17.123217 -5.1818752 -2.7040226 17.123217 -2.9249949 13.614825 12.432531 -2.9249949 
		9.2813148 12.432531 -5.1818752 1.6294875 12.432531 -5.1818752 -2.7040226 12.432531 
		-2.9249949 12.248776 12.11798 -2.9249949 8.3772955 10.948809 -5.1818752 1.6294875 
		10.428084 -5.1818752 -2.7040226 10.428084 -2.9249949 15.213302 13.013254 2.0282447 
		8.3772955 10.948809 2.0282447 1.6294875 10.428084 2.0282447 -6.0223398 10.428084 
		2.0282447 15.213302 13.013254 9.2383652 8.3772955 10.948809 9.2383652 1.6294875 10.428084 
		9.2383652 -6.0223398 10.428084 9.2383652 12.248776 12.11798 14.191604 8.3772955 10.948809 
		16.448484 1.6294875 10.428084 16.448484 -2.7040226 10.428084 14.191604 13.614825 
		12.432531 14.191604 9.2813148 12.432531 16.448484 1.6294875 12.432531 16.448484 -2.7040226 
		12.432531 14.191604 13.614825 17.123217 14.191604 9.2813148 17.123217 16.448484 1.6294875 
		17.123217 16.448484 -2.7040226 17.123217 14.191604 14.023539 16.441423 14.608904 
		9.4729595 16.441423 16.975828 1.4378434 16.441423 16.975828 -3.1127362 16.441423 
		14.608904 17.508074 16.441423 9.4141455 9.4729595 16.441423 9.4141455 1.4378434 16.441423 
		9.4141455 -6.5972724 16.441423 9.4141455 17.508074 16.441423 1.8524625 9.4729595 
		16.441423 1.8524625 1.4378434 16.441423 1.8524625 -6.5972724 16.441423 1.8524625 
		-6.0223398 17.123217 9.2383642 -6.0223398 17.123217 2.028244 -6.0223398 12.432531 
		9.2383642 -6.0223398 12.432531 2.028244 16.933142 17.123217 9.2383642 16.933142 17.123217 
		2.028244 16.933142 12.432531 9.2383642 16.933142 12.432531 2.028244 -3.3318453 15.133828 
		-3.566005 1.3351071 15.133965 -5.9919171 9.575695 15.133965 -5.9919171 14.242648 
		15.133828 -3.566005 17.816294 15.133771 1.7582269 17.816294 15.133771 9.5083809 14.242648 
		15.133828 14.832614 9.575695 15.133964 17.258526 1.3351071 15.133964 17.258526 -3.3318453 
		15.133828 14.832614 -6.9054918 15.133771 9.5083809 -6.9054918 15.133771 1.7582269;
	setAttr -s 68 ".vt[0:67]"  -4.93912172 -11.065385818 5.27035427 -2.31592989 -11.065385818 6.66018152
		 2.31592989 -11.065385818 6.66018152 4.93912172 -11.065385818 5.27035427 -4.93912172 -3.68846178 5.27035427
		 -2.31592989 -3.68846178 6.66018152 2.31592989 -3.68846178 6.66018152 4.93912172 -3.68846178 5.27035427
		 -4.93912172 3.68846226 5.27035427 -2.31592989 3.68846226 6.66018152 2.31592989 3.68846226 6.66018152
		 4.93912172 3.68846226 5.27035427 -4.93912172 11.065385818 5.27035427 -2.31592989 11.065385818 6.66018152
		 2.31592989 11.065385818 6.66018152 4.93912172 11.065385818 5.27035427 -6.94778967 11.065385818 2.22006035
		 -2.31592989 11.065385818 2.22006035 2.31592989 11.065385818 2.22006035 6.94778967 11.065385818 2.22006035
		 -6.94778967 11.065385818 -2.22006083 -2.31592989 11.065385818 -2.22006083 2.31592989 11.065385818 -2.22006083
		 6.94778967 11.065385818 -2.22006083 -4.93912172 11.065385818 -5.27035427 -2.31592989 11.065385818 -6.66018152
		 2.31592989 11.065385818 -6.66018152 4.93912172 11.065385818 -5.27035427 -4.93912172 3.68846178 -5.27035427
		 -2.31592989 3.68846178 -6.66018152 2.31592989 3.68846178 -6.66018152 4.93912172 3.68846178 -5.27035427
		 -4.93912172 -3.68846226 -5.27035427 -2.31592989 -3.68846226 -6.66018152 2.31592989 -3.68846226 -6.66018152
		 4.93912172 -3.68846226 -5.27035427 -4.93912172 -11.065385818 -5.27035427 -2.31592989 -11.065385818 -6.66018152
		 2.31592989 -11.065385818 -6.66018152 4.93912172 -11.065385818 -5.27035427 -6.94778967 -11.065385818 -2.22006035
		 -2.31592989 -11.065385818 -2.22006035 2.31592989 -11.065385818 -2.22006035 6.94778967 -11.065385818 -2.22006035
		 -6.94778967 -11.065385818 2.22006083 -2.31592989 -11.065385818 2.22006083 2.31592989 -11.065385818 2.22006083
		 6.94778967 -11.065385818 2.22006083 6.94778967 -3.68846178 -2.22006035 6.94778967 -3.68846178 2.22006083
		 6.94778967 3.68846226 -2.22006035 6.94778967 3.68846226 2.22006083 -6.94778967 -3.68846178 -2.22006035
		 -6.94778967 -3.68846178 2.22006083 -6.94778967 3.68846226 -2.22006035 -6.94778967 3.68846226 2.22006083
		 4.93912172 -7.37692356 5.27035427 2.31592989 -7.37692356 6.66018152 -2.31592989 -7.37692356 6.66018152
		 -4.93912172 -7.37692356 5.27035427 -6.94778967 -7.37692356 2.22006083 -6.94778967 -7.37692356 -2.22006035
		 -4.93912172 -7.37692404 -5.27035427 -2.31592989 -7.37692404 -6.66018152 2.31592989 -7.37692404 -6.66018152
		 4.93912172 -7.37692404 -5.27035427 6.94778967 -7.37692356 -2.22006035 6.94778967 -7.37692356 2.22006083;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1
		 9 10 1 10 11 1 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1
		 24 25 0 25 26 0 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0
		 38 39 0 40 41 1 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 59 0 1 58 1 2 57 1 3 56 0
		 4 8 0 5 9 1 6 10 1 7 11 0 8 12 0 9 13 1 10 14 1 11 15 0 12 16 0 13 17 1 14 18 1 15 19 0
		 16 20 0 17 21 1 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 28 0 25 29 1 26 30 1
		 27 31 0 28 32 0 29 33 1 30 34 1 31 35 0 32 62 0 33 63 1 34 64 1 35 65 0 36 40 0 37 41 1
		 38 42 1 39 43 0 40 44 0 41 45 1 42 46 1 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1
		 48 49 1 49 7 1 31 50 1 50 51 1 51 11 1 43 66 1 47 67 1 48 50 1 49 51 1 50 23 1 51 19 1
		 32 52 1 52 53 1 53 4 1 28 54 1 54 55 1 55 8 1 40 61 1 44 60 1 52 54 1 53 55 1 54 20 1
		 55 16 1 56 7 0 57 6 1 56 57 1 58 5 1 57 58 1 59 4 0 58 59 1 60 53 1 59 60 1 61 52 1
		 60 61 1 62 36 0 61 62 1 63 37 1 62 63 1 64 38 1 63 64 1 65 39 0 64 65 1 66 48 1 65 66 1
		 67 49 1 66 67 1 67 56 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 37 114 -37
		mu 0 4 0 1 78 79
		f 4 1 38 112 -38
		mu 0 4 1 2 77 78
		f 4 2 39 110 -39
		mu 0 4 2 3 76 77
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 -10 -45
		mu 0 4 8 9 13 12
		f 4 7 46 -11 -46
		mu 0 4 9 10 14 13
		f 4 8 47 -12 -47
		mu 0 4 10 11 15 14
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 18 61 -22 -61
		mu 0 4 24 25 29 28
		f 4 19 62 -23 -62
		mu 0 4 25 26 30 29
		f 4 20 63 -24 -63
		mu 0 4 26 27 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 122 121 -28 -120
		mu 0 4 83 84 37 36
		f 4 124 123 -29 -122
		mu 0 4 84 85 38 37
		f 4 126 125 -30 -124
		mu 0 4 85 86 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 31 78 -35 -78
		mu 0 4 41 42 46 45
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -126 128 -91
		mu 0 4 53 52 87 88
		f 4 -80 90 130 -92
		mu 0 4 54 53 88 89
		f 4 -84 91 131 -40
		mu 0 4 3 54 89 76
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -64 -60 -95
		mu 0 4 59 58 61 62
		f 4 -89 94 -56 -96
		mu 0 4 60 59 62 63
		f 4 -90 95 -52 -48
		mu 0 4 11 60 63 15
		f 4 72 102 120 119
		mu 0 4 64 65 81 82
		f 4 76 103 118 -103
		mu 0 4 65 66 80 81
		f 4 80 36 116 -104
		mu 0 4 66 0 79 80
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 56 60
		mu 0 4 70 71 74 73
		f 4 100 107 52 -107
		mu 0 4 71 72 75 74
		f 4 101 44 48 -108
		mu 0 4 72 8 12 75
		f 4 -111 108 -6 -110
		mu 0 4 77 76 7 6
		f 4 -113 109 -5 -112
		mu 0 4 78 77 6 5
		f 4 -115 111 -4 -114
		mu 0 4 79 78 5 4
		f 4 -117 113 -99 -116
		mu 0 4 80 79 4 69
		f 4 -119 115 -98 -118
		mu 0 4 81 80 69 68
		f 4 -121 117 -97 68
		mu 0 4 82 81 68 67
		f 4 24 69 -123 -69
		mu 0 4 32 33 84 83
		f 4 25 70 -125 -70
		mu 0 4 33 34 85 84
		f 4 26 71 -127 -71
		mu 0 4 34 35 86 85
		f 4 -129 -72 84 -128
		mu 0 4 88 87 55 56
		f 4 -131 127 85 -130
		mu 0 4 89 88 56 57
		f 4 -132 129 86 -109
		mu 0 4 76 89 57 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube8";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 -7.1054273576010019e-15 -1.4718650860341462 ;
	setAttr ".rp" -type "double3" -17.169237844579186 38.423858850752481 5.6333046228546442 ;
	setAttr ".sp" -type "double3" -17.169237844579186 38.423858850752481 5.6333046228546442 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 90 ".uvst[0].uvsp[0:89]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.625 0.041666668 0.54166669 0.041666668 0.45833334
		 0.041666668 0.375 0.041666668 0.29166669 0.041666668 0.20833334 0.041666668 0.125
		 0.041666668 0.375 0.70833325 0.45833334 0.70833325 0.54166669 0.70833325 0.625 0.70833325
		 0.875 0.041666668 0.79166669 0.041666668 0.70833337 0.041666668;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt[0:67]" -type "float3"  -19.732403 46.690254 -2.6405129 
		-22.379944 48.10796 -4.8223724 -27.054796 50.611252 -4.8223724 -29.702337 52.028957 
		-2.6405129 -10.661574 39.769844 -2.3022504 -13.306374 41.027882 -4.3949089 -17.976385 
		43.249245 -4.3949089 -20.621183 44.507282 -2.3022504 -8.1455297 32.436131 -2.3022504 
		-10.790329 33.694168 -4.3949089 -15.46034 35.915531 -4.3949089 -18.105139 37.173573 
		-2.3022504 -0.58656383 26.496147 -1.7399025 -2.8701165 27.428986 -3.6842656 -6.9022613 
		29.076126 -3.6842656 -9.1858149 30.008965 -1.7399025 1.1620286 25.781845 2.5274479 
		-2.8701165 27.428986 2.5274479 -6.9022613 29.076126 2.5274479 -10.934407 30.723267 
		2.5274479 1.1620286 25.781845 8.7391615 -2.8701165 27.428986 8.7391615 -6.9022613 
		29.076126 8.7391615 -10.934407 30.723267 8.7391615 -0.58656383 26.496147 13.006512 
		-2.8701165 27.428986 14.950874 -6.9022613 29.076126 14.950874 -9.1858149 30.008965 
		13.006512 -8.1455307 32.436131 13.568859 -10.790329 33.694172 15.661518 -15.46034 
		35.915535 15.661518 -18.105139 37.173573 13.568859 -10.661575 39.769844 13.568859 
		-13.306374 41.027882 15.661518 -17.976385 43.249245 15.661518 -20.621183 44.507282 
		13.568859 -19.732403 46.690254 13.907122 -22.379944 48.10796 16.088982 -27.054796 
		50.611252 16.088982 -29.702337 52.028957 13.907122 -17.70509 45.604664 9.1185293 
		-22.379944 48.10796 9.1185293 -27.054796 50.611252 9.1185293 -31.72965 53.114548 
		9.1185293 -17.70509 45.604664 2.1480782 -22.379944 48.10796 2.1480782 -27.054796 
		50.611252 2.1480782 -31.72965 53.114548 2.1480782 -22.646397 45.470608 8.9760418 
		-22.646397 45.470608 2.2905662 -20.130352 38.136894 8.9760418 -20.130352 38.136894 
		2.2905662 -8.6363621 38.806519 8.9760418 -8.6363621 38.806519 2.2905662 -6.120317 
		31.472805 8.9760418 -6.120317 31.472805 2.2905662 -27.475086 48.539967 -2.8218517 
		-24.825947 47.036663 -5.0515265 -20.148497 44.382233 -5.0515265 -17.499613 42.878925 
		-2.8218517 -15.471229 41.727791 2.071691 -15.471229 41.727791 9.1949167 -17.499615 
		42.878925 14.08846 -20.148499 44.382233 16.318136 -24.825949 47.036663 16.318136 
		-27.475086 48.539967 14.08846 -29.503578 49.691097 9.1949167 -29.503578 49.691097 
		2.071691;
	setAttr -s 68 ".vt[0:67]"  -4.93912172 -11.065385818 5.27035427 -2.31592989 -11.065385818 6.66018152
		 2.31592989 -11.065385818 6.66018152 4.93912172 -11.065385818 5.27035427 -4.93912172 -3.68846178 5.27035427
		 -2.31592989 -3.68846178 6.66018152 2.31592989 -3.68846178 6.66018152 4.93912172 -3.68846178 5.27035427
		 -4.93912172 3.68846226 5.27035427 -2.31592989 3.68846226 6.66018152 2.31592989 3.68846226 6.66018152
		 4.93912172 3.68846226 5.27035427 -4.93912172 11.065385818 5.27035427 -2.31592989 11.065385818 6.66018152
		 2.31592989 11.065385818 6.66018152 4.93912172 11.065385818 5.27035427 -6.94778967 11.065385818 2.22006035
		 -2.31592989 11.065385818 2.22006035 2.31592989 11.065385818 2.22006035 6.94778967 11.065385818 2.22006035
		 -6.94778967 11.065385818 -2.22006083 -2.31592989 11.065385818 -2.22006083 2.31592989 11.065385818 -2.22006083
		 6.94778967 11.065385818 -2.22006083 -4.93912172 11.065385818 -5.27035427 -2.31592989 11.065385818 -6.66018152
		 2.31592989 11.065385818 -6.66018152 4.93912172 11.065385818 -5.27035427 -4.93912172 3.68846178 -5.27035427
		 -2.31592989 3.68846178 -6.66018152 2.31592989 3.68846178 -6.66018152 4.93912172 3.68846178 -5.27035427
		 -4.93912172 -3.68846226 -5.27035427 -2.31592989 -3.68846226 -6.66018152 2.31592989 -3.68846226 -6.66018152
		 4.93912172 -3.68846226 -5.27035427 -4.93912172 -11.065385818 -5.27035427 -2.31592989 -11.065385818 -6.66018152
		 2.31592989 -11.065385818 -6.66018152 4.93912172 -11.065385818 -5.27035427 -6.94778967 -11.065385818 -2.22006035
		 -2.31592989 -11.065385818 -2.22006035 2.31592989 -11.065385818 -2.22006035 6.94778967 -11.065385818 -2.22006035
		 -6.94778967 -11.065385818 2.22006083 -2.31592989 -11.065385818 2.22006083 2.31592989 -11.065385818 2.22006083
		 6.94778967 -11.065385818 2.22006083 6.94778967 -3.68846178 -2.22006035 6.94778967 -3.68846178 2.22006083
		 6.94778967 3.68846226 -2.22006035 6.94778967 3.68846226 2.22006083 -6.94778967 -3.68846178 -2.22006035
		 -6.94778967 -3.68846178 2.22006083 -6.94778967 3.68846226 -2.22006035 -6.94778967 3.68846226 2.22006083
		 4.93912172 -7.37692356 5.27035427 2.31592989 -7.37692356 6.66018152 -2.31592989 -7.37692356 6.66018152
		 -4.93912172 -7.37692356 5.27035427 -6.94778967 -7.37692356 2.22006083 -6.94778967 -7.37692356 -2.22006035
		 -4.93912172 -7.37692404 -5.27035427 -2.31592989 -7.37692404 -6.66018152 2.31592989 -7.37692404 -6.66018152
		 4.93912172 -7.37692404 -5.27035427 6.94778967 -7.37692356 -2.22006035 6.94778967 -7.37692356 2.22006083;
	setAttr -s 132 ".ed[0:131]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1
		 9 10 1 10 11 1 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1
		 24 25 0 25 26 0 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0
		 38 39 0 40 41 1 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 59 0 1 58 1 2 57 1 3 56 0
		 4 8 0 5 9 1 6 10 1 7 11 0 8 12 0 9 13 1 10 14 1 11 15 0 12 16 0 13 17 1 14 18 1 15 19 0
		 16 20 0 17 21 1 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 28 0 25 29 1 26 30 1
		 27 31 0 28 32 0 29 33 1 30 34 1 31 35 0 32 62 0 33 63 1 34 64 1 35 65 0 36 40 0 37 41 1
		 38 42 1 39 43 0 40 44 0 41 45 1 42 46 1 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1
		 48 49 1 49 7 1 31 50 1 50 51 1 51 11 1 43 66 1 47 67 1 48 50 1 49 51 1 50 23 1 51 19 1
		 32 52 1 52 53 1 53 4 1 28 54 1 54 55 1 55 8 1 40 61 1 44 60 1 52 54 1 53 55 1 54 20 1
		 55 16 1 56 7 0 57 6 1 56 57 1 58 5 1 57 58 1 59 4 0 58 59 1 60 53 1 59 60 1 61 52 1
		 60 61 1 62 36 0 61 62 1 63 37 1 62 63 1 64 38 1 63 64 1 65 39 0 64 65 1 66 48 1 65 66 1
		 67 49 1 66 67 1 67 56 1;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 37 114 -37
		mu 0 4 0 1 78 79
		f 4 1 38 112 -38
		mu 0 4 1 2 77 78
		f 4 2 39 110 -39
		mu 0 4 2 3 76 77
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 -10 -45
		mu 0 4 8 9 13 12
		f 4 7 46 -11 -46
		mu 0 4 9 10 14 13
		f 4 8 47 -12 -47
		mu 0 4 10 11 15 14
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 18 61 -22 -61
		mu 0 4 24 25 29 28
		f 4 19 62 -23 -62
		mu 0 4 25 26 30 29
		f 4 20 63 -24 -63
		mu 0 4 26 27 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 122 121 -28 -120
		mu 0 4 83 84 37 36
		f 4 124 123 -29 -122
		mu 0 4 84 85 38 37
		f 4 126 125 -30 -124
		mu 0 4 85 86 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 31 78 -35 -78
		mu 0 4 41 42 46 45
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -126 128 -91
		mu 0 4 53 52 87 88
		f 4 -80 90 130 -92
		mu 0 4 54 53 88 89
		f 4 -84 91 131 -40
		mu 0 4 3 54 89 76
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -64 -60 -95
		mu 0 4 59 58 61 62
		f 4 -89 94 -56 -96
		mu 0 4 60 59 62 63
		f 4 -90 95 -52 -48
		mu 0 4 11 60 63 15
		f 4 72 102 120 119
		mu 0 4 64 65 81 82
		f 4 76 103 118 -103
		mu 0 4 65 66 80 81
		f 4 80 36 116 -104
		mu 0 4 66 0 79 80
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 56 60
		mu 0 4 70 71 74 73
		f 4 100 107 52 -107
		mu 0 4 71 72 75 74
		f 4 101 44 48 -108
		mu 0 4 72 8 12 75
		f 4 -111 108 -6 -110
		mu 0 4 77 76 7 6
		f 4 -113 109 -5 -112
		mu 0 4 78 77 6 5
		f 4 -115 111 -4 -114
		mu 0 4 79 78 5 4
		f 4 -117 113 -99 -116
		mu 0 4 80 79 4 69
		f 4 -119 115 -98 -118
		mu 0 4 81 80 69 68
		f 4 -121 117 -97 68
		mu 0 4 82 81 68 67
		f 4 24 69 -123 -69
		mu 0 4 32 33 84 83
		f 4 25 70 -125 -70
		mu 0 4 33 34 85 84
		f 4 26 71 -127 -71
		mu 0 4 34 35 86 85
		f 4 -129 -72 84 -128
		mu 0 4 88 87 55 56
		f 4 -131 127 85 -130
		mu 0 4 89 88 56 57
		f 4 -132 129 86 -109
		mu 0 4 76 89 57 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pPlane1";
	setAttr ".t" -type "double3" 25.587303870731105 0 12.169106372214047 ;
createNode transform -n "transform2" -p "pPlane1";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape1" -p "transform2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.17200440168380737 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 41 ".uvst[0].uvsp[0:40]" -type "float2" 0 0 0.086002201 0
		 0.1720044 0 0.2580066 0 0.3440088 0 0 0.25 0.086002201 0.25 0.1720044 0.25 0.2580066
		 0.25 0.3440088 0.25 0 0.5 0.086002201 0.5 0.1720044 0.5 0.2580066 0.5 0.3440088 0.5
		 0.3440088 0.375 0.2580066 0.375 0.1720044 0.375 0.086002201 0.375 0 0.375 0.3440088
		 0.125 0.2580066 0.125 0.1720044 0.125 0.086002201 0.125 0 0.125 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt[25:29]" -type "float3"  3.9980412 1.0152473 -4.8439064 
		1.9990197 1.0152473 -4.8439064 -5.2050694e-07 1.0152473 -4.8439064 -1.9990208 1.0152473 
		-4.8439064 -3.9980412 1.0152473 -4.8439064;
	setAttr -s 30 ".vt[0:29]"  -7.32522392 13.80707932 7.92250538 -3.66261101 13.80707932 7.92250538
		 0 13.80707932 7.92250538 3.66261101 13.80707932 7.92250538 7.32522202 13.80707932 7.92250538
		 -7.32522392 0.40332985 9.61371708 -3.66261101 0.40332985 9.61371708 0 0.40332985 9.61371708
		 3.66261101 0.40332985 9.61371708 7.32522202 0.40332985 9.61371708 -7.32522392 -10.35816383 0
		 -3.66261101 -10.35816383 0 0 -10.35816383 0 3.66261101 -10.35816383 0 7.32522202 -10.35816383 0
		 7.32522202 -8.81809425 5.32342815 3.66261101 -8.81809425 5.32342815 0 -8.81809425 5.32342815
		 -3.66261101 -8.81809425 5.32342815 -7.32522392 -8.81809425 5.32342815 7.32522202 6.82258272 9.85189152
		 3.66261101 6.82258272 9.85189152 0 6.82258272 9.85189152 -3.66261101 6.82258272 9.85189152
		 -7.32522392 6.82258272 9.85189152 -7.32522392 24.48460007 4.97296619 -3.66261101 24.48460007 4.97296619
		 0 24.48460007 4.97296429 3.66261101 24.48460007 4.97296429 7.32522202 24.48460007 4.97296619;
	setAttr -s 49 ".ed[0:48]"  0 1 1 0 24 0 1 2 1 1 23 1 2 3 1 2 22 1 3 4 1
		 3 21 1 4 20 0 5 6 1 5 19 0 6 7 1 6 18 1 7 8 1 7 17 1 8 9 1 8 16 1 9 15 0 10 11 0
		 11 12 0 12 13 0 13 14 0 15 14 0 16 13 1 15 16 1 17 12 1 16 17 1 18 11 1 17 18 1 19 10 0
		 18 19 1 20 9 0 21 8 1 20 21 1 22 7 1 21 22 1 23 6 1 22 23 1 24 5 0 23 24 1 0 25 0
		 1 26 1 25 26 0 2 27 1 26 27 0 3 28 1 27 28 0 4 29 0 28 29 0;
	setAttr -s 20 -ch 80 ".fc[0:19]" -type "polyFaces" 
		f 4 0 3 39 -2
		mu 0 4 0 1 23 24
		f 4 2 5 37 -4
		mu 0 4 1 2 22 23
		f 4 4 7 35 -6
		mu 0 4 2 3 21 22
		f 4 6 8 33 -8
		mu 0 4 3 4 20 21
		f 4 9 12 30 -11
		mu 0 4 5 6 18 19
		f 4 11 14 28 -13
		mu 0 4 6 7 17 18
		f 4 13 16 26 -15
		mu 0 4 7 8 16 17
		f 4 15 17 24 -17
		mu 0 4 8 9 15 16
		f 4 -25 22 -22 -24
		mu 0 4 16 15 14 13
		f 4 -27 23 -21 -26
		mu 0 4 17 16 13 12
		f 4 -29 25 -20 -28
		mu 0 4 18 17 12 11
		f 4 -31 27 -19 -30
		mu 0 4 19 18 11 10
		f 4 -34 31 -16 -33
		mu 0 4 21 20 9 8
		f 4 -36 32 -14 -35
		mu 0 4 22 21 8 7
		f 4 -38 34 -12 -37
		mu 0 4 23 22 7 6
		f 4 -40 36 -10 -39
		mu 0 4 24 23 6 5
		f 4 -1 40 42 -42
		mu 0 4 25 26 27 28
		f 4 -3 41 44 -44
		mu 0 4 29 30 31 32
		f 4 -5 43 46 -46
		mu 0 4 33 34 35 36
		f 4 -7 45 48 -48
		mu 0 4 37 38 39 40;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane2";
	setAttr ".t" -type "double3" 78.734267471803435 33.688778067213505 1.8711753052752371 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode transform -n "transform4" -p "pPlane2";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape2" -p "transform4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 30 ".uvst[0].uvsp[0:29]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.1460738 0.25 0.1460738 0.5 0.1460738 0.75 0.1460738 1 0.1460738 0
		 0.29214761 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761 1 0.29214761 0 0.4382214
		 0.25 0.4382214 0.5 0.4382214 0.75 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521
		 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0.875 0.4382214 0.875
		 0.29214761 0.875 0.1460738 0.875 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 30 ".vt[0:29]"  -16.80016899 -2.89659071 6.7016201 -8.4000845 -2.1796472e-15 8.63739109
		 -2.86981964 -2.1796472e-15 10.732584 3.28050041 -2.1796472e-15 11.97109222 16.80016899 -2.97128391 9.83943367
		 -16.80016899 -2.89659071 3.35081005 -8.4000845 -1.0898236e-15 4.32637119 -2.92216492 -1.0898236e-15 5.35724258
		 3.27960777 -1.0898236e-15 5.98441887 16.80016899 -2.97128391 4.91982031 -16.80016899 -2.89659071 -4.3909764e-16
		 -8.4000845 0 0 -2.94003296 0 0 3.27931023 0 0 16.80016899 -2.97128391 -6.6132913e-16
		 -16.80016899 -2.89659071 -3.35081005 -8.4000845 1.0898237e-15 -4.32637167 -2.92216492 1.0898237e-15 -5.35724258
		 3.27960777 1.0898237e-15 -5.98441887 16.80016899 -2.97128391 -4.91982079 -16.80016899 -2.89659071 -6.7016201
		 -8.4000845 2.1796472e-15 -8.63739109 -2.86981964 2.1796472e-15 -10.732584 3.28050041 2.1796472e-15 -11.97109222
		 16.80016899 -2.97128391 -9.83943367 9.15307426 -0.93614626 -11.19478703 9.15183067 -0.9360013 -5.59701204
		 9.15142632 -0.93595552 -3.6548805e-16 9.15183067 -0.9360013 5.59701204 9.15307426 -0.93614626 11.19478703;
	setAttr -s 49 ".ed[0:48]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 29 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 28 1 8 13 1 9 14 0 10 11 1 10 15 0
		 11 12 1 11 16 1 12 13 1 12 17 1 13 27 1 13 18 1 14 19 0 15 16 1 15 20 0 16 17 1 16 21 1
		 17 18 1 17 22 1 18 26 1 18 23 1 19 24 0 20 21 0 21 22 0 22 23 0 23 25 0 25 24 0 25 26 1
		 26 27 1 27 28 1 28 29 1 29 4 0 28 9 1 27 14 1 26 19 1;
	setAttr -s 20 -ch 80 ".fc[0:19]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 44 45 8 -47
		mu 0 4 28 29 4 9
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -21 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -23 -15
		mu 0 4 7 8 13 12
		f 4 43 46 17 -48
		mu 0 4 27 28 9 14
		f 4 18 21 -28 -20
		mu 0 4 10 11 16 15
		f 4 20 23 -30 -22
		mu 0 4 11 12 17 16
		f 4 22 25 -32 -24
		mu 0 4 12 13 18 17
		f 4 42 47 26 -49
		mu 0 4 26 27 14 19
		f 4 27 30 -37 -29
		mu 0 4 15 16 21 20
		f 4 29 32 -38 -31
		mu 0 4 16 17 22 21
		f 4 31 34 -39 -33
		mu 0 4 17 18 23 22
		f 4 -41 41 48 35
		mu 0 4 24 25 26 19
		f 4 33 -42 -40 -35
		mu 0 4 18 26 25 23
		f 4 24 -43 -34 -26
		mu 0 4 13 27 26 18
		f 4 15 -44 -25 -17
		mu 0 4 8 28 27 13
		f 4 6 -45 -16 -8
		mu 0 4 3 29 28 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pPlane3";
	setAttr ".rp" -type "double3" 95.879229844570204 4.5404575312448099 2.553793890373933 ;
	setAttr ".sp" -type "double3" 95.879229844570204 4.5404575312448099 2.553793890373933 ;
createNode transform -n "transform6" -p "pPlane3";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape3" -p "transform6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 15 ".uvst[0].uvsp[0:14]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.13593556 0.25 0.13593556 0.5 0.13593556 0.75 0.13593556 1 0.13593556
		 0 0.27187112 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  5.9135447 -0.57689297 0 1.8729539 
		-2.5896893 0 -0.10658795 -0.7016086 0 -1.8385738 0.0087423846 0 -3.9799981 -7.0129342 
		0 3.0112891 -0.48400885 0 0.6525178 -2.5688682 0 -0.33931023 -1.0070237 0 -0.93973851 
		-0.41153604 0 -0.89328134 -3.9494143 0 0.10903601 -0.28550801 0 -0.0029686955 -1.7846785 
		0 -0.11322062 -0.29444179 0 -0.11342081 0 0;
	setAttr -s 15 ".vt[0:14]"  90.28933716 25.10125732 2.55379391 90.28933716 14.820858 2.55379391
		 90.28933716 4.54045773 2.55379391 90.28933716 -5.7399435 2.55379391 90.28933716 -16.020343781 2.55379391
		 95.87922668 25.10125732 2.55379391 95.87922668 14.820858 2.55379391 95.87922668 4.54045773 2.55379391
		 95.87922668 -5.7399435 2.55379391 95.87922668 -16.020343781 2.55379391 101.46911621 25.10125732 2.55379391
		 101.46911621 14.820858 2.55379391 101.46911621 4.54045773 2.55379391 101.46911621 -5.7399435 2.55379391
		 101.46911621 -16.020343781 2.55379391;
	setAttr -s 22 ".ed[0:21]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 4 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 9 1 8 13 1 9 14 0 10 11 1 11 12 1
		 12 13 1 13 14 1;
	setAttr -s 8 -ch 32 ".fc[0:7]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 6 8 -16 -8
		mu 0 4 3 4 9 8
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -20 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -21 -15
		mu 0 4 7 8 13 12
		f 4 15 17 -22 -17
		mu 0 4 8 9 14 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "bowGRP";
	setAttr ".t" -type "double3" -87.612035823608394 8.7014997480500824 8.7992172316980781 ;
	setAttr ".r" -type "double3" -18.512846896075857 0 0 ;
	setAttr ".s" -type "double3" 0.12346704444718123 0.12346704444718123 0.12346704444718123 ;
	setAttr ".rp" -type "double3" 88.006568843010726 32.213752095030969 0.6121238511392626 ;
	setAttr ".sp" -type "double3" 88.006568843010726 32.213752095030969 0.6121238511392626 ;
createNode transform -n "polySurface4" -p "bowGRP";
	setAttr ".t" -type "double3" -26.958518879276038 8.6652709282360405 -1.5143874528764849 ;
	setAttr ".r" -type "double3" 0 0 -29.999999999999996 ;
	setAttr ".s" -type "double3" 0.76850662625953037 1 1 ;
	setAttr ".rp" -type "double3" 101.68158721923828 0.89123630523681652 2.5537939071655273 ;
	setAttr ".sp" -type "double3" 101.68158721923828 0.89123630523681652 2.5537939071655273 ;
createNode mesh -n "polySurfaceShape7" -p "polySurface4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:15]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 30 ".uvst[0].uvsp[0:29]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.13593556 0.25 0.13593556 0.5 0.13593556 0.75 0.13593556 1 0.13593556
		 0 0.27187112 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112 0 0 0.25
		 0 0.25 0.13593556 0 0.13593556 0.5 0 0.5 0.13593556 0.75 0 0.75 0.13593556 1 0 1
		 0.13593556 0.25 0.27187112 0 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[0]" -type "float3" 3.3141758 1.0794237 0 ;
	setAttr ".pt[1]" -type "float3" 1.0033834 0.2825768 0 ;
	setAttr ".pt[2]" -type "float3" -1.1920929e-07 0 0 ;
	setAttr ".pt[5]" -type "float3" 1.6883709 1.0794237 0 ;
	setAttr ".pt[6]" -type "float3" 0.67646682 0.34491643 0 ;
	setAttr ".pt[9]" -type "float3" 0 1.4347146 0 ;
	setAttr ".pt[10]" -type "float3" 1.4210855e-14 0.71636426 0 ;
	setAttr ".pt[11]" -type "float3" -1.4210855e-14 -0.42108214 0 ;
	setAttr ".pt[12]" -type "float3" 0 -0.91088861 0 ;
	setAttr ".pt[13]" -type "float3" 0 -0.50051749 0 ;
	setAttr ".pt[15]" -type "float3" -3.3141758 1.0794237 0 ;
	setAttr ".pt[16]" -type "float3" -1.0033834 0.2825768 0 ;
	setAttr ".pt[17]" -type "float3" 1.1920929e-07 0 0 ;
	setAttr ".pt[20]" -type "float3" -1.6883709 1.0794237 0 ;
	setAttr ".pt[21]" -type "float3" -0.67646682 0.34491643 0 ;
	setAttr ".pt[24]" -type "float3" 0 1.4347146 0 ;
	setAttr -s 25 ".vt[0:24]"  96.20288086 24.52436447 2.55379391 92.16229248 12.23116875 2.55379391
		 90.18274689 3.83884907 2.55379391 88.45075989 -5.73120117 2.55379391 86.30934143 -23.033277512 2.55379391
		 98.89051819 24.61724854 2.55379391 96.53174591 12.25198936 2.55379391 95.53991699 3.53343391 2.55379391
		 94.93949127 -6.15147972 2.55379391 94.98594666 -19.96975708 2.55379391 101.68158722 24.81575012 2.55379391
		 101.68158722 13.036179543 2.55379391 101.68158722 4.24601603 2.55379391 101.68158722 -5.7399435 2.55379391
		 101.68158722 -16.020343781 2.55379391 107.16029358 24.52436447 2.55379391 111.20088196 12.23116875 2.55379391
		 113.18042755 3.83884907 2.55379391 114.91241455 -5.73120117 2.55379391 117.053833008 -23.033277512 2.55379391
		 104.47265625 24.61724854 2.55379391 106.83142853 12.25198936 2.55379391 107.82325745 3.53343391 2.55379391
		 108.42368317 -6.15147972 2.55379391 108.37722778 -19.96975708 2.55379391;
	setAttr -s 40 ".ed[0:39]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 4 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 9 1 8 13 1 9 14 0 10 11 0 11 12 0
		 12 13 0 13 14 0 15 16 0 15 20 0 16 17 0 16 21 1 17 18 0 17 22 1 18 19 0 18 23 1 19 24 0
		 20 21 1 20 10 0 21 22 1 21 11 1 22 23 1 22 12 1 23 24 1 23 13 1 24 14 0;
	setAttr -s 16 -ch 64 ".fc[0:15]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 6 8 -16 -8
		mu 0 4 3 4 9 8
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -20 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -21 -15
		mu 0 4 7 8 13 12
		f 4 15 17 -22 -17
		mu 0 4 8 9 14 13
		f 4 23 31 -26 -23
		mu 0 4 15 18 17 16
		f 4 25 33 -28 -25
		mu 0 4 16 17 20 19
		f 4 27 35 -30 -27
		mu 0 4 19 20 22 21
		f 4 29 37 -31 -29
		mu 0 4 21 22 24 23
		f 4 32 18 -35 -32
		mu 0 4 18 26 25 17
		f 4 34 19 -37 -34
		mu 0 4 17 25 27 20
		f 4 36 20 -39 -36
		mu 0 4 20 27 28 22
		f 4 38 21 -40 -38
		mu 0 4 22 28 29 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "polySurface5" -p "bowGRP";
	setAttr ".t" -type "double3" -9.3788836897160142 -3.8090654815303968 -2.539441077288962 ;
	setAttr ".r" -type "double3" 4.444785074718042 181.28060464644139 20.559964218695171 ;
	setAttr ".s" -type "double3" 0.77068753959165781 0.77068753959165781 0.77068753959165781 ;
	setAttr ".rp" -type "double3" 78.734268188476562 33.688777923583984 -1.2920215129852295 ;
	setAttr ".sp" -type "double3" 78.734268188476562 33.688777923583984 -1.2920215129852295 ;
createNode mesh -n "polySurfaceShape8" -p "polySurface5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:39]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 60 ".uvst[0].uvsp[0:59]" -type "float2" 0 0 0.25 0 0.5 0
		 0.75 0 1 0 0 0.1460738 0.25 0.1460738 0.5 0.1460738 0.75 0.1460738 1 0.1460738 0
		 0.29214761 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761 1 0.29214761 0 0.4382214
		 0.25 0.4382214 0.5 0.4382214 0.75 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521
		 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0.875 0.4382214 0.875
		 0.29214761 0.875 0.1460738 0.875 0 0 0 0.25 0 0.25 0.1460738 0 0.1460738 0.5 0 0.5
		 0.1460738 0.75 0 0.75 0.1460738 0.875 0.1460738 0.875 0 1 0 1 0.1460738 0.25 0.29214761
		 0 0.29214761 0.5 0.29214761 0.75 0.29214761 0.875 0.29214761 1 0.29214761 0.25 0.4382214
		 0 0.4382214 0.5 0.4382214 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0.25 0.58429521
		 0 0.58429521 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 60 ".vt[0:59]"  61.93409729 26.98715782 -1.025415421 70.33418274 25.051387787 1.87117529
		 75.86444855 22.95619392 1.87117529 82.014770508 21.7176857 1.87117529 95.53443909 23.84934425 -1.10010862
		 61.93409729 30.33796692 -1.025415421 70.33418274 29.36240768 1.87117529 75.81210327 28.33153534 1.87117529
		 82.013877869 27.70435905 1.87117529 95.53443909 28.76895714 -1.10010862 61.93409729 33.68877792 -1.025415421
		 70.33418274 33.68877792 1.87117529 75.79423523 33.68877792 1.87117529 82.013580322 33.68877792 1.87117529
		 95.53443909 33.68877792 -1.10010862 61.93409729 37.039588928 -1.025415421 70.33418274 38.015148163 1.87117529
		 75.81210327 39.046020508 1.87117529 82.013877869 39.67319489 1.87117529 95.53443909 38.6085968 -1.10010862
		 61.93409729 40.39039612 -1.025415421 70.33418274 42.32616806 1.87117529 75.86444855 44.42136383 1.87117529
		 82.014770508 45.65987015 1.87117529 95.53443909 43.5282135 -1.10010862 87.88734436 44.883564 0.93502903
		 87.88610077 39.28578949 0.93517399 87.88569641 33.68877792 0.93521976 87.88610077 28.091766357 0.93517399
		 87.88734436 22.49399185 0.93502903 61.93409729 26.98715782 -1.55862761 70.33418274 25.051387787 -4.45521832
		 75.86444855 22.95619392 -4.45521832 82.014770508 21.7176857 -4.45521832 95.53443909 23.84934425 -1.4839344
		 61.93409729 30.33796692 -1.55862761 70.33418274 29.36240768 -4.45521832 75.81210327 28.33153534 -4.45521832
		 82.013877869 27.70435905 -4.45521832 95.53443909 28.76895714 -1.4839344 61.93409729 33.68877792 -1.55862761
		 70.33418274 33.68877792 -4.45521832 75.79423523 33.68877792 -4.45521832 82.013580322 33.68877792 -4.45521832
		 95.53443909 33.68877792 -1.4839344 61.93409729 37.039588928 -1.55862761 70.33418274 38.015148163 -4.45521832
		 75.81210327 39.046020508 -4.45521832 82.013877869 39.67319489 -4.45521832 95.53443909 38.6085968 -1.4839344
		 61.93409729 40.39039612 -1.55862761 70.33418274 42.32616806 -4.45521832 75.86444855 44.42136383 -4.45521832
		 82.014770508 45.65987015 -4.45521832 95.53443909 43.5282135 -1.4839344 87.88734436 44.883564 -3.51907206
		 87.88610077 39.28578949 -3.51921701 87.88569641 33.68877792 -3.51926279 87.88610077 28.091766357 -3.51921701
		 87.88734436 22.49399185 -3.51907206;
	setAttr -s 98 ".ed[0:97]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 29 0
		 3 8 1 4 9 0 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 28 1 8 13 1 9 14 0 10 11 1 10 15 0
		 11 12 1 11 16 1 12 13 1 12 17 1 13 27 1 13 18 1 14 19 0 15 16 1 15 20 0 16 17 1 16 21 1
		 17 18 1 17 22 1 18 26 1 18 23 1 19 24 0 20 21 0 21 22 0 22 23 0 23 25 0 25 24 0 25 26 1
		 26 27 1 27 28 1 28 29 1 29 4 0 28 9 1 27 14 1 26 19 1 30 31 0 30 35 0 31 32 0 31 36 1
		 32 33 0 32 37 1 33 59 0 33 38 1 34 39 0 35 36 1 35 40 0 36 37 1 36 41 1 37 38 1 37 42 1
		 38 58 1 38 43 1 39 44 0 40 41 1 40 45 0 41 42 1 41 46 1 42 43 1 42 47 1 43 57 1 43 48 1
		 44 49 0 45 46 1 45 50 0 46 47 1 46 51 1 47 48 1 47 52 1 48 56 1 48 53 1 49 54 0 50 51 0
		 51 52 0 52 53 0 53 55 0 55 54 0 55 56 1 56 57 1 57 58 1 58 59 1 59 34 0 58 39 1 57 44 1
		 56 49 1;
	setAttr -s 40 -ch 160 ".fc[0:39]" -type "polyFaces" 
		f 4 0 3 -10 -2
		mu 0 4 0 1 6 5
		f 4 2 5 -12 -4
		mu 0 4 1 2 7 6
		f 4 4 7 -14 -6
		mu 0 4 2 3 8 7
		f 4 44 45 8 -47
		mu 0 4 28 29 4 9
		f 4 9 12 -19 -11
		mu 0 4 5 6 11 10
		f 4 11 14 -21 -13
		mu 0 4 6 7 12 11
		f 4 13 16 -23 -15
		mu 0 4 7 8 13 12
		f 4 43 46 17 -48
		mu 0 4 27 28 9 14
		f 4 18 21 -28 -20
		mu 0 4 10 11 16 15
		f 4 20 23 -30 -22
		mu 0 4 11 12 17 16
		f 4 22 25 -32 -24
		mu 0 4 12 13 18 17
		f 4 42 47 26 -49
		mu 0 4 26 27 14 19
		f 4 27 30 -37 -29
		mu 0 4 15 16 21 20
		f 4 29 32 -38 -31
		mu 0 4 16 17 22 21
		f 4 31 34 -39 -33
		mu 0 4 17 18 23 22
		f 4 -41 41 48 35
		mu 0 4 24 25 26 19
		f 4 33 -42 -40 -35
		mu 0 4 18 26 25 23
		f 4 24 -43 -34 -26
		mu 0 4 13 27 26 18
		f 4 15 -44 -25 -17
		mu 0 4 8 28 27 13
		f 4 6 -45 -16 -8
		mu 0 4 3 29 28 8
		f 4 50 58 -53 -50
		mu 0 4 30 33 32 31
		f 4 52 60 -55 -52
		mu 0 4 31 32 35 34
		f 4 54 62 -57 -54
		mu 0 4 34 35 37 36
		f 4 95 -58 -95 -94
		mu 0 4 38 41 40 39
		f 4 59 67 -62 -59
		mu 0 4 33 43 42 32
		f 4 61 69 -64 -61
		mu 0 4 32 42 44 35
		f 4 63 71 -66 -63
		mu 0 4 35 44 45 37
		f 4 96 -67 -96 -93
		mu 0 4 46 47 41 38
		f 4 68 76 -71 -68
		mu 0 4 43 49 48 42
		f 4 70 78 -73 -70
		mu 0 4 42 48 50 44
		f 4 72 80 -75 -72
		mu 0 4 44 50 51 45
		f 4 97 -76 -97 -92
		mu 0 4 52 53 47 46
		f 4 77 85 -80 -77
		mu 0 4 49 55 54 48
		f 4 79 86 -82 -79
		mu 0 4 48 54 56 50
		f 4 81 87 -84 -81
		mu 0 4 50 56 57 51
		f 4 -85 -98 -91 89
		mu 0 4 58 53 52 59
		f 4 83 88 90 -83
		mu 0 4 51 57 59 52
		f 4 74 82 91 -74
		mu 0 4 45 51 52 46
		f 4 65 73 92 -65
		mu 0 4 37 45 46 38
		f 4 56 64 93 -56
		mu 0 4 36 37 38 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dsm" 2;
createNode transform -n "pCylinder1";
	setAttr ".t" -type "double3" 0 0 -0.12883618307030176 ;
	setAttr ".rp" -type "double3" -1.1094752550125122 48.983345031738281 9.7349662780761719 ;
	setAttr ".sp" -type "double3" -1.1094752550125122 48.983345031738281 9.7349662780761719 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 85 ".uvst[0].uvsp[0:84]" -type "float2" 0.64435619 0.096455798
		 0.61048549 0.045764633 0.55979437 0.011893868 0.50000006 1.4901161e-08 0.44020578
		 0.011893794 0.38951463 0.045764521 0.35564384 0.096455663 0.34375 0.15624994 0.35564381
		 0.21604425 0.38951454 0.2667354 0.44020569 0.30060616 0.49999997 0.3125 0.55979425
		 0.30060619 0.61048543 0.26673543 0.64435619 0.21604429 0.65625 0.15625 0.375 0.3125
		 0.390625 0.3125 0.40625 0.3125 0.421875 0.3125 0.4375 0.3125 0.453125 0.3125 0.46875
		 0.3125 0.484375 0.3125 0.5 0.3125 0.515625 0.3125 0.53125 0.3125 0.546875 0.3125
		 0.5625 0.3125 0.578125 0.3125 0.59375 0.3125 0.609375 0.3125 0.625 0.3125 0.375 0.68843985
		 0.390625 0.68843985 0.40625 0.68843985 0.421875 0.68843985 0.4375 0.68843985 0.453125
		 0.68843985 0.46875 0.68843985 0.484375 0.68843985 0.5 0.68843985 0.515625 0.68843985
		 0.53125 0.68843985 0.546875 0.68843985 0.5625 0.68843985 0.578125 0.68843985 0.59375
		 0.68843985 0.609375 0.68843985 0.625 0.68843985 0.64435619 0.78395581 0.61048549
		 0.73326463 0.55979437 0.69939387 0.50000006 0.6875 0.44020578 0.69939381 0.38951463
		 0.73326451 0.35564384 0.78395569 0.34375 0.84374994 0.35564381 0.90354425 0.38951454
		 0.95423543 0.44020569 0.98810613 0.49999997 1 0.55979425 0.98810619 0.61048543 0.95423543
		 0.64435619 0.90354431 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.53125 0.50046992
		 0.515625 0.50046992 0.5 0.50046992 0.484375 0.50046992 0.46875 0.50046992 0.453125
		 0.50046992 0.4375 0.50046992 0.421875 0.50046992 0.40625 0.50046992 0.390625 0.50046992
		 0.625 0.50046992 0.375 0.50046992 0.609375 0.50046992 0.59375 0.50046992 0.578125
		 0.50046992 0.5625 0.50046992 0.546875 0.50046992;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 50 ".vt[0:49]"  -0.67571986 49.24972916 9.62385941 -0.77749312 49.47555923 9.62385941
		 -0.92980772 49.6264534 9.62385941 -1.10947502 49.67943954 9.62385941 -1.28914237 49.6264534 9.62385941
		 -1.44145703 49.47555923 9.62385941 -1.54323053 49.24972916 9.62385941 -1.57896864 48.98334503 9.62385941
		 -1.54323053 48.71696091 9.62385941 -1.44145727 48.49113083 9.62385941 -1.28914261 48.34023666 9.62385941
		 -1.10947537 48.28725052 9.62385941 -0.92980796 48.34023666 9.62385941 -0.77749336 48.49113083 9.62385941
		 -0.67571998 48.71696091 9.62385941 -0.63998187 48.98334503 9.62385941 -0.67571986 49.24972916 9.84607315
		 -0.77749312 49.47555923 9.84607315 -0.92980772 49.6264534 9.84607315 -1.10947502 49.67943954 9.84607315
		 -1.28914237 49.6264534 9.84607315 -1.44145703 49.47555923 9.84607315 -1.54323053 49.24972916 9.84607315
		 -1.57896864 48.98334503 9.84607315 -1.54323053 48.71696091 9.84607315 -1.44145727 48.49113083 9.84607315
		 -1.28914261 48.34023666 9.84607315 -1.10947537 48.28725052 9.84607315 -0.92980796 48.34023666 9.84607315
		 -0.77749336 48.49113083 9.84607315 -0.67571998 48.71696091 9.84607315 -0.63998187 48.98334503 9.84607315
		 -1.10947526 48.98334503 9.52155972 -1.10947526 48.98334503 9.94837284 -1.28914261 48.34023666 9.73496628
		 -1.44145727 48.49113083 9.73496628 -1.54323053 48.71696091 9.73496628 -1.57896864 48.98334503 9.73496628
		 -1.54323053 49.24972916 9.73496628 -1.44145703 49.47555923 9.73496628 -1.28914237 49.6264534 9.73496628
		 -1.10947502 49.67943954 9.73496628 -0.92980772 49.6264534 9.73496628 -0.77749312 49.47555923 9.73496628
		 -0.67571986 49.24972916 9.73496628 -0.63998187 48.98334503 9.73496628 -0.67571998 48.71696091 9.73496628
		 -0.77749336 48.49113083 9.73496628 -0.92980796 48.34023666 9.73496628 -1.10947537 48.28725052 9.73496628;
	setAttr -s 96 ".ed[0:95]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 0 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 16 0 0 44 1 1 43 1 2 42 1 3 41 1 4 40 1 5 39 1 6 38 1 7 37 1 8 36 1
		 9 35 1 10 34 1 11 49 1 12 48 1 13 47 1 14 46 1 15 45 1 32 0 1 32 2 1 32 4 1 32 6 1
		 32 8 1 32 10 1 32 12 1 32 14 1 16 33 1 18 33 1 20 33 1 22 33 1 24 33 1 26 33 1 28 33 1
		 30 33 1 34 26 1 35 25 1 34 35 1 36 24 1 35 36 1 37 23 1 36 37 1 38 22 1 37 38 1 39 21 1
		 38 39 1 40 20 1 39 40 1 41 19 1 40 41 1 42 18 1 41 42 1 43 17 1 42 43 1 44 16 1 43 44 1
		 45 31 1 44 45 1 46 30 1 45 46 1 47 29 1 46 47 1 48 28 1 47 48 1 49 27 1 48 49 1 49 34 1;
	setAttr -s 48 -ch 192 ".fc[0:47]" -type "polyFaces" 
		f 4 0 33 84 -33
		mu 0 4 16 17 77 79
		f 4 1 34 82 -34
		mu 0 4 17 18 76 77
		f 4 2 35 80 -35
		mu 0 4 18 19 75 76
		f 4 3 36 78 -36
		mu 0 4 19 20 74 75
		f 4 4 37 76 -37
		mu 0 4 20 21 73 74
		f 4 5 38 74 -38
		mu 0 4 21 22 72 73
		f 4 6 39 72 -39
		mu 0 4 22 23 71 72
		f 4 7 40 70 -40
		mu 0 4 23 24 70 71
		f 4 8 41 68 -41
		mu 0 4 24 25 69 70
		f 4 9 42 66 -42
		mu 0 4 25 26 68 69
		f 4 10 43 95 -43
		mu 0 4 26 27 84 68
		f 4 11 44 94 -44
		mu 0 4 27 28 83 84
		f 4 12 45 92 -45
		mu 0 4 28 29 82 83
		f 4 13 46 90 -46
		mu 0 4 29 30 81 82
		f 4 14 47 88 -47
		mu 0 4 30 31 80 81
		f 4 15 32 86 -48
		mu 0 4 31 32 78 80
		f 4 -1 -49 49 -2
		mu 0 4 1 0 66 2
		f 4 -3 -50 50 -4
		mu 0 4 3 2 66 4
		f 4 -5 -51 51 -6
		mu 0 4 5 4 66 6
		f 4 -7 -52 52 -8
		mu 0 4 7 6 66 8
		f 4 -9 -53 53 -10
		mu 0 4 9 8 66 10
		f 4 -11 -54 54 -12
		mu 0 4 11 10 66 12
		f 4 -13 -55 55 -14
		mu 0 4 13 12 66 14
		f 4 -15 -56 48 -16
		mu 0 4 15 14 66 0
		f 4 -57 16 17 57
		mu 0 4 67 64 63 62
		f 4 -58 18 19 58
		mu 0 4 67 62 61 60
		f 4 -59 20 21 59
		mu 0 4 67 60 59 58
		f 4 -60 22 23 60
		mu 0 4 67 58 57 56
		f 4 -61 24 25 61
		mu 0 4 67 56 55 54
		f 4 -62 26 27 62
		mu 0 4 67 54 53 52
		f 4 -63 28 29 63
		mu 0 4 67 52 51 50
		f 4 -64 30 31 56
		mu 0 4 67 50 65 64
		f 4 -67 64 -26 -66
		mu 0 4 69 68 43 42
		f 4 -69 65 -25 -68
		mu 0 4 70 69 42 41
		f 4 -71 67 -24 -70
		mu 0 4 71 70 41 40
		f 4 -73 69 -23 -72
		mu 0 4 72 71 40 39
		f 4 -75 71 -22 -74
		mu 0 4 73 72 39 38
		f 4 -77 73 -21 -76
		mu 0 4 74 73 38 37
		f 4 -79 75 -20 -78
		mu 0 4 75 74 37 36
		f 4 -81 77 -19 -80
		mu 0 4 76 75 36 35
		f 4 -83 79 -18 -82
		mu 0 4 77 76 35 34
		f 4 -85 81 -17 -84
		mu 0 4 79 77 34 33
		f 4 -87 83 -32 -86
		mu 0 4 80 78 49 48
		f 4 -89 85 -31 -88
		mu 0 4 81 80 48 47
		f 4 -91 87 -30 -90
		mu 0 4 82 81 47 46
		f 4 -93 89 -29 -92
		mu 0 4 83 82 46 45
		f 4 -95 91 -28 -94
		mu 0 4 84 83 45 44
		f 4 -96 93 -27 -65
		mu 0 4 68 84 44 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube10";
	setAttr ".t" -type "double3" 0.33351003275980928 46.395741341889426 11.743665230680577 ;
	setAttr ".s" -type "double3" 0.069251696962714965 0.069251696962714965 0.069251696962714965 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".pt[0:55]" -type "float3"  9.6291618 0 -2.6914134 2.487093 
		0 -1.5513059 -2.487093 0 -1.5513059 -9.6291618 0 -2.6914134 6.5401621 0 -2.1211751 
		1.1348643 0 -0.78254765 -1.1348643 0 -0.78254765 -6.5401621 0 -2.1211751 5.5591846 
		0 -2.1211751 0.7054351 0 -0.78254765 -0.7054351 0 -0.78254765 -5.5591846 0 -2.1211751 
		5.5591846 0 -2.1211751 0.7054351 0 -0.78254765 -0.7054351 0 -0.78254765 -5.5591846 
		0 -2.1211751 2.1163054 0 -0.26084921 0.7054351 3.065083 -0.26084921 -0.7054351 3.065083 
		-0.26084921 -2.1163054 0 -0.26084921 2.1163054 0 0.26084924 0.7054351 3.065083 0.26084924 
		-0.7054351 3.065083 0.26084924 -2.1163054 0 0.26084924 5.5591846 0 2.1211751 0.7054351 
		0 0.78254765 -0.7054351 0 0.78254765 -5.5591846 0 2.1211751 5.5591846 0 2.1211751 
		0.7054351 0 0.78254765 -0.7054351 0 0.78254765 -5.5591846 0 2.1211751 6.5401621 0 
		2.1211751 1.1348643 0 0.78254765 -1.1348643 0 0.78254765 -6.5401621 0 2.1211751 9.6291618 
		0 2.6914134 2.487093 0 1.5513059 -2.487093 0 1.5513059 -9.6291618 0 2.6914134 7.4612794 
		0 0.51710194 2.487093 0 0.51710194 -2.487093 0 0.51710194 -7.4612794 0 0.51710194 
		7.4612794 0 -0.517102 2.487093 0 -0.517102 -2.487093 0 -0.517102 -7.4612794 0 -0.517102 
		-3.404593 0 0.26084921 -3.404593 0 -0.26084924 -2.1163054 0 0.26084921 -2.1163054 
		0 -0.26084924 3.404593 0 0.26084921 3.404593 0 -0.26084924 2.1163054 0 0.26084921 
		2.1163054 0 -0.26084924;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube11";
	setAttr ".rp" -type "double3" -1.0843619060979299 50.11952375834634 9.5880352222310226 ;
	setAttr ".sp" -type "double3" -1.0843619060979299 50.11952375834634 9.5880352222310226 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	setAttr -k off ".v";
	setAttr -s 2 ".iog";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 76 ".uvst[0].uvsp[0:75]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 56 ".vt[0:55]"  -2.52194118 50.4037323 8.89678383 -2.49217558 50.21166992 8.89678383
		 -2.46239305 50.019504547 8.89678383 -2.43264961 49.82758331 8.89678383 -2.52194118 50.4037323 8.97633266
		 -2.49217558 50.21166992 8.97633266 -2.46239305 50.019504547 8.97633266 -2.43264961 49.82758331 8.97633266
		 -2.52194118 50.4037323 9.055880547 -2.49217558 50.21166992 9.055880547 -2.46239305 50.019504547 9.055880547
		 -2.43264961 49.82758331 9.055880547 -2.52194118 50.4037323 9.13542938 -2.49217558 50.21166992 9.13542938
		 -2.46239305 50.019504547 9.13542938 -2.43264961 49.82758331 9.13542938 -1.58404779 50.42764282 9.37421608
		 -1.56802201 50.23235703 9.37363148 -1.55193615 50.037086487 9.37304497 -1.53578973 49.84183502 9.37246799
		 -0.57578576 50.22588348 9.69866276 -0.60336155 50.035800934 9.69535255 -0.63061434 49.84592056 9.6915226
		 -0.6574918 49.65553665 9.68863392 0.31892502 49.95426941 9.70735836 0.2719686 49.76642227 9.70735836
		 0.22501218 49.57857513 9.70735836 0.17805588 49.390728 9.70735836 0.31892502 49.95426941 9.62780952
		 0.2719686 49.76642227 9.62780952 0.22501218 49.57857513 9.62780952 0.17805588 49.390728 9.62780952
		 0.31892502 49.95426941 9.54826164 0.2719686 49.76642227 9.54826164 0.22501218 49.57857513 9.54826164
		 0.17805588 49.390728 9.54826164 0.31892502 49.95426941 9.46871281 0.2719686 49.76642227 9.46871281
		 0.22501218 49.57857513 9.46871281 0.17805588 49.390728 9.46871281 -0.5757857 50.22588348 9.46001816
		 -0.60336149 50.035800934 9.45670795 -0.63061428 49.84592056 9.452878 -0.65749174 49.65553665 9.44998932
		 -1.58404779 50.42764282 9.13557148 -1.56802189 50.23235703 9.13498688 -1.55193603 50.037086487 9.13440037
		 -1.53578973 49.84183502 9.13382339 -0.65749174 49.65553665 9.52953815 -1.53578973 49.84183502 9.21337128
		 -0.65749174 49.65553665 9.60908604 -1.53578973 49.84183502 9.29291916 -0.5757857 50.22588348 9.53956699
		 -1.58404779 50.42764282 9.21511936 -0.5757857 50.22588348 9.61911488 -1.58404779 50.42764282 9.2946682;
	setAttr -s 108 ".ed[0:107]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1
		 9 10 1 10 11 1 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1
		 24 25 0 25 26 0 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0
		 38 39 0 40 41 1 41 42 1 42 43 1 44 45 1 45 46 1 46 47 1 0 4 0 1 5 1 2 6 1 3 7 0 4 8 0
		 5 9 1 6 10 1 7 11 0 8 12 0 9 13 1 10 14 1 11 15 0 12 16 0 13 17 1 14 18 1 15 19 0
		 16 20 0 17 21 1 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 28 0 25 29 1 26 30 1
		 27 31 0 28 32 0 29 33 1 30 34 1 31 35 0 32 36 0 33 37 1 34 38 1 35 39 0 36 40 0 37 41 1
		 38 42 1 39 43 0 40 44 0 41 45 1 42 46 1 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1
		 48 49 1 49 7 1 31 50 1 50 51 1 51 11 1 43 48 1 47 49 1 48 50 1 49 51 1 50 23 1 51 19 1
		 32 52 1 52 53 1 53 4 1 28 54 1 54 55 1 55 8 1 40 52 1 44 53 1 52 54 1 53 55 1 54 20 1
		 55 16 1;
	setAttr -s 54 -ch 216 ".fc[0:53]" -type "polyFaces" 
		f 4 0 37 -4 -37
		mu 0 4 0 1 5 4
		f 4 1 38 -5 -38
		mu 0 4 1 2 6 5
		f 4 2 39 -6 -39
		mu 0 4 2 3 7 6
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 -10 -45
		mu 0 4 8 9 13 12
		f 4 7 46 -11 -46
		mu 0 4 9 10 14 13
		f 4 8 47 -12 -47
		mu 0 4 10 11 15 14
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 18 61 -22 -61
		mu 0 4 24 25 29 28
		f 4 19 62 -23 -62
		mu 0 4 25 26 30 29
		f 4 20 63 -24 -63
		mu 0 4 26 27 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 24 69 -28 -69
		mu 0 4 32 33 37 36
		f 4 25 70 -29 -70
		mu 0 4 33 34 38 37
		f 4 26 71 -30 -71
		mu 0 4 34 35 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 31 78 -35 -78
		mu 0 4 41 42 46 45
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -72 84 -91
		mu 0 4 53 52 55 56
		f 4 -80 90 85 -92
		mu 0 4 54 53 56 57
		f 4 -84 91 86 -40
		mu 0 4 3 54 57 7
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -64 -60 -95
		mu 0 4 59 58 61 62
		f 4 -89 94 -56 -96
		mu 0 4 60 59 62 63
		f 4 -90 95 -52 -48
		mu 0 4 11 60 63 15
		f 4 72 102 -97 68
		mu 0 4 64 65 68 67
		f 4 76 103 -98 -103
		mu 0 4 65 66 69 68
		f 4 80 36 -99 -104
		mu 0 4 66 0 4 69
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 56 60
		mu 0 4 70 71 74 73
		f 4 100 107 52 -107
		mu 0 4 71 72 75 74
		f 4 101 44 48 -108
		mu 0 4 72 8 12 75;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube12";
	setAttr ".t" -type "double3" 3.9556507908769465 -0.19777969238754167 -0.34057505784625874 ;
	setAttr ".r" -type "double3" 0 9.583675626743652 0 ;
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" -1.0843619060979299 50.11952375834634 9.5880352222310226 ;
	setAttr ".sp" -type "double3" -1.0843619060979299 50.11952375834634 9.5880352222310226 ;
createNode transform -n "pPlane4";
	setAttr ".t" -type "double3" 26.882548723972238 20.457299888192527 2.9255491048470361 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode mesh -n "pPlaneShape4" -p "pPlane4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface6";
	setAttr ".rp" -type "double3" 0.46310019493103027 39.008340835571289 10.058321475982666 ;
	setAttr ".sp" -type "double3" 0.46310019493103027 39.008340835571289 10.058321475982666 ;
createNode mesh -n "polySurfaceShape9" -p "polySurface6";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:431]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 619 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0 0 0.25 0 0.5 0 0.75 0 1 0
		 0 0.1460738 0.25 0.1460738 0.5 0.1460738 0.75 0.1460738 1 0.1460738 0 0.29214761
		 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761 1 0.29214761 0 0.4382214 0.25 0.4382214
		 0.5 0.4382214 0.75 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521 0.5 0.58429521
		 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0.875 0.4382214 0.875 0.29214761 0.875
		 0.1460738 0.875 0 0 0 0.25 0 0.25 0.1460738 0 0.1460738 0.5 0 0.5 0.1460738 0.75
		 0 0.75 0.1460738 0.875 0.1460738 0.875 0 1 0 1 0.1460738 0.25 0.29214761 0 0.29214761
		 0.5 0.29214761 0.75 0.29214761 0.875 0.29214761 1 0.29214761 0.25 0.4382214 0 0.4382214
		 0.5 0.4382214 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0.25 0.58429521 0 0.58429521
		 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0 0 0.25 0 0.25 0.1460738
		 0 0.1460738 0.5 0 0.5 0.1460738 0.75 0 0.75 0.1460738 0.875 0.1460738 0.875 0 1 0
		 1 0.1460738 0.25 0.29214761 0 0.29214761 0.5 0.29214761 0.75 0.29214761 0.875 0.29214761
		 1 0.29214761 0.25 0.4382214 0 0.4382214 0.5 0.4382214 0.75 0.4382214 0.875 0.4382214
		 1 0.4382214 0.25 0.58429521 0 0.58429521 0.5 0.58429521 0.75 0.58429521 1 0.58429521
		 0.875 0.58429521 0 0 0 0.1460738 0.25 0.1460738 0.25 0 0.5 0.1460738 0.5 0 0.75 0.1460738
		 0.75 0 0.875 0.1460738 1 0.1460738 1 0 0.875 0 0 0.29214761 0.25 0.29214761 0.5 0.29214761
		 0.75 0.29214761 0.875 0.29214761 1 0.29214761 0 0.4382214 0.25 0.4382214 0.5 0.4382214
		 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521 0.5 0.58429521
		 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0 0 0.25 0 0.5 0 0.75 0 0.875 0 1 0
		 0.25 0.58429521 0 0.58429521 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521
		 0.25 0 0 0 0.5 0 0.75 0 1 0 0.875 0 0 0.58429521 0.25 0.58429521 0.5 0.58429521 0.75
		 0.58429521 0.875 0.58429521 1 0.58429521 0.25 0.58429521 0 0.58429521 0 0.58429521
		 0.25 0.58429521 0.5 0.58429521 0.75 0.58429521 0.875 0.58429521 1 0.58429521 1 0.58429521
		 0.875 0.58429521 0.75 0.58429521 0.5 0.58429521 0.75 0 0.5 0 0.25 0 0 0 0 0 0.25
		 0 0.5 0 0.75 0 0.875 0 1 0 1 0 0.875 0 0 0 0.25 0 0.25 0.13593556 0 0.13593556 0.5
		 0 0.5 0.13593556 0.75 0 0.75 0.13593556 1 0 1 0.13593556 0.25 0.27187112 0 0.27187112
		 0.5 0.27187112 0.75 0.27187112 1 0.27187112 0 0 0 0.13593556 0.25 0.13593556 0.25
		 0 0.5 0.13593556 0.5 0 0.75 0.13593556 0.75 0 1 0.13593556 1 0 0 0.27187112 0.25
		 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112 0 0 0 0.13593556 0.25 0.13593556
		 0.25 0 0.5 0.13593556 0.5 0 0.75 0.13593556 0.75 0 1 0.13593556 1 0 0 0.27187112
		 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112 0 0 0.25 0 0.25 0.13593556
		 0 0.13593556 0.5 0 0.5 0.13593556 0.75 0 0.75 0.13593556 1 0 1 0.13593556 0.25 0.27187112
		 0 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112 0 0 0.25 0 0.25 0 0 0 0
		 0.13593556 0 0.13593556 0.5 0 0.5 0 0.75 0 0.75 0 1 0 1 0 1 0.13593556 1 0.13593556
		 0 0.27187112 0 0.27187112 1 0.27187112 1 0.27187112 0 0 0 0.13593556 0 0.13593556
		 0 0;
	setAttr ".uvst[0].uvsp[250:499]" 0.25 0 0.25 0 0.5 0 0.5 0 0.75 0 0.75 0 1
		 0.13593556 1 0 1 0 1 0.13593556 0 0.27187112 0 0.27187112 1 0.27187112 1 0.27187112
		 0 0 0.25 0 0.25 0.13593556 0 0.13593556 0.5 0 0.5 0.13593556 0.75 0 0.75 0.13593556
		 1 0 1 0.13593556 0.25 0.27187112 0 0.27187112 0.5 0.27187112 0.75 0.27187112 1 0.27187112
		 0 0 0 0.13593556 0.25 0.13593556 0.25 0 0.5 0.13593556 0.5 0 0.75 0.13593556 0.75
		 0 1 0.13593556 1 0 0 0.27187112 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112 1
		 0.27187112 0 0 0 0.13593556 0.25 0.13593556 0.25 0 0.5 0.13593556 0.5 0 0.75 0.13593556
		 0.75 0 1 0.13593556 1 0 0 0.27187112 0.25 0.27187112 0.5 0.27187112 0.75 0.27187112
		 1 0.27187112 0 0 0.25 0 0.25 0.13593556 0 0.13593556 0.5 0 0.5 0.13593556 0.75 0
		 0.75 0.13593556 1 0 1 0.13593556 0.25 0.27187112 0 0.27187112 0.5 0.27187112 0.75
		 0.27187112 1 0.27187112 0 0 0.25 0 0.25 0 0 0 0 0.13593556 0 0.13593556 0.5 0 0.5
		 0 0.75 0 0.75 0 1 0 1 0 1 0.13593556 1 0.13593556 0 0.27187112 0 0.27187112 1 0.27187112
		 1 0.27187112 0 0 0 0.13593556 0 0.13593556 0 0 0.25 0 0.25 0 0.5 0 0.5 0 0.75 0 0.75
		 0 1 0.13593556 1 0 1 0 1 0.13593556 0 0.27187112 0 0.27187112 1 0.27187112 1 0.27187112
		 0.375 0.0625 0.5 0.0625 0.5 0.125 0.375 0.125 0.625 0.0625 0.625 0.125 0.5 0.1875
		 0.375 0.1875 0.625 0.1875 0.375 0.25 0.5 0.25 0.5 0.3125 0.375 0.3125 0.625 0.25
		 0.625 0.3125 0.375 0.375 0.5 0.375 0.5 0.4375 0.375 0.4375 0.625 0.375 0.625 0.4375
		 0.375 0.5625 0.5 0.5625 0.5 0.625 0.375 0.625 0.625 0.5625 0.625 0.625 0.5 0.6875
		 0.375 0.6875 0.625 0.6875 0.375 0.8125 0.5 0.8125 0.5 0.875 0.375 0.875 0.625 0.8125
		 0.625 0.875 0.375 0.9375 0.5 0.9375 0.5 1 0.375 1 0.625 0.9375 0.625 1 0.75 0.0625
		 0.8125 0.0625 0.8125 0.125 0.75 0.125 0.6875 0.0625 0.6875 0.125 0.8125 0.1875 0.75
		 0.1875 0.6875 0.1875 0.1875 0.0625 0.25 0.0625 0.25 0.125 0.1875 0.125 0.3125 0.0625
		 0.3125 0.125 0.25 0.1875 0.1875 0.1875 0.3125 0.1875 0.3125 0.25 0.25 0.25 0.1875
		 0.25 0.375 0.5 0.5 0.5 0.625 0.5 0.8125 0.25 0.75 0.25 0.6875 0.25 0.5 0.75 0.375
		 0.75 0.1875 0 0.25 0 0.3125 0 0.375 0 0.5 0 0.625 0 0.6875 0 0.75 0 0.8125 0 0.625
		 0.75 0.125 0.1875 0.125 0.25 0.125 0.125 0.125 0.0625 0.125 0 0.875 0 0.875 0.0625
		 0.875 0.125 0.875 0.1875 0.875 0.25 0 0 0.25 0 0.25 0.1460738 0 0.1460738 0.5 0 0.5
		 0.1460738 0.75 0 0.75 0.1460738 0.875 0.1460738 0.875 0 1 0 1 0.1460738 0.25 0.29214761
		 0 0.29214761 0.5 0.29214761 0.75 0.29214761 0.875 0.29214761 1 0.29214761 0.25 0.4382214
		 0 0.4382214 0.5 0.4382214 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0.25 0.58429521
		 0 0.58429521 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0 0 0 0.1460738
		 0.25 0.1460738 0.25 0 0.5 0.1460738 0.5 0 0.75 0.1460738 0.75 0 0.875 0.1460738 1
		 0.1460738 1 0 0.875 0 0 0.29214761 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761
		 0.875 0.29214761 1 0.29214761 0 0.4382214;
	setAttr ".uvst[0].uvsp[500:618]" 0.25 0.4382214 0.5 0.4382214 0.75 0.4382214
		 0.875 0.4382214 1 0.4382214 0 0.58429521 0.25 0.58429521 0.5 0.58429521 0.75 0.58429521
		 1 0.58429521 0.875 0.58429521 0 0 0 0.1460738 0.25 0.1460738 0.25 0 0.5 0.1460738
		 0.5 0 0.75 0.1460738 0.75 0 0.875 0.1460738 1 0.1460738 1 0 0.875 0 0 0.29214761
		 0.25 0.29214761 0.5 0.29214761 0.75 0.29214761 0.875 0.29214761 1 0.29214761 0 0.4382214
		 0.25 0.4382214 0.5 0.4382214 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0 0.58429521
		 0.25 0.58429521 0.5 0.58429521 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0 0
		 0.25 0 0.25 0.1460738 0 0.1460738 0.5 0 0.5 0.1460738 0.75 0 0.75 0.1460738 0.875
		 0.1460738 0.875 0 1 0 1 0.1460738 0.25 0.29214761 0 0.29214761 0.5 0.29214761 0.75
		 0.29214761 0.875 0.29214761 1 0.29214761 0.25 0.4382214 0 0.4382214 0.5 0.4382214
		 0.75 0.4382214 0.875 0.4382214 1 0.4382214 0.25 0.58429521 0 0.58429521 0.5 0.58429521
		 0.75 0.58429521 1 0.58429521 0.875 0.58429521 0 0 0.25 0 0.25 0 0 0 0.5 0 0.5 0 0.75
		 0 0.75 0 0.875 0 1 0 1 0 0.875 0 0.25 0.58429521 0 0.58429521 0 0.58429521 0.25 0.58429521
		 0.5 0.58429521 0.5 0.58429521 0.75 0.58429521 0.75 0.58429521 1 0.58429521 0.875
		 0.58429521 0.875 0.58429521 1 0.58429521 0.25 0 0 0 0 0 0.25 0 0.5 0 0.5 0 0.75 0
		 0.75 0 1 0 0.875 0 0.875 0 1 0 0 0.58429521 0.25 0.58429521 0.25 0.58429521 0 0.58429521
		 0.5 0.58429521 0.5 0.58429521 0.75 0.58429521 0.75 0.58429521 0.875 0.58429521 1
		 0.58429521 1 0.58429521 0.875 0.58429521;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 438 ".vt";
	setAttr ".vt[0:165]"  0.37987566 40.81322861 8.41886139 -0.75403786 40.1700325 9.014180183
		 -1.014557362 39.67264557 8.99684143 -1.87809706 39.1367569 9.25017357 -3.38951635 39.24057388 9.49641418
		 0.38483191 40.80111313 8.42407036 -0.88553047 40.4979248 8.79194641 -1.16549492 40.24254608 9.012536049
		 -2.09460783 39.67880249 9.015417099 -3.51690531 39.55202866 9.36253452 0.18253136 41.081977844 8.42200565
		 -0.98851299 40.65909195 8.55247498 -1.30845451 40.55261612 8.60920238 -2.31212664 40.21706772 8.78360558
		 -3.64429998 39.86349487 9.22864914 0.18749237 41.069850922 8.42721939 -1.10323477 40.93067551 8.43818665
		 -1.49890614 40.95993042 8.43591595 -2.53438234 40.75401688 8.55323505 -3.77169466 40.1749649 9.094763756
		 0.19244862 41.057731628 8.43242836 -1.24726915 41.24955368 8.31651211 -1.72049761 41.39860535 8.2549324
		 -2.76045084 41.29403687 8.32286167 -3.89908361 40.48641968 8.96088409 -3.56587362 40.75072861 8.61988735
		 -3.40550661 40.33347321 8.79916 -3.24004698 39.92051315 8.97664261 -3.068556309 39.50965881 9.15327835
		 -2.89016676 39.098686218 9.33002186 -0.7684536 40.30594635 9.37735367 -1.018382549 39.90904999 9.55045128
		 -1.88160467 39.35357285 9.75790787 -0.88463402 40.62326813 9.22555447 -1.16410065 40.36841583 9.34540939
		 -2.098185062 39.89577484 9.52357197 -0.9923377 40.89549637 9.10608387 -1.31227922 40.78902054 9.16281128
		 -2.31571722 40.43410492 9.29192829 -1.10705996 41.16707993 8.99179554 -1.50273085 41.19633865 8.98952579
		 -2.53795671 40.9709816 9.06139183 -1.25109386 41.48596191 8.87012196 -1.7243228 41.63501358 8.80854225
		 -2.76395845 41.51085281 8.83059597 -3.5685668 40.91717148 9.0096559525 -3.40819979 40.49992371 9.18895435
		 -3.24274015 40.086971283 9.36644459 -3.071249485 39.67611313 9.5430727 -2.89285994 39.26512909 9.71979141
		 0.67230892 40.90271759 8.33707237 -0.52404976 39.91747665 8.82234478 -0.75137901 40.33303452 8.55892658
		 0.6780262 40.88873672 8.34308147 -0.90443039 39.54198456 9.0049266815 -1.072525024 40.071891785 8.76839924
		 -1.90057611 38.92380524 9.2971611 -2.15033579 39.54909134 9.026355743 -3.2738471 39.35397339 9.1853857
		 -3.068064213 38.87988663 9.38927174 -3.64409494 39.043563843 9.58121586 -3.79104662 39.40284729 9.42677689
		 -0.87438536 40.67991257 8.49232101 0.6837492 40.87474442 8.3490963 -1.24345875 40.55708694 8.55776024
		 -2.40125799 40.17001343 8.75894547 -3.47167349 39.82791901 8.98162556 -3.93800402 39.76214218 9.27233124
		 -1.0067248344 40.99320221 8.36048317 0.6894722 40.86075211 8.35511017 -1.4631567 41.026954651 8.35786438
		 -2.6576438 40.78941345 8.49319935 -3.66253996 40.30429077 8.77688885 -4.084961414 40.12144089 9.11788654
		 -1.17287827 41.36104965 8.22012329 0.69518948 40.84677505 8.36111927 -1.7187767 41.5329895 8.14908791
		 -2.91842699 41.41236496 8.22744846 -3.84753466 40.78562546 8.57008648 -4.23191309 40.48072433 8.96344757
		 -0.75455523 40.63859177 9.26876163 -0.62053394 40.27253723 9.4438715 -1.076937199 40.34460068 9.40702248
		 -0.90884304 39.81469345 9.64355087 -2.15446138 39.79938126 9.61254311 -1.90462351 39.17391586 9.882864
		 -3.27695417 39.54598618 9.63503838 -3.071171284 39.07188797 9.83889484 -0.87879753 40.95262146 9.13094521
		 -1.2478714 40.82979584 9.19638348 -2.4053998 40.42037582 9.34532833 -3.47478056 40.019935608 9.43128777
		 -1.011137009 41.2659111 8.99910641 -1.46756887 41.29966354 8.99648762 -2.66176748 41.039699554 9.079389572
		 -3.66564703 40.49630737 9.22654152 -1.17729044 41.63375854 8.85874748 -1.72318935 41.80570221 8.78771114
		 -2.92247438 41.66247559 8.81315231 -3.85064173 40.9776268 9.019709587 -1.21419191 41.55986023 8.86443424
		 0.3401885 41.098751068 8.39313507 -1.21007347 41.30530167 8.26831818 -1.71963692 41.46579742 8.20201015
		 -2.83943892 41.35319901 8.27515507 -3.70670462 40.76817703 8.59498692 -4.065498829 40.48357391 8.96216583
		 -3.70960474 40.94739914 9.01468277 -2.84321547 41.58666229 8.82187366 -1.72375584 41.72035599 8.79812717
		 -1.88933706 39.030281067 9.27366734 -0.95949364 39.60731506 9.00088405609 -0.65496254 40.070957184 8.96459866
		 0.52609158 40.85797501 8.37796688 -0.69449377 40.28924179 9.41061306 -0.96361256 39.86186981 9.59700108
		 -1.89311361 39.26374435 9.82038593 -2.98201609 39.16850662 9.77934265 -3.51680613 39.14206696 9.53881454
		 -2.97911596 38.98928833 9.3596468 0.10218525 40.88238525 8.83041573 -1.22782135 39.84088898 9.82672119
		 -2.0084705353 39.09551239 10.076306343 -2.74158573 38.2030983 10.37512589 -3.89434147 36.69198227 11.010940552
		 0.19517326 40.84403229 8.82669449 -0.89050007 39.66745377 9.88479424 -1.58711052 38.82353973 10.16737461
		 -2.23433256 37.86857605 10.4871397 -3.086324692 37.23947144 10.82761669 0.27562618 40.77774048 8.84889221
		 -0.52178669 39.46805191 9.95156288 -1.094671249 38.52713394 10.26662445 -1.68580627 37.8624382 10.59172344
		 -2.28420258 36.96273041 10.88508892 0.45791626 40.6876297 8.89562798 0.17173386 39.074668884 10.083286285
		 -0.11868191 38.060905457 10.42273998 -0.56715202 38.061618805 10.77374077 -1.4593029 36.21097946 11.3934164
		 0.37639618 40.74481964 8.85991573 -0.15531921 39.26496124 10.019566536 -0.57775116 38.27094269 10.35240936
		 -1.12629604 38.3109169 10.69026375 -1.89459372 37.057407379 11.10999489 0.10218525 40.90699387 8.90391445
		 -1.22782135 39.86549759 9.90022087 -0.89050007 39.69206619 9.95829391 0.19517326 40.86864471 8.90019321
		 -2.0084705353 39.12012482 10.14980507 -1.58711052 38.84815216 10.24087429 -2.74158573 38.22771072 10.44862556
		 -2.23433256 37.89318466 10.56063938 -3.89434147 36.71659088 11.084439278 -3.086324692 37.26408386 10.90111542
		 -0.52178669 39.49266434 10.025062561 0.27562618 40.80235291 8.92239094 -1.094671249 38.55174255 10.34012413
		 -1.68580627 37.88704681 10.66522217 -2.28405571 36.98361969 10.95935917 0.45791626 40.71224213 8.9691267
		 0.37639618 40.76942825 8.93341446 -0.15531921 39.28957367 10.093066216 0.17173386 39.099277496 10.15678596
		 -0.57775116 38.2955513 10.42590809 -0.11868191 38.085514069 10.49623871;
	setAttr ".vt[166:331]" -1.12629604 38.33552933 10.76376247 -0.56715202 38.086227417 10.84723949
		 -1.89459372 37.082019806 11.18349457 -1.4593029 36.23559189 11.46691513 -1.22782135 39.85319138 9.86347103
		 0.10218525 40.89469147 8.86476326 0.19517326 40.57838821 8.030908585 0.27562523 40.51209259 8.053107262
		 0.37639618 40.75712585 8.89422226 0.45791626 40.69993591 8.92997551 0.17173386 39.086971283 10.12003613
		 -0.11868191 38.073207855 10.45948982 -0.56715202 38.073925018 10.81048965 -1.4593029 36.22328568 11.43016624
		 -1.89459372 37.069713593 11.14674473 -2.28417778 36.97444153 10.92196178 -3.086324692 37.25177765 10.86436653
		 -3.89434147 36.70428848 11.047689438 -2.74158573 38.21540451 10.41187572 -2.0084705353 39.1078186 10.11305618
		 0.37948799 40.6876297 8.81493378 0.66566944 39.07466507 10.083286285 0.95608521 38.060901642 10.42273998
		 1.40455532 37.4060173 10.72939968 1.47825003 36.42575073 12.048472404 0.4610076 40.74481583 8.77715874
		 0.99272346 39.26496124 10.019566536 1.41515398 38.27093887 10.35240936 1.96369934 37.53583527 10.50078297
		 2.45916557 36.27186203 12.02933979 0.56177902 40.77774048 8.76613426 1.35919189 39.46805191 9.95156288
		 1.93207645 38.52713013 10.26662445 2.52321148 37.65595627 10.43860245 3.17901516 36.75611115 11.014186859
		 0.73521852 40.88238144 8.74972153 2.065224648 39.84088516 9.82672119 2.84587288 39.09551239 10.076306343
		 3.57898998 38.2030983 10.37512589 4.82307434 37.36123657 10.93028641 0.64223003 40.84403229 8.74393654
		 1.72790432 39.66745377 9.88479424 2.42451191 38.82353592 10.16737556 3.071735382 37.86857224 10.48714066
		 3.83239937 37.42698669 10.90827084 0.37948799 40.71223831 8.88843346 0.66566944 39.099273682 10.15678596
		 0.99272346 39.28956985 10.093066216 0.4610076 40.76942825 8.85065746 0.95608521 38.085510254 10.49623966
		 1.41515398 38.2955513 10.42590904 1.4045558 37.47341919 10.76767349 1.96369839 37.60323715 10.53905678
		 1.48949194 36.49277878 12.085734367 2.47040749 36.33889389 12.066601753 1.35919189 39.49266052 10.025062561
		 0.56177902 40.80234909 8.83963394 1.93207645 38.55174255 10.34012413 2.52321148 37.69557953 10.50521946
		 3.20053959 36.72272491 11.12378311 0.73521852 40.90699387 8.82322121 0.64223003 40.8686409 8.81743622
		 1.72790432 39.69206238 9.95829391 2.065224648 39.86549377 9.90022087 2.42451191 38.84814835 10.24087429
		 2.84587288 39.120121 10.14980507 3.071735382 37.89318085 10.56063938 3.57898998 38.22770691 10.44862556
		 3.83239937 37.45159912 10.98176956 4.82307434 37.385849 11.0037851334 0.66566944 39.086971283 10.12003613
		 0.37948799 40.69993591 8.84898281 0.46100712 40.75712204 8.81116104 0.56177902 40.79004669 8.80013657
		 0.64223003 40.8563385 8.77793884 0.735219 40.89468765 8.78377056 2.065224648 39.85319138 9.86347103
		 2.84587288 39.10781479 10.11305618 3.57898998 38.21540451 10.41187572 4.82307434 37.37354279 10.96703625
		 3.83239937 37.43929291 10.94502068 3.19041634 36.74155426 11.067716599 2.46478653 36.30537796 12.047969818
		 1.48387146 36.45926666 12.067103386 1.40455532 37.43971634 10.74853611 0.95608521 38.073204041 10.45948982
		 0.23354478 40.5421524 9.053379059 0.3898927 40.56584167 9.12412643 0.54624063 40.5421524 9.053379059
		 0.070434749 40.97225189 8.90936375 0.3898927 40.99594116 8.98011017 0.70935065 40.97225189 8.90936375
		 0.16726068 41.40234756 8.76534843 0.3898927 41.42603683 8.83609486 0.61252475 41.40234756 8.76534843
		 0.02889061 41.36260986 8.64667702 0.3898927 41.36260986 8.64667702 0.75089478 41.36260986 8.64667702
		 0.16726068 41.32287598 8.5280056 0.3898927 41.29918671 8.45725822 0.61252475 41.32287598 8.5280056
		 0.070434749 40.89277649 8.67202091 0.3898927 40.86908722 8.60127449 0.70935065 40.89277649 8.67202091
		 0.23354478 40.46268082 8.81603622 0.3898927 40.43899155 8.7452898 0.54624063 40.46268082 8.81603622
		 0.13637155 40.50241852 8.93470764 0.3898927 40.50241852 8.93470764 0.6434139 40.50241852 8.93470764
		 0.90789992 40.93251419 8.79069233 -0.12811452 40.93251419 8.79069233 0.3898927 41.21098709 8.90810204
		 0.1188477 41.18729782 8.83735561 -0.049611956 41.14756393 8.7186842 0.1188477 41.10782623 8.60001278
		 0.3898927 41.084136963 8.52926636 0.66093767 41.10782623 8.60001278 0.82939732 41.14756393 8.7186842
		 0.66093767 41.18729782 8.83735561 0.3898927 40.65404129 8.67328262 0.15198976 40.67773056 8.74402905
		 0.0041285157 40.71746445 8.86270046 0.15198976 40.75720215 8.98137188 0.3898927 40.78089142 9.052118301
		 0.62779564 40.75720215 8.98137188 0.77565688 40.71746445 8.86270046 0.62779564 40.67773056 8.74402905
		 0.68170977 41.38248062 8.70601273 0.3898927 41.39432526 8.74138546 0.098075658 41.38248062 8.70601273
		 0.034617901 41.16743088 8.77801991 -0.028839886 40.95238113 8.85002804 0.078059167 40.73733521 8.92203617
		 0.18495816 40.52228546 8.99404335 0.3898927 40.5341301 9.029417038 0.59482723 40.52228546 8.99404335
		 0.7017262 40.73733521 8.92203617 0.80862528 40.95238113 8.85002804 0.74516749 41.16743088 8.77801991
		 0.68170977 41.34274292 8.58734131 0.3898927 41.33089828 8.55196762 0.098075658 41.34274292 8.58734131
		 0.034617901 41.12769318 8.65934849 -0.028839886 40.91264725 8.73135662 0.078059167 40.6975975 8.80336475
		 0.18495816 40.48254776 8.87537193 0.3898927 40.47070313 8.8399992 0.59482723 40.48254776 8.87537193
		 0.7017262 40.6975975 8.80336475 0.80862528 40.91264725 8.73135662 0.74516749 41.12769318 8.65934849
		 0.32971907 40.86144638 8.28640938 1.67333555 40.69442749 9.83593559 1.85115719 40.36842728 10.17068291
		 2.60966444 39.86956406 10.69303703 4.11324501 39.56334305 10.7287941 0.32273817 40.85253143 8.2947979
		 1.84295893 40.94314575 9.62005806 2.074321747 40.71658325 9.84931374 2.91682339 40.27029037 10.31770515
		 4.29266167 39.79243088 10.51319313 0.31584644 40.84418869 8.30292606 1.99622726 41.14593124 9.4319973
		 2.29312277 41.029460907 9.55530834 3.22430134 40.66681671 9.94487381;
	setAttr ".vt[332:437]" 4.472085 40.021530151 10.29758358 0.15244484 40.7634697 8.3711977
		 2.15622616 41.34314346 9.24361229 2.55125332 41.32555771 9.27619648 3.53620815 41.061161041 9.57340527
		 4.65150928 40.25062943 10.08197403 0.5366869 40.75632477 8.38031197 2.35309362 41.56238556 9.019085884
		 2.84541225 41.6379509 8.97590828 3.85238457 41.4563446 9.19968891 4.83092594 40.47972107 9.86637306
		 4.5516634 40.91874695 9.7037859 4.32145119 40.61112213 9.99418068 4.086960793 40.30713272 10.28056622
		 3.84688663 40.0051689148 10.56445503 3.59998894 39.70359421 10.84739113 1.70751762 40.26854706 9.4118557
		 1.88533878 39.94254684 9.74660301 2.6410141 39.4789772 10.30409813 1.877141 40.51726532 9.19597816
		 2.10850334 40.29070282 9.42523384 2.94826031 39.87936401 9.92847729 2.030409336 40.72005463 9.0079174042
		 2.32730484 40.60358429 9.13122845 3.25575829 40.27576065 9.55552578 2.19040823 40.91726303 8.81953239
		 2.58543491 40.89967728 8.85211658 3.56764126 40.67023087 9.18418026 2.3872757 41.13650513 8.59500599
		 2.87959385 41.21207428 8.55182838 3.88373375 41.065753937 8.81075001 4.57572842 40.61890793 9.4052124
		 4.34551811 40.31126404 9.69558811 4.11102867 40.0072669983 9.98196697 3.87095356 39.70531082 10.26586151
		 3.62405396 39.40375137 10.54881763 0.07123518 41.026981354 8.12832069 1.51563406 40.6972847 9.88470078
		 1.71130466 40.98419571 9.63567352 0.063194752 41.016777039 8.13796234 1.72076225 40.3212204 10.270854
		 1.97819614 40.72284317 9.90013409 2.5957489 39.74575424 10.87342167 2.95007515 40.20801544 10.44045353
		 4.02296257 39.90218353 10.72509384 3.73814964 39.55429459 11.05147934 4.33022308 39.39250565 10.91466999
		 4.53719139 39.65677643 10.66596127 1.88810921 41.21812439 9.41873264 0.055201054 41.0068855286 8.14746189
		 2.2305975 41.083766937 9.56097984 3.30477047 40.66543579 10.010368347 4.29990387 40.2505188 10.39761066
		 4.74416828 39.92105484 10.4172411 2.072678566 41.44561768 9.20141888 -0.58226633 40.8730545 8.29677963
		 2.52836704 41.42533493 9.23900604 3.66457462 41.12033081 9.58185482 4.57040215 40.60118866 10.067247391
		 4.95114517 40.18533707 10.16852188 2.29977751 41.69852829 8.94241333 -0.59014606 40.86381531 8.30597115
		 2.86769962 41.78569794 8.89260578 4.029304504 41.57620239 9.15075016 4.83596611 40.95605469 9.73225784
		 5.15811348 40.44960785 9.9198122 1.75337267 40.46006393 9.11375713 1.55770206 40.17315292 9.36278534
		 2.017627239 40.23156357 9.41093159 1.76019287 39.8299408 9.78165054 2.98634052 39.75705719 9.99145317
		 2.63191223 39.29518509 10.424757 4.050725937 39.55627441 10.38064861 3.7659111 39.20840836 10.70705605
		 1.93017721 40.69399261 8.89681625 2.27002859 40.59248734 9.071777344 3.34105873 40.21432495 9.56123066
		 4.32766724 39.90460205 10.053157806 2.11210966 40.95433807 8.71221542 2.56779814 40.93405533 8.74980354
		 3.70083618 40.66936874 9.13286018 4.59816551 40.25527954 9.72280121 2.3392086 41.20724869 8.45321083
		 2.90713024 41.29442215 8.40340233 4.065467834 41.12563324 8.70208549 4.86372757 40.61016846 9.38783455
		 2.36324215 41.171875 8.52410793 0.16687965 40.90891266 8.26604462 2.32643557 41.63045502 8.98074913
		 2.85655594 41.71182632 8.93425655 3.94084454 41.5162735 9.17521954 4.69381428 40.93740082 9.71802235
		 4.99452019 40.46466446 9.89309311 4.71972847 40.61453629 9.39652348 3.97460079 41.095695496 8.75641823
		 2.89336205 41.25324631 8.47761536 2.60270643 39.80765915 10.78322887 1.78595972 40.34482574 10.22076893
		 1.59448481 40.695858 9.86031818 0.19656706 40.94493866 8.23100185 1.63129139 40.23727798 9.40367699
		 1.82276583 39.88624573 9.76412773 2.63646317 39.38708115 10.36442757 3.69498348 39.30607986 10.62793636
		 4.22173405 39.47792435 10.82173252 3.66906929 39.6289444 10.94943523;
	setAttr -s 864 ".ed";
	setAttr ".ed[0:165]"  0 1 0 0 5 0 1 2 0 1 6 1 2 3 0 2 7 1 3 29 0 3 8 1 4 9 0
		 5 6 1 5 10 0 6 7 1 6 11 1 7 8 1 7 12 1 8 28 1 8 13 1 9 14 0 10 11 1 10 15 0 11 12 1
		 11 16 1 12 13 1 12 17 1 13 27 1 13 18 1 14 19 0 15 16 1 15 20 0 16 17 1 16 21 1 17 18 1
		 17 22 1 18 26 1 18 23 1 19 24 0 20 21 0 21 22 0 22 23 0 23 25 0 25 24 0 25 26 1 26 27 1
		 27 28 1 28 29 1 29 4 0 28 9 1 27 14 1 26 19 1 0 30 0 30 31 0 30 33 1 31 32 0 31 34 1
		 32 49 0 32 35 1 5 33 1 33 34 1 33 36 1 34 35 1 34 37 1 35 48 1 35 38 1 10 36 1 36 37 1
		 36 39 1 37 38 1 37 40 1 38 47 1 38 41 1 15 39 1 39 40 1 39 42 1 40 41 1 40 43 1 41 46 1
		 41 44 1 20 42 0 42 43 0 43 44 0 44 45 0 45 24 0 45 46 1 46 47 1 47 48 1 48 49 1 49 4 0
		 48 9 1 47 14 1 46 19 1 0 113 0 1 112 0 50 51 0 51 52 1 53 52 1 50 53 0 2 111 0 51 54 0
		 54 55 1 52 55 1 3 110 0 54 56 0 56 57 1 55 57 1 29 119 0 58 59 1 4 118 0 59 60 0
		 60 61 0 58 61 1 52 62 1 63 62 1 53 63 0 55 64 1 62 64 1 57 65 1 64 65 1 66 58 1 61 67 0
		 66 67 1 62 68 1 69 68 1 63 69 0 64 70 1 68 70 1 65 71 1 70 71 1 72 66 1 67 73 0 72 73 1
		 21 102 0 68 74 1 20 101 0 75 74 0 69 75 0 22 103 0 70 76 1 74 76 0 23 104 0 71 77 1
		 76 77 0 25 105 0 24 106 0 78 79 0 78 72 1 73 79 0 71 72 1 77 78 0 65 66 1 57 58 1
		 56 59 0 53 80 1 30 114 0 81 80 1 50 81 0 80 82 1 31 115 0 83 82 1 81 83 0 82 84 1
		 32 116 0 85 84 1 83 85 0 86 61 1 49 117 0 87 60 0;
	setAttr ".ed[166:331]" 86 87 1 63 88 1 80 88 1 88 89 1 82 89 1 89 90 1 84 90 1
		 91 67 1 91 86 1 69 92 1 88 92 1 92 93 1 89 93 1 93 94 1 90 94 1 95 73 1 95 91 1 42 100 0
		 75 96 0 92 96 1 43 109 0 96 97 0 93 97 1 44 108 0 97 98 0 94 98 1 45 107 0 99 95 1
		 99 79 0 98 99 0 94 95 1 90 91 1 84 86 1 85 87 0 100 96 0 101 75 0 100 101 1 102 74 0
		 101 102 1 103 76 0 102 103 1 104 77 0 103 104 1 105 78 0 104 105 1 106 79 0 105 106 1
		 107 99 0 106 107 1 108 98 0 107 108 1 109 97 0 108 109 1 109 100 1 110 56 0 111 54 0
		 110 111 1 112 51 0 111 112 1 113 50 0 112 113 1 114 81 0 113 114 1 115 83 0 114 115 1
		 116 85 0 115 116 1 117 87 0 116 117 1 118 60 0 117 118 1 119 59 0 118 119 1 119 110 1
		 120 121 0 120 125 0 121 122 0 121 126 1 122 123 0 122 127 1 123 124 0 123 128 1 124 129 0
		 125 126 1 125 130 0 126 127 1 126 131 1 127 128 1 127 132 1 128 129 1 128 133 1 129 134 0
		 130 131 0 131 132 0 132 133 0 133 134 0 135 136 0 135 140 0 136 137 0 136 141 1 137 138 0
		 137 142 1 138 139 0 138 143 1 139 144 0 140 141 1 140 130 0 141 142 1 141 131 1 142 143 1
		 142 132 1 143 144 1 143 133 1 144 134 0 120 171 0 121 170 1 145 146 0 146 147 1 125 172 1
		 148 147 1 145 148 0 122 185 1 146 149 0 149 150 1 147 150 1 123 184 1 149 151 0 151 152 1
		 150 152 1 124 183 0 151 153 0 129 182 1 153 154 0 152 154 1 147 155 1 130 173 1 156 155 0
		 148 156 0 150 157 1 155 157 0 152 158 1 157 158 0 134 181 0 154 159 0 158 159 0 135 175 0
		 140 174 1 160 161 0 161 162 1 136 176 1 163 162 1 160 163 0 162 164 1 137 177 1 165 164 1
		 163 165 0 164 166 1 138 178 1 167 166 1 165 167 0 144 180 1 166 168 1 139 179 0 169 168 0
		 167 169 0 161 156 0;
	setAttr ".ed[332:497]" 162 155 1 164 157 1 166 158 1 168 159 0 170 146 1 171 145 0
		 170 171 1 172 148 1 171 172 1 173 156 1 172 173 1 174 161 1 173 174 1 175 160 0 174 175 1
		 176 163 1 175 176 1 177 165 1 176 177 1 178 167 1 177 178 1 179 169 0 178 179 1 180 168 1
		 179 180 1 181 159 0 180 181 1 182 154 1 181 182 1 183 153 0 182 183 1 184 151 1 183 184 1
		 185 149 1 184 185 1 185 170 1 186 187 0 186 191 0 187 188 0 187 192 1 188 189 0 188 193 1
		 189 190 0 189 194 1 190 195 0 191 192 1 191 196 0 192 193 1 192 197 1 193 194 1 193 198 1
		 194 195 1 194 199 1 195 200 0 196 197 0 197 198 0 198 199 0 199 200 0 201 202 0 201 206 0
		 202 203 0 202 207 1 203 204 0 203 208 1 204 205 0 204 209 1 205 210 0 206 207 1 206 196 0
		 207 208 1 207 197 1 208 209 1 208 198 1 209 210 1 209 199 1 210 200 0 186 237 0 187 236 1
		 211 212 0 212 213 1 191 238 1 214 213 1 211 214 0 188 251 1 212 215 0 215 216 1 213 216 1
		 189 250 1 215 217 0 217 218 1 216 218 1 190 249 0 217 219 0 195 248 1 219 220 0 218 220 1
		 213 221 1 196 239 1 222 221 0 214 222 0 216 223 1 221 223 0 218 224 1 223 224 0 200 247 0
		 220 225 0 224 225 0 201 241 0 206 240 1 226 227 0 227 228 1 202 242 1 229 228 1 226 229 0
		 228 230 1 203 243 1 231 230 1 229 231 0 230 232 1 204 244 1 233 232 1 231 233 0 210 246 1
		 232 234 1 205 245 0 235 234 0 233 235 0 227 222 0 228 221 1 230 223 1 232 224 1 234 225 0
		 236 212 1 237 211 0 236 237 1 238 214 1 237 238 1 239 222 1 238 239 1 240 227 1 239 240 1
		 241 226 0 240 241 1 242 229 1 241 242 1 243 231 1 242 243 1 244 233 1 243 244 1 245 235 0
		 244 245 1 246 234 1 245 246 1 247 225 0 246 247 1 248 220 1 247 248 1 249 219 0 248 249 1
		 250 217 1 249 250 1 251 215 1 250 251 1 251 236 1 252 253 0 253 254 0;
	setAttr ".ed[498:663]" 255 256 1 256 257 1 258 259 0 259 260 0 261 262 1 262 263 1
		 264 265 0 265 266 0 267 268 1 268 269 1 270 271 0 271 272 0 273 274 1 274 275 1 252 289 0
		 253 290 1 254 291 0 255 279 0 256 278 1 257 285 0 258 296 0 259 295 1 260 294 0 261 308 0
		 262 307 1 263 306 0 264 281 0 265 282 1 266 283 0 267 287 0 268 286 1 269 293 0 270 312 0
		 271 313 1 272 314 0 273 300 0 274 301 1 275 302 0 269 316 1 276 304 1 275 292 1 276 284 1
		 267 310 1 277 298 1 273 288 1 277 280 1 278 259 1 279 258 0 278 279 1 280 261 1 279 297 1
		 281 267 0 280 309 1 282 268 1 281 282 1 283 269 0 282 283 1 284 263 1 283 317 1 285 260 0
		 284 305 1 285 278 1 286 271 1 287 270 0 286 287 1 288 277 1 287 311 1 289 255 0 288 299 1
		 290 256 1 289 290 1 291 257 0 290 291 1 292 276 1 291 303 1 293 272 0 292 315 1 293 286 1
		 294 263 0 295 262 1 294 295 1 296 261 0 295 296 1 297 280 1 296 297 1 298 255 1 297 298 1
		 299 289 1 298 299 1 300 252 0 299 300 1 301 253 1 300 301 1 302 254 0 301 302 1 303 292 1
		 302 303 1 304 257 1 303 304 1 305 285 1 304 305 1 305 294 1 306 266 0 307 265 1 306 307 1
		 308 264 0 307 308 1 309 281 1 308 309 1 310 277 1 309 310 1 311 288 1 310 311 1 312 273 0
		 311 312 1 313 274 1 312 313 1 314 275 0 313 314 1 315 293 1 314 315 1 316 276 1 315 316 1
		 317 284 1 316 317 1 317 306 1 318 319 0 318 323 0 319 320 0 319 324 1 320 321 0 320 325 1
		 321 347 0 321 326 1 322 327 0 323 324 1 323 328 0 324 325 1 324 329 1 325 326 1 325 330 1
		 326 346 1 326 331 1 327 332 0 328 329 1 328 333 0 329 330 1 329 334 1 330 331 1 330 335 1
		 331 345 1 331 336 1 332 337 0 333 334 1 333 338 0 334 335 1 334 339 1 335 336 1 335 340 1
		 336 344 1 336 341 1 337 342 0 338 339 0 339 340 0 340 341 0 341 343 0;
	setAttr ".ed[664:829]" 343 342 0 343 344 1 344 345 1 345 346 1 346 347 1 347 322 0
		 346 327 1 345 332 1 344 337 1 318 348 0 348 349 0 348 351 1 349 350 0 349 352 1 350 367 0
		 350 353 1 323 351 1 351 352 1 351 354 1 352 353 1 352 355 1 353 366 1 353 356 1 328 354 1
		 354 355 1 354 357 1 355 356 1 355 358 1 356 365 1 356 359 1 333 357 1 357 358 1 357 360 1
		 358 359 1 358 361 1 359 364 1 359 362 1 338 360 0 360 361 0 361 362 0 362 363 0 363 342 0
		 363 364 1 364 365 1 365 366 1 366 367 1 367 322 0 366 327 1 365 332 1 364 337 1 318 431 0
		 319 430 0 368 369 0 369 370 1 371 370 1 368 371 0 320 429 0 369 372 0 372 373 1 370 373 1
		 321 428 0 372 374 0 374 375 1 373 375 1 347 437 0 376 377 1 322 436 0 377 378 0 378 379 0
		 376 379 1 370 380 1 381 380 1 371 381 0 373 382 1 380 382 1 375 383 1 382 383 1 384 376 1
		 379 385 0 384 385 1 380 386 1 387 386 1 381 387 0 382 388 1 386 388 1 383 389 1 388 389 1
		 390 384 1 385 391 0 390 391 1 339 420 0 386 392 1 338 419 0 393 392 0 387 393 0 340 421 0
		 388 394 1 392 394 0 341 422 0 389 395 1 394 395 0 343 423 0 342 424 0 396 397 0 396 390 1
		 391 397 0 389 390 1 395 396 0 383 384 1 375 376 1 374 377 0 371 398 1 348 432 0 399 398 1
		 368 399 0 398 400 1 349 433 0 401 400 1 399 401 0 400 402 1 350 434 0 403 402 1 401 403 0
		 404 379 1 367 435 0 405 378 0 404 405 1 381 406 1 398 406 1 406 407 1 400 407 1 407 408 1
		 402 408 1 409 385 1 409 404 1 387 410 1 406 410 1 410 411 1 407 411 1 411 412 1 408 412 1
		 413 391 1 413 409 1 360 418 0 393 414 0 410 414 1 361 427 0 414 415 0 411 415 1 362 426 0
		 415 416 0 412 416 1 363 425 0 417 413 1 417 397 0 416 417 0 412 413 1 408 409 1 402 404 1
		 403 405 0 418 414 0 419 393 0 418 419 1 420 392 0 419 420 1 421 394 0;
	setAttr ".ed[830:863]" 420 421 1 422 395 0 421 422 1 423 396 0 422 423 1 424 397 0
		 423 424 1 425 417 0 424 425 1 426 416 0 425 426 1 427 415 0 426 427 1 427 418 1 428 374 0
		 429 372 0 428 429 1 430 369 0 429 430 1 431 368 0 430 431 1 432 399 0 431 432 1 433 401 0
		 432 433 1 434 403 0 433 434 1 435 405 0 434 435 1 436 378 0 435 436 1 437 377 0 436 437 1
		 437 428 1;
	setAttr -s 432 -ch 1728 ".fc[0:431]" -type "polyFaces" 
		f 4 92 93 -95 -96
		mu 0 4 120 121 6 5
		f 4 97 98 -100 -94
		mu 0 4 121 122 7 6
		f 4 101 102 -104 -99
		mu 0 4 122 123 8 7
		f 4 105 107 108 -110
		mu 0 4 28 124 125 9
		f 4 94 110 -112 -113
		mu 0 4 5 6 11 10
		f 4 99 113 -115 -111
		mu 0 4 6 7 12 11
		f 4 103 115 -117 -114
		mu 0 4 7 8 13 12
		f 4 117 109 118 -120
		mu 0 4 27 28 9 14
		f 4 111 120 -122 -123
		mu 0 4 10 11 16 15
		f 4 114 123 -125 -121
		mu 0 4 11 12 17 16
		f 4 116 125 -127 -124
		mu 0 4 12 13 18 17
		f 4 127 119 128 -130
		mu 0 4 26 27 14 19
		f 4 121 131 -134 -135
		mu 0 4 15 16 126 127
		f 4 124 136 -138 -132
		mu 0 4 16 17 128 126
		f 4 126 139 -141 -137
		mu 0 4 17 18 129 128
		f 4 -144 144 129 145
		mu 0 4 130 131 26 19
		f 4 146 -145 -148 -140
		mu 0 4 18 26 131 129
		f 4 148 -128 -147 -126
		mu 0 4 13 27 26 18
		f 4 149 -118 -149 -116
		mu 0 4 8 28 27 13
		f 4 150 -106 -150 -103
		mu 0 4 123 124 28 8
		f 4 95 151 -154 -155
		mu 0 4 133 33 32 132
		f 4 153 155 -158 -159
		mu 0 4 132 32 35 134
		f 4 157 159 -162 -163
		mu 0 4 134 35 37 135
		f 4 163 -109 -166 -167
		mu 0 4 38 41 136 137
		f 4 112 167 -169 -152
		mu 0 4 33 43 42 32
		f 4 168 169 -171 -156
		mu 0 4 32 42 44 35
		f 4 170 171 -173 -160
		mu 0 4 35 44 45 37
		f 4 173 -119 -164 -175
		mu 0 4 46 47 41 38
		f 4 122 175 -177 -168
		mu 0 4 43 49 48 42
		f 4 176 177 -179 -170
		mu 0 4 42 48 50 44
		f 4 178 179 -181 -172
		mu 0 4 44 50 51 45
		f 4 181 -129 -174 -183
		mu 0 4 52 53 47 46
		f 4 134 184 -186 -176
		mu 0 4 49 138 139 48
		f 4 185 187 -189 -178
		mu 0 4 48 139 140 50
		f 4 188 190 -192 -180
		mu 0 4 50 140 141 51
		f 4 -146 -182 -194 194
		mu 0 4 143 53 52 142
		f 4 191 195 193 -197
		mu 0 4 51 141 142 52
		f 4 180 196 182 -198
		mu 0 4 45 51 52 46
		f 4 172 197 174 -199
		mu 0 4 37 45 46 38
		f 4 161 198 166 -200
		mu 0 4 135 37 38 137
		f 4 1 9 -4 -1
		mu 0 4 60 63 62 61
		f 4 3 11 -6 -3
		mu 0 4 61 62 65 64
		f 4 5 13 -8 -5
		mu 0 4 64 65 67 66
		f 4 46 -9 -46 -45
		mu 0 4 68 71 70 69
		f 4 10 18 -13 -10
		mu 0 4 63 73 72 62
		f 4 12 20 -15 -12
		mu 0 4 62 72 74 65
		f 4 14 22 -17 -14
		mu 0 4 65 74 75 67
		f 4 47 -18 -47 -44
		mu 0 4 76 77 71 68
		f 4 19 27 -22 -19
		mu 0 4 73 79 78 72
		f 4 21 29 -24 -21
		mu 0 4 72 78 80 74
		f 4 23 31 -26 -23
		mu 0 4 74 80 81 75
		f 4 48 -27 -48 -43
		mu 0 4 82 83 77 76
		f 4 28 36 -31 -28
		mu 0 4 79 85 84 78
		f 4 30 37 -33 -30
		mu 0 4 78 84 86 80
		f 4 32 38 -35 -32
		mu 0 4 80 86 87 81
		f 4 -36 -49 -42 40
		mu 0 4 88 83 82 89
		f 4 34 39 41 -34
		mu 0 4 81 87 89 82
		f 4 25 33 42 -25
		mu 0 4 75 81 82 76
		f 4 16 24 43 -16
		mu 0 4 67 75 76 68
		f 4 7 15 44 -7
		mu 0 4 66 67 68 69
		f 4 49 51 -57 -2
		mu 0 4 90 93 92 91
		f 4 50 53 -58 -52
		mu 0 4 93 95 94 92
		f 4 52 55 -60 -54
		mu 0 4 95 97 96 94
		f 4 85 86 8 -88
		mu 0 4 98 101 100 99
		f 4 56 58 -64 -11
		mu 0 4 91 92 103 102
		f 4 57 60 -65 -59
		mu 0 4 92 94 104 103
		f 4 59 62 -67 -61
		mu 0 4 94 96 105 104
		f 4 84 87 17 -89
		mu 0 4 106 98 99 107
		f 4 63 65 -71 -20
		mu 0 4 102 103 109 108
		f 4 64 67 -72 -66
		mu 0 4 103 104 110 109
		f 4 66 69 -74 -68
		mu 0 4 104 105 111 110
		f 4 83 88 26 -90
		mu 0 4 112 106 107 113
		f 4 70 72 -78 -29
		mu 0 4 108 109 115 114
		f 4 71 74 -79 -73
		mu 0 4 109 110 116 115
		f 4 73 76 -80 -75
		mu 0 4 110 111 117 116
		f 4 -82 82 89 35
		mu 0 4 118 119 112 113
		f 4 75 -83 -81 -77
		mu 0 4 111 112 119 117
		f 4 68 -84 -76 -70
		mu 0 4 105 106 112 111
		f 4 61 -85 -69 -63
		mu 0 4 96 98 106 105
		f 4 54 -86 -62 -56
		mu 0 4 97 101 98 96
		f 4 0 91 226 -91
		mu 0 4 0 1 158 160
		f 4 2 96 224 -92
		mu 0 4 1 2 157 158
		f 4 4 100 222 -97
		mu 0 4 2 3 156 157
		f 4 45 106 238 -105
		mu 0 4 29 4 165 167
		f 4 -37 132 204 -131
		mu 0 4 21 20 145 147
		f 4 -38 130 206 -136
		mu 0 4 22 21 147 148
		f 4 -39 135 208 -139
		mu 0 4 23 22 148 149
		f 4 -41 141 212 -143
		mu 0 4 24 25 150 152
		f 4 -40 138 210 -142
		mu 0 4 25 23 149 150
		f 4 6 104 239 -101
		mu 0 4 3 29 167 156
		f 4 -50 90 228 -153
		mu 0 4 31 30 159 161
		f 4 -51 152 230 -157
		mu 0 4 34 31 161 162
		f 4 -53 156 232 -161
		mu 0 4 36 34 162 163
		f 4 -87 164 236 -107
		mu 0 4 40 39 164 166
		f 4 77 183 202 -133
		mu 0 4 55 54 144 146
		f 4 78 186 219 -184
		mu 0 4 54 56 155 144
		f 4 79 189 218 -187
		mu 0 4 56 57 154 155
		f 4 81 142 214 -193
		mu 0 4 59 58 151 153
		f 4 80 192 216 -190
		mu 0 4 57 59 153 154
		f 4 -55 160 234 -165
		mu 0 4 39 36 163 164
		f 4 -203 200 -185 -202
		mu 0 4 146 144 139 138
		f 4 -205 201 133 -204
		mu 0 4 147 145 127 126
		f 4 -207 203 137 -206
		mu 0 4 148 147 126 128
		f 4 -209 205 140 -208
		mu 0 4 149 148 128 129
		f 4 -211 207 147 -210
		mu 0 4 150 149 129 131
		f 4 -213 209 143 -212
		mu 0 4 152 150 131 130
		f 4 -215 211 -195 -214
		mu 0 4 153 151 143 142
		f 4 -217 213 -196 -216
		mu 0 4 154 153 142 141
		f 4 -219 215 -191 -218
		mu 0 4 155 154 141 140
		f 4 -220 217 -188 -201
		mu 0 4 144 155 140 139
		f 4 -223 220 -102 -222
		mu 0 4 157 156 123 122
		f 4 -225 221 -98 -224
		mu 0 4 158 157 122 121
		f 4 -227 223 -93 -226
		mu 0 4 160 158 121 120
		f 4 -229 225 154 -228
		mu 0 4 161 159 133 132
		f 4 -231 227 158 -230
		mu 0 4 162 161 132 134
		f 4 -233 229 162 -232
		mu 0 4 163 162 134 135
		f 4 -235 231 199 -234
		mu 0 4 164 163 135 137
		f 4 -237 233 165 -236
		mu 0 4 166 164 137 136
		f 4 -239 235 -108 -238
		mu 0 4 167 165 125 124
		f 4 -240 237 -151 -221
		mu 0 4 156 167 124 123
		f 4 282 283 -286 -287
		mu 0 4 168 169 170 171
		f 4 288 289 -291 -284
		mu 0 4 169 172 173 170
		f 4 292 293 -295 -290
		mu 0 4 172 174 175 173
		f 4 296 298 -300 -294
		mu 0 4 174 176 177 175
		f 4 285 300 -303 -304
		mu 0 4 171 170 178 179
		f 4 290 304 -306 -301
		mu 0 4 170 173 180 178
		f 4 294 306 -308 -305
		mu 0 4 173 175 181 180
		f 4 299 309 -311 -307
		mu 0 4 175 177 182 181
		f 4 313 314 -317 -318
		mu 0 4 183 184 185 186
		f 4 316 318 -321 -322
		mu 0 4 186 185 187 188
		f 4 320 322 -325 -326
		mu 0 4 188 187 189 190
		f 4 324 327 -330 -331
		mu 0 4 190 189 191 192
		f 4 331 302 -333 -315
		mu 0 4 184 193 194 185
		f 4 332 305 -334 -319
		mu 0 4 185 194 195 187
		f 4 333 307 -335 -323
		mu 0 4 187 195 196 189
		f 4 334 310 -336 -328
		mu 0 4 189 196 197 191
		f 4 241 249 -244 -241
		mu 0 4 198 199 200 201
		f 4 243 251 -246 -243
		mu 0 4 201 200 202 203
		f 4 245 253 -248 -245
		mu 0 4 203 202 204 205
		f 4 247 255 -249 -247
		mu 0 4 205 204 206 207
		f 4 250 258 -253 -250
		mu 0 4 199 208 209 200
		f 4 252 259 -255 -252
		mu 0 4 200 209 210 202
		f 4 254 260 -257 -254
		mu 0 4 202 210 211 204
		f 4 256 261 -258 -256
		mu 0 4 204 211 212 206
		f 4 262 265 -272 -264
		mu 0 4 213 214 215 216
		f 4 264 267 -274 -266
		mu 0 4 214 217 218 215
		f 4 266 269 -276 -268
		mu 0 4 217 219 220 218
		f 4 268 270 -278 -270
		mu 0 4 219 221 222 220
		f 4 271 274 -259 -273
		mu 0 4 216 215 223 224
		f 4 273 276 -260 -275
		mu 0 4 215 218 225 223
		f 4 275 278 -261 -277
		mu 0 4 218 220 226 225
		f 4 277 279 -262 -279
		mu 0 4 220 222 227 226
		f 4 240 281 338 -281
		mu 0 4 228 229 230 231
		f 4 -242 280 340 -285
		mu 0 4 232 228 231 233
		f 4 242 287 367 -282
		mu 0 4 229 234 235 230
		f 4 244 291 366 -288
		mu 0 4 234 236 237 235
		f 4 246 295 364 -292
		mu 0 4 236 238 239 237
		f 4 248 297 362 -296
		mu 0 4 238 240 241 239
		f 4 -251 284 342 -302
		mu 0 4 242 232 233 243
		f 4 257 308 360 -298
		mu 0 4 240 244 245 241
		f 4 263 312 346 -312
		mu 0 4 246 247 248 249
		f 4 -263 311 348 -316
		mu 0 4 250 246 249 251
		f 4 -265 315 350 -320
		mu 0 4 252 250 251 253
		f 4 -267 319 352 -324
		mu 0 4 254 252 253 255
		f 4 -271 328 356 -327
		mu 0 4 256 257 258 259
		f 4 -269 323 354 -329
		mu 0 4 257 254 255 258
		f 4 272 301 344 -313
		mu 0 4 247 260 261 248
		f 4 -280 326 358 -309
		mu 0 4 262 256 259 263
		f 4 -339 336 -283 -338
		mu 0 4 231 230 169 168
		f 4 -341 337 286 -340
		mu 0 4 233 231 168 171
		f 4 -343 339 303 -342
		mu 0 4 243 233 171 179
		f 4 -345 341 -332 -344
		mu 0 4 248 261 193 184
		f 4 -347 343 -314 -346
		mu 0 4 249 248 184 183
		f 4 -349 345 317 -348
		mu 0 4 251 249 183 186
		f 4 -351 347 321 -350
		mu 0 4 253 251 186 188
		f 4 -353 349 325 -352
		mu 0 4 255 253 188 190
		f 4 -355 351 330 -354
		mu 0 4 258 255 190 192
		f 4 -357 353 329 -356
		mu 0 4 259 258 192 191
		f 4 -359 355 335 -358
		mu 0 4 263 259 191 197
		f 4 -361 357 -310 -360
		mu 0 4 241 245 182 177
		f 4 -363 359 -299 -362
		mu 0 4 239 241 177 176
		f 4 -365 361 -297 -364
		mu 0 4 237 239 176 174
		f 4 -367 363 -293 -366
		mu 0 4 235 237 174 172
		f 4 -368 365 -289 -337
		mu 0 4 230 235 172 169
		f 4 410 411 -414 -415
		mu 0 4 264 265 266 267
		f 4 416 417 -419 -412
		mu 0 4 265 268 269 266
		f 4 420 421 -423 -418
		mu 0 4 268 270 271 269
		f 4 424 426 -428 -422
		mu 0 4 270 272 273 271
		f 4 413 428 -431 -432
		mu 0 4 267 266 274 275
		f 4 418 432 -434 -429
		mu 0 4 266 269 276 274
		f 4 422 434 -436 -433
		mu 0 4 269 271 277 276
		f 4 427 437 -439 -435
		mu 0 4 271 273 278 277
		f 4 441 442 -445 -446
		mu 0 4 279 280 281 282
		f 4 444 446 -449 -450
		mu 0 4 282 281 283 284
		f 4 448 450 -453 -454
		mu 0 4 284 283 285 286
		f 4 452 455 -458 -459
		mu 0 4 286 285 287 288
		f 4 459 430 -461 -443
		mu 0 4 280 289 290 281
		f 4 460 433 -462 -447
		mu 0 4 281 290 291 283
		f 4 461 435 -463 -451
		mu 0 4 283 291 292 285
		f 4 462 438 -464 -456
		mu 0 4 285 292 293 287
		f 4 369 377 -372 -369
		mu 0 4 294 295 296 297
		f 4 371 379 -374 -371
		mu 0 4 297 296 298 299
		f 4 373 381 -376 -373
		mu 0 4 299 298 300 301
		f 4 375 383 -377 -375
		mu 0 4 301 300 302 303
		f 4 378 386 -381 -378
		mu 0 4 295 304 305 296
		f 4 380 387 -383 -380
		mu 0 4 296 305 306 298
		f 4 382 388 -385 -382
		mu 0 4 298 306 307 300
		f 4 384 389 -386 -384
		mu 0 4 300 307 308 302
		f 4 390 393 -400 -392
		mu 0 4 309 310 311 312
		f 4 392 395 -402 -394
		mu 0 4 310 313 314 311
		f 4 394 397 -404 -396
		mu 0 4 313 315 316 314
		f 4 396 398 -406 -398
		mu 0 4 315 317 318 316
		f 4 399 402 -387 -401
		mu 0 4 312 311 319 320
		f 4 401 404 -388 -403
		mu 0 4 311 314 321 319
		f 4 403 406 -389 -405
		mu 0 4 314 316 322 321
		f 4 405 407 -390 -407
		mu 0 4 316 318 323 322
		f 4 368 409 466 -409
		mu 0 4 324 325 326 327
		f 4 -370 408 468 -413
		mu 0 4 328 324 327 329
		f 4 370 415 495 -410
		mu 0 4 325 330 331 326
		f 4 372 419 494 -416
		mu 0 4 330 332 333 331
		f 4 374 423 492 -420
		mu 0 4 332 334 335 333
		f 4 376 425 490 -424
		mu 0 4 334 336 337 335
		f 4 -379 412 470 -430
		mu 0 4 338 328 329 339
		f 4 385 436 488 -426
		mu 0 4 336 340 341 337
		f 4 391 440 474 -440
		mu 0 4 342 343 344 345
		f 4 -391 439 476 -444
		mu 0 4 346 342 345 347
		f 4 -393 443 478 -448
		mu 0 4 348 346 347 349
		f 4 -395 447 480 -452
		mu 0 4 350 348 349 351
		f 4 -399 456 484 -455
		mu 0 4 352 353 354 355
		f 4 -397 451 482 -457
		mu 0 4 353 350 351 354
		f 4 400 429 472 -441
		mu 0 4 343 356 357 344
		f 4 -408 454 486 -437
		mu 0 4 358 352 355 359
		f 4 -467 464 -411 -466
		mu 0 4 327 326 265 264
		f 4 -469 465 414 -468
		mu 0 4 329 327 264 267
		f 4 -471 467 431 -470
		mu 0 4 339 329 267 275
		f 4 -473 469 -460 -472
		mu 0 4 344 357 289 280
		f 4 -475 471 -442 -474
		mu 0 4 345 344 280 279
		f 4 -477 473 445 -476
		mu 0 4 347 345 279 282
		f 4 -479 475 449 -478
		mu 0 4 349 347 282 284
		f 4 -481 477 453 -480
		mu 0 4 351 349 284 286
		f 4 -483 479 458 -482
		mu 0 4 354 351 286 288
		f 4 -485 481 457 -484
		mu 0 4 355 354 288 287
		f 4 -487 483 463 -486
		mu 0 4 359 355 287 293
		f 4 -489 485 -438 -488
		mu 0 4 337 341 278 273
		f 4 -491 487 -427 -490
		mu 0 4 335 337 273 272
		f 4 -493 489 -425 -492
		mu 0 4 333 335 272 270
		f 4 -495 491 -421 -494
		mu 0 4 331 333 270 268
		f 4 -496 493 -417 -465
		mu 0 4 326 331 268 265
		f 4 568 567 -499 -566
		mu 0 4 360 361 362 363
		f 4 570 569 -500 -568
		mu 0 4 361 364 365 362
		f 4 498 516 546 -516
		mu 0 4 363 362 366 367
		f 4 499 517 559 -517
		mu 0 4 362 365 368 366
		f 4 500 519 580 -519
		mu 0 4 369 370 371 372
		f 4 501 520 578 -520
		mu 0 4 370 373 374 371
		f 4 502 522 604 -522
		mu 0 4 375 376 377 378
		f 4 503 523 602 -523
		mu 0 4 376 379 380 377
		f 4 552 551 -507 -550
		mu 0 4 381 382 383 384
		f 4 554 553 -508 -552
		mu 0 4 382 385 386 383
		f 4 506 528 562 -528
		mu 0 4 384 383 387 388
		f 4 507 529 575 -529
		mu 0 4 383 386 389 387
		f 4 614 613 -511 -612
		mu 0 4 390 391 392 393
		f 4 616 615 -512 -614
		mu 0 4 391 394 395 392
		f 4 590 589 -497 -588
		mu 0 4 396 397 398 399
		f 4 592 591 -498 -590
		mu 0 4 397 400 401 398
		f 4 574 620 619 -572
		mu 0 4 402 403 404 405
		f 4 572 596 595 -570
		mu 0 4 364 406 407 365
		f 4 -620 622 621 -540
		mu 0 4 405 404 408 409
		f 4 -596 598 597 -518
		mu 0 4 365 407 410 368
		f 4 609 563 -608 610
		mu 0 4 411 412 413 414
		f 4 585 565 -584 586
		mu 0 4 415 360 363 416
		f 4 607 543 550 608
		mu 0 4 414 413 417 418
		f 4 583 515 548 584
		mu 0 4 416 363 367 419
		f 4 -547 544 -501 -546
		mu 0 4 367 366 370 369
		f 4 -549 545 518 582
		mu 0 4 419 367 369 420
		f 4 -551 547 521 606
		mu 0 4 418 417 421 422
		f 4 504 525 -553 -525
		mu 0 4 423 424 382 381
		f 4 505 526 -555 -526
		mu 0 4 424 425 385 382
		f 4 -622 623 -524 -556
		mu 0 4 409 408 426 427
		f 4 -598 599 -521 -558
		mu 0 4 368 410 428 373
		f 4 -560 557 -502 -545
		mu 0 4 366 368 373 370
		f 4 -563 560 -509 -562
		mu 0 4 388 387 429 430
		f 4 611 542 -610 612
		mu 0 4 431 432 412 411
		f 4 587 512 -586 588
		mu 0 4 433 434 360 415
		f 4 496 513 -569 -513
		mu 0 4 434 435 361 360
		f 4 497 514 -571 -514
		mu 0 4 435 436 364 361
		f 4 -592 594 -573 -515
		mu 0 4 436 437 406 364
		f 4 -616 618 -575 -539
		mu 0 4 438 439 403 402
		f 4 -576 573 -510 -561
		mu 0 4 387 389 440 429
		f 4 -579 576 -504 -578
		mu 0 4 371 374 379 376
		f 4 -581 577 -503 -580
		mu 0 4 372 371 376 375
		f 4 -582 -583 579 -548
		mu 0 4 417 419 420 421
		f 4 541 -585 581 -544
		mu 0 4 413 416 419 417
		f 4 566 -587 -542 -564
		mu 0 4 412 415 416 413
		f 4 533 -589 -567 -543
		mu 0 4 432 433 415 412
		f 4 510 534 -591 -534
		mu 0 4 393 392 397 396
		f 4 511 535 -593 -535
		mu 0 4 392 395 400 397
		f 4 -595 -536 538 -594
		mu 0 4 406 437 438 402
		f 4 -597 593 571 537
		mu 0 4 407 406 402 405
		f 4 -599 -538 539 558
		mu 0 4 410 407 405 409
		f 4 -600 -559 555 -577
		mu 0 4 428 410 409 427
		f 4 -603 600 -506 -602
		mu 0 4 377 380 425 424
		f 4 -605 601 -505 -604
		mu 0 4 378 377 424 423
		f 4 -606 -607 603 524
		mu 0 4 441 418 422 442
		f 4 540 -609 605 549
		mu 0 4 443 414 418 441
		f 4 564 -611 -541 527
		mu 0 4 444 411 414 443
		f 4 530 -613 -565 561
		mu 0 4 445 431 411 444
		f 4 508 531 -615 -531
		mu 0 4 430 429 391 390
		f 4 509 532 -617 -532
		mu 0 4 429 440 394 391
		f 4 -619 -533 -574 -618
		mu 0 4 403 439 446 447
		f 4 -621 617 -530 536
		mu 0 4 404 403 447 448
		f 4 -623 -537 -554 556
		mu 0 4 408 404 448 449
		f 4 -624 -557 -527 -601
		mu 0 4 426 408 449 450
		f 4 716 717 -719 -720
		mu 0 4 451 452 453 454
		f 4 721 722 -724 -718
		mu 0 4 452 455 456 453
		f 4 725 726 -728 -723
		mu 0 4 455 457 458 456
		f 4 729 731 732 -734
		mu 0 4 459 460 461 462
		f 4 718 734 -736 -737
		mu 0 4 454 453 463 464
		f 4 723 737 -739 -735
		mu 0 4 453 456 465 463
		f 4 727 739 -741 -738
		mu 0 4 456 458 466 465
		f 4 741 733 742 -744
		mu 0 4 467 459 462 468
		f 4 735 744 -746 -747
		mu 0 4 464 463 469 470
		f 4 738 747 -749 -745
		mu 0 4 463 465 471 469
		f 4 740 749 -751 -748
		mu 0 4 465 466 472 471
		f 4 751 743 752 -754
		mu 0 4 473 467 468 474
		f 4 745 755 -758 -759
		mu 0 4 470 469 475 476
		f 4 748 760 -762 -756
		mu 0 4 469 471 477 475
		f 4 750 763 -765 -761
		mu 0 4 471 472 478 477
		f 4 -768 768 753 769
		mu 0 4 479 480 473 474
		f 4 770 -769 -772 -764
		mu 0 4 472 473 480 478
		f 4 772 -752 -771 -750
		mu 0 4 466 467 473 472
		f 4 773 -742 -773 -740
		mu 0 4 458 459 467 466
		f 4 774 -730 -774 -727
		mu 0 4 457 460 459 458
		f 4 719 775 -778 -779
		mu 0 4 481 482 483 484
		f 4 777 779 -782 -783
		mu 0 4 484 483 485 486
		f 4 781 783 -786 -787
		mu 0 4 486 485 487 488
		f 4 787 -733 -790 -791
		mu 0 4 489 490 491 492
		f 4 736 791 -793 -776
		mu 0 4 482 493 494 483
		f 4 792 793 -795 -780
		mu 0 4 483 494 495 485
		f 4 794 795 -797 -784
		mu 0 4 485 495 496 487
		f 4 797 -743 -788 -799
		mu 0 4 497 498 490 489
		f 4 746 799 -801 -792
		mu 0 4 493 499 500 494
		f 4 800 801 -803 -794
		mu 0 4 494 500 501 495
		f 4 802 803 -805 -796
		mu 0 4 495 501 502 496
		f 4 805 -753 -798 -807
		mu 0 4 503 504 498 497
		f 4 758 808 -810 -800
		mu 0 4 499 505 506 500
		f 4 809 811 -813 -802
		mu 0 4 500 506 507 501
		f 4 812 814 -816 -804
		mu 0 4 501 507 508 502
		f 4 -770 -806 -818 818
		mu 0 4 509 504 503 510
		f 4 815 819 817 -821
		mu 0 4 502 508 510 503
		f 4 804 820 806 -822
		mu 0 4 496 502 503 497
		f 4 796 821 798 -823
		mu 0 4 487 496 497 489
		f 4 785 822 790 -824
		mu 0 4 488 487 489 492
		f 4 625 633 -628 -625
		mu 0 4 511 512 513 514
		f 4 627 635 -630 -627
		mu 0 4 514 513 515 516
		f 4 629 637 -632 -629
		mu 0 4 516 515 517 518
		f 4 670 -633 -670 -669
		mu 0 4 519 520 521 522
		f 4 634 642 -637 -634
		mu 0 4 512 523 524 513
		f 4 636 644 -639 -636
		mu 0 4 513 524 525 515
		f 4 638 646 -641 -638
		mu 0 4 515 525 526 517
		f 4 671 -642 -671 -668
		mu 0 4 527 528 520 519
		f 4 643 651 -646 -643
		mu 0 4 523 529 530 524
		f 4 645 653 -648 -645
		mu 0 4 524 530 531 525
		f 4 647 655 -650 -647
		mu 0 4 525 531 532 526
		f 4 672 -651 -672 -667
		mu 0 4 533 534 528 527
		f 4 652 660 -655 -652
		mu 0 4 529 535 536 530
		f 4 654 661 -657 -654
		mu 0 4 530 536 537 531
		f 4 656 662 -659 -656
		mu 0 4 531 537 538 532
		f 4 -660 -673 -666 664
		mu 0 4 539 534 533 540
		f 4 658 663 665 -658
		mu 0 4 532 538 540 533
		f 4 649 657 666 -649
		mu 0 4 526 532 533 527
		f 4 640 648 667 -640
		mu 0 4 517 526 527 519
		f 4 631 639 668 -631
		mu 0 4 518 517 519 522
		f 4 673 675 -681 -626
		mu 0 4 541 542 543 544
		f 4 674 677 -682 -676
		mu 0 4 542 545 546 543
		f 4 676 679 -684 -678
		mu 0 4 545 547 548 546
		f 4 709 710 632 -712
		mu 0 4 549 550 551 552
		f 4 680 682 -688 -635
		mu 0 4 544 543 553 554
		f 4 681 684 -689 -683
		mu 0 4 543 546 555 553
		f 4 683 686 -691 -685
		mu 0 4 546 548 556 555
		f 4 708 711 641 -713
		mu 0 4 557 549 552 558
		f 4 687 689 -695 -644
		mu 0 4 554 553 559 560
		f 4 688 691 -696 -690
		mu 0 4 553 555 561 559
		f 4 690 693 -698 -692
		mu 0 4 555 556 562 561
		f 4 707 712 650 -714
		mu 0 4 563 557 558 564
		f 4 694 696 -702 -653
		mu 0 4 560 559 565 566
		f 4 695 698 -703 -697
		mu 0 4 559 561 567 565
		f 4 697 700 -704 -699
		mu 0 4 561 562 568 567
		f 4 -706 706 713 659
		mu 0 4 569 570 563 564
		f 4 699 -707 -705 -701
		mu 0 4 562 563 570 568
		f 4 692 -708 -700 -694
		mu 0 4 556 557 563 562
		f 4 685 -709 -693 -687
		mu 0 4 548 549 557 556
		f 4 678 -710 -686 -680
		mu 0 4 547 550 549 548
		f 4 624 715 850 -715
		mu 0 4 571 572 573 574
		f 4 626 720 848 -716
		mu 0 4 572 575 576 573
		f 4 628 724 846 -721
		mu 0 4 575 577 578 576
		f 4 669 730 862 -729
		mu 0 4 579 580 581 582
		f 4 -661 756 828 -755
		mu 0 4 583 584 585 586
		f 4 -662 754 830 -760
		mu 0 4 587 583 586 588
		f 4 -663 759 832 -763
		mu 0 4 589 587 588 590
		f 4 -665 765 836 -767
		mu 0 4 591 592 593 594
		f 4 -664 762 834 -766
		mu 0 4 592 589 590 593
		f 4 630 728 863 -725
		mu 0 4 577 579 582 578
		f 4 -674 714 852 -777
		mu 0 4 595 596 597 598
		f 4 -675 776 854 -781
		mu 0 4 599 595 598 600
		f 4 -677 780 856 -785
		mu 0 4 601 599 600 602
		f 4 -711 788 860 -731
		mu 0 4 603 604 605 606
		f 4 701 807 826 -757
		mu 0 4 607 608 609 610
		f 4 702 810 843 -808
		mu 0 4 608 611 612 609
		f 4 703 813 842 -811
		mu 0 4 611 613 614 612
		f 4 705 766 838 -817
		mu 0 4 615 616 617 618
		f 4 704 816 840 -814
		mu 0 4 613 615 618 614
		f 4 -679 784 858 -789
		mu 0 4 604 601 602 605
		f 4 -827 824 -809 -826
		mu 0 4 610 609 506 505
		f 4 -829 825 757 -828
		mu 0 4 586 585 476 475
		f 4 -831 827 761 -830
		mu 0 4 588 586 475 477
		f 4 -833 829 764 -832
		mu 0 4 590 588 477 478
		f 4 -835 831 771 -834
		mu 0 4 593 590 478 480
		f 4 -837 833 767 -836
		mu 0 4 594 593 480 479
		f 4 -839 835 -819 -838
		mu 0 4 618 617 509 510
		f 4 -841 837 -820 -840
		mu 0 4 614 618 510 508
		f 4 -843 839 -815 -842
		mu 0 4 612 614 508 507
		f 4 -844 841 -812 -825
		mu 0 4 609 612 507 506
		f 4 -847 844 -726 -846
		mu 0 4 576 578 457 455
		f 4 -849 845 -722 -848
		mu 0 4 573 576 455 452
		f 4 -851 847 -717 -850
		mu 0 4 574 573 452 451
		f 4 -853 849 778 -852
		mu 0 4 598 597 481 484
		f 4 -855 851 782 -854
		mu 0 4 600 598 484 486
		f 4 -857 853 786 -856
		mu 0 4 602 600 486 488
		f 4 -859 855 823 -858
		mu 0 4 605 602 488 492
		f 4 -861 857 789 -860
		mu 0 4 606 605 492 491
		f 4 -863 859 -732 -862
		mu 0 4 582 581 461 460
		f 4 -864 861 -775 -845
		mu 0 4 578 582 460 457;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dsm" 2;
parent -s -nc -r -add "|pCube11|pCubeShape11" "pCube12" ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 5;
	setAttr -s 7 ".dli[1:6]"  1 2 3 0 4 5;
	setAttr -s 6 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode displayLayer -n "imgPLN";
	setAttr ".do" 1;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 0\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 0\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n"
		+ "                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n"
		+ "                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n"
		+ "            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"perPolygonSort\" \n"
		+ "                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 0\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n"
		+ "            -transparencyAlgorithm \"perPolygonSort\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 0\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n"
		+ "                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n"
		+ "            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n"
		+ "                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n"
		+ "                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n"
		+ "                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n"
		+ "                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n"
		+ "                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n"
		+ "                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n"
		+ "                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"perPolygonSort\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"perPolygonSort\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 0\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode displayLayer -n "head";
	setAttr ".c" 30;
	setAttr ".do" 2;
createNode displayLayer -n "body";
	setAttr ".c" 31;
	setAttr ".do" 3;
createNode displayLayer -n "limbs";
	setAttr ".c" 30;
	setAttr ".do" 4;
createNode displayLayer -n "bow";
	setAttr ".c" 12;
	setAttr ".do" 5;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1.3537328726025699 0 0 0 0 1.3537328726025699 0 0 0 0 1.3537328726025699 0
		 0.33028732307709063 48.542118156309321 3.5726893504498323 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.3502755 48.559605 9.2664642 ;
	setAttr ".rs" 455488446;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.4129643572530186 47.059760772642221 8.6655735747551859 ;
	setAttr ".cbx" -type "double3" 5.2875866618996277 50.05945153842702 9.8673543296231649 ;
createNode polyCube -n "polyCube2";
	setAttr ".w" 33.098717508828017;
	setAttr ".h" 9.1097224871859179;
	setAttr ".d" 11.932621568397032;
	setAttr ".sw" 3;
	setAttr ".sh" 3;
	setAttr ".sd" 3;
	setAttr ".cuv" 4;
createNode shadingEngine -n "lambert2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode file -n "file1";
	setAttr ".ftn" -type "string" "/Users/oneanimefan/Desktop/brownn.png";
createNode place2dTexture -n "place2dTexture1";
createNode file -n "file2";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "/Users/oneanimefan/Desktop/Fur0037_1_S.jpg";
createNode place2dTexture -n "place2dTexture2";
createNode bump2d -n "bump2d1";
	setAttr ".bi" 1;
	setAttr ".vc2" -type "float3" 9.9999997e-06 9.9999997e-06 0 ;
createNode polyPlane -n "polyPlane1";
	setAttr ".w" 6.7469291567458356;
	setAttr ".h" 6.7469291567458356;
	setAttr ".sw" 4;
	setAttr ".sh" 4;
	setAttr ".cuv" 2;
createNode groupId -n "groupId1";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :initialShadingGroup;
	setAttr -s 14 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "head.di" "pCube1.do";
connectAttr "polyExtrudeFace1.out" "pCubeShape1.i";
connectAttr "imgPLN.di" "frontOrtho.do";
connectAttr ":frontShape.msg" "frontOrthoShape.ltc";
connectAttr "imgPLN.di" "sideOrtho.do";
connectAttr ":sideShape.msg" "sideOrthoShape.ltc";
connectAttr "head.di" "pCube2.do";
connectAttr "head.di" "pCube3.do";
connectAttr "body.di" "pCube4.do";
connectAttr "limbs.di" "pCube5.do";
connectAttr "limbs.di" "pCube7.do";
connectAttr "limbs.di" "pCube6.do";
connectAttr "limbs.di" "pCube8.do";
connectAttr "bow.di" "polySurface4.do";
connectAttr "bow.di" "polySurface5.do";
connectAttr "polyCube2.out" "pCubeShape10.i";
connectAttr "polyPlane1.out" "pPlaneShape4.i";
connectAttr "groupId1.id" "polySurfaceShape9.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape9.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "layerManager.dli[1]" "imgPLN.id";
connectAttr "layerManager.dli[2]" "head.id";
connectAttr "layerManager.dli[3]" "body.id";
connectAttr "layerManager.dli[5]" "limbs.id";
connectAttr "layerManager.dli[6]" "bow.id";
connectAttr "polySurfaceShape1.o" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr "file2.oa" "bump2d1.bv";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|pCube12|pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape9.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of bear_001.ma
