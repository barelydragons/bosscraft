//Maya ASCII 2014 scene
//Name: dragon.ma
//Last modified: Wed, May 28, 2014 03:43:19 PM
//Codeset: UTF-8
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010035-864206";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".t" -type "double3" 162.95534515470186 46.150053529418415 122.72657839738417 ;
	setAttr ".r" -type "double3" -372.59999999422683 3652.7999999998519 -1.3151502389218114e-15 ;
	setAttr ".rp" -type "double3" 0 3.5527136788005009e-15 -7.1054273576010019e-15 ;
	setAttr ".rpt" -type "double3" -3.3892464440950435e-15 4.3136446799896797e-15 1.4186625825481476e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v";
	setAttr ".fl" 334.57139458432442;
	setAttr ".coi" 209.69401179864548;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 3.7422623038291927 3.2460943460464482 1.6085203886032104 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".t" -type "double3" 1.4062377873061078 100.37149993952868 0.3315826481921384 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 2.5647029100638661;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".t" -type "double3" 0.67695692782046335 1.73441516260108 100.10325400138881 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 5.4170611836455036;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".t" -type "double3" 100.20832265078836 2.8073053859128341 -0.1788894201041239 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 12.008454367200461;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "dragon_wing_left";
	setAttr ".t" -type "double3" 0.9025312988787092 1.9661410230136491 0 ;
	setAttr ".r" -type "double3" 0 0 35 ;
	setAttr ".s" -type "double3" 0.42934112238818822 0.42934112238818822 0.42934112238818822 ;
createNode transform -n "curve1" -p "dragon_wing_left";
createNode nurbsCurve -n "curveShape1" -p "|dragon_wing_left|curve1";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		6.6671488285064697 -1.9896903038024905 -1.4901161193847656e-08
		7.2638380011126484 -2.1600448026002517 -1.4901161193847656e-08
		7.8605271737188254 -2.3303993013980131 -1.4901161193847656e-08
		8.0083396160886977 -2.333732660606882 -1.1175870895385742e-08
		8.5763658148192512 -2.0839230100290882 -1.1175870895385742e-08
		9.1443920135498047 -1.8341133594512939 -1.1175870895385742e-08
		;
createNode transform -n "curve2" -p "dragon_wing_left";
createNode nurbsCurve -n "curveShape2" -p "|dragon_wing_left|curve2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		9.4844417572021484 -1.7633609771728516 -7.4505805969238314e-09
		9.9633236471091227 -1.5545929423261513 -7.4505805969238314e-09
		10.442205537016097 -1.3458249074794506 -7.4505805969238314e-09
		10.643261536210826 -1.4905662607652583 -3.7252902984619157e-09
		10.943365387246038 -1.0629570519677125 -3.7252902984619157e-09
		11.24346923828125 -0.63534784317016602 -3.7252902984619157e-09
		;
createNode transform -n "curve3" -p "dragon_wing_left";
createNode nurbsCurve -n "curveShape3" -p "|dragon_wing_left|curve3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		12.04529857635498 1.8644154071807859 -7.4505805969238314e-09
		12.061481177454825 1.2553858201418888 -7.4505805969238314e-09
		12.077663778554667 0.64635623310299151 -7.4505805969238314e-09
		12.295726575881638 0.46447394779543766 -9.3132257461547852e-09
		11.943649668708639 -0.03273905072996669 -9.3132257461547852e-09
		11.591572761535645 -0.52995204925537109 -9.3132257461547852e-09
		;
createNode transform -n "curve4" -p "dragon_wing_left";
createNode nurbsCurve -n "curveShape4" -p "|dragon_wing_left|curve4";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		3.4317564219236374 0.33367514610290527 -1.1175870895385742e-08
		3.6692549456716721 -0.5646542169361668 0.00083184417270952039
		3.9067534694197064 -1.4629835799752389 0.0016636995212899362
		4.551484077947805 -1.4611134349099828 -2.6077032089233398e-08
		5.4423756449309337 -1.7251477151509145 -2.6077032089233398e-08
		6.3332672119140625 -1.9891819953918457 -2.6077032089233398e-08
		;
createNode transform -n "dragon_wing_right";
	setAttr ".t" -type "double3" -0.903 1.9661410230136491 0 ;
	setAttr ".r" -type "double3" 0 0 -35 ;
	setAttr ".s" -type "double3" -0.429 0.42934112238818822 0.42934112238818822 ;
createNode transform -n "curve1" -p "dragon_wing_right";
createNode nurbsCurve -n "curveShape1" -p "|dragon_wing_right|curve1";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		6.6671488285064697 -1.9896903038024905 -1.4901161193847656e-08
		7.2638380011126484 -2.1600448026002517 -1.4901161193847656e-08
		7.8605271737188254 -2.3303993013980131 -1.4901161193847656e-08
		8.0083396160886977 -2.333732660606882 -1.1175870895385742e-08
		8.5763658148192512 -2.0839230100290882 -1.1175870895385742e-08
		9.1443920135498047 -1.8341133594512939 -1.1175870895385742e-08
		;
createNode transform -n "curve2" -p "dragon_wing_right";
createNode nurbsCurve -n "curveShape2" -p "|dragon_wing_right|curve2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		9.4844417572021484 -1.7633609771728516 -7.4505805969238314e-09
		9.9633236471091227 -1.5545929423261513 -7.4505805969238314e-09
		10.442205537016097 -1.3458249074794506 -7.4505805969238314e-09
		10.643261536210826 -1.4905662607652583 -3.7252902984619157e-09
		10.943365387246038 -1.0629570519677125 -3.7252902984619157e-09
		11.24346923828125 -0.63534784317016602 -3.7252902984619157e-09
		;
createNode transform -n "curve3" -p "dragon_wing_right";
createNode nurbsCurve -n "curveShape3" -p "|dragon_wing_right|curve3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		12.04529857635498 1.8644154071807859 -7.4505805969238314e-09
		12.061481177454825 1.2553858201418888 -7.4505805969238314e-09
		12.077663778554667 0.64635623310299151 -7.4505805969238314e-09
		12.295726575881638 0.46447394779543766 -9.3132257461547852e-09
		11.943649668708639 -0.03273905072996669 -9.3132257461547852e-09
		11.591572761535645 -0.52995204925537109 -9.3132257461547852e-09
		;
createNode transform -n "curve4" -p "dragon_wing_right";
createNode nurbsCurve -n "curveShape4" -p "|dragon_wing_right|curve4";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		5 1 0 no 3
		10 0 0 0 0 0 1 1 1 1 1
		6
		3.4317564219236374 0.33367514610290527 -1.1175870895385742e-08
		3.6692549456716721 -0.5646542169361668 0.00083184417270952039
		3.9067534694197064 -1.4629835799752389 0.0016636995212899362
		4.551484077947805 -1.4611134349099828 -2.6077032089233398e-08
		5.4423756449309337 -1.7251477151509145 -2.6077032089233398e-08
		6.3332672119140625 -1.9891819953918457 -2.6077032089233398e-08
		;
createNode joint -n "dragon_joints";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0 1.1529652440010061 -0.9886062077219514 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90 -68.385221057213755 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 1.1102230246251563e-16 0.36836436810895223 0.92968150046545084 0
		 1.6653345369377348e-16 0.92968150046545084 -0.36836436810895223 0 -0.99999999999999989 1.6653345369377348e-16 0 0
		 0 1.1529652440010061 -0.9886062077219514 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint30" -p "dragon_joints";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.24538830067369513 5.5511151231257827e-17 2.7243574138157718e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.8651651173825994e-16 -3.6009689073269701e-15 -15.388949878360418 ;
	setAttr ".bps" -type "matrix" 6.2848763694798264e-17 0.10844751017541868 0.9941021766079946 0
		 1.900246714121351e-16 0.9941021766079946 -0.10844751017541868 0 -0.99999999999999989 1.6653345369377348e-16 0 0
		 6.162975822039154e-33 1.2433575503200014 -0.76047324415496331 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint31" -p "joint30";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.23814629946066615 -2.3245294578089215e-16 1.4967200499594025e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.4539050011383533e-16 -1.9234530118664141e-15 -8.6453382810820969 ;
	setAttr ".bps" -type "matrix" 3.3570588064470679e-17 -0.042215852683816266 0.99910851351701435 0
		 1.9731284413403028e-16 0.99910851351701435 0.042215852683816266 0 -0.99999999999999989 1.6653345369377348e-16 0 0
		 1.2325951644078307e-32 1.2691839235539999 -0.52373148950997583 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint32" -p "joint31";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.30588477541161446 -2.688821387764051e-16 1.0268731790536412e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -8.1858284983773492e-16 -5.265041288084203e-15 17.674627919713995 ;
	setAttr ".bps" -type "matrix" 9.1892305730512813e-17 0.26311740579210796 0.96476382123773241 0
		 1.7780648748227517e-16 0.96476382123773241 -0.26311740579210796 0 -0.99999999999999989 1.6653345369377348e-16 0 0
		 -1.2325951644078307e-32 1.256270736937001 -0.21811940624099191 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint33" -p "joint32";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.24538830067369519 -3.7470027081099033e-16 2.2549296748198123e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 15.255118703057736 89.999999999999929 0 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 1.5236312358785226e-16 1.0638000326112281e-15 0
		 -1.0959160037159139e-16 1 1.1102230246251563e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 0 1.3208366700219971 0.018622348403995959 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint21" -p "joint33";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.52377600000000046 -0.25724667002199708 0.28904665159600329 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90.000000000000028 26.221966777554485 89.999999999999929 ;
	setAttr ".bps" -type "matrix" 1.2619164187220694e-15 0.89708903366018988 -0.44184982254904936 0
		 8.4029700781873729e-16 -0.44184982254904959 -0.89708903366018988 0 -1 9.0234874980604611e-16 -1.5078892424612909e-15 0
		 0.52377600000000002 1.06359 0.3076689999999998 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint22" -p "joint21";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.43511264373809511 4.9540737723230421e-07 -0.26523900000000045 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 8.148416387000402e-15 -58.08794447115821 ;
	setAttr ".bps" -type "matrix" -4.6224508593170376e-17 0.84928532528904388 0.52793412112562232 0
		 -1.3929215428003764e-15 -0.52793412112562232 0.84928532528904332 0 1 -8.3769546664632984e-16 1.4038817869434864e-15 0
		 0.7890149999999998 0.67325499999999983 0.4999229999999999 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint23" -p "joint22";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -0.50761985884711636 -1.8362925932602361e-07 2.2204460492503131e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 8.2659231251338242e-15 -42.706283147934244 ;
	setAttr ".bps" -type "matrix" 9.1076783797874036e-16 0.98215519189214651 -0.18807227078785721 0
		 9.3246141934321424e-16 -0.18807227078785729 -0.98215519189214651 0 -1 8.6072767708920227e-16 -1.2836024657418193e-15 0
		 0.78901500000000013 0.24214099999999977 0.23193299999999983 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint24" -p "joint23";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -0.55758082856725666 5.015260550744749e-07 -3.3306690738754696e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -179.99999999999997 -1.9713204540870769e-14 -52.473084728558945 ;
	setAttr ".bps" -type "matrix" -1.8472494286350577e-16 0.74741813179644867 0.66435392394559312 0
		 -7.2374028163337827e-16 -0.66435392394559301 0.747418131796449 0 1 -4.8433537729731968e-16 8.6014981589180244e-16 0
		 0.7890149999999998 -0.30549000000000032 0.33679799999999988 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint25" -p "joint24";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.49064359058770346 1.2665694293367835e-07 -1.1102230246251563e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -179.99999999999994 -5.5808661754629118e-14 -66.256344060392095 ;
	setAttr ".bps" -type "matrix" 5.8810142072828671e-16 0.90906482289428381 -0.4166547104932149 0
		 -5.5014335074190208e-16 -0.41665471049321456 -0.90906482289428436 0 -1 9.0542458537931488e-16 5.859027446891121e-17 0
		 0.7890149999999998 -0.6722060000000003 0.010837099999999655 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint26" -p "joint25";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -0.2883029352961769 -1.363597230685798e-07 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.9120541310255656e-15 7.6146701321039835e-14 7.3818184219198484 ;
	setAttr ".bps" -type "matrix" 5.1254431072458053e-16 0.8479983040050878 -0.52999894000318071 0
		 -6.211435558484216e-16 -0.52999894000318037 -0.84799830400508835 0 -1 9.0542458537931488e-16 5.859027446891121e-17 0
		 0.78901499999999969 -0.93429200000000068 0.1309599999999998 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint27" -p "joint26";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -0.41208519983042458 8.4799830418758049e-07 -2.2204460492503131e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364216671586e-07 2.6584736009960888e-14 -17.47092812754342 ;
	setAttr ".bps" -type "matrix" 6.7538120155889705e-16 0.96799689816580115 -0.25096215878372602 0
		 -4.3861283611975761e-16 -0.2509621587837258 -0.96799689816580148 0 -1 9.0542458537931488e-16 5.859027446891121e-17 0
		 0.78901499999999969 -1.2837400000000008 0.34936400000000001 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint28" -p "joint27";
	setAttr ".t" -type "double3" -0.15229412739847523 -9.679968981379794e-07 -1.1102230246251563e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -75.465544919459873 89.999999999999829 0 ;
	setAttr ".bps" -type "matrix" 1 2.0101762120656462e-15 -7.571403631400241e-16 0 2.1782488828452943e-15 -1.0000000000000002 -3.053113317719182e-16 0
		 -9.2121455528955633e-16 4.7184478546569212e-16 -1.0000000000000009 0 0.78901499999999969 -1.4311600000000009 0.38758500000000001 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint13" -p "joint33";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -0.52377584149921264 -0.2572482942473433 0.28904660846252356 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999901 -26.221966777554471 -89.999999999999929 ;
	setAttr ".bps" -type "matrix" 5.1444042067818121e-16 -0.89708903366019044 0.44184982254904914 0
		 2.6992601680641905e-16 0.44184982254904881 0.89708903366019044 0 -1 -5.4094118220665739e-16 3.7948989940147525e-16 0
		 -0.52377584149921286 1.0635883757746538 0.30766895686651891 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint14" -p "joint13";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.43511071753373309 -1.4546006916321921e-16 0.26523951716053296 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 3.4114651140392572e-15 -58.087944471158195 ;
	setAttr ".bps" -type "matrix" 4.2812399793345479e-17 -0.84928532528904355 -0.52793412112562277 0
		 -7.0184029158686809e-16 0.52793412112562299 -0.84928532528904366 0 1 6.0559446536637395e-16 -4.8349735491927986e-16 0
		 -0.78901535865974559 0.67325532264712495 0.49992255019798826 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint15" -p "joint14";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.50761988408658043 2.7755575615628914e-17 3.0224347071822627e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999997 1.8940613410488785e-14 -42.706283147934244 ;
	setAttr ".bps" -type "matrix" 5.0747661363633464e-16 -0.98215519189214673 0.1880722707878571 0
		 1.053257272825779e-15 0.18807227078785657 0.98215519189214684 0 -1 -4.9904138879461528e-16 1.0399411992384673e-15 0
		 -0.78901535865974559 0.24214120426746699 0.23193269282684892 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint16" -p "joint15";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0.55758138040937488 0 1.8432305941154501e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999997 3.4470248174822037e-14 -52.473084728558938 ;
	setAttr ".bps" -type "matrix" -5.2618259776793299e-16 -0.74741813179644867 -0.66435392394559312 0
		 -1.6105918536120849e-15 0.66435392394559334 -0.74741813179644923 0 1 8.7543368858649796e-16 -1.4633938490884836e-15 0
		 -0.78901535865974548 -0.30549024340399067 0.33679828918946803 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint17" -p "joint16";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0.4906438205440069 2.4980018054066022e-16 2.9518080394967184e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999994 6.193010812484153e-14 -66.256344060392067 ;
	setAttr ".bps" -type "matrix" 1.2624000546654198e-15 -0.90906482289428436 0.41665471049321462 0
		 2.140784837228407e-15 0.4166547104932139 0.90906482289428436 0 -1 -4.5434448050450395e-16 2.3821339394491976e-15 0
		 -0.78901535865974548 -0.67220633113246409 0.010837141751399436 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint18" -p "joint17";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.2883024504093622 4.163336342344337e-17 3.1162159025049709e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.9796732834426443e-15 -7.719493394741975e-14 7.3818184219198484 ;
	setAttr ".bps" -type "matrix" 1.5269872340930528e-15 -0.84799830400508835 0.52999894000318049 0
		 1.960847674921018e-15 0.52999894000317982 0.84799830400508835 0 -1 -4.5434448050450395e-16 2.3821339394491976e-15 0
		 -0.78901535865974548 -0.93429194715383901 0.13095971576119664 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint19" -p "joint18";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.41208512608818071 1.1102230246251563e-16 5.5520466532492054e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.0631522760006317e-15 -3.9459512810926105e-14 -17.47092812754342 ;
	setAttr ".bps" -type "matrix" 8.6785709944661062e-16 -0.96799689816580148 0.2509621587837258 0
		 2.3288275316704817e-15 0.25096215878372513 0.96799689816580148 0 -1 -4.5434448050450395e-16 2.3821339394491976e-15 0
		 -0.78901535865974548 -1.2837394351823392 0.34936439577900946 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint20" -p "joint19";
	setAttr ".t" -type "double3" 0.15229713988894664 -4.163336342344337e-17 1.0488679957907088e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -75.465544919459902 89.999999999999929 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 -4.1934067313258677e-16 -2.2129693880616065e-15 0
		 7.9907647786141022e-16 1.0000000000000004 -2.5124649499516525e-30 0 2.4720971182130496e-15 -8.6042284408449632e-16 1.0000000000000009 0
		 -0.78901535865974548 -1.4311625941943626 0.38758521478212654 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint34" -p "joint33";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 7.2226763195609237e-16 -0.52026198141731472 0.72942580012829483 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999999999915 -34.750219821101197 89.999999999999929 ;
	setAttr ".bps" -type "matrix" 1.8917214581693933e-16 0.8216447510854078 0.56999991492437818 0
		 -1.8386159074898961e-16 0.56999991492437807 -0.8216447510854078 0 -0.99999999999999989 1.4170330105921026e-17 2.684675969389598e-16 0
		 1.97215226305253e-31 0.80057468860468228 0.74804814853229074 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint35" -p "joint34";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.44541451074890959 1.1102230246251563e-16 9.890188906711774e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 -3.8920963104916522e-15 47.554485886388051 ;
	setAttr ".bps" -type "matrix" -8.0048868548804868e-18 0.97513285579145936 -0.22162110358896975 0
		 1.4121525313322128e-16 0.2216211035889698 0.97513285579145947 0 0.99999999999999989 1.2970427407452561e-17 -1.4904826388011695e-16 0
		 -1.4641870290744151e-17 1.1665471834187988 1.0019343817652526 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint36" -p "joint35";
	setAttr ".t" -type "double3" 0.34168132455793188 1.3877787807814457e-17 -2.3210376645145092e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4987750896777801e-16 3.2652166092832813e-15 -5.2562058709122983 ;
	setAttr ".bps" -type "matrix" -2.0907881824352214e-17 0.95072983948147893 -0.31002059983091634 0
		 1.3988812104198885e-16 0.31002059983091634 0.95072983948147904 0 0.99999999999999989 1.2970427407452561e-17 -1.4904826388011695e-16 0
		 -4.0587367279401177e-17 1.4997318692055834 0.92621058954098268 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint37" -p "joint36";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.24425406655418497 -3.1918911957973251e-16 -1.391974141508299e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 7.1118267799516618e-15 42.117821386210849 ;
	setAttr ".bps" -type "matrix" 7.830821712942497e-17 0.91313788243751148 0.40765084037388855 0
		 4.6782245769077736e-18 0.40765084037388838 -0.91313788243751137 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -5.9613943853116571e-17 1.731951498693342 0.85048679731671339 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint38" -p "joint37";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.30959416214628238 1.5265566588595902e-16 3.8428311334273713e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.8498749737768387e-16 -6.885754687861874e-15 -3.0777901720652028 ;
	setAttr ".bps" -type "matrix" 7.7944080053059187e-17 0.88993325055687555 0.45609079091039934 0
		 8.8759810648428962e-18 0.45609079091039928 -0.88993325055687533 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -7.3798488316036806e-17 2.0146536563306139 0.97669311769049516 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint39" -p "joint38";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0.45380956936195688 -1.0269562977782698e-15 5.4538421436736283e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 9.262077476258122e-16 -4.7070976666868267e-15 -22.263565732918583 ;
	setAttr ".bps" -type "matrix" 6.8770590399708263e-17 0.65079137345596916 0.75925660236529635 0
		 3.774478177518983e-17 0.75925660236529613 -0.65079137345596894 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -9.2965140349580404e-17 2.4185138815267155 1.1836714831034989 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint40" -p "joint39";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0.46542591259097249 -3.8857805861880479e-16 3.8236764484063499e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.2160814883191867e-16 -1.4553818345494347e-15 -24.921770522585057 ;
	setAttr ".bps" -type "matrix" 4.6462033594783338e-17 0.27025639012822128 0.96278838983177539 0
		 6.3208718460497001e-17 0.96278838983177562 -0.27025639012822117 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -9.9194290037439715e-17 2.7214090504237922 1.5370491801500883 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint41" -p "joint40";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.59774383133660369 -5.5511151231257827e-17 1.5183413529484198e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.8511584748183489e-16 1.8585822602568585e-15 -23.413122221442343 ;
	setAttr ".bps" -type "matrix" 1.75200247106537e-17 -0.13456727237027519 0.99090446018111356 0
		 7.6466406933497506e-17 0.99090446018111356 0.13456727237027524 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -8.6605309594288131e-17 2.8829531405022348 2.1125500010545331 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint42" -p "joint41";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.41266186039400354 -1.1796119636642288e-16 -1.3386082181459268e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0409142483495532e-16 -6.6417560787205173e-16 17.814196086565254 ;
	setAttr ".bps" -type "matrix" 4.0073462858702538e-17 0.17503333471749527 0.98456250778590659 0
		 6.7440197432194244e-17 0.98456250778590659 -0.17503333471749519 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -6.5989381421581612e-17 2.8274223595377705 2.5214584790655872 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint43" -p "joint42";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.69219996153522045 -1.2490009027033011e-16 8.0240173731590486e-18 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4759754038447876e-15 4.3898157046853802e-15 -37.168088601579107 ;
	setAttr ".bps" -type "matrix" -8.8111572058717275e-18 -0.45535053624049199 0.89031224250006613 0
		 7.7951434649238905e-17 0.89031224250006613 0.45535053624049215 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -4.6274549345363678e-17 2.9485804270966014 3.202972609084012 1;
	setAttr ".radi" 0.52511691429873808;
createNode joint -n "joint44" -p "joint43";
	setAttr ".t" -type "double3" 1.4855936764422708 7.7715611723760958e-16 -1.1382134089193288e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -27.087490614036749 89.999999999999972 0 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 -2.8972272708071156e-16 4.5700144997522678e-16 0
		 2.9545788667228548e-16 1 1.1102230246251563e-16 0 -5.274609658753118e-16 5.5511151231257852e-17 1 0
		 5.4456992119387451e-17 2.2721145498931303 4.5256148466012487 1;
	setAttr ".radi" 0.52511691429873808;
createNode joint -n "joint45" -p "joint43";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.28781450355232341 -0.74326884051930053 -1.1142417147510092e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.5463810373153749e-14 3.3505513871635102e-14 -57.631268757518122 ;
	setAttr ".bps" -type "matrix" -7.0556551510216788e-17 -0.99575488812622948 0.092044569490659811 0
		 3.4290460687591776e-17 0.092044569490659922 0.99575488812622948 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 4.6747708442519338e-18 2.1557825903830974 3.1207695200644121 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint46" -p "joint45";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.46653122221885679 -0.0024785635548406296 2.6868761336959783e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.8339663194139667e-14 -2.2610100220865985e-14 78.09288466593739 ;
	setAttr ".bps" -type "matrix" 1.89950155828764e-17 -0.11538574471225954 0.99332075882727744 0
		 7.6113415625548142e-17 0.99332075882727733 0.11538574471225964 0 -0.99999999999999989 3.6952402275908542e-17 3.7221125389387917e-17 0
		 -2.9701466782309397e-16 1.6910037070798063 3.1612431437922339 1;
	setAttr ".radi" 0.50907546379561874;
createNode joint -n "joint47" -p "joint46";
	setAttr ".t" -type "double3" 1.1754589667152957 3.0531133177191805e-16 4.6386043210879119e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -6.6258747056174858 89.999999999999957 0 ;
	setAttr ".bps" -type "matrix" 1 -1.1381474857175111e-16 6.2446342098351753e-16 0
		 1.5668000859414094e-16 1.0000000000000004 -2.775557561562892e-17 0 -6.3848326833782745e-16 9.714451465470121e-17 1.0000000000000009 0
		 -7.3854723854209615e-16 1.5553724988266593 4.3288509365801993 1;
	setAttr ".radi" 0.50907546379561874;
createNode joint -n "joint69" -p "joint33";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.378511 0.23360332997800293 0.070279251596004205 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999997 4.90712992250267e-14 -33.499400078347577 ;
	setAttr ".bps" -type "matrix" 0.83389160113731997 -0.5519282539901692 8.2581756693200548e-16 0
		 -0.55192825399016909 -0.83389160113731997 -1.1316797039142143e-16 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -0.37851099999999999 1.55444 0.088901599999999789 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint70" -p "joint69";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -0.37974876386915701 4.8833652042112874e-06 0.088901600000000011 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.1655343468645067e-15 -3.8727201118969402e-15 -33.499400078347577 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 1.1102230246251563e-16 7.5110293344200413e-16 0
		 1.6653345369377348e-16 -1 3.6142222780402828e-16 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -0.69518300000000011 1.7640299999999998 -5.4123372450476381e-16 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint71" -p "joint70";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -0.11265499999999984 -0.27624000000000049 -0.123635 ;
	setAttr ".r" -type "double3" 0 0 1.1185233270963555 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.7285767754882267e-17 -1.2811645051484977e-16 32.453191732372929 ;
	setAttr ".bps" -type "matrix" 0.8438301157369934 -0.5366104134055657 8.2774620633264194e-16 0
		 -0.53661041340556559 -0.84383011573699362 -9.8070695306651885e-17 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -0.80783799999999995 2.0402700000000005 0.12363499999999927 1;
	setAttr ".radi" 0.52190410007148968;
createNode joint -n "joint72" -p "joint71";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -1.4234749720598772 1.769585487254588e-06 -1.118125 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.4543399102816202e-17 5.2587245595226087e-16 5.3443128861504059 ;
	setAttr ".bps" -type "matrix" 0.79018170533604337 -0.61287263974844097 8.1501359164812483e-16 0
		 -0.61287263974844086 -0.79018170533604348 -1.7474120522885978e-16 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -2.0090100000000004 2.8041200000000011 1.2417599999999982 1;
	setAttr ".radi" 0.5265213131757539;
createNode joint -n "joint73" -p "joint72";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -1.512750183237014 4.047272146201486e-06 -1.0676800000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364628770278e-07 -4.9503168037273961e-16 -8.3437016044850036 ;
	setAttr ".bps" -type "matrix" 0.8707524489373264 -0.4917216414493556 8.3174384981071458e-16 0
		 -0.49172164144935554 -0.87075244893732651 -5.4624277368786659e-17 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -3.2043600000000012 3.7312400000000014 2.3094399999999968 1;
	setAttr ".radi" 0.51255608241644324;
createNode joint -n "joint74" -p "joint73";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -1.2427448179173457 -6.2913759331095775e-06 -0.75026999999999955 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.8722790192031834e-18 -6.2620414765411003e-16 -1.0745599279303999 ;
	setAttr ".bps" -type "matrix" 0.87982082260744388 -0.47530550186838882 8.3262197413659307e-16 0
		 -0.47530550186838871 -0.87982082260744388 -3.9016554959315881e-17 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -4.2864800000000018 4.3423300000000005 3.0597099999999955 1;
	setAttr ".radi" 0.51128293037202577;
createNode joint -n "joint75" -p "joint74";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.218140346101289 1.0011754186578514e-05 -0.66760000000000064 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.2801241185791573e-16 -2.1068736545642632e-15 -12.353375637657972 ;
	setAttr ".bps" -type "matrix" 0.96113715538881084 -0.27607131059040657 8.2169124495900173e-16 0
		 -0.27607131059040652 -0.96113715538881106 1.4001836070221736e-16 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -5.3582300000000025 4.9213100000000027 3.7273099999999952 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint76" -p "joint75";
	setAttr ".t" -type "double3" -0.60239260989130461 -6.9529070803042714e-06 -0.1984799999999991 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4516682522862305e-16 -3.8727201118969347e-15 -16.025867448449922 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 -4.4408920985006262e-16 7.5110293344200453e-16 0
		 -4.4408920985006262e-16 -1.0000000000000002 3.6142222780402779e-16 0 7.5565553515636037e-16 -3.6142222780402858e-16 -1 0
		 -5.937210000000003 5.0876200000000038 3.9257899999999935 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint67" -p "joint33";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.37851062424888671 0.23359942401301792 0.070279211438350447 ;
	setAttr ".r" -type "double3" 0 0 14.794303356199753 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.0988480322241657e-14 5.2582175209147569e-14 33.499400078347527 ;
	setAttr ".bps" -type "matrix" 0.8338916011373203 0.55192825399016876 9.483702580362154e-16 0
		 -0.55192825399016865 0.83389160113732053 -4.9456072903138043e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 0.3785106242488866 1.554436094035015 0.088901559842346836 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint68" -p "joint67";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.37975203443956446 -1.851611311248381e-16 -0.088901559842347044 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -33.499400078347556 ;
	setAttr ".bps" -type "matrix" 1 -2.2204460492503131e-16 1.0638000326112282e-15 0
		 2.7755575615628914e-16 1.0000000000000002 1.1102230246251617e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 0.69518265628285003 1.7640319713524582 1.5265566588595902e-16 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint61" -p "joint68";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0.11265490816986444 0.27623314628330564 0.12363478153895333 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 32.453191732372893 ;
	setAttr ".bps" -type "matrix" 0.84383011573699396 0.53661041340556526 9.5724222826099907e-16 0
		 -0.53661041340556515 0.84383011573699429 -4.7716221294403259e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 0.80783756445271437 2.040265117635764 0.12363478153895364 1;
	setAttr ".radi" 0.52190410007148968;
createNode joint -n "joint62" -p "joint61";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 1.4234792680488011 1.1102230246251563e-16 1.1181257735475969 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 5.3443128861504752 ;
	setAttr ".bps" -type "matrix" 0.79018170533604315 0.61287263974844153 9.0863785548643067e-16 0
		 -0.61287263974844142 0.79018170533604337 -5.6424614186075677e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 2.0090122399595445 2.8041189161376825 1.2417605550865518 1;
	setAttr ".radi" 0.5265213131757539;
createNode joint -n "joint63" -p "joint62";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.5127453880645758 2.55351295663786e-15 1.0676837260835546 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -8.3437016044850889 ;
	setAttr ".bps" -type "matrix" 0.87075244893732706 0.49172164144935498 9.8089855238019046e-16 0
		 -0.49172164144935482 0.87075244893732717 -4.2642055645357402e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 3.2043559704396429 3.7312391753881018 2.3094442811701077 1;
	setAttr ".radi" 0.51255608241644324;
createNode joint -n "joint64" -p "joint63";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.2427509267179035 -1.1102230246251563e-15 0.75026423995060654 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -1.0745599279303999 ;
	setAttr ".bps" -type "matrix" 0.87982082260744432 0.4753055018683881 9.88722930972367e-16 0
		 -0.47530550186838799 0.87982082260744443 -4.0795027490754412e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 4.2864843832983901 4.3423267009865354 3.0597085211207156 1;
	setAttr ".radi" 0.51128293037202577;
createNode joint -n "joint65" -p "joint64";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.2181366538591647 4.4408920985006262e-16 0.66760359396195135 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -12.353375637657974 ;
	setAttr ".bps" -type "matrix" 0.96113715538881117 0.27607131059040574 1.0531078097920717e-15 0
		 -0.27607131059040568 0.96113715538881161 -1.8697700923555935e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 5.3582263761450388 4.9213137545933447 3.7273121150826678 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint66" -p "joint65";
	setAttr ".t" -type "double3" 0.60239795159369236 -8.3266726846886741e-17 0.19847674415084929 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -16.025867448449905 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 -1.6653345369377348e-16 1.0638000326112282e-15 0
		 1.6653345369377348e-16 1.0000000000000004 1.110223024625161e-16 0 -1.0683526343255841e-15 -1.1102230246251548e-16 1 0
		 5.9372134297518473 5.0876185465867909 3.9257888592335175 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint7" -p "dragon_joints";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -0.45801838103092513 -0.10410188052769632 -0.78745900000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.2129829929429622e-14 1.4154294589884214e-14 89.198748419673407 ;
	setAttr ".bps" -type "matrix" 1.6806970821417532e-16 0.93474181064887407 -0.35532766205977351 0
		 -1.086826390186422e-16 -0.35532766205977351 -0.93474181064887407 0 -0.99999999999999989 1.6653345369377348e-16 0 0
		 0.7874589999999998 0.8874660000000002 -1.3760699999999999 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint8" -p "joint7";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.81438005586986018 0.029336187777146267 -2.2204460492503131e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 9.683231346471698e-15 -67.049934965315828 ;
	setAttr ".bps" -type "matrix" 1.6561507718921584e-16 0.69168440422950073 0.72219989265139106 0
		 1.0077006876876728e-17 -0.72219989265139106 0.69168440422950095 0 0.99999999999999989 -7.8089475005764674e-17 -8.4706909165980211e-17 0
		 0.78745900000000002 0.17572200000000027 -1.1141199999999998 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint9" -p "joint8";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.94160417935156515 0.096758632750082341 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 -3.0827410228053009e-15 -50.963628631353821 ;
	setAttr ".bps" -type "matrix" 9.6479349482482025e-17 0.99659834563597349 -0.082411998371845485 0
		 -2.5745221631504017e-16 -0.082411998371845263 -0.99659834563597349 0 -0.99999999999999989 8.81820340075064e-17 2.0675500656784448e-16 0
		 0.7874589999999998 -0.54544999999999988 -1.7272199999999995 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint10" -p "joint9";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -0.40642897545192558 -5.3942862832112581e-06 2.2204460492503131e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.3874411361144096e-15 1.6987695866606092e-14 28.962846497482222 ;
	setAttr ".bps" -type "matrix" -4.0256218121312966e-17 0.83205029433784283 -0.5547001962252307 0
		 -2.7197306018180991e-16 -0.55470019622523048 -0.83205029433784283 0 -0.99999999999999989 8.81820340075064e-17 2.0675500656784448e-16 0
		 0.78745899999999969 -0.95049600000000012 -1.6937199999999988 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint11" -p "joint10";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -0.45045871654979897 -5.5470019533210291e-07 -4.4408920985006262e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.4949054080080123e-15 2.0358940370399449e-14 8.3990946478523139 ;
	setAttr ".bps" -type "matrix" -7.9550852604017942e-17 0.74210264275113891 -0.67028625796876951 0
		 -2.631759305332898e-16 -0.67028625796876928 -0.74210264275113891 0 -0.99999999999999989 8.81820340075064e-17 2.0675500656784448e-16 0
		 0.78745900000000013 -1.3252999999999999 -1.443849999999999 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint12" -p "joint11";
	setAttr ".t" -type "double3" -0.2174614494487066 1.9151035934861227e-06 -1.1102230246251563e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -47.910837826167878 90 0 ;
	setAttr ".bps" -type "matrix" 1 -8.8182034007506437e-17 -2.067550065678445e-16 0
		 -6.3460092515435378e-18 -1 1.4988010832439611e-15 0 -2.4862539686741698e-16 -1.2212453270876722e-15 -1.0000000000000002 0
		 0.78745900000000035 -1.4866799999999998 -1.2980899999999986 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint1" -p "dragon_joints";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.45802177066836885 -0.10410030995911156 0.7874589001175466 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.6757168852055487e-15 3.3413664479219163e-14 -90.801251580326621 ;
	setAttr ".bps" -type "matrix" -1.6806970821417542e-16 -0.9347418106488744 0.35532766205977268 0
		 1.0868263901864206e-16 0.35532766205977268 0.9347418106488744 0 -0.99999999999999989 1.6653345369377348e-16 0 0
		 -0.7874589001175466 0.88746621150690363 -1.376073729824729 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint2" -p "joint1";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.75837837553336396 -0.0080392663526717735 3.3589511569740233e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 -4.0639212675979285e-15 -67.049934965315771 ;
	setAttr ".bps" -type "matrix" -1.6561507718921584e-16 -0.69168440422950095 -0.72219989265139106 0
		 -1.007700687687678e-17 0.72219989265139084 -0.69168440422950117 0 0.99999999999999989 -7.8089475005764674e-17 -8.4706909165980248e-17 0
		 -0.78745890011754704 0.17572166188612426 -1.1141155530765554 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint3" -p "joint2";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.94160514283718399 -0.0967567261282965 -1.2239232402705165e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999997 2.8135805762293233e-14 -50.963628631353792 ;
	setAttr ".bps" -type "matrix" -9.6479349482482025e-17 -0.99659834563597349 0.082411998371845041 0
		 7.0154142616510269e-16 0.08241199837184493 0.99659834563597371 0 -0.99999999999999989 1.2478031324662359e-16 6.4933357841920369e-16 0
		 -0.78745890011754716 -0.5454496275798032 -1.7272175676863195 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint4" -p "joint3";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.40642846863574894 1.3877787807814457e-16 1.9958175884823629e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.261650981355479e-14 -4.8849756603351303e-14 28.962846497482204 ;
	setAttr ".bps" -type "matrix" 2.553030297591777e-16 -0.83205029433784305 0.55470019622523037 0
		 6.6052176383379952e-16 0.55470019622523004 0.83205029433784339 0 -0.99999999999999989 1.2478031324662359e-16 6.4933357841920369e-16 0
		 -0.78745890011754749 -0.95049556704155269 -1.6937229853908389 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint5" -p "joint4";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.45046253740780662 -3.6082248300317583e-16 3.8405944553516366e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.8719142559408076e-15 -5.2731143411299753e-14 8.3990946478523547 ;
	setAttr ".bps" -type "matrix" 3.4904550764393452e-16 -0.74210264275113869 0.67028625796876984 0
		 6.1614598196849185e-16 0.67028625796876951 0.74210264275113902 0 -0.99999999999999989 1.2478031324662359e-16 6.4933357841920369e-16 0
		 -0.78745890011754771 -1.32530305387989 -1.4438513274986136 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint6" -p "joint5";
	setAttr ".t" -type "double3" 0.21745704222775336 9.7144514654701197e-17 2.0013268999840669e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -47.910837826167821 89.999999999999972 0 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 -4.5434008949362011e-16 -3.5166668374449746e-16 0
		 5.4254464957198132e-16 1 -4.9960036108132074e-16 0 4.6915936379374464e-16 5.5511151231257827e-16 1.0000000000000004 0
		 -0.78745890011754782 -1.4866784996019518 -1.2980928603948161 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint48" -p "dragon_joints";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.99360810061403837 -0.2268748621903236 -1.4809491343243099e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.544443745170814e-14 179.99999999999997 14.313723480036577 ;
	setAttr ".bps" -type "matrix" 4.1780585379558993e-16 -0.5867752483892763 -0.80974984277720163 0
		 1.339155130616231e-16 0.80974984277720163 -0.5867752483892763 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 2.4651903288156619e-32 0.57603406157138715 -1.8287726623248937 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint49" -p "joint48";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0.38119042319612584 -1.9428902930940239e-16 -4.2320638459894564e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 7.8081374860490075e-16 9.804647419667048e-15 9.1065212212413424 ;
	setAttr ".bps" -type "matrix" 4.3373461354628072e-16 -0.45121994299851315 -0.89241277615261572 0
		 6.6101294361594226e-17 0.89241277615261572 -0.45121994299851315 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 1.1694295176226504e-16 0.35236095631686692 -2.137441547576131 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint50" -p "joint49";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0.44613674074625614 1.1102230246251563e-16 -7.6344426782389483e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5019039049234189e-15 1.4024241493738344e-14 12.225405676087233 ;
	setAttr ".bps" -type "matrix" 4.3789591020828943e-16 -0.25201151940741467 -0.96772423452446721 0
		 -2.7244594459696666e-17 0.96772423452446721 -0.25201151940741467 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 2.3410347181625024e-16 0.15105516158779891 -2.5355796749291772 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint51" -p "joint50";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.44377555791987405 4.0245584642661925e-16 -1.0862258348131464e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.1539366700549501e-15 1.6458029161279673e-14 8.0213345245178349 ;
	setAttr ".bps" -type "matrix" 4.2980985409304795e-16 -0.11450787752313137 -0.99342234018827424 0
		 -8.808283277082125e-17 0.99342234018827424 -0.11450787752313137 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 3.1980839019845029e-16 0.039218608960538685 -2.9650320370178558 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint52" -p "joint51";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.30246759947492302 1.0408340855860845e-16 -8.688284922214761e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.0863682474680609e-18 1.6451917017958031e-14 -0.021497296921154256 ;
	setAttr ".bps" -type "matrix" 4.2984287239580442e-16 -0.1148806001372448 -0.99337930706860722 0
		 -8.7921562514231195e-17 0.99337930706860722 -0.1148806001372448 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 3.629290957744939e-16 0.0045836861251487629 -3.2655101075193635 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint53" -p "joint52";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.28558103032272147 1.6618650899857812e-14 -8.200177136905706e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.1928159698015752e-16 1.7050386459757795e-14 2.1455642950109541 ;
	setAttr ".bps" -type "matrix" 4.2624988195901457e-16 -0.07760951990123463 -0.99698383257738932 0
		 -1.039525576132966e-16 0.99698383257738932 -0.07760951990123463 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 4.0368229478110738e-16 -0.028224034026121676 -3.5492003935332894 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint54" -p "joint53";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.32326261801635897 2.7061686225238191e-16 -9.6198229817496722e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.8495487387520069e-16 1.771484095475949e-14 2.4897563351161569 ;
	setAttr ".bps" -type "matrix" 4.2133172456757438e-16 -0.034226510320448343 -0.99941410135703213 0
		 -1.2237105040545006e-16 0.99941410135703213 -0.034226510320448343 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 4.4527471773484564e-16 -0.053312290612387223 -3.87148799737224 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint55" -p "joint54";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.28192559994910044 1.8735013540549517e-16 -8.7166405739069115e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -6.6627697486702113e-16 1.6451013050310986e-14 -4.6384993141433952 ;
	setAttr ".bps" -type "matrix" 4.2984774949088153e-16 -0.11493571115626196 -0.99337293213626687 0
		 -8.7897715340297376e-17 0.99337293213626687 -0.11493571115626196 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 4.7689251122207904e-16 -0.062961620068643517 -4.153248417494912 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint56" -p "joint55";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.23507160834271887 -1.2490009027033011e-16 -6.7494781107231327e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 7.4589268876049125e-16 1.7759394621836699e-14 4.8100063736677594 ;
	setAttr ".bps" -type "matrix" 4.2096353654349284e-16 -0.031234752377721609 -0.99951207608707893 0
		 -1.2363169903068954e-16 0.99951207608707893 -0.031234752377721609 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 5.1044273193016738e-16 -0.089979742546160327 -4.3867621903363068 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint57" -p "joint56";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0.24714342126531019 1.6566609195578508e-16 -7.6604552442459446e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.7155429483407411e-16 1.8346383194579342e-14 2.3204119249199366 ;
	setAttr ".bps" -type "matrix" 4.1561279063966207e-16 0.0092588623686593874 -0.99995713581514989 0
		 -1.4057419521779939e-16 0.99995713581514989 0.0092588623686593874 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 5.378765481370112e-16 -0.097699206111165046 -4.6337850244164605 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint58" -p "joint57";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0.20843445062795024 -1.5287250632045613e-16 -6.6741710029423564e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.5189838035240858e-16 1.8106804876351773e-14 -0.96128853368203249 ;
	setAttr ".bps" -type "matrix" 4.1791269068074434e-16 -0.0075185844740096111 -0.99997173504429959 0
		 -1.3358173180641141e-16 0.99997173504429959 -0.0075185844740096111 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 5.577628617985149e-16 -0.095769340219913873 -4.8422105406715898 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint59" -p "joint58";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.25667941856880172 -2.3399251286582157e-15 -8.1116692840212736e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3741015740660446e-16 1.8321239818875156e-14 0.85942594970988062 ;
	setAttr ".bps" -type "matrix" 4.158620517655061e-16 0.0074810874035394813 -0.99997201627408572 0
		 -1.39835081455989e-16 0.99997201627408572 0.0074810874035394813 0 0.99999999999999989 1.6590634569887646e-16 4.5876692316185728e-16 0
		 5.839157554147594e-16 -0.097699206111165421 -5.0988827042079965 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint60" -p "joint59";
	setAttr ".t" -type "double3" 0.77389787893785933 5.4825935458246988e-15 -2.4746619656529099e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.42863873270167063 -89.999999999999915 0 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 1.7796796011036397e-16 -1.0954985714613213e-15 0
		 -1.4842945145516695e-16 1 3.7556763254897874e-16 0 1.1374157016129344e-15 -3.7643499428696689e-16 1 0
		 6.5828431864153905e-16 -0.091909608437411999 -5.872758926599726 1;
	setAttr ".radi" 0.5;
createNode transform -n "dragon_geometry";
createNode mesh -n "dragon_geometryShape" -p "dragon_geometry";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:4703]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5597 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.61048543 0.04576458 0.5 1.4901161e-08
		 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543
		 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125 0.46875 0.3125
		 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125 0.375 0.43781328
		 0.40625 0.43781328 0.4375 0.43781328 0.46875 0.43781328 0.5 0.43781328 0.53125 0.43781328
		 0.5625 0.43781328 0.59375 0.43781328 0.625 0.43781328 0.375 0.56312656 0.40625 0.56312656
		 0.4375 0.56312656 0.46875 0.56312656 0.5 0.56312656 0.53125 0.56312656 0.5625 0.56312656
		 0.59375 0.56312656 0.625 0.56312656 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985
		 0.46875 0.68843985 0.5 0.68843985 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985
		 0.625 0.68843985 0.61048543 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375
		 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001
		 0.5 0.83749998 0.40625 0.34802842 0.625 0.34802842 0.375 0.34802842 0.59375 0.34802842
		 0.5625 0.34802842 0.53125 0.34802842 0.5 0.34802842 0.46875 0.34802842 0.4375 0.34802842
		 0.53125 0.38252786 0.5 0.38252786 0.46875 0.38252786 0.4375 0.38252786 0.40625 0.38252786
		 0.625 0.38252786 0.375 0.38252786 0.59375 0.38252786 0.5625 0.38252786 0.5625 0.38923517
		 0.53125 0.38923517 0.5 0.38923517 0.46875 0.38923517 0.4375 0.38923517 0.40625 0.38923517
		 0.625 0.38923517 0.37499997 0.38923517 0.59375 0.38923517 0.53125 0.40999597 0.5
		 0.40999597 0.46875 0.40999597 0.4375 0.40999597 0.40625 0.40999597 0.625 0.40999597
		 0.375 0.40999597 0.59375 0.40999597 0.5625 0.40999597 0.53125 0.35461456 0.5 0.35461456
		 0.46875 0.35461456 0.4375 0.35461456 0.40625 0.35461456 0.625 0.35461456 0.375 0.35461456
		 0.59375 0.35461456 0.5625 0.35461456 0.59375 0.4237591 0.5625 0.4237591 0.53125 0.4237591
		 0.5 0.4237591 0.46875 0.4237591 0.4375 0.4237591 0.40625 0.4237591 0.625 0.4237591
		 0.375 0.4237591 0.59375 0.33407199 0.5625 0.33407199 0.53125 0.33407199 0.5 0.33407199
		 0.46875 0.33407199 0.4375 0.33407199 0.40625 0.33407199 0.625 0.33407199 0.375 0.33407199
		 0.5 1.4901161e-08 0.61048543 0.04576458 0.38951457 0.04576458 0.34375 0.15625 0.38951457
		 0.26673543 0.5 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.5 1.4901161e-08 0.38951457
		 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543
		 0.65625 0.15625 0.61048543 0.04576458 0.5 1.4901161e-08 0.61048543 0.04576458 0.5
		 0.15000001 0.61048543 0.26673543 0.5 0.3125 0.65625 0.15625 0.38951457 0.04576458
		 0.5 1.4901161e-08 0.5 0.15000001 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125
		 0.375 0.3125 0.40625 0.3125 0.40625 0.33407199 0.375 0.33407199 0.4375 0.3125 0.4375
		 0.33407199 0.46875 0.3125 0.46875 0.33407199 0.5 0.3125 0.5 0.33407199 0.53125 0.3125
		 0.53125 0.33407199 0.5625 0.3125 0.5625 0.33407199 0.59375 0.3125 0.59375 0.33407199
		 0.625 0.3125 0.625 0.33407199 0.375 0.43781328 0.40625 0.43781328 0.40625 0.56312656
		 0.375 0.56312656 0.4375 0.43781328 0.4375 0.56312656 0.46875 0.43781328 0.46875 0.56312656
		 0.5 0.43781328 0.5 0.56312656 0.53125 0.43781328 0.53125 0.56312656 0.5625 0.43781328
		 0.5625 0.56312656 0.59375 0.43781328 0.59375 0.56312656 0.625 0.43781328 0.625 0.56312656
		 0.40625 0.68843985 0.375 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.5 1.4901161e-08
		 0.61048543 0.04576458 0.5 0.15000001 0.38951457 0.04576458 0.5 1.4901161e-08 0.5
		 0.15000001 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543
		 0.5 0.3125 0.65625 0.15625 0.61048543 0.95423543 0.5 1 0.5 0.83749998 0.38951457
		 0.95423543 0.34375 0.84375 0.38951457 0.73326457 0.5 0.6875 0.61048543 0.73326457
		 0.65625 0.84375 0.375 0.34802842 0.40625 0.34802842 0.40625 0.35461456 0.375 0.35461456
		 0.59375 0.34802842 0.625 0.34802842 0.625 0.35461456 0.59375 0.35461456 0.5625 0.34802842
		 0.5625 0.35461456 0.53125 0.34802842 0.53125 0.35461456 0.5 0.34802842 0.5 0.35461456
		 0.46875 0.34802842 0.46875 0.35461456 0.4375 0.34802842 0.4375 0.35461456 0.5 0.38252786
		 0.53125 0.38252786 0.53125 0.38923517 0.5 0.38923517 0.46875 0.38252786 0.46875 0.38923517
		 0.4375 0.38252786 0.4375 0.38923517 0.40625 0.38252786 0.40625 0.38923517 0.375 0.38252786
		 0.37499997 0.38923517 0.59375 0.38252786 0.625 0.38252786 0.625 0.38923517 0.59375
		 0.38923517 0.5625 0.38252786 0.5625 0.38923517 0.5625 0.40999597 0.53125 0.40999597
		 0.5 0.40999597;
	setAttr ".uvst[0].uvsp[250:499]" 0.46875 0.40999597 0.4375 0.40999597 0.40625
		 0.40999597 0.375 0.40999597 0.625 0.40999597 0.59375 0.40999597 0.53125 0.4237591
		 0.5 0.4237591 0.46875 0.4237591 0.4375 0.4237591 0.40625 0.4237591 0.375 0.4237591
		 0.625 0.4237591 0.59375 0.4237591 0.5625 0.4237591 0.5 1.4901161e-08 0.61048543 0.04576458
		 0.61048543 0.04576458 0.5 1.4901161e-08 0.38951457 0.04576458 0.38951457 0.04576458
		 0.34375 0.15625 0.34375 0.15625 0.38951457 0.26673543 0.38951457 0.26673543 0.5 0.3125
		 0.5 0.3125 0.61048543 0.26673543 0.61048543 0.26673543 0.65625 0.15625 0.65625 0.15625
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.61048543 0.04576458 0.5 0.15000001
		 0.375 0.3125 0.40625 0.3125 0.40625 0.33369148 0.375 0.33369148 0.4375 0.3125 0.4375
		 0.33369148 0.46875 0.3125 0.46875 0.33369148 0.5 0.3125 0.5 0.33369148 0.53125 0.3125
		 0.53125 0.33369148 0.5625 0.3125 0.5625 0.33369148 0.59375 0.3125 0.59375 0.33369148
		 0.625 0.3125 0.625 0.33369148 0.375 0.43781328 0.40625 0.43781328 0.40625 0.56312656
		 0.375 0.56312656 0.4375 0.43781328 0.4375 0.56312656 0.46875 0.43781328 0.46875 0.56312656
		 0.5 0.43781328 0.5 0.56312656 0.53125 0.43781328 0.53125 0.56312656 0.5625 0.43781328
		 0.5625 0.56312656 0.59375 0.43781328 0.59375 0.56312656 0.625 0.43781328 0.625 0.56312656
		 0.40625 0.68843985 0.375 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.5 1.4901161e-08
		 0.61048543 0.04576458 0.5 0.15000001 0.38951457 0.04576458 0.5 1.4901161e-08 0.5
		 0.15000001 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543
		 0.5 0.3125 0.65625 0.15625 0.61048543 0.95423543 0.5 1 0.5 0.83749998 0.38951457
		 0.95423543 0.34375 0.84375 0.38951457 0.73326457 0.5 0.6875 0.61048543 0.73326457
		 0.65625 0.84375 0.375 0.42757189 0.40625 0.42757189 0.59375 0.42757189 0.625 0.42757189
		 0.5625 0.42757189 0.53125 0.42757189 0.5 0.42757189 0.46875 0.42757189 0.4375 0.42757189
		 0.375 0.37431598 0.40625 0.37431598 0.40625 0.41081479 0.375 0.41081479 0.59375 0.37431598
		 0.625 0.37431598 0.625 0.41081479 0.59375 0.41081479 0.5625 0.37431598 0.5625 0.41081479
		 0.53125 0.37431598 0.53125 0.41081479 0.5 0.37431598 0.5 0.41081479 0.46875 0.37431598
		 0.46875 0.41081479 0.4375 0.37431598 0.4375 0.41081479 0.40625 0.35625231 0.375 0.35625231
		 0.625 0.35625231 0.59375 0.35625231 0.5625 0.35625231 0.53125 0.35625231 0.5 0.35625231
		 0.46875 0.35625231 0.4375 0.35625231 0.5 0.36240453 0.53125 0.36240453 0.53125 0.36659157
		 0.5 0.36659157 0.46875 0.36240453 0.46875 0.36659157 0.4375 0.36240453 0.4375 0.36659157
		 0.40625 0.36240453 0.40625 0.36659157 0.375 0.36240453 0.375 0.36659157 0.59375 0.36240453
		 0.625 0.36240453 0.625 0.36659157 0.59375 0.36659157 0.5625 0.36240453 0.5625 0.36659157
		 0.5 1.4901161e-08 0.61048543 0.04576458 0.61048543 0.04576458 0.5 1.4901161e-08 0.38951457
		 0.04576458 0.38951457 0.04576458 0.34375 0.15625 0.34375 0.15625 0.38951457 0.26673543
		 0.38951457 0.26673543 0.5 0.3125 0.5 0.3125 0.61048543 0.26673543 0.61048543 0.26673543
		 0.65625 0.15625 0.65625 0.15625 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625
		 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.61048543
		 0.04576458 0.5 0.15000001 0.375 0.3125 0.40625 0.3125 0.40625 0.33369148 0.375 0.33369148
		 0.4375 0.3125 0.4375 0.33369148 0.46875 0.3125 0.46875 0.33369148 0.5 0.3125 0.5
		 0.33369148 0.53125 0.3125 0.53125 0.33369148 0.5625 0.3125 0.5625 0.33369148 0.59375
		 0.3125 0.59375 0.33369148 0.625 0.3125 0.625 0.33369148 0.375 0.43781328 0.40625
		 0.43781328 0.40625 0.56312656 0.375 0.56312656 0.4375 0.43781328 0.4375 0.56312656
		 0.46875 0.43781328 0.46875 0.56312656 0.5 0.43781328 0.5 0.56312656 0.53125 0.43781328
		 0.53125 0.56312656 0.5625 0.43781328 0.5625 0.56312656 0.59375 0.43781328 0.59375
		 0.56312656 0.625 0.43781328 0.625 0.56312656 0.40625 0.68843985 0.375 0.68843985
		 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985 0.53125 0.68843985 0.5625 0.68843985
		 0.59375 0.68843985 0.625 0.68843985 0.5 1.4901161e-08 0.61048543 0.04576458 0.5 0.15000001
		 0.38951457 0.04576458 0.5 1.4901161e-08 0.5 0.15000001 0.34375 0.15625 0.38951457
		 0.26673543 0.5 0.3125 0.61048543 0.26673543 0.5 0.3125 0.65625 0.15625 0.61048543
		 0.95423543 0.5 1 0.5 0.83749998 0.38951457 0.95423543 0.34375 0.84375 0.38951457
		 0.73326457 0.5 0.6875 0.61048543 0.73326457;
	setAttr ".uvst[0].uvsp[500:749]" 0.65625 0.84375 0.375 0.42757189 0.40625 0.42757189
		 0.59375 0.42757189 0.625 0.42757189 0.5625 0.42757189 0.53125 0.42757189 0.5 0.42757189
		 0.46875 0.42757189 0.4375 0.42757189 0.375 0.37431598 0.40625 0.37431598 0.40625
		 0.41081479 0.375 0.41081479 0.59375 0.37431598 0.625 0.37431598 0.625 0.41081479
		 0.59375 0.41081479 0.5625 0.37431598 0.5625 0.41081479 0.53125 0.37431598 0.53125
		 0.41081479 0.5 0.37431598 0.5 0.41081479 0.46875 0.37431598 0.46875 0.41081479 0.4375
		 0.37431598 0.4375 0.41081479 0.40625 0.35625231 0.375 0.35625231 0.625 0.35625231
		 0.59375 0.35625231 0.5625 0.35625231 0.53125 0.35625231 0.5 0.35625231 0.46875 0.35625231
		 0.4375 0.35625231 0.5 0.36240453 0.53125 0.36240453 0.53125 0.36659157 0.5 0.36659157
		 0.46875 0.36240453 0.46875 0.36659157 0.4375 0.36240453 0.4375 0.36659157 0.40625
		 0.36240453 0.40625 0.36659157 0.375 0.36240453 0.375 0.36659157 0.59375 0.36240453
		 0.625 0.36240453 0.625 0.36659157 0.59375 0.36659157 0.5625 0.36240453 0.5625 0.36659157
		 0.5 1.4901161e-08 0.61048543 0.04576458 0.61048543 0.04576458 0.5 1.4901161e-08 0.38951457
		 0.04576458 0.38951457 0.04576458 0.34375 0.15625 0.34375 0.15625 0.38951457 0.26673543
		 0.38951457 0.26673543 0.5 0.3125 0.5 0.3125 0.61048543 0.26673543 0.61048543 0.26673543
		 0.65625 0.15625 0.65625 0.15625 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625
		 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.61048543
		 0.04576458 0.5 0.15000001 0.375 0.20833334 0.625 0.20833334 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.54166669 0.375 0.54166669 0.875 0.20833334 0.875 0.25
		 0.125 0.20833334 0.125 0.25 0.375 0.70833337 0.625 0.70833337 0.625 0.75 0.375 0.75
		 0.125 0 0.375 0 0.375 0.041666672 0.125 0.041666672 0.625 0 0.625 0.041666672 0.875
		 0 0.875 0.041666672 0.375 0.66666675 0.625 0.66666675 0.375 0.083333336 0.125 0.083333336
		 0.625 0.083333336 0.875 0.083333336 0.375 0.62500006 0.625 0.62500006 0.375 0.125
		 0.125 0.125 0.625 0.125 0.875 0.125 0.375 0.58333337 0.625 0.58333337 0.375 0.16666667
		 0.125 0.16666667 0.625 0.16666667 0.875 0.16666667 0.375 0.20833334 0.625 0.20833334
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.54166669 0.375 0.54166669 0.875
		 0.20833334 0.875 0.25 0.125 0.20833334 0.125 0.25 0.375 0.70833337 0.625 0.70833337
		 0.625 0.75 0.375 0.75 0.125 0 0.375 0 0.375 0.041666672 0.125 0.041666672 0.625 0
		 0.625 0.041666672 0.875 0 0.875 0.041666672 0.375 0.66666675 0.625 0.66666675 0.375
		 0.083333336 0.125 0.083333336 0.625 0.083333336 0.875 0.083333336 0.375 0.62500006
		 0.625 0.62500006 0.375 0.125 0.125 0.125 0.625 0.125 0.875 0.125 0.375 0.58333337
		 0.625 0.58333337 0.375 0.16666667 0.125 0.16666667 0.625 0.16666667 0.875 0.16666667
		 0.375 0.20833334 0.625 0.20833334 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625
		 0.54166669 0.375 0.54166669 0.875 0.20833334 0.875 0.25 0.125 0.20833334 0.125 0.25
		 0.375 0.70833337 0.625 0.70833337 0.625 0.75 0.375 0.75 0.125 0 0.375 0 0.375 0.041666672
		 0.125 0.041666672 0.625 0 0.625 0.041666672 0.875 0 0.875 0.041666672 0.375 0.66666675
		 0.625 0.66666675 0.375 0.083333336 0.125 0.083333336 0.625 0.083333336 0.875 0.083333336
		 0.375 0.62500006 0.625 0.62500006 0.375 0.125 0.125 0.125 0.625 0.125 0.875 0.125
		 0.375 0.58333337 0.625 0.58333337 0.375 0.16666667 0.125 0.16666667 0.625 0.16666667
		 0.875 0.16666667 0.875 0.25 0.79166663 0.25 0.79166675 0.25 0.87500006 0.25 0.875
		 0 0.87500006 0 0.79166663 0 0.79166675 0 0.625 0.25 0.625 0 0.625 0 0.625 0.25 0.14583333
		 0.875 0.96527779 0.875 0.97791159 0.92048186 0.092771165 0.92048186 0.14583333 0.83333337
		 0.97916663 0.875 0.98674691 0.92048186 0.092771165 0.89397585 0.13194443 0.83333337
		 0.97916663 0.83333337 0.98674691 0.89397585 0.083935812 0.89397585 0.10416667 0.875
		 0.9375 0.83333337 0.96024096 0.89397585 0.066265129 0.92048186 0.875 0.25 0.79166669
		 0.25 0.79166669 0.25 0.875 0.25 0.875 0 0.875 0 0.79166669 0 0.875 0 0.875 0 0.79166669
		 0 0.62500006 0.25 0.62500006 0 0.62500006 0 0.62500006 0.25 0.875 0.25 0.79166663
		 0.25;
	setAttr ".uvst[0].uvsp[750:999]" 0.79166663 0.25 0.875 0.25 0.875 0 0.875 0
		 0.79166663 0 0.79166663 0 0.625 0.25 0.625 0 0.625 0 0.625 0.25 0.79166675 0 0.87500006
		 0 0.875 0 0.79166663 0 0.79166669 0.25 0.875 0.25 0.875 0 0.79166669 0 0.79166669
		 0 0.62500006 0 0.62500006 0.25 0.79166663 0.25 0.875 0.25 0.875 0 0.79166663 0 0.79166663
		 0 0.875 0 0.875 0 0.625 0 0.625 0.25 0.79166663 0 0.79166669 0 0.875 0 0.79166669
		 0 0.875 0 0.79166669 0 0.62500006 0 0.62500006 0 0.87500006 0 0.79166675 0 0.875
		 0 0.79166663 0 0.625 0 0.625 0 0.875 0 0.79166663 0 0.875 0 0.79166663 0 0.625 0
		 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.625 0 0.70833337 0
		 0.70833331 0 0.625 0 0.70833337 0 0.70833337 0 0.70833331 0 0.70833337 0.25 0.70833331
		 0.25 0.70833331 0.25 0.70833331 0.25 0.70833331 0.25 0.70833337 0.25 0.70833337 0.25
		 0.70833337 0.25 0.11805555 0.875 0.9375 0.875 0.96024096 0.92048186 0.075100467 0.92048186
		 0.10416667 0.83333337 0.9513889 0.83333337 0.96907628 0.89397585 0.066265129 0.89397585
		 0.70833337 0 0.70833337 0 0.70833337 0 0.62500006 0 0.70833337 0 0.70833337 0 0.62500006
		 0 0.70833337 0 0.70833337 0 0.70833331 0 0.70833331 0 0.625 0 0.70833331 0 0.70833331
		 0 0.625 0 0.70833337 0 0.79166675 0 0.79166663 0 0.70833331 0 0.79166663 0.25 0.79166663
		 0.25 0.70833331 0.25 0.70833331 0.25 0.13194443 0.875 0.9513889 0.875 0.96907628
		 0.92048186 0.083935812 0.92048186 0.70833337 0 0.79166669 0 0.79166669 0 0.70833337
		 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.875 0 0.79166663 0 0.87500006
		 0 0.79166675 0 0.70833337 0 0.625 0 0.625 0 0.70833331 0 0.79166663 0 0.70833331
		 0 0.875 0 0.875 0 0.79166663 0 0.70833331 0 0.625 0 0.625 0 0.70833337 0 0.62500006
		 0 0.79166669 0 0.875 0 0.875 0 0.79166669 0 0.70833337 0 0.62500006 0 0.625 0 0.625
		 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.875 0 0.79166663 0 0.79166663 0 0.875
		 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.625 0 0.625 0 0.875 0 0.79166663 0 0.79166663 0 0.875 0 0.875 0
		 0.875 0 0.79166675 0 0.79166675 0 0.625 0 0.70833337 0 0.70833337 0 0.625 0 0.62500006
		 0 0.62500006 0 0.62500006 0 0.62500006 0 0.70833337 0 0.70833337 0 0.875 0 0.79166675
		 0 0.79166675 0 0.875 0 0.875 0 0.875 0 0.79166675 0 0.79166675 0 0.70833337 0 0.70833337
		 0 0.79166663 0 0.79166663 0 0.70833325 0 0.79166663 0 0.70833325 0 0.70833325 0 0.79166663
		 0 0.70833325 0 0.79166663 0 0.79166663 0 0.70833337 0 0.79166663 0 0.70833337 0 0.70833337
		 0 0.79166663 0 0.70833337 0 4.7772944e-16 0 0.23069231 0 0.23069231 1 0 1 0 1.0098774e-15
		 0.42010182 0 0.51182556 0.95874602 0.10181157 1 0.079096138 0 0.44136256 6.9667812e-15
		 0.35356736 1 0 0.88404495 0.79166675 0 0.70833337 0 0.20189096 0.74497986 0.24189815
		 0.69444448 0.31029916 0.83333337 0.13194443 0.83333337 0.91087961 0.69444448 0.92561907
		 0.74497986 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.79166663 0 0.70833331
		 0 0.70833331 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833331
		 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.79166663 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.70833331 0 0.79166663 0;
	setAttr ".uvst[0].uvsp[1000:1249]" 0.79166663 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.79166663 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833331 0 0.79166663 0 0.79166663 0 0.70833331
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.79166663 0 0.70833337 0 0.70833337
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833337 0 0.79166663
		 0 0.79166663 0 0.70833337 0 0.70833337 0 0.70833337 0 0.79166663 0 0.70833337 0 0.79166663
		 0 0.79166663 0 0.70833337 0 0.79166663 0 0.70833337 0 0.70833337 0 0.79166663 0 0.70833337
		 0 0.79166663 0 0.79166663 0 0.70833337 0 0.79166663 0 0.65972221 0.16666669 0.62872654
		 0.16666669 0.68171293 0.13888891 0.67371148 0.148996 0.70833337 0 0.79166669 0 0.75179052
		 0.148996 0.7488426 0.13888891 0.52777779 0.33333334 0.54911971 0.33333334 0.57175922
		 0.27777779 0.55575633 0.29799196 0.70833337 0 0.79166675 0 0.79524767 0.29799196
		 0.78935188 0.27777779 0.39583334 0.5 0.46951285 0.5 0.46180558 0.41666669 0.43780124
		 0.44698793 0.70833337 0 0.79166675 0 0.83870482 0.44698793 0.82986116 0.41666669
		 0.26388887 0.66666669 0.38990599 0.66666669 0.35185182 0.55555558 0.31984606 0.59598392
		 0.70833337 0 0.79166675 0 0.88216197 0.59598392 0.87037039 0.55555558 0 0.82552218
		 1 0.82552218 1 0.88559628 0 0.88559628 0 0.82552218 1 0.82552218 1 0.88559628 0 0.88559628
		 0 0.82552218 1 0.82552218 1 0.88559628 0 0.88559628 0 0.82552218 1 0.82552218 1 0.88559628
		 0 0.88559628 0 0.82552218 1 0.82552218 1 0.88559628 0 0.88559628 0 0.82552218 1 0.82552218
		 1 0.88559628 0 0.88559628 0 0.82552218 1 0.82552218 1 0.88559628 0 0.88559628 0 0.99062657
		 1 0.99062657 1 1 0 1 0 0.99062657 1 0.99062657 1 1 0 1 0 0.99062657 1 0.99062657
		 1 1 0 1 0 0.99062657 1 0.99062657 1 1 0 1 0 0.99062657 1 0.99062657 1 1 0 1 0 0.99062657
		 1 0.99062657 1 1 0 1 0 0.99062657 1 0.99062657 1 1 0 1 0 0.99062657 1 0.99062657
		 1 1 0 1 0.019675924 0.86099625 0.019675924 0.80259097 0.021990741 0.96310914 0.99189818
		 0.96310914 0.99421299 0.80259097 0.99421299 0.86099625 0.70833331 0 0.79166663 0
		 0.59027779 0.13758704 0.59027779 0.14759938 0.79166675 0 0.70833337 0 0.82638896
		 0.14759938 0.82638896 0.13758704 0.75694448 0.16510445 0.65972227 0.16510445 0.47222224
		 0.27517405 0.47222227 0.29519874 0.79166669 0 0.70833337 0 0.86111116 0.29519874
		 0.86111116 0.27517405 0.80555558 0.33020887 0.52777785 0.33020887 0.35416669 0.41276109
		 0.35416669 0.44279814 0.79166663 0 0.70833331 0 0.89583337 0.44279814 0.89583337
		 0.41276109 0.85416669 0.49531329 0.39583337 0.49531329 0.2361111 0.5503481 0.2361111
		 0.59039748 0.79166663 0 0.70833331 0 0.93055558 0.59039748 0.93055558 0.5503481 0.90277779
		 0.66041774 0.2638889 0.66041774 0.11805555 0.68793517 0.11805555 0.73799688 0.79166663
		 0 0.70833331 0 0.96527779 0.73799688 0.96527779 0.68793517 0.9513889 0.82552218 0.13194445
		 0.82552218 0.098379627 0.71086633 0.098379627 0.76259673 0.79166663 0 0.70833331
		 0 0.97106481 0.76259673 0.97106481 0.71086633 0.95949078 0.85303956 0.1099537 0.85303956
		 0.078703701 0.73379743 0.078703701 0.78719658 0.79166663 0 0.70833331 0 0.97685182
		 0.78719658 0.97685182 0.73379743 0.9675926 0.88055694 0.087962963 0.88055694 0.059027776
		 0.75672865 0.059027776 0.81179649 0.79166663 0 0.70833331 0 0.98263884 0.81179649
		 0.98263884 0.75672865 0.97569442 0.90807432 0.065972224 0.90807432 0.039351847 0.77965987
		 0.039351847 0.83639646 0.79166663 0 0.70833331 0 0.98842597 0.83639646 0.98842597
		 0.77965987 0.9837963 0.93559176 0.043981481 0.93559176 0.019675924 0 0.019675924
		 0.16051821 0.039351847 0.15593198 0.039351847 0 0 0 0 0.16510445 1 0 1 0.16510445
		 0 0 1 0 1 0.16510445 0 0.16510445 0 0 1 0 1 0.16510445 0 0.16510445 0 0 1 0 1 0.16510445
		 0 0.16510445 0 0 1 0 1 0.16510445 0 0.16510445 0 0 1 0 1 0.16510445 0 0.16510445;
	setAttr ".uvst[0].uvsp[1250:1499]" 0 0 1 0 1 0.16510445 0 0.16510445 0.99421299
		 0.16051821 0.99421299 0 0.98842597 0.15593198 0.98842591 0 0.98263884 0.15134574
		 0.98263884 0 0.97685182 0.14675951 0.97685182 0 0.97106481 0.14217328 0.97106481
		 0 0.96527779 0.13758704 0.96527779 0 0.93055558 0.11006963 0.93055558 0 0.89583337
		 0.082552224 0.89583337 0 0.86111116 0.055034816 0.86111116 0 0.82638896 0.027517412
		 0.82638896 0 0.79166675 0 0.87500006 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875
		 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166669 0 0.875 0 0.875 0 0.79166669 0 0.76967591 0.027777784 0.74768525 0.055555563
		 0.72569448 0.083333343 0.70370376 0.11111113 0.68171299 0.13888891 0.65972221 0.16666669
		 0.89583331 0.16666669 0.89583331 0.375 0.72916663 0.16666669 0.8263889 0.375 0.72916663
		 0.375 0.75694448 0.375 0.65972221 0.375 0.68750006 0.375 0.59027779 0.375 0.68750006
		 0.16666669 0.52083337 0.375 0.75694448 0.16666669 0.52083337 0.16666669 0.7488426
		 0.13888891 0.74074078 0.11111113 0.73263896 0.083333343 0.72453707 0.055555563 0.71643519
		 0.027777784 0.70833337 0 0.62500006 0 0.62500006 0 0.70833337 0 0.70833337 0 0.70833337
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833331 0 0.625 0 0.625 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833325
		 0 0.70833331 0 0.625 0 0.625 0 0.70833337 0 0.59027779 0 0.59027779 0.027517412 0.47222224
		 0 0.47222227 0.055034816 0.35416669 0 0.35416669 0.082552224 0.2361111 0 0.2361111
		 0.11006963 0.11805555 0 0.11805555 0.13758704 0.098379627 0 0.098379627 0.14217328
		 0.078703701 0 0.078703701 0.14675951 0.059027776 0 0.059027776 0.15134574 0.019675924
		 0.3210364 0.039351847 0.31186393 0 0.33020887 1 0.33020887 1 0.33020887 0 0.33020887
		 1 0.33020887 0 0.33020887 1 0.33020887 0 0.33020887 1 0.33020887 0 0.33020887 1 0.33020887
		 0 0.33020887 1 0.33020887 0 0.33020887 0.99421299 0.3210364 0.98842597 0.31186393
		 0.98263884 0.30269146 0.97685182 0.29351899 0.97106481 0.28434652 0.96527779 0.27517408
		 0.93055558 0.22013924 0.89583337 0.16510443 0.86111116 0.11006962 0.82638896 0.055034816
		 0.79166675 0 0.87500006 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875 0 0.875
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166675
		 0 0.875 0 0.875 0 0.79166675 0 0.74768513 0.05555556 0.70370376 0.11111112 0.65972227
		 0.16666667 0.61574078 0.22222224 0.57175928 0.27777779 0.52777779 0.33333334 0.91666663
		 0.33333334 0.91666663 0.5 0.58333331 0.33333334 0.8611111 0.5 0.58333331 0.5 0.80555558
		 0.5 0.52777779 0.5 0.75000006 0.5 0.47222224 0.5 0.75000006 0.33333334 0.41666672
		 0.5 0.80555558 0.33333334 0.41666672 0.33333334 0.78935182 0.27777779 0.77314818
		 0.22222224 0.75694454 0.16666667 0.74074078 0.11111112 0.72453707 0.05555556 0.70833337
		 0 0.62500006 0 0.62500006 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337
		 0 0.70833337 0 0.70833331 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833325 0 0.70833331 0 0.625
		 0 0.625 0 0.70833337 0 0.59027779 0.055034816 0.47222227 0.11006962 0.35416669 0.16510443
		 0.2361111 0.22013924 0.11805555 0.27517408 0.098379627 0.28434652 0.078703701 0.29351899
		 0.059027776 0.30269146 0.019675924 0.48155457 0.039351847 0.46779591 0 0.49531329
		 1 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329
		 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329 0.99421299
		 0.48155457 0.98842597 0.46779591 0.98263884 0.45403719 0.97685182 0.44027847 0.97106481
		 0.42651978 0.96527779 0.41276109 0.93055558 0.33020884 0.89583337 0.24765664 0.86111116
		 0.16510442 0.82638896 0.082552224 0.79166675 0 0.87500006 0 0.875 0 0.79166663 0
		 0.79166663 0 0.79166663 0;
	setAttr ".uvst[0].uvsp[1500:1749]" 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166675 0 0.875 0 0.875 0 0.79166675
		 0 0.72569442 0.083333343 0.65972221 0.16666669 0.59375 0.25 0.52777779 0.33333337
		 0.46180558 0.41666669 0.39583334 0.5 0.9375 0.5 0.9375 0.625 0.4375 0.5 0.89583331
		 0.625 0.4375 0.625 0.85416669 0.625 0.39583334 0.625 0.81250006 0.625 0.35416669
		 0.625 0.81250006 0.5 0.31250003 0.625 0.85416669 0.5 0.31250003 0.5 0.82986104 0.41666669
		 0.80555558 0.33333337 0.78125006 0.25 0.75694448 0.16666669 0.7326389 0.083333343
		 0.70833337 0 0.62500006 0 0.62500006 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337
		 0 0.70833337 0 0.70833337 0 0.70833331 0 0.625 0 0.625 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833325 0 0.70833331
		 0 0.625 0 0.625 0 0.70833337 0 0.59027779 0.082552224 0.47222224 0.16510442 0.35416669
		 0.24765664 0.2361111 0.33020884 0.11805555 0.41276109 0.098379627 0.42651978 0.078703701
		 0.44027847 0.059027776 0.45403719 0.019675924 0.6420728 0.039351847 0.62372792 0
		 0.66041774 1 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774
		 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774 0 0.66041774
		 0.99421299 0.6420728 0.98842597 0.62372792 0.98263884 0.60538292 0.97685182 0.58703798
		 0.97106481 0.56869304 0.96527779 0.55034816 0.93055558 0.44027847 0.89583337 0.33020887
		 0.86111116 0.22013924 0.82638896 0.11006963 0.79166675 0 0.875 0 0.875 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166675 0 0.875 0 0.875 0 0.79166675 0 0.70370364
		 0.11111113 0.61574072 0.22222224 0.52777779 0.33333334 0.43981481 0.44444448 0.35185185
		 0.55555558 0.26388887 0.66666669 0.95833331 0.66666669 0.95833331 0.75 0.29166666
		 0.66666669 0.93055552 0.75 0.29166666 0.75 0.90277779 0.75 0.26388887 0.75 0.87500006
		 0.75 0.2361111 0.75 0.87500006 0.66666669 0.20833334 0.75 0.90277779 0.66666669 0.20833334
		 0.66666669 0.87037033 0.55555558 0.83796299 0.44444448 0.80555558 0.33333334 0.77314818
		 0.22222224 0.74074078 0.11111113 0.70833337 0 0.62500006 0 0.62500006 0 0.70833337
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833331 0 0.625
		 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833325 0 0.70833331 0 0.625 0 0.625 0 0.70833337 0 0.59027779
		 0.11006963 0.47222224 0.22013924 0.35416669 0.33020887 0.2361111 0.44027847 0.11805555
		 0.55034816 0.098379627 0.56869304 0.078703701 0.58703798 0.059027776 0.60538292 0.0020004236
		 0.99788189 0.99894094 0.99788189 1 1 0 1 0.0022357674 0.99788189 0.99917632 0.99788189
		 1 1 0 1 0.0024711117 0.99788189 0.99941164 0.99788189 1 1 0 1 0.0024711117 0.99717587
		 0.99964696 0.99788189 1 1 0 1 0.0022357674 0.99717587 0.99964696 0.99717587 1 1 0
		 1 0.13380757 0.83097994 0.13194443 0.83333337 0.26537937 0.66478395 0.26388887 0.66666669
		 0.39695123 0.49858794 0.39583334 0.5 0.52852303 0.33239198 0.52777779 0.33333334
		 0.6600948 0.166196 0.65972221 0.16666669 0.79166669 0 0.875 0 0.875 0 0.79166669
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875
		 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875 0 0.87500006 0 0.79166675 0 0.82638896
		 0.16510445 0.82638896 0.16463816 0.86111116 0.33020887 0.86111116 0.32927629 0.89583337
		 0.49531329 0.89583337 0.49391446 0.93055558 0.66041774 0.93055558 0.65855259 0.96527779
		 0.82552218 0.96527779 0.82319081 0.97106481 0.85303956 0.97106481 0.85063046 0.97685182
		 0.88055694 0.97685182 0.87807012 0.98263884 0.90807432 0.98263884 0.90550977;
	setAttr ".uvst[0].uvsp[1750:1999]" 0.98842591 0.93559176 0.98842591 0.93294948
		 0.99421299 0.96310914 0.99421299 0.9603892 1 0.98782891 0 0.98782891 0 0.98782891
		 1 0.98782891 0 0.98782891 1 0.98782891 0 0.98782891 1 0.98782891 0 0.98782891 1 0.98782891
		 0 0.98782891 1 0.98782891 0 0.98782891 1 0.98782891 0.019675924 0.9603892 0.019675924
		 0.96310914 0.039351847 0.93294948 0.039351847 0.93559176 0.059027776 0.90550977 0.059027776
		 0.90807432 0.078703701 0.87807012 0.078703701 0.88055694 0.098379627 0.85063046 0.098379627
		 0.85303956 0.11805555 0.82319081 0.11805555 0.82552218 0.2361111 0.65855259 0.2361111
		 0.66041774 0.35416669 0.49391446 0.35416669 0.49531329 0.47222224 0.32927629 0.47222224
		 0.33020887 0.59027779 0.16463816 0.59027779 0.16510445 0.70833337 0 0.625 0 0.625
		 0 0.70833331 0 0.70833325 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.625 0 0.625 0 0.70833331 0 0.70833337 0 0.70833337
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.62500006 0 0.62500006 0 0.70833337
		 0 0.75694448 0.16666669 0.75680715 0.166196 0.80555558 0.33333334 0.80528104 0.33239198
		 0.85416669 0.5 0.85375482 0.49858794 0.90277779 0.66666669 0.90222865 0.66478395
		 0.9513889 0.83333337 0.95070243 0.83097994 1 1 0.99917632 0.99717587 0.0017650799
		 0.99717587 0 1 0.0017650799 0.99788189 0.99894094 0.99717587 1 1 0 1 0.79166663 0.25
		 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331
		 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25
		 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663
		 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25
		 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663
		 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166675 0 0.70833337 0 0.79166663
		 0 0.70833331 0 0.79166669 0 0.70833337 0 0.79166669 0 0.70833337 0 0.70833331 0 0.70833331
		 0 0.79166663 0 0.79166663 0 0.79166675 0 0.70833337 0 0.79166663 0 0.70833331 0 0.79166669
		 0 0.70833337 0 0.79166669 0 0.70833337 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663
		 0 0.79166675 0 0.70833337 0 0.79166663 0 0.70833331 0 0.79166669 0 0.70833337 0 0.79166669
		 0 0.70833337 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.875 0.25 0.87500006
		 0.25 0.79166675 0.25 0.79166663 0.25 0.875 0 0.87500006 0 0.79166663 0 0.79166675
		 0 0.625 0.25 0.625 0.25 0.625 0 0.625 0 0.14583333 0.875 0.092771165 0.92048186 0.97791159
		 0.92048186 0.96527779 0.875 0.14583333 0.83333337 0.092771165 0.89397585 0.98674691
		 0.92048186 0.97916663 0.875 0.13194443 0.83333337 0.083935812 0.89397585 0.98674691
		 0.89397585 0.97916663 0.83333337 0.10416667 0.875 0.066265129 0.92048186 0.96024096
		 0.89397585 0.9375 0.83333337 0.875 0.25 0.875 0.25 0.79166669 0.25 0.79166669 0.25
		 0.875 0 0.875 0 0.79166669 0 0.79166669 0 0.875 0 0.875 0 0.62500006 0.25 0.62500006
		 0.25 0.62500006 0 0.62500006 0 0.875 0.25 0.875 0.25 0.79166663 0.25 0.79166663 0.25
		 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.625 0.25 0.625 0.25 0.625 0 0.625 0 0.79166675
		 0 0.79166663 0 0.875 0 0.87500006 0 0.875 0.25 0.79166669 0.25 0.875 0 0.79166669
		 0 0.79166669 0 0.62500006 0.25 0.62500006 0 0.875 0.25 0.79166663 0.25 0.875 0 0.79166663
		 0 0.875 0 0.875 0 0.79166663 0 0.625 0.25 0.625 0 0.79166663 0 0.79166669 0 0.79166669
		 0 0.875 0 0.875 0 0.79166669 0 0.62500006 0 0.62500006 0 0.79166675 0 0.87500006
		 0 0.875 0 0.79166663 0 0.625 0 0.625 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0
		 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.625 0 0.625
		 0 0.70833331 0 0.70833337 0;
	setAttr ".uvst[0].uvsp[2000:2249]" 0.70833337 0 0.70833337 0 0.70833331 0 0.70833337
		 0.25 0.70833331 0.25 0.70833331 0.25 0.70833331 0.25 0.70833331 0.25 0.70833337 0.25
		 0.70833337 0.25 0.70833337 0.25 0.11805555 0.875 0.075100467 0.92048186 0.96024096
		 0.92048186 0.9375 0.875 0.10416667 0.83333337 0.066265129 0.89397585 0.96907628 0.89397585
		 0.9513889 0.83333337 0.70833337 0 0.70833337 0 0.70833337 0 0.62500006 0 0.62500006
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833331 0 0.70833331 0 0.625
		 0 0.625 0 0.70833331 0 0.70833331 0 0.70833337 0 0.70833331 0 0.79166663 0 0.79166675
		 0 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.13194443 0.875
		 0.083935812 0.92048186 0.96907628 0.92048186 0.9513889 0.875 0.70833337 0 0.70833337
		 0 0.79166669 0 0.79166669 0 0.70833331 0 0.79166663 0 0.79166663 0 0.70833331 0 0.875
		 0 0.79166663 0 0.87500006 0 0.79166675 0 0.70833337 0 0.625 0 0.625 0 0.70833331
		 0 0.70833331 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.70833331 0 0.625 0 0.625
		 0 0.62500006 0 0.70833337 0 0.79166669 0 0.875 0 0.875 0 0.79166669 0 0.70833337
		 0 0.62500006 0 0.625 0 0.625 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.875 0
		 0.875 0 0.79166663 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.625 0 0.625 0 0.70833331 0 0.875 0 0.875 0 0.79166663
		 0 0.79166663 0 0.875 0 0.875 0 0.79166675 0 0.79166675 0 0.625 0 0.625 0 0.70833337
		 0 0.70833337 0 0.62500006 0 0.62500006 0 0.62500006 0 0.62500006 0 0.70833337 0 0.70833337
		 0 0.875 0 0.875 0 0.79166675 0 0.79166675 0 0.875 0 0.875 0 0.79166675 0 0.79166675
		 0 0.70833337 0 0.70833337 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833325 0 0.70833325
		 0 0.70833325 0 0.70833325 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833337
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.79166663 0 4.7772944e-16 0 0 1 0.23069231
		 1 0.23069231 0 0 1.0098774e-15 0.10181157 1 0.51182556 0.95874602 0.42010182 0 0.079096138
		 0 0 0.88404495 0.35356736 1 0.44136256 6.9667812e-15 0.70833337 0 0.79166675 0 0.24189815
		 0.69444448 0.20189096 0.74497986 0.13194443 0.83333337 0.31029916 0.83333337 0.92561907
		 0.74497986 0.91087961 0.69444448 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.70833331
		 0 0.70833331 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.70833331 0 0.70833331
		 0 0.79166663 0 0.79166663 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.79166663
		 0 0.79166663 0 0.70833337 0 0.70833337 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.70833337 0 0.70833337 0 0.79166663 0 0.79166663 0 0.70833337 0 0.70833337 0 0.79166663
		 0 0.70833337 0 0.79166663 0 0.79166663 0 0.70833337 0 0.79166663 0 0.70833337 0 0.70833337
		 0 0.79166663 0 0.70833337 0 0.79166663 0 0.79166663 0 0.70833337 0 0.79166663 0 0.65972221
		 0.16666669 0.62872654 0.16666669 0.68171293 0.13888891 0.67371148 0.148996 0.70833337
		 0 0.79166669 0 0.75179052 0.148996 0.7488426 0.13888891;
	setAttr ".uvst[0].uvsp[2250:2499]" 0.52777779 0.33333334 0.54911971 0.33333334
		 0.57175922 0.27777779 0.55575633 0.29799196 0.70833337 0 0.79166675 0 0.79524767
		 0.29799196 0.78935188 0.27777779 0.39583334 0.5 0.46951285 0.5 0.46180558 0.41666669
		 0.43780124 0.44698793 0.70833337 0 0.79166675 0 0.83870482 0.44698793 0.82986116
		 0.41666669 0.26388887 0.66666669 0.38990599 0.66666669 0.35185182 0.55555558 0.31984606
		 0.59598392 0.70833337 0 0.79166675 0 0.88216197 0.59598392 0.87037039 0.55555558
		 0 0.82552218 0 0.88559628 1 0.88559628 1 0.82552218 0 0.82552218 0 0.88559628 1 0.88559628
		 1 0.82552218 0 0.82552218 0 0.88559628 1 0.88559628 1 0.82552218 0 0.82552218 0 0.88559628
		 1 0.88559628 1 0.82552218 0 0.82552218 0 0.88559628 1 0.88559628 1 0.82552218 0 0.82552218
		 0 0.88559628 1 0.88559628 1 0.82552218 0 0.82552218 0 0.88559628 1 0.88559628 1 0.82552218
		 0 0.99062657 0 1 1 1 1 0.99062657 0 0.99062657 0 1 1 1 1 0.99062657 0 0.99062657
		 0 1 1 1 1 0.99062657 0 0.99062657 0 1 1 1 1 0.99062657 0 0.99062657 0 1 1 1 1 0.99062657
		 0 0.99062657 0 1 1 1 1 0.99062657 0 0.99062657 0 1 1 1 1 0.99062657 0 0.99062657
		 0 1 1 1 1 0.99062657 0.019675924 0.80259097 0.019675924 0.86099625 0.99189818 0.96310914
		 0.021990741 0.96310914 0.99421299 0.86099625 0.99421299 0.80259097 0.79166663 0 0.70833331
		 0 0.59027779 0.13758704 0.59027779 0.14759938 0.79166675 0 0.70833337 0 0.82638896
		 0.14759938 0.82638896 0.13758704 0.75694448 0.16510445 0.65972227 0.16510445 0.47222224
		 0.27517405 0.47222227 0.29519874 0.79166669 0 0.70833337 0 0.86111116 0.29519874
		 0.86111116 0.27517405 0.80555558 0.33020887 0.52777785 0.33020887 0.35416669 0.41276109
		 0.35416669 0.44279814 0.79166663 0 0.70833331 0 0.89583337 0.44279814 0.89583337
		 0.41276109 0.85416669 0.49531329 0.39583337 0.49531329 0.2361111 0.5503481 0.2361111
		 0.59039748 0.79166663 0 0.70833331 0 0.93055558 0.59039748 0.93055558 0.5503481 0.90277779
		 0.66041774 0.2638889 0.66041774 0.11805555 0.68793517 0.11805555 0.73799688 0.79166663
		 0 0.70833331 0 0.96527779 0.73799688 0.96527779 0.68793517 0.9513889 0.82552218 0.13194445
		 0.82552218 0.098379627 0.71086633 0.098379627 0.76259673 0.79166663 0 0.70833331
		 0 0.97106481 0.76259673 0.97106481 0.71086633 0.95949078 0.85303956 0.1099537 0.85303956
		 0.078703701 0.73379743 0.078703701 0.78719658 0.79166663 0 0.70833331 0 0.97685182
		 0.78719658 0.97685182 0.73379743 0.9675926 0.88055694 0.087962963 0.88055694 0.059027776
		 0.75672865 0.059027776 0.81179649 0.79166663 0 0.70833331 0 0.98263884 0.81179649
		 0.98263884 0.75672865 0.97569442 0.90807432 0.065972224 0.90807432 0.039351847 0.83639646
		 0.039351847 0.77965987 0.79166663 0 0.70833331 0 0.98842597 0.83639646 0.98842597
		 0.77965987 0.9837963 0.93559176 0.043981481 0.93559176 0.019675924 0 0.039351847
		 0 0.039351847 0.15593198 0.019675924 0.16051821 0 0 0 0.16510445 1 0.16510445 1 0
		 0 0 0 0.16510445 1 0.16510445 1 0 0 0 0 0.16510445 1 0.16510445 1 0 0 0 0 0.16510445
		 1 0.16510445 1 0 0 0 0 0.16510445 1 0.16510445 1 0 0 0 0 0.16510445 1 0.16510445
		 1 0 0 0 0 0.16510445 1 0.16510445 1 0 0.99421299 0.16051821 0.99421299 0 0.98842597
		 0.15593198 0.98842591 0 0.98263884 0.15134574 0.98263884 0 0.97685182 0.14675951
		 0.97685182 0 0.97106481 0.14217328 0.97106481 0 0.96527779 0.13758704 0.96527779
		 0 0.93055558 0.11006963 0.93055558 0 0.89583337 0.082552224 0.89583337 0 0.86111116
		 0.055034816 0.86111116 0 0.82638896 0.027517412 0.82638896 0 0.79166675 0 0.87500006
		 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166669 0 0.875 0 0.875
		 0 0.79166669 0 0.76967591 0.027777784 0.74768525 0.055555563 0.72569448 0.083333343
		 0.70370376 0.11111113 0.68171299 0.13888891 0.65972221 0.16666669 0.89583331 0.16666669
		 0.72916663 0.16666669 0.89583331 0.375 0.72916663 0.375;
	setAttr ".uvst[0].uvsp[2500:2749]" 0.8263889 0.375 0.65972221 0.375 0.75694448
		 0.375 0.59027779 0.375 0.68750006 0.375 0.52083337 0.375 0.68750006 0.16666669 0.52083337
		 0.16666669 0.75694448 0.16666669 0.7488426 0.13888891 0.74074078 0.11111113 0.73263896
		 0.083333343 0.72453707 0.055555563 0.71643519 0.027777784 0.70833337 0 0.62500006
		 0 0.62500006 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337
		 0 0.70833331 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833325 0 0.70833331 0 0.625 0 0.625
		 0 0.70833337 0 0.59027779 0 0.59027779 0.027517412 0.47222224 0 0.47222227 0.055034816
		 0.35416669 0 0.35416669 0.082552224 0.2361111 0 0.2361111 0.11006963 0.11805555 0
		 0.11805555 0.13758704 0.098379627 0 0.098379627 0.14217328 0.078703701 0 0.078703701
		 0.14675951 0.059027776 0 0.059027776 0.15134574 0.039351847 0.31186393 0.019675924
		 0.3210364 0 0.33020887 1 0.33020887 0 0.33020887 1 0.33020887 0 0.33020887 1 0.33020887
		 0 0.33020887 1 0.33020887 0 0.33020887 1 0.33020887 0 0.33020887 1 0.33020887 0 0.33020887
		 1 0.33020887 0.99421299 0.3210364 0.98842597 0.31186393 0.98263884 0.30269146 0.97685182
		 0.29351899 0.97106481 0.28434652 0.96527779 0.27517408 0.93055558 0.22013924 0.89583337
		 0.16510443 0.86111116 0.11006962 0.82638896 0.055034816 0.79166675 0 0.87500006 0
		 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166675 0 0.875 0 0.875
		 0 0.79166675 0 0.74768513 0.05555556 0.70370376 0.11111112 0.65972227 0.16666667
		 0.61574078 0.22222224 0.57175928 0.27777779 0.52777779 0.33333334 0.91666663 0.33333334
		 0.58333331 0.33333334 0.91666663 0.5 0.58333331 0.5 0.8611111 0.5 0.52777779 0.5
		 0.80555558 0.5 0.47222224 0.5 0.75000006 0.5 0.41666672 0.5 0.75000006 0.33333334
		 0.41666672 0.33333334 0.80555558 0.33333334 0.78935182 0.27777779 0.77314818 0.22222224
		 0.75694454 0.16666667 0.74074078 0.11111112 0.72453707 0.05555556 0.70833337 0 0.62500006
		 0 0.62500006 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337
		 0 0.70833331 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833325 0 0.70833331 0 0.625 0 0.625
		 0 0.70833337 0 0.59027779 0.055034816 0.47222227 0.11006962 0.35416669 0.16510443
		 0.2361111 0.22013924 0.11805555 0.27517408 0.098379627 0.28434652 0.078703701 0.29351899
		 0.059027776 0.30269146 0.039351847 0.46779591 0.019675924 0.48155457 0 0.49531329
		 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329
		 0 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329 0 0.49531329 1 0.49531329 0.99421299
		 0.48155457 0.98842597 0.46779591 0.98263884 0.45403719 0.97685182 0.44027847 0.97106481
		 0.42651978 0.96527779 0.41276109 0.93055558 0.33020884 0.89583337 0.24765664 0.86111116
		 0.16510442 0.82638896 0.082552224 0.79166675 0 0.87500006 0 0.875 0 0.79166663 0
		 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166675 0 0.875 0 0.875 0 0.79166675 0 0.72569442
		 0.083333343 0.65972221 0.16666669 0.59375 0.25 0.52777779 0.33333337 0.46180558 0.41666669
		 0.39583334 0.5 0.9375 0.5 0.4375 0.5 0.9375 0.625 0.4375 0.625 0.89583331 0.625 0.39583334
		 0.625 0.85416669 0.625 0.35416669 0.625 0.81250006 0.625 0.31250003 0.625 0.81250006
		 0.5 0.31250003 0.5 0.85416669 0.5 0.82986104 0.41666669 0.80555558 0.33333337 0.78125006
		 0.25 0.75694448 0.16666669 0.7326389 0.083333343 0.70833337 0 0.62500006 0 0.62500006
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833331
		 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0;
	setAttr ".uvst[0].uvsp[2750:2999]" 0.70833331 0 0.70833331 0 0.70833331 0 0.70833325
		 0 0.70833331 0 0.625 0 0.625 0 0.70833337 0 0.59027779 0.082552224 0.47222224 0.16510442
		 0.35416669 0.24765664 0.2361111 0.33020884 0.11805555 0.41276109 0.098379627 0.42651978
		 0.078703701 0.44027847 0.059027776 0.45403719 0.039351847 0.62372792 0.019675924
		 0.6420728 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774
		 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774 0 0.66041774 1 0.66041774 0 0.66041774
		 1 0.66041774 0.99421299 0.6420728 0.98842597 0.62372792 0.98263884 0.60538292 0.97685182
		 0.58703798 0.97106481 0.56869304 0.96527779 0.55034816 0.93055558 0.44027847 0.89583337
		 0.33020887 0.86111116 0.22013924 0.82638896 0.11006963 0.79166675 0 0.875 0 0.875
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166675 0 0.875 0 0.875 0 0.79166675
		 0 0.70370364 0.11111113 0.61574072 0.22222224 0.52777779 0.33333334 0.43981481 0.44444448
		 0.35185185 0.55555558 0.26388887 0.66666669 0.95833331 0.66666669 0.29166666 0.66666669
		 0.95833331 0.75 0.29166666 0.75 0.93055552 0.75 0.26388887 0.75 0.90277779 0.75 0.2361111
		 0.75 0.87500006 0.75 0.20833334 0.75 0.87500006 0.66666669 0.20833334 0.66666669
		 0.90277779 0.66666669 0.87037033 0.55555558 0.83796299 0.44444448 0.80555558 0.33333334
		 0.77314818 0.22222224 0.74074078 0.11111113 0.70833337 0 0.62500006 0 0.62500006
		 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833331
		 0 0.625 0 0.625 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833325 0 0.70833331 0 0.625 0 0.625 0 0.70833337
		 0 0.59027779 0.11006963 0.47222224 0.22013924 0.35416669 0.33020887 0.2361111 0.44027847
		 0.11805555 0.55034816 0.098379627 0.56869304 0.078703701 0.58703798 0.059027776 0.60538292
		 0.0020004236 0.99788189 0 1 1 1 0.99894094 0.99788189 0.0022357674 0.99788189 0 1
		 1 1 0.99917632 0.99788189 0.0024711117 0.99788189 0 1 1 1 0.99941164 0.99788189 0.0024711117
		 0.99717587 0 1 1 1 0.99964696 0.99788189 0.0022357674 0.99717587 0 1 1 1 0.99964696
		 0.99717587 0.13380757 0.83097994 0.13194443 0.83333337 0.26537937 0.66478395 0.26388887
		 0.66666669 0.39695123 0.49858794 0.39583334 0.5 0.52852303 0.33239198 0.52777779
		 0.33333334 0.6600948 0.166196 0.65972221 0.16666669 0.79166669 0 0.875 0 0.875 0
		 0.79166669 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.875 0 0.875 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663
		 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.875 0 0.87500006 0 0.79166675
		 0 0.82638896 0.16510445 0.82638896 0.16463816 0.86111116 0.33020887 0.86111116 0.32927629
		 0.89583337 0.49531329 0.89583337 0.49391446 0.93055558 0.66041774 0.93055558 0.65855259
		 0.96527779 0.82552218 0.96527779 0.82319081 0.97106481 0.85303956 0.97106481 0.85063046
		 0.97685182 0.88055694 0.97685182 0.87807012 0.98263884 0.90807432 0.98263884 0.90550977
		 0.98842591 0.93559176 0.98842591 0.93294948 0.99421299 0.96310914 0.99421299 0.9603892
		 1 0.98782891 0 0.98782891 0 0.98782891 1 0.98782891 0 0.98782891 1 0.98782891 0 0.98782891
		 1 0.98782891 0 0.98782891 1 0.98782891 0 0.98782891 1 0.98782891 0 0.98782891 1 0.98782891
		 0.019675924 0.9603892 0.019675924 0.96310914 0.039351847 0.93294948 0.039351847 0.93559176
		 0.059027776 0.90550977 0.059027776 0.90807432 0.078703701 0.87807012 0.078703701
		 0.88055694 0.098379627 0.85063046 0.098379627 0.85303956 0.11805555 0.82319081 0.11805555
		 0.82552218 0.2361111 0.65855259 0.2361111 0.66041774 0.35416669 0.49391446 0.35416669
		 0.49531329 0.47222224 0.32927629 0.47222224 0.33020887 0.59027779 0.16463816 0.59027779
		 0.16510445 0.70833337 0 0.625 0 0.625 0 0.70833331 0 0.70833325 0 0.70833331 0 0.70833331
		 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.70833331 0 0.625 0 0.625
		 0 0.70833331 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0 0.70833337 0;
	setAttr ".uvst[0].uvsp[3000:3249]" 0.70833337 0 0.62500006 0 0.62500006 0 0.70833337
		 0 0.75694448 0.16666669 0.75680715 0.166196 0.80555558 0.33333334 0.80528104 0.33239198
		 0.85416669 0.5 0.85375482 0.49858794 0.90277779 0.66666669 0.90222865 0.66478395
		 0.9513889 0.83333337 0.95070243 0.83097994 1 1 0.99917632 0.99717587 0.0017650799
		 0.99717587 0 1 0.0017650799 0.99788189 0 1 1 1 0.99894094 0.99717587 0.79166663 0.25
		 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331
		 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25
		 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663
		 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25
		 0.70833331 0.25 0.79166663 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.79166663
		 0.25 0.79166663 0.25 0.70833331 0.25 0.70833331 0.25 0.70833337 0 0.79166675 0 0.79166663
		 0 0.70833331 0 0.70833337 0 0.79166669 0 0.79166669 0 0.70833337 0 0.70833331 0 0.70833331
		 0 0.79166663 0 0.79166663 0 0.70833337 0 0.79166675 0 0.79166663 0 0.70833331 0 0.70833337
		 0 0.79166669 0 0.79166669 0 0.70833337 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663
		 0 0.70833337 0 0.79166675 0 0.79166663 0 0.70833331 0 0.70833337 0 0.79166669 0 0.79166669
		 0 0.70833337 0 0.70833331 0 0.70833331 0 0.79166663 0 0.79166663 0 0.375 0.3125 0.40000001
		 0.3125 0.40000001 0.37515664 0.375 0.37515664 0.42500001 0.3125 0.42500001 0.37515664
		 0.45000002 0.3125 0.45000002 0.37515664 0.47500002 0.3125 0.47500002 0.37515664 0.5
		 0.3125 0.5 0.37515664 0.52499998 0.3125 0.52499998 0.37515664 0.54166663 0.3125 0.54999995
		 0.3125 0.54999995 0.37515664 0.54166663 0.37515664 0.57499993 0.3125 0.57499993 0.37515664
		 0.5999999 0.3125 0.5999999 0.37515664 0.62499988 0.3125 0.62499988 0.37515664 0.375
		 0.43781328 0.40000001 0.43781328 0.40000001 0.50046992 0.375 0.50046992 0.42500001
		 0.43781328 0.42500001 0.50046992 0.45000002 0.43781328 0.45000002 0.50046992 0.47500002
		 0.43781328 0.47500002 0.50046992 0.5 0.43781328 0.5 0.50046992 0.52499998 0.43781328
		 0.52499998 0.50046992 0.54166663 0.43781328 0.54999995 0.43781328 0.54999995 0.50046992
		 0.54166663 0.50046992 0.57499993 0.43781328 0.57499993 0.50046992 0.5999999 0.43781328
		 0.5999999 0.50046992 0.62499988 0.43781328 0.62499988 0.50046992 0.40000001 0.56312656
		 0.42500001 0.56312656 0.42500001 0.57705027 0.40000001 0.57705027 0.45000002 0.56312656
		 0.47500002 0.56312656 0.47500002 0.57705027 0.44999999 0.57705027 0.5 0.56312656
		 0.5 0.57705027 0.52499998 0.56312656 0.52499998 0.57705027 0.54166663 0.56312656
		 0.54999995 0.56312656 0.54999995 0.57705027 0.54166663 0.57705027 0.57499993 0.56312656
		 0.57499993 0.57705027 0.5999999 0.56312656 0.5999999 0.57705027 0.62499988 0.56312656
		 0.62499988 0.57705027 0.54828387 0.0076473951 0.62640893 0.064408526 0.5 0.15000001
		 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625001 0.37359107 0.24809146
		 0.4517161 0.3048526 0.54828393 0.3048526 0.51609468 0.3048526 0.48390538 0.3048526
		 0.62640893 0.24809144 0.65625 0.15625 0.62640893 0.93559146 0.54828393 0.9923526
		 0.54828393 0.9923526 0.62640893 0.93559146 0.4517161 0.9923526 0.4517161 0.9923526
		 0.37359107 0.93559146 0.37359107 0.93559146 0.34375 0.84375 0.34375 0.84375 0.37359107
		 0.75190854 0.37359107 0.75190854 0.45171607 0.6951474 0.45171607 0.6951474 0.51609462
		 0.6951474 0.54828387 0.6951474 0.54828387 0.6951474 0.51609462 0.6951474 0.62640893
		 0.75190854 0.62640893 0.75190854 0.65625 0.84375 0.65625 0.84375 0.54828393 0.9923526
		 0.62640893 0.93559146 0.4517161 0.9923526 0.37359107 0.93559146 0.34375 0.84375 0.37359107
		 0.75190854 0.45171607 0.6951474 0.54828387 0.6951474 0.51609462 0.6951474 0.62640893
		 0.75190854 0.65625 0.84375 0.54828393 0.9923526 0.62640893 0.93559146 0.4517161 0.9923526
		 0.37359107 0.93559146 0.34375 0.84375 0.37359107 0.75190854 0.45171607 0.6951474
		 0.54828387 0.6951474 0.51609462 0.6951474 0.62640893 0.75190854 0.65625 0.84375 0.62640893
		 0.93559146 0.54828393 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.4517161
		 0.9923526 0.45171607 0.9923526 0.37359107 0.93559146 0.37359107 0.93559146 0.34375
		 0.84375 0.34375 0.84375 0.37359107 0.75190854 0.37359107 0.75190854 0.45171607 0.6951474
		 0.45171607 0.6951474 0.51609462 0.6951474 0.54828387 0.6951474 0.54828387 0.6951474
		 0.51609462 0.6951474 0.62640893 0.75190854 0.62640893 0.75190854 0.65625 0.84375
		 0.65625 0.84375 0.62640893 0.93559146 0.54828393 0.9923526 0.54828393 0.9923526 0.62640893
		 0.93559146 0.65625 0.84375 0.65625 0.84375 0.62640893 0.75190854 0.62640893 0.75190854
		 0.54828387 0.6951474 0.54828387 0.6951474 0.51609462 0.6951474;
	setAttr ".uvst[0].uvsp[3250:3499]" 0.51609462 0.6951474 0.37359107 0.75190854
		 0.45171607 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.34375
		 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.4517161 0.9923526 0.4517161
		 0.9923526 0.62640893 0.93559146 0.54828393 0.9923526 0.54828393 0.9923526 0.62640893
		 0.93559146 0.65625 0.84375 0.65625 0.84375 0.62640893 0.75190854 0.62640893 0.75190854
		 0.54828387 0.6951474 0.54828393 0.6951474 0.51609462 0.6951474 0.51609468 0.6951474
		 0.37359107 0.75190854 0.45171607 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854
		 0.34375 0.84375 0.34375 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.45171607
		 0.9923526 0.4517161 0.9923526 0.65625 0.84375 0.62640893 0.93559146 0.62640893 0.93559146
		 0.65625 0.84375 0.62640893 0.75190854 0.62640893 0.75190854 0.54828387 0.6951474
		 0.54828387 0.6951474 0.51609462 0.6951474 0.51609462 0.6951474 0.37359107 0.75190854
		 0.45171607 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.34375
		 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.45171607 0.9923526 0.45171607
		 0.9923526 0.54828393 0.9923526 0.54828393 0.9923526 0.62640893 0.75190854 0.65625
		 0.84375 0.54828387 0.6951474 0.51609462 0.6951474 0.37359107 0.75190854 0.45171607
		 0.6951474 0.34375 0.84375 0.37359107 0.93559146 0.45171607 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.51609462 0.6951474 0.54828387 0.6951474 0.37359107 0.75190854
		 0.45171607 0.6951474 0.34375 0.84375 0.37359107 0.93559146 0.45171607 0.9923526 0.54828393
		 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.62640893 0.75190854 0.5999999 0.64666879
		 0.62499988 0.64666879 0.62499988 0.68843985 0.5999999 0.68843985 0.57499993 0.64666879
		 0.57499993 0.68843985 0.54999995 0.64666879 0.54999995 0.68843985 0.54166663 0.64666879
		 0.54166663 0.68843985 0.5 0.64666879 0.52499998 0.64666879 0.52499998 0.68843985
		 0.5 0.68843985 0.47500002 0.64666879 0.47500002 0.68843985 0.44999999 0.64666879
		 0.45000002 0.68843985 0.42500001 0.64666879 0.42500001 0.68843985 0.40000001 0.64666879
		 0.40000001 0.68843985 0.375 0.64666879 0.375 0.68843985 0.5999999 0.60489768 0.62499988
		 0.60489768 0.57499993 0.60489768 0.54999995 0.60489768 0.54166663 0.60489768 0.5
		 0.60489768 0.52499998 0.60489768 0.47500002 0.60489768 0.44999999 0.60489768 0.42500001
		 0.60489768 0.40000001 0.60489768 0.375 0.60489768 0.375 0.56312656 0.54828387 0.6951474
		 0.51609462 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107
		 0.93559146 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625
		 0.84375 0.62640893 0.75190854 0.54828387 0.0076473951 0.62640893 0.064408526 0.62640893
		 0.064408526 0.54828387 0.0076473951 0.45171607 0.00764741 0.45171607 0.00764741 0.37359107
		 0.064408556 0.37359107 0.064408556 0.34375 0.15625001 0.34375 0.15625001 0.37359107
		 0.24809146 0.37359107 0.24809146 0.4517161 0.3048526 0.4517161 0.3048526 0.54828393
		 0.3048526 0.51609468 0.3048526 0.51609468 0.3048526 0.54828393 0.3048526 0.62640893
		 0.24809144 0.62640893 0.24809144 0.65625 0.15625 0.65625 0.15625 0.45171607 0.00764741
		 0.54828387 0.0076473951 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107
		 0.064408556 0.37359107 0.064408556 0.34375 0.15625001 0.34375 0.15625001 0.37359107
		 0.24809146 0.37359107 0.24809146 0.4517161 0.3048526 0.4517161 0.3048526 0.54828393
		 0.3048526 0.51609468 0.3048526 0.51609468 0.3048526 0.54828393 0.3048526 0.62640893
		 0.24809144 0.62640893 0.24809144 0.65625 0.15625 0.65625 0.15625 0.62640893 0.064408526
		 0.62640893 0.064408526 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556
		 0.34375 0.15625001 0.37359107 0.24809146 0.4517161 0.3048526 0.51609468 0.3048526
		 0.54828393 0.3048526 0.62640893 0.24809144 0.65625 0.15625 0.62640893 0.064408526
		 0.62640893 0.064408526 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556
		 0.34375 0.15625001 0.37359107 0.24809146 0.4517161 0.3048526 0.51609468 0.3048526
		 0.54828393 0.3048526 0.62640893 0.24809144 0.65625 0.15625 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359104 0.064408556 0.34375 0.15625001
		 0.37359104 0.24809146 0.4517161 0.3048526 0.51609468 0.3048526 0.54828393 0.3048526
		 0.62640893 0.24809144 0.65625 0.15625 0.54828387 0.0076473951 0.45171604 0.00764741
		 0.37359104 0.064408556 0.34375 0.15625001 0.37359104 0.24809146 0.45171607 0.3048526
		 0.51609468 0.3048526 0.54828393 0.3048526 0.62640893 0.24809144 0.65625 0.15625 0.62640893
		 0.064408526 0.65625 0.15625 0.62640899 0.064408526 0.54828387 0.0076473951 0.45171607
		 0.0076474105 0.37359104 0.064408556 0.34375003 0.15625003 0.37359104 0.24809147 0.4517161
		 0.3048526 0.51609468 0.3048526 0.54828393 0.3048526 0.62640899 0.24809146 0.62640899
		 0.064408526 0.54828387 0.0076473951 0.45171607 0.0076474105 0.37359104 0.064408556
		 0.34375003 0.15625003 0.37359104 0.24809147 0.4517161 0.3048526 0.51609468 0.3048526
		 0.54828393 0.3048526 0.62640899 0.24809146 0.65625 0.15625 0.62640899 0.064408526
		 0.54828387 0.0076473951 0.45171604 0.0076474105 0.37359104 0.064408556 0.34375 0.15625003
		 0.37359104 0.24809147 0.45171607 0.3048526 0.51609468 0.3048526 0.54828393 0.3048526
		 0.62640899 0.24809146 0.65625 0.15625 0.65625 0.15625 0.62640899 0.064408526 0.54828387
		 0.0076473951 0.45171604 0.00764741;
	setAttr ".uvst[0].uvsp[3500:3749]" 0.37359104 0.064408556 0.34375 0.15625001
		 0.37359104 0.24809147 0.45171607 0.30485258 0.51609468 0.30485258 0.54828393 0.30485258
		 0.62640899 0.24809146 0.65625 0.15625 0.62640893 0.064408526 0.54828387 0.0076473951
		 0.45171604 0.00764741 0.37359104 0.064408556 0.34375 0.15625001 0.37359104 0.24809146
		 0.45171607 0.30485258 0.51609468 0.30485258 0.54828393 0.30485258 0.62640893 0.24809144
		 0.65625 0.15625 0.62640893 0.064408526 0.54828387 0.0076473951 0.45171604 0.00764741
		 0.37359107 0.064408556 0.34375 0.15625001 0.37359107 0.24809146 0.45171607 0.30485258
		 0.51609468 0.30485258 0.54828393 0.30485258 0.62640893 0.24809144 0.65624994 0.15625
		 0.62640893 0.064408526 0.54828382 0.0076473947 0.45171601 0.00764741 0.37359107 0.064408556
		 0.34375 0.15625001 0.37359107 0.24809146 0.45171607 0.30485258 0.51609468 0.30485258
		 0.54828393 0.30485258 0.62640893 0.24809143 0.65625 0.15625 0.62640899 0.064408526
		 0.54828382 0.0076473951 0.45171604 0.00764741 0.3735911 0.064408556 0.34375003 0.15625003
		 0.3735911 0.24809146 0.45171607 0.30485258 0.51609468 0.30485258 0.54828393 0.30485258
		 0.62640899 0.24809144 0.65625 0.15625 0.62640893 0.064408526 0.54828382 0.0076473951
		 0.45171604 0.00764741 0.37359107 0.064408556 0.34375 0.15625001 0.37359107 0.24809146
		 0.45171604 0.30485258 0.51609462 0.30485258 0.54828393 0.30485258 0.62640893 0.24809143
		 0.65625 0.15625 0.62640893 0.064408526 0.54828382 0.0076473951 0.45171607 0.00764741
		 0.37359107 0.064408556 0.34375 0.15625001 0.37359107 0.24809146 0.45171607 0.30485258
		 0.51609468 0.30485258 0.54828393 0.30485258 0.62640893 0.24809143 0.54828393 0.3048526
		 0.62640893 0.24809143 0.65625 0.15625 0.62640893 0.064408526 0.54828382 0.0076473951
		 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625001 0.37359107 0.24809146
		 0.45171607 0.3048526 0.51609468 0.3048526 0.40000001 0.56312656 0.375 0.56312656
		 0.40000001 0.57705027 0.375 0.60489768 0.40000001 0.60489768 0.375 0.57705027 0.375
		 0.57705027 0.45000002 0.56312656 0.42500001 0.56312656 0.44999999 0.57705027 0.42500001
		 0.60489768 0.44999999 0.60489768 0.42500001 0.57705027 0.62640893 0.93559146 0.54828393
		 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.4517161 0.9923526 0.45171613
		 0.9923526 0.37359107 0.93559146 0.3735911 0.93559146 0.34375 0.84375 0.34375 0.84375
		 0.37359107 0.75190854 0.3735911 0.7519086 0.45171607 0.6951474 0.4517161 0.6951474
		 0.51609462 0.6951474 0.54828387 0.6951474 0.54828387 0.6951474 0.51609462 0.6951474
		 0.62640893 0.75190854 0.62640893 0.7519086 0.65625 0.84375 0.65625 0.84375 0 0 1
		 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 0.33333331 0 0.66666663 0 1
		 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0.54828393
		 0.99235255 0.62640893 0.93559146 0.65625 0.84375 0.62640893 0.7519086 0.54828382
		 0.69514734 0.51609457 0.69514734 0.45171607 0.69514734 0.3735911 0.7519086 0.34375
		 0.84375 0.3735911 0.93559146 0.45171613 0.99235255 0.54828382 0.69514734 0.51609457
		 0.69514734 0.45171607 0.69514734 0.37359107 0.7519086 0.34375 0.84375 0.37359107
		 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625
		 0.84375 0.62640893 0.7519086 0.54828382 0.69514734 0.62640893 0.7519086 0.62640893
		 0.75190854 0.54828382 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.48390532
		 0.69514734 0.45171604 0.69514734 0.37359107 0.7519086 0.37359107 0.75190854 0.34375
		 0.84375 0.34375 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.45171613 0.99235255
		 0.45171613 0.99235255 0.54828393 0.99235255 0.54828393 0.99235255 0.62640893 0.93559146
		 0.62640893 0.93559146 0.65625 0.84375 0.65625 0.84375 0.62640893 0.93559146 0.54828393
		 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.65625 0.84375
		 0.62640893 0.75190854 0.62640893 0.75190854 0.54828387 0.69514734 0.54828387 0.6951474
		 0.45171607 0.69514734 0.48390535 0.69514734 0.48390538 0.6951474 0.45171607 0.6951474
		 0.37359107 0.75190854 0.45171607 0.69514734 0.45171607 0.6951474 0.37359107 0.75190854
		 0.34375 0.84375 0.34375 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.45171613
		 0.9923526 0.45171613 0.9923526 0.48390532 0.69514734 0.45171607 0.69514734 0.37359107
		 0.75190854 0.34375 0.84375 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393
		 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.62640893 0.75190854 0.54828382
		 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.48390532 0.69514734 0.45171607
		 0.69514734 0.37359107 0.75190854 0.37359107 0.75190854 0.34375 0.84375 0.34375 0.84375
		 0.37359107 0.93559146 0.34375 0.84375 0.34375 0.84375 0.37359107 0.93559146 0.45171613
		 0.99235255 0.37359107 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393
		 0.99235255 0.54828393 0.9923526 0.62640893 0.93559146 0.62640893 0.93559146;
	setAttr ".uvst[0].uvsp[3750:3999]" 0.65625 0.84375 0.62640893 0.93559146 0.62640893
		 0.93559146 0.65625 0.84375 0.62640893 0.75190854 0.65625 0.84375 0.65625 0.84375
		 0.62640893 0.75190854 0.54828382 0.69514734 0.54828382 0.69514734 0.45171607 0.69514734
		 0.48390532 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.37359107 0.75190854
		 0.45171607 0.69514734 0.45171607 0.69514734 0.37359107 0.75190854 0.34375 0.84375
		 0.34375 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.54828393 0.9923526
		 0.45171613 0.9923526 0.45171613 0.9923526 0.54828393 0.9923526 0.65625 0.84375 0.62640893
		 0.93559146 0.62640893 0.93559146 0.65625 0.84375 0.62640893 0.75190854 0.62640893
		 0.75190854 0.54828382 0.69514734 0.54828382 0.69514734 0.34375 0.84375 0.37359107
		 0.93559146 0.34375 0.84375 0.37359107 0.93559146 0.34375 0.84375 0.37359107 0.93559146
		 0.62640893 0.93559146 0.65625 0.84375 0.62640893 0.93559146 0.65625 0.84375 0.62640893
		 0.93559146 0.65625 0.84375 0.37359107 0.93559146 0.34375 0.84375 0.65625 0.84375
		 0.62640893 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.37359107 0.93559146
		 0.45171613 0.99235255 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.54828393 0.9923526 0.62640893 0.93559146 0.54828393 0.99235255
		 0.62640893 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.37359107 0.93559146
		 0.45171613 0.99235255 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.54828393 0.9923526 0.62640893 0.93559146 0.54828393 0.99235255
		 0.62640893 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.37359107 0.93559146
		 0.45171613 0.99235255 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.54828393 0.9923526 0.62640893 0.93559146 0.54828393 0.99235255
		 0.62640893 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.37359107 0.93559146
		 0.45171613 0.99235255 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.54828393 0.9923526 0.62640893 0.93559146 0.54828393 0.99235255
		 0.62640893 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.37359107 0.93559146
		 0.45171613 0.99235255 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526
		 0.62640893 0.93559146 0.54828393 0.9923526 0.62640893 0.93559146 0.54828393 0.99235255
		 0.62640893 0.93559146 0 0 1 0 1 1 0 1 0 0 0.33333331 0 0.33333331 1 0 1 0 0 1 0 1
		 1 0 1 0.66666669 0 1 0 1 1 0.66666669 1 0 0 1 0 1 1 0 1 0 0 0.33333331 0 0.33333331
		 1 0 1 0 0 1 0 1 1 0 1 0.66666669 0 1 0 1 1 0.66666669 1 0 0 1 0 1 1 0 1 0 0 0.33333331
		 0 0.33333331 1 0 1 0 0 1 0 1 1 0 1 0.66666669 0 1 0 1 1 0.66666669 1 0 0 1 0 1 1
		 0 1 0 0 0.33333331 0 0.33333331 1 0 1 0 0 1 0 1 1 0 1 0.66666669 0 1 0 1 1 0.66666669
		 1 0.74952781 0 1 0 0.85224783 0.73200488 0.56816524 0.69619536 0 2.2943457e-15 0.25046894
		 1.5295638e-15 0.43183872 0.69617629 0.14775813 0.73199022 0.74952781 0 1 0 1 0 0.74952781
		 0 0.85224783 0.73200488 0.85224783 0.73200488 0.56816524 0.69619536 0.56816524 0.69619536
		 0 0.62457633 0.24858341 0 0.24858341 0 0 0.62457633 0 2.2943457e-15 0.25046894 1.5295638e-15
		 0.25046894 1.5295638e-15 0 2.2943457e-15 0.75140685 0 1 0.62454844 1 0.62454844 0.75140685
		 0 0.43183872 0.69617629 0.14775813 0.73199022 0.14775813 0.73199022 0.43183872 0.69617629
		 0.74952781 0 1 0 1 0 0.74952781 0 0.85224783 0.73200488 0.85224783 0.73200488 0.56816524
		 0.69619536 0.56816524 0.69619536 0 0.62457633 0.24858341 0 0.24858341 0 0 0.62457633
		 0 2.2943457e-15 0.25046894 1.5295638e-15 0.25046894 1.5295638e-15 0 2.2943457e-15
		 0.75140685 0 1 0.62454844 1 0.62454844 0.75140685 0 0.43183872 0.69617629 0.14775813
		 0.73199022 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783
		 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15
		 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629
		 1 0 0.74952781 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633
		 0.25046894 1.5295638e-15 0 2.2943457e-15;
	setAttr ".uvst[0].uvsp[4000:4249]" 1 0.62454844 0.75140685 0 0.14775813 0.73199022
		 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488 0.56816524 0.69619536
		 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685
		 0 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 0.74952781
		 0 1 0 1 0 0.74952781 0 0.85224783 0.73200488 0.85224783 0.73200488 0.56816524 0.69619536
		 0.56816524 0.69619536 0 0.62457633 0.24858341 0 0.24858341 0 0 0.62457633 0 2.2943457e-15
		 0.25046894 1.5295638e-15 0.25046894 1.5295638e-15 0 2.2943457e-15 0.75140685 0 1
		 0.62454844 1 0.62454844 0.75140685 0 0.43183872 0.69617629 0.14775813 0.73199022
		 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894
		 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872
		 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341
		 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0
		 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894
		 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872
		 0.69617629 0.74952781 0 1 0 1 0 0.74952781 0 0.85224783 0.73200488 0.85224783 0.73200488
		 0.56816524 0.69619536 0.56816524 0.69619536 0 0.62457633 0.24858341 0 0.24858341
		 0 0 0.62457633 0 2.2943457e-15 0.25046894 1.5295638e-15 0.25046894 1.5295638e-15
		 0 2.2943457e-15 0.75140685 0 1 0.62454844 1 0.62454844 0.75140685 0 0.43183872 0.69617629
		 0.14775813 0.73199022 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894
		 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872
		 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341
		 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0
		 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894
		 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872
		 0.69617629 0.74952781 0 1 0 1 0 0.74952781 0 0.85224783 0.73200488 0.85224783 0.73200488
		 0.56816524 0.69619536 0.56816524 0.69619536 0 0.62457633 0.24858341 0 0.24858341
		 0 0 0.62457633 0 2.2943457e-15 0.25046894 1.5295638e-15 0.25046894 1.5295638e-15
		 0 2.2943457e-15 0.75140685 0 1 0.62454844 1 0.62454844 0.75140685 0 0.43183872 0.69617629
		 0.14775813 0.73199022 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894
		 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872
		 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341
		 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0
		 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633;
	setAttr ".uvst[0].uvsp[4250:4499]" 0.25046894 1.5295638e-15 0 2.2943457e-15 1
		 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 0.74952781 0
		 1 0 1 0 0.74952781 0 0.85224783 0.73200488 0.85224783 0.73200488 0.56816524 0.69619536
		 0.56816524 0.69619536 0 0.62457633 0.24858341 0 0.24858341 0 0 0.62457633 0 2.2943457e-15
		 0.25046894 1.5295638e-15 0.25046894 1.5295638e-15 0 2.2943457e-15 0.75140685 0 1
		 0.62454844 1 0.62454844 0.75140685 0 0.43183872 0.69617629 0.14775813 0.73199022
		 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781
		 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894
		 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872
		 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488 0.56816524 0.69619536 0.24858341
		 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15 1 0.62454844 0.75140685 0
		 0.14775813 0.73199022 0.43183872 0.69617629 1 0 0.74952781 0 0.85224783 0.73200488
		 0.56816524 0.69619536 0.24858341 0 0 0.62457633 0.25046894 1.5295638e-15 0 2.2943457e-15
		 1 0.62454844 0.75140685 0 0.14775813 0.73199022 0.43183872 0.69617629 0.74952781
		 0 1 0 0.85224783 0.73200488 0.56816524 0.69619536 0 0.62457633 0.24858341 0 0.24858341
		 0 0 0.62457633 0 2.2943457e-15 0.25046894 1.5295638e-15 0.75140685 0 1 0.62454844
		 1 0.62454844 0.75140685 0 0.43183872 0.69617629 0.14775813 0.73199022 0.43183872
		 0.69617629 0.14775813 0.73199022 0.75140685 0 1 0.62454844 0 2.2943457e-15 0.25046894
		 1.5295638e-15 0.43183872 0.69617629 0.14775813 0.73199022 0.75140685 0 1 0.62454844
		 0 2.2943457e-15 0.25046894 1.5295638e-15 0.43183872 0.69617629 0.14775813 0.73199022
		 0.75140685 0 1 0.62454844 0 2.2943457e-15 0.25046894 1.5295638e-15 0.43183872 0.69617629
		 0.14775813 0.73199022 0.75140685 0 1 0.62454844 0 2.2943457e-15 0.25046894 1.5295638e-15
		 0.43183872 0.69617629 0.14775813 0.73199022 0.75140685 0 1 0.62454844 0 2.2943457e-15
		 0.25046894 1.5295638e-15 0.43183872 0.69617629 0.14775813 0.73199022 0.75140685 0
		 1 0.62454844 0 2.2943457e-15 0.25046894 1.5295638e-15 0.85224783 0.73200488 0.56816524
		 0.69619536 1 0 0.74952781 0 0 0.62457633 0.24858341 0 0.85224783 0.73200488 0.56816524
		 0.69619536 1 0 0.74952781 0 0 0.62457633 0.24858341 0 0.85224783 0.73200488 0.56816524
		 0.69619536 1 0 0.74952781 0 0 0.62457633 0.24858341 0 0.85224783 0.73200488 0.56816524
		 0.69619536 1 0 0.74952781 0 0 0.62457633 0.24858341 0 0.85224783 0.73200488 0.56816524
		 0.69619536 1 0 0.74952781 0 0 0.62457633 0.24858341 0 0.85224783 0.73200488 0.56816524
		 0.69619536 1 0 0.74952781 0 0 0.62457633 0.24858341 0 0.375 0.59097397 0.375 0.59097397
		 0.5999999 0.59097397 0.62499988 0.59097397 0.57499993 0.59097397 0.54999995 0.59097397
		 0.54166663 0.59097397 0.5 0.59097397 0.52499998 0.59097397 0.47500002 0.59097397
		 0.44999999 0.59097397 0.44999999 0.59097397 0.42500001 0.59097397 0.42500001 0.59097397
		 0.40000001 0.59097397 0.40000001 0.59097397 0 0 0.39194593 0 0.90600288 0.70817196
		 0.43373793 0.99999994 0 8.8003409e-16 1.000000119209 0 0.85320807 0.50752485 0.40260041
		 0.66062456 0 0 0.67657334 0 0.99999994 0.61071837 0.72367978 0.84484237 0.059228122
		 4.5331986e-16 0.99999994 0 0.89490002 0.4108341 0 0.46866167 0 0 0.90071374 0 1 0.28902972
		 0.10999473 0.070386283 0 0 0.8072775 1.4279472e-15 0.69903022 1 0.48536679 0.8655861
		 0.10103046 0 0.36568543 1.3002728e-15 0.19625373 0.99999994 0 0.86522245 0.1477194
		 1.1314224e-15 0.7873587 0 0.17208602 1 0 0.88615257 0.51150894 1.9160861e-15 0.90150625
		 0 0.46991783 1 0 0.70997822 0 0 1 0 0.59784734 0.66549021 0.14725533 0.51213223 0.32362539
		 0 1 0 0.276256 0.8476792 0 0.6137678 0 0 0.94012082 0 1 0.46932715 0.10570972 0.41139144
		 0.09989991 0 1 0 0.88939583 0.071236901 0 0.28979337 0 0 0.80401307 1.422173e-15
		 0.32056618 0.86621422 0.10776642 1 0 0 0.26374498 1.295802e-15 0.36442962 0.8657257
		 0.16885033 0.99999994 0.28497267 0 0.46194857 0 0.44495627 1 0 0.71034408 0.65625
		 0.84375 0.62640893 0.93559146 0.62640893 0.7519086 0.54828382 0.69514734;
	setAttr ".uvst[0].uvsp[4500:4749]" 0.37359107 0.7519086 0.45171607 0.69514734
		 0.34375 0.84375 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526 0.51609457
		 0.69514734 0 3.0431369e-16 0.96138448 0 1 0.12282004 0.0089067938 0.26942292 0.038615238
		 0 1.000000119209 0 0.99109375 0.26942089 0 0.1228181 0.38419378 1.0180856e-06 0.61580288
		 0 0.99999994 0.069919109 0 0.06992092 0.21399629 0.11481335 0.78600699 0.11481333
		 1 1.1088493e-06 0 0 0 2.8649044e-16 0.98420781 0 1 0.24246556 0.024861535 0.36063778
		 0.015792448 0 1 2.8649037e-16 0.97513831 0.36063579 0 0.24246323 0.40619716 4.4127464e-07
		 0.59380054 0 0.99999994 0.080787301 0 0.08078827 0.29261503 0.13908932 0.70738721
		 0.1390893 0.99999994 2.32715e-06 0 0 0 1.8142926e-16 0.88958156 0 1 0.43134835 0.041240696
		 0.50911903 0.1104202 0 1 1.8142889e-16 0.95875794 0.50911558 0 0.43134475 0.4360145
		 3.9178869e-07 0.56398338 0 0.99999994 0.06694001 0 0.06694071 0.3735427 0.13270225
		 0.62645954 0.13270184 1 0 0 9.2543905e-07 0.66666663 0 1 0 1 1 0.66666663 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0.66666669 0 1 0 1 1 0.66666669 1 0.66666663 0 1 0 1 1
		 0.66666663 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.66666669 0 1 0 1 1 0.66666669 1 0.66666663
		 0 1 0 1 0.33333331 0.66666663 0.33333331 0 0 1 0 1 0.33333331 0 0.33333331 0 0 1
		 0 1 0.33333331 0 0.33333331 0.66666669 0 1 0 1 0.33333331 0.66666669 0.33333331 0.66666663
		 0 1 0 1 1 0.66666663 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.66666669 0 1 0 1 1 0.66666669
		 1 0.66666663 0 1 0 1 0.054881282 0.66666663 0.054881282 0 0 1 0 1 0.054881282 0 0.054881282
		 0 0 1 0 1 0.054881282 0 0.054881282 0.66666669 0 1 0 1 0.054881282 0.66666669 0.054881282
		 0.33333331 0 0.33333331 0.054881282 0.33333331 0 0.33333331 1 0.33333331 0 0.33333331
		 0.33333331 0.33333331 0 0.33333331 1 0.33333331 0 0.33333331 1 0.51609457 0.69514734
		 0.51609457 0.69514734 0.51609457 0.69514734 0.48390532 0.69514734 0.48390532 0.69514734
		 0.51609457 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734
		 0.48390538 0.6951474 0.48390535 0.69514734 0.51609462 0.69514734 0.51609462 0.6951474
		 0 0 0 0.054881282 0 0 0 1 0 0 0 0.33333331 0 0 0 1 0 0 0 1 0.51609457 0.69514734
		 0.51609457 0.69514734 0.54828382 0.69514734 0.54828382 0.69514734 0.51609462 0.6951474
		 0.51609462 0.69514734 0.54828387 0.69514734 0.54828387 0.6951474 0.51609462 0.6951474
		 0.51609462 0.6951474 0.54828387 0.6951474 0.54828387 0.6951474 0.48390535 0.6951474
		 0.48390538 0.6951474 0.51609462 0.6951474 0.51609462 0.6951474 0.45171607 0.6951474
		 0.48390538 0.6951474 0.48390535 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854
		 0.45171607 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.34375
		 0.84375 0.37359107 0.93559146 0.37359107 0.93559146 0.4517161 0.9923526 0.4517161
		 0.9923526 0.54828393 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.62640893
		 0.93559146 0.65625 0.84375 0.65625 0.84375 0.62640893 0.75190854 0.62640893 0.75190854
		 0.54828387 0.6951474 0.54828387 0.6951474 0.37359107 0.93559146 0.34375 0.84375 0.37359107
		 0.93559146 0.37359107 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526 0.62640893
		 0.93559146 0.62640893 0.93559146 0.62640893 0.93559146 0.65625 0.84375 0.65625 0.84375
		 0.65625 0.84375 0.62640893 0.75190854 0.54828387 0.69514734 0.51609462 0.69514734
		 0.48390535 0.69514734 0.45171607 0.69514734 0.37359107 0.75190854 0.34375 0.84375
		 0.34375 0.84375 0.51609462 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734
		 0.54828387 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.48390535 0.69514734
		 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734
		 0.54828382 0.69514734 0.48390532 0.69514734 0.51609457 0.69514734 0.45171607 0.69514734
		 0.48390532 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.37359107 0.75190854
		 0.45171607 0.69514734 0.34375 0.84375 0.37359107 0.93559146;
	setAttr ".uvst[0].uvsp[4750:4999]" 0.37359107 0.93559146 0.37359107 0.93559146
		 0.37359107 0.93559146 0.37359107 0.93559146 0.37359107 0.93559146 0.66666663 1 0.66666663
		 0 1 0 1 1 0.66666663 1 0.66666663 0 1 0 1 1 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585
		 0.28408265 0.66038585 0.28408265 0.66038585 0.28408265 0.66038585 0.49905562 0 0.49905562
		 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562
		 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562
		 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562
		 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562
		 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562
		 0 0.49905562 0 0.49905562 0 0.49905562 0 0.49905562 0 0 0 0.33333337 0 0.33333337
		 1 0 1 0 0 0.33333337 0 0.33333337 1 0 1 0.45171613 0.9923526 0.45171613 0.9923526
		 0.45171613 0.9923526 0.45171613 0.9923526 0.45171613 0.9923526 0.45171613 0.9923526
		 0.54828393 0.9923526 0.54828393 0.9923526 0.54828393 0.9923526 0.54828393 0.9923526
		 0.54828393 0.9923526 0.54828393 0.9923526 0.66666663 1 0.66666663 0 1 0 1 1 0.66666663
		 1 0.66666663 0 1 0 1 1 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788
		 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16
		 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788
		 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16
		 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788
		 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16
		 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788
		 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16
		 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788
		 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16
		 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788
		 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16 0.50093788 7.6478197e-16
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236 0.71591932 0.66036236
		 0.71591932 0.66036236 0 0 0.33333337 0 0.33333337 1 0 1 0 0 0.33333337 0 0.33333337
		 1 0 1 0.62640893 0.93559146 0.62640893 0.93559146 0.62640893 0.93559146 0.62640893
		 0.93559146 0.62640893 0.93559146 0.62640893 0.93559146 0.65625 0.84375 0.62640893
		 0.75190854 0.54828382 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0.37359107 0.75190854 0.34375 0.84375 0.37359107
		 0.93559146 0.45171613 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625
		 0.84375 0.62640893 0.75190854 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382
		 0.69514734 0.54828382 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0 0.92284733 1 0.92284733 1 1 0 1 0.66666669 0.92284733
		 1 0.92284733 1 1 0.66666669 1;
	setAttr ".uvst[0].uvsp[5000:5249]" 0 0.92284733 1 0.92284733 1 1 0 1 0.66666663
		 0.92284733 1 0.92284733 1 1 0.66666663 1 0.33333331 0.92284733 0.33333331 1 0 0.92284733
		 0 1 0 0.84407264 1 0.84407264 0.66666669 0.84407264 1 0.84407264 0 0.84407264 1 0.84407264
		 0.66666663 0.84407264 1 0.84407264 1 0.92284733 0.66666663 0.92284733 0.33333331
		 0.84407264 0.66666663 0.84407264 0 0.84407264 0.33333331 0.84407264 0.33333331 0.92284733
		 0 0.92284733 0 0.75182015 1 0.75182015 0.66666669 0.75182009 1 0.75182015 0 0.75182015
		 1 0.75182015 0.66666663 0.75182015 1 0.75182015 1 0.84407264 0.33333331 0.75182015
		 0 0.75182015 0 0.84407264 0 0.66550273 1 0.66550273 0.66666669 0.66550273 1 0.66550273
		 0 0.66550273 1 0.66550273 0.66666663 0.66550273 1 0.66550273 1 0.75182015 0.66666663
		 0.75182015 0.33333331 0.66550273 0.66666663 0.66550273 0 0.66550273 0.33333331 0.66550273
		 0.33333331 0.75182015 0 0.75182015 0 0.56898975 1 0.56898975 0.66666669 0.56898975
		 1 0.56898975 0 0.56898975 1 0.56898975 0.66666663 0.56898975 1 0.56898975 1 0.66550273
		 0.33333331 0.56898975 0 0.56898975 0 0.66550273 0 0.47166085 1 0.47166085 0.66666669
		 0.47166085 1 0.47166085 0 0.47166085 1 0.47166085 0.66666663 0.47166085 1 0.47166085
		 1 0.56898975 0.66666663 0.56898975 0.33333331 0.47166085 0.66666663 0.47166085 0
		 0.47166085 0.33333331 0.47166085 0.33333331 0.56898975 0 0.56898975 0 0.38994196
		 1 0.38994196 0.66666669 0.38994193 1 0.38994196 0 0.38994196 1 0.38994196 0.66666663
		 0.38994196 1 0.38994196 1 0.47166085 0.33333331 0.38994196 0 0.38994196 0 0.47166085
		 0 0.31108981 1 0.31108981 0.66666669 0.31108981 1 0.31108981 0 0.31108981 1 0.31108981
		 0.66666663 0.31108981 1 0.31108981 1 0.38994196 0.66666663 0.38994196 0.33333331
		 0.31108981 0.66666663 0.31108981 0 0.31108981 0.33333331 0.31108981 0.33333331 0.38994196
		 0 0.38994196 0 0.21447517 1 0.21447517 0.66666669 0.21447517 1 0.21447517 0 0.21447517
		 1 0.21447517 0.66666663 0.21447517 1 0.21447517 1 0.31108981 0.33333331 0.21447517
		 0 0.21447517 0 0.31108981 0 0.13545586 1 0.13545586 0.66666669 0.13545586 1 0.13545586
		 0 0.13545586 1 0.13545586 0.66666663 0.13545586 1 0.13545586 1 0.21447517 0.66666663
		 0.21447517 0.33333331 0.13545586 0.66666663 0.13545586 0 0.13545586 0.33333331 0.13545586
		 0.33333331 0.21447517 0 0.21447517 1 0.13545586 0 0.13545586 0 0.92284733 0.33333337
		 0.92284733 0.33333337 1 0 1 0 0.84407264 0.33333337 0.84407264 0 0.75182015 0.33333337
		 0.75182009 0 0.66550273 0.33333337 0.66550273 0 0.56898975 0.33333337 0.56898975
		 0 0.47166085 0.33333337 0.47166085 0 0.38994196 0.33333337 0.38994193 0 0.31108981
		 0.33333337 0.31108981 0 0.21447517 0.33333337 0.21447517 0 0.13545586 0.33333337
		 0.13545586 0 0.054881282 0.33333337 0.054881282 0 0 0.33333337 0 0 0 0.33333337 0
		 0.33333337 1 0 1 0 0 0.33333337 0 0.33333337 0.33333331 0 0.33333331 0 0 0.33333337
		 0 0.33333337 1 0 1 0 0 0.33333337 0 0.33333337 1 0 1 0.48390532 0.69514734 0.48390532
		 0.69514734 0.48390532 0.69514734 0.48390535 0.6951474 0.48390535 0.6951474 0.48390535
		 0.6951474 0.48390535 0.6951474 0.48390538 0.6951474 0.48390535 0.6951474 0.48390535
		 0.6951474 0.48390535 0.6951474 0.48390535 0.6951474 0.48390535 0.6951474 0.48390535
		 0.6951474 0.48390535 0.6951474 0.48390535 0.6951474 0.48390535 0.6951474 0.48390535
		 0.6951474 0.48390535 0.6951474 0.48390535 0.6951474 0.5333333 0.64666879 0.5333333
		 0.68843985 0.5333333 0.60489768 0.5333333 0.59097397 0.5333333 0.57705027 0.5333333
		 0.56312656 0.5333333 0.50046992 0.5333333 0.43781328 0.5333333 0.37515664 0.5333333
		 0.3125 0.48390538 0.3048526 0.48390538 0.3048526 0.48390538 0.3048526 0.48390538
		 0.3048526 0.48390538 0.3048526 0.48390538 0.3048526 0.48390538 0.3048526 0.48390538
		 0.3048526 0.48390538 0.3048526 0.48390538 0.3048526 0.48390538 0.3048526 0.48390538
		 0.30485258 0.48390538 0.30485258 0.48390538 0.30485258 0.48390538 0.30485258 0.48390538
		 0.30485258 0.48390532 0.30485258 0.48390538 0.30485258 0.48390538 0.3048526 0 0.60887909
		 1 0.60887569 0.99348027 0 0.0065132892 5.503196e-06 0.0064778728 0.60493565 0.99999994
		 0.60493213 0.98057342 0 0 4.3219807e-06 0 0.60493636 0.9935286 0.60493338 1 0 0.01942005
		 3.3694801e-06 0.48390535 0.69514734 0.45171607 0.69514734 0.48390538 0.6951474 0.45171607
		 0.6951474 0.48390532 0.69514734;
	setAttr ".uvst[0].uvsp[5250:5499]" 0.45171607 0.69514734 0.48390532 0.69514734
		 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734
		 0.54828382 0.69514734 0.51609462 0.69514734 0.51609462 0.6951474 0.54828387 0.69514734
		 0.54828387 0.6951474 0.51609462 0.6951474 0.51609462 0.6951474 0.54828387 0.6951474
		 0.54828387 0.6951474 0.48390538 0.6951474 0.45171607 0.6951474 0.48390535 0.6951474
		 0.45171607 0.6951474 0.51609457 0.69514734 0.51609462 0.69514734 0.54828382 0.69514734
		 0.54828387 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.48390535 0.69514734
		 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734
		 0.54828382 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734
		 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734
		 0.54828382 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734
		 0.45171607 0.69514734 1 0.84407264 0.66666663 0.84407264 1 0.92284733 0.66666663
		 0.92284733 0.33333331 0.84407264 0 0.84407264 0.33333331 0.92284733 0 0.92284733
		 1 0.66550273 0.66666663 0.66550273 1 0.75182015 0.66666663 0.75182015 0.33333331
		 0.66550273 0 0.66550273 0.33333331 0.75182015 0 0.75182015 1 0.47166085 0.66666663
		 0.47166085 1 0.56898975 0.66666663 0.56898975 0.33333331 0.47166085 0 0.47166085
		 0.33333331 0.56898975 0 0.56898975 1 0.31108981 0.66666663 0.31108981 1 0.38994196
		 0.66666663 0.38994196 0.33333331 0.31108981 0 0.31108981 0.33333331 0.38994196 0
		 0.38994196 1 0.13545586 0.66666663 0.13545586 1 0.21447517 0.66666663 0.21447517
		 0.33333331 0.13545586 0 0.13545586 0.33333331 0.21447517 0 0.21447517 0.48390535
		 0.69514734 0.45171607 0.69514734 0.48390538 0.6951474 0.45171607 0.6951474 0.48390532
		 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.51609457
		 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734 0.54828382 0.69514734 0.51609462
		 0.69514734 0.51609462 0.6951474 0.54828387 0.69514734 0.54828387 0.6951474 0.51609462
		 0.6951474 0.51609462 0.6951474 0.54828387 0.6951474 0.54828387 0.6951474 0.48390538
		 0.6951474 0.45171607 0.6951474 0.48390535 0.6951474 0.45171607 0.6951474 0.51609457
		 0.69514734 0.51609462 0.69514734 0.54828382 0.69514734 0.54828387 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0.48390535 0.69514734 0.45171607 0.69514734 0.51609457
		 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734 0.54828382 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.51609457
		 0.69514734 0.51609457 0.69514734 0.54828382 0.69514734 0.54828382 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 1 0.84407264
		 0.66666663 0.84407264 1 0.92284733 0.66666663 0.92284733 0.33333331 0.84407264 0
		 0.84407264 0.33333331 0.92284733 0 0.92284733 1 0.66550273 0.66666663 0.66550273
		 1 0.75182015 0.66666663 0.75182015 0.33333331 0.66550273 0 0.66550273 0.33333331
		 0.75182015 0 0.75182015 1 0.47166085 0.66666663 0.47166085 1 0.56898975 0.66666663
		 0.56898975 0.33333331 0.47166085 0 0.47166085 0.33333331 0.56898975 0 0.56898975
		 1 0.31108981 0.66666663 0.31108981 1 0.38994196 0.66666663 0.38994196 0.33333331
		 0.31108981 0 0.31108981 0.33333331 0.38994196 0 0.38994196 1 0.13545586 0.66666663
		 0.13545586 1 0.21447517 0.66666663 0.21447517 0.33333331 0.13545586 0 0.13545586
		 0.33333331 0.21447517 0 0.21447517 0.48390535 0.69514734 0.45171607 0.69514734 0.48390538
		 0.6951474 0.45171607 0.6951474 0.48390532 0.69514734 0.45171607 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382
		 0.69514734 0.54828382 0.69514734 0.51609462 0.69514734 0.51609462 0.6951474 0.54828387
		 0.69514734 0.54828387 0.6951474 0.51609462 0.6951474 0.51609462 0.6951474 0.54828387
		 0.6951474 0.54828387 0.6951474 0.48390538 0.6951474 0.45171607 0.6951474 0.48390535
		 0.6951474 0.45171607 0.6951474 0.51609457 0.69514734 0.51609462 0.69514734 0.54828382
		 0.69514734 0.54828387 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.48390535
		 0.69514734 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382
		 0.69514734 0.54828382 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 0.51609457 0.69514734 0.51609457 0.69514734 0.54828382
		 0.69514734 0.54828382 0.69514734 0.48390532 0.69514734 0.45171607 0.69514734 0.48390532
		 0.69514734 0.45171607 0.69514734 1 0.84407264 0.66666663 0.84407264 1 0.92284733
		 0.66666663 0.92284733 0.33333331 0.84407264 0 0.84407264 0.33333331 0.92284733 0
		 0.92284733 1 0.66550267 0.66666663 0.66550267 1 0.75182015 0.66666663 0.75182015
		 0.33333331 0.66550273 0 0.66550273 0.33333331 0.75182015 0 0.75182015 1 0.47166085
		 0.66666663 0.47166085 1 0.56898975 0.66666663 0.56898975 0.33333331 0.47166085 0
		 0.47166085 0.33333331 0.56898975 0 0.56898975 1 0.31108981 0.66666663 0.31108981
		 1 0.38994196 0.66666663 0.38994196 0.33333331 0.31108981 0 0.31108981 0.33333331
		 0.38994196;
	setAttr ".uvst[0].uvsp[5500:5596]" 0 0.38994196 1 0.13545586 0.66666663 0.13545586
		 1 0.21447517 0.66666663 0.21447517 0.33333331 0.13545586 0 0.13545586 0.33333331
		 0.21447517 0 0.21447517 0 0.66666663 1 0.66666663 1 1 0 1 0.66666669 0.66666663 1
		 0.66666663 1 1 0.66666669 1 0.33333337 0.66666663 0.33333337 1 0 0.66666663 0 1 0
		 0.66666663 1 0.66666663 1 1 0 1 0.66666663 0.66666663 1 0.66666663 1 1 0.66666663
		 1 0.33333331 0.66666663 0.33333331 1 0 0.66666663 0 1 0 0 1 0 1 0 0 0 0.5 1 0.5 1
		 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.5 1 0.5 1 0 0 1 0 1 0 0 0 0.5 1 0.5 1 0 0 1 0 1
		 0 0 0 0 0 1 0 1 0 0 0 0.5 1 0.5 1 1 0 0 0 0.5 1 1 0 0 0 1 0 0 0 0.5 1 1 0 0 0 0.5
		 1 1 0 0 0 1 0 0 0 0.5 1 1 0 0 0 0.5 1 1 0 0 0 1 0 0 0 0.5 1 1 0 0 0 0.5 1 1 0 0 0
		 1 0 0 0 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4658 ".vt";
	setAttr ".vt[0:165]"  -0.86468518 -0.9790327 -1.79586101 -0.76099998 -0.9790327 -1.83880877
		 -0.65731496 -0.9790327 -1.79586101 -0.61436719 -0.9790327 -1.69217515 -0.65731496 -0.9790327 -1.58849013
		 -0.76099998 -0.9790327 -1.54554188 -0.86468518 -0.9790327 -1.58849013 -0.90763307 -0.9790327 -1.69217515
		 -0.94539922 0.76663685 -1.7259618 -0.68591952 0.76663685 -1.85954571 -0.42643973 0.76663685 -1.7259618
		 -0.31895965 0.76663685 -1.40346169 -0.42643973 0.76663685 -1.080961943 -0.68591952 0.76663685 -0.94737792
		 -0.94539946 0.76663685 -1.080961823 -1.052879453 0.76663685 -1.40346169 -0.81799877 0.99330431 -1.59245539
		 -0.62510425 0.99330431 -1.69203949 -0.43220988 0.99330431 -1.59245539 -0.35231039 0.99330431 -1.35203838
		 -0.43220988 0.99330431 -1.11162138 -0.62510425 0.99330431 -1.012037396 -0.81799877 0.99330431 -1.11162138
		 -0.8978982 0.99330431 -1.35203838 -0.71098965 1.20560586 -1.47633147 -0.60556936 1.20560586 -1.54368353
		 -0.50014901 1.20560586 -1.47633147 -0.45648238 1.20560586 -1.31372941 -0.50014901 1.20560586 -1.1511271
		 -0.60556936 1.20560586 -1.083775282 -0.71098965 1.20560586 -1.1511271 -0.75465626 1.20560586 -1.31372941
		 -0.60556924 1.21997178 -1.36161542 -0.76967722 -0.48410624 -1.96869779 -0.87144607 -0.48410624 -1.89702582
		 -0.91360027 -0.48410624 -1.72399449 -0.87144607 -0.48410624 -1.55096388 -0.76967722 -0.48410624 -1.47929192
		 -0.66790807 -0.48410624 -1.55096388 -0.62575418 -0.48410624 -1.72399449 -0.66790807 -0.48410624 -1.89702582
		 -0.79449272 -0.0035138726 -1.19649231 -0.62941611 -0.0035138726 -1.27155614 -0.56103909 -0.0035138726 -1.45277548
		 -0.62941611 -0.0035138726 -1.63399565 -0.79449272 -0.0035138726 -1.70905924 -0.95956945 -0.0035138726 -1.63399565
		 -1.027946472 -0.0035138726 -1.45277548 -0.95956945 -0.0035138726 -1.27155614 -0.97644079 0.089921772 -1.090758085
		 -0.79449272 0.089921772 -0.98138642 -0.61254513 0.089921772 -1.090758085 -0.53717965 0.089921772 -1.35480487
		 -0.61254513 0.089921772 -1.61885166 -0.79449272 0.089921772 -1.72822332 -0.97644079 0.089921772 -1.61885166
		 -1.051806092 0.089921772 -1.35480487 -0.79286587 0.37912905 -0.87079155 -0.56686628 0.37912905 -1.0039806366
		 -0.47325405 0.37912905 -1.32552624 -0.56686628 0.37912905 -1.6470716 -0.79286587 0.37912905 -1.7802608
		 -1.018865824 0.37912905 -1.6470716 -1.1124779 0.37912905 -1.32552624 -1.018865943 0.37912905 -1.0039806366
		 -0.78015906 -0.39235836 -1.35913098 -0.68307865 -0.39235836 -1.43852258 -0.64286673 -0.39235836 -1.63019001
		 -0.68307865 -0.39235836 -1.82185793 -0.78015906 -0.39235836 -1.90124965 -0.87723929 -0.39235836 -1.82185793
		 -0.91745132 -0.39235836 -1.63019001 -0.87723929 -0.39235836 -1.43852258 -1.16731167 0.57085568 -1.36408603
		 -1.042140841 0.57085568 -1.042068243 -0.73995221 0.57085568 -0.90868402 -0.43776363 0.57085568 -1.042068243
		 -0.31259298 0.57085568 -1.36408603 -0.43776363 0.57085568 -1.68610406 -0.73995221 0.57085568 -1.81948841
		 -1.042140841 0.57085568 -1.68610406 -0.96969944 -0.67852563 -1.71149552 -0.91011596 -0.67852563 -1.56570506
		 -0.76626855 -0.67852563 -1.5053165 -0.62242121 -0.67852563 -1.56570506 -0.56283784 -0.67852563 -1.71149552
		 -0.62242121 -0.67852563 -1.85728598 -0.76626855 -0.67852563 -1.91767406 -0.91011596 -0.67852563 -1.85728598
		 -0.88661397 -1.46122313 -1.5737716 -0.76099998 -1.49593544 -1.61253107 -0.76099998 -1.37742066 -1.48019767
		 -0.63538623 -1.46122313 -1.5737716 -0.58335543 -1.37742066 -1.48019767 -0.63538623 -1.29361773 -1.38662386
		 -0.76099998 -1.25890565 -1.34786439 -0.88661397 -1.29361773 -1.38662386 -0.93864483 -1.37742066 -1.48019767
		 -0.76099998 -1.19024277 -1.82825804 -0.61525536 -1.1763618 -1.77357507 -0.55488592 -1.14284945 -1.64155829
		 -0.61525536 -1.10933733 -1.50954175 -0.76099998 -1.095456362 -1.4548583 -0.9067449 -1.10933733 -1.50954175
		 -0.96711439 -1.14284945 -1.64155829 -0.9067449 -1.1763618 -1.77357507 -1.08019948 -1.45364118 -0.9648369
		 -0.95458561 -1.47883201 -0.91931093 -0.95458615 -1.39282513 -1.074747324 -0.95458663 -1.30681753 -1.23018432
		 -1.080200076 -1.3320086 -1.18465829 -1.1322304 -1.39282513 -1.074747562 -0.63790351 -1.4778204 -0.92286658
		 -0.51228976 -1.45268679 -0.96842438 -0.63790327 -1.39201021 -1.078413129 -0.46025881 -1.39201021 -1.078412533
		 -0.51228935 -1.33133364 -1.18840134 -0.63790292 -1.30620027 -1.23395967 0.86465657 -1.038947701 -1.79586101
		 0.76097107 -1.038947701 -1.83880877 0.65728569 -1.038947701 -1.79586101 0.6143378 -1.038947701 -1.69217515
		 0.65728569 -1.038947701 -1.58849013 0.76097107 -1.038947701 -1.54554188 0.86465657 -1.038947701 -1.58849013
		 0.90760458 -1.038947701 -1.69217515 0.94537091 0.76663685 -1.7259618 0.68589032 0.76663685 -1.85954571
		 0.42640975 0.76663685 -1.7259618 0.31892931 0.76663685 -1.40346169 0.42640975 0.76663685 -1.080961943
		 0.68589032 0.76663685 -0.94737792 0.94537115 0.76663685 -1.080961823 1.052851439 0.76663685 -1.40346169
		 0.81797004 0.99330431 -1.59245539 0.62507486 0.99330431 -1.69203949 0.4321799 0.99330431 -1.59245539
		 0.35228017 0.99330431 -1.35203838 0.4321799 0.99330431 -1.11162138 0.62507486 0.99330431 -1.012037396
		 0.81797004 0.99330431 -1.11162138 0.89786971 0.99330431 -1.35203838 0.71096057 1.20560586 -1.47633147
		 0.60553992 1.20560586 -1.54368353 0.50011921 1.20560586 -1.47633147 0.45645246 1.20560586 -1.31372941
		 0.50011921 1.20560586 -1.1511271 0.60553992 1.20560586 -1.083775282 0.71096057 1.20560586 -1.1511271
		 0.75462735 1.20560586 -1.31372941 0.6055398 1.21997178 -1.36161542 0.76964831 -0.54402131 -1.96869779
		 0.87141752 -0.54402131 -1.89702582 0.91357183 -0.54402131 -1.72399449 0.87141752 -0.54402131 -1.55096388
		 0.76964831 -0.54402131 -1.47929192 0.66787887 -0.54402131 -1.55096388 0.62572485 -0.54402131 -1.72399449
		 0.66787887 -0.54402131 -1.89702582 0.79446393 -0.063428916 -1.19649231 0.62938678 -0.063428916 -1.27155614
		 0.56100953 -0.063428916 -1.45277548 0.62938678 -0.063428916 -1.63399565 0.79446393 -0.063428916 -1.70905924
		 0.95954114 -0.063428916 -1.63399565 1.027918458 -0.063428916 -1.45277548;
	setAttr ".vt[166:331]" 0.95954114 -0.063428916 -1.27155614 0.97641253 0.030006725 -1.090758085
		 0.79446393 0.030006725 -0.98138642 0.61251575 0.030006725 -1.090758085 0.53715003 0.030006725 -1.35480487
		 0.61251575 0.030006725 -1.61885166 0.79446393 0.030006725 -1.72822332 0.97641253 0.030006725 -1.61885166
		 1.051778078 0.030006725 -1.35480487 0.79283708 0.37912905 -0.87079155 0.56683671 0.37912905 -1.0039806366
		 0.47322419 0.37912905 -1.32552624 0.56683671 0.37912905 -1.6470716 0.79283708 0.37912905 -1.7802608
		 1.01883781 0.37912905 -1.6470716 1.11245012 0.37912905 -1.32552624 1.018837929 0.37912905 -1.0039806366
		 0.78013021 -0.4522734 -1.35913098 0.6830495 -0.4522734 -1.43852258 0.64283741 -0.4522734 -1.63019001
		 0.6830495 -0.4522734 -1.82185793 0.78013021 -0.4522734 -1.90124965 0.87721074 -0.4522734 -1.82185793
		 0.91742289 -0.4522734 -1.63019001 0.87721074 -0.4522734 -1.43852258 1.16728401 0.57085568 -1.36408603
		 1.042112827 0.57085568 -1.042068243 0.73992324 0.57085568 -0.90868402 0.43773365 0.57085568 -1.042068243
		 0.31256261 0.57085568 -1.36408603 0.43773365 0.57085568 -1.68610406 0.73992324 0.57085568 -1.81948841
		 1.042112827 0.57085568 -1.68610406 0.96967125 -0.73844069 -1.71149552 0.91008759 -0.73844069 -1.56570506
		 0.76623964 -0.73844069 -1.5053165 0.62239182 -0.73844069 -1.56570506 0.56280828 -0.73844069 -1.71149552
		 0.62239182 -0.73844069 -1.85728598 0.76623964 -0.73844069 -1.91767406 0.91008759 -0.73844069 -1.85728598
		 0.88658547 -1.52113819 -1.5737716 0.76097107 -1.55585051 -1.61253107 0.76097107 -1.43733573 -1.48019767
		 0.6353569 -1.52113819 -1.5737716 0.58332598 -1.43733573 -1.48019767 0.6353569 -1.35353279 -1.38662386
		 0.76097107 -1.31882071 -1.34786439 0.88658547 -1.35353279 -1.38662386 0.93861645 -1.43733573 -1.48019767
		 0.76097107 -1.25015783 -1.82825804 0.61522591 -1.23627687 -1.77357507 0.5548563 -1.20276451 -1.64155829
		 0.61522591 -1.1692524 -1.50954175 0.76097107 -1.15537143 -1.4548583 0.90671647 -1.1692524 -1.50954175
		 0.96708614 -1.20276451 -1.64155829 0.90671647 -1.23627687 -1.77357507 1.080171704 -1.51355624 -0.9648369
		 0.95455736 -1.53874707 -0.91931093 0.95455784 -1.45274019 -1.074747324 0.95455831 -1.3667326 -1.23018432
		 1.080172181 -1.39192367 -1.18465829 1.13220263 -1.45274019 -1.074747562 0.63787419 -1.53773546 -0.92286658
		 0.51226008 -1.51260185 -0.96842438 0.63787395 -1.45192528 -1.078413129 0.46022895 -1.45192528 -1.078412533
		 0.51225966 -1.3912487 -1.18840134 0.63787365 -1.36611533 -1.23395967 0.8839258 -0.92298567 0.036869973
		 0.78423721 -0.92298567 -0.0044223666 0.68454874 -0.92298567 0.036869973 0.64325643 -0.92298567 0.13655844
		 0.68454874 -0.92298567 0.23624697 0.78423721 -0.92298567 0.27753928 0.8839258 -0.92298567 0.23624697
		 0.92521805 -0.92298567 0.13655844 0.89933079 0.90203595 0.10939148 0.72589475 0.90203589 0.023672551
		 0.55245864 0.90203595 0.10939148 0.48061919 0.90203589 0.31633538 0.55245864 0.90203589 0.52327919
		 0.72589475 0.90203589 0.60899818 0.89933079 0.90203589 0.52327919 0.97117043 0.90203589 0.31633538
		 0.79148555 1.08973217 0.18371108 0.64770436 1.08973217 0.12415491 0.50392318 1.08973217 0.18371108
		 0.44436696 1.08973217 0.32749227 0.50392318 1.08973217 0.47127345 0.64770436 1.08973217 0.53082955
		 0.79148561 1.08973217 0.47127345 0.85104167 1.08973217 0.32749227 0.66903973 1.23667228 0.25464028
		 0.58657014 1.23667228 0.22048023 0.5041008 1.23667228 0.25464028 0.46994066 1.23667228 0.33710968
		 0.5041008 1.23667228 0.41957921 0.58657014 1.23667228 0.45373929 0.66903973 1.23667228 0.41957921
		 0.70319968 1.23667228 0.33710968 0.58657014 1.23667228 0.33710968 0.78423721 0.75288379 -0.057801485
		 0.97918361 0.75288385 0.040569216 1.059932947 0.75288385 0.27805713 0.97918361 0.75288385 0.51554501
		 0.78423721 0.75288379 0.61391568 0.58929092 0.75288385 0.51554495 0.50854146 0.75288385 0.27805713
		 0.58929092 0.75288385 0.040569216 0.78423721 0.14306492 -0.00042757392 0.96932805 0.14306492 0.076239616
		 1.045995235 0.14306492 0.26133043 0.96932805 0.14306492 0.44642124 0.78423721 0.14306492 0.52308828
		 0.59914637 0.14306492 0.44642124 0.5224793 0.14306492 0.26133043 0.59914637 0.14306492 0.076239616
		 0.78423721 -0.61435986 -0.1207014 0.93433213 -0.61435962 -0.058530092 0.99650353 -0.61435962 0.091564879
		 0.93433213 -0.61435962 0.24165985 0.78423721 -0.61435986 0.30383113 0.63414234 -0.61435962 0.24165982
		 0.57197088 -0.61435962 0.091564879 0.63414234 -0.61435962 -0.058530092 0.78423721 -0.34100986 0.37202162
		 0.64735091 -0.29573739 0.31550092 0.59065074 -0.18644023 0.17904741 0.64735091 -0.077142954 0.042593837
		 0.78423721 -0.031870723 -0.013927132 0.9211235 -0.077142954 0.042593837 0.97782379 -0.18644023 0.17904741
		 0.92112356 -0.29573739 0.31550094 0.78423721 -0.18584526 0.4998664 0.63730252 -0.15209484 0.42674956
		 0.57644016 -0.070614576 0.25022936 0.63730252 0.010865927 0.073709279 0.78423721 0.044616222 0.00059220195
		 0.93117201 0.010865927 0.073709279 0.99203444 -0.070614576 0.25022936 0.93117201 -0.15209484 0.42674956
		 0.88961422 -0.37033832 0.28302798 0.78423721 -0.4016608 0.33386216 0.67886025 -0.37033832 0.28302795
		 0.63521165 -0.29471993 0.16030285 0.67886025 -0.21910131 0.037577718 0.78423721 -0.18777919 -0.013256729
		 0.88961422 -0.21910131 0.037577718 0.93326283 -0.29471993 0.16030285 0.90657073 -1.39290631 0.22298926
		 0.78423721 -1.42496955 0.18375108 0.78423721 -1.31549966 0.31771901 0.66190374 -1.39290631 0.22298941
		 0.61123157 -1.31549942 0.31771883 0.66190374 -1.23809278 0.41244841 0.78423721 -1.20603001 0.45168668
		 0.90657073 -1.23809254 0.41244903 0.95724297 -1.31549942 0.31771883 0.78423721 -1.11882079 -0.029719442
		 0.64136034 -1.10335553 0.02394703 0.58217871 -1.066018462 0.1535092 0.64136034 -1.028681397 0.28307161
		 0.78423721 -1.013216376 0.33673802 0.92711425 -1.028681397 0.28307182;
	setAttr ".vt[332:497]" 0.98629582 -1.066018462 0.1535092 0.92711425 -1.10335553 0.023946911
		 1.062136412 -1.38597023 0.8348074 0.89748919 -1.41211021 0.8977977 0.89748931 -1.32286155 0.6827358
		 0.89748937 -1.23361313 0.46767479 1.062136531 -1.25975239 0.53066474 1.13033605 -1.32286155 0.68273604
		 0.67593336 -1.41135085 0.89921558 0.51036203 -1.38457024 0.83607817 0.67593342 -1.31991541 0.68365341
		 0.44178054 -1.31991613 0.68365264 0.51036215 -1.25526154 0.53122735 0.6759336 -1.22848046 0.46809113
		 1.16665697 0.56100297 0.27279404 1.054648757 0.56100297 0.4937951 0.78423721 0.56100291 0.58533663
		 0.51382565 0.56100297 0.49379504 0.40181756 0.56100297 0.27279404 0.51382565 0.56100297 0.051792979
		 0.78423721 0.56100291 -0.039748639 1.054648757 0.56100297 0.051792979 -0.88384986 -0.92298567 0.036869973
		 -0.78399998 -0.92298567 -0.0044223666 -0.68415022 -0.92298567 0.036869973 -0.64279109 -0.92298567 0.13655844
		 -0.68415022 -0.92298567 0.23624697 -0.78399998 -0.92298567 0.27753928 -0.88384986 -0.92298567 0.23624697
		 -0.92520893 -0.92298567 0.13655844 -0.89927983 0.90203595 0.10939148 -0.72556311 0.90203589 0.023672551
		 -0.55184639 0.90203595 0.10939148 -0.4798907 0.90203589 0.31633538 -0.55184639 0.90203589 0.52327919
		 -0.72556311 0.90203589 0.60899818 -0.89927983 0.90203589 0.52327919 -0.97123563 0.90203589 0.31633538
		 -0.79126006 1.08973217 0.18371108 -0.64724618 1.08973217 0.12415491 -0.50323236 1.08973217 0.18371108
		 -0.44357982 1.08973217 0.32749227 -0.50323236 1.08973217 0.47127345 -0.64724618 1.08973217 0.53082955
		 -0.79126012 1.08973217 0.47127345 -0.85091257 1.08973217 0.32749227 -0.66861612 1.23667228 0.25464028
		 -0.58601308 1.23667228 0.22048023 -0.50341022 1.23667228 0.25464028 -0.46919486 1.23667228 0.33710968
		 -0.50341022 1.23667228 0.41957921 -0.58601308 1.23667228 0.45373929 -0.66861612 1.23667228 0.41957921
		 -0.70283133 1.23667228 0.33710968 -0.58601308 1.23667228 0.33710968 -0.78399998 0.75288379 -0.057801485
		 -0.97926182 0.75288385 0.040569216 -1.060141921 0.75288385 0.27805713 -0.97926182 0.75288385 0.51554501
		 -0.78399998 0.75288379 0.61391568 -0.5887382 0.75288385 0.51554495 -0.50785816 0.75288385 0.27805713
		 -0.5887382 0.75288385 0.040569216 -0.78399998 0.14306492 -0.00042757392 -0.96939033 0.14306492 0.076239616
		 -1.04618156 0.14306492 0.26133043 -0.96939033 0.14306492 0.44642124 -0.78399998 0.14306492 0.52308828
		 -0.59860963 0.14306492 0.44642124 -0.52181852 0.14306492 0.26133043 -0.59860963 0.14306492 0.076239616
		 -0.78399998 -0.61435986 -0.1207014 -0.93433779 -0.61435962 -0.058530092 -0.99660975 -0.61435962 0.091564879
		 -0.93433779 -0.61435962 0.24165985 -0.78399998 -0.61435986 0.30383113 -0.63366222 -0.61435962 0.24165982
		 -0.57139021 -0.61435962 0.091564879 -0.63366222 -0.61435962 -0.058530092 -0.78399998 -0.34100986 0.37202162
		 -0.64689219 -0.29573739 0.31550092 -0.59010029 -0.18644023 0.17904741 -0.64689219 -0.077142954 0.042593837
		 -0.78399998 -0.031870723 -0.013927132 -0.92110777 -0.077142954 0.042593837 -0.97789979 -0.18644023 0.17904741
		 -0.92110783 -0.29573739 0.31550094 -0.78399998 -0.18584526 0.4998664 -0.63682753 -0.15209484 0.42674956
		 -0.5758667 -0.070614576 0.25022936 -0.63682753 0.010865927 0.073709279 -0.78399998 0.044616222 0.00059220195
		 -0.93117255 0.010865927 0.073709279 -0.99213344 -0.070614576 0.25022936 -0.93117255 -0.15209484 0.42674956
		 -0.88954753 -0.37033832 0.28302798 -0.78399998 -0.4016608 0.33386216 -0.67845249 -0.37033832 0.28302795
		 -0.63473326 -0.29471993 0.16030285 -0.67845249 -0.21910131 0.037577718 -0.78399998 -0.18777919 -0.013256729
		 -0.88954747 -0.21910131 0.037577718 -0.93326676 -0.29471993 0.16030285 -0.90653145 -1.39290631 0.22298926
		 -0.78399998 -1.42496955 0.18375108 -0.78399998 -1.31549966 0.31771901 -0.66146857 -1.39290631 0.22298941
		 -0.61071438 -1.31549942 0.31771883 -0.66146857 -1.23809278 0.41244841 -0.78399998 -1.20603001 0.45168668
		 -0.90653145 -1.23809254 0.41244903 -0.9572857 -1.31549942 0.31771883 -0.78399998 -1.11882079 -0.029719442
		 -0.64089191 -1.10335553 0.02394703 -0.58161455 -1.066018462 0.1535092 -0.64089191 -1.028681397 0.28307161
		 -0.78399998 -1.013216376 0.33673802 -0.92710823 -1.028681397 0.28307182 -0.98638552 -1.066018462 0.1535092
		 -0.92710823 -1.10335553 0.023946911 -1.062348843 -1.38597023 0.8348074 -0.89743525 -1.41211021 0.8977977
		 -0.89743531 -1.32286155 0.6827358 -0.89743537 -1.23361313 0.46767479 -1.062348962 -1.25975239 0.53066474
		 -1.13065875 -1.32286155 0.68273604 -0.6755209 -1.41135085 0.89921558 -0.50968164 -1.38457024 0.83607817
		 -0.67552096 -1.31991541 0.68365341 -0.4409892 -1.31991613 0.68365264 -0.50968182 -1.25526154 0.53122735
		 -0.67552114 -1.22848046 0.46809113 -1.16703844 0.56100297 0.27279404 -1.054849148 0.56100297 0.4937951
		 -0.78399998 0.56100291 0.58533663 -0.51315093 0.56100297 0.49379504 -0.40096158 0.56100297 0.27279404
		 -0.51315093 0.56100297 0.051792979 -0.78399998 0.56100291 -0.039748639 -1.054849148 0.56100297 0.051792979
		 -0.043878034 2.85120368 0.84357822 0.043878034 2.85120368 0.84357822 -0.043878034 2.99847865 0.445986
		 0.043878034 2.99847865 0.445986 -0.022668874 2.93719459 0.38267609 0.022668874 2.93719459 0.38267609
		 -0.022668874 2.66247201 0.64860815 0.022668874 2.66247201 0.64860815 0.022668874 2.76535726 0.66327155
		 -0.022668874 2.76535726 0.66327155 -0.043878034 2.87382936 0.77532923 0.043878034 2.87382936 0.77532923
		 0.022668874 2.86054754 0.66998589 -0.022668874 2.86054754 0.66998589 -0.043878034 2.91961646 0.73100722
		 0.043878034 2.91961646 0.73100722 0.022668874 2.9186902 0.63842773 -0.022668874 2.9186902 0.63842773
		 -0.043878034 2.9692781 0.69068778 0.043878034 2.9692781 0.69068778 0.022668874 2.95212173 0.58134186
		 -0.022668874 2.95212173 0.58134186 -0.043878034 3.0073444843 0.63838983 0.043878034 3.0073444843 0.63838983
		 0.022668874 2.9485054 0.48598346 -0.022668874 2.9485054 0.48598346;
	setAttr ".vt[498:663]" -0.043878034 3.034543514 0.5748654 0.043878034 3.034543514 0.5748654
		 -0.061485898 3.16958666 1.50784779 0.061485898 3.16958666 1.50784779 -0.061485898 3.48479223 1.17443335
		 0.061485898 3.48479223 1.17443335 -0.031765688 3.45790482 1.053924561 0.031765688 3.45790482 1.053924561
		 -0.031765688 3.086784124 1.13672709 0.031765688 3.086784124 1.13672709 0.031765688 3.17368817 1.23520386
		 -0.031765688 3.17368817 1.23520386 -0.061485898 3.22127843 1.44850302 0.061485898 3.22127843 1.44850302
		 0.031765688 3.25721645 1.31854987 -0.031765688 3.25721645 1.31854987 -0.061485898 3.28313184 1.43470263
		 0.061485898 3.28313184 1.43470263 0.031765688 3.32449079 1.32904518 -0.031765688 3.32449079 1.32904518
		 -0.061485898 3.34668517 1.42852092 0.061485898 3.34668517 1.42852092 0.031765688 3.38092351 1.29094911
		 -0.031765688 3.38092351 1.29094911 -0.061485898 3.40515137 1.39953852 0.061485898 3.40515137 1.39953852
		 0.031765688 3.42110205 1.18000221 -0.031765688 3.42110205 1.18000221 -0.061485898 3.45884967 1.34918666
		 0.061485898 3.45884967 1.34918666 -0.05654243 3.15467215 2.26034594 0.05654243 3.15467215 2.26034594
		 -0.05654243 3.54131436 2.091417551 0.05654243 3.54131436 2.091417551 -0.029211728 3.56186867 1.97974861
		 0.029211728 3.56186867 1.97974861 -0.029211728 3.2179718 1.91644907 0.029211728 3.2179718 1.91644907
		 0.029211728 3.25613761 2.031039953 -0.029211728 3.25613761 2.031039953 -0.05654243 3.2197566 2.22869253
		 0.05654243 3.2197566 2.22869253 0.029211728 3.29688406 2.13161016 -0.029211728 3.29688406 2.13161016
		 -0.05654243 3.27707267 2.23924232 0.05654243 3.27707267 2.23924232 0.029211728 3.35005617 2.16467357
		 -0.029211728 3.35005617 2.16467357 -0.05654243 3.33308935 2.25685215 0.05654243 3.33308935 2.25685215
		 0.029211728 3.41151643 2.15270996 -0.029211728 3.41151643 2.15270996 -0.05654243 3.39299488 2.25333381
		 0.05654243 3.39299488 2.25333381 0.029211728 3.48540211 2.073239803 -0.029211728 3.48540211 2.073239803
		 -0.05654243 3.45654535 2.23001361 0.05654243 3.45654535 2.23001361 2.38321495 4.1930275 2.18755317
		 2.34176254 4.34998226 2.050197363 2.5080688 4.30004501 1.9429456 2.54952121 4.14309025 2.080301523
		 2.60257459 4.48428965 2.40259981 2.57410979 4.5920682 2.30827951 2.71054721 4.5970974 2.27285099
		 2.739012 4.48931932 2.36717153 2.52945542 4.38720322 2.33091831 2.49666142 4.51137352 2.22225332
		 2.64305687 4.49808168 2.16288424 2.67585087 4.37391138 2.2715497 2.45633769 4.29011774 2.25923824
		 2.41921449 4.43068027 2.13622689 2.57556486 4.39906454 2.052915812 2.61268806 4.25850248 2.17592692
		 2.38701701 4.19807196 2.19127798 2.34578919 4.35417604 2.054666758 2.51157212 4.30518389 1.9486531
		 2.55279994 4.1490798 2.085264444 2.60001516 4.48089361 2.40009189 2.57139921 4.58924484 2.3052702
		 2.70819664 4.59364176 2.26901078 2.73681283 4.48529053 2.36383271 2.45960808 4.29445887 2.26244307
		 2.42267823 4.43428898 2.1400733 2.57858324 4.40349197 2.057832479 2.61551285 4.26366234 2.18020177
		 2.52771497 4.38489151 2.32921171 2.49481773 4.50945234 2.22020435 2.64145565 4.49572802 2.16026807
		 2.67435288 4.37116718 2.26927543 5.94355059 5.36729813 2.16007233 5.97634459 5.24312782 2.26873827
		 6.028792858 5.45853281 2.25518942 6.057409286 5.3501811 2.35001135 3.91065884 2.76045036 -0.23871785
		 3.95188665 2.60434628 -0.1021061 4.051416874 2.81329703 -0.20451066 4.088540077 2.67273498 -0.081499994
		 5.44884253 4.070522308 0.81114185 5.48577213 3.93069267 0.9335115 5.55197668 4.15080118 0.88776326
		 5.58487368 4.026239872 0.99677086 5.48004389 3.95238233 0.9145301 2.60320282 4.31027222 2.13941193
		 2.60031366 4.3053565 2.13492322 4.084482193 2.68809891 -0.094945833 3.9473803 2.62140965 -0.11703863
		 2.53905725 4.20111465 2.039727211 2.53570366 4.19540882 2.034516335 2.36939764 4.24534559 2.14176822
		 2.37327456 4.25010633 2.14574122 2.44396329 4.33697176 2.2182343 2.45613289 4.30761719 2.25092816
		 2.52461934 4.39661264 2.31895447 2.51852393 4.42859364 2.29469681 2.59047651 4.51701069 2.36848474
		 2.59308624 4.52021599 2.37115955 2.72952366 4.52524519 2.33573151 2.72727418 4.5214076 2.33222532
		 6.05282402 5.36754179 2.33481884 5.97109032 5.2630229 2.25132704 2.66491961 4.41530132 2.23532796
		 2.6633873 4.4126873 2.23293948 5.57977104 4.045561314 0.97986227 5.45457077 4.048832893 0.83012307
		 2.59089303 4.3568821 2.098622322 2.58793926 4.352211 2.093919516 4.055474758 2.79793262 -0.19106469
		 3.91516519 2.74338722 -0.22378507 2.52531457 4.25314951 1.99418986 2.52188611 4.24772692 1.98873067
		 2.35558009 4.29766369 2.095983028 2.35953188 4.30214119 2.10020423 2.43158889 4.38382578 2.17723083
		 2.42615342 4.42113066 2.15158868 2.49791336 4.49773121 2.23046184 2.50759268 4.46998358 2.25847459
		 2.58093786 4.55312777 2.33687758 2.5835979 4.55614185 2.33971977 2.72003555 4.56117153 2.30429149
		 2.71773529 4.55752468 2.30061793 6.033378124 5.44117212 2.2703824 5.94880486 5.34740305 2.17748356
		 2.65398836 4.45669174 2.19910574 2.65242147 4.45420742 2.19660401 5.55707979 4.13147974 0.90467197
		 4.048992157 2.78234434 -0.14087076 4.036617756 2.82919836 -0.18187466 3.8966279 2.7759378 -0.21678609
		 3.91037035 2.72390342 -0.17124893 3.92411304 2.67186856 -0.12571135 3.93785572 2.61983395 -0.080174409
		 4.073740959 2.68863606 -0.058863595 4.061366558 2.73549008 -0.099867083 5.54647017 4.071013927 0.97243327
		 5.53550434 4.11253452 0.93609756 5.52453852 4.15405464 0.89976192 5.42178345 4.073662281 0.82289571
		 5.43409348 4.027052402 0.86368561 5.44640303 3.98044252 0.90447545 5.45871305 3.93383265 0.94526535
		 5.55743551 4.029493809 1.0087690353 6.018995285 5.3401742 2.35016942 6.0094566345 5.37629128 2.31856203
		 5.99991798 5.41240835 2.28695488 5.99037933 5.44852543 2.25534749;
	setAttr ".vt[664:829]" 5.9053607 5.35723352 2.16009712 5.91629219 5.31584358 2.19631886
		 5.92722368 5.27445364 2.23254061 5.9381547 5.23306322 2.26876259 2.62559676 4.26249123 2.17582035
		 2.68457842 4.36995363 2.26480293 2.67361259 4.41147375 2.22846723 2.66264677 4.45299435 2.19213176
		 2.65168118 4.49451494 2.15579581 2.58866715 4.40232086 2.053451061 2.60097694 4.35571098 2.094240904
		 2.61328697 4.3091011 2.13503051 2.61941266 4.25126266 2.16562486 2.60703826 4.29811668 2.12462139
		 2.59466386 4.34497118 2.083617449 2.58228946 4.3918252 2.042613983 2.51795316 4.29813719 1.93867433
		 2.5316956 4.24610233 1.98421133 2.54543829 4.19406748 2.029748678 2.55918097 4.1420331 2.075285912
		 2.68909621 4.37740135 2.27154016 2.7501359 4.48876047 2.36377764 2.74059725 4.52487803 2.33217001
		 2.73105836 4.5609951 2.30056286 2.72151971 4.59711218 2.26895571 2.65630221 4.50157166 2.1628747
		 2.66723347 4.46018171 2.19909644 2.67816496 4.41879129 2.23531866 4.84031105 6.39173317 3.87619495
		 4.83195162 6.42290878 3.85315156 4.8195734 6.47025299 3.80747938 4.86316729 6.41187429 3.72761536
		 4.91817284 6.41766787 3.77882504 4.88390493 6.33335495 3.79633045 4.82662344 6.44308376 3.83549547
		 4.92350101 6.39749289 3.79648066 4.25308084 3.69306087 0.93633896 2.61038613 4.3039999 2.13019609
		 2.5980463 4.35072279 2.08930707 4.24074078 3.739784 0.89544988 4.026762009 3.19451594 0.43504056
		 2.60843349 4.30056667 2.12694287 2.59607339 4.34736681 2.085986853 4.014401913 3.24131584 0.3940846
		 4.73943377 3.94092155 1.072724342 2.61197901 4.30680275 2.13285255 2.59965539 4.35346365 2.092017889
		 4.72711039 3.98758245 1.031889558 4.098415852 3.45317531 0.70894367 2.60936236 4.30220652 2.12849808
		 2.59701204 4.34896994 2.087574005 4.086065769 3.49993849 0.66801977 4.4821353 3.83662891 1.03123188
		 2.61122274 4.30546999 2.13158941 2.5988915 4.35216093 2.090728998 4.46980381 3.88331962 0.99037135
		 4.98720837 3.99364591 1.058172345 2.6125524 4.30781794 2.13381624 2.60023499 4.35445642 2.093001366
		 4.97489119 4.040284157 1.017357469 4.044149399 2.96382332 0.16621314 2.60772729 4.29933119 2.12577271
		 2.59536004 4.3461585 2.084792376 4.03178215 3.010650396 0.12523332 5.2387085 4.70480299 1.78946388
		 2.67590356 4.41516399 2.23192334 2.66495514 4.45661831 2.19564509 5.22775984 4.74625778 1.75318587
		 5.42695236 5.068592072 2.14830518 2.67741394 4.41758442 2.23418856 2.66647696 4.45899582 2.19794774
		 5.41601563 5.11000395 2.11206436 5.31344318 4.89696407 1.98646653 2.67666292 4.41637754 2.23305821
		 2.66572022 4.4578104 2.1967988 5.30250025 4.93839741 1.95020711 5.23723364 4.46896935 1.52044868
		 2.67514515 4.41393805 2.23077488 2.66419101 4.45541477 2.19447708 5.22627926 4.51044607 1.48415112
		 5.3291254 4.29307127 1.29174352 2.67437935 4.41270685 2.22962141 2.66341949 4.45420504 2.19330478
		 5.3181653 4.33456945 1.25542712 5.1166482 5.34406567 2.54839015 5.10781145 5.37752581 2.51910853
		 2.72922182 4.56102467 2.30118418 2.73875213 4.52493954 2.33276367 4.78764105 5.39384651 2.7017777
		 4.77950621 5.42464924 2.67482138 2.72737789 4.56104898 2.30180168 2.73689985 4.52499533 2.33335304
		 4.45950985 5.5494566 2.9758296 4.45207644 5.57760239 2.95119882 2.72554994 4.5610857 2.30242848
		 2.73506331 4.52506399 2.33395243 4.35952616 5.68044567 3.15289617 4.35279417 5.70593452 3.13058949
		 2.7237072 4.56111002 2.30304646 2.73321223 4.52512026 2.3345418 4.32698584 5.81624937 3.31511021
		 4.32095575 5.83908081 3.29512978 2.72187757 4.56114626 2.30367303 2.73137426 4.52518845 2.33514118
		 0.48681563 1.95808172 0.14808866 0.48441821 1.96012747 0.022597253 0.48684812 1.95806491 -0.14808859
		 0.88205248 1.7210896 -0.14808859 0.86927813 1.71902215 -0.022597253 0.88195252 1.72112405 0.14808866
		 0.86917847 1.71905649 0.022597253 0.48445103 1.96011031 -0.022597253 0.49171072 2.027803421 0.3482722
		 0.85514534 1.9045099 0.097706243 0.81824154 1.99427974 0.017933398 0.80947822 2.027800322 -0.011459398
		 0.79765636 2.12252283 -0.09314207 0.43415818 2.24582291 0.15745032 0.45637822 2.15472436 0.24013618
		 0.46520478 2.12119794 0.26950312 3.74141002 2.83143687 0.10852858 2.54381394 4.19428921 2.030541897
		 2.53005886 4.24637127 1.9849633 3.72849751 2.8803854 0.065680996 3.37561679 2.84204769 0.22781189
		 2.54219174 4.19451332 2.031337023 2.52842426 4.24664211 1.98571682 3.36353397 2.88791013 0.18765494
		 2.90580773 2.61382008 0.10556956 2.54057097 4.19473791 2.032132387 2.52679086 4.24691439 1.98647094
		 2.89455557 2.65659809 0.068104409 2.66198945 2.35592628 -0.11877334 2.53894615 4.19495964 2.032925606
		 2.52515364 4.24718332 1.98722279 2.65156674 2.39561844 -0.15354787 2.46055841 1.31892467 -0.62707216
		 2.53732395 4.19518375 2.03372097 2.52351904 4.24745464 1.98797667 2.46063185 1.35676765 -0.65915418
		 2.17943573 1.88103139 -0.5195713 2.53705454 4.19522142 2.033853292 2.52324748 4.24749994 1.9881022
		 2.169981 1.91712427 -0.55120605 1.90719748 1.90368187 -0.41206998 2.53678322 4.19525814 2.033985376
		 2.52297378 4.24754477 1.98822725 1.89787984 1.93925905 -0.443257 1.63495851 1.92633116 -0.30456933
		 2.53651166 4.19529438 2.034117699 2.52270031 4.24758911 1.98835278 1.62577951 1.96139526 -0.33530653
		 1.36271846 1.94898009 -0.19706865 2.53623581 4.19532681 2.034245729 2.52242255 4.24762917 1.98847437
		 1.35367882 1.98352945 -0.22735831 1.090480447 1.97163057 -0.089566991 2.53597069 4.19536829 2.034381628
		 2.52215528 4.24767876 1.98860323 1.081579447 2.0056657791 -0.11940818 2.34065056 3.82093668 1.66236055
		 2.29505229 3.82474303 1.68038821 2.24945927 3.82855296 1.69841838 2.26711845 3.7699883 1.74986529
		 2.15205789 3.7876327 1.80475092 2.1361258 3.84679699 1.75346828;
	setAttr ".vt[830:995]" 2.12313986 3.89598322 1.7104193 2.10792232 3.95476508 1.65848398
		 2.22299361 3.93711948 1.60359359 2.23648357 3.87773824 1.65536487 2.28205538 3.87400746 1.63726592
		 2.32763267 3.87028003 1.61916959 2.37320995 3.86655331 1.60107315 2.418787 3.86282635 1.58297658
		 2.46436858 3.8591032 1.56488347 2.50994611 3.85537672 1.54678762 2.54622197 3.9385891 1.63042772
		 2.58808398 3.98186088 1.6667428 2.66761065 4.020188332 1.68604064 2.72980118 4.018708706 1.66508436
		 2.76147437 3.99240232 1.62496793 2.74773192 4.044436932 1.5794307 2.8246839 4.13139248 1.67186987
		 2.83705831 4.08453846 1.71287358 2.83476901 4.12357759 1.75820231 2.8324604 4.16302347 1.80400181
		 2.84518743 4.20746422 1.85098076 2.87182689 4.24889851 1.8903296 2.91070867 4.27401924 1.9073348
		 2.95422411 4.29247761 1.91532576 2.99600983 4.30209303 1.91372621 3.073161364 4.30093336 1.88914716
		 3.060851574 4.34754324 1.84835768 3.13049078 4.43777132 1.94645631 3.14145637 4.39625072 1.98279226
		 3.10587502 4.43426371 2.036990404 3.091197491 4.46458006 2.076084614 3.092095852 4.50489712 2.12190604
		 3.1051805 4.53790569 2.15569806 3.12472701 4.56749201 2.18362951 3.20874286 4.60279131 2.19863319
		 3.19781137 4.64418173 2.16241121 3.26632714 4.73901224 2.26668549 3.27586579 4.70289516 2.29829311
		 3.12565112 4.69710636 2.33750415 3.069405317 4.70498753 2.36397552 3.013298273 4.73049974 2.4105525
		 2.99522758 4.75191879 2.44097376 2.9883914 4.77413559 2.46891594 3.027307272 4.89037514 2.59048843
		 3.01879549 4.92631483 2.56112981 2.89782667 4.93184948 2.60396147 2.90690398 4.8973794 2.63482904
		 2.91569877 4.864079 2.6639719 2.92500353 4.82894754 2.69401455 3.045972347 4.8234129 2.65118289
		 3.036102057 4.85707474 2.61963081 2.9973104 4.74036551 2.49846935 3.0042703152 4.7176795 2.47093773
		 3.022464991 4.69579077 2.44092751 3.078696251 4.66980886 2.39476156 3.13506579 4.66145897 2.36870027
		 3.28540444 4.66677809 2.3299005 3.29494309 4.63066101 2.36150765 3.2306056 4.52001143 2.27107716
		 3.21967411 4.56140137 2.23485518 3.13566422 4.52608013 2.21987033 3.1161232 4.49647236 2.19195771
		 3.10304427 4.4634428 2.15818429 3.10215163 4.42310381 2.11238194 3.11683488 4.39276552 2.073306561
		 3.15242219 4.35473061 2.019127846 3.16338778 4.31321001 2.055463552 3.097781181 4.2077136 1.97072697
		 3.085471392 4.25432348 1.92993701 3.0083272457 4.25545454 1.95454085 2.96654749 4.24581671 1.95616007
		 2.92303991 4.22732878 1.94819534 2.88416672 4.20217562 1.93121839 2.85753775 4.16070127 1.89190495
		 2.8448205 4.11622381 1.84495783 2.84713626 4.076750755 1.79918242 2.84943271 4.037684441 1.75387704
		 2.86180711 3.99083042 1.79488051 2.78895974 3.88833284 1.7160424 2.77521706 3.94036746 1.67050517
		 2.74341226 3.9671464 1.71020579 2.68109441 3.96910143 1.73074865 2.60144091 3.93124914 1.71103704
		 2.55945563 3.88845611 1.6743108 2.5230484 3.80571628 1.5902549 2.47744536 3.8095181 1.60827887
		 2.4318471 3.81332445 1.6263063 2.38624883 3.81713033 1.64433312 2.14506364 3.44654465 1.29047394
		 2.054142237 3.4541235 1.32639956 1.96321523 3.46169758 1.36232102 1.98472357 3.39689207 1.419433
		 1.8677907 3.41035938 1.47011077 1.84974349 3.47636986 1.41333091 1.83759642 3.52242947 1.37302244
		 1.82097161 3.58766937 1.31493294 1.93792558 3.57419968 1.26424563 1.95108151 3.5077498 1.32199967
		 2.041964293 3.50034189 1.2859329 2.13284636 3.49293423 1.2498672 2.22372317 3.48552108 1.21379626
		 2.31460524 3.47811294 1.17772961 2.40548682 3.47070432 1.14166284 2.49636865 3.46329546 1.10559583
		 2.56729317 3.62999678 1.27363372 2.64937973 3.71680951 1.34701598 2.80679321 3.79373074 1.38636231
		 2.92953897 3.79104352 1.34520352 2.99125051 3.73870015 1.26572263 2.97750783 3.79073453 1.2201854
		 3.067070484 3.87095356 1.30112088 3.079444885 3.82409954 1.34212458 3.074172974 3.90099359 1.43160939
		 3.068847179 3.97868061 1.52201748 3.0933671 4.065962315 1.61439061 3.14560342 4.14707088 1.69134974
		 3.22252369 4.19587612 1.72393966 3.30880761 4.23150396 1.73864233 3.39178419 4.24973011 1.73445165
		 3.5453403 4.24615097 1.68405044 3.53303051 4.29276085 1.64326048 3.60930014 4.3810277 1.73711717
		 3.62026596 4.33950758 1.77345288 3.54832625 4.41432047 1.88067365 3.51822042 4.4737587 1.95770168
		 3.51922894 4.55316925 2.048162222 3.54464412 4.61800385 2.11460042 3.58298945 4.67599916 2.16932011
		 3.75025582 4.74540472 2.19817233 3.73932433 4.78679466 2.16195059 3.81113815 4.88091564 2.26441836
		 3.8206768 4.84479856 2.29602623 3.52208471 4.83319139 2.37382579 3.41142607 4.84891987 2.42614436
		 3.30105948 4.89992428 2.51868463 3.26674128 4.94272232 2.57889748 3.25490475 4.98712492 2.63415909
		 3.4248395 5.17178631 2.79483485 3.41730738 5.20774126 2.76755953 3.23509288 5.26445675 2.88735771
		 3.24376559 5.23144674 2.91765451 3.25186706 5.20077133 2.94449973 3.2609818 5.1664319 2.97314262
		 3.44319654 5.10971594 2.85334444 3.43294096 5.14111137 2.82168031 3.26324582 4.95554256 2.66179776
		 3.27532172 4.91023302 2.60733008 3.30987954 4.86652851 2.54791188 3.42048573 4.8146162 2.4561646
		 3.53138375 4.79798126 2.40463901 3.83021545 4.80868149 2.32763314 3.83975434 4.77256441 2.35924053
		 3.77211857 4.66262436 2.27061605 3.76118708 4.7040143 2.23439455 3.59392667 4.63458729 2.20556092
		 3.55558681 4.57657099 2.15085983 3.53017735 4.51171494 2.08444047 3.52917457 4.43228197 1.99399948
		 3.55928636 4.37282181 1.91699052 3.63123155 4.29798698 1.80978906 3.64219737 4.25646687 1.84612477
		 3.56996012 4.15293169 1.76563048 3.55765033 4.19954157 1.72484052 3.40410161 4.20309162 1.77526677
		 3.32113099 4.18484259 1.77947664 3.23485494 4.14918566 1.76479995;
	setAttr ".vt[996:1161]" 3.15794325 4.10034704 1.73223901 3.10571742 4.019198895 1.65531528
		 3.081207037 3.93188119 1.56297326 3.086540222 3.85416651 1.47258973 3.091819286 3.77724528 1.38312829
		 3.10419369 3.73039103 1.42413175 3.018735647 3.63463044 1.35679698 3.0049929619 3.6866653 1.31125987
		 2.9430151 3.74000669 1.38987172 2.81999779 3.74368906 1.43016028 2.66231799 3.66776633 1.38994563
		 2.57995629 3.58194518 1.31569004 2.50877666 3.41625214 1.14679134 2.41784573 3.42382312 1.18271029
		 2.32691622 3.43139577 1.21863067 2.23599005 3.43897009 1.25455225 1.94947875 3.07215476 0.91858917
		 1.81322336 3.083498001 0.97240621 1.67697001 3.094841957 1.026223063 1.70232975 3.023797512 1.089002132
		 1.5237726 3.064721823 1.1896522 1.50361037 3.13757801 1.12737465 1.49228847 3.18050051 1.089798331
		 1.47426963 3.25220871 1.025562406 1.65285981 3.21128201 0.9249 1.66567886 3.13776135 0.98863375
		 1.80186594 3.12667131 0.93459666 1.93805158 3.11557984 0.88055825 2.074237108 3.10448956 0.82652056
		 2.21042895 3.093403339 0.77248627 2.34661031 3.082308769 0.71844578 2.48279572 3.071218491 0.66440809
		 2.58835649 3.32139802 0.91683531 2.71066833 3.45175219 1.027284622 2.94597316 3.56727147 1.086681962
		 3.12928081 3.56338072 1.025323987 3.22103286 3.48500252 0.90648103 3.20729041 3.53703737 0.86094373
		 3.30945683 3.61051464 0.93037194 3.32183123 3.56366062 0.97137558 3.31357718 3.67840934 1.10501611
		 3.30523801 3.79434085 1.24003482 3.34154272 3.92445707 1.37779927 3.41938734 4.045248032 1.49237394
		 3.53435016 4.11774206 1.54055119 3.66338301 4.17052317 1.56195366 3.78755856 4.19736671 1.55517662
		 4.01753521 4.19138193 1.47896349 4.0052251816 4.23799181 1.43817353 4.088106632 4.32428169 1.5277766
		 4.099072456 4.28276157 1.56411254 3.99079037 4.39438581 1.72436452 3.94523597 4.48293114 1.83931422
		 3.94636631 4.6014452 1.97442091 3.98410845 4.69810247 2.073502064 4.041236877 4.78449202 2.15500045
		 4.29175949 4.8880105 2.19770622 4.280828 4.92940044 2.16148424 4.35594893 5.022819042 2.26215148
		 4.36548758 4.98670149 2.29375958 3.91851664 4.969275 2.41014695 3.75344253 4.99284935 2.48831129
		 3.58881164 5.069342136 2.62681246 3.53825855 5.13352823 2.71682262 3.52141881 5.20011473 2.79940128
		 3.7802279 5.47549915 3.037383318 3.77367973 5.5114727 3.01219368 3.65848565 5.55147028 3.092661619
		 3.66674304 5.51991367 3.12238264 3.67415142 5.49186277 3.14693046 3.68308663 5.45832157 3.17417908
		 3.79828072 5.41832399 3.093711615 3.78763604 5.44744921 3.061931372 3.52918196 5.1707201 2.82512593
		 3.54637671 5.10278893 2.74372387 3.59728503 5.037258625 2.65488958 3.76227117 4.95942116 2.51756549
		 3.92770028 4.9345026 2.44057775 4.37502623 4.95058441 2.32536602 4.38456488 4.91446781 2.35697341
		 4.313622 4.80523014 2.27015018 4.30269098 4.84662008 2.23392797 4.052173615 4.74308062 2.19124079
		 3.99505115 4.65666914 2.10976124 3.95731449 4.55999088 2.010699272 3.95619011 4.44145441 1.87561178
		 4.0017504692 4.35288715 1.76068115 4.11003828 4.24124146 1.60044801 4.12100363 4.19972086 1.63678396
		 4.042154789 4.098162174 1.5605433 4.029844761 4.14477205 1.51975334 3.79987597 4.15072823 1.59599173
		 3.67570639 4.12386227 1.60278869 3.5466814 4.071051121 1.58141196 3.43172717 3.99852467 1.53326321
		 3.35389304 3.87769413 1.41872275 3.3175981 3.74754119 1.28099084 3.32594442 3.63158202 1.14599621
		 3.33420563 3.51680636 1.01237905 3.34658027 3.46995211 1.053382516 3.24851823 3.38093328 0.99755549
		 3.23477554 3.4329679 0.9520182 3.14261246 3.51286387 1.069535971 2.95889783 3.51827502 1.12956989
		 2.72318459 3.40427542 1.068848252 2.6004653 3.27544117 0.95707446 2.49450111 3.026784658 0.70332599
		 2.35824108 3.038123131 0.75713831 2.22198629 3.049466372 0.81095499 2.085731506 3.060810328 0.86477232
		 1.75389159 2.69776297 0.54670304 1.57230914 2.71287537 0.61841458 1.3907268 2.72798753 0.69012636
		 1.41993511 2.65070152 0.75856966 1.20852184 2.70385146 0.88310552 1.18624544 2.7835536 0.81533098
		 1.17575514 2.82334423 0.78049123 1.15633631 2.9015162 0.71010572 1.36779273 2.84836268 0.58555293
		 1.38027871 2.76777411 0.65526944 1.5617702 2.75300241 0.58326137 1.74326122 2.73823023 0.51125282
		 1.92475259 2.72345901 0.43924525 2.10624313 2.70868635 0.36723652 2.28773451 2.69391537 0.29522955
		 2.46922588 2.67914271 0.2232209 2.60943341 3.012810469 0.56004518 2.77196908 3.18670535 0.70756143
		 3.085157871 3.34081602 0.78700501 3.32901573 3.33571219 0.70544058 3.45080423 3.23129725 0.54723364
		 3.43706179 3.28333187 0.50169635 3.55184698 3.35007834 0.55962515 3.56422138 3.30322433 0.60062879
		 3.55297995 3.45582414 0.77842301 3.54162407 3.60999846 0.95805115 3.58971381 3.78294921 1.14120448
		 3.69317579 3.94343019 1.29340196 3.84617138 4.039604187 1.35716009 4.017957687 4.10954285 1.38526618
		 4.1833334 4.14500427 1.3759023 4.48971939 4.13660383 1.27386987 4.47740936 4.18321371 1.23307967
		 4.56691599 4.26753855 1.31843746 4.57788181 4.22601795 1.3547734 4.43324184 4.37444162 1.56804752
		 4.3722496 4.49210262 1.72092652 4.37349892 4.64971685 1.90067613 4.42357206 4.77820063 2.032403708
		 4.49950218 4.89300108 2.140692 4.83327675 5.030626774 2.19724774 4.82234526 5.072016716 2.161026
		 4.90075636 5.16471863 2.25988173 4.91029501 5.12860155 2.29148889 4.31494522 5.10535717 2.44646597
		 4.095462322 5.13678217 2.55048084 3.87656736 5.23876333 2.73494124 3.80976462 5.32432508 2.85474133
		 3.78792834 5.4131012 2.96464324 4.098471642 5.79889631 3.31363511 4.092891693 5.83487749 3.29052138
		 3.99557424 5.88417673 3.37622476 4.0034265518 5.85408115 3.40537667 4.010141373 5.82865572 3.42762709
		 4.01888752 5.79590416 3.45347524 4.11620522 5.74660444 3.36777163;
	setAttr ".vt[1162:1327]" 4.10518646 5.77347136 3.33588576 3.7951138 5.3858943 2.98845243
		 3.81742096 5.29533625 2.88011003 3.88469434 5.20799208 2.76187062 4.1040597 5.1042285 2.57896876
		 4.32401371 5.071021557 2.47651386 4.91983366 5.092484474 2.32309604 4.92937279 5.056367397 2.35470366
		 4.85513926 4.94784641 2.26969147 4.84420824 4.98923635 2.23346996 4.5104394 4.85158968 2.17693305
		 4.434515 4.73676729 2.06866312 4.3844471 4.60826206 1.93695438 4.38320398 4.4506259 1.75722408
		 4.44420195 4.33294296 1.6043644 4.58884716 4.18449783 1.39110887 4.59981298 4.14297771 1.42744458
		 4.51433945 4.043384075 1.35544944 4.50202942 4.089993954 1.31465971 4.19565105 4.098365784 1.41671717
		 4.030281067 4.062881947 1.42610097 3.85850263 3.99291325 1.39802086 3.70551562 3.89670682 1.33429098
		 3.60206413 3.73618603 1.18212855 3.55398417 3.56319857 0.99900717 3.56534719 3.40899706 0.81940299
		 3.57659578 3.25637007 0.64163238 3.58897018 3.20951605 0.68263584 3.4782896 3.12722778 0.63830817
		 3.46454692 3.17926264 0.59277093 3.34221172 3.28572083 0.74919927 3.097802162 3.29286385 0.82898265
		 2.78406525 3.14079523 0.7477591 2.62097216 2.96893549 0.59845787 2.4802227 2.63731503 0.25985804
		 2.29863954 2.652426 0.33156857 2.11705756 2.66753936 0.40328071 1.93547559 2.68265128 0.4749921
		 1.55830598 2.32337213 0.17481732 1.33139527 2.34225297 0.26442397 1.17840338 2.32200122 0.28700593
		 1.21145821 2.23847246 0.36111352 0.83883071 2.37180161 0.62592089 0.8144393 2.45834994 0.5526495
		 0.80478102 2.49500847 0.52054578 0.78396106 2.57964325 0.44401011 1.15664339 2.44631028 0.17918155
		 1.16879737 2.35865498 0.25488105 1.32167411 2.37933373 0.23192631 1.54847038 2.36088037 0.14194785
		 1.77526593 2.34242654 0.051968876 2.0020616055 2.32397318 -0.038009431 2.22885752 2.30551934 -0.12798858
		 2.45565391 2.28706598 -0.21796717 2.63049698 2.70421243 0.20324713 2.83326173 2.92165112 0.38783225
		 3.22434378 3.11436129 0.48732862 3.52875686 3.10804915 0.38556087 3.68058372 2.97759724 0.18798958
		 3.66684103 3.029631615 0.14245236 3.79422951 3.089636326 0.18887381 3.80660391 3.042782068 0.22987747
		 3.79238343 3.23323941 0.45182952 3.77801132 3.4256556 0.67606682 3.83788943 3.64144373 0.90461218
		 3.96696019 3.84160876 1.09442687 4.15799236 3.9614656 1.17376828 4.37252665 4.048556328 1.20857227
		 4.57911396 4.092645645 1.1966306 4.96190453 4.081827164 1.068776965 4.94959497 4.12843704 1.027986765
		 5.04572916 4.21079826 1.10910022 5.056694984 4.16927767 1.14543617 4.87570572 4.35450792 1.41173935
		 4.79926491 4.50127459 1.6025393 4.800632 4.69798946 1.82693279 4.86303663 4.85829878 1.99130523
		 4.95774937 5.0014953613 2.12637305 5.37478971 5.17323971 2.19678688 5.3638587 5.21462965 2.16056538
		 5.44556761 5.30662251 2.25761485 5.45510626 5.27050543 2.28922224 4.71136999 5.2414341 2.48278165
		 4.43747711 5.28071022 2.61264706 4.16432095 5.40818119 2.84306884 4.081288815 5.51513767 2.99267173
		 4.054442406 5.626091 3.12988615 4.37235451 6.14576674 3.6300962 4.39891052 6.16525602 3.58080173
		 4.33985138 6.21307325 3.65326476 4.34728861 6.18442917 3.68183994 4.35331011 6.16162968 3.70179296
		 4.36187696 6.1296773 3.72624683 4.42093563 6.081860065 3.65378404 4.37837601 6.12296677 3.65004945
		 4.061049938 5.60107136 3.15178132 4.088482857 5.48789883 3.016509295 4.17210102 5.37872314 2.86884856
		 4.4458437 5.24903202 2.64036965 4.72032261 5.2075367 2.51244593 5.46464491 5.23438835 2.32082939
		 5.47418356 5.1982708 2.35243678 5.3966527 5.090459824 2.26923084 5.38572121 5.13184977 2.2330091
		 4.9686861 4.96008396 2.16261387 4.87397909 4.81686592 2.027564764 4.81158018 4.65653515 1.86321104
		 4.81021929 4.45979786 1.63883662 4.88666582 4.31300926 1.44805598 5.067660809 4.12775755 1.18177211
		 5.078626633 4.086236954 1.21810782 4.98652458 3.98860741 1.15035653 4.97421455 4.035217285 1.10956657
		 4.59143162 4.046007156 1.23744535 4.38485003 4.0018949509 1.2494067 4.17032385 3.91477489 1.21462905
		 3.97930002 3.79488564 1.13531625 3.85023999 3.59468055 0.9455362 3.79037142 3.37885571 0.71702278
		 3.80475068 3.1864121 0.49280941 3.81897831 2.99592805 0.27088094 3.83135271 2.94907379 0.31188443
		 3.70806885 2.87352753 0.27906403 3.6943264 2.92556214 0.23352692 3.54181027 3.058578014 0.4288632
		 3.23671007 3.067456245 0.52839756 2.84493852 2.87730908 0.42666543 2.64148116 2.66243148 0.2398425
		 2.46594906 2.24784875 -0.18360682 2.23903775 2.26672983 -0.094000451 2.012126446 2.28561044 -0.0043943767
		 1.78521609 2.3044908 0.085211009 4.83399153 6.38634682 3.87203312 4.82561874 6.41752768 3.8489604
		 4.82027912 6.43774652 3.83126497 4.81323242 6.46494913 3.80324554 4.85708857 6.40674973 3.7235074
		 4.91210651 6.41253996 3.77474952 4.3164382 5.83546972 3.29232836 4.34819365 5.70270109 3.12825251
		 4.44719124 5.57472324 2.94936085 4.77369928 5.42220116 2.67376089 5.10109234 5.37521935 2.5184927
		 5.9906764 5.40999651 2.28698754 5.98113775 5.44611359 2.25538015 5.8961854 5.35481787 2.16010571
		 5.90711689 5.31342793 2.19632769 5.40824699 5.10816288 2.1123054 5.29505205 4.93703842 1.95090234
		 5.22053003 4.74544621 1.75443995 5.21904135 4.51028872 1.48615623 5.31066465 4.33490467 1.25807381
		 5.52738714 4.11349344 0.93964279 5.51642132 4.15501356 0.90330708 5.41377258 4.074582577 0.82636482
		 5.42608261 4.027972698 0.86715466 4.96818256 4.04117012 1.020394683 4.72110081 3.98861504 1.034883261
		 4.46452665 3.88464928 0.99348319 4.2361002 3.74150825 0.89882046 4.081857681 3.50233436 0.67202699
		 4.010393143 3.2444365 0.39886039 4.027725697 3.014422178 0.13076729 4.04488945 2.78676033 -0.1345858
		 4.032515049 2.83361483 -0.17558944 3.89272714 2.78023076 -0.21070346;
	setAttr ".vt[1328:1493]" 3.90646982 2.72819638 -0.16516633 3.72511339 2.88424277 0.071101315
		 3.3611691 2.89174223 0.19272906 2.89351559 2.66108847 0.073521286 2.65121007 2.40084791 -0.14750166
		 2.46255064 1.36313128 -0.65167987 2.17097783 1.92370522 -0.54403496 1.89964545 1.94577825 -0.43638977
		 1.62831211 1.96785128 -0.3287445 1.3569802 1.98992383 -0.22110048 1.085647464 2.011997223 -0.11345538
		 0.78258973 2.053794146 0.023715043 0.76989359 2.15534592 -0.06385801 0.38029435 2.28747916 0.20470528
		 0.40412068 2.18980742 0.29335228 0.41361061 2.15376091 0.32492614 0.44202504 2.053631544 0.40938085
		 0.83155602 1.92150486 0.14084567 0.79201186 2.017754316 0.055316456 1.094563007 1.9779104 -0.083568878
		 1.36603343 1.95532441 -0.19076651 1.63750386 1.93273854 -0.2979649 1.90897489 1.91015279 -0.40516257
		 2.18044519 1.8875674 -0.51235992 2.46247864 1.325243 -0.61955792 2.6616416 2.36112022 -0.11269677
		 2.90477633 2.61828542 0.111011 3.37325716 2.84586167 0.23290065 3.73802853 2.83528566 0.11395629
		 3.92021251 2.67616153 -0.11962903 3.93395495 2.62412691 -0.074091785 4.069638252 2.69305253 -0.052578673
		 4.057263851 2.73990631 -0.09358222 4.040092945 2.9675951 0.17174713 4.022753239 3.19763684 0.43981642
		 4.094208241 3.45557117 0.71295124 4.24844027 3.69478464 0.9397096 4.47685814 3.83795857 1.034343719
		 4.73342419 3.94195414 1.075717807 4.98049974 3.99453187 1.06120944 5.43839264 3.98136282 0.90794456
		 5.45070219 3.93475294 0.94873434 5.54931879 4.030452728 1.01231432 5.53835297 4.071972847 0.97597855
		 5.32162476 4.29340649 1.2943902 5.22999573 4.46881247 1.52245378 5.23147869 4.70399141 1.79071844
		 5.30599499 4.89560556 1.98716199 5.41918373 5.06675148 2.14854622 5.9180479 5.27203751 2.23254967
		 5.9289794 5.23064756 2.26877165 6.0097537041 5.33776188 2.35020232 6.00021505356 5.37387943 2.31859446
		 5.10993099 5.34175205 2.54778171 4.78183842 5.39138317 2.70073009 4.45463037 5.54655504 2.97401118
		 4.35493326 5.6771822 3.15058446 4.32247782 5.81260109 3.31234121 4.91744661 6.39232111 3.79244328
		 4.87784767 6.32814741 3.79229498 0.6477465 2.27744675 0.58586603 1.070846677 2.12115455 0.27958661
		 1.035389185 2.20939088 0.20125137 1.24373674 2.20739961 0.13562174 1.48713946 2.18714762 0.039505355
		 1.73054397 2.16689587 -0.05661203 1.97394717 2.1466434 -0.1527293 2.21735191 2.12639141 -0.2488469
		 2.46075535 2.10613918 -0.34496424 2.64894366 2.55090857 0.10935899 2.8670814 2.78143358 0.30982995
		 3.28724694 2.98543549 0.41902417 3.61443949 2.97593474 0.31230983 3.77793288 2.83325338 0.10281517
		 3.79167557 2.78121853 0.14835277 3.91954732 2.85431218 0.17698592 3.90717292 2.9011662 0.13598216
		 3.8918581 3.10542274 0.37397641 3.8763814 3.31178164 0.61442107 3.94053769 3.54319191 0.85944998
		 4.078908443 3.75782967 1.062912703 4.28377867 3.88634181 1.14789963 4.51387119 3.97971272 1.18512273
		 4.73543501 4.026954174 1.1722151 5.1460228 4.015287399 1.034943461 5.15833282 3.96867752 1.075733423
		 5.25284004 4.065588951 1.14193797 5.24187422 4.10710907 1.10560215 5.047657013 4.30575418 1.39118052
		 4.96558809 4.46313334 1.59575939 4.96699762 4.67410135 1.8363806 5.033870697 4.84600306 2.012605906
		 5.13543415 4.99956942 2.15741086 5.5827508 5.18373871 2.23284006 5.59368229 5.14234829 2.26906204
		 5.67240763 5.24989653 2.35160637 5.6628685 5.2860136 2.31999922 4.86453104 5.25721693 2.52552772
		 4.57021093 5.30172539 2.662714 4.27666807 5.44083786 2.9077692 4.18710613 5.55796003 3.066135883
		 4.15781403 5.67936659 3.2112112 4.5778923 6.22265625 3.70274901 4.59005117 6.1730175 3.70505047
		 4.53661156 6.22467184 3.78020239 4.52813244 6.25635052 3.7562685 4.52236271 6.27819538 3.73715138
		 4.51505518 6.30629349 3.70877242 4.56849432 6.25463915 3.63362074 4.57212305 6.24450064 3.6836319
		 4.1514163 5.70359039 3.19001198 4.18008089 5.58456182 3.042856216 4.26901388 5.46981859 2.88240719
		 4.56192827 5.33308506 2.63527036 4.85562038 5.29095554 2.4960022 5.65332985 5.32213068 2.28839207
		 5.6437912 5.35824776 2.25678468 5.56088829 5.26651859 2.16039658 5.57181978 5.22512817 2.19661832
		 5.12449741 5.040980816 2.12116957 5.022928238 4.88743591 1.97634661 4.95604944 4.71555614 1.80010235
		 4.95463419 4.50460958 1.55946183 5.036696911 4.34725285 1.35486412 5.23090839 4.14862919 1.0692662
		 5.21994305 4.19014978 1.032930255 5.12140322 4.10850716 0.95336372 5.13371325 4.061897278 0.99415386
		 4.72311783 4.073592663 1.13140047 4.50154781 4.026373863 1.14428806 4.27144718 3.93303275 1.10703909
		 4.066568375 3.80455303 1.022023439 3.92818737 3.58995509 0.81852591 3.8640213 3.3585813 0.57346511
		 3.87949061 3.15225005 0.33299628 3.89479828 2.94802046 0.094978474 3.88242412 2.99487448 0.053975061
		 3.75044775 2.93732262 0.011740834 3.7641902 2.885288 0.057278078 3.60143328 3.025212765 0.26917154
		 3.27498531 3.031963825 0.37828928 2.85556102 2.82520819 0.27149615 2.63816381 2.59192944 0.073426001
		 2.45071554 2.1444068 -0.37849578 2.20743489 2.16419983 -0.28197828 1.96415532 2.18399334 -0.18546073
		 1.72087491 2.20378637 -0.08894369 1.47759438 2.22357917 0.00757402 1.23431444 2.24337244 0.10409151
		 1.025851488 2.24581766 0.16932049 1.013497114 2.33861661 0.089226171 0.5903365 2.49491334 0.39552888
		 0.61226946 2.40545321 0.47654733 0.62186509 2.36902094 0.50845426 2.39091182 4.43099785 2.20671058
		 2.44265318 4.4862299 2.26358151 2.4125278 4.34915161 2.278337 2.46190882 4.41332102 2.32738614
		 2.27662325 4.46387196 2.27876663 2.32836509 4.51910353 2.33563662 2.29823923 4.38202572 2.35039258
		 2.34762049 4.44619465 2.39944148 2.28452587 4.46135616 2.28495169 2.32936811 4.50922394 2.33423996
		 2.30325961 4.39042282 2.34702778 2.34605646 4.44603634 2.3895371;
	setAttr ".vt[1494:1659]" 2.28860474 4.45997047 2.29205585 2.32746935 4.50145531 2.33477187
		 2.30484056 4.39849472 2.345855 2.3419323 4.44669294 2.38269639 2.24140048 4.47456169 2.32297468
		 2.28026485 4.51604652 2.36569071 2.25763631 4.41308594 2.37677431 2.2947278 4.46128368 2.41361475
		 2.20986724 4.4813714 2.3402729 2.20022321 4.52263403 2.39737391 2.22610331 4.41989565 2.39407182
		 2.21468616 4.46787167 2.44529843 2.18828368 4.4744153 2.3388381 2.13534069 4.49263239 2.38267231
		 2.20451951 4.41294003 2.39263701 2.14980388 4.43786955 2.43059635 2.16830873 4.4580121 2.32612228
		 2.099706411 4.44162655 2.33514214 2.1845448 4.39653683 2.37992167 2.11416936 4.38686371 2.38306689
		 2.15173197 4.38742161 2.2967937 2.13526773 4.3771224 2.29197431 2.15630054 4.37012291 2.31193209
		 2.13933754 4.36171246 2.30545998 3.93467617 2.73604083 -0.20821883 3.95937443 2.64252472 -0.12637994
		 4.042248249 2.77785993 -0.18313266 4.064487457 2.69365382 -0.10944131 5.96083593 5.34880495 2.1961031
		 5.97792101 5.28411341 2.25271654 6.025675297 5.42069435 2.26732588 6.040584087 5.36424446 2.31672668
		 5.56501818 4.045060635 0.96271735 5.48856068 3.97362351 0.91262937 5.46903133 4.047568798 0.8479172
		 5.54762173 4.11093187 0.90507144 3.95747542 2.71594572 -0.22008099 3.9776454 2.63957429 -0.15324588
		 4.045326233 2.75009847 -0.19959366 4.063488007 2.68133044 -0.13941266 5.99167967 5.342978 2.19257545
		 6.0056328773 5.29014683 2.23880935 6.044631004 5.40168667 2.2507391 6.056806087 5.35558605 2.29108357
		 5.57665348 4.034977436 0.93501842 5.51421356 3.97663665 0.89411247 5.49826479 4.037024975 0.84126431
		 5.56244612 4.08877182 0.88794112 3.97129583 2.70088792 -0.23478357 3.98978496 2.63088059 -0.17351793
		 4.051825047 2.73219275 -0.21600527 4.068473816 2.6691556 -0.16083916 6.014571667 5.33757877 2.18411398
		 6.027362347 5.28914976 2.2264955 6.063108921 5.39139509 2.23743176 6.074269772 5.34913588 2.27441382
		 5.59092569 4.026489735 0.91631097 5.53368855 3.97301126 0.87881517 5.51906872 4.028367519 0.8303709
		 5.57790232 4.075801373 0.87315673 4.035114765 2.63368773 -0.29656884 4.044975758 2.59635091 -0.26389417
		 4.078063488 2.65038371 -0.28655359 4.086942673 2.61676383 -0.25713176 6.11687136 5.31416702 2.15019059
		 6.12369299 5.28833818 2.17279434 6.14275742 5.34286785 2.17862558 6.14870977 5.32032967 2.19834924
		 5.6509552 3.98922729 0.8314814 5.62042999 3.96070576 0.8114838 5.61263227 3.99022913 0.7856468
		 5.64400959 4.015526772 0.80846572 -2.95837092 3.62891817 2.17962909 -2.95520973 3.79083562 2.041815042
		 -3.10497403 3.70267987 1.9348067 -3.10813546 3.5407629 2.072620153 -3.24009609 3.86053991 2.39389491
		 -3.23792529 3.9717257 2.29926085 -3.37163234 3.94422913 2.26388788 -3.37380314 3.83304381 2.35852218
		 -3.14618874 3.7833333 2.32247353 -3.14368773 3.91142964 2.21344614 -3.28274918 3.86371398 2.15419531
		 -3.28525019 3.73561788 2.26322317 -3.052282572 3.70612764 2.25105333 -3.049451351 3.85113406 2.12763238
		 -3.19386387 3.78319788 2.044501781 -3.19669485 3.63819098 2.16792297 -2.96325326 3.63292909 2.18334055
		 -2.960109 3.79396915 2.046273708 -3.10958791 3.70685887 1.94049942 -3.11273193 3.54581928 2.077566385
		 -3.23680949 3.85783887 2.39139605 -3.23462701 3.96961617 2.29625845 -3.36853528 3.94141817 2.26005721
		 -3.37071753 3.82964134 2.35519457 -3.056482553 3.70957947 2.25424671 -3.053666115 3.85383034 2.13146949
		 -3.19783831 3.78679752 2.049405575 -3.20065475 3.64254665 2.17218328 -3.14395308 3.78149486 2.32077312
		 -3.14144421 3.90999413 2.21140242 -3.28063941 3.86179948 2.15158606 -3.28314829 3.73330021 2.26095676
		 -6.69337606 3.92709208 2.15042114 -6.6958766 3.79899573 2.25944901 -6.79766798 3.99580765 2.24529696
		 -6.79985046 3.8840301 2.34043503 -4.10446596 1.86923492 -0.24133867 -4.10761023 1.70819485 -0.10427279
		 -4.2536335 1.88735628 -0.20722479 -4.25646448 1.74235022 -0.083803907 -5.90725183 2.78075528 0.80525923
		 -5.91006804 2.63650465 0.92803687 -6.026341438 2.83454347 0.88168335 -6.028850079 2.70604467 0.99105382
		 -5.90963125 2.65887976 0.90899235 -3.19971585 3.6906302 2.1312573 -3.19575119 3.68652678 2.12678266
		 -4.25615501 1.75820017 -0.097294286 -4.10726643 1.72579777 -0.1192546 -3.11168385 3.59949899 2.031877518
		 -3.10708165 3.59473515 2.026682138 -2.95731711 3.68289042 2.13369131 -2.96220517 3.68660927 2.13765168
		 -3.051338673 3.75446296 2.20991302 -3.05621767 3.72315359 2.24269342 -3.14371705 3.79358649 2.31048155
		 -3.14535522 3.82603216 2.28613114 -3.23608184 3.89509797 2.35968328 -3.23937249 3.89760208 2.36235023
		 -3.37307954 3.8701055 2.32697725 -3.36999011 3.86690044 2.3234818 -6.79950094 3.90193963 2.32519174
		 -6.69547606 3.81951976 2.24197984 -3.28441668 3.7783165 2.22688055 -3.28231215 3.77613306 2.22449923
		 -6.028460979 2.72597671 0.97408897 -5.90768862 2.7583797 0.82430339 -3.1987772 3.73871398 2.090331793
		 -3.19480753 3.73486233 2.085641861 -4.25394297 1.8715055 -0.19373491 -4.10480976 1.85163248 -0.22635686
		 -3.11063576 3.65317917 1.98618853 -3.10602784 3.64870763 1.98074436 -2.9562633 3.73686314 2.087753296
		 -2.96115708 3.74028921 2.091962814 -3.050395012 3.80279827 2.1687727 -3.053931236 3.84025621 2.14302278
		 -3.14168024 3.89790201 2.22169423 -3.14452147 3.86873078 2.24978876 -3.23535442 3.93235707 2.32797122
		 -3.23864889 3.93466377 2.3308053 -3.37235594 3.90716743 2.29543281 -3.3692627 3.90415907 2.29176927
		 -6.7980175 3.97789764 2.26054072 -6.69377661 3.90656781 2.16789007 -3.28358293 3.82101536 2.19053769
		 -3.28147578 3.81896615 2.18804312 -6.026730537 2.81461167 0.89864838 -4.24395084 1.85803151 -0.14350481
		 -4.24300671 1.90636671 -0.18464531 -4.094487667 1.88765991 -0.21946275 -4.095535755 1.83397949 -0.17377405
		 -4.096583843 1.78029966 -0.12808473 -4.097631931 1.72662008 -0.082395919;
	setAttr ".vt[1660:1825]" -4.24583817 1.76136053 -0.061224252 -4.2448945 1.80969548 -0.10236505
		 -6.0021295547 2.75856352 0.96657068 -6.0012931824 2.80139661 0.93011379 -6.00045728683 2.8442297 0.89365685
		 -5.88170958 2.79024076 0.81698942 -5.88264847 2.74215698 0.85791504 -5.88358736 2.69407344 0.89884096
		 -5.88452625 2.64598989 0.939767 -6.0029659271 2.71573043 1.0030276775 -6.76018095 3.88339901 2.34060359
		 -6.7594533 3.92065835 2.30889106 -6.75872612 3.95791698 2.27717829 -6.75799847 3.99517655 2.24546576
		 -6.65391016 3.92635202 2.15045643 -6.65474367 3.88365293 2.18679905 -6.65557766 3.84095454 2.22314191
		 -6.65641117 3.79825568 2.25948429 -3.21017361 3.63901114 2.16781116 -3.29279423 3.72969007 2.25649309
		 -3.29195786 3.77252293 2.22003675 -3.29112148 3.81535578 2.18357992 -3.29028535 3.85818911 2.14712286
		 -3.20735717 3.78326178 2.045033455 -3.20829606 3.73517847 2.085959435 -3.20923471 3.68709469 2.1268847
		 -3.20152068 3.62953854 2.15764666 -3.20057678 3.67787457 2.1165061 -3.19963312 3.72620988 2.07536602
		 -3.19868946 3.77454567 2.034225464 -3.11412501 3.69847584 1.93054616 -3.11517286 3.64479589 1.97623503
		 -3.11622095 3.59111595 2.02192378 -3.11726904 3.53743625 2.067613125 -3.2989378 3.7358737 2.26320934
		 -3.38447595 3.82985973 2.35513592 -3.38374853 3.8671186 2.32342315 -3.38302112 3.9043777 2.29171062
		 -3.3822937 3.9416368 2.25999808 -3.29643679 3.86397004 2.15418243 -3.29727054 3.82127142 2.19052482
		 -3.29810429 3.77857232 2.22686696 -5.86316156 5.18798351 3.86275005 -5.86240959 5.22018623 3.83961606
		 -5.8615799 5.26898527 3.79380608 -5.8901639 5.20174217 3.71414661 -5.94493532 5.19449043 3.76535678
		 -5.89174557 5.12073946 3.78308988 -5.86200333 5.24099874 3.82190061 -5.94534159 5.1736784 3.78307152
		 -4.6567831 2.69736528 0.93096894 -3.20521498 3.68281269 2.12206507 -3.20427394 3.73101306 2.081039429
		 -4.65584183 2.74556565 0.8899433 -4.31946421 2.26525259 0.43109936 -3.2025094 3.67993069 2.11882091
		 -3.2015667 3.72821021 2.07772851 -4.3185215 2.31353235 0.39000696 -5.18759298 2.82342982 1.06684041
		 -3.20742249 3.68516541 2.12471342 -3.20648265 3.73330188 2.083742857 -5.18665314 2.87156653 1.025869608
		 -4.45002127 2.50031281 0.7042371 -3.20379829 3.68130803 2.12037134 -3.2028563 3.72955012 2.079311132
		 -4.44907951 2.54855371 0.6631763 -4.91309881 2.78288865 1.025536299 -3.20637417 3.68404651 2.12345409
		 -3.20543361 3.7322135 2.082457304 -4.91215849 2.83105564 0.98453963 -5.44068241 2.81597757 1.052255034
		 -3.20821905 3.68601894 2.12567425 -3.20727968 3.73413181 2.084723234 -5.43974304 2.86409044 1.011304259
		 -4.28198385 2.036304474 0.16299711 -3.20153236 3.67889428 2.11765409 -3.20058918 3.72720194 2.076537609
		 -4.28104067 2.084611893 0.12188075 -5.8525691 3.44929624 1.78147233 -3.29505277 3.77557468 2.22348237
		 -3.29421782 3.81833982 2.18708301 -5.85173416 3.49206138 1.7450732 -6.12114716 3.75912452 2.13928246
		 -3.29709005 3.77757478 2.22574043 -3.29625607 3.82029533 2.18937898 -6.12031317 3.80184555 2.10292053
		 -5.97044706 3.618819 1.97791743 -3.29607654 3.77657676 2.2246139 -3.29524207 3.8193202 2.18823338
		 -5.9696126 3.66156197 1.94153655 -5.79555655 3.21981621 1.51318812 -3.29402709 3.77455997 2.22233677
		 -3.29319167 3.817348 2.18591833 -5.79472113 3.262604 1.47676957 -5.84335613 3.026576281 1.28507757
		 -3.29299331 3.77354169 2.22118688 -3.29215765 3.81635237 2.18474913 -5.84252024 3.069386721 1.24863994
		 -5.8846736 4.10124493 2.5383513 -5.88399982 4.13576317 2.50897169 -3.38124418 3.90484262 2.29233098
		 -3.38197088 3.86761665 2.32401586 -5.57684183 4.2278738 2.69141078 -5.57622147 4.25965118 2.66436481
		 -3.3794589 3.90530396 2.29294729 -3.38018513 3.86811042 2.32460403 -5.29480219 4.45742941 2.96480894
		 -5.29423523 4.48646593 2.94009519 -3.37769198 3.90577388 2.29357362 -3.37841749 3.86861372 2.32520247
		 -5.22855949 4.60881758 3.14141631 -5.22804594 4.63511276 3.11903572 -3.37590766 3.90623569 2.29419017
		 -3.37663269 3.86910796 2.32579064 -5.22895956 4.74888372 3.30319238 -5.22849989 4.7724371 3.28314495
		 -3.37413907 3.90670514 2.2948153 -3.37486339 3.86961031 2.32638836 -0.58695155 1.91717565 0.14808866
		 -0.58515596 1.91976571 0.022597253 -0.58697867 1.91715109 -0.14808859 -0.90852171 1.58708262 -0.14808859
		 -0.89564931 1.5883503 -0.022597253 -0.90843391 1.58714163 0.14808866 -0.89556181 1.58840895 0.022597253
		 -0.58518332 1.9197408 -0.022597253 -0.60946715 1.97199094 0.34606618 -0.92932248 1.75925314 0.096074164
		 -0.91657108 1.85524786 0.016033256 -0.91665769 1.88981187 -0.01345692 -0.92940879 1.98417878 -0.095410287
		 -0.60647327 2.19303703 0.15460801 -0.60771513 2.1034205 0.23755857 -0.60768813 2.068834543 0.26702362
		 -3.95673132 1.97918046 0.1055636 -3.11469507 3.59171796 2.022715807 -3.11364603 3.64544654 1.97698534
		 -3.95576048 2.029672623 0.062573567 -3.60393786 2.076370955 0.22462268 -3.11317229 3.5923214 2.023509026
		 -3.1121223 3.64609861 1.97773695 -3.60304284 2.12367678 0.18433213 -3.093825102 1.96550226 0.10284245
		 -3.11165118 3.59292555 2.024302959 -3.11060023 3.64675164 1.97848988 -3.093008041 2.0096218586 0.065252304
		 -2.79622602 1.77206039 -0.12082779 -3.11012506 3.59352779 2.025094986 -3.10907316 3.64740252 1.97923982
		 -2.79548478 1.81299293 -0.15571778 -2.46064425 1.30471015 -0.62769145 -3.10860229 3.59413123 2.025888443
		 -3.10754967 3.64805484 1.97999179 -2.46071768 1.34246254 -0.65988088 -2.20329952 1.39646542 -0.52040464
		 -3.10834956 3.59423161 2.026020527 -3.10729647 3.64816356 1.98011684 -2.20337439 1.43368697 -0.55214494
		 -1.94595444 1.48822284 -0.4131166 -3.10809445 3.59433198 2.026152372 -3.10704112 3.64827204 1.98024189
		 -1.94603026 1.52491164 -0.44440717 -1.68860865 1.57997918 -0.30582884 -3.10783958 3.59443235 2.026284218
		 -3.10678625 3.64838052 1.98036742 -1.68868744 1.61613762 -0.3366687;
	setAttr ".vt[1826:1991]" -1.43126154 1.67173469 -0.19854194 -3.10757923 3.59452939 2.026412249
		 -3.10652566 3.64848566 1.9804883 -1.43134379 1.70736134 -0.22893298 -1.17391694 1.76349163 -0.09125378
		 -3.10733151 3.59463286 2.026548147 -3.10627794 3.64859724 1.98061717 -1.17400181 1.79858685 -0.12119442
		 -2.82937312 3.27610397 1.65558803 -2.7859807 3.29064035 1.67357922 -2.742594 3.30517936 1.69157362
		 -2.74590254 3.24424529 1.74319243 -2.63830256 3.28876162 1.7979629 -2.63681293 3.34987044 1.74650729
		 -2.63582611 3.40061069 1.7033149 -2.63494086 3.46117711 1.65120828 -2.74255133 3.41665673 1.59643328
		 -2.7416172 3.35591626 1.6483767 -2.78500175 3.3414588 1.63031316 -2.82839298 3.32700419 1.612252
		 -2.87178397 3.31254959 1.59419143 -2.91517472 3.29809475 1.57613075 -2.95857072 3.28364301 1.55807257
		 -3.0019619465 3.26918864 1.54001296 -3.056808233 3.34166718 1.62341368 -3.10766721 3.37389612 1.65961647
		 -3.19394445 3.39236236 1.67883766 -3.25400138 3.37615204 1.65791821 -3.27856612 3.34299302 1.61790061
		 -3.27751803 3.39667296 1.57221127 -3.37274981 3.46318603 1.66441822 -3.37369347 3.41485071 1.70555866
		 -3.38067007 3.45343971 1.7507652 -3.38772416 3.49242949 1.79644072 -3.41055989 3.53271675 1.84328878
		 -3.44619966 3.56677127 1.88252258 -3.48988628 3.58201957 1.89946997 -3.53650284 3.58967495 1.90742588
		 -3.57935524 3.5891242 1.90581942 -3.65401983 3.56967402 1.88128459 -3.65308094 3.61775756 1.84035873
		 -3.74198055 3.68919563 1.93821204 -3.74281693 3.64636254 1.97466862 -3.71721506 3.69185662 2.028729677
		 -3.71010351 3.72488618 2.067722082 -3.72047782 3.76396394 2.11341882 -3.74096632 3.79302502 2.14711547
		 -3.76692486 3.8172164 2.1749649 -3.85684896 3.8316679 2.18990326 -3.85601521 3.87436652 2.15356064
		 -3.9449079 3.95055866 2.25757456 -3.94563532 3.91329932 2.28928709 -3.7983675 3.94332838 2.32843733
		 -3.74559331 3.96436548 2.35485435 -3.69710875 4.0025525093 2.40132308 -3.68460417 4.027718544 2.43166828
		 -3.68320012 4.050994396 2.45953774 -3.74839401 4.1550355 2.58077002 -3.74862075 4.19188452 2.55130672
		 -3.63242841 4.22600317 2.59405756 -3.63309693 4.19045115 2.62502432 -3.6337676 4.15609789 2.65426469
		 -3.63450098 4.11984777 2.68441057 -3.75069356 4.085729122 2.64165998 -3.74906492 4.12068176 2.61000991
		 -3.68388033 4.016156673 2.48919034 -3.68529367 3.99239612 2.46173167 -3.69780779 3.96674633 2.43179893
		 -3.74630189 3.9280746 2.38574266 -3.79908538 3.90655398 2.35973716 -3.94636273 3.87604046 2.32099938
		 -3.94709039 3.83878183 2.35271239 -3.85851622 3.74627042 2.2625885 -3.8576827 3.78896952 2.22624588
		 -3.76775908 3.7744956 2.21132636 -3.74180102 3.75028205 2.18349552 -3.72131276 3.72119856 2.14981866
		 -3.71093893 3.68209839 2.10414076 -3.71805096 3.64904618 2.065167189 -3.74365306 3.60352945 2.011125565
		 -3.74448943 3.56069636 2.04758215 -3.65589738 3.47350693 1.96313667 -3.65495849 3.52159071 1.92221069
		 -3.58029461 3.5410111 1.94677031 -3.53744268 3.54153895 1.94839668 -3.49082661 3.53385282 1.94046676
		 -3.44714093 3.51857042 1.9235481 -3.41150188 3.48447514 1.88434887 -3.38866687 3.44415021 1.83753335
		 -3.38161325 3.40513206 1.79188168 -3.37463713 3.36651516 1.74669909 -3.37558079 3.31817961 1.78783906
		 -3.2806623 3.23563337 1.70927835 -3.27961421 3.28931308 1.66358984 -3.25503373 3.32296109 1.70318985
		 -3.19496512 3.33966231 1.72369421 -3.10867691 3.32168674 1.70405793 -3.05781126 3.28995109 1.66744292
		 -3.0029497147 3.21796083 1.5836246 -2.9595511 3.23249483 1.60161304 -2.91615844 3.24703121 1.61960459
		 -2.87276602 3.26156735 1.63759589 -2.55116558 2.9576776 1.28476107 -2.46463966 2.98665237 1.32061529
		 -2.37810707 3.015624046 1.3564651 -2.38367844 2.94773126 1.41376829 -2.27327538 2.98862147 1.46434247
		 -2.27134991 3.056866646 1.40736949 -2.27043915 3.10437894 1.36692703 -2.26971316 3.17153549 1.30864763
		 -2.38013649 3.13063741 1.25806379 -2.37720752 3.063125372 1.3160094 -2.4637363 3.034325123 1.28001392
		 -2.55026484 3.005525589 1.24401879 -2.63678622 2.97672176 1.20801866 -2.72331429 2.94792128 1.1720227
		 -2.80984163 2.91912127 1.13602674 -2.89636922 2.89032054 1.10003102 -3.004545927 3.035933733 1.26758873
		 -3.1047368 3.10104179 1.34074438 -3.2757616 3.13862395 1.37993467 -3.39435196 3.10685587 1.33884919
		 -3.44195604 3.041188478 1.25956333 -3.44090796 3.094868422 1.21387446 -3.54680109 3.15182233 1.29460514
		 -3.54774475 3.10348654 1.33574545 -3.56074595 3.1796751 1.4249891 -3.57388186 3.25664902 1.51515305
		 -3.61826825 3.33588624 1.60726869 -3.68812084 3.40252638 1.684003 -3.77433586 3.43182421 1.71648133
		 -3.86654019 3.4460566 1.73111928 -3.9514308 3.44411659 1.72691584 -4.099737167 3.40416622 1.67660618
		 -4.098798275 3.45224977 1.63568044 -4.19367599 3.52020168 1.72930121 -4.19451237 3.47736907 1.76575768
		 -4.14226866 3.56735945 1.87270784 -4.12703514 3.63243365 1.94953537 -4.14672947 3.70958328 2.039749622
		 -4.18669605 3.76673269 2.10600019 -4.23760843 3.81414557 2.16055942 -4.41643143 3.84206724 2.18928504
		 -4.41559792 3.88476586 2.15294266 -4.50752687 3.95948267 2.25515318 -4.50825453 3.92222333 2.28686571
		 -4.21549559 3.98181581 2.36454439 -4.11171913 4.023422241 2.41675615 -4.016540051 4.099338055 2.50908065
		 -3.99329329 4.14919758 2.56914163 -3.99226093 4.19528341 2.62425923 -4.20083857 4.33489466 2.7844522
		 -4.20202065 4.37153292 2.75707173 -4.038401604 4.47007132 2.87659764 -4.039021969 4.43603182 2.90699029
		 -4.03963995 4.404387 2.93392491 -4.040376186 4.36894655 2.96266866 -4.20399475 4.27040768 2.84314275
		 -4.20145655 4.30324888 2.81138587 -3.99289703 4.16270256 2.65199041 -3.99394774 4.11568117 2.59766841
		 -4.017212868 4.064886093 2.53840446 -4.11241007 3.98803449 2.44687533 -4.21620464 3.94549251 2.39546037
		 -4.5089817 3.88496447 2.318578 -4.50970936 3.84770536 2.35029101;
	setAttr ".vt[1992:2157]" -4.41809893 3.75666952 2.26197004 -4.41726542 3.79936814 2.22562766
		 -4.23844242 3.77142501 2.19692135 -4.18753052 3.72398949 2.14238048 -4.14756441 3.66681814 2.076149225
		 -4.12787056 3.58964586 1.98595381 -4.14310455 3.52454877 1.90914595 -4.19534826 3.43453622 1.80221474
		 -4.19618464 3.39170313 1.8386718 -4.10161448 3.30799937 1.75845766 -4.10067558 3.35608292 1.7175318
		 -3.95237017 3.39600372 1.76786625 -3.86748004 3.39792037 1.77208996 -3.77527642 3.38365769 1.75747836
		 -3.68906212 3.35432577 1.72502851 -3.61921 3.28764462 1.64832902 -3.57482433 3.20836949 1.55624533
		 -3.56168914 3.13136744 1.46610582 -3.54868841 3.055150986 1.37688577 -3.54963231 3.0068154335 1.41802597
		 -3.44405222 2.93382859 1.3509413 -3.44300413 2.98750854 1.30525243 -3.39537692 3.054206133 1.38366628
		 -3.27675796 3.087002993 1.42387879 -3.10571074 3.050451517 1.38381696 -3.005487442 2.98636913 1.30978489
		 -2.89730096 2.84179354 1.14136326 -2.81076407 2.87076306 1.17721105 -2.72422838 2.89973378 1.21306026
		 -2.63769722 2.92870569 1.24891067 -2.27295995 2.6392529 0.91393602 -2.14328814 2.68266058 0.96764588
		 -2.013618708 2.72606874 1.021355867 -2.021456003 2.65121865 1.08434546 -1.85766828 2.7335 1.1847744
		 -1.85530627 2.80888128 1.12228394 -1.85445547 2.85315776 1.084582686 -1.85390484 2.92691135 1.020138264
		 -2.017724037 2.84461999 0.91969651 -2.012797356 2.77033424 0.98364151 -2.14246249 2.72718835 0.92971069
		 -2.27212548 2.68404126 0.87577826 -2.40178895 2.64089489 0.82184684 -2.53145957 2.59775114 0.76791883
		 -2.66111827 2.55460167 0.71398395 -2.79078174 2.5114553 0.66005254 -2.9522748 2.730196 0.91175872
		 -3.10179806 2.82818365 1.021867752 -3.35757589 2.88488412 1.081030846 -3.53470635 2.83756161 1.01978159
		 -3.60535407 2.73938751 0.90123022 -3.60430598 2.79306746 0.85554099 -3.7208519 2.84045839 0.92479247
		 -3.72179556 2.79212284 0.96593279 -3.7408216 2.90591025 1.099213123 -3.7600441 3.020870686 1.23386765
		 -3.82597208 3.13905406 1.37124646 -3.93005013 3.23828554 1.4854871 -4.05879879 3.28163552 1.53350008
		 -4.19656801 3.30243397 1.55480683 -4.32350636 3.29910874 1.54801154 -4.54547215 3.23866773 1.47193778
		 -4.54453325 3.28675127 1.43101192 -4.64536762 3.35120726 1.52038777 -4.64620352 3.30837417 1.55684471
		 -4.56733656 3.44286823 1.71669364 -4.54395723 3.53997636 1.83134496 -4.5729866 3.65520573 1.96608388
		 -4.63242483 3.74043965 2.064883947 -4.70827389 3.81106567 2.14614391 -4.97600365 3.85246062 2.18866038
		 -4.97517014 3.89515924 2.152318 -5.070145607 3.96840668 2.25273204 -5.070872784 3.93114781 2.28444505
		 -4.63262177 4.020302773 2.40065169 -4.4778409 4.08247757 2.47865605 -4.33596134 4.19611835 2.61683249
		 -4.30198574 4.27067852 2.70661759 -4.30132198 4.33957195 2.78898168 -4.61760473 4.54649353 3.026243925
		 -4.61974573 4.58292389 3.00095033646 -4.51728344 4.64925528 3.081233025 -4.51784372 4.61672449 3.11104512
		 -4.51840878 4.58778763 3.13567495 -4.51915979 4.5531621 3.16302204 -4.62162161 4.48683023 3.082739115
		 -4.61816978 4.51755667 3.050873041 -4.30191374 4.30924845 2.81479096 -4.30260515 4.2389679 2.73360848
		 -4.33660746 4.16302013 2.6450038 -4.47851419 4.047992706 2.50800824 -4.63332224 3.98443103 2.43118358
		 -5.071600437 3.89388895 2.31615782 -5.072327614 3.85662985 2.34787011 -4.97767115 3.76706314 2.26134539
		 -4.97683764 3.809762 2.22500277 -4.70910788 3.7683444 2.18250537 -4.6332593 3.69769645 2.10126448
		 -4.57382154 3.61244059 2.0024824142 -4.54479265 3.49718857 1.86776316 -4.56817245 3.40005803 1.75313127
		 -4.64703989 3.26554108 1.59330177 -4.64787626 3.22270799 1.6297586 -4.54734945 3.14250064 1.55378914
		 -4.54641104 3.19058394 1.5128634 -4.32444572 3.25099564 1.58896232 -4.19750786 3.25429773 1.59577715
		 -4.05973959 3.23346877 1.57449675 -3.93099117 3.19008493 1.52651298 -3.82691407 3.090811968 1.41230655
		 -3.7609868 2.97259116 1.27495992 -3.74176478 2.8576026 1.14032984 -3.72273946 2.74378753 1.0070732832
		 -3.72368312 2.69545197 1.048213363 -3.60745025 2.63202786 0.99260807 -3.60640216 2.68570781 0.94691908
		 -3.53571415 2.7854495 1.064140439 -3.35854793 2.83434224 1.12406158 -3.1027329 2.77921176 1.063570142
		 -2.95317268 2.6827929 0.95213211 -2.79164767 2.46562386 0.69909954 -2.66197038 2.50902796 0.75280529
		 -2.53229952 2.55243587 0.80651557 -2.40262866 2.59584475 0.86022627 -1.99475193 2.32082701 0.54311019
		 -1.82194221 2.37867045 0.6146791 -1.64913273 2.43651438 0.68624848 -1.65923262 2.3547051 0.75492138
		 -1.46641278 2.45670247 0.87918079 -1.46361542 2.53922009 0.81117308 -1.46283197 2.58026457 0.77621716
		 -1.46244955 2.66061258 0.70560473 -1.65531039 2.5586009 0.58132792 -1.64838946 2.47754455 0.65127528
		 -1.82119119 2.42005229 0.57940888 -1.99399245 2.36256003 0.5075416 -2.16679406 2.30506825 0.43567565
		 -2.3395946 2.24757552 0.36380818 -2.51239634 2.1900835 0.29194194 -2.68519759 2.13259125 0.22007522
		 -2.90001965 2.42446566 0.55593747 -3.098874092 2.55533314 0.70299989 -3.43939567 2.63114738 0.78213012
		 -3.67505312 2.56826282 0.70070958 -3.76873875 2.43758202 0.54289156 -3.76769066 2.49126124 0.49720263
		 -3.894907 2.52909636 0.55498177 -3.89585066 2.48076105 0.59612203 -3.92089605 2.63214517 0.77343684
		 -3.94620132 2.78509092 0.95258009 -4.033671379 2.94221973 1.1352216 -4.17198563 3.074048042 1.28697562
		 -4.34325552 3.13144422 1.35051525 -4.52659512 3.15881181 1.37849462 -4.69558239 3.15410113 1.36910772
		 -4.99119568 3.073163271 1.26726246 -4.99025679 3.12124681 1.22633672 -5.097062588 3.18221402 1.31147671
		 -5.09789896 3.13938093 1.34793365 -4.99238968 3.31837034 1.56067145 -4.9608779 3.44751883 1.71315324
		 -4.99923706 3.60082459 1.89241385 -5.078154087 3.71414757 2.023768663 -5.1789608 3.80799556 2.13174009
		 -5.53559113 3.86286163 2.18804407 -5.53475761 3.90556049 2.15170193;
	setAttr ".vt[2158:2323]" -5.63276005 3.97732854 2.25030828 -5.6334877 3.9400692 2.28202105
		 -5.049745083 4.0587883 2.43675709 -4.84396648 4.14153528 2.54055905 -4.6553874 4.29290104 2.72458744
		 -4.6106658 4.39215326 2.84408689 -4.61037779 4.48385811 2.95370054 -5.0029306412 4.786098 3.30165792
		 -5.0060138702 4.82231045 3.27844071 -4.92310715 4.89346266 3.36393905 -4.92361927 4.86244631 3.39317918
		 -4.92413139 4.83621645 3.41550303 -4.92488527 4.80239916 3.44144678 -5.007791996 4.73124647 3.35594797
		 -5.0034427643 4.7598691 3.32398295 -4.61092615 4.45579147 2.97758961 -4.61124945 4.36224699 2.86953998
		 -4.65600681 4.26115751 2.75160623 -4.84462214 4.10795307 2.56914234 -5.050436974 4.023367882 2.46690488
		 -5.63421488 3.9028101 2.31373334 -5.63494253 3.86555123 2.34544635 -5.53725863 3.77746439 2.26072931
		 -5.53642511 3.82016325 2.22438669 -5.17979479 3.76527476 2.16810203 -5.078988552 3.67140436 2.060148716
		 -5.000072002411 3.55805945 1.9288131 -4.96171331 3.40473127 1.74957156 -4.9932251 3.27556014 1.59710896
		 -5.098735332 3.096547604 1.38439047 -5.099571705 3.053714514 1.42084754 -4.99307346 2.97699571 1.34911418
		 -4.99213457 3.025079727 1.30818832 -4.69652176 3.10598826 1.41005826 -4.52753496 3.11067533 1.41946542
		 -4.34419632 3.083277225 1.39151216 -4.17292643 3.025847197 1.3280009 -4.034613132 2.89397788 1.17628229
		 -3.94714379 2.7368114 0.99367267 -3.92183924 2.58383775 0.81455338 -3.89679456 2.43242526 0.63726252
		 -3.89773822 2.38408995 0.67840266 -3.77083492 2.33022165 0.63426971 -3.76978683 2.38390183 0.58858055
		 -3.67605281 2.51669288 0.74461454 -3.44034171 2.58168387 0.82424742 -3.099770546 2.5079782 0.74333107
		 -2.9008553 2.37921476 0.59447873 -2.68599129 2.089452267 0.25683391 -2.51318073 2.14729548 0.3284021
		 -2.34037209 2.2051394 0.39997232 -2.16756296 2.26298332 0.47154105 -1.71654546 2.0024008751 0.17228416
		 -1.50059652 2.074680805 0.26171243 -1.34722281 2.0912714 0.28427741 -1.35958266 2.0025031567 0.35863411
		 -1.02907145 2.22091866 0.62283123 -1.025838017 2.31057334 0.54930627 -1.025122523 2.34838486 0.51709563
		 -1.024906993 2.43532634 0.44031402 -1.3554703 2.21689367 0.1760951 -1.34655678 2.12906623 0.25204548
		 -1.49991965 2.11291671 0.22910707 -1.71585834 2.041078329 0.1393051 -1.93179619 1.96923995 0.049502831
		 -2.14773417 1.89740193 -0.040298969 -2.36367249 1.82556367 -0.13010165 -2.57961106 1.75372458 -0.21990386
		 -2.84774852 2.11872816 0.20010792 -3.095940113 2.2824769 0.38412517 -3.52121663 2.37741041 0.48322943
		 -3.81540704 2.29896832 0.38164222 -3.93213296 2.13577867 0.1845559 -3.93108487 2.18945837 0.13886699
		 -4.068953514 2.21773028 0.18516637 -4.069897175 2.16939425 0.22630656 -4.10097122 2.35838032 0.44766036
		 -4.13235903 2.54931068 0.67129242 -4.24137545 2.74538684 0.89919901 -4.41391611 2.90980816 1.088460803
		 -4.6277132 2.98125267 1.1675303 -4.85661554 3.015184164 1.20217693 -5.0676651 3.0090966225 1.190207
		 -5.43692064 2.90765929 1.062588096 -5.43598175 2.95574307 1.021662235 -5.54876232 3.013223171 1.10256827
		 -5.54959869 2.97038984 1.13902485 -5.41745758 3.19388032 1.40465748 -5.37779999 3.35506177 1.59496152
		 -5.42548847 3.54644465 1.81874585 -5.52388334 3.68785477 1.98265231 -5.64962578 3.80491567 2.11732459
		 -6.095173836 3.8732605 2.18742514 -6.094340324 3.91595936 2.15108252 -6.19537926 3.98625255 2.2478869
		 -6.19610643 3.94899321 2.27959967 -5.46686268 4.097270489 2.47285867 -5.21008587 4.20058966 2.60245752
		 -4.97480965 4.38968325 2.83233976 -4.91936731 4.51363802 2.98156691 -4.91943884 4.62814808 3.11842275
		 -5.35070229 5.059110641 3.61718631 -5.38111019 5.071627617 3.56785512 -5.33501577 5.13224983 3.64013839
		 -5.33546734 5.10274124 3.66879725 -5.33592653 5.079219818 3.68881702 -5.33669567 5.046217918 3.7133646
		 -5.38279009 4.98559523 3.64108062 -5.35116148 5.035590172 3.63720655 -4.91994286 4.60233688 3.14039063
		 -4.91991568 4.48553848 3.0054843426 -4.97540331 4.35929251 2.8582058 -5.21072388 4.1679101 2.63027287
		 -5.46754503 4.062301636 2.50262189 -6.19683409 3.91173434 2.31131291 -6.19756126 3.87447548 2.34302545
		 -6.096841335 3.78786302 2.26011038 -6.096007824 3.83056188 2.223768 -5.65045977 3.76219463 2.15368676
		 -5.52471781 3.64511156 2.019032478 -5.42632341 3.50367951 1.85514462 -5.37863541 3.31227398 1.63138008
		 -5.41829348 3.15106988 1.44109535 -5.55043507 2.92755675 1.17548168 -5.55127096 2.88472366 1.21193886
		 -5.43879795 2.8114922 1.14443922 -5.43785954 2.85957599 1.10351384 -5.068604469 2.96098375 1.23115778
		 -4.85755539 2.96704793 1.24314761 -4.62865353 2.93308568 1.20852709 -4.41485739 2.86160731 1.1294862
		 -4.2423172 2.69714522 0.94025934 -4.13330173 2.5010314 0.71238488 -4.10191441 2.31007218 0.48877656
		 -4.070841312 2.12105942 0.26744699 -4.071784973 2.072723866 0.30858725 -3.93422914 2.028418779 0.27593416
		 -3.93318105 2.082098722 0.23024505 -3.81639147 2.24793625 0.42508858 -3.52214074 2.32902765 0.52443534
		 -3.096800089 2.23674107 0.42308757 -2.84854102 2.075638294 0.23682573 -2.58034062 1.71328306 -0.1854286
		 -2.36439133 1.78556311 -0.09599942 -2.14844203 1.85784185 -0.0065721292 -1.93249345 1.93012142 0.082855538
		 -5.85575342 5.1842351 3.858603 -5.85499001 5.21644592 3.8354373 -5.85458279 5.23730373 3.81768441
		 -5.85417032 5.26532221 3.78958583 -5.88305187 5.19819164 3.71005058 -5.93783522 5.19093418 3.76129484
		 -5.22326088 4.7699914 3.28035188 -5.22281504 4.63305426 3.11670589 -5.28881168 4.48481989 2.93826318
		 -5.57000446 4.2586441 2.66330886 -5.87692976 4.13511133 2.50835991 -6.74918127 3.95776105 2.27721405
		 -6.74845409 3.99502015 2.24550128 -6.64442873 3.92617679 2.15046835 -6.64526272 3.88347769 2.18681049
		 -6.11233377 3.80189586 2.10316277 -5.96205759 3.66200662 1.94223249 -5.84452057 3.49298692 1.74632597
		 -5.78765345 3.26416993 1.47877121 -5.83531427 3.071494579 1.25128186;
	setAttr ".vt[2324:2489]" -5.99363518 2.80425858 0.93365192 -5.99279881 2.84709144 0.89719522
		 -5.87414598 2.79303908 0.8204509 -5.87508488 2.74495554 0.86137652 -5.43343592 2.86654663 1.014334917
		 -5.18105936 2.87399936 1.028856874 -4.90734625 2.83360457 0.98764491 -4.65174103 2.748348 0.89330643
		 -4.44555712 2.55188799 0.66717482 -4.31536293 2.31752539 0.39477092 -4.27798986 2.08925128 0.1274012
		 -4.2410059 1.8633101 -0.13723627 -4.24006224 1.91164541 -0.17837681 -4.091710567 1.89276946 -0.21339595
		 -4.092758656 1.83908939 -0.16770695 -3.95338249 2.034235477 0.067980148 -3.60164881 2.12797236 0.18939316
		 -3.093055964 2.014244556 0.070655063 -2.79637098 1.81817341 -0.14968815 -2.4626317 1.34884524 -0.65242654
		 -2.20601368 1.43981314 -0.5449934 -1.94939673 1.53078103 -0.43755975 -1.69277918 1.62174881 -0.33012581
		 -1.43616283 1.71271646 -0.222692 -1.17954588 1.80368388 -0.11525878 -0.89729071 1.92190552 0.021623014
		 -0.91093993 2.023082018 -0.066239402 -0.56397814 2.2463789 0.20170833 -0.56403184 2.14609313 0.29063851
		 -0.56472862 2.10891461 0.32231736 -0.56865948 2.0051448345 0.4070659 -0.91084695 1.7818265 0.13914621
		 -0.89719772 1.88474405 0.053329259 -1.17946184 1.76853514 -0.085273333 -1.43608093 1.67703807 -0.19225822
		 -1.69270039 1.58554018 -0.29924333 -1.94931996 1.49404299 -0.40622798 -2.20593929 1.40254593 -0.51321238
		 -2.4625597 1.31104708 -0.62019789 -2.79711223 1.77720451 -0.11476779 -3.093875885 1.9700985 0.10826884
		 -3.60254526 2.080648184 0.22969826 -3.95435381 1.98373401 0.11097796 -4.093806744 1.78540957 -0.12201776
		 -4.094854355 1.7317301 -0.076328821 -4.2428937 1.76663864 -0.05495581 -4.24194956 1.81497395 -0.096095957
		 -4.27893257 2.040943384 0.1685176 -4.31630564 2.26924586 0.4358632 -4.44649887 2.50364637 0.70823455
		 -4.65268183 2.70014739 0.9343316 -4.90828657 2.78543758 1.028641343 -5.18199873 2.82586336 1.069827437
		 -5.43437529 2.818434 1.055285811 -5.87602329 2.69687223 0.90230238 -5.87696218 2.64878845 0.94322842
		 -5.99530792 2.71859241 1.00656569 -5.99447155 2.76142526 0.97010905 -5.83615017 3.028684139 1.28771949
		 -5.78848886 3.22138214 1.51518989 -5.84535551 3.45022178 1.7827251 -5.96289206 3.61926389 1.9786129
		 -6.11316776 3.75917506 2.1395247 -6.64609623 3.84077907 2.22315335 -6.64692974 3.79808044 2.25949574
		 -6.7506361 3.88324308 2.34063911 -6.74990892 3.92050219 2.30892658 -5.87760401 4.10058498 2.53774548
		 -5.57062483 4.22685194 2.69036841 -5.28937912 4.45576191 2.96299624 -5.22332907 4.60672903 3.13911271
		 -5.2237215 4.74639893 3.30043244 -5.93824244 5.17007494 3.77904773 -5.88463497 5.1171031 3.77906799
		 -0.82123065 2.17436671 0.58296734 -1.19535398 1.92158711 0.27739525 -1.1817733 2.015486479 0.19879739
		 -1.38367248 1.96407199 0.1332835 -1.61531663 1.88654041 0.03735739 -1.84696281 1.80900824 -0.058569126
		 -2.078607559 1.73147595 -0.15449534 -2.31025338 1.6539439 -0.250422 -2.54189873 1.57641232 -0.34634817
		 -2.82950687 1.96518004 0.10669218 -3.095712423 2.13804722 0.30656168 -3.55189705 2.23709393 0.41534334
		 -3.86745954 2.15015006 0.30883017 -3.99263334 1.97228658 0.099864095 -3.99368119 1.91860688 0.14555272
		 -4.13511515 1.95943213 0.17402877 -4.13417149 2.0077679157 0.13288875 -4.16743422 2.21046114 0.37024075
		 -4.20103598 2.41524148 0.61003703 -4.31788921 2.62552595 0.85438126 -4.50287342 2.80184293 1.057249904
		 -4.73215103 2.87843657 1.14194608 -4.97764587 2.91479421 1.1789999 -5.20398474 2.90822148 1.166062
		 -5.60003948 2.79935718 1.029042721 -5.6009779 2.75127363 1.069968581 -5.71561813 2.8232336 1.13592458
		 -5.71478224 2.86606669 1.099467516 -5.57295465 3.10577226 1.38432693 -5.53033113 3.27863145 1.58837414
		 -5.5814209 3.48389411 1.82834125 -5.68688774 3.63554025 2.0040676594 -5.82172823 3.7610805 2.14844847
		 -6.29961157 3.83434415 2.22354126 -6.30044508 3.79164553 2.2598834 -6.40226316 3.87771773 2.34213901
		 -6.40153599 3.91497684 2.31042624 -5.61932373 4.076473713 2.51562476 -5.34394026 4.18972921 2.65251899
		 -5.091608047 4.39499521 2.89698863 -5.03222084 4.53039551 3.054945469 -5.032382965 4.65566015 3.19962811
		 -5.56844616 5.085361958 3.68970132 -5.56853628 5.03426981 3.69215393 -5.5288043 5.097298145 3.76711726
		 -5.52805567 5.1300149 3.74309015 -5.52761555 5.15255117 3.72390985 -5.52716064 5.18149996 3.69544816
		 -5.56689215 5.1184721 3.62048602 -5.56800604 5.10789776 3.67052031 -5.031895161 4.68064928 3.17835832
		 -5.031684875 4.55783939 3.031588078 -5.091024399 4.4248929 2.87154245 -5.34330893 4.22208023 2.62498426
		 -5.61864424 4.11127901 2.48600149 -6.40080833 3.95223594 2.27871418 -6.40008116 3.9894948 2.24700141
		 -6.29794407 3.91974187 2.15085649 -6.29877806 3.87704301 2.1871984 -5.82089424 3.8038013 2.11208677
		 -5.68605328 3.67828321 1.96768725 -5.58058596 3.52665949 1.79194212 -5.52949572 3.32141948 1.5519557
		 -5.57211876 3.14858222 1.34788954 -5.71394587 2.90890002 1.063010812 -5.71310949 2.95173287 1.026553869
		 -5.5981617 2.89552402 0.94719106 -5.59910059 2.84744048 0.98811698 -5.20304537 2.95633459 1.1251111
		 -4.97670603 2.9629302 1.13802922 -4.73121071 2.92660356 1.10094941 -4.50193262 2.85004377 1.016224384
		 -4.31694698 2.67376781 0.81332064 -4.20009375 2.463521 0.56894469 -4.16649103 2.25876904 0.32912421
		 -4.13322783 2.056103706 0.091748416 -4.13228416 2.10443902 0.050608531 -3.99053717 2.079646349 0.0084858313
		 -3.99158525 2.025966406 0.054174785 -3.86647511 2.20098448 0.26554799 -3.55098581 2.28508687 0.37447262
		 -3.09487009 2.18319607 0.26810002 -2.82873297 2.0074846745 0.070639864 -2.54119277 1.6158731 -0.37999189
		 -2.30955863 1.69293106 -0.28366435 -2.077925682 1.76998973 -0.18733534 -1.84629154 1.84704745 -0.091008015
		 -1.6146574 1.92410541 0.0053198691 -1.38302398 2.0011634827 0.10164806 -1.18112028 2.053045273 0.16675976
		 -1.19105482 2.14590502 0.086400688 -0.81687385 2.39870286 0.3919957;
	setAttr ".vt[2490:2655]" -0.81702965 2.30682278 0.47327381 -0.81773794 2.26924515 0.50528711
		 -3.022015333 3.85832977 2.19809079 -3.085285187 3.89989448 2.25481558 -3.023663759 3.77389574 2.26995564
		 -3.086753845 3.82468009 2.31883311 -2.91875434 3.91750526 2.26998448 -2.98202491 3.95906925 2.32670903
		 -2.920403 3.83307123 2.34184933 -2.98349333 3.88385558 2.39072633 -2.92583299 3.91320777 2.27618027
		 -2.98066735 3.94923019 2.3253417 -2.92726159 3.84003186 2.33846331 -2.98194003 3.88404512 2.3808229
		 -2.92946506 3.91091514 2.28328896 -2.97698903 3.94213462 2.32589555 -2.9307034 3.84749579 2.33726764
		 -2.97809219 3.88564038 2.37397957 -2.88705444 3.93634343 2.31413722 -2.93457818 3.96756339 2.35674429
		 -2.88829255 3.87292409 2.36811614 -2.93568134 3.91106915 2.40482855 -2.8580308 3.95046782 2.33139753
		 -2.8583858 3.99298906 2.38836455 -2.8592689 3.88704848 2.38537669 -2.85948873 3.93649507 2.43644905
		 -2.83542752 3.94881392 2.3299737 -2.78829503 3.97915721 2.37372136 -2.83666587 3.88539505 2.38395238
		 -2.78939819 3.92266321 2.42180586 -2.81215978 3.93757105 2.31729865 -2.74166226 3.93791151 2.32633209
		 -2.81339812 3.87415218 2.37127709 -2.74276519 3.88141727 2.37441587 -2.77940679 3.87283874 2.28817272
		 -2.76098752 3.86671615 2.28337574 -2.77975512 3.8549931 2.30336142 -2.7612977 3.85081887 2.29690623
		 -4.12201881 1.83991992 -0.21076243 -4.12390232 1.74344707 -0.12865061 -4.23635626 1.85515666 -0.18575157
		 -4.23805237 1.76828849 -0.11181445 -6.70578575 3.90513253 2.18650794 -6.70708895 3.8383956 2.24330997
		 -6.78570509 3.95981884 2.2575407 -6.78684187 3.90158415 2.30710626 -6.01402092 2.72893524 0.95694101
		 -5.92291784 2.67749476 0.9070338 -5.92142868 2.75377822 0.84210563 -6.012694359 2.79688883 0.89910305
		 -4.13942242 1.81497109 -0.2225526 -4.14096069 1.73618507 -0.15549484 -4.23279762 1.82741499 -0.20212632
		 -4.23418283 1.75647259 -0.14174521 -6.73436737 3.89216399 2.1830132 -6.73543119 3.83766222 2.22940135
		 -6.79963255 3.93682361 2.24102163 -6.80056095 3.8892653 2.28150034 -6.022949696 2.71631169 0.92928171
		 -5.94854927 2.67430139 0.88852352 -5.94733334 2.73659945 0.83549923 -6.021866322 2.77180743 0.88204765
		 -4.14929485 1.79703283 -0.23720223 -4.15070486 1.72481287 -0.1757324 -4.23488855 1.80843902 -0.2184799
		 -4.23615837 1.74340832 -0.16312973 -6.75532866 3.88147831 2.17457938 -6.75630379 3.83151841 2.21710205
		 -6.81515312 3.92241645 2.22775459 -6.81600428 3.87882161 2.26485991 -6.034813404 2.70463848 0.91060883
		 -5.96661329 2.66612935 0.87324798 -5.96549845 2.72323608 0.82464194 -6.033820152 2.75550914 0.86731076
		 -4.19543314 1.71648264 -0.29874963 -4.19618511 1.67796326 -0.26596776 -4.24108267 1.72256458 -0.2887651
		 -4.24175978 1.68788218 -0.25924495 -6.84916592 3.83443451 2.14077878 -6.84968615 3.80778933 2.16345763
		 -6.88107252 3.85626745 2.16913772 -6.88152647 3.83301687 2.18892765 -6.084346771 2.65400577 0.82593071
		 -6.047973633 2.63346791 0.80600452 -6.047379017 2.66392446 0.78008151 -6.083817005 2.68113661 0.80283827
		 0.53452033 1.25248754 -0.89018536 0.20416856 1.49250197 -0.89018536 -0.20416869 1.49250197 -0.89018536
		 -0.53452045 1.25248742 -0.89018536 -0.66070348 0.86413568 -0.89018536 -0.53452033 0.47578621 -0.89018536
		 -0.20416857 0.21218371 -0.89018536 0.20416862 0.21218371 -0.89018536 0.53452033 0.4757863 -0.89018536
		 0.66070342 0.86413568 -0.89018536 0.53452033 1.25248754 -0.44971639 0.20416856 1.53982973 -0.44971639
		 -0.20416869 1.53982973 -0.44971639 -0.53452045 1.25248742 -0.44971639 -0.66070348 0.86413568 -0.44971639
		 -0.53452033 0.47578621 -0.44971639 -0.20416857 0.23576939 -0.44971639 0.20416862 0.23576939 -0.44971639
		 0.53452033 0.4757863 -0.44971639 0.66070342 0.86413568 -0.44971639 0.53452033 1.25248754 -0.0092485696
		 0.20416856 1.54261374 -0.0092485696 -0.20416869 1.54261374 -0.0092485696 -0.53452045 1.25248742 -0.0092485696
		 -0.66070348 0.86413568 -0.0092485696 -0.53452033 0.47578621 -0.0092485696 -0.20416857 0.20957291 -0.0092485696
		 0.20416862 0.20957291 -0.0092485696 0.53452033 0.4757863 -0.0092485696 0.66070342 0.86413568 -0.0092485696
		 0.78910989 1.63548374 0.48100588 0.23580556 1.80343735 0.29233107 -0.30411896 1.83162475 0.29233107
		 -0.79619348 1.52794981 0.48100594 -0.98414999 1.03659606 0.78628719 -0.79619324 0.54524148 1.09156239
		 -0.30411872 0.24156982 1.28024435 0.3041189 0.2415691 1.28024435 0.79619342 0.54524207 1.09156239
		 0.98415023 1.03659606 0.78628719 0.75478417 1.76545179 0.55005825 0.21069804 1.8985045 0.31295243
		 -0.30411854 1.92995071 0.31295243 -0.79619372 1.66237402 0.55005872 -0.98414999 1.22942841 0.9337008
		 -0.79619366 0.79648244 1.317348 -0.3041186 0.52890909 1.55445361 0.30411935 0.52890754 1.55445409
		 0.79619336 0.79648292 1.317348 0.98415023 1.22942841 0.9337008 0.74113876 1.92975974 0.60433054
		 0.1773973 2.067619085 0.35865912 -0.31510493 2.10196233 0.3586593 -0.82916129 1.82509446 0.60433102
		 -1.01970315 1.3761332 1.0018280745 -0.82495683 0.9275468 1.39933896 -0.31510505 0.65030718 1.64501035
		 0.31510597 0.65030605 1.64501083 0.82495642 0.92754728 1.39933896 1.019703388 1.37613297 1.0018280745
		 0.66128951 2.03899765 0.65872872 0.14049909 2.13357162 0.39238605 -0.29627076 2.17515016 0.39238605
		 -0.77740276 1.95011401 0.65872955 -0.9675625 1.59063303 1.089673638 -0.77564865 1.22447598 1.52063131
		 -0.296271 1.000054597855 1.78697395 0.29627213 1.000052332878 1.78697491 0.7756483 1.22447634 1.52063179
		 0.93105072 1.73466647 1.089673638 0.2044951 2.98907423 2.5345695 0.078110464 3.080749989 2.53977633
		 -0.078109838 3.080749989 2.53977633 -0.20449542 2.98907351 2.53456879 -0.25277033 2.84073806 2.55707455
		 -0.20449542 2.6924026 2.51771951 -0.078110129 2.60072589 2.51251292 0.078110605 2.60072565 2.51251268
		 0.20449546 2.6924026 2.51771975 0.2527706 2.84073806 2.55707455;
	setAttr ".vt[2656:2821]" 0.12612547 3.2019341 1.90910912 0.33019987 3.082312346 1.96650124
		 0.40815052 2.88875818 2.090293169 0.33020034 2.69520569 2.15222621 0.12612571 2.57558203 2.20961714
		 -0.1261251 2.57558298 2.20961714 -0.33020037 2.69520569 2.15222621 -0.40815029 2.88875818 2.090293169
		 -0.33020037 3.08231163 1.96650124 -0.1261248 3.20193481 1.90910912 0.16932395 3.05986619 1.14161837
		 0.44329494 2.92790294 1.27148402 0.54794389 2.71437907 1.52630866 0.44329545 2.50085711 1.6917367
		 0.16932429 2.36889195 1.82160234 -0.16932352 2.36889291 1.82160187 -0.44329566 2.50085664 1.6917367
		 -0.54794365 2.71437907 1.52630866 -0.44329566 2.92790198 1.27148414 -0.16932331 3.059866667 1.14161837
		 0.46376818 2.55550027 0.84609282 0.57325011 2.40932226 1.19398642 0.46376854 2.26314569 1.43044901
		 0.17714435 2.17280269 1.61102378 -0.17714359 2.17280388 1.61102378 -0.46376869 2.26314545 1.43044901
		 -0.57324988 2.40932226 1.19398642 -0.46376869 2.55549979 0.84609282 -0.17714338 2.64584303 0.66551697
		 0.17714398 2.64584208 0.66551721 0.62104106 2.13864493 1.058395147 0.50243229 2.04335475 1.35676527
		 0.19191262 1.98446178 1.57049239 -0.19191179 1.98446321 1.57049143 -0.50243241 2.043354511 1.35676527
		 -0.62104082 2.13864517 1.058395147 -0.49871013 2.22793674 0.66513133 -0.19191164 2.29282856 0.45140365
		 0.19191219 2.29282808 0.45140412 0.32481366 2.24305296 0.66513062 0.21167095 1.78697777 1.60524738
		 -0.21167009 1.78697944 1.60524642 -0.5541603 1.86996269 1.379246 -0.68497998 2.0042355061 1.013564587
		 -0.55229098 2.13353705 0.64789128 -0.21166986 2.22149348 0.42188933 0.21167047 2.22149253 0.42188957
		 0.39919397 2.16399765 0.64789057 0.68498021 2.0042352676 1.013564587 0.55416012 1.86996317 1.379246
		 0.24805522 1.44854605 1.6834029 -0.24805424 1.44854784 1.68340194 -0.64941543 1.5923593 1.4400512
		 -0.80272192 1.82505405 1.046295047 -0.64910442 2.054652691 0.65255249 -0.24805404 2.20156264 0.4092007
		 0.24805471 2.20156169 0.409201 0.51191294 2.11023903 0.65255165 0.80272216 1.82505393 1.046295047
		 0.64941519 1.59235954 1.44005167 0.72822344 1.52443397 0.31758806 0.87633461 0.97910929 0.52110887
		 0.708969 0.52208948 0.72463012 0.27080214 0.23963636 0.85041165 -0.27080199 0.23963636 0.85041165
		 -0.70896894 0.522089 0.72463012 -0.87633443 0.97910929 0.52110887 -0.70896918 1.43612909 0.31758812
		 -0.2708022 1.71858382 0.19180486 0.27080202 1.71858215 0.19180486 0.66733718 1.41338432 0.1541701
		 0.76851898 0.92162246 0.25593066 0.62174469 0.49893892 0.35769132 0.23748536 0.21150565 0.42058298
		 -0.23748529 0.21150637 0.42058298 -0.62174457 0.49893868 0.35769132 -0.76851898 0.92162246 0.25593066
		 -0.62174481 1.34430814 0.15417033 -0.23748542 1.6055429 0.091278613 0.23748529 1.60554218 0.091278613
		 0.60645086 1.30233455 -0.22948198 0.66070342 0.86413568 -0.22948198 0.53452033 0.4757863 -0.22948198
		 0.20416862 0.23576939 -0.22948198 -0.20416857 0.23576939 -0.22948198 -0.53452033 0.47578621 -0.22948198
		 -0.66070348 0.86413568 -0.22948198 -0.53452045 1.25248742 -0.22948198 -0.20416869 1.54261374 -0.22948198
		 0.20416856 1.54261374 -0.22948198 0.53452033 1.25248754 -0.6699509 0.66070342 0.86413568 -0.6699509
		 0.53452033 0.4757863 -0.6699509 0.20416862 0.2279076 -0.6699509 -0.20416857 0.2279076 -0.6699509
		 -0.53452033 0.47578621 -0.6699509 -0.66070348 0.86413568 -0.6699509 -0.53452045 1.25248742 -0.6699509
		 -0.20416869 1.53704572 -0.6699509 0.20416856 1.53704572 -0.6699509 0.32229578 0.74910128 1.79280615
		 -0.32229468 0.74910372 1.79280519 -0.84377968 1.013560772 1.52289069 -1.042969465 1.44146466 1.08615315
		 -0.84710896 1.87010252 0.64942944 -0.3222945 2.13382888 0.37951395 0.17621152 2.10026503 0.37951395
		 0.74793631 1.97773516 0.64942873 1.042969704 1.44146442 1.08615315 0.84377927 1.013561249 1.52289069
		 0.83372998 1.0845505 1.55281866 0.31845734 0.8328585 1.82890058 -0.3184562 0.83286095 1.82889962
		 -0.83373034 1.084550142 1.55281866 -1.030547857 1.49179554 1.10610962 -0.83648229 1.89957285 0.65940154
		 -0.31845599 2.15073347 0.38331929 0.16818362 2.11562943 0.38331929 0.73064339 2.0038573742 0.6594007
		 1.030548096 1.4917953 1.10610962 0.27735859 0.85083348 -1.70123756 0.1059415 0.97537524 -1.70123756
		 -0.10594159 0.97537524 -1.70123756 -0.27735871 0.85083342 -1.70123756 -0.34283414 0.64932048 -1.70123756
		 -0.27735865 0.44780976 -1.70123756 -0.10594156 0.31102717 -1.70123756 0.1059415 0.31102717 -1.70123756
		 0.27735856 0.44780985 -1.70123756 0.34283412 0.64932048 -1.70123756 0.15505503 1.23393869 -1.2957114
		 -0.15505512 1.23393869 -1.2957114 -0.40593955 1.051660419 -1.2957114 -0.50176883 0.75672811 -1.2957114
		 -0.40593943 0.46179795 -1.2957114 -0.15505506 0.26160544 -1.2957114 0.15505506 0.26160544 -1.2957114
		 0.40593943 0.46179804 -1.2957114 0.50176877 0.75672811 -1.2957114 0.40593943 1.051660538 -1.2957114
		 0.18824713 1.39029598 -1.092948437 -0.18824725 1.39029598 -1.092948437 -0.49283761 1.16899812 -1.092948437
		 -0.60918069 0.81093085 -1.092948437 -0.49283749 0.45286551 -1.092948437 -0.18824717 0.20981944 -1.092948437
		 0.18824717 0.20981944 -1.092948437 0.49283746 0.4528656 -1.092948437 0.60918057 0.81093085 -1.092948437
		 0.49283746 1.16899824 -1.092948437 0.13805419 1.12834811 -1.4984746 -0.13805427 1.12834811 -1.4984746
		 -0.36143079 0.96605557 -1.4984746 -0.44675303 0.70346075 -1.4984746 -0.36143067 0.44086805 -1.4984746
		 -0.13805424 0.26262462 -1.4984746 0.13805419 0.26262462 -1.4984746 0.36143067 0.44086817 -1.4984746
		 0.44675291 0.70346075 -1.4984746 0.36143067 0.96605563 -1.4984746 0.18799582 0.75218087 -1.81532001
		 0.071807973 0.8351211 -1.8310324 -0.071808092 0.8351211 -1.8310324 -0.18799599 0.75218087 -1.81532001
		 -0.2323757 0.61798197 -1.78989661 -0.18799588 0.48378271 -1.76447332;
	setAttr ".vt[2822:2987]" -0.071808025 0.39269191 -1.7472167 0.071807943 0.39269191 -1.7472167
		 0.18799576 0.48378274 -1.76447332 0.23237567 0.61798197 -1.78989661 0.032518066 -0.064411759 -5.87021208
		 0.01242108 -0.049813688 -5.8699131 4.371563e-08 -0.088032007 -5.8706975 -0.01242078 -0.049813688 -5.8699131
		 -0.032517761 -0.064411759 -5.87021208 -0.040193655 -0.088032007 -5.8706975 -0.032517083 -0.11165202 -5.87118196
		 -0.01242044 -0.12768567 -5.87151098 0.012420401 -0.12768567 -5.87151098 0.032517042 -0.11165202 -5.87118196
		 0.040193621 -0.088032007 -5.8706975 0.085240662 0.48205248 -2.33417821 -0.085240729 0.48205248 -2.33417821
		 -0.22316307 0.43233067 -2.24723411 -0.27584448 0.35187703 -2.10655546 -0.22316283 0.27142549 -1.96587718
		 -0.085240595 0.21681744 -1.8703891 0.085240513 0.21681744 -1.8703891 0.2231627 0.27142549 -1.96587718
		 0.27584448 0.35187703 -2.10655546 0.22316293 0.43233067 -2.24723411 0.2897723 0.14806557 -2.52104235
		 0.23443089 0.24741143 -2.65654922 0.089544699 0.30881065 -2.74029708 -0.089544721 0.30881065 -2.74029708
		 -0.234431 0.24741143 -2.65654922 -0.28977236 0.14806557 -2.52104235 -0.23443064 0.048719764 -2.38553548
		 -0.089544512 -0.018713415 -2.29355741 0.08954443 -0.018713415 -2.29355741 0.23443051 0.048719764 -2.38553548
		 0.18546213 0.11135972 -3.087828636 0.070840314 0.16085273 -3.15228224 -0.070840284 0.16085273 -3.15228224
		 -0.18546215 0.11135972 -3.087828636 -0.22924352 0.031277835 -2.98354197 -0.18546176 -0.048803806 -2.87925482
		 -0.070840076 -0.10316044 -2.80846763 0.070840009 -0.10316044 -2.80846763 0.18546164 -0.048803806 -2.87925482
		 0.22924347 0.031277835 -2.98354197 0.16151366 0.094772756 -3.35385203 0.061692826 0.14128935 -3.40650153
		 -0.061692778 0.14128935 -3.40650153 -0.16151367 0.094772756 -3.35385203 -0.19964156 0.019507647 -3.26866508
		 -0.16151324 -0.055757225 -3.1834774 -0.061692566 -0.10684454 -3.12565422 0.061692506 -0.10684454 -3.12565422
		 0.16151315 -0.055757225 -3.1834774 0.1996415 0.019507647 -3.26866508 0.18048848 -0.023255229 -3.54820299
		 0.14601861 0.048911929 -3.62011099 0.055774268 0.093513906 -3.66455388 -0.055774193 0.093513906 -3.66455388
		 -0.14601859 0.048911929 -3.62011099 -0.18048854 -0.023255229 -3.54820299 -0.14601815 -0.095421851 -3.4762938
		 -0.055773966 -0.14440733 -3.42748404 0.055773906 -0.14440733 -3.42748404 0.14601806 -0.095421851 -3.4762938
		 0.16002591 -0.03258884 -3.88682365 0.12946409 0.036261022 -3.94391799 0.049451023 0.07881242 -3.97920465
		 -0.049450919 0.07881242 -3.97920465 -0.12946403 0.036261022 -3.94391799 -0.16002597 -0.03258884 -3.88682365
		 -0.12946355 -0.1014387 -3.82972908 -0.049450673 -0.1481716 -3.79097486 0.049450625 -0.1481716 -3.79097486
		 0.12946346 -0.10143793 -3.82972908 0.14266063 -0.067790806 -4.15846968 0.11541533 -0.0054966807 -4.2072196
		 0.044084895 0.033004224 -4.23734951 -0.044084761 0.033004224 -4.23734951 -0.11541522 -0.0054966807 -4.2072196
		 -0.14266069 -0.067790806 -4.15846968 -0.11541472 -0.13008696 -4.1097188 -0.044084501 -0.17237145 -4.076628208
		 0.044084456 -0.17237145 -4.076628208 0.11541466 -0.13008696 -4.1097188 0.12938431 -0.081734121 -4.38032198
		 0.1046746 -0.0244506 -4.42269182 0.039982319 0.010953546 -4.44887924 -0.039982162 0.010953546 -4.44887924
		 -0.10467448 -0.024450362 -4.42269182 -0.12938438 -0.081734121 -4.38032198 -0.10467395 -0.13901907 -4.33795023
		 -0.039981894 -0.17790204 -4.3091898 0.039981849 -0.1779018 -4.3091898 0.10467388 -0.13901907 -4.33795023
		 0.11367141 -0.1009559 -4.64288521 0.09196265 -0.049602926 -4.67770958 0.035126805 -0.017864287 -4.69923115
		 -0.035126626 -0.017864287 -4.69923115 -0.091962494 -0.049602926 -4.67770958 -0.1136715 -0.1009559 -4.64288521
		 -0.091961943 -0.15231031 -4.60806799 -0.035126343 -0.18716735 -4.58443213 0.035126299 -0.18716735 -4.58443213
		 0.091961883 -0.15231007 -4.60806799 0.092570923 -0.099258661 -4.82629728 0.07489197 -0.059718251 -4.85723114
		 0.02860637 -0.035279512 -4.87634802 -0.028606199 -0.035279512 -4.87634802 -0.074891806 -0.059718251 -4.85723114
		 -0.092570983 -0.099258661 -4.82629728 -0.074891299 -0.13880056 -4.7953577 -0.028605932 -0.16564006 -4.77436447
		 0.028605888 -0.16564006 -4.77436447 0.074891225 -0.13880056 -4.7953577 0.079402611 -0.096517742 -5.088991165
		 0.064238638 -0.063636899 -5.11568451 0.024537187 -0.043316543 -5.1321764 -0.024536982 -0.043316543 -5.1321764
		 -0.064238437 -0.063636899 -5.11568451 -0.079402663 -0.096517742 -5.088991165 -0.064237885 -0.12939686 -5.062297821
		 -0.024536699 -0.15171486 -5.044176579 0.024536654 -0.15171486 -5.044176579 0.064237826 -0.12939686 -5.062297821
		 0.029692264 -0.21060568 -5.3883009 0.077735379 -0.17748564 -5.39584112 0.096086688 -0.12869161 -5.40695286
		 0.077736743 -0.079898179 -5.41806412 0.02969316 -0.049741387 -5.424932 -0.029692769 -0.049741387 -5.424932
		 -0.077736355 -0.07989794 -5.41806412 -0.09608674 -0.12869161 -5.40695286 -0.077735439 -0.17748588 -5.39584303
		 -0.029692303 -0.21060592 -5.3883009 0.028159369 -0.21700603 -5.6902051 0.073722191 -0.18353468 -5.69406128
		 0.091126218 -0.13422322 -5.69974375 0.073723905 -0.084912241 -5.70542526 0.028160509 -0.054435372 -5.70893669
		 -0.028159982 -0.054435372 -5.70893669 -0.073723391 -0.084912241 -5.70542526 -0.091126271 -0.13422322 -5.69974375
		 -0.073722236 -0.1835348 -5.69406128 -0.028159406 -0.21700603 -5.6902051 0.75499278 1.60929656 -0.027208343
		 0.36150894 1.8054496 -0.027208343 0.37765247 1.87479925 0.073318601 0.81587911 1.72034645 0.13621023
		 -0.42616913 1.80146933 -0.027208522 -0.75652093 1.511343 -0.027208522 -0.84374517 1.60316372 0.13621023
		 -0.45948589 1.86439848 0.073318601 0.074812077 2.53626871 4.47933722 0.19586015 2.40341759 4.49650002
		 0.242098 2.27437472 4.50162554 0.19586021 2.10203266 4.48128128 0.1996904 1.99972904 4.44495201
		 -0.19968814 1.99973011 4.44495487 -0.19586134 2.10203362 4.48128128 -0.24209666 2.27437544 4.50162554
		 -0.19586027 2.40341759 4.49650002 -0.074811593 2.53626871 4.47933722 3.1480909e-07 2.22100401 4.56156063
		 0.086048983 3.049496651 2.64827156 0.22527838 2.94871879 2.64133286;
	setAttr ".vt[2988:3153]" 0.27846029 2.78565502 2.66103506 0.22527875 2.61094379 2.59946394
		 0.086049192 2.52181363 2.61193871 -0.086048603 2.52181387 2.61193895 -0.22527881 2.61094379 2.5994637
		 -0.27845997 2.78565502 2.66103506 -0.22527875 2.94871807 2.64133263 -0.086048335 3.049496651 2.64827156
		 0.11897618 2.36367655 2.77532578 -0.11897536 2.36367726 2.77532578 -0.31148279 2.50250745 2.78778005
		 -0.3850137 2.727139 2.80792832 -0.31148249 2.85403514 2.82807803 -0.11897509 3.1122539 2.75030446
		 0.1189758 3.1122539 2.75030446 0.31148183 2.85403585 2.82807803 0.38501412 2.727139 2.80792832
		 0.31148225 2.50250745 2.78778052 0.4953509 2.36706018 3.20744443 0.50979561 2.14652944 3.1862309
		 -0.50979453 2.14653063 3.18623233 -0.49535203 2.36706114 3.20744371 -0.61228836 2.72388506 3.24176431
		 -0.49535167 2.94275999 3.27608633 -0.1892066 3.30123806 3.29729795 0.18920735 3.30123806 3.29729795
		 0.49535012 2.9427619 3.27608681 0.61228889 2.72388339 3.24176431 0.10211422 2.70130205 4.17586851
		 0.26733792 2.52808332 4.1869421 0.33044979 2.33933091 4.18873215 0.2673381 2.14462495 4.18754864
		 0.27256575 2.027969837 4.16289473 -0.27256367 2.027970791 4.16289711 -0.26733947 2.14462566 4.18754721
		 -0.33044851 2.3393321 4.18873405 -0.2673384 2.52808332 4.1869421 -0.10211371 2.70130205 4.17586851
		 0.4493106 2.1181097 3.42034578 -0.44930941 2.11811066 3.42034721 -0.44069555 2.31374168 3.44238257
		 -0.54472888 2.43378496 3.47803903 -0.44069493 2.85511875 3.44492483 -0.1683297 3.20866466 3.43130255
		 0.1683304 3.20866466 3.43130255 0.44069365 2.85511971 3.44492531 0.54472953 2.43378401 3.47803903
		 0.44069433 2.31374049 3.442384 0.33369607 2.059146404 3.90607119 -0.33369428 2.059147358 3.90607309
		 -0.32729763 2.20311689 3.92981911 -0.40456116 2.36163688 3.92954063 -0.32729673 2.67496777 3.93819571
		 -0.12501566 2.88740993 3.92621589 0.12501621 2.88740993 3.92621589 0.32729599 2.6749692 3.93819571
		 0.40456212 2.36163592 3.92954063 0.32729638 2.20311689 3.92981911 0.38901889 2.087361574 3.67364478
		 -0.38901743 2.087362051 3.67364669 -0.38155994 2.25605226 3.69657516 -0.4716332 2.39204931 3.73367548
		 -0.38155907 2.83760142 3.60521817 -0.14574192 3.059777975 3.64852262 0.1457426 3.059777975 3.64852262
		 0.38155815 2.83760309 3.60521817 0.47163409 2.39204836 3.73367357 0.38155872 2.2560513 3.69657564
		 0.47971633 2.83528805 1.79451799 0.3880983 2.59571075 1.91648293 0.1482408 2.469769 2.010976553
		 -0.1482401 2.46976995 2.010976553 -0.38809842 2.59571075 1.91648293 -0.47971609 2.83528805 1.79451799
		 -0.38809842 3.0032637119 1.61069393 -0.14823984 3.12920475 1.51619971 0.14824051 3.12920403 1.51619971
		 0.38809779 3.0032639503 1.61069393 0.56142008 2.55192852 1.35724211 0.45419788 2.37426949 1.55259216
		 0.17348868 2.26446939 1.7094624 -0.1734879 2.26447034 1.7094624 -0.45419809 2.37426925 1.55259216
		 -0.56141984 2.55192852 1.35724211 -0.45419809 2.72958803 1.044946313 -0.17348771 2.83938837 0.8880806
		 0.17348832 2.83938742 0.8880806 0.45419741 2.72958875 1.044946194 0.31988159 2.86147857 2.35546446
		 0.25878933 2.69361329 2.35985732 0.098849058 2.58986568 2.38168716 -0.098848529 2.58986616 2.38168764
		 -0.25878933 2.69361329 2.35985732 -0.31988132 2.86147857 2.35546446 -0.25878933 3.029344559 2.28921175
		 -0.098848231 3.13309145 2.26737976 0.098848879 3.13309121 2.26737976 0.25878891 3.029345274 2.28921175
		 -0.39649647 2.37587166 3.94317961 -0.3282463 2.65264773 3.95082283 -0.33103028 2.35616922 4.17213249
		 -0.27528304 2.52289963 4.17054987 0.32824573 2.65264821 3.95082283 0.39649743 2.37587118 3.94317746
		 0.27528271 2.52290058 4.17054987 0.33103144 2.35616755 4.17213011 -0.30878389 2.35177445 3.91200089
		 -0.24053374 2.62855053 3.91964388 -0.24331775 2.33207202 4.14095354 -0.18757048 2.49880266 4.13937092
		 0.2405331 2.62855101 3.91964388 0.3087849 2.35177398 3.91199899 0.18757023 2.49880362 4.13937092
		 0.24331899 2.33207059 4.14095306 -0.36392063 2.841012 3.62790966 -0.15954569 3.033566236 3.66544056
		 -0.31689316 2.70006299 3.91649175 -0.14158289 2.88418007 3.90610886 0.1595462 3.033566236 3.66544056
		 0.36391968 2.84101343 3.62790966 0.14158335 2.88418007 3.90610886 0.3168925 2.70006466 3.91649175
		 -0.36643833 2.85666871 3.65175104 -0.18250093 3.02996707 3.68552685 -0.32411361 2.72981501 3.91147304
		 -0.16633439 2.89551997 3.90212917 0.18250138 3.02996707 3.68552876 0.36643749 2.85667086 3.65175104
		 0.16633475 2.89551997 3.90212965 0.32411301 2.72981644 3.91147304 -0.34916773 2.85198236 3.66050696
		 -0.18362406 3.0079510212 3.69090557 -0.31107551 2.73781371 3.89425564 -0.16907421 2.88694811 3.88584709
		 0.18362446 3.0079510212 3.69090772 0.34916702 2.85198426 3.66050696 0.16907459 2.88694811 3.88584781
		 0.31107503 2.73781538 3.89425564 -0.34297663 2.85317993 3.66847181 -0.18846919 2.99875116 3.69684362
		 -0.30742386 2.74662232 3.88663769 -0.17488927 2.88581443 3.87878895 0.18846953 2.99875116 3.69684577
		 0.34297588 2.85318184 3.66847181 0.17488965 2.88581443 3.87878895 0.30742338 2.74662423 3.88663769
		 -0.33864278 2.97904801 3.69640398 -0.1918608 3.11458182 3.65791321 -0.30486774 2.98247123 3.92703676
		 -0.17895979 3.097042561 3.86059546 0.19186106 3.11458278 3.65791512 0.33864215 2.97904968 3.69640303
		 0.17896017 3.097042561 3.86059546 0.30486721 2.98247313 3.92703581 -0.35457408 2.99711204 3.6985805
		 -0.20771776 3.13267469 3.66035223 -0.32056904 3.00073647499 3.92918992 -0.19458532 3.11533976 3.86301255
		 0.20771958 3.13267684 3.66035318 0.3545734 2.99711204 3.6985805 0.19458863 3.1153388 3.86301303
		 0.32056856 3.00073862076 3.92919135 -0.35457408 2.99711204 3.6985805 -0.20771776 3.13267469 3.66035223
		 -0.32056904 3.00073647499 3.92918992 -0.19458532 3.11533976 3.86301255;
	setAttr ".vt[3154:3319]" 0.20771958 3.13267684 3.66035318 0.3545734 2.99711204 3.6985805
		 0.19458863 3.1153388 3.86301303 0.32056856 3.00073862076 3.92919135 -0.26588464 3.19963861 3.66879892
		 -0.25275201 3.18230391 3.87145948 -0.41274092 3.064076185 3.70702791 -0.37873581 3.067700863 3.93763661
		 0.2658872 3.19964099 3.66880035 0.25275624 3.18230295 3.87145996 0.37873614 3.067702532 3.93763828
		 0.41274104 3.064076185 3.70702791 -0.27445757 3.19967723 3.67779613 -0.26220044 3.18349886 3.86694527
		 -0.41152361 3.073153019 3.71347547 -0.37978524 3.07653594 3.9287107 0.27446014 3.19968009 3.67779779
		 0.26220453 3.18349791 3.86694574 0.37978572 3.076537848 3.92871165 0.41152367 3.073153019 3.71347547
		 -0.28046536 3.19283199 3.68952107 -0.26943398 3.17827082 3.85975552 -0.40382469 3.078959465 3.72163224
		 -0.3752602 3.08200407 3.91534352 0.2804679 3.1928339 3.68952227 0.26943797 3.17827082 3.85975599
		 0.37526098 3.082006454 3.91534495 0.40382522 3.078960419 3.72163415 -0.27652162 3.18416381 3.692204
		 -0.26585811 3.17008805 3.85676432 -0.39576894 3.074086905 3.72324467 -0.36815673 3.077029705 3.9104991
		 0.27652442 3.18416524 3.69220567 0.26586199 3.17008758 3.85676455 0.36815757 3.077032566 3.91050029
		 0.39576966 3.074087858 3.72324681 -0.28000635 3.18019319 3.69900465 -0.27005357 3.16705489 3.85259295
		 -0.39130372 3.077454805 3.72797585 -0.36553222 3.080202103 3.9027462 0.28000873 3.18019485 3.69900608
		 0.27005726 3.16705465 3.85259318 0.36553314 3.080205202 3.90274715 0.39130434 3.077455759 3.72797751
		 -0.31287283 3.21792054 3.70465159 -0.31707668 3.22123933 3.85902929 -0.42800224 3.11961389 3.73402095
		 -0.41811126 3.14088988 3.90913773 0.31287557 3.21792269 3.70465255 0.31708056 3.22123885 3.85902953
		 0.41811261 3.14089251 3.90913892 0.4280034 3.11961484 3.73402286 -0.32081306 3.21784306 3.7124064
		 -0.32466659 3.22088575 3.85391974 -0.42634845 3.12772751 3.73932743 -0.41728172 3.14723182 3.89985037
		 0.32081586 3.21784496 3.71240735 0.32467046 3.22088504 3.85391998 0.41728318 3.14723277 3.89985228
		 0.4263497 3.12772846 3.73932934 -0.32791463 3.21237326 3.72409987 -0.33125418 3.21501136 3.84674597
		 -0.41937864 3.13427353 3.74743295 -0.41152093 3.15117645 3.88655257 0.32791728 3.21237469 3.72410154
		 0.33125794 3.21501017 3.84674668 0.41152227 3.15117788 3.88655424 0.41937992 3.13427448 3.74743485
		 -0.32634473 3.20752454 3.72685385 -0.32957292 3.21007442 3.8454113 -0.41475984 3.13202834 3.74940872
		 -0.40716404 3.14836717 3.88388968 0.32634735 3.20752597 3.72685552 0.32957664 3.21007395 3.84541154
		 0.40716556 3.14836884 3.88389158 0.41476119 3.13202929 3.74941039 -0.3293193 3.20523381 3.73175216
		 -0.33233237 3.20761299 3.8424058 -0.41184017 3.13477063 3.7528038 -0.40475067 3.15002108 3.87831998
		 0.32932198 3.20523477 3.73175406 0.33233613 3.20761228 3.84240627 0.40475214 3.15002203 3.87832189
		 0.41184163 3.13477159 3.75280571 -0.3293193 3.20523381 3.73175216 -0.33233237 3.20761299 3.8424058
		 -0.41184017 3.13477063 3.7528038 -0.40475067 3.15002108 3.87831998 0.32932198 3.20523477 3.73175406
		 0.33233613 3.20761228 3.84240627 0.40475214 3.15002203 3.87832189 0.41184163 3.13477159 3.75280571
		 -0.36042994 3.2420032 3.73131752 -0.3805317 3.26434803 3.83787966 -0.44556609 3.17461205 3.75200939
		 -0.45792487 3.21254492 3.87219906 0.36043262 3.24200416 3.73131919 0.38053536 3.26434612 3.83788061
		 0.45792666 3.21254587 3.87220073 0.44556764 3.174613 3.7520113 -0.36958891 3.24458098 3.73614073
		 -0.38768053 3.26469111 3.83204603 -0.44621137 3.18392873 3.75476241 -0.45733449 3.21806812 3.86293316
		 0.36959156 3.24458122 3.73614097 0.38768411 3.26468897 3.83204722 0.45733637 3.21806884 3.86293507
		 0.44621316 3.18392968 3.75476456 -0.3742623 3.24277449 3.74208736 -0.39054459 3.26087499 3.82840443
		 -0.44322243 3.18818784 3.75884819 -0.45323321 3.21891308 3.85620165 0.3742649 3.24277544 3.74208903
		 0.39054787 3.26087189 3.8284061 0.45323521 3.21891356 3.85620356 0.44322425 3.18818879 3.7588501
		 -0.3721877 3.24037766 3.74302244 -0.38846999 3.25847697 3.82933855 -0.44114804 3.1857903 3.75978184
		 -0.45115867 3.21651626 3.85713506 0.37219033 3.2403779 3.74302268 0.38847354 3.25847483 3.8293395
		 0.4511607 3.21651578 3.85713744 0.44115001 3.18579102 3.75978374 -0.37849641 3.23793888 3.75105166
		 -0.39233661 3.25332403 3.82442117 -0.43711275 3.19154 3.76529813 -0.44562188 3.21765733 3.84804773
		 0.37849924 3.23793983 3.75105333 0.39233974 3.25332212 3.82442212 0.44562393 3.21765709 3.84805036
		 0.43711478 3.19154096 3.7652998 -0.40293819 3.26645112 3.74072123 -0.42646712 3.29250956 3.80838609
		 -0.46109894 3.21962094 3.75541925 -0.48092636 3.25806069 3.83112526 0.40294084 3.26645207 3.74072289
		 0.42646989 3.2925086 3.80838871 0.48092818 3.25806046 3.83112955 0.46110097 3.21962261 3.75542283
		 -0.40967867 3.26837134 3.74293184 -0.43085492 3.29182434 3.80383134 -0.46202353 3.22622442 3.75616002
		 -0.47986779 3.26081991 3.82429504 0.40968117 3.2683723 3.74293375 0.43085772 3.29182267 3.80383229
		 0.47986969 3.2608192 3.8242979 0.46202561 3.22622561 3.75616241 -0.41333699 3.26761413 3.746768
		 -0.4323957 3.28872061 3.80157685 -0.46044734 3.22968149 3.75867367 -0.47650731 3.26081634 3.81999326
		 0.41333953 3.26761484 3.74676967 0.43239847 3.28871894 3.80157781 0.47650921 3.26081657 3.81999803
		 0.46044937 3.22968221 3.75867534 -0.41322511 3.26545787 3.74984217 -0.43133062 3.28551006 3.80191088
		 -0.45797965 3.22942281 3.76115203 -0.47323686 3.25900102 3.81940556 0.41322732 3.26545858 3.74984384
		 0.43133321 3.28550816 3.80191231 0.47323868 3.25900078 3.81941032 0.45798162 3.22942376 3.76115394
		 -0.41635284 3.26480842 3.75312018 -0.43264773 3.28285694 3.79998326;
	setAttr ".vt[3320:3485]" -0.45663205 3.23237777 3.7632997 -0.47036335 3.25899935 3.81572866
		 0.41635489 3.26481032 3.75312376 0.43265069 3.2828548 3.79998422 0.47036529 3.25899911 3.81573248
		 0.4566341 3.23237872 3.76330209 -0.43724883 3.28679061 3.73764372 -0.46166691 3.31222939 3.77693844
		 -0.47567081 3.25279617 3.74972534 -0.49910226 3.28801751 3.79280996 0.43725163 3.28679228 3.73764729
		 0.46166956 3.31222749 3.77693987 0.49910483 3.28801751 3.79281449 0.47567332 3.25279713 3.74972725
		 -0.44373211 3.28814936 3.73856759 -0.46489435 3.31019664 3.77262235 -0.47703114 3.25868702 3.74903703
		 -0.49733812 3.2892127 3.7863791 0.44373494 3.28815126 3.73857117 0.4648973 3.31019568 3.77262449
		 0.49734104 3.28921175 3.78638196 0.47703373 3.25868797 3.74903893 -0.44646612 3.28791332 3.74085236
		 -0.46551198 3.30775452 3.7715013 -0.47643512 3.26139688 3.75027585 -0.49471143 3.28886914 3.78388166
		 0.44646886 3.28791451 3.7408545 0.4655149 3.30775332 3.77150416 0.49471417 3.28886819 3.78388429
		 0.47643802 3.26139712 3.75027633 -0.44696525 3.28656149 3.74433064 -0.46442392 3.30474949 3.77242517
		 -0.474437 3.26225448 3.7529695 -0.49119008 3.28743815 3.78377485 0.4469682 3.2865634 3.7443347
		 0.46442702 3.30474854 3.77242827 0.49119279 3.28743744 3.78377748 0.47443965 3.26225448 3.7529695
		 -0.44959626 3.28606534 3.74635148 -0.46501842 3.30306315 3.77178836 -0.47386312 3.26412439 3.75366974
		 -0.48866203 3.28752542 3.78164911 0.4495995 3.28606701 3.7463553 0.46502134 3.30306268 3.77179098
		 0.48866501 3.28752422 3.78165197 0.47386608 3.26412535 3.75367165 -0.49639341 3.32212591 3.69352674
		 -0.51936281 3.34245396 3.70879126 -0.51689738 3.29875827 3.70607257 -0.5409888 3.32591152 3.7212956
		 0.49639592 3.32212853 3.69353056 0.51936454 3.34245372 3.70879364 0.54099071 3.32591248 3.72129774
		 0.51689887 3.29875922 3.70607471 -0.50188309 3.32214785 3.69211245 -0.52140713 3.33942604 3.70508623
		 -0.51931155 3.30228543 3.70277596 -0.53978914 3.32536483 3.71571517 0.50188541 3.32214928 3.69211531
		 0.52140856 3.3394258 3.70508933 0.53979093 3.32536602 3.71571779 0.51931316 3.30228639 3.70277762
		 0.39335167 3.041755199 3.70421219 0.35934696 3.045381069 3.93482208 0.23336701 3.15998149 3.868644
		 0.24649802 3.17731953 3.66598439 0.37396255 3.01943326 3.70139623 0.33995777 3.023059607 3.93200636
		 0.2139778 3.1376605 3.86582851 0.22710882 3.15499806 3.66316891 0.41577014 3.10556149 3.73200703
		 0.40058622 3.1206634 3.90700769 0.30140615 3.20317745 3.85688329 0.30192006 3.20534635 3.70276999
		 0.40353751 3.091508389 3.72999215 0.38305962 3.10043406 3.90487719 0.2857317 3.18511629 3.85473847
		 0.29096437 3.19277048 3.7008884 0.43432558 3.16133213 3.75227571 0.44020191 3.19170403 3.87424064
		 0.36446887 3.24543476 3.83938861 0.35006243 3.22974777 3.73146343 0.42308366 3.14805198 3.75254083
		 0.42247716 3.17086291 3.87628174 0.34840238 3.2265234 3.84089804 0.33969223 3.21749115 3.73160839
		 0.45310551 3.2102623 3.75871444 0.46915996 3.24459243 3.83676958 0.41509336 3.2794466 3.8137331
		 0.39479351 3.25694776 3.74416614 0.44511005 3.20090151 3.76200724 0.45739186 3.2311244 3.84241033
		 0.40371653 3.2663846 3.81907749 0.38664639 3.24744344 3.74760938 0.46932715 3.24599099 3.75425196
		 0.48952481 3.27834439 3.80045366 0.45199659 3.30243683 3.78462172 0.43028596 3.27946472 3.74280667
		 0.46298057 3.23918509 3.75877714 0.47994497 3.26867175 3.80809331 0.44232363 3.29264545 3.79230309
		 0.42332059 3.27213717 3.74796557 0.50255477 3.28721428 3.72194028 0.52354878 3.31311607 3.74141574
		 0.50124991 3.32932329 3.72979236 0.48079717 3.31010747 3.71113825 0.48821038 3.27566981 3.73780584
		 0.50610697 3.30032039 3.7615335 0.48313579 3.3161931 3.75079203 0.46519831 3.2980876 3.72874665
		 -0.35934666 3.045379162 3.93482161 -0.39335185 3.041755199 3.70421219 -0.2464956 3.17731714 3.66598296
		 -0.23336311 3.15998197 3.86864376 -0.33995783 3.023057938 3.93200564 -0.37396312 3.01943326 3.70139623
		 -0.22710674 3.15499592 3.663167 -0.21397422 3.13766098 3.86582828 -0.400585 3.12066078 3.90700793
		 -0.41576949 3.10556078 3.73200583 -0.30191734 3.20534468 3.7027688 -0.30140215 3.20317769 3.85688329
		 -0.38305879 3.10043073 3.90487695 -0.40353659 3.091507912 3.7299912 -0.29096186 3.19276905 3.70088625
		 -0.28572786 3.18511677 3.85473824 -0.44020006 3.1917038 3.87423968 -0.43432412 3.16133142 3.75227404
		 -0.35005963 3.22974706 3.73146176 -0.3644653 3.24543619 3.83938789 -0.42247549 3.17086196 3.87627983
		 -0.42308205 3.14805079 3.75253844 -0.33968955 3.21749043 3.73160696 -0.34839883 3.22652483 3.84089732
		 -0.46915841 3.24459267 3.83676577 -0.4531036 3.21026111 3.75871229 -0.39479077 3.2569468 3.74416447
		 -0.41509044 3.27944803 3.81373072 -0.45738977 3.23112535 3.84240699 -0.44510803 3.20090055 3.76200557
		 -0.38664383 3.24744296 3.74760866 -0.40371341 3.26638579 3.81907558 -0.48952252 3.27834439 3.80044913
		 -0.46932468 3.24599004 3.75425005 -0.43028361 3.27946281 3.7428031 -0.45199406 3.30243826 3.78462029
		 -0.47994301 3.26867175 3.80808902 -0.46297821 3.2391839 3.758775 -0.42331812 3.27213526 3.74796176
		 -0.44232094 3.29264736 3.79230189 -0.5235464 3.31311607 3.74141288 -0.50255263 3.28721356 3.72193861
		 -0.48079443 3.31010556 3.71113467 -0.501248 3.32932377 3.72979069 -0.50610441 3.30032015 3.76153088
		 -0.48820806 3.27566862 3.73780394 -0.46519554 3.29808569 3.72874308 -0.48313341 3.31619358 3.75078988
		 0.37227124 1.85168278 0.039809614 0.79558372 1.68332994 0.081737369 0.64704168 1.37636781 0.099697202
		 0.73258048 0.90246022 0.16753757 0.59266984 0.4912214 0.235378 0.22637978 0.21086138 0.27730581
		 -0.22637971 0.21086186 0.27730581 -0.59266979 0.49122119 0.235378;
	setAttr ".vt[3486:3651]" -0.73258048 0.90246022 0.16753757 -0.59267002 1.31370127 0.099697351
		 -0.81467044 1.57255673 0.08173731 -0.44838032 1.84342217 0.039809555 -0.22637984 1.58456659 0.057769567
		 0.22637971 1.58456612 0.057769567 0.3668901 1.82856607 0.0063006431 0.77528822 1.64631331 0.027264535
		 0.56359512 1.28309464 0.045224309 0.69664198 0.88329792 0.079144478 0.56359512 0.48350382 0.11306471
		 0.21527418 0.21021718 0.13402861 -0.21527416 0.21021742 0.13402861 -0.563595 0.4835037 0.11306471
		 -0.69664204 0.88329792 0.079144478 -0.56359524 1.28309429 0.045224369 -0.78559566 1.54194999 0.027264416
		 -0.43727472 1.82244575 0.0063005239 -0.21527426 1.56359017 0.024260491 0.21527413 1.56358981 0.024260491
		 0.48817477 2.93843842 3.14544606 0.60341966 2.72564149 3.11200905 0.48817557 2.37391734 3.078574896
		 0.50241101 2.15653825 3.057908773 -0.50240993 2.15653944 3.057909966 -0.4881767 2.37391829 3.078574419
		 -0.60341918 2.72564292 3.11200905 -0.48817632 2.93843651 3.14544559 -0.18646584 3.30061698 3.14164352
		 0.1864666 3.30061698 3.14164352 0.40626347 2.90101647 2.96070099 0.50217116 2.72788215 2.93340278
		 0.40626401 2.43507957 2.90610647 0.15517974 2.2541163 2.88923383 -0.15517877 2.25411725 2.8892343
		 -0.40626484 2.43508005 2.906106 -0.5021708 2.72788286 2.93340278 -0.40626451 2.90101576 2.96070099
		 -0.15517853 3.21451521 2.92395616 0.15517919 3.21451521 2.92395616 0.46272588 2.14698815 3.18034935
		 -0.46272475 2.14698935 3.18035078 0.45669514 2.15607953 3.063790321 -0.45669407 2.15608072 3.063791275
		 0.46272588 2.06381321 3.18034935 -0.46272475 2.063814402 3.18035078 0.45669514 2.072904587 3.063790321
		 -0.45669407 2.072905779 3.063791275 0.32515174 1.73167193 3.204247 -0.32515061 1.73167372 3.204247
		 0.32091403 1.64876413 3.12181568 -0.32091296 1.64876556 3.1218152 0.32515174 1.73167193 3.20424676
		 -0.32515061 1.73167372 3.20424676 0.32091403 1.64876413 3.12181544 -0.32091296 1.64876556 3.12181497
		 0.2023904 1.60213733 4.3879838 -0.20238927 1.60213923 4.3879838 0.19975263 1.51922965 4.30555248
		 -0.19975153 1.51923108 4.30555201 -0.06746269 1.60213852 4.3879838 -0.10838314 1.73167312 3.20424676
		 -0.10838314 1.73167312 3.204247 -0.15424117 2.063813925 3.1803503 -0.15424117 2.14698887 3.1803503
		 -0.16993111 2.14653015 3.18623185 -0.14976935 2.11811042 3.42034674 -0.12967196 2.087361813 3.67364597
		 -0.11123084 2.05914712 3.90607238 -0.09085387 2.027970552 4.16289616 -0.066561908 1.99972987 4.44495392
		 0.067463852 1.60213792 4.3879838 0.10838431 1.73167253 3.20424676 0.10838431 1.73167253 3.204247
		 0.15424235 2.063813686 3.18034983 0.15424235 2.14698839 3.18034983 0.16993225 2.14652967 3.18623137
		 0.14977062 2.11811018 3.42034626 0.12967348 2.087361813 3.6736455 0.11123262 2.059146881 3.90607166
		 0.090855941 2.027970314 4.1628952 0.066564232 1.99972939 4.44495296 0.22398216 2.0091426373 4.35093307
		 0.074661478 2.0091431141 4.35093403 -0.074659228 2.0091433525 4.35093451 -0.22397998 2.0091438293 4.35093546
		 -0.21968737 2.11623096 4.38336992 -0.27154729 2.29602766 4.39732838 -0.2196863 2.44497299 4.39331436
		 -0.083912298 2.59127998 4.37818098 0.08391279 2.59127998 4.37818098 0.21968606 2.44497299 4.39331436
		 0.2715486 2.29602671 4.3973279 0.21968618 2.11623001 4.3833704 0.24827391 2.018556118 4.25691414
		 0.08275871 2.018556595 4.25691462 -0.082756542 2.018557072 4.25691509 -0.24827182 2.01855731 4.25691605
		 -0.24351342 2.13042831 4.28545856 -0.30099791 2.31767988 4.29303122 -0.24351235 2.48652816 4.29012823
		 -0.093013003 2.64629102 4.27702475 0.093013503 2.64629102 4.27702475 0.24351199 2.48652816 4.29012823
		 0.30099919 2.31767893 4.29302979 0.24351214 2.13042736 4.28545952 -0.26513979 2.3386395 4.06463623
		 -0.2052249 2.54205203 4.066128731 -0.29293746 2.566149 4.097307682 -0.28732452 2.57704496 4.10402679
		 -0.10974769 2.76333809 4.09265089 0.10974821 2.76333809 4.09265089 0.28732395 2.57704544 4.10402679
		 0.29293704 2.56614971 4.097307682 0.20522451 2.54205275 4.066128731 0.26514095 2.33863831 4.0646348
		 0.35285342 2.36273527 4.095812798 0.35515389 2.346766 4.10233498 0.28732419 2.1641221 4.10163879
		 0.29294246 2.038362026 4.07728672 0.097648144 2.038362503 4.077287197 -0.097646177 2.038362741 4.077288151
		 -0.29294059 2.03836298 4.077289104 -0.28732553 2.16412258 4.10163784 -0.35515273 2.34676695 4.10233641
		 -0.35285234 2.3627367 4.095814705 -0.28696185 2.34520698 3.98831844 -0.22287932 2.5853014 3.9928863
		 -0.31059188 2.60939837 4.024065018 -0.30731064 2.62600636 4.021111488 -0.11738168 2.82537413 4.0094332695
		 0.11738221 2.82537413 4.0094332695 0.30730999 2.62600732 4.021111488 0.3105914 2.60939884 4.024065018
		 0.22287881 2.58530188 3.9928863 0.28696293 2.34520626 3.98831701 0.37467542 2.36930323 4.01949501
		 0.37985802 2.35420084 4.015937805 0.30731028 2.1836195 4.015728951 0.31331918 2.048754215 3.99167895
		 0.10444039 2.048754692 3.99167943 -0.10443849 2.04875493 3.99168015 -0.31331736 2.048755169 3.9916811
		 -0.30731159 2.18361974 4.015728474 -0.37985694 2.35420179 4.015938759 -0.37467441 2.36930418 4.019496918
		 0.35213703 2.06855154 3.82859564 0.11737958 2.068551779 3.82859612 -0.11737787 2.068552017 3.82859683
		 -0.35213533 2.068552256 3.82859755 -0.34538507 2.22076201 3.85207129 -0.42691851 2.37177444 3.86425209
		 -0.34538418 2.72917891 3.82720327 -0.33256897 2.74704599 3.82029772 -0.33822185 2.77209949 3.8248992
		 -0.32377291 2.77586985 3.81633949 -0.31927478 2.78214145 3.81391573 -0.31612608 2.98133016 3.85015917
		 -0.33190405 2.99952841 3.85232019 -0.33190405 2.99952841 3.85232019 -0.35129294 3.021849632 3.85513592
		 -0.3706817 3.044171333 3.85795164 -0.39007086 3.066492558 3.86076689 -0.39036468 3.07540822 3.85696554
		 -0.38478169 3.080989122 3.85077286 -0.37736079 3.076048851 3.84808087;
	setAttr ".vt[3652:3817]" -0.37412274 3.079286337 3.84448957 -0.38988471 3.097456455 3.84658194
		 -0.4056465 3.11562729 3.84867382 -0.42140824 3.13379788 3.85076571 -0.42030394 3.14073038 3.84634256
		 -0.41414016 3.14554214 3.84017944 -0.40969598 3.14292097 3.83906269 -0.40711385 3.14493752 3.83648109
		 -0.40711385 3.14493752 3.83648109 -0.42267767 3.16325808 3.83503246 -0.43824142 3.18157959 3.83358455
		 -0.45380527 3.19990063 3.83213592 -0.45362681 3.2066884 3.82687616 -0.44989628 3.20867133 3.8237505
		 -0.4478218 3.20627427 3.82468414 -0.4427855 3.20895147 3.82046461 -0.45329583 3.2210505 3.81560659
		 -0.46380681 3.23314881 3.8107481 -0.47431722 3.24524736 3.80589008 -0.47391969 3.24928808 3.80158329
		 -0.47115397 3.25043821 3.79955339 -0.46815115 3.24914169 3.79998779 -0.46578622 3.25012541 3.79825234
		 -0.47428811 3.25884247 3.79165101 -0.48278993 3.26755953 3.78504944 -0.49129176 3.27627707 3.77844858
		 -0.49056911 3.27903748 3.77393198 -0.48861933 3.27971172 3.77267981 -0.48560572 3.27904367 3.77350664
		 -0.48372906 3.27972507 3.77232265 -0.50013894 3.29210281 3.75362206 -0.51654845 3.30448174 3.73492146
		 -0.53295833 3.31686044 3.71622133 -0.53296328 3.31767154 3.71140194 -0.51489913 3.33366656 3.70076156
		 -0.51170635 3.33567786 3.70370293 -0.49443012 3.32291746 3.72357202 -0.47715414 3.31015778 3.7434411
		 -0.45987767 3.29739714 3.76330948 -0.4586044 3.29868698 3.76306057 -0.45916337 3.30114079 3.76128483
		 -0.45784026 3.30284739 3.76127076 -0.45352754 3.3037498 3.7638402 -0.44475722 3.29477978 3.77068138
		 -0.43598664 3.28580999 3.77752185 -0.42721608 3.27684069 3.78436232 -0.42529544 3.27882576 3.78455472
		 -0.4260428 3.28168511 3.78330731 -0.42379618 3.2840066 3.78353167 -0.41862413 3.28382325 3.78583121
		 -0.40832388 3.27194762 3.79054189 -0.39802355 3.26007152 3.79525328 -0.38772321 3.24819565 3.79996467
		 -0.38304257 3.25244379 3.80056667 -0.38511717 3.25484157 3.79963207 -0.38164997 3.2579875 3.80007768
		 -0.37383109 3.25689983 3.8023591 -0.3596634 3.24020648 3.80341244 -0.34549573 3.22351336 3.8044672
		 -0.33132803 3.20681977 3.80552149 -0.33132803 3.20681977 3.80552149 -0.32849687 3.20922446 3.80589223
		 -0.33014101 3.21413207 3.80586386 -0.32338208 3.21987152 3.80674863 -0.31567541 3.22013283 3.80756998
		 -0.30157387 3.20389986 3.80551195 -0.28747252 3.18766737 3.8034544 -0.27337116 3.1714344 3.80139685
		 -0.26941261 3.17477989 3.80191088 -0.2731111 3.18312454 3.8030107 -0.26628616 3.18889141 3.80389547
		 -0.25712955 3.18808222 3.80390596 -0.23774061 3.16576028 3.80109024 -0.21835172 3.14343929 3.79827452
		 -0.19896279 3.12111807 3.79545903 -0.19896279 3.12111807 3.79545903 -0.18326013 3.10288906 3.79303479
		 -0.17941591 2.92346001 3.81814051 -0.17392416 2.92728233 3.82086658 -0.17172323 2.94033575 3.8299284
		 -0.14757049 2.93397546 3.82588601 -0.13192442 2.94486594 3.83365154 0.13192502 2.94486594 3.83365154
		 0.14757097 2.93397546 3.82588601 0.17172362 2.94033575 3.82992935 0.17392455 2.92728233 3.82086754
		 0.17941627 2.92346001 3.81814098 0.18326047 3.1028893 3.79303551 0.19896561 3.12111807 3.79545975
		 0.19896561 3.12111807 3.79545975 0.21835481 3.14343977 3.79827523 0.23774402 3.16576076 3.80109072
		 0.25713322 3.18808222 3.80390692 0.26628971 3.18889189 3.80389643 0.27311462 3.18312526 3.80301142
		 0.26941612 3.17478013 3.80191183 0.27337444 3.17143464 3.80139756 0.28747591 3.18766761 3.80345488
		 0.30157745 3.20390034 3.80551243 0.31567889 3.2201333 3.80757046 0.3233856 3.21987176 3.80674934
		 0.33014438 3.21413159 3.80586505 0.32850021 3.2092247 3.80589294 0.33133143 3.20681953 3.8055222
		 0.33133143 3.20681953 3.8055222 0.34549901 3.22351265 3.80446815 0.35966671 3.24020576 3.80341339
		 0.37383443 3.25689888 3.80236006 0.38165325 3.25798631 3.80007839 0.38512021 3.25483966 3.79963398
		 0.38304579 3.25244236 3.80056715 0.38772625 3.24819469 3.79996586 0.39802647 3.2600708 3.79525471
		 0.40832675 3.27194691 3.79054403 0.41862684 3.28382301 3.78583336 0.42379886 3.28400588 3.78353262
		 0.42604548 3.28168416 3.78330851 0.42529792 3.27882481 3.78455639 0.42721874 3.27683997 3.78436399
		 0.43598926 3.28580928 3.77752399 0.44475973 3.2947793 3.77068329 0.45353025 3.30374908 3.76384258
		 0.45784318 3.30284739 3.76127338 0.45916623 3.30114031 3.76128769 0.45860744 3.29868674 3.76306367
		 0.45988074 3.29739738 3.76331234 0.47715664 3.31015778 3.74344349 0.49443233 3.32291794 3.7235744
		 0.51170832 3.33567858 3.70370579 0.5149008 3.33366704 3.70076466 0.532965 3.31767273 3.71140432
		 0.53296012 3.31686139 3.71622324 0.51655078 3.30448198 3.73492384 0.50014144 3.29210353 3.7536242
		 0.48373204 3.2797246 3.77232528 0.4856084 3.2790432 3.77350807 0.48862213 3.27971101 3.77268171
		 0.49057192 3.279037 3.77393436 0.49129432 3.27627754 3.77845192 0.48279226 3.26756001 3.78505325
		 0.47429019 3.25884295 3.79165459 0.46578825 3.25012541 3.79825592 0.468153 3.24914169 3.79999161
		 0.47115594 3.25043821 3.79955721 0.47392166 3.24928808 3.80158615 0.4743191 3.24524784 3.8058939
		 0.46380848 3.23314905 3.81075144 0.45329791 3.22105002 3.81560922 0.44278753 3.20895171 3.820467
		 0.44782379 3.20627403 3.82468605 0.44989821 3.20867205 3.8237524 0.45362866 3.20668912 3.82687831
		 0.453807 3.19990158 3.83213758 0.43824312 3.18158007 3.83358574 0.42267931 3.16325927 3.83503485
		 0.40711528 3.14493847 3.836483 0.40711528 3.14493847 3.836483 0.40969744 3.1429224 3.8390646
		 0.41414148 3.14554334 3.84018135 0.42030534 3.14073133 3.84634471 0.42140955 3.13379979 3.8507669
		 0.40564752 3.11562943 3.8486743 0.38988557 3.097458839 3.84658194 0.37412354 3.079288483 3.84449053
		 0.3773616 3.076050997 3.84808254 0.38478237 3.08099103 3.85077477;
	setAttr ".vt[3818:3983]" 0.39036503 3.075409651 3.85696602 0.39007109 3.066493511 3.86076832
		 0.37068188 3.044172287 3.85795212 0.3512927 3.021850824 3.85513639 0.33190352 2.99952984 3.85232115
		 0.33190352 2.99952984 3.85232115 0.31612551 2.98133206 3.85015821 0.31927422 2.78214335 3.81391573
		 0.32377237 2.77587175 3.81633949 0.33822119 2.77210116 3.8248992 0.33256823 2.74704766 3.82029772
		 0.34538338 2.72918057 3.82720327 0.42691946 2.37177348 3.86425161 0.34538382 2.22076178 3.85207129
		 0.37057796 2.077956676 3.75112009 0.12352653 2.077956676 3.75112081 -0.12352489 2.077956915 3.75112152
		 -0.37057644 2.077957153 3.751122 -0.36347252 2.23840714 3.77432323 -0.44927585 2.38191175 3.79896379
		 -0.36347163 2.78339005 3.71621084 -0.34824479 2.794029 3.72410369 -0.35233009 2.81438398 3.73832512
		 -0.33647031 2.81392622 3.73842335 -0.33112571 2.81766081 3.74119377 -0.32738441 2.98018909 3.77328157
		 -0.34323907 2.9983201 3.77545023 -0.34323907 2.9983201 3.77545023 -0.36262804 3.020641327 3.77826595
		 -0.38201678 3.042963266 3.78108191 -0.40140587 3.065284252 3.7838974 -0.40094414 3.074280739 3.78522062
		 -0.3943032 3.079974174 3.78620243 -0.38656485 3.075067997 3.78566265 -0.38271323 3.078370571 3.78623271
		 -0.39671063 3.094482183 3.78828669 -0.41070801 3.11059403 3.79033995 -0.42470524 3.12670588 3.79239321
		 -0.42332619 3.13422894 3.792835 -0.4167594 3.13990784 3.79380608 -0.41222793 3.13747454 3.79423571
		 -0.409477 3.13985395 3.79464245 -0.409477 3.13985395 3.79464245 -0.42287987 3.15565443 3.79378557
		 -0.43628275 3.17145538 3.79292917 -0.44968569 3.18725634 3.79207277 -0.4499191 3.19530869 3.79081917
		 -0.44655937 3.19842958 3.79129934 -0.44448492 3.19603229 3.79223299 -0.43994913 3.20024586 3.79288149
		 -0.44920194 3.21097565 3.78880596 -0.4584552 3.22170496 3.7847302 -0.46770808 3.23243427 3.78065467
		 -0.46797162 3.23775625 3.77887154 -0.46580064 3.24005985 3.77911353 -0.46306539 3.23928213 3.78057003
		 -0.46120912 3.24125147 3.78077602 -0.46863317 3.24901319 3.775213 -0.47605729 3.2567749 3.76964974
		 -0.48348129 3.26453662 3.76408696 -0.48380011 3.26886225 3.76148462 -0.48252723 3.2705543 3.76147795
		 -0.48002136 3.27064896 3.76323795 -0.47879609 3.27192473 3.7629962 -0.4941735 3.28388572 3.745713
		 -0.50955057 3.29584765 3.72843003 -0.52492785 3.30780935 3.71114683 -0.52613741 3.30997849 3.70708895
		 -0.50839114 3.32790709 3.69643688 -0.5040499 3.32890177 3.69861484 -0.48761228 3.31651163 3.71735334
		 -0.47117484 3.30412173 3.73609209 -0.45473695 3.29173136 3.75483036 -0.45278484 3.29262424 3.75369549
		 -0.45281476 3.29452705 3.75106859 -0.45078617 3.29549837 3.74991918 -0.4453882 3.2952702 3.75074196
		 -0.43752041 3.2871213 3.75674224 -0.42965239 3.27897263 3.7627418 -0.42178446 3.27082443 3.76874113
		 -0.41926026 3.27214193 3.76719856 -0.41968989 3.27464962 3.76503754 -0.41673744 3.27618885 3.76323175
		 -0.41078115 3.27513719 3.7632761 -0.40155733 3.26444721 3.76735306 -0.39233369 3.25375724 3.77143097
		 -0.38310981 3.24306726 3.77550817 -0.37761515 3.24641085 3.77179456 -0.37968975 3.24880791 3.77085972
		 -0.37561944 3.25128412 3.76810932 -0.36713052 3.24945164 3.76683831 -0.3548615 3.23497677 3.76743698
		 -0.34259266 3.2205019 3.76803708 -0.33032367 3.20602679 3.7686367 -0.33032367 3.20602679 3.7686367
		 -0.3274208 3.2083745 3.76637316 -0.32902783 3.21325254 3.76498175 -0.32209757 3.21885729 3.75957751
		 -0.31427413 3.21902657 3.75611067 -0.30174559 3.20462227 3.75414038 -0.28921717 3.19021821 3.75217032
		 -0.27668875 3.17581367 3.75020075 -0.2729671 3.17947197 3.74705744 -0.27678823 3.18797827 3.74626589
		 -0.27037185 3.19428444 3.74084568 -0.26150709 3.19386053 3.73635244 -0.24211811 3.17153883 3.73353672
		 -0.22272924 3.14921761 3.73072076 -0.20334028 3.12689638 3.72790575 -0.20334028 3.12689638 3.72790575
		 -0.18756047 3.10873556 3.72547388 -0.18394256 2.96110559 3.75749207 -0.17877412 2.96761656 3.75588608
		 -0.17711207 2.98515129 3.75772762 -0.15355809 2.98377085 3.74566317 -0.13883317 3.0023219585 3.74108696
		 0.13883381 3.0023219585 3.74108696 0.15355858 2.98377085 3.74566317 0.17711249 2.98515129 3.75772905
		 0.17877451 2.96761656 3.75588751 0.1839429 2.96110559 3.7574935 0.18756077 3.10873604 3.72547531
		 0.20334259 3.12689734 3.72790647 0.20334259 3.12689734 3.72790647 0.22273181 3.14921904 3.73072195
		 0.24212101 3.17154026 3.73353767 0.26151019 3.19386148 3.73635364 0.27037492 3.19428587 3.74084711
		 0.27679127 3.1879797 3.74626684 0.27297026 3.17947268 3.74705887 0.27669159 3.17581463 3.7502017
		 0.28922015 3.19021893 3.75217152 0.30174875 3.20462322 3.75414133 0.31427723 3.219028 3.75611162
		 0.32210073 3.21885824 3.75957823 0.32903081 3.21325302 3.76498318 0.32742378 3.20837545 3.76637411
		 0.33032671 3.20602703 3.76863813 0.33032671 3.20602703 3.76863813 0.34259564 3.2205019 3.76803827
		 0.35486457 3.23497677 3.76743841 0.36713353 3.24945164 3.7668395 0.37562239 3.25128365 3.7681098
		 0.37969255 3.24880743 3.77086163 0.37761807 3.24641013 3.7717948 0.38311273 3.24306726 3.7755096
		 0.39233643 3.253757 3.77143192 0.40156013 3.26444721 3.76735497 0.41078383 3.27513742 3.76327801
		 0.41674 3.27618909 3.76323318 0.41969252 3.27464962 3.76503897 0.41926262 3.2721417 3.76719999
		 0.42178681 3.27082515 3.76874399 0.42965493 3.2789731 3.7627449 0.43752283 3.28712201 3.75674486
		 0.44539094 3.29527068 3.75074482 0.45078906 3.29549932 3.74992228 0.45281756 3.29452753 3.75107098
		 0.45278782 3.29262495 3.7536993 0.45474011 3.29173231 3.7548337 0.47117746 3.30412269 3.73609495
		 0.48761475 3.31651258 3.7173562 0.5040521 3.32890368 3.69861817 0.50839311 3.32790804 3.69643998
		 0.52613908 3.30997944 3.70709085 0.52492952 3.30781031 3.71114898;
	setAttr ".vt[3984:4149]" 0.50955278 3.29584813 3.72843218 0.49417591 3.28388667 3.74571514
		 0.47879905 3.27192497 3.76299858 0.48002404 3.27064896 3.76323891 0.48253006 3.27055407 3.7614789
		 0.48380283 3.26886249 3.76148653 0.48348382 3.26453733 3.76408958 0.47605971 3.25677538 3.76965261
		 0.46863538 3.2490139 3.77521586 0.46121117 3.24125195 3.78077888 0.46306729 3.23928261 3.78057289
		 0.46580267 3.24006033 3.77911615 0.46797365 3.23775673 3.7788744 0.46771002 3.23243523 3.78065825
		 0.45845699 3.22170568 3.78473282 0.44920397 3.21097565 3.78880835 0.43995115 3.20024633 3.7928834
		 0.44448692 3.19603252 3.7922349 0.44656122 3.19843054 3.79130125 0.44992089 3.1953094 3.79082155
		 0.4496873 3.18725729 3.79207444 0.43628436 3.1714561 3.7929306 0.42288148 3.15565562 3.79378796
		 0.40947846 3.13985491 3.79464436 0.40947846 3.13985491 3.79464436 0.4122293 3.13747597 3.79423761
		 0.41676068 3.13990879 3.79380798 0.42332751 3.1342299 3.79283714 0.42470646 3.12670732 3.79239488
		 0.41070884 3.11059546 3.79034066 0.39671153 3.094483614 3.78828716 0.38271394 3.078372002 3.7862339
		 0.38656563 3.075069427 3.78566456 0.3943038 3.079975605 3.78620434 0.40094435 3.074281216 3.78522062
		 0.40140605 3.065284729 3.78389812 0.38201678 3.042963743 3.78108215 0.36262763 3.020642042 3.77826643
		 0.34323847 2.99832106 3.77545071 0.34323847 2.99832106 3.77545071 0.32738382 2.98019075 3.77328062
		 0.33112505 2.81766272 3.74119377 0.33646971 2.81392813 3.73842335 0.35232934 2.81438589 3.73832512
		 0.34824395 2.79403067 3.72410369 0.36347076 2.78339195 3.71621084 0.44927678 2.3819108 3.79896259
		 0.36347127 2.23840666 3.77432346 0.40911618 2.09761095 3.58921194 0.13637251 2.097611427 3.58921242
		 -0.13637111 2.097611427 3.58921289 -0.40911466 2.097611666 3.58921337 -0.40127182 2.27528214 3.6118443
		 -0.49599841 2.40596128 3.64846325 -0.40127102 2.84344053 3.5517869 -0.15327118 3.10940695 3.57611585
		 0.15327187 3.10940695 3.57611585 0.40126997 2.84344196 3.55178714 0.49599922 2.40596008 3.64846182
		 0.40127057 2.27528095 3.61184502 0.42921332 2.10786033 3.50477886 0.14307155 2.1078608 3.50477934
		 -0.14307019 2.10786104 3.50477982 -0.429212 2.10786104 3.50478029 -0.42098367 2.2945118 3.52711344
		 -0.52036363 2.41987324 3.56325102 -0.42098296 2.84927964 3.49835587 -0.16080044 3.15903568 3.50370932
		 0.16080114 3.15903568 3.50370932 0.42098182 2.84928083 3.49835634 0.5203644 2.41987205 3.56325054
		 0.42098245 2.29451084 3.52711439 0.21186176 1.61213124 4.29665518 0.20910056 1.52922356 4.21422386
		 -0.20909944 1.52922499 4.21422338 -0.21186063 1.61213315 4.29665518 -0.070619822 1.61213243 4.29665518
		 0.070620969 1.61213183 4.29665518 0.22153226 1.62233531 4.20340681 0.21864499 1.53942764 4.12097549
		 -0.2186439 1.53942907 4.12097502 -0.22153112 1.6223371 4.20340681 -0.073843315 1.62233651 4.20340681
		 0.073844463 1.62233591 4.20340681 0.2328573 1.63428521 4.094203949 0.22982243 1.55137753 4.011772633
		 -0.22982135 1.55137897 4.011772156 -0.23285617 1.634287 4.094203949 -0.077618323 1.6342864 4.094203949
		 0.077619478 1.63428581 4.094203949 0.24345373 1.64546633 3.99202681 0.24028078 1.56255865 3.90959549
		 -0.24027969 1.56256008 3.90959501 -0.24345259 1.64546812 3.99202681 -0.081150472 1.64546752 3.99202681
		 0.081151627 1.64546692 3.99202681 0.25530183 1.65796816 3.87778091 0.2519744 1.57506037 3.7953496
		 -0.25197333 1.5750618 3.79534912 -0.2553007 1.65796995 3.87778091 -0.085099831 1.65796924 3.87778091
		 0.085100979 1.65796876 3.87778091 0.26725003 1.67057562 3.76256895 0.26376694 1.58766782 3.68013763
		 -0.26376581 1.58766925 3.6801374 -0.2672489 1.67057741 3.76256895 -0.089082569 1.67057669 3.76256895
		 0.089083724 1.67057621 3.76256895 0.27728197 1.68116105 3.66583538 0.27366811 1.59825325 3.58340406
		 -0.27366704 1.59825468 3.58340383 -0.27728084 1.68116283 3.66583538 -0.092426546 1.68116212 3.66583538
		 0.092427701 1.68116164 3.66583538 0.28696197 1.69137502 3.57249522 0.28322196 1.60846734 3.49006367
		 -0.28322086 1.60846877 3.49006367 -0.28696081 1.69137681 3.57249522 -0.095653214 1.69137621 3.57249522
		 0.095654368 1.69137561 3.57249522 0.29882252 1.70388997 3.45812893 0.29492792 1.62098217 3.37569737
		 -0.29492682 1.6209836 3.37569714 -0.29882139 1.70389175 3.45812893 -0.099606723 1.70389116 3.45812893
		 0.09960787 1.70389056 3.45812893 0.308523 1.71412575 3.36459088 0.30450201 1.63121784 3.28215933
		 -0.30450094 1.63121939 3.28215933 -0.30852187 1.71412754 3.36459088 -0.10284022 1.71412683 3.36459088
		 0.10284138 1.71412635 3.36459088 0.31841445 1.72456288 3.26921177 0.31426454 1.64165509 3.18678045
		 -0.31426346 1.64165652 3.18677998 -0.31841338 1.72456479 3.26921177 -0.10613738 1.72456408 3.26921177
		 0.10613853 1.7245636 3.26921177 -0.066583462 1.5192306 4.30555201 -0.069699436 1.5292244 4.21422338
		 -0.072880924 1.53942859 4.12097549 -0.076606736 1.55137849 4.011772156 -0.080092847 1.5625596 3.90959501
		 -0.083990723 1.57506132 3.79534936 -0.08792156 1.58766866 3.6801374 -0.091221943 1.5982542 3.58340406
		 -0.09440656 1.60846829 3.49006367 -0.098308563 1.62098312 3.37569714 -0.10149992 1.63121879 3.28215933
		 -0.10475409 1.64165604 3.18677998 -0.10697059 1.64876509 3.1218152 -0.10697059 1.64876509 3.1218152
		 -0.15223096 2.07290554 3.063791037 -0.15223096 2.15608025 3.063791037 -0.16746958 2.15653896 3.057909489
		 -0.051725924 2.25411701 2.88923407 -0.039658166 2.36367702 2.77532578 -0.028682662 2.52181387 2.61193895
		 -0.026036544 2.60072565 2.51251292 -0.032949325 2.58986592 2.38168764 -0.042041484 2.5755825 2.20961714
		 -0.049413122 2.46976972 2.010976553 -0.056440901 2.36889267 1.82160199 -0.05782903 2.2644701 1.7094624
		 -0.059047595 2.1728034 1.61102366 -0.063970298 1.98446274 1.57049179;
	setAttr ".vt[4150:4315]" -0.070556395 1.78697896 1.60524678 -0.082684398 1.44854724 1.68340218
		 -0.098756596 1.000053882599 1.78697419 -0.10615166 0.83286011 1.82889986 -0.10743117 0.74910295 1.79280555
		 -0.10503468 0.65030682 1.64501047 -0.10137259 0.52890855 1.55445373 -0.10137283 0.24156958 1.28024435
		 -0.090267256 0.23963636 0.85041165 -0.079161711 0.21150613 0.42058295 -0.07545986 0.2108617 0.27730581
		 -0.071758024 0.21021733 0.13402861 -0.068056166 0.20957291 -0.0092485696 -0.068056166 0.23576939 -0.22948197
		 -0.068056166 0.23576939 -0.44971639 -0.068056166 0.2279076 -0.6699509 -0.068056166 0.21218371 -0.89018536
		 -0.062749036 0.20981944 -1.092948437 -0.051685005 0.26160544 -1.2957114 -0.046018083 0.26262462 -1.4984746
		 -0.03531386 0.31102717 -1.70123744 -0.02393603 0.39269191 -1.7472167 -0.028413553 0.21681744 -1.87038898
		 -0.029848192 -0.018713415 -2.29355741 -0.023613377 -0.10316044 -2.80846763 -0.020564204 -0.10684454 -3.12565422
		 -0.018591335 -0.14440733 -3.42748404 -0.01648357 -0.1481716 -3.79097462 -0.014694844 -0.17237145 -4.076628208
		 -0.013327309 -0.17790195 -4.3091898 -0.011708793 -0.18716735 -4.58443213 -0.0095353229 -0.16564006 -4.77436447
		 -0.0081789121 -0.15171486 -5.044176579 -0.0098974453 -0.21060584 -5.3883009 -0.009386478 -0.21700603 -5.6902051
		 -0.0041401591 -0.12768567 -5.87151098 0.066584572 1.51923013 4.30555248 0.069700561 1.52922392 4.21422386
		 0.072882034 1.53942811 4.12097549 0.076607853 1.55137801 4.011772156 0.080093972 1.56255913 3.90959525
		 0.083991855 1.57506084 3.7953496 0.087922692 1.58766818 3.68013763 0.091223091 1.59825373 3.58340406
		 0.094407693 1.60846782 3.49006367 0.098309681 1.62098265 3.37569714 0.10150105 1.63121831 3.28215933
		 0.10475522 1.64165556 3.18678021 0.10697172 1.64876461 3.1218152 0.10697172 1.64876461 3.12181544
		 0.15223208 2.072905064 3.063790798 0.15223208 2.15607977 3.063790798 0.16747072 2.15653849 3.057909012
		 0.051726907 2.25411654 2.88923407 0.039659008 2.36367679 2.77532578 0.028683264 2.52181387 2.61193895
		 0.02603703 2.60072565 2.51251268 0.032949865 2.58986568 2.3816874 0.042042114 2.57558227 2.20961714
		 0.049413841 2.46976948 2.010976553 0.056441694 2.36889219 1.82160211 0.057829823 2.26446962 1.7094624
		 0.059048377 2.17280293 1.61102366 0.063971162 1.98446226 1.57049203 0.070557281 1.78697836 1.60524702
		 0.082685411 1.44854665 1.68340254 0.098757766 1.000053167343 1.78697455 0.10615284 0.83285928 1.82890022
		 0.10743231 0.74910212 1.79280591 0.10503565 0.65030646 1.64501071 0.10137337 0.52890801 1.55445385
		 0.10137303 0.24156934 1.28024435 0.090267442 0.23963636 0.85041165 0.079161823 0.21150589 0.42058295
		 0.075459957 0.21086153 0.27730581 0.071758077 0.21021725 0.13402861 0.068056226 0.20957291 -0.0092485696
		 0.068056226 0.23576939 -0.22948197 0.068056226 0.23576939 -0.44971639 0.068056226 0.2279076 -0.6699509
		 0.068056226 0.21218371 -0.89018536 0.062749073 0.20981944 -1.092948437 0.051685028 0.26160544 -1.2957114
		 0.046018057 0.26262462 -1.4984746 0.035313819 0.31102717 -1.70123744 0.023935957 0.39269191 -1.7472167
		 0.02841348 0.21681744 -1.87038898 0.029848119 -0.018713415 -2.29355741 0.023613315 -0.10316044 -2.80846763
		 0.02056415 -0.10684454 -3.12565422 0.018591285 -0.14440733 -3.42748404 0.016483527 -0.1481716 -3.79097462
		 0.014694806 -0.17237145 -4.076628208 0.013327271 -0.17790188 -4.3091898 0.011708753 -0.18716735 -4.58443213
		 0.0095352829 -0.16564006 -4.77436447 0.0081788711 -0.15171486 -5.044176579 0.009897409 -0.21060577 -5.3883009
		 0.0093864454 -0.21700603 -5.6902051 0.0041401209 -0.12768567 -5.87151098 -0.10058844 2.027437925 4.16821575
		 -0.26109877 2.027438164 4.16821671 -0.093435802 2.019122601 4.25126648 -0.23964095 2.019122839 4.25126648
		 -0.14413893 2.086823463 3.67808199 -0.37322742 2.086823702 3.6780827 -0.13870899 2.078515768 3.74651861
		 -0.35693789 2.078516006 3.74651909 0.14414045 2.086823463 3.67808104 0.13871063 2.07851553 3.7465179
		 0.37322891 2.086823225 3.6780808 0.35693941 2.07851553 3.74651718 0.10059051 2.027437687 4.1682148
		 0.093437955 2.019122124 4.251266 0.26110086 2.02743721 4.16821432 0.23964307 2.019121647 4.25126553
		 0.082506686 2.0086128712 4.35623074 0.075354129 2.00029730797 4.43928146 0.21440661 2.0086123943 4.35623074
		 0.19294891 2.00029683113 4.43928051 -0.082504436 2.0086131096 4.35623217 -0.21440443 2.0086135864 4.35623312
		 -0.075351804 2.00029778481 4.43928242 -0.19294664 2.00029802322 4.43928337 0.11589403 2.04816246 3.99655604
		 0.10989422 2.038982868 4.072176456 0.30040362 2.04816246 3.99655557 0.28240418 2.038982391 4.07217598
		 -0.11589213 2.048163176 3.99655676 -0.30040181 2.048162937 3.99655771 -0.10989225 2.038983107 4.07217741
		 -0.28240231 2.038983345 4.072178364 0.13041225 2.068013906 3.83302546 0.12498244 2.059706688 3.90146208
		 0.33778131 2.068013668 3.83302498 0.32149184 2.059706211 3.9014616 -0.13041054 2.068014145 3.83302617
		 -0.33777961 2.068014383 3.83302641 -0.12498066 2.05970645 3.90146279 -0.32149005 2.059706688 3.90146351
		 0.15904222 2.10727382 3.50961447 0.15312475 2.098220348 3.58419704 0.41180077 2.10727334 3.50961399
		 0.3940483 2.098219872 3.58419657 -0.15904087 2.10727406 3.50961494 -0.41179946 2.10727406 3.50961542
		 -0.15312335 2.098220348 3.58419752 -0.39404678 2.098220587 3.584198 -0.21255282 1.62174988 4.20877314
		 -0.08209525 1.62174928 4.20877314 -0.20401055 1.61273634 4.29114199 -0.079247832 1.61273563 4.29114199
		 0.082096398 1.62174869 4.20877314 0.21255395 1.62174809 4.20877314 0.07924898 1.61273503 4.29114199
		 0.20401168 1.61273444 4.29114199 -0.23358701 1.64482462 3.99790668 -0.090220138 1.64482403 3.99790668
		 -0.22422683 1.6349479 4.088163376 -0.087100066 1.63494754 4.088163376 0.090221278 1.64482343 3.99790668
		 0.23358814 1.64482284 3.99790668 0.087101221 1.6349467 4.088163376 0.22422795 1.63494635 4.088163376
		 -0.25640711 1.66985214 3.76919746 -0.099026874 1.66985142 3.76919746;
	setAttr ".vt[4316:4481]" -0.24585289 1.65871549 3.87096786 -0.095508784 1.65871477 3.87096786
		 0.099028029 1.66985083 3.76919746 0.25640824 1.66985035 3.76919746 0.095509931 1.65871429 3.87096786
		 0.24585402 1.6587137 3.87096786 -0.27543768 1.69078767 3.57787776 -0.10644928 1.69078732 3.57787776
		 -0.26688704 1.68176556 3.66032839 -0.10359905 1.68176484 3.66032839 0.10645042 1.69078648 3.57787776
		 0.27543879 1.69078612 3.57787776 0.1036002 1.68176413 3.66032839 0.26688817 1.68176377 3.66032839
		 -0.29615945 1.71353698 3.36998773 -0.11447401 1.71353626 3.36998773 -0.28759068 1.70449519 3.45261288
		 -0.11161774 1.70449483 3.45261288 0.11447516 1.71353579 3.36998773 0.29616058 1.71353519 3.36998773
		 0.11161889 1.70449424 3.45261335 0.28759181 1.70449364 3.45261335 -0.10058844 2.024036646 4.16787529
		 -0.26109877 2.024036884 4.16787624 -0.093435802 2.015721321 4.25092602 -0.23964095 2.01572156 4.25092602
		 -0.14413893 2.083429813 3.67767 -0.37322742 2.083430052 3.67767072 -0.13870899 2.075122118 3.74610662
		 -0.35693789 2.075122356 3.7461071 0.14414045 2.083429813 3.67766905 0.13871063 2.07512188 3.74610591
		 0.37322891 2.083429575 3.67766881 0.35693941 2.07512188 3.74610519 0.1005905 2.024036407 4.16787434
		 0.09343794 2.015720844 4.25092554 0.26110086 2.024035931 4.16787386 0.23964307 2.015720367 4.25092506
		 0.082506672 2.0052115917 4.35589027 0.075354114 1.99689603 4.438941 0.21440661 2.0052111149 4.35589027
		 0.19294891 1.99689555 4.43894005 -0.082504451 2.0052118301 4.3558917 -0.21440443 2.005212307 4.35589266
		 -0.075351819 1.99689651 4.43894196 -0.19294664 1.99689674 4.43894291 0.11589403 2.04476881 3.99614406
		 0.10989422 2.035589218 4.071764469 0.30040362 2.04476881 3.99614358 0.28240418 2.035588741 4.071763992
		 -0.11589213 2.044769526 3.99614477 -0.30040181 2.044769287 3.99614573 -0.10989225 2.035589457 4.071765423
		 -0.28240231 2.035589695 4.071766376 0.13041225 2.064620256 3.83261347 0.12498242 2.056313038 3.90105009
		 0.33778131 2.064620018 3.83261299 0.32149184 2.056312561 3.90104961 -0.13041054 2.064620495 3.83261418
		 -0.33777961 2.064620733 3.83261442 -0.12498066 2.056312799 3.90105081 -0.32149005 2.056313038 3.90105152
		 0.15904222 2.10388017 3.50920248 0.15312475 2.094826698 3.58378506 0.41180077 2.10387969 3.509202
		 0.3940483 2.094826221 3.58378458 -0.15904087 2.10388041 3.50920296 -0.41179946 2.10388041 3.50920343
		 -0.15312335 2.094826698 3.58378553 -0.39404678 2.094826937 3.58378601 -0.21255279 1.62514806 4.20914507
		 -0.082095236 1.62514746 4.20914507 -0.20401052 1.61613452 4.29151392 -0.079247817 1.61613381 4.29151392
		 0.082096413 1.62514687 4.20914507 0.21255398 1.62514627 4.20914507 0.079248995 1.61613321 4.29151392
		 0.20401171 1.61613262 4.29151392 -0.23358701 1.6482228 3.99827862 -0.090220124 1.64822221 3.99827862
		 -0.22422683 1.63834608 4.088535309 -0.087100051 1.63834572 4.088535309 0.090221293 1.64822161 3.99827862
		 0.23358814 1.64822102 3.99827862 0.087101236 1.63834488 4.088535309 0.22422795 1.63834453 4.088535309
		 -0.25640711 1.67325032 3.7695694 -0.099026859 1.6732496 3.7695694 -0.24585286 1.66211367 3.8713398
		 -0.095508769 1.66211295 3.8713398 0.099028043 1.67324901 3.7695694 0.25640824 1.67324853 3.7695694
		 0.095509946 1.66211247 3.8713398 0.24585402 1.66211188 3.8713398 -0.27543768 1.69418585 3.57824969
		 -0.10644927 1.6941855 3.57824969 -0.26688704 1.68516374 3.66070032 -0.10359903 1.68516302 3.66070032
		 0.10645042 1.69418466 3.57824969 0.27543879 1.6941843 3.57824969 0.1036002 1.68516231 3.66070032
		 0.26688817 1.68516195 3.66070032 -0.29615945 1.71693516 3.37035966 -0.11447399 1.71693444 3.37035966
		 -0.28759068 1.70789337 3.45298481 -0.11161773 1.70789301 3.45298481 0.11447518 1.71693397 3.37035966
		 0.29616058 1.71693337 3.37035966 0.11161891 1.70789242 3.45298529 0.28759181 1.70789182 3.45298529
		 -0.16987085 1.89003301 4.19270134 -0.1795015 1.89003325 4.19270039 -0.1694417 1.88953424 4.19768429
		 -0.17821401 1.88953424 4.19768429 -0.2471025 1.9496876 3.69347143 -0.26084781 1.94968772 3.69347143
		 -0.24677671 1.94918907 3.69757795 -0.25987047 1.94918919 3.69757748 0.24710372 1.94968748 3.69346905
		 0.24677794 1.94918907 3.69757509 0.260849 1.94968724 3.69346905 0.25987166 1.94918907 3.69757509
		 0.16987237 1.89003277 4.19269848 0.16944322 1.889534 4.1976819 0.17950299 1.89003277 4.19269896
		 0.17821553 1.889534 4.19768143 0.1383415 1.87122416 4.38055515 0.13791235 1.87072539 4.3855381
		 0.14625549 1.87122416 4.38055515 0.14496803 1.87072515 4.3855381 -0.13834034 1.8712244 4.38055754
		 -0.14625433 1.8712244 4.38055754 -0.13791119 1.87072539 4.38554001 -0.14496687 1.87072563 4.38554096
		 0.19741106 1.91064596 4.015082359 0.19705108 1.91009521 4.019619465 0.20848164 1.91064596 4.015081882
		 0.20740169 1.91009498 4.019618988 -0.19740942 1.91064572 4.01508379 -0.20848 1.91064572 4.01508379
		 -0.19704944 1.91009498 4.019620895 -0.20740005 1.91009498 4.019621372 0.22316739 1.93088365 3.84836721
		 0.22284159 1.93038511 3.85247326 0.23560953 1.93088341 3.84836721 0.23463215 1.93038511 3.85247326
		 -0.22316614 1.93088388 3.84836769 -0.23560826 1.93088365 3.84836769 -0.22284034 1.93038535 3.85247397
		 -0.23463093 1.93038511 3.85247421 0.27270746 1.96979296 3.52784395 0.2723524 1.96924973 3.53231907
		 0.28787294 1.9697926 3.52784395 0.28680781 1.96924973 3.53231859 -0.27270645 1.96979332 3.52784443
		 -0.28787199 1.96979332 3.52784443 -0.27235141 1.96924984 3.53231955 -0.28680682 1.96924996 3.53231907
		 -0.1486522 1.75105596 4.26157093 -0.14082472 1.75105596 4.26157093 -0.14813963 1.7505151 4.26651287
		 -0.14065388 1.7505151 4.26651287 0.14082722 1.75105429 4.26157045 0.14865467 1.75105429 4.26157045
		 0.14065638 1.75051355 4.26651239 0.14814213 1.75051343 4.26651239;
	setAttr ".vt[4482:4647]" -0.16337185 1.77373028 4.054362297 -0.15476982 1.77373004 4.054362297
		 -0.16281024 1.77313769 4.059778214 -0.15458262 1.77313757 4.059778214 0.15477172 1.77372909 4.054362297
		 0.16337372 1.77372909 4.054362297 0.15458451 1.7731365 4.059778214 0.16281211 1.77313638 4.059778214
		 -0.1792441 1.79817438 3.83098078 -0.16980129 1.79817438 3.83098078 -0.17861083 1.79750633 3.83708715
		 -0.1695902 1.79750609 3.83708715 0.1698035 1.79817367 3.83098078 0.17924631 1.79817367 3.83098078
		 0.16959241 1.79750562 3.83708715 0.17861307 1.79750538 3.83708715 -0.19342527 1.82008088 3.63079333
		 -0.18328598 1.820081 3.63079333 -0.19291225 1.81953955 3.63574028 -0.18311498 1.81953955 3.63574028
		 0.18328778 1.82007957 3.63079357 0.19342709 1.82007957 3.63079357 0.18311676 1.81953835 3.63574076
		 0.19291404 1.81953824 3.63574076 -0.20817403 1.84281874 3.42300582 -0.19727288 1.8428185 3.42300582
		 -0.2076599 1.8422761 3.42796326 -0.19710152 1.84227622 3.42796326 0.19727488 1.84281754 3.42300653
		 0.20817602 1.84281766 3.42300653 0.1971035 1.84227514 3.42796373 0.20766187 1.84227514 3.42796373
		 0.14695919 1.87596107 4.38775396 0.1490837 1.87678444 4.37953186 0.1360244 1.87678444 4.37953186
		 0.13531624 1.8759613 4.38775396 -0.13646059 1.87365091 4.38677835 -0.13704565 1.87433124 4.37998581
		 -0.14783457 1.87433124 4.37998581 -0.14607941 1.87365115 4.38677883 -0.16759679 1.8925972 4.19897652
		 -0.16818914 1.89328563 4.19209862 -0.18148214 1.89328587 4.19209766 -0.17970505 1.8925972 4.19897652
		 -0.19489108 1.91320264 4.02091217 -0.19539072 1.91396713 4.014614582 -0.21075635 1.91396713 4.014614582
		 -0.20925741 1.91320264 4.020912647 -0.22039613 1.93353069 3.85368729 -0.22084941 1.93422413 3.8479743
		 -0.23816016 1.93422389 3.8479743 -0.23680037 1.93353045 3.85368752 -0.24397057 1.9524591 3.698838
		 -0.24442889 1.95316041 3.69306111 -0.26376593 1.95316052 3.69306135 -0.26239097 1.95245922 3.69883752
		 0.17988417 1.89296174 4.19912767 0.18171953 1.89367282 4.19202471 0.16799039 1.89367282 4.19202423
		 0.16737859 1.89296174 4.19912815 -0.26919353 1.9725759 3.53368258 -0.26969588 1.97334468 3.52735066
		 -0.29115435 1.97334468 3.52735066 -0.28964719 1.97257602 3.53368211 0.20890577 1.9126116 4.020664692
		 0.210325 1.91333556 4.014701843 0.19577636 1.91333556 4.01470232 0.19530328 1.91261184 4.020665169
		 0.23730378 1.93425846 3.85396719 0.23875214 1.93499684 3.84788275 0.22031441 1.93499708 3.84788275
		 0.21983163 1.93425846 3.85396743 0.26198536 1.95193124 3.69863176 0.26329613 1.95259953 3.69312501
		 0.24486163 1.95259976 3.69312501 0.24442472 1.95193124 3.69863176 0.29018381 1.97320294 3.53393865
		 0.29177427 1.97401369 3.52725697 0.26912922 1.97401404 3.52725697 0.26859903 1.97320294 3.53393912
		 -0.19299997 1.83582842 3.42916369 -0.21149501 1.8358283 3.42916369 -0.21239561 1.83677888 3.42047977
		 -0.19330016 1.83677852 3.42047977 0.21072969 1.83711731 3.42892408 0.19382247 1.83711731 3.42892408
		 0.19409691 1.83798587 3.4209857 0.21155298 1.83798599 3.4209857 -0.18005258 1.81436431 3.63670158
		 -0.19576125 1.81436443 3.63670158 -0.19658381 1.81523228 3.62876964 -0.18032676 1.8152324 3.62876964
		 0.19632326 1.81334531 3.63689113 0.17945211 1.81334543 3.63689113 0.1797466 1.81427753 3.62837195
		 0.19720674 1.81427753 3.62837195 -0.16724922 1.79322767 3.83816957 -0.18073568 1.79322791 3.83816957
		 -0.18168245 1.79422677 3.82904029 -0.16756481 1.79422677 3.82904029 0.18078299 1.79313612 3.83819246
		 0.16720171 1.79313636 3.83819246 0.16751951 1.79414225 3.8289988 0.18173638 1.79414225 3.8289988
		 -0.15219183 1.76836216 4.060797215 -0.1649861 1.76836228 4.060797215 -0.16585943 1.76928377 4.052375317
		 -0.15248294 1.76928353 4.052375317 0.1642877 1.76989782 4.060469151 0.15296315 1.76989794 4.060469151
		 0.15322082 1.77071357 4.053014755 0.1650607 1.77071357 4.053014755 -0.13891362 1.74670672 4.26722145
		 -0.14972302 1.74670672 4.26722145 -0.15046315 1.74748778 4.26008511 -0.13916031 1.74748766 4.26008511
		 0.15063375 1.74452043 4.26762724 0.13791779 1.74452066 4.26762724 0.138208 1.74543917 4.25923252
		 0.15150438 1.74543905 4.25923252 0.3710098 1.84238577 3.19628096 0.3661744 1.79014432 3.10247397
		 0.12205851 1.79014468 3.10247374 -0.12205739 1.79014516 3.10247374 -0.36617333 1.79014564 3.10247374
		 -0.37100866 1.84238732 3.19628143 -0.12366915 1.84238672 3.19628143 0.12367032 1.84238625 3.19628119
		 0.41686785 1.95309949 3.18831515 0.41143477 1.93152452 3.083132267 0.1371453 1.93152487 3.083132267
		 -0.13714418 1.93152535 3.083132267 -0.4114337 1.93152571 3.083132505 -0.41686672 1.95310092 3.18831611
		 -0.13895516 1.95310032 3.18831587 0.13895634 1.95309997 3.18831539 0.076832734 2.48295307 4.49156046
		 0.16599098 2.38050604 4.50430965 0.036599841 2.24356198 4.54659271 0.20645782 2.26526308 4.50794363
		 0.17681471 2.13503313 4.49328995 -0.1768153 2.13503408 4.49328995 -0.20645674 2.2652638 4.50794363
		 -0.036599293 2.24356222 4.54659271 -0.16599089 2.38050628 4.50430965 -0.076832324 2.48295307 4.49156046
		 0.079505496 2.45078039 4.49813604 0.15059753 2.36656213 4.50816154 0.056422416 2.25563049 4.53863668
		 0.18669423 2.26245427 4.51097012 0.16566768 2.15644598 4.49969101 -0.16566801 2.15644693 4.49969101
		 -0.18669331 2.26245499 4.51097012 -0.056421902 2.25563073 4.53863668 -0.15059732 2.36656213 4.50816154
		 -0.079505101 2.45078039 4.49813604 0.079505496 2.45078039 4.41783381 0.15059753 2.36656213 4.42785931
		 0.056422416 2.25563049 4.45833445 0.18669423 2.26245427 4.43066788 0.16566768 2.15644598 4.41938877
		 -0.16566801 2.15644693 4.41938877 -0.18669331 2.26245499 4.43066788 -0.056421902 2.25563073 4.45833445
		 -0.15059732 2.36656213 4.42785931 -0.079505101 2.45078039 4.41783381;
	setAttr ".vt[4648:4657]" 0.082324587 2.42632008 4.42238474 0.14028956 2.35583401 4.43035984
		 0.070231199 2.26395154 4.45287514 0.172581 2.26174664 4.43258142 0.15738407 2.1735568 4.42378139
		 -0.15738425 2.17355776 4.42378139 -0.1725802 2.26174736 4.43258142 -0.070230722 2.26395178 4.45287514
		 -0.14028926 2.35583401 4.43036032 -0.082324192 2.42632008 4.42238474;
	setAttr -s 9368 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1 7 0 1 8 9 1
		 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 1 17 18 1 18 19 1 19 20 1
		 20 21 1 21 22 1 22 23 1 23 16 1 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 24 0 0 88 0 1 87 0 2 86 0 3 85 0 4 84 0 5 83 0 6 82 0 7 81 0 8 16 0 9 17 0 10 18 0
		 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0 18 26 0 19 27 0 20 28 0 21 29 0
		 22 30 0 23 31 0 24 32 1 25 32 1 26 32 1 27 32 1 28 32 1 29 32 1 30 32 1 31 32 1 33 69 0
		 34 70 0 33 34 1 35 71 0 34 35 1 36 72 0 35 36 1 37 65 0 36 37 1 38 66 0 37 38 1 39 67 0
		 38 39 1 40 68 0 39 40 1 40 33 1 41 50 0 42 51 0 41 42 1 43 52 0 42 43 1 44 53 0 43 44 1
		 45 54 0 44 45 1 46 55 0 45 46 1 47 56 0 46 47 1 48 49 0 47 48 1 48 41 1 49 64 0 50 57 0
		 49 50 1 51 58 0 50 51 1 52 59 0 51 52 1 53 60 0 52 53 1 54 61 0 53 54 1 55 62 0 54 55 1
		 56 63 0 55 56 1 56 49 1 57 75 0 58 76 0 57 58 1 59 77 0 58 59 1 60 78 0 59 60 1 61 79 0
		 60 61 1 62 80 0 61 62 1 63 73 0 62 63 1 64 74 0 63 64 1 64 57 1 65 41 0 66 42 0 65 66 1
		 67 43 0 66 67 1 68 44 0 67 68 1 69 45 0 68 69 1 70 46 0 69 70 1 71 47 0 70 71 1 72 48 0
		 71 72 1 72 65 1 73 15 0 74 14 0 73 74 1 75 13 0 74 75 1 76 12 0 75 76 1 77 11 0 76 77 1
		 78 10 0 77 78 1 79 9 0 78 79 1 80 8 0 79 80 1 80 73 1 81 35 0 82 36 0 81 82 1 83 37 0
		 82 83 1 84 38 0;
	setAttr ".ed[166:331]" 83 84 1 85 39 0 84 85 1 86 40 0 85 86 1 87 33 0 86 87 1
		 88 34 0 87 88 1 88 81 1 0 105 0 1 98 0 89 90 0 91 90 0 2 99 0 90 92 0 3 100 0 92 93 1
		 4 101 0 93 94 0 5 102 0 94 95 0 91 95 0 6 103 0 95 96 0 7 104 0 96 97 0 97 89 1 98 90 0
		 99 92 0 98 99 1 100 93 0 99 100 1 101 94 0 100 101 1 102 95 0 101 102 1 103 96 0
		 102 103 1 104 97 0 103 104 1 105 89 0 104 105 1 105 98 1 89 106 0 90 107 0 106 107 0
		 91 108 1 108 106 1 108 107 0 95 109 0 96 110 0 109 110 0 108 109 0 108 110 1 97 111 0
		 110 111 0 108 111 1 111 106 0 90 112 0 92 113 0 112 113 0 91 114 1 114 112 0 114 113 1
		 93 115 0 113 115 0 114 115 1 94 116 0 115 116 0 114 116 1 95 117 0 116 117 0 114 117 0
		 118 119 1 119 120 1 120 121 1 121 122 1 122 123 1 123 124 1 124 125 1 125 118 1 126 127 1
		 127 128 1 128 129 1 129 130 1 130 131 1 131 132 1 132 133 1 133 126 1 134 135 1 135 136 1
		 136 137 1 137 138 1 138 139 1 139 140 1 140 141 1 141 134 1 142 143 0 143 144 0 144 145 0
		 145 146 0 146 147 0 147 148 0 148 149 0 149 142 0 118 206 0 119 205 0 120 204 0 121 203 0
		 122 202 0 123 201 0 124 200 0 125 199 0 126 134 0 127 135 0 128 136 0 129 137 0 130 138 0
		 131 139 0 132 140 0 133 141 0 134 142 0 135 143 0 136 144 0 137 145 0 138 146 0 139 147 0
		 140 148 0 141 149 0 142 150 1 143 150 1 144 150 1 145 150 1 146 150 1 147 150 1 148 150 1
		 149 150 1 151 187 0 152 188 0 151 152 1 153 189 0 152 153 1 154 190 0 153 154 1 155 183 0
		 154 155 1 156 184 0 155 156 1 157 185 0 156 157 1 158 186 0 157 158 1 158 151 1 159 168 0
		 160 169 0 159 160 1 161 170 0 160 161 1 162 171 0 161 162 1 163 172 0 162 163 1 164 173 0
		 163 164 1 165 174 0;
	setAttr ".ed[332:497]" 164 165 1 166 167 0 165 166 1 166 159 1 167 182 0 168 175 0
		 167 168 1 169 176 0 168 169 1 170 177 0 169 170 1 171 178 0 170 171 1 172 179 0 171 172 1
		 173 180 0 172 173 1 174 181 0 173 174 1 174 167 1 175 193 0 176 194 0 175 176 1 177 195 0
		 176 177 1 178 196 0 177 178 1 179 197 0 178 179 1 180 198 0 179 180 1 181 191 0 180 181 1
		 182 192 0 181 182 1 182 175 1 183 159 0 184 160 0 183 184 1 185 161 0 184 185 1 186 162 0
		 185 186 1 187 163 0 186 187 1 188 164 0 187 188 1 189 165 0 188 189 1 190 166 0 189 190 1
		 190 183 1 191 133 0 192 132 0 191 192 1 193 131 0 192 193 1 194 130 0 193 194 1 195 129 0
		 194 195 1 196 128 0 195 196 1 197 127 0 196 197 1 198 126 0 197 198 1 198 191 1 199 153 0
		 200 154 0 199 200 1 201 155 0 200 201 1 202 156 0 201 202 1 203 157 0 202 203 1 204 158 0
		 203 204 1 205 151 0 204 205 1 206 152 0 205 206 1 206 199 1 118 223 0 119 216 0 207 208 0
		 209 208 0 120 217 0 208 210 0 121 218 0 210 211 1 122 219 0 211 212 0 123 220 0 212 213 0
		 209 213 0 124 221 0 213 214 0 125 222 0 214 215 0 215 207 1 216 208 0 217 210 0 216 217 1
		 218 211 0 217 218 1 219 212 0 218 219 1 220 213 0 219 220 1 221 214 0 220 221 1 222 215 0
		 221 222 1 223 207 0 222 223 1 223 216 1 207 224 0 208 225 0 224 225 0 209 226 1 226 224 1
		 226 225 0 213 227 0 214 228 0 227 228 0 226 227 0 226 228 1 215 229 0 228 229 0 226 229 1
		 229 224 0 208 230 0 210 231 0 230 231 0 209 232 1 232 230 0 232 231 1 211 233 0 231 233 0
		 232 233 1 212 234 0 233 234 0 232 234 1 213 235 0 234 235 0 232 235 0 236 237 1 237 238 1
		 238 239 1 239 240 1 240 241 1 241 242 1 242 243 1 243 236 1 244 245 1 245 246 1 246 247 1
		 247 248 1 248 249 1 249 250 1 250 251 1 251 244 1 252 253 1 253 254 1;
	setAttr ".ed[498:663]" 254 255 1 255 256 1 256 257 1 257 258 1 258 259 1 259 252 1
		 260 261 0 261 262 0 262 263 0 263 264 0 264 265 0 265 266 0 266 267 0 267 260 0 236 286 0
		 237 285 0 238 292 0 239 291 0 240 290 0 241 289 0 242 288 0 243 287 0 244 252 0 245 253 0
		 246 254 0 247 255 0 248 256 0 249 257 0 250 258 0 251 259 0 252 260 0 253 261 0 254 262 0
		 255 263 0 256 264 0 257 265 0 258 266 0 259 267 0 260 268 1 261 268 1 262 268 1 263 268 1
		 264 268 1 265 268 1 266 268 1 267 268 1 269 245 0 270 244 0 269 270 1 271 251 0 270 271 1
		 272 250 0 271 272 1 273 249 0 272 273 1 274 248 0 273 274 1 275 247 0 274 275 1 276 246 0
		 275 276 1 276 269 1 277 352 0 278 353 0 277 278 1 279 346 0 278 279 1 280 347 0 279 280 1
		 281 348 0 280 281 1 282 349 0 281 282 1 283 350 0 282 283 1 284 351 0 283 284 1 284 277 1
		 285 314 0 286 315 0 285 286 1 287 316 0 286 287 1 288 309 0 287 288 1 289 310 0 288 289 1
		 290 311 0 289 290 1 291 312 0 290 291 1 292 313 0 291 292 1 292 285 1 293 301 0 294 302 0
		 293 294 1 295 303 0 294 295 1 296 304 0 295 296 1 297 305 0 296 297 1 298 306 0 297 298 1
		 299 307 0 298 299 1 300 308 0 299 300 1 300 293 1 301 281 0 302 282 0 301 302 1 303 283 0
		 302 303 1 304 284 0 303 304 1 305 277 0 304 305 1 306 278 0 305 306 1 307 279 0 306 307 1
		 308 280 0 307 308 1 308 301 1 309 300 0 310 293 0 309 310 1 311 294 0 310 311 1 312 295 0
		 311 312 1 313 296 0 312 313 1 314 297 0 313 314 1 315 298 0 314 315 1 316 299 0 315 316 1
		 316 309 1 236 333 0 237 326 0 317 318 0 319 318 0 238 327 0 318 320 0 239 328 0 320 321 1
		 240 329 0 321 322 0 241 330 0 322 323 0 319 323 0 242 331 0 323 324 0 243 332 0 324 325 0
		 325 317 1 326 318 0 327 320 0 326 327 1 328 321 0 327 328 1 329 322 0;
	setAttr ".ed[664:829]" 328 329 1 330 323 0 329 330 1 331 324 0 330 331 1 332 325 0
		 331 332 1 333 317 0 332 333 1 333 326 1 317 334 0 318 335 0 334 335 0 319 336 1 336 334 1
		 336 335 0 323 337 0 324 338 0 337 338 0 336 337 0 336 338 1 325 339 0 338 339 0 336 339 1
		 339 334 0 318 340 0 320 341 0 340 341 0 319 342 1 342 340 0 342 341 1 321 343 0 341 343 0
		 342 343 1 322 344 0 343 344 0 342 344 1 323 345 0 344 345 0 342 345 0 346 271 0 347 272 0
		 346 347 1 348 273 0 347 348 1 349 274 0 348 349 1 350 275 0 349 350 1 351 276 0 350 351 1
		 352 269 0 351 352 1 353 270 0 352 353 1 353 346 1 354 355 1 355 356 1 356 357 1 357 358 1
		 358 359 1 359 360 1 360 361 1 361 354 1 362 363 1 363 364 1 364 365 1 365 366 1 366 367 1
		 367 368 1 368 369 1 369 362 1 370 371 1 371 372 1 372 373 1 373 374 1 374 375 1 375 376 1
		 376 377 1 377 370 1 378 379 0 379 380 0 380 381 0 381 382 0 382 383 0 383 384 0 384 385 0
		 385 378 0 354 404 0 355 403 0 356 410 0 357 409 0 358 408 0 359 407 0 360 406 0 361 405 0
		 362 370 0 363 371 0 364 372 0 365 373 0 366 374 0 367 375 0 368 376 0 369 377 0 370 378 0
		 371 379 0 372 380 0 373 381 0 374 382 0 375 383 0 376 384 0 377 385 0 378 386 1 379 386 1
		 380 386 1 381 386 1 382 386 1 383 386 1 384 386 1 385 386 1 387 363 0 388 362 0 387 388 1
		 389 369 0 388 389 1 390 368 0 389 390 1 391 367 0 390 391 1 392 366 0 391 392 1 393 365 0
		 392 393 1 394 364 0 393 394 1 394 387 1 395 470 0 396 471 0 395 396 1 397 464 0 396 397 1
		 398 465 0 397 398 1 399 466 0 398 399 1 400 467 0 399 400 1 401 468 0 400 401 1 402 469 0
		 401 402 1 402 395 1 403 432 0 404 433 0 403 404 1 405 434 0 404 405 1 406 427 0 405 406 1
		 407 428 0 406 407 1 408 429 0 407 408 1 409 430 0 408 409 1 410 431 0;
	setAttr ".ed[830:995]" 409 410 1 410 403 1 411 419 0 412 420 0 411 412 1 413 421 0
		 412 413 1 414 422 0 413 414 1 415 423 0 414 415 1 416 424 0 415 416 1 417 425 0 416 417 1
		 418 426 0 417 418 1 418 411 1 419 399 0 420 400 0 419 420 1 421 401 0 420 421 1 422 402 0
		 421 422 1 423 395 0 422 423 1 424 396 0 423 424 1 425 397 0 424 425 1 426 398 0 425 426 1
		 426 419 1 427 418 0 428 411 0 427 428 1 429 412 0 428 429 1 430 413 0 429 430 1 431 414 0
		 430 431 1 432 415 0 431 432 1 433 416 0 432 433 1 434 417 0 433 434 1 434 427 1 354 451 0
		 355 444 0 435 436 0 437 436 0 356 445 0 436 438 0 357 446 0 438 439 1 358 447 0 439 440 0
		 359 448 0 440 441 0 437 441 0 360 449 0 441 442 0 361 450 0 442 443 0 443 435 1 444 436 0
		 445 438 0 444 445 1 446 439 0 445 446 1 447 440 0 446 447 1 448 441 0 447 448 1 449 442 0
		 448 449 1 450 443 0 449 450 1 451 435 0 450 451 1 451 444 1 435 452 0 436 453 0 452 453 0
		 437 454 1 454 452 1 454 453 0 441 455 0 442 456 0 455 456 0 454 455 0 454 456 1 443 457 0
		 456 457 0 454 457 1 457 452 0 436 458 0 438 459 0 458 459 0 437 460 1 460 458 0 460 459 1
		 439 461 0 459 461 0 460 461 1 440 462 0 461 462 0 460 462 1 441 463 0 462 463 0 460 463 0
		 464 389 0 465 390 0 464 465 1 466 391 0 465 466 1 467 392 0 466 467 1 468 393 0 467 468 1
		 469 394 0 468 469 1 470 387 0 469 470 1 471 388 0 470 471 1 471 464 1 472 473 0 474 475 0
		 476 477 0 478 479 0 472 482 0 473 483 0 474 476 0 475 477 0 476 497 0 477 496 0 478 472 0
		 479 473 0 480 479 0 481 478 0 480 481 1 482 486 0 481 482 1 483 487 0 482 483 1 483 480 1
		 484 480 0 485 481 0 484 485 1 486 490 0 485 486 1 487 491 0 486 487 1 487 484 1 488 484 0
		 489 485 0 488 489 1 490 494 0 489 490 1 491 495 0 490 491 1 491 488 1;
	setAttr ".ed[996:1161]" 492 488 0 493 489 0 492 493 1 494 498 0 493 494 1 495 499 0
		 494 495 1 495 492 1 496 492 0 497 493 0 496 497 1 498 474 0 497 498 1 499 475 0 498 499 1
		 499 496 1 500 501 0 502 503 0 504 505 0 506 507 0 500 510 0 501 511 0 502 504 0 503 505 0
		 504 525 0 505 524 0 506 500 0 507 501 0 508 507 0 509 506 0 508 509 1 510 514 0 509 510 1
		 511 515 0 510 511 1 511 508 1 512 508 0 513 509 0 512 513 1 514 518 0 513 514 1 515 519 0
		 514 515 1 515 512 1 516 512 0 517 513 0 516 517 1 518 522 0 517 518 1 519 523 0 518 519 1
		 519 516 1 520 516 0 521 517 0 520 521 1 522 526 0 521 522 1 523 527 0 522 523 1 523 520 1
		 524 520 0 525 521 0 524 525 1 526 502 0 525 526 1 527 503 0 526 527 1 527 524 1 528 529 0
		 530 531 0 532 533 0 534 535 0 528 538 0 529 539 0 530 532 0 531 533 0 532 553 0 533 552 0
		 534 528 0 535 529 0 536 535 0 537 534 0 536 537 1 538 542 0 537 538 1 539 543 0 538 539 1
		 539 536 1 540 536 0 541 537 0 540 541 1 542 546 0 541 542 1 543 547 0 542 543 1 543 540 1
		 544 540 0 545 541 0 544 545 1 546 550 0 545 546 1 547 551 0 546 547 1 547 544 1 548 544 0
		 549 545 0 548 549 1 550 554 0 549 550 1 551 555 0 550 551 1 551 548 1 552 548 0 553 549 0
		 552 553 1 554 530 0 553 554 1 555 531 0 554 555 1 555 552 1 556 572 0 557 573 0 556 607 1
		 558 574 0 557 558 1 559 575 0 558 628 0 559 556 1 560 614 1 561 562 1 562 637 1 563 560 1
		 564 576 0 565 577 0 564 612 1 566 578 1 565 566 1 567 579 1 566 641 0 567 564 1 568 580 0
		 569 581 0 568 609 1 570 582 0 569 570 1 571 583 0 570 624 0 571 568 1 572 568 0 573 569 0
		 572 608 1 574 570 1 573 574 1 575 571 1 574 627 0 575 572 1 576 560 0 577 561 0 576 613 1
		 578 562 0 577 578 1 579 563 0 578 638 0 579 576 1 580 584 0 581 585 0;
	setAttr ".ed[1162:1327]" 580 610 1 582 586 1 581 582 1 583 587 1 582 623 0 583 580 1
		 584 564 0 585 565 0 584 611 1 586 566 0 585 586 1 587 567 0 586 642 0 587 584 1 566 689 0
		 567 684 0 588 640 0 578 688 0 588 590 0 579 685 0 590 639 0 589 591 0 574 680 0 575 683 0
		 592 626 0 570 679 0 592 594 0 571 676 0 594 625 0 593 595 0 582 673 0 583 668 0 596 622 0
		 586 672 0 596 598 0 587 669 0 598 643 0 597 599 0 600 597 0 601 583 0 600 657 1 602 571 0
		 601 602 1 603 595 0 602 677 1 604 593 0 603 604 1 605 575 0 604 648 1 606 559 0 605 606 1
		 607 629 1 608 630 1 607 608 1 609 631 1 608 609 1 610 632 0 609 610 1 611 633 0 610 611 0
		 612 634 1 611 612 1 613 635 1 612 613 1 614 636 1 613 614 1 615 563 1 616 579 0 615 616 1
		 617 591 0 616 686 1 618 589 0 617 618 1 619 567 0 618 666 1 620 587 0 619 620 1 621 599 0
		 620 670 1 621 600 1 622 600 0 623 601 0 622 656 1 624 602 0 623 624 1 625 603 0 624 678 1
		 626 604 0 625 626 1 627 605 0 626 647 1 628 606 0 627 628 1 629 557 1 630 573 1 629 630 1
		 631 569 1 630 631 1 632 581 1 631 632 1 633 585 1 632 633 0 634 565 1 633 634 1 635 577 1
		 634 635 1 636 561 1 635 636 1 637 615 0 638 616 0 637 638 1 639 617 0 638 687 1 640 618 0
		 639 640 1 641 619 0 640 665 1 642 620 0 641 642 1 643 621 0 642 671 1 643 622 1 644 625 1
		 645 594 0 644 645 1 646 592 0 645 646 1 647 1328 0 646 647 1 648 1357 0 647 648 0
		 649 593 0 648 649 1 650 595 0 649 650 1 651 603 1 650 651 1 651 644 0 652 621 1 653 643 1
		 652 653 0 654 598 0 653 654 1 655 596 0 654 655 1 656 1317 0 655 656 1 657 1368 0
		 656 657 0 658 597 0 657 658 1 659 599 0 658 659 1 659 652 1 660 591 0 661 617 1 660 661 1
		 662 639 1 661 662 0 663 590 0 662 663 1 664 588 0 663 664 1 665 1308 0 664 665 1
		 666 1377 0;
	setAttr ".ed[1328:1493]" 665 666 0 667 589 0 666 667 1 667 660 1 668 897 0 669 896 0
		 668 669 1 670 895 0 669 670 1 671 858 0 670 671 0 672 857 0 671 672 1 673 856 0 672 673 1
		 674 623 1 673 674 1 675 601 1 674 675 0 675 668 1 676 907 0 677 906 0 676 677 1 678 847 0
		 677 678 0 679 846 0 678 679 1 680 845 0 679 680 1 681 627 1 680 681 1 682 605 1 681 682 0
		 683 908 0 682 683 1 683 676 1 684 888 0 685 887 0 684 685 1 686 886 0 685 686 1 687 867 0
		 686 687 0 688 866 0 687 688 1 689 865 0 688 689 1 690 641 1 689 690 1 691 619 1 690 691 0
		 691 684 1 644 727 0 678 726 0 677 725 0 651 724 0 653 747 0 671 746 0 670 745 0 652 744 0
		 560 878 0 614 877 1 692 693 0 561 875 0 562 874 0 694 695 0 637 873 0 695 696 0 563 879 0
		 697 692 0 636 876 1 693 698 0 615 880 0 699 697 0 698 694 0 696 699 1 693 699 0 696 698 0
		 615 767 0 637 766 0 696 765 0 699 764 0 700 716 0 701 717 0 700 1364 1 702 718 0
		 701 702 1 703 719 0 702 851 1 703 700 1 704 712 0 705 713 0 704 1362 1 706 714 0
		 705 706 1 707 715 0 706 849 1 707 704 1 708 720 0 709 721 0 708 1366 1 710 722 0
		 709 710 1 711 723 0 710 853 1 711 708 1 712 700 0 713 701 0 712 1363 1 714 702 0
		 713 714 1 715 703 0 714 850 1 715 712 1 716 708 0 717 709 0 716 1365 1 718 710 0
		 717 718 1 719 711 0 718 852 1 719 716 1 720 657 0 721 675 0 720 1367 1 722 674 0
		 721 722 1 723 656 0 722 854 1 723 720 1 724 704 0 725 705 0 724 1361 1 726 706 0
		 725 726 1 727 707 0 726 848 1 727 724 1 728 736 0 729 737 0 728 1374 1 730 738 0
		 729 730 1 731 739 0 730 861 1 731 728 1 732 666 0 733 691 0 732 1376 1 734 690 0
		 733 734 1 735 665 0 734 863 1 735 732 1 736 732 0 737 733 0 736 1375 1 738 734 0
		 737 738 1 739 735 0 738 862 1 739 736 1 740 728 0 741 729 0 740 1373 1 742 730 0;
	setAttr ".ed[1494:1659]" 741 742 1 743 731 0 742 860 1 743 740 1 744 740 0 745 741 0
		 744 1372 1 746 742 0 745 746 1 747 743 0 746 859 1 747 744 1 748 661 0 749 662 0
		 748 749 1 750 687 0 749 1304 1 751 686 0 750 751 1 751 885 1 752 748 0 753 749 0
		 752 753 1 754 750 0 753 1303 1 755 751 0 754 755 1 755 884 1 756 752 0 757 753 0
		 756 757 1 758 754 0 757 1302 1 759 755 0 758 759 1 759 883 1 760 756 0 761 757 0
		 760 761 1 762 758 0 761 1301 1 763 759 0 762 763 1 763 882 1 764 760 0 765 761 0
		 764 765 1 766 762 0 765 1300 1 767 763 0 766 767 1 767 881 1 556 828 0 607 829 1
		 768 769 0 557 831 0 558 832 0 770 771 0 628 833 0 771 772 0 559 827 0 773 768 0 606 826 0
		 774 773 0 629 830 1 769 775 0 772 774 0 775 770 0 776 768 0 777 773 0 776 777 1 778 774 0
		 777 778 1 779 772 0 778 779 0 780 771 0 779 780 1 781 770 0 780 781 1 782 775 1 781 782 1
		 783 769 1 782 783 1 783 776 1 606 821 0 778 820 0 779 823 0 628 822 0 784 648 0 785 682 0
		 784 1356 1 786 681 0 785 786 1 787 647 0 786 843 1 787 784 1 788 784 0 789 785 0
		 788 1355 1 790 786 0 789 790 1 791 787 0 790 842 1 791 788 1 792 788 0 793 789 0
		 792 1354 1 794 790 0 793 794 1 795 791 0 794 841 1 795 792 1 796 792 0 797 793 0
		 796 1353 1 798 794 0 797 798 1 799 795 0 798 840 1 799 796 1 800 796 0 801 797 0
		 800 1352 1 802 798 0 801 802 1 803 799 0 802 839 1 803 800 1 804 800 0 805 801 0
		 804 1351 1 806 802 0 805 806 1 807 803 0 806 838 1 807 804 1 808 804 0 809 805 0
		 808 1350 1 810 806 0 809 810 1 811 807 0 810 837 1 811 808 1 812 808 0 813 809 0
		 812 1349 1 814 810 0 813 814 1 815 811 0 814 836 1 815 812 1 816 812 0 817 813 0
		 816 1348 1 818 814 0 817 818 1 819 815 0 818 835 1 819 816 1 820 816 0 821 817 0
		 820 1347 1 822 818 0 821 822 1 823 819 0;
	setAttr ".ed[1660:1825]" 822 834 1 823 820 1 824 817 1 825 821 1 824 825 1 826 920 0
		 825 826 1 827 921 0 826 827 1 828 922 0 827 828 1 829 923 1 828 829 1 830 924 1 829 830 1
		 831 925 0 830 831 1 832 926 0 831 832 1 833 927 0 832 833 1 834 928 1 833 834 1 835 929 1
		 834 835 1 836 930 1 835 836 1 837 931 1 836 837 1 838 932 1 837 838 1 839 933 1 838 839 1
		 840 934 1 839 840 1 841 935 1 840 841 1 842 936 1 841 842 1 843 937 1 842 843 1 844 681 0
		 843 844 1 845 939 0 844 845 1 846 940 0 845 846 1 847 941 0 846 847 1 848 942 1 847 848 1
		 849 943 1 848 849 1 850 944 1 849 850 1 851 945 1 850 851 1 852 946 1 851 852 1 853 947 1
		 852 853 1 854 948 1 853 854 1 855 674 0 854 855 1 856 950 0 855 856 1 857 951 0 856 857 1
		 858 952 0 857 858 1 859 953 1 858 859 1 860 954 1 859 860 1 861 955 1 860 861 1 862 956 1
		 861 862 1 863 957 1 862 863 1 864 690 0 863 864 1 865 959 0 864 865 1 866 960 0 865 866 1
		 867 961 0 866 867 1 868 750 1 867 868 1 869 754 1 868 869 1 870 758 1 869 870 1 871 762 1
		 870 871 1 872 766 1 871 872 1 873 967 0 872 873 1 874 968 0 873 874 1 875 969 0 874 875 1
		 876 970 1 875 876 1 877 971 1 876 877 1 878 972 0 877 878 1 879 973 0 878 879 1 880 974 0
		 879 880 1 881 975 1 880 881 1 882 976 1 881 882 1 883 977 1 882 883 1 884 978 1 883 884 1
		 885 979 1 884 885 1 886 980 0 885 886 1 887 981 0 886 887 1 888 982 0 887 888 1 889 691 0
		 888 889 1 890 733 1 889 890 1 891 737 1 890 891 1 892 729 1 891 892 1 893 741 1 892 893 1
		 894 745 1 893 894 1 895 989 0 894 895 1 896 990 0 895 896 1 897 991 0 896 897 1 898 675 0
		 897 898 1 899 721 1 898 899 1 900 709 1 899 900 1 901 717 1 900 901 1 902 701 1 901 902 1
		 903 713 1 902 903 1 904 705 1 903 904 1 905 725 1 904 905 1 906 1000 0;
	setAttr ".ed[1826:1991]" 905 906 1 907 1001 0 906 907 1 908 1002 0 907 908 1
		 909 682 0 908 909 1 910 785 1 909 910 1 911 789 1 910 911 1 912 793 1 911 912 1 913 797 1
		 912 913 1 914 801 1 913 914 1 915 805 1 914 915 1 916 809 1 915 916 1 917 813 1 916 917 1
		 917 824 1 918 824 1 919 825 1 918 919 1 920 1014 0 919 920 1 921 1015 0 920 921 1
		 922 1016 0 921 922 1 923 1017 1 922 923 1 924 1018 1 923 924 1 925 1019 0 924 925 1
		 926 1020 0 925 926 1 927 1021 0 926 927 1 928 1022 1 927 928 1 929 1023 1 928 929 1
		 930 1024 1 929 930 1 931 1025 1 930 931 1 932 1026 1 931 932 1 933 1027 1 932 933 1
		 934 1028 1 933 934 1 935 1029 1 934 935 1 936 1030 1 935 936 1 937 1031 1 936 937 1
		 938 844 0 937 938 1 939 1033 0 938 939 1 940 1034 0 939 940 1 941 1035 0 940 941 1
		 942 1036 1 941 942 1 943 1037 1 942 943 1 944 1038 1 943 944 1 945 1039 1 944 945 1
		 946 1040 1 945 946 1 947 1041 1 946 947 1 948 1042 1 947 948 1 949 855 0 948 949 1
		 950 1044 0 949 950 1 951 1045 0 950 951 1 952 1046 0 951 952 1 953 1047 1 952 953 1
		 954 1048 1 953 954 1 955 1049 1 954 955 1 956 1050 1 955 956 1 957 1051 1 956 957 1
		 958 864 0 957 958 1 959 1053 0 958 959 1 960 1054 0 959 960 1 961 1055 0 960 961 1
		 962 868 1 961 962 1 963 869 1 962 963 1 964 870 1 963 964 1 965 871 1 964 965 1 966 872 1
		 965 966 1 967 1061 0 966 967 1 968 1062 0 967 968 1 969 1063 0 968 969 1 970 1064 1
		 969 970 1 971 1065 1 970 971 1 972 1066 0 971 972 1 973 1067 0 972 973 1 974 1068 0
		 973 974 1 975 1069 1 974 975 1 976 1070 1 975 976 1 977 1071 1 976 977 1 978 1072 1
		 977 978 1 979 1073 1 978 979 1 980 1074 0 979 980 1 981 1075 0 980 981 1 982 1076 0
		 981 982 1 983 889 0 982 983 1 984 890 1 983 984 1 985 891 1 984 985 1 986 892 1 985 986 1
		 987 893 1 986 987 1 988 894 1 987 988 1 989 1083 0;
	setAttr ".ed[1992:2157]" 988 989 1 990 1084 0 989 990 1 991 1085 0 990 991 1
		 992 898 0 991 992 1 993 899 1 992 993 1 994 900 1 993 994 1 995 901 1 994 995 1 996 902 1
		 995 996 1 997 903 1 996 997 1 998 904 1 997 998 1 999 905 1 998 999 1 1000 1094 0
		 999 1000 1 1001 1095 0 1000 1001 1 1002 1096 0 1001 1002 1 1003 909 0 1002 1003 1
		 1004 910 1 1003 1004 1 1005 911 1 1004 1005 1 1006 912 1 1005 1006 1 1007 913 1 1006 1007 1
		 1008 914 1 1007 1008 1 1009 915 1 1008 1009 1 1010 916 1 1009 1010 1 1011 917 1 1010 1011 1
		 1011 918 1 1012 918 1 1013 919 1 1012 1013 1 1014 1108 0 1013 1014 1 1015 1109 0
		 1014 1015 1 1016 1110 0 1015 1016 1 1017 1111 1 1016 1017 1 1018 1112 1 1017 1018 1
		 1019 1113 0 1018 1019 1 1020 1114 0 1019 1020 1 1021 1115 0 1020 1021 1 1022 1116 1
		 1021 1022 1 1023 1117 1 1022 1023 1 1024 1118 1 1023 1024 1 1025 1119 1 1024 1025 1
		 1026 1120 1 1025 1026 1 1027 1121 1 1026 1027 1 1028 1122 1 1027 1028 1 1029 1123 1
		 1028 1029 1 1030 1124 1 1029 1030 1 1031 1125 1 1030 1031 1 1032 938 0 1031 1032 1
		 1033 1127 0 1032 1033 1 1034 1128 0 1033 1034 1 1035 1129 0 1034 1035 1 1036 1130 1
		 1035 1036 1 1037 1131 1 1036 1037 1 1038 1132 1 1037 1038 1 1039 1133 1 1038 1039 1
		 1040 1134 1 1039 1040 1 1041 1135 1 1040 1041 1 1042 1136 1 1041 1042 1 1043 949 0
		 1042 1043 1 1044 1138 0 1043 1044 1 1045 1139 0 1044 1045 1 1046 1140 0 1045 1046 1
		 1047 1141 1 1046 1047 1 1048 1142 1 1047 1048 1 1049 1143 1 1048 1049 1 1050 1144 1
		 1049 1050 1 1051 1145 1 1050 1051 1 1052 958 0 1051 1052 1 1053 1147 0 1052 1053 1
		 1054 1148 0 1053 1054 1 1055 1149 0 1054 1055 1 1056 962 1 1055 1056 1 1057 963 1
		 1056 1057 1 1058 964 1 1057 1058 1 1059 965 1 1058 1059 1 1060 966 1 1059 1060 1
		 1061 1155 0 1060 1061 1 1062 1156 0 1061 1062 1 1063 1157 0 1062 1063 1 1064 1158 1
		 1063 1064 1 1065 1159 1 1064 1065 1 1066 1160 0 1065 1066 1 1067 1161 0 1066 1067 1
		 1068 1162 0 1067 1068 1 1069 1163 1 1068 1069 1 1070 1164 1 1069 1070 1 1071 1165 1
		 1070 1071 1 1072 1166 1;
	setAttr ".ed[2158:2323]" 1071 1072 1 1073 1167 1 1072 1073 1 1074 1168 0 1073 1074 1
		 1075 1169 0 1074 1075 1 1076 1170 0 1075 1076 1 1077 983 0 1076 1077 1 1078 984 1
		 1077 1078 1 1079 985 1 1078 1079 1 1080 986 1 1079 1080 1 1081 987 1 1080 1081 1
		 1082 988 1 1081 1082 1 1083 1177 0 1082 1083 1 1084 1178 0 1083 1084 1 1085 1179 0
		 1084 1085 1 1086 992 0 1085 1086 1 1087 993 1 1086 1087 1 1088 994 1 1087 1088 1
		 1089 995 1 1088 1089 1 1090 996 1 1089 1090 1 1091 997 1 1090 1091 1 1092 998 1 1091 1092 1
		 1093 999 1 1092 1093 1 1094 1188 0 1093 1094 1 1095 1189 0 1094 1095 1 1096 1190 0
		 1095 1096 1 1097 1003 0 1096 1097 1 1098 1004 1 1097 1098 1 1099 1005 1 1098 1099 1
		 1100 1006 1 1099 1100 1 1101 1007 1 1100 1101 1 1102 1008 1 1101 1102 1 1103 1009 1
		 1102 1103 1 1104 1010 1 1103 1104 1 1105 1011 1 1104 1105 1 1105 1012 1 1106 1012 1
		 1107 1013 1 1106 1107 1 1108 1202 0 1107 1108 1 1109 1203 0 1108 1109 1 1110 1204 0
		 1109 1110 1 1111 1205 1 1110 1111 1 1112 1206 1 1111 1112 1 1113 1207 0 1112 1113 1
		 1114 1208 0 1113 1114 1 1115 1209 0 1114 1115 1 1116 1210 1 1115 1116 1 1117 1211 1
		 1116 1117 1 1118 1212 1 1117 1118 1 1119 1213 1 1118 1119 1 1120 1214 1 1119 1120 1
		 1121 1215 1 1120 1121 1 1122 1216 1 1121 1122 1 1123 1217 1 1122 1123 1 1124 1218 1
		 1123 1124 1 1125 1219 1 1124 1125 1 1126 1032 0 1125 1126 1 1127 1221 0 1126 1127 1
		 1128 1222 0 1127 1128 1 1129 1223 0 1128 1129 1 1130 1224 1 1129 1130 1 1131 1225 1
		 1130 1131 1 1132 1226 1 1131 1132 1 1133 1227 1 1132 1133 1 1134 1228 1 1133 1134 1
		 1135 1229 1 1134 1135 1 1136 1230 1 1135 1136 1 1137 1043 0 1136 1137 1 1138 1232 0
		 1137 1138 1 1139 1233 0 1138 1139 1 1140 1234 0 1139 1140 1 1141 1235 1 1140 1141 1
		 1142 1236 1 1141 1142 1 1143 1237 1 1142 1143 1 1144 1238 1 1143 1144 1 1145 1239 1
		 1144 1145 1 1146 1052 0 1145 1146 1 1147 1241 0 1146 1147 1 1148 1242 0 1147 1148 1
		 1149 1243 0 1148 1149 1 1150 1056 1 1149 1150 1 1151 1057 1 1150 1151 1 1152 1058 1
		 1151 1152 1 1153 1059 1 1152 1153 1 1154 1060 1 1153 1154 1 1155 1249 0;
	setAttr ".ed[2324:2489]" 1154 1155 1 1156 1250 0 1155 1156 1 1157 1251 0 1156 1157 1
		 1158 1252 1 1157 1158 1 1159 1253 1 1158 1159 1 1160 1254 0 1159 1160 1 1161 1255 0
		 1160 1161 1 1162 1256 0 1161 1162 1 1163 1257 1 1162 1163 1 1164 1258 1 1163 1164 1
		 1165 1259 1 1164 1165 1 1166 1260 1 1165 1166 1 1167 1261 1 1166 1167 1 1168 1262 0
		 1167 1168 1 1169 1263 0 1168 1169 1 1170 1264 0 1169 1170 1 1171 1077 0 1170 1171 1
		 1172 1078 1 1171 1172 1 1173 1079 1 1172 1173 1 1174 1080 1 1173 1174 1 1175 1081 1
		 1174 1175 1 1176 1082 1 1175 1176 1 1177 1271 0 1176 1177 1 1178 1272 0 1177 1178 1
		 1179 1273 0 1178 1179 1 1180 1086 0 1179 1180 1 1181 1087 1 1180 1181 1 1182 1088 1
		 1181 1182 1 1183 1089 1 1182 1183 1 1184 1090 1 1183 1184 1 1185 1091 1 1184 1185 1
		 1186 1092 1 1185 1186 1 1187 1093 1 1186 1187 1 1188 1282 0 1187 1188 1 1189 1283 0
		 1188 1189 1 1190 1284 0 1189 1190 1 1191 1097 0 1190 1191 1 1192 1098 1 1191 1192 1
		 1193 1099 1 1192 1193 1 1194 1100 1 1193 1194 1 1195 1101 1 1194 1195 1 1196 1102 1
		 1195 1196 1 1197 1103 1 1196 1197 1 1198 1104 1 1197 1198 1 1199 1105 1 1198 1199 1
		 1199 1106 1 1200 1106 1 1201 1107 1 1200 1201 1 1202 1390 0 1201 1202 1 1203 1389 0
		 1202 1203 1 1204 1388 0 1203 1204 1 1205 1481 1 1204 1205 1 1206 1480 1 1205 1206 1
		 1207 1479 0 1206 1207 1 1208 1478 0 1207 1208 1 1209 1477 0 1208 1209 1 1210 1476 1
		 1209 1210 1 1211 1475 1 1210 1211 1 1212 1474 1 1211 1212 1 1213 1473 1 1212 1213 1
		 1214 1472 1 1213 1214 1 1215 1471 1 1214 1215 1 1216 1470 1 1215 1216 1 1217 1469 1
		 1216 1217 1 1218 1468 1 1217 1218 1 1219 1467 1 1218 1219 1 1220 1126 0 1219 1220 1
		 1221 1465 0 1220 1221 1 1222 1464 0 1221 1222 1 1223 1463 0 1222 1223 1 1224 1462 1
		 1223 1224 1 1225 1461 1 1224 1225 1 1226 1460 1 1225 1226 1 1227 1459 1 1226 1227 1
		 1228 1458 1 1227 1228 1 1229 1457 1 1228 1229 1 1230 1456 1 1229 1230 1 1231 1137 0
		 1230 1231 1 1232 1454 0 1231 1232 1 1233 1453 0 1232 1233 1 1234 1452 0 1233 1234 1
		 1235 1451 1 1234 1235 1 1236 1450 1 1235 1236 1 1237 1449 1 1236 1237 1 1238 1448 1;
	setAttr ".ed[2490:2655]" 1237 1238 1 1239 1447 1 1238 1239 1 1240 1146 0 1239 1240 1
		 1241 1445 0 1240 1241 1 1242 1444 0 1241 1242 1 1243 1443 0 1242 1243 1 1244 1150 1
		 1243 1244 1 1245 1151 1 1244 1245 1 1246 1152 1 1245 1246 1 1247 1153 1 1246 1247 1
		 1248 1154 1 1247 1248 1 1249 1437 0 1248 1249 1 1250 1436 0 1249 1250 1 1251 1435 0
		 1250 1251 1 1252 1434 1 1251 1252 1 1253 1433 1 1252 1253 1 1254 1432 0 1253 1254 1
		 1255 1431 0 1254 1255 1 1256 1430 0 1255 1256 1 1257 1429 1 1256 1257 1 1258 1428 1
		 1257 1258 1 1259 1427 1 1258 1259 1 1260 1426 1 1259 1260 1 1261 1425 1 1260 1261 1
		 1262 1424 0 1261 1262 1 1263 1423 0 1262 1263 1 1264 1422 0 1263 1264 1 1265 1171 0
		 1264 1265 1 1266 1172 1 1265 1266 1 1267 1173 1 1266 1267 1 1268 1174 1 1267 1268 1
		 1269 1175 1 1268 1269 1 1270 1176 1 1269 1270 1 1271 1415 0 1270 1271 1 1272 1414 0
		 1271 1272 1 1273 1413 0 1272 1273 1 1274 1180 0 1273 1274 1 1275 1181 1 1274 1275 1
		 1276 1182 1 1275 1276 1 1277 1183 1 1276 1277 1 1278 1184 1 1277 1278 1 1279 1185 1
		 1278 1279 1 1280 1186 1 1279 1280 1 1281 1187 1 1280 1281 1 1282 1404 0 1281 1282 1
		 1283 1403 0 1282 1283 1 1284 1402 0 1283 1284 1 1285 1191 0 1284 1285 1 1286 1192 1
		 1285 1286 1 1287 1193 1 1286 1287 1 1288 1194 1 1287 1288 1 1289 1195 1 1288 1289 1
		 1290 1196 1 1289 1290 1 1291 1197 1 1290 1291 1 1292 1198 1 1291 1292 1 1293 1199 1
		 1292 1293 1 1293 1200 1 1294 692 0 1295 693 1 1294 1295 1 1296 698 1 1295 1296 1
		 1297 694 0 1296 1297 1 1298 695 0 1297 1298 1 1299 696 0 1298 1299 1 1300 1438 1
		 1299 1300 1 1301 1439 1 1300 1301 1 1302 1440 1 1301 1302 1 1303 1441 1 1302 1303 1
		 1304 1442 1 1303 1304 1 1305 662 0 1304 1305 1 1306 663 0 1305 1306 1 1307 664 0
		 1306 1307 1 1308 1446 0 1307 1308 1 1309 735 1 1308 1309 1 1310 739 1 1309 1310 1
		 1311 731 1 1310 1311 1 1312 743 1 1311 1312 1 1313 747 1 1312 1313 1 1314 653 0 1313 1314 1
		 1315 654 0 1314 1315 1 1316 655 0 1315 1316 1 1317 1455 0 1316 1317 1 1318 723 1
		 1317 1318 1 1319 711 1 1318 1319 1 1320 719 1 1319 1320 1 1321 703 1;
	setAttr ".ed[2656:2821]" 1320 1321 1 1322 715 1 1321 1322 1 1323 707 1 1322 1323 1
		 1324 727 1 1323 1324 1 1325 644 0 1324 1325 1 1326 645 0 1325 1326 1 1327 646 0 1326 1327 1
		 1328 1466 0 1327 1328 1 1329 787 1 1328 1329 1 1330 791 1 1329 1330 1 1331 795 1
		 1330 1331 1 1332 799 1 1331 1332 1 1333 803 1 1332 1333 1 1334 807 1 1333 1334 1
		 1335 811 1 1334 1335 1 1336 815 1 1335 1336 1 1337 819 1 1336 1337 1 1338 823 1 1337 1338 1
		 1339 779 0 1338 1339 1 1340 780 0 1339 1340 1 1341 781 0 1340 1341 1 1342 782 1 1341 1342 1
		 1343 783 1 1342 1343 1 1344 776 0 1343 1344 1 1345 777 0 1344 1345 1 1346 778 0 1345 1346 1
		 1347 1391 1 1346 1347 1 1348 1392 1 1347 1348 1 1349 1393 1 1348 1349 1 1350 1394 1
		 1349 1350 1 1351 1395 1 1350 1351 1 1352 1396 1 1351 1352 1 1353 1397 1 1352 1353 1
		 1354 1398 1 1353 1354 1 1355 1399 1 1354 1355 1 1356 1400 1 1355 1356 1 1357 1401 0
		 1356 1357 1 1358 649 0 1357 1358 1 1359 650 0 1358 1359 1 1360 651 0 1359 1360 1
		 1361 1405 1 1360 1361 1 1362 1406 1 1361 1362 1 1363 1407 1 1362 1363 1 1364 1408 1
		 1363 1364 1 1365 1409 1 1364 1365 1 1366 1410 1 1365 1366 1 1367 1411 1 1366 1367 1
		 1368 1412 0 1367 1368 1 1369 658 0 1368 1369 1 1370 659 0 1369 1370 1 1371 652 0
		 1370 1371 1 1372 1416 1 1371 1372 1 1373 1417 1 1372 1373 1 1374 1418 1 1373 1374 1
		 1375 1419 1 1374 1375 1 1376 1420 1 1375 1376 1 1377 1421 0 1376 1377 1 1378 667 0
		 1377 1378 1 1379 660 0 1378 1379 1 1380 661 0 1379 1380 1 1381 748 1 1380 1381 1
		 1382 752 1 1381 1382 1 1383 756 1 1382 1383 1 1384 760 1 1383 1384 1 1385 764 1 1384 1385 1
		 1386 699 0 1385 1386 1 1387 697 0 1386 1387 1 1387 1294 1 1388 1344 0 1389 1345 0
		 1388 1389 1 1390 1346 0 1389 1390 1 1391 1201 1 1390 1391 1 1392 1200 1 1391 1392 1
		 1393 1293 1 1392 1393 1 1394 1292 1 1393 1394 1 1395 1291 1 1394 1395 1 1396 1290 1
		 1395 1396 1 1397 1289 1 1396 1397 1 1398 1288 1 1397 1398 1 1399 1287 1 1398 1399 1
		 1400 1286 1 1399 1400 1 1401 1285 0 1400 1401 1 1402 1358 0 1401 1402 1 1403 1359 0
		 1402 1403 1 1404 1360 0;
	setAttr ".ed[2822:2987]" 1403 1404 1 1405 1281 1 1404 1405 1 1406 1280 1 1405 1406 1
		 1407 1279 1 1406 1407 1 1408 1278 1 1407 1408 1 1409 1277 1 1408 1409 1 1410 1276 1
		 1409 1410 1 1411 1275 1 1410 1411 1 1412 1274 0 1411 1412 1 1413 1369 0 1412 1413 1
		 1414 1370 0 1413 1414 1 1415 1371 0 1414 1415 1 1416 1270 1 1415 1416 1 1417 1269 1
		 1416 1417 1 1418 1268 1 1417 1418 1 1419 1267 1 1418 1419 1 1420 1266 1 1419 1420 1
		 1421 1265 0 1420 1421 1 1422 1378 0 1421 1422 1 1423 1379 0 1422 1423 1 1424 1380 0
		 1423 1424 1 1425 1381 1 1424 1425 1 1426 1382 1 1425 1426 1 1427 1383 1 1426 1427 1
		 1428 1384 1 1427 1428 1 1429 1385 1 1428 1429 1 1430 1386 0 1429 1430 1 1431 1387 0
		 1430 1431 1 1432 1294 0 1431 1432 1 1433 1295 1 1432 1433 1 1434 1296 1 1433 1434 1
		 1435 1297 0 1434 1435 1 1436 1298 0 1435 1436 1 1437 1299 0 1436 1437 1 1438 1248 1
		 1437 1438 1 1439 1247 1 1438 1439 1 1440 1246 1 1439 1440 1 1441 1245 1 1440 1441 1
		 1442 1244 1 1441 1442 1 1443 1305 0 1442 1443 1 1444 1306 0 1443 1444 1 1445 1307 0
		 1444 1445 1 1446 1240 0 1445 1446 1 1447 1309 1 1446 1447 1 1448 1310 1 1447 1448 1
		 1449 1311 1 1448 1449 1 1450 1312 1 1449 1450 1 1451 1313 1 1450 1451 1 1452 1314 0
		 1451 1452 1 1453 1315 0 1452 1453 1 1454 1316 0 1453 1454 1 1455 1231 0 1454 1455 1
		 1456 1318 1 1455 1456 1 1457 1319 1 1456 1457 1 1458 1320 1 1457 1458 1 1459 1321 1
		 1458 1459 1 1460 1322 1 1459 1460 1 1461 1323 1 1460 1461 1 1462 1324 1 1461 1462 1
		 1463 1325 0 1462 1463 1 1464 1326 0 1463 1464 1 1465 1327 0 1464 1465 1 1466 1220 0
		 1465 1466 1 1467 1329 1 1466 1467 1 1468 1330 1 1467 1468 1 1469 1331 1 1468 1469 1
		 1470 1332 1 1469 1470 1 1471 1333 1 1470 1471 1 1472 1334 1 1471 1472 1 1473 1335 1
		 1472 1473 1 1474 1336 1 1473 1474 1 1475 1337 1 1474 1475 1 1476 1338 1 1475 1476 1
		 1477 1339 0 1476 1477 1 1478 1340 0 1477 1478 1 1479 1341 0 1478 1479 1 1480 1342 1
		 1479 1480 1 1481 1343 1 1480 1481 1 1481 1388 1 632 1482 0 633 1483 0 1482 1483 1
		 610 1484 0 1484 1482 1 611 1485 0 1484 1485 1 1485 1483 1 1482 1486 0 1483 1487 0;
	setAttr ".ed[2988:3153]" 1486 1487 0 1484 1488 0 1488 1486 0 1485 1489 0 1488 1489 0
		 1489 1487 0 1486 1490 0 1487 1491 0 1490 1491 1 1488 1492 0 1492 1490 0 1489 1493 0
		 1492 1493 1 1493 1491 0 1490 1494 1 1491 1495 1 1494 1495 0 1492 1496 1 1496 1494 0
		 1493 1497 1 1496 1497 0 1497 1495 0 1494 1498 0 1495 1499 0 1498 1499 1 1496 1500 0
		 1500 1498 1 1497 1501 0 1500 1501 1 1501 1499 1 1498 1502 0 1499 1503 0 1502 1503 1
		 1500 1504 0 1504 1502 0 1501 1505 0 1504 1505 1 1505 1503 1 1502 1506 0 1503 1507 0
		 1506 1507 1 1504 1508 0 1508 1506 0 1505 1509 0 1508 1509 1 1509 1507 1 1506 1510 0
		 1507 1511 0 1510 1511 1 1508 1512 0 1512 1510 0 1509 1513 0 1512 1513 1 1513 1511 0
		 1510 1514 0 1511 1515 0 1514 1515 0 1512 1516 0 1516 1514 0 1513 1517 0 1516 1517 0
		 1517 1515 0 626 1518 1 604 1519 1 1518 1519 0 625 1520 1 1520 1518 0 603 1521 1 1520 1521 0
		 1521 1519 0 640 1522 1 618 1523 1 1522 1523 0 639 1524 1 1524 1522 0 617 1525 1 1524 1525 0
		 1525 1523 0 621 1526 1 600 1527 1 1526 1527 0 622 1528 1 1528 1527 0 643 1529 1 1529 1528 0
		 1529 1526 0 1518 1530 0 1519 1531 0 1530 1531 1 1520 1532 0 1532 1530 1 1521 1533 0
		 1532 1533 1 1533 1531 1 1522 1534 0 1523 1535 0 1534 1535 1 1524 1536 0 1536 1534 1
		 1525 1537 0 1536 1537 1 1537 1535 1 1526 1538 0 1527 1539 0 1538 1539 1 1528 1540 0
		 1540 1539 1 1529 1541 0 1541 1540 1 1541 1538 1 1530 1542 0 1531 1543 0 1542 1543 1
		 1532 1544 0 1544 1542 1 1533 1545 0 1544 1545 1 1545 1543 1 1534 1546 0 1535 1547 0
		 1546 1547 1 1536 1548 0 1548 1546 1 1537 1549 0 1548 1549 1 1549 1547 1 1538 1550 0
		 1539 1551 0 1550 1551 1 1540 1552 0 1552 1551 1 1541 1553 0 1553 1552 1 1553 1550 1
		 1542 1554 0 1543 1555 0 1554 1555 0 1544 1556 0 1556 1554 0 1545 1557 0 1556 1557 0
		 1557 1555 0 1546 1558 0 1547 1559 0 1558 1559 0 1548 1560 0 1560 1558 0 1549 1561 0
		 1560 1561 0 1561 1559 0 1550 1562 0 1551 1563 0 1562 1563 0 1552 1564 0 1564 1563 0
		 1553 1565 0 1565 1564 0 1565 1562 0 1566 1582 0 1567 1583 0 1566 1617 1 1568 1584 0
		 1567 1568 1 1569 1585 0 1568 1638 0 1569 1566 1;
	setAttr ".ed[3154:3319]" 1570 1624 1 1571 1572 1 1572 1647 1 1573 1570 1 1574 1586 0
		 1575 1587 0 1574 1622 1 1576 1588 1 1575 1576 1 1577 1589 1 1576 1651 0 1577 1574 1
		 1578 1590 0 1579 1591 0 1578 1619 1 1580 1592 0 1579 1580 1 1581 1593 0 1580 1634 0
		 1581 1578 1 1582 1578 0 1583 1579 0 1582 1618 1 1584 1580 1 1583 1584 1 1585 1581 1
		 1584 1637 0 1585 1582 1 1586 1570 0 1587 1571 0 1586 1623 1 1588 1572 0 1587 1588 1
		 1589 1573 0 1588 1648 0 1589 1586 1 1590 1594 0 1591 1595 0 1590 1620 1 1592 1596 1
		 1591 1592 1 1593 1597 1 1592 1633 0 1593 1590 1 1594 1574 0 1595 1575 0 1594 1621 1
		 1596 1576 0 1595 1596 1 1597 1577 0 1596 1652 0 1597 1594 1 1576 1699 0 1577 1694 0
		 1598 1650 0 1588 1698 0 1598 1600 0 1589 1695 0 1600 1649 0 1599 1601 0 1584 1690 0
		 1585 1693 0 1602 1636 0 1580 1689 0 1602 1604 0 1581 1686 0 1604 1635 0 1603 1605 0
		 1592 1683 0 1593 1678 0 1606 1632 0 1596 1682 0 1606 1608 0 1597 1679 0 1608 1653 0
		 1607 1609 0 1610 1607 0 1611 1593 0 1610 1667 1 1612 1581 0 1611 1612 1 1613 1605 0
		 1612 1687 1 1614 1603 0 1613 1614 1 1615 1585 0 1614 1658 1 1616 1569 0 1615 1616 1
		 1617 1639 1 1618 1640 1 1617 1618 1 1619 1641 1 1618 1619 1 1620 1642 0 1619 1620 1
		 1621 1643 0 1620 1621 0 1622 1644 1 1621 1622 1 1623 1645 1 1622 1623 1 1624 1646 1
		 1623 1624 1 1625 1573 1 1626 1589 0 1625 1626 1 1627 1601 0 1626 1696 1 1628 1599 0
		 1627 1628 1 1629 1577 0 1628 1676 1 1630 1597 0 1629 1630 1 1631 1609 0 1630 1680 1
		 1631 1610 1 1632 1610 0 1633 1611 0 1632 1666 1 1634 1612 0 1633 1634 1 1635 1613 0
		 1634 1688 1 1636 1614 0 1635 1636 1 1637 1615 0 1636 1657 1 1638 1616 0 1637 1638 1
		 1639 1567 1 1640 1583 1 1639 1640 1 1641 1579 1 1640 1641 1 1642 1591 1 1641 1642 1
		 1643 1595 1 1642 1643 0 1644 1575 1 1643 1644 1 1645 1587 1 1644 1645 1 1646 1571 1
		 1645 1646 1 1647 1625 0 1648 1626 0 1647 1648 1 1649 1627 0 1648 1697 1 1650 1628 0
		 1649 1650 1 1651 1629 0 1650 1675 1 1652 1630 0 1651 1652 1 1653 1631 0 1652 1681 1
		 1653 1632 1 1654 1635 1 1655 1604 0 1654 1655 1 1656 1602 0 1655 1656 1 1657 2338 0;
	setAttr ".ed[3320:3485]" 1656 1657 1 1658 2367 0 1657 1658 0 1659 1603 0 1658 1659 1
		 1660 1605 0 1659 1660 1 1661 1613 1 1660 1661 1 1661 1654 0 1662 1631 1 1663 1653 1
		 1662 1663 0 1664 1608 0 1663 1664 1 1665 1606 0 1664 1665 1 1666 2327 0 1665 1666 1
		 1667 2378 0 1666 1667 0 1668 1607 0 1667 1668 1 1669 1609 0 1668 1669 1 1669 1662 1
		 1670 1601 0 1671 1627 1 1670 1671 1 1672 1649 1 1671 1672 0 1673 1600 0 1672 1673 1
		 1674 1598 0 1673 1674 1 1675 2318 0 1674 1675 1 1676 2387 0 1675 1676 0 1677 1599 0
		 1676 1677 1 1677 1670 1 1678 1907 0 1679 1906 0 1678 1679 1 1680 1905 0 1679 1680 1
		 1681 1868 0 1680 1681 0 1682 1867 0 1681 1682 1 1683 1866 0 1682 1683 1 1684 1633 1
		 1683 1684 1 1685 1611 1 1684 1685 0 1685 1678 1 1686 1917 0 1687 1916 0 1686 1687 1
		 1688 1857 0 1687 1688 0 1689 1856 0 1688 1689 1 1690 1855 0 1689 1690 1 1691 1637 1
		 1690 1691 1 1692 1615 1 1691 1692 0 1693 1918 0 1692 1693 1 1693 1686 1 1694 1898 0
		 1695 1897 0 1694 1695 1 1696 1896 0 1695 1696 1 1697 1877 0 1696 1697 0 1698 1876 0
		 1697 1698 1 1699 1875 0 1698 1699 1 1700 1651 1 1699 1700 1 1701 1629 1 1700 1701 0
		 1701 1694 1 1654 1737 0 1688 1736 0 1687 1735 0 1661 1734 0 1663 1757 0 1681 1756 0
		 1680 1755 0 1662 1754 0 1570 1888 0 1624 1887 1 1702 1703 0 1571 1885 0 1572 1884 0
		 1704 1705 0 1647 1883 0 1705 1706 0 1573 1889 0 1707 1702 0 1646 1886 1 1703 1708 0
		 1625 1890 0 1709 1707 0 1708 1704 0 1706 1709 1 1703 1709 0 1706 1708 0 1625 1777 0
		 1647 1776 0 1706 1775 0 1709 1774 0 1710 1726 0 1711 1727 0 1710 2374 1 1712 1728 0
		 1711 1712 1 1713 1729 0 1712 1861 1 1713 1710 1 1714 1722 0 1715 1723 0 1714 2372 1
		 1716 1724 0 1715 1716 1 1717 1725 0 1716 1859 1 1717 1714 1 1718 1730 0 1719 1731 0
		 1718 2376 1 1720 1732 0 1719 1720 1 1721 1733 0 1720 1863 1 1721 1718 1 1722 1710 0
		 1723 1711 0 1722 2373 1 1724 1712 0 1723 1724 1 1725 1713 0 1724 1860 1 1725 1722 1
		 1726 1718 0 1727 1719 0 1726 2375 1 1728 1720 0 1727 1728 1 1729 1721 0 1728 1862 1
		 1729 1726 1 1730 1667 0 1731 1685 0 1730 2377 1 1732 1684 0 1731 1732 1 1733 1666 0;
	setAttr ".ed[3486:3651]" 1732 1864 1 1733 1730 1 1734 1714 0 1735 1715 0 1734 2371 1
		 1736 1716 0 1735 1736 1 1737 1717 0 1736 1858 1 1737 1734 1 1738 1746 0 1739 1747 0
		 1738 2384 1 1740 1748 0 1739 1740 1 1741 1749 0 1740 1871 1 1741 1738 1 1742 1676 0
		 1743 1701 0 1742 2386 1 1744 1700 0 1743 1744 1 1745 1675 0 1744 1873 1 1745 1742 1
		 1746 1742 0 1747 1743 0 1746 2385 1 1748 1744 0 1747 1748 1 1749 1745 0 1748 1872 1
		 1749 1746 1 1750 1738 0 1751 1739 0 1750 2383 1 1752 1740 0 1751 1752 1 1753 1741 0
		 1752 1870 1 1753 1750 1 1754 1750 0 1755 1751 0 1754 2382 1 1756 1752 0 1755 1756 1
		 1757 1753 0 1756 1869 1 1757 1754 1 1758 1671 0 1759 1672 0 1758 1759 1 1760 1697 0
		 1759 2314 1 1761 1696 0 1760 1761 1 1761 1895 1 1762 1758 0 1763 1759 0 1762 1763 1
		 1764 1760 0 1763 2313 1 1765 1761 0 1764 1765 1 1765 1894 1 1766 1762 0 1767 1763 0
		 1766 1767 1 1768 1764 0 1767 2312 1 1769 1765 0 1768 1769 1 1769 1893 1 1770 1766 0
		 1771 1767 0 1770 1771 1 1772 1768 0 1771 2311 1 1773 1769 0 1772 1773 1 1773 1892 1
		 1774 1770 0 1775 1771 0 1774 1775 1 1776 1772 0 1775 2310 1 1777 1773 0 1776 1777 1
		 1777 1891 1 1566 1838 0 1617 1839 1 1778 1779 0 1567 1841 0 1568 1842 0 1780 1781 0
		 1638 1843 0 1781 1782 0 1569 1837 0 1783 1778 0 1616 1836 0 1784 1783 0 1639 1840 1
		 1779 1785 0 1782 1784 0 1785 1780 0 1786 1778 0 1787 1783 0 1786 1787 1 1788 1784 0
		 1787 1788 1 1789 1782 0 1788 1789 0 1790 1781 0 1789 1790 1 1791 1780 0 1790 1791 1
		 1792 1785 1 1791 1792 1 1793 1779 1 1792 1793 1 1793 1786 1 1616 1831 0 1788 1830 0
		 1789 1833 0 1638 1832 0 1794 1658 0 1795 1692 0 1794 2366 1 1796 1691 0 1795 1796 1
		 1797 1657 0 1796 1853 1 1797 1794 1 1798 1794 0 1799 1795 0 1798 2365 1 1800 1796 0
		 1799 1800 1 1801 1797 0 1800 1852 1 1801 1798 1 1802 1798 0 1803 1799 0 1802 2364 1
		 1804 1800 0 1803 1804 1 1805 1801 0 1804 1851 1 1805 1802 1 1806 1802 0 1807 1803 0
		 1806 2363 1 1808 1804 0 1807 1808 1 1809 1805 0 1808 1850 1 1809 1806 1 1810 1806 0
		 1811 1807 0 1810 2362 1 1812 1808 0 1811 1812 1 1813 1809 0 1812 1849 1 1813 1810 1;
	setAttr ".ed[3652:3817]" 1814 1810 0 1815 1811 0 1814 2361 1 1816 1812 0 1815 1816 1
		 1817 1813 0 1816 1848 1 1817 1814 1 1818 1814 0 1819 1815 0 1818 2360 1 1820 1816 0
		 1819 1820 1 1821 1817 0 1820 1847 1 1821 1818 1 1822 1818 0 1823 1819 0 1822 2359 1
		 1824 1820 0 1823 1824 1 1825 1821 0 1824 1846 1 1825 1822 1 1826 1822 0 1827 1823 0
		 1826 2358 1 1828 1824 0 1827 1828 1 1829 1825 0 1828 1845 1 1829 1826 1 1830 1826 0
		 1831 1827 0 1830 2357 1 1832 1828 0 1831 1832 1 1833 1829 0 1832 1844 1 1833 1830 1
		 1834 1827 1 1835 1831 1 1834 1835 1 1836 1930 0 1835 1836 1 1837 1931 0 1836 1837 1
		 1838 1932 0 1837 1838 1 1839 1933 1 1838 1839 1 1840 1934 1 1839 1840 1 1841 1935 0
		 1840 1841 1 1842 1936 0 1841 1842 1 1843 1937 0 1842 1843 1 1844 1938 1 1843 1844 1
		 1845 1939 1 1844 1845 1 1846 1940 1 1845 1846 1 1847 1941 1 1846 1847 1 1848 1942 1
		 1847 1848 1 1849 1943 1 1848 1849 1 1850 1944 1 1849 1850 1 1851 1945 1 1850 1851 1
		 1852 1946 1 1851 1852 1 1853 1947 1 1852 1853 1 1854 1691 0 1853 1854 1 1855 1949 0
		 1854 1855 1 1856 1950 0 1855 1856 1 1857 1951 0 1856 1857 1 1858 1952 1 1857 1858 1
		 1859 1953 1 1858 1859 1 1860 1954 1 1859 1860 1 1861 1955 1 1860 1861 1 1862 1956 1
		 1861 1862 1 1863 1957 1 1862 1863 1 1864 1958 1 1863 1864 1 1865 1684 0 1864 1865 1
		 1866 1960 0 1865 1866 1 1867 1961 0 1866 1867 1 1868 1962 0 1867 1868 1 1869 1963 1
		 1868 1869 1 1870 1964 1 1869 1870 1 1871 1965 1 1870 1871 1 1872 1966 1 1871 1872 1
		 1873 1967 1 1872 1873 1 1874 1700 0 1873 1874 1 1875 1969 0 1874 1875 1 1876 1970 0
		 1875 1876 1 1877 1971 0 1876 1877 1 1878 1760 1 1877 1878 1 1879 1764 1 1878 1879 1
		 1880 1768 1 1879 1880 1 1881 1772 1 1880 1881 1 1882 1776 1 1881 1882 1 1883 1977 0
		 1882 1883 1 1884 1978 0 1883 1884 1 1885 1979 0 1884 1885 1 1886 1980 1 1885 1886 1
		 1887 1981 1 1886 1887 1 1888 1982 0 1887 1888 1 1889 1983 0 1888 1889 1 1890 1984 0
		 1889 1890 1 1891 1985 1 1890 1891 1 1892 1986 1 1891 1892 1 1893 1987 1 1892 1893 1
		 1894 1988 1 1893 1894 1 1895 1989 1 1894 1895 1 1896 1990 0 1895 1896 1 1897 1991 0;
	setAttr ".ed[3818:3983]" 1896 1897 1 1898 1992 0 1897 1898 1 1899 1701 0 1898 1899 1
		 1900 1743 1 1899 1900 1 1901 1747 1 1900 1901 1 1902 1739 1 1901 1902 1 1903 1751 1
		 1902 1903 1 1904 1755 1 1903 1904 1 1905 1999 0 1904 1905 1 1906 2000 0 1905 1906 1
		 1907 2001 0 1906 1907 1 1908 1685 0 1907 1908 1 1909 1731 1 1908 1909 1 1910 1719 1
		 1909 1910 1 1911 1727 1 1910 1911 1 1912 1711 1 1911 1912 1 1913 1723 1 1912 1913 1
		 1914 1715 1 1913 1914 1 1915 1735 1 1914 1915 1 1916 2010 0 1915 1916 1 1917 2011 0
		 1916 1917 1 1918 2012 0 1917 1918 1 1919 1692 0 1918 1919 1 1920 1795 1 1919 1920 1
		 1921 1799 1 1920 1921 1 1922 1803 1 1921 1922 1 1923 1807 1 1922 1923 1 1924 1811 1
		 1923 1924 1 1925 1815 1 1924 1925 1 1926 1819 1 1925 1926 1 1927 1823 1 1926 1927 1
		 1927 1834 1 1928 1834 1 1929 1835 1 1928 1929 1 1930 2024 0 1929 1930 1 1931 2025 0
		 1930 1931 1 1932 2026 0 1931 1932 1 1933 2027 1 1932 1933 1 1934 2028 1 1933 1934 1
		 1935 2029 0 1934 1935 1 1936 2030 0 1935 1936 1 1937 2031 0 1936 1937 1 1938 2032 1
		 1937 1938 1 1939 2033 1 1938 1939 1 1940 2034 1 1939 1940 1 1941 2035 1 1940 1941 1
		 1942 2036 1 1941 1942 1 1943 2037 1 1942 1943 1 1944 2038 1 1943 1944 1 1945 2039 1
		 1944 1945 1 1946 2040 1 1945 1946 1 1947 2041 1 1946 1947 1 1948 1854 0 1947 1948 1
		 1949 2043 0 1948 1949 1 1950 2044 0 1949 1950 1 1951 2045 0 1950 1951 1 1952 2046 1
		 1951 1952 1 1953 2047 1 1952 1953 1 1954 2048 1 1953 1954 1 1955 2049 1 1954 1955 1
		 1956 2050 1 1955 1956 1 1957 2051 1 1956 1957 1 1958 2052 1 1957 1958 1 1959 1865 0
		 1958 1959 1 1960 2054 0 1959 1960 1 1961 2055 0 1960 1961 1 1962 2056 0 1961 1962 1
		 1963 2057 1 1962 1963 1 1964 2058 1 1963 1964 1 1965 2059 1 1964 1965 1 1966 2060 1
		 1965 1966 1 1967 2061 1 1966 1967 1 1968 1874 0 1967 1968 1 1969 2063 0 1968 1969 1
		 1970 2064 0 1969 1970 1 1971 2065 0 1970 1971 1 1972 1878 1 1971 1972 1 1973 1879 1
		 1972 1973 1 1974 1880 1 1973 1974 1 1975 1881 1 1974 1975 1 1976 1882 1 1975 1976 1
		 1977 2071 0 1976 1977 1 1978 2072 0 1977 1978 1 1979 2073 0 1978 1979 1 1980 2074 1;
	setAttr ".ed[3984:4149]" 1979 1980 1 1981 2075 1 1980 1981 1 1982 2076 0 1981 1982 1
		 1983 2077 0 1982 1983 1 1984 2078 0 1983 1984 1 1985 2079 1 1984 1985 1 1986 2080 1
		 1985 1986 1 1987 2081 1 1986 1987 1 1988 2082 1 1987 1988 1 1989 2083 1 1988 1989 1
		 1990 2084 0 1989 1990 1 1991 2085 0 1990 1991 1 1992 2086 0 1991 1992 1 1993 1899 0
		 1992 1993 1 1994 1900 1 1993 1994 1 1995 1901 1 1994 1995 1 1996 1902 1 1995 1996 1
		 1997 1903 1 1996 1997 1 1998 1904 1 1997 1998 1 1999 2093 0 1998 1999 1 2000 2094 0
		 1999 2000 1 2001 2095 0 2000 2001 1 2002 1908 0 2001 2002 1 2003 1909 1 2002 2003 1
		 2004 1910 1 2003 2004 1 2005 1911 1 2004 2005 1 2006 1912 1 2005 2006 1 2007 1913 1
		 2006 2007 1 2008 1914 1 2007 2008 1 2009 1915 1 2008 2009 1 2010 2104 0 2009 2010 1
		 2011 2105 0 2010 2011 1 2012 2106 0 2011 2012 1 2013 1919 0 2012 2013 1 2014 1920 1
		 2013 2014 1 2015 1921 1 2014 2015 1 2016 1922 1 2015 2016 1 2017 1923 1 2016 2017 1
		 2018 1924 1 2017 2018 1 2019 1925 1 2018 2019 1 2020 1926 1 2019 2020 1 2021 1927 1
		 2020 2021 1 2021 1928 1 2022 1928 1 2023 1929 1 2022 2023 1 2024 2118 0 2023 2024 1
		 2025 2119 0 2024 2025 1 2026 2120 0 2025 2026 1 2027 2121 1 2026 2027 1 2028 2122 1
		 2027 2028 1 2029 2123 0 2028 2029 1 2030 2124 0 2029 2030 1 2031 2125 0 2030 2031 1
		 2032 2126 1 2031 2032 1 2033 2127 1 2032 2033 1 2034 2128 1 2033 2034 1 2035 2129 1
		 2034 2035 1 2036 2130 1 2035 2036 1 2037 2131 1 2036 2037 1 2038 2132 1 2037 2038 1
		 2039 2133 1 2038 2039 1 2040 2134 1 2039 2040 1 2041 2135 1 2040 2041 1 2042 1948 0
		 2041 2042 1 2043 2137 0 2042 2043 1 2044 2138 0 2043 2044 1 2045 2139 0 2044 2045 1
		 2046 2140 1 2045 2046 1 2047 2141 1 2046 2047 1 2048 2142 1 2047 2048 1 2049 2143 1
		 2048 2049 1 2050 2144 1 2049 2050 1 2051 2145 1 2050 2051 1 2052 2146 1 2051 2052 1
		 2053 1959 0 2052 2053 1 2054 2148 0 2053 2054 1 2055 2149 0 2054 2055 1 2056 2150 0
		 2055 2056 1 2057 2151 1 2056 2057 1 2058 2152 1 2057 2058 1 2059 2153 1 2058 2059 1
		 2060 2154 1 2059 2060 1 2061 2155 1 2060 2061 1 2062 1968 0 2061 2062 1 2063 2157 0;
	setAttr ".ed[4150:4315]" 2062 2063 1 2064 2158 0 2063 2064 1 2065 2159 0 2064 2065 1
		 2066 1972 1 2065 2066 1 2067 1973 1 2066 2067 1 2068 1974 1 2067 2068 1 2069 1975 1
		 2068 2069 1 2070 1976 1 2069 2070 1 2071 2165 0 2070 2071 1 2072 2166 0 2071 2072 1
		 2073 2167 0 2072 2073 1 2074 2168 1 2073 2074 1 2075 2169 1 2074 2075 1 2076 2170 0
		 2075 2076 1 2077 2171 0 2076 2077 1 2078 2172 0 2077 2078 1 2079 2173 1 2078 2079 1
		 2080 2174 1 2079 2080 1 2081 2175 1 2080 2081 1 2082 2176 1 2081 2082 1 2083 2177 1
		 2082 2083 1 2084 2178 0 2083 2084 1 2085 2179 0 2084 2085 1 2086 2180 0 2085 2086 1
		 2087 1993 0 2086 2087 1 2088 1994 1 2087 2088 1 2089 1995 1 2088 2089 1 2090 1996 1
		 2089 2090 1 2091 1997 1 2090 2091 1 2092 1998 1 2091 2092 1 2093 2187 0 2092 2093 1
		 2094 2188 0 2093 2094 1 2095 2189 0 2094 2095 1 2096 2002 0 2095 2096 1 2097 2003 1
		 2096 2097 1 2098 2004 1 2097 2098 1 2099 2005 1 2098 2099 1 2100 2006 1 2099 2100 1
		 2101 2007 1 2100 2101 1 2102 2008 1 2101 2102 1 2103 2009 1 2102 2103 1 2104 2198 0
		 2103 2104 1 2105 2199 0 2104 2105 1 2106 2200 0 2105 2106 1 2107 2013 0 2106 2107 1
		 2108 2014 1 2107 2108 1 2109 2015 1 2108 2109 1 2110 2016 1 2109 2110 1 2111 2017 1
		 2110 2111 1 2112 2018 1 2111 2112 1 2113 2019 1 2112 2113 1 2114 2020 1 2113 2114 1
		 2115 2021 1 2114 2115 1 2115 2022 1 2116 2022 1 2117 2023 1 2116 2117 1 2118 2212 0
		 2117 2118 1 2119 2213 0 2118 2119 1 2120 2214 0 2119 2120 1 2121 2215 1 2120 2121 1
		 2122 2216 1 2121 2122 1 2123 2217 0 2122 2123 1 2124 2218 0 2123 2124 1 2125 2219 0
		 2124 2125 1 2126 2220 1 2125 2126 1 2127 2221 1 2126 2127 1 2128 2222 1 2127 2128 1
		 2129 2223 1 2128 2129 1 2130 2224 1 2129 2130 1 2131 2225 1 2130 2131 1 2132 2226 1
		 2131 2132 1 2133 2227 1 2132 2133 1 2134 2228 1 2133 2134 1 2135 2229 1 2134 2135 1
		 2136 2042 0 2135 2136 1 2137 2231 0 2136 2137 1 2138 2232 0 2137 2138 1 2139 2233 0
		 2138 2139 1 2140 2234 1 2139 2140 1 2141 2235 1 2140 2141 1 2142 2236 1 2141 2142 1
		 2143 2237 1 2142 2143 1 2144 2238 1 2143 2144 1 2145 2239 1 2144 2145 1 2146 2240 1;
	setAttr ".ed[4316:4481]" 2145 2146 1 2147 2053 0 2146 2147 1 2148 2242 0 2147 2148 1
		 2149 2243 0 2148 2149 1 2150 2244 0 2149 2150 1 2151 2245 1 2150 2151 1 2152 2246 1
		 2151 2152 1 2153 2247 1 2152 2153 1 2154 2248 1 2153 2154 1 2155 2249 1 2154 2155 1
		 2156 2062 0 2155 2156 1 2157 2251 0 2156 2157 1 2158 2252 0 2157 2158 1 2159 2253 0
		 2158 2159 1 2160 2066 1 2159 2160 1 2161 2067 1 2160 2161 1 2162 2068 1 2161 2162 1
		 2163 2069 1 2162 2163 1 2164 2070 1 2163 2164 1 2165 2259 0 2164 2165 1 2166 2260 0
		 2165 2166 1 2167 2261 0 2166 2167 1 2168 2262 1 2167 2168 1 2169 2263 1 2168 2169 1
		 2170 2264 0 2169 2170 1 2171 2265 0 2170 2171 1 2172 2266 0 2171 2172 1 2173 2267 1
		 2172 2173 1 2174 2268 1 2173 2174 1 2175 2269 1 2174 2175 1 2176 2270 1 2175 2176 1
		 2177 2271 1 2176 2177 1 2178 2272 0 2177 2178 1 2179 2273 0 2178 2179 1 2180 2274 0
		 2179 2180 1 2181 2087 0 2180 2181 1 2182 2088 1 2181 2182 1 2183 2089 1 2182 2183 1
		 2184 2090 1 2183 2184 1 2185 2091 1 2184 2185 1 2186 2092 1 2185 2186 1 2187 2281 0
		 2186 2187 1 2188 2282 0 2187 2188 1 2189 2283 0 2188 2189 1 2190 2096 0 2189 2190 1
		 2191 2097 1 2190 2191 1 2192 2098 1 2191 2192 1 2193 2099 1 2192 2193 1 2194 2100 1
		 2193 2194 1 2195 2101 1 2194 2195 1 2196 2102 1 2195 2196 1 2197 2103 1 2196 2197 1
		 2198 2292 0 2197 2198 1 2199 2293 0 2198 2199 1 2200 2294 0 2199 2200 1 2201 2107 0
		 2200 2201 1 2202 2108 1 2201 2202 1 2203 2109 1 2202 2203 1 2204 2110 1 2203 2204 1
		 2205 2111 1 2204 2205 1 2206 2112 1 2205 2206 1 2207 2113 1 2206 2207 1 2208 2114 1
		 2207 2208 1 2209 2115 1 2208 2209 1 2209 2116 1 2210 2116 1 2211 2117 1 2210 2211 1
		 2212 2400 0 2211 2212 1 2213 2399 0 2212 2213 1 2214 2398 0 2213 2214 1 2215 2491 1
		 2214 2215 1 2216 2490 1 2215 2216 1 2217 2489 0 2216 2217 1 2218 2488 0 2217 2218 1
		 2219 2487 0 2218 2219 1 2220 2486 1 2219 2220 1 2221 2485 1 2220 2221 1 2222 2484 1
		 2221 2222 1 2223 2483 1 2222 2223 1 2224 2482 1 2223 2224 1 2225 2481 1 2224 2225 1
		 2226 2480 1 2225 2226 1 2227 2479 1 2226 2227 1 2228 2478 1 2227 2228 1 2229 2477 1;
	setAttr ".ed[4482:4647]" 2228 2229 1 2230 2136 0 2229 2230 1 2231 2475 0 2230 2231 1
		 2232 2474 0 2231 2232 1 2233 2473 0 2232 2233 1 2234 2472 1 2233 2234 1 2235 2471 1
		 2234 2235 1 2236 2470 1 2235 2236 1 2237 2469 1 2236 2237 1 2238 2468 1 2237 2238 1
		 2239 2467 1 2238 2239 1 2240 2466 1 2239 2240 1 2241 2147 0 2240 2241 1 2242 2464 0
		 2241 2242 1 2243 2463 0 2242 2243 1 2244 2462 0 2243 2244 1 2245 2461 1 2244 2245 1
		 2246 2460 1 2245 2246 1 2247 2459 1 2246 2247 1 2248 2458 1 2247 2248 1 2249 2457 1
		 2248 2249 1 2250 2156 0 2249 2250 1 2251 2455 0 2250 2251 1 2252 2454 0 2251 2252 1
		 2253 2453 0 2252 2253 1 2254 2160 1 2253 2254 1 2255 2161 1 2254 2255 1 2256 2162 1
		 2255 2256 1 2257 2163 1 2256 2257 1 2258 2164 1 2257 2258 1 2259 2447 0 2258 2259 1
		 2260 2446 0 2259 2260 1 2261 2445 0 2260 2261 1 2262 2444 1 2261 2262 1 2263 2443 1
		 2262 2263 1 2264 2442 0 2263 2264 1 2265 2441 0 2264 2265 1 2266 2440 0 2265 2266 1
		 2267 2439 1 2266 2267 1 2268 2438 1 2267 2268 1 2269 2437 1 2268 2269 1 2270 2436 1
		 2269 2270 1 2271 2435 1 2270 2271 1 2272 2434 0 2271 2272 1 2273 2433 0 2272 2273 1
		 2274 2432 0 2273 2274 1 2275 2181 0 2274 2275 1 2276 2182 1 2275 2276 1 2277 2183 1
		 2276 2277 1 2278 2184 1 2277 2278 1 2279 2185 1 2278 2279 1 2280 2186 1 2279 2280 1
		 2281 2425 0 2280 2281 1 2282 2424 0 2281 2282 1 2283 2423 0 2282 2283 1 2284 2190 0
		 2283 2284 1 2285 2191 1 2284 2285 1 2286 2192 1 2285 2286 1 2287 2193 1 2286 2287 1
		 2288 2194 1 2287 2288 1 2289 2195 1 2288 2289 1 2290 2196 1 2289 2290 1 2291 2197 1
		 2290 2291 1 2292 2414 0 2291 2292 1 2293 2413 0 2292 2293 1 2294 2412 0 2293 2294 1
		 2295 2201 0 2294 2295 1 2296 2202 1 2295 2296 1 2297 2203 1 2296 2297 1 2298 2204 1
		 2297 2298 1 2299 2205 1 2298 2299 1 2300 2206 1 2299 2300 1 2301 2207 1 2300 2301 1
		 2302 2208 1 2301 2302 1 2303 2209 1 2302 2303 1 2303 2210 1 2304 1702 0 2305 1703 1
		 2304 2305 1 2306 1708 1 2305 2306 1 2307 1704 0 2306 2307 1 2308 1705 0 2307 2308 1
		 2309 1706 0 2308 2309 1 2310 2448 1 2309 2310 1 2311 2449 1 2310 2311 1 2312 2450 1;
	setAttr ".ed[4648:4813]" 2311 2312 1 2313 2451 1 2312 2313 1 2314 2452 1 2313 2314 1
		 2315 1672 0 2314 2315 1 2316 1673 0 2315 2316 1 2317 1674 0 2316 2317 1 2318 2456 0
		 2317 2318 1 2319 1745 1 2318 2319 1 2320 1749 1 2319 2320 1 2321 1741 1 2320 2321 1
		 2322 1753 1 2321 2322 1 2323 1757 1 2322 2323 1 2324 1663 0 2323 2324 1 2325 1664 0
		 2324 2325 1 2326 1665 0 2325 2326 1 2327 2465 0 2326 2327 1 2328 1733 1 2327 2328 1
		 2329 1721 1 2328 2329 1 2330 1729 1 2329 2330 1 2331 1713 1 2330 2331 1 2332 1725 1
		 2331 2332 1 2333 1717 1 2332 2333 1 2334 1737 1 2333 2334 1 2335 1654 0 2334 2335 1
		 2336 1655 0 2335 2336 1 2337 1656 0 2336 2337 1 2338 2476 0 2337 2338 1 2339 1797 1
		 2338 2339 1 2340 1801 1 2339 2340 1 2341 1805 1 2340 2341 1 2342 1809 1 2341 2342 1
		 2343 1813 1 2342 2343 1 2344 1817 1 2343 2344 1 2345 1821 1 2344 2345 1 2346 1825 1
		 2345 2346 1 2347 1829 1 2346 2347 1 2348 1833 1 2347 2348 1 2349 1789 0 2348 2349 1
		 2350 1790 0 2349 2350 1 2351 1791 0 2350 2351 1 2352 1792 1 2351 2352 1 2353 1793 1
		 2352 2353 1 2354 1786 0 2353 2354 1 2355 1787 0 2354 2355 1 2356 1788 0 2355 2356 1
		 2357 2401 1 2356 2357 1 2358 2402 1 2357 2358 1 2359 2403 1 2358 2359 1 2360 2404 1
		 2359 2360 1 2361 2405 1 2360 2361 1 2362 2406 1 2361 2362 1 2363 2407 1 2362 2363 1
		 2364 2408 1 2363 2364 1 2365 2409 1 2364 2365 1 2366 2410 1 2365 2366 1 2367 2411 0
		 2366 2367 1 2368 1659 0 2367 2368 1 2369 1660 0 2368 2369 1 2370 1661 0 2369 2370 1
		 2371 2415 1 2370 2371 1 2372 2416 1 2371 2372 1 2373 2417 1 2372 2373 1 2374 2418 1
		 2373 2374 1 2375 2419 1 2374 2375 1 2376 2420 1 2375 2376 1 2377 2421 1 2376 2377 1
		 2378 2422 0 2377 2378 1 2379 1668 0 2378 2379 1 2380 1669 0 2379 2380 1 2381 1662 0
		 2380 2381 1 2382 2426 1 2381 2382 1 2383 2427 1 2382 2383 1 2384 2428 1 2383 2384 1
		 2385 2429 1 2384 2385 1 2386 2430 1 2385 2386 1 2387 2431 0 2386 2387 1 2388 1677 0
		 2387 2388 1 2389 1670 0 2388 2389 1 2390 1671 0 2389 2390 1 2391 1758 1 2390 2391 1
		 2392 1762 1 2391 2392 1 2393 1766 1 2392 2393 1 2394 1770 1 2393 2394 1 2395 1774 1;
	setAttr ".ed[4814:4979]" 2394 2395 1 2396 1709 0 2395 2396 1 2397 1707 0 2396 2397 1
		 2397 2304 1 2398 2354 0 2399 2355 0 2398 2399 1 2400 2356 0 2399 2400 1 2401 2211 1
		 2400 2401 1 2402 2210 1 2401 2402 1 2403 2303 1 2402 2403 1 2404 2302 1 2403 2404 1
		 2405 2301 1 2404 2405 1 2406 2300 1 2405 2406 1 2407 2299 1 2406 2407 1 2408 2298 1
		 2407 2408 1 2409 2297 1 2408 2409 1 2410 2296 1 2409 2410 1 2411 2295 0 2410 2411 1
		 2412 2368 0 2411 2412 1 2413 2369 0 2412 2413 1 2414 2370 0 2413 2414 1 2415 2291 1
		 2414 2415 1 2416 2290 1 2415 2416 1 2417 2289 1 2416 2417 1 2418 2288 1 2417 2418 1
		 2419 2287 1 2418 2419 1 2420 2286 1 2419 2420 1 2421 2285 1 2420 2421 1 2422 2284 0
		 2421 2422 1 2423 2379 0 2422 2423 1 2424 2380 0 2423 2424 1 2425 2381 0 2424 2425 1
		 2426 2280 1 2425 2426 1 2427 2279 1 2426 2427 1 2428 2278 1 2427 2428 1 2429 2277 1
		 2428 2429 1 2430 2276 1 2429 2430 1 2431 2275 0 2430 2431 1 2432 2388 0 2431 2432 1
		 2433 2389 0 2432 2433 1 2434 2390 0 2433 2434 1 2435 2391 1 2434 2435 1 2436 2392 1
		 2435 2436 1 2437 2393 1 2436 2437 1 2438 2394 1 2437 2438 1 2439 2395 1 2438 2439 1
		 2440 2396 0 2439 2440 1 2441 2397 0 2440 2441 1 2442 2304 0 2441 2442 1 2443 2305 1
		 2442 2443 1 2444 2306 1 2443 2444 1 2445 2307 0 2444 2445 1 2446 2308 0 2445 2446 1
		 2447 2309 0 2446 2447 1 2448 2258 1 2447 2448 1 2449 2257 1 2448 2449 1 2450 2256 1
		 2449 2450 1 2451 2255 1 2450 2451 1 2452 2254 1 2451 2452 1 2453 2315 0 2452 2453 1
		 2454 2316 0 2453 2454 1 2455 2317 0 2454 2455 1 2456 2250 0 2455 2456 1 2457 2319 1
		 2456 2457 1 2458 2320 1 2457 2458 1 2459 2321 1 2458 2459 1 2460 2322 1 2459 2460 1
		 2461 2323 1 2460 2461 1 2462 2324 0 2461 2462 1 2463 2325 0 2462 2463 1 2464 2326 0
		 2463 2464 1 2465 2241 0 2464 2465 1 2466 2328 1 2465 2466 1 2467 2329 1 2466 2467 1
		 2468 2330 1 2467 2468 1 2469 2331 1 2468 2469 1 2470 2332 1 2469 2470 1 2471 2333 1
		 2470 2471 1 2472 2334 1 2471 2472 1 2473 2335 0 2472 2473 1 2474 2336 0 2473 2474 1
		 2475 2337 0 2474 2475 1 2476 2230 0 2475 2476 1 2477 2339 1 2476 2477 1 2478 2340 1;
	setAttr ".ed[4980:5145]" 2477 2478 1 2479 2341 1 2478 2479 1 2480 2342 1 2479 2480 1
		 2481 2343 1 2480 2481 1 2482 2344 1 2481 2482 1 2483 2345 1 2482 2483 1 2484 2346 1
		 2483 2484 1 2485 2347 1 2484 2485 1 2486 2348 1 2485 2486 1 2487 2349 0 2486 2487 1
		 2488 2350 0 2487 2488 1 2489 2351 0 2488 2489 1 2490 2352 1 2489 2490 1 2491 2353 1
		 2490 2491 1 2491 2398 1 1642 2492 0 1643 2493 0 2492 2493 1 1620 2494 0 2494 2492 1
		 1621 2495 0 2494 2495 1 2495 2493 1 2492 2496 0 2493 2497 0 2496 2497 0 2494 2498 0
		 2498 2496 0 2495 2499 0 2498 2499 0 2499 2497 0 2496 2500 0 2497 2501 0 2500 2501 1
		 2498 2502 0 2502 2500 0 2499 2503 0 2502 2503 1 2503 2501 0 2500 2504 1 2501 2505 1
		 2504 2505 0 2502 2506 1 2506 2504 0 2503 2507 1 2506 2507 0 2507 2505 0 2504 2508 0
		 2505 2509 0 2508 2509 1 2506 2510 0 2510 2508 1 2507 2511 0 2510 2511 1 2511 2509 1
		 2508 2512 0 2509 2513 0 2512 2513 1 2510 2514 0 2514 2512 0 2511 2515 0 2514 2515 1
		 2515 2513 1 2512 2516 0 2513 2517 0 2516 2517 1 2514 2518 0 2518 2516 0 2515 2519 0
		 2518 2519 1 2519 2517 1 2516 2520 0 2517 2521 0 2520 2521 1 2518 2522 0 2522 2520 0
		 2519 2523 0 2522 2523 1 2523 2521 0 2520 2524 0 2521 2525 0 2524 2525 0 2522 2526 0
		 2526 2524 0 2523 2527 0 2526 2527 0 2527 2525 0 1636 2528 1 1614 2529 1 2528 2529 0
		 1635 2530 1 2530 2528 0 1613 2531 1 2530 2531 0 2531 2529 0 1650 2532 1 1628 2533 1
		 2532 2533 0 1649 2534 1 2534 2532 0 1627 2535 1 2534 2535 0 2535 2533 0 1631 2536 1
		 1610 2537 1 2536 2537 0 1632 2538 1 2538 2537 0 1653 2539 1 2539 2538 0 2539 2536 0
		 2528 2540 0 2529 2541 0 2540 2541 1 2530 2542 0 2542 2540 1 2531 2543 0 2542 2543 1
		 2543 2541 1 2532 2544 0 2533 2545 0 2544 2545 1 2534 2546 0 2546 2544 1 2535 2547 0
		 2546 2547 1 2547 2545 1 2536 2548 0 2537 2549 0 2548 2549 1 2538 2550 0 2550 2549 1
		 2539 2551 0 2551 2550 1 2551 2548 1 2540 2552 0 2541 2553 0 2552 2553 1 2542 2554 0
		 2554 2552 1 2543 2555 0 2554 2555 1 2555 2553 1 2544 2556 0 2545 2557 0 2556 2557 1
		 2546 2558 0 2558 2556 1 2547 2559 0 2558 2559 1 2559 2557 1 2548 2560 0 2549 2561 0;
	setAttr ".ed[5146:5311]" 2560 2561 1 2550 2562 0 2562 2561 1 2551 2563 0 2563 2562 1
		 2563 2560 1 2552 2564 0 2553 2565 0 2564 2565 0 2554 2566 0 2566 2564 0 2555 2567 0
		 2566 2567 0 2567 2565 0 2556 2568 0 2557 2569 0 2568 2569 0 2558 2570 0 2570 2568 0
		 2559 2571 0 2570 2571 0 2571 2569 0 2560 2572 0 2561 2573 0 2572 2573 0 2562 2574 0
		 2574 2573 0 2563 2575 0 2575 2574 0 2575 2572 0 2576 2577 1 2577 2578 1 2578 2579 1
		 2579 2580 1 2580 2581 1 2581 2582 1 2582 4166 1 2583 2584 1 2584 2585 1 2585 2576 1
		 2586 2587 1 2587 2588 1 2588 2589 1 2589 2590 1 2590 2591 1 2591 2592 1 2592 4164 1
		 2593 2594 1 2594 2595 1 2595 2586 1 2596 2597 0 2597 2598 1 2598 2599 0 2599 2600 1
		 2600 2601 1 2601 2602 1 2602 4162 1 2603 2604 1 2604 2605 1 2605 2596 1 2606 2607 1
		 2607 2608 1 2608 2609 1 2609 2610 1 2610 2611 1 2611 2612 1 2612 4157 0 2613 2614 1
		 2614 2615 1 2615 2606 1 2576 2746 0 2577 2755 0 2578 2754 0 2579 2753 0 2580 2752 0
		 2581 2751 0 2582 2750 0 2583 2749 0 2584 2748 0 2585 2747 0 2586 2736 0 2587 2745 0
		 2588 2744 0 2589 2743 0 2590 2742 0 2591 2741 0 2592 2740 0 2593 2739 0 2594 2738 0
		 2595 2737 0 2596 3494 0 2597 3505 0 2598 3504 0 2599 3501 0 2600 3500 0 2601 3499 0
		 2602 3498 0 2603 3497 0 2604 3496 0 2605 3495 0 2606 2616 0 2607 2617 0 2616 2617 1
		 2608 2618 0 2617 2618 0 2609 2619 0 2618 2619 1 2610 2620 0 2619 2620 1 2611 2621 0
		 2620 2621 1 2612 2622 0 2621 2622 1 2613 2623 0 2622 4156 1 2614 2624 0 2623 2624 1
		 2615 2625 0 2624 2625 1 2625 2616 1 2616 2626 0 2617 2627 0 2626 2627 1 2618 2628 0
		 2627 2628 1 2619 2629 0 2628 2629 1 2620 2630 0 2629 2630 1 2621 2631 0 2630 2631 1
		 2622 2632 0 2631 2632 1 2623 2633 0 2632 4155 1 2624 2634 0 2633 2634 1 2625 2635 0
		 2634 2635 1 2635 2626 1 2626 2763 0 2627 2762 0 2636 2637 1 2628 2761 0 2637 2638 1
		 2629 2760 0 2638 2639 1 2630 2759 0 2639 2640 1 2631 2758 0 2640 2641 1 2632 2757 0
		 2641 2642 1 2633 2756 0 2642 4152 1 2634 2765 0 2643 2644 1 2635 2764 0 2644 2645 1
		 2645 2636 1 2636 2713 0 2637 2712 0 2646 2647 1 2638 2711 0 2647 2648 1 2639 2710 0;
	setAttr ".ed[5312:5477]" 2648 2649 1 2640 2709 0 2649 2650 1 2641 2708 0 2650 2651 1
		 2642 2707 0 2651 2652 1 2643 2706 0 2652 4142 1 2644 2715 0 2653 2654 1 2645 2714 0
		 2654 2655 1 2655 2646 1 2656 3084 0 2657 3085 0 2656 2657 1 2658 3076 0 2657 2658 1
		 2659 3077 0 2658 2659 1 2660 3078 0 2659 2660 1 2661 3079 0 2660 4208 1 2662 3080 0
		 2661 2662 1 2663 3081 0 2662 2663 1 2664 3082 0 2663 2664 1 2665 3083 0 2664 2665 1
		 2665 2656 1 2666 3064 0 2667 3065 0 2666 2667 1 2668 3056 0 2667 2668 1 2669 3057 0
		 2668 2669 1 2670 3058 0 2669 2670 1 2671 3059 0 2670 4210 1 2672 3060 0 2671 2672 1
		 2673 3061 0 2672 2673 1 2674 3062 0 2673 2674 1 2675 3063 0 2674 2675 1 2675 2666 1
		 2676 3075 0 2677 3066 0 2676 2677 1 2678 3067 0 2677 2678 1 2679 3068 0 2678 2679 1
		 2680 3069 0 2679 4212 1 2681 3070 0 2680 2681 1 2682 3071 0 2681 2682 1 2683 3072 0
		 2682 2683 1 2684 3073 0 2683 2684 1 2685 3074 0 2684 2685 1 2685 2676 1 2686 2677 0
		 2687 2678 0 2686 2687 1 2688 2679 0 2687 2688 1 2689 2680 0 2688 4213 1 2690 2681 0
		 2689 2690 1 2691 2682 0 2690 2691 1 2692 2683 0 2691 2692 1 2693 2684 0 2692 2693 1
		 2694 2685 0 2693 2694 1 2695 2676 0 2694 2695 1 2695 2686 1 2696 2688 0 2697 2689 0
		 2696 4214 1 2698 2690 0 2697 2698 1 2699 2691 0 2698 2699 1 2700 2692 0 2699 2700 1
		 2701 2693 0 2700 2701 1 2702 2694 0 2701 2702 1 2703 2695 0 2702 2703 1 2704 2686 0
		 2703 2704 1 2705 2687 0 2704 2705 1 2705 2696 1 2706 2696 0 2707 2697 0 2706 4215 1
		 2708 2698 0 2707 2708 1 2709 2699 0 2708 2709 1 2710 2700 0 2709 2710 1 2711 2701 0
		 2710 2711 1 2712 2702 0 2711 2712 1 2713 2703 0 2712 2713 1 2714 2704 0 2713 2714 1
		 2715 2705 0 2714 2715 1 2715 2706 1 2716 2606 0 2717 2615 0 2716 2717 1 2718 2614 0
		 2717 2718 1 2719 2613 0 2718 2719 1 2720 2612 0 2719 4222 1 2721 2611 0 2720 2721 1
		 2722 2610 0 2721 2722 1 2723 2609 0 2722 2723 1 2724 2608 0 2723 2724 1 2725 2607 0
		 2724 2725 1 2725 2716 1 2726 2716 0 2727 2717 0 2726 2727 1 2728 2718 0 2727 2728 1
		 2729 2719 0 2728 2729 1 2730 2720 0 2729 4223 1 2731 2721 0 2730 2731 1 2732 2722 0;
	setAttr ".ed[5478:5643]" 2731 2732 1 2733 2723 0 2732 2733 1 2734 2724 0 2733 2734 0
		 2735 2725 0 2734 2735 1 2735 2726 0 2736 2596 0 2737 2605 0 2736 2737 1 2738 2604 0
		 2737 2738 1 2739 2603 0 2738 2739 1 2740 2602 0 2739 4227 1 2741 2601 0 2740 2741 1
		 2742 2600 0 2741 2742 1 2743 2599 0 2742 2743 1 2744 2598 0 2743 2744 1 2745 2597 0
		 2744 2745 1 2745 2736 1 2746 2586 0 2747 2595 0 2746 2747 1 2748 2594 0 2747 2748 1
		 2749 2593 0 2748 2749 1 2750 2592 0 2749 4229 1 2751 2591 0 2750 2751 1 2752 2590 0
		 2751 2752 1 2753 2589 0 2752 2753 1 2754 2588 0 2753 2754 1 2755 2587 0 2754 2755 1
		 2755 2746 1 2756 2767 0 2757 2768 0 2756 4218 1 2758 2769 0 2757 2758 1 2759 2770 0
		 2758 2759 1 2760 2771 0 2759 2760 1 2761 2772 0 2760 2761 1 2762 2773 0 2761 2762 1
		 2763 2774 0 2762 2763 1 2764 2775 0 2763 2764 1 2765 2766 0 2764 2765 1 2765 2756 1
		 2766 2644 0 2767 2643 0 2766 2767 1 2768 2642 0 2767 4217 1 2769 2641 0 2768 2769 1
		 2770 2640 0 2769 2770 1 2771 2639 0 2770 2771 1 2772 2638 0 2771 2772 1 2773 2637 0
		 2772 2773 1 2774 2636 0 2773 2774 1 2775 2645 0 2774 2775 1 2775 2766 1 2576 2805 0
		 2577 2796 0 2776 2777 0 2578 2797 0 2777 2778 0 2579 2798 0 2778 2779 0 2580 2799 0
		 2779 2780 1 2581 2800 0 2780 2781 1 2582 2801 0 2781 2782 1 2583 2802 0 2782 4170 1
		 2584 2803 0 2783 2784 1 2585 2804 0 2784 2785 1 2785 2776 1 2786 2806 0 2787 2807 0
		 2786 2787 1 2788 2808 0 2787 2788 1 2789 2809 0 2788 2789 1 2790 2810 0 2789 2790 1
		 2791 2811 0 2790 2791 1 2792 2812 0 2791 4168 1 2793 2813 0 2792 2793 1 2794 2814 0
		 2793 2794 1 2795 2815 0 2794 2795 1 2795 2786 1 2796 2786 0 2797 2787 0 2796 2797 1
		 2798 2788 0 2797 2798 1 2799 2789 0 2798 2799 1 2800 2790 0 2799 2800 1 2801 2791 0
		 2800 2801 1 2802 2792 0 2801 4167 1 2803 2793 0 2802 2803 1 2804 2794 0 2803 2804 1
		 2805 2795 0 2804 2805 1 2805 2796 1 2806 2777 0 2807 2778 0 2806 2807 1 2808 2779 0
		 2807 2808 1 2809 2780 0 2808 2809 1 2810 2781 0 2809 2810 1 2811 2782 0 2810 2811 1
		 2812 2783 0 2811 4169 1 2813 2784 0 2812 2813 1 2814 2785 0 2813 2814 1 2815 2776 0;
	setAttr ".ed[5644:5809]" 2814 2815 1 2815 2806 1 2776 2816 0 2777 2817 0 2816 2817 0
		 2778 2818 0 2817 2818 0 2779 2819 0 2818 2819 0 2780 2820 0 2819 2820 0 2781 2821 0
		 2820 2821 0 2782 2822 0 2821 2822 0 2783 2823 0 2822 4171 0 2784 2824 0 2823 2824 0
		 2785 2825 0 2824 2825 0 2825 2816 0 2816 2846 0 2817 2837 0 2826 2827 0 2828 2826 1
		 2828 2827 1 2818 2838 0 2827 2829 0 2828 2829 1 2819 2839 0 2829 2830 0 2828 2830 1
		 2820 2840 0 2830 2831 0 2828 2831 1 2821 2841 0 2831 2832 0 2828 2832 1 2822 2842 0
		 2832 2833 0 2828 2833 1 2823 2843 0 2833 4185 0 2828 2834 1 2824 2844 0 2834 2835 0
		 2828 2835 1 2825 2845 0 2835 2836 0 2828 2836 1 2836 2826 0 2837 2849 0 2838 2850 0
		 2837 2838 1 2839 2851 0 2838 2839 1 2840 2852 0 2839 2840 1 2841 2853 0 2840 2841 1
		 2842 2854 0 2841 2842 1 2843 2855 0 2842 4172 1 2844 2856 0 2843 2844 1 2845 2847 0
		 2844 2845 1 2846 2848 0 2845 2846 1 2846 2837 1 2847 2866 0 2848 2857 0 2847 2848 1
		 2849 2858 0 2848 2849 1 2850 2859 0 2849 2850 1 2851 2860 0 2850 2851 1 2852 2861 0
		 2851 2852 1 2853 2862 0 2852 2853 1 2854 2863 0 2853 2854 1 2855 2864 0 2854 4173 1
		 2856 2865 0 2855 2856 1 2856 2847 1 2857 2867 0 2858 2868 0 2857 2858 1 2859 2869 0
		 2858 2859 1 2860 2870 0 2859 2860 1 2861 2871 0 2860 2861 1 2862 2872 0 2861 2862 1
		 2863 2873 0 2862 2863 1 2864 2874 0 2863 4174 1 2865 2875 0 2864 2865 1 2866 2876 0
		 2865 2866 1 2866 2857 1 2867 2878 0 2868 2879 0 2867 2868 1 2869 2880 0 2868 2869 1
		 2870 2881 0 2869 2870 1 2871 2882 0 2870 2871 1 2872 2883 0 2871 2872 1 2873 2884 0
		 2872 2873 1 2874 2885 0 2873 4175 1 2875 2886 0 2874 2875 1 2876 2877 0 2875 2876 1
		 2876 2867 1 2877 2887 0 2878 2888 0 2877 2878 1 2879 2889 0 2878 2879 1 2880 2890 0
		 2879 2880 1 2881 2891 0 2880 2881 1 2882 2892 0 2881 2882 1 2883 2893 0 2882 2883 1
		 2884 2894 0 2883 2884 1 2885 2895 0 2884 4176 1 2886 2896 0 2885 2886 1 2886 2877 1
		 2887 2897 0 2888 2898 0 2887 2888 1 2889 2899 0 2888 2889 1 2890 2900 0 2889 2890 1
		 2891 2901 0 2890 2891 1 2892 2902 0 2891 2892 1 2893 2903 0 2892 2893 1 2894 2904 0;
	setAttr ".ed[5810:5975]" 2893 2894 1 2895 2905 0 2894 4177 1 2896 2906 0 2895 2896 1
		 2896 2887 1 2897 2907 0 2898 2908 0 2897 2898 1 2899 2909 0 2898 2899 1 2900 2910 0
		 2899 2900 1 2901 2911 0 2900 2901 1 2902 2912 0 2901 2902 1 2903 2913 0 2902 2903 1
		 2904 2914 0 2903 2904 1 2905 2915 0 2904 4178 1 2906 2916 0 2905 2906 1 2906 2897 1
		 2907 2917 0 2908 2918 0 2907 2908 1 2909 2919 0 2908 2909 1 2910 2920 0 2909 2910 1
		 2911 2921 0 2910 2911 1 2912 2922 0 2911 2912 1 2913 2923 0 2912 2913 1 2914 2924 0
		 2913 2914 1 2915 2925 0 2914 4179 1 2916 2926 0 2915 2916 1 2916 2907 1 2917 2927 0
		 2918 2928 0 2917 2918 1 2919 2929 0 2918 2919 1 2920 2930 0 2919 2920 1 2921 2931 0
		 2920 2921 1 2922 2932 0 2921 2922 1 2923 2933 0 2922 2923 1 2924 2934 0 2923 2924 1
		 2925 2935 0 2924 4180 1 2926 2936 0 2925 2926 1 2926 2917 1 2927 2937 0 2928 2938 0
		 2927 2928 1 2929 2939 0 2928 2929 1 2930 2940 0 2929 2930 1 2931 2941 0 2930 2931 1
		 2932 2942 0 2931 2932 1 2933 2943 0 2932 2933 1 2934 2944 0 2933 2934 1 2935 2945 0
		 2934 4181 1 2936 2946 0 2935 2936 1 2936 2927 1 2937 2949 0 2938 2950 0 2937 2938 1
		 2939 2951 0 2938 2939 1 2940 2952 0 2939 2940 1 2941 2953 0 2940 2941 1 2942 2954 0
		 2941 2942 1 2943 2955 0 2942 2943 1 2944 2956 0 2943 2944 1 2945 2947 0 2944 4182 1
		 2946 2948 0 2945 2946 1 2946 2937 1 2947 2957 0 2948 2958 0 2947 2948 1 2949 2959 0
		 2948 2949 1 2950 2960 0 2949 2950 1 2951 2961 0 2950 2951 1 2952 2962 0 2951 2952 1
		 2953 2963 0 2952 2953 1 2954 2964 0 2953 2954 1 2955 2965 0 2954 2955 1 2956 2966 0
		 2955 2956 1 2956 4183 1 2957 2834 0 2958 2835 0 2957 2958 1 2959 2836 0 2958 2959 1
		 2960 2826 0 2959 2960 1 2961 2827 0 2960 2961 1 2962 2829 0 2961 2962 1 2963 2830 0
		 2962 2963 1 2964 2831 0 2963 2964 1 2965 2832 0 2964 2965 1 2966 2833 0 2965 2966 1
		 2966 4184 1 2596 2967 0 2597 2968 0 2967 2968 0 2735 2969 0 2968 3492 0 2726 2970 0
		 2969 2970 0 2967 3493 0 2598 2971 0 2599 2972 0 2971 2972 0 2733 2973 0 2972 3502 0
		 2734 2974 0 2973 2974 0 2971 3503 0 2646 2987 0 2647 2986 0 2648 2995 0 2649 2994 0;
	setAttr ".ed[5976:6141]" 2650 2993 0 2651 2992 0 2652 2991 0 2653 2990 0 2654 2989 0
		 2655 2988 0 2975 2976 0 2976 2977 0 2977 2978 0 2978 2979 0 2979 3567 0 2980 2981 0
		 2981 2982 0 2982 2983 0 2983 2984 0 2984 2975 0 2975 2985 1 2978 2985 1 2979 2985 0
		 2980 2985 0 2981 2985 1 2984 2985 1 2986 3002 0 2987 3003 0 2986 2987 1 2988 3004 0
		 2987 2988 1 2989 3005 0 2988 2989 1 2990 2996 0 2989 2990 1 2991 2997 0 2990 4205 1
		 2992 2998 0 2991 2992 1 2993 2999 0 2992 2993 1 2994 3000 0 2993 2994 1 2995 3001 0
		 2994 2995 1 2995 2986 1 2996 3519 0 2997 3520 0 2996 4204 1 2998 3521 0 2997 2998 1
		 2999 3522 0 2998 2999 1 3000 3523 0 2999 3000 1 3001 3524 0 3000 3001 1 3002 3525 0
		 3001 3002 1 3003 3516 0 3002 3003 1 3004 3517 0 3003 3004 1 3005 3518 0 3004 3005 1
		 3005 2996 1 3006 3035 0 3007 3026 0 3006 3007 1 3008 3027 0 3007 3562 1 3009 3028 0
		 3008 3009 1 3010 3029 0 3009 3010 1 3011 3030 0 3010 3011 1 3012 3031 0 3011 3012 1
		 3013 3032 0 3012 3013 1 3014 3033 0 3013 3014 1 3015 3034 0 3014 3015 1 3015 3006 1
		 3016 3588 0 3017 3589 0 3016 3017 1 3018 3590 0 3017 3018 1 3019 3591 0 3018 3019 1
		 3020 3580 0 3019 3020 1 3021 3583 0 3020 3566 1 3022 3584 0 3021 3022 1 3023 3585 0
		 3022 3023 1 3024 3586 0 3023 3024 1 3025 3587 0 3024 3025 1 3025 3016 1 3026 4044 0
		 3027 4047 0 3026 3563 1 3028 4048 0 3027 3028 1 3029 4049 0 3028 3029 1 3030 4050 0
		 3029 3030 1 3031 4051 0 3030 3031 1 3032 4052 0 3031 3032 1 3033 4053 0 3032 3033 1
		 3034 4054 0 3033 3034 1 3035 4055 0 3034 3035 1 3035 3026 1 3036 3625 0 3037 3628 0
		 3036 3565 1 3038 3629 0 3037 3038 1 3039 3630 0 3038 3039 1 3040 3615 1 3039 3040 1
		 3041 3616 0 3040 3041 1 3042 3617 0 3041 3042 1 3043 3618 1 3042 3043 1 3044 3623 0
		 3043 3044 1 3045 3624 0 3044 3045 1 3045 3036 1 3046 3832 0 3047 3835 0 3046 3564 1
		 3048 3836 0 3047 3048 1 3049 3837 0 3048 3049 1 3050 3838 1 3049 3050 1 3051 3933 0
		 3050 3051 1 3052 3934 0 3051 3052 1 3053 4029 1 3052 3053 1 3054 4030 0 3053 3054 1
		 3055 4031 0 3054 3055 1 3055 3046 1 3056 2658 0 3057 2659 0 3056 3057 1 3058 2660 0;
	setAttr ".ed[6142:6307]" 3057 3058 1 3059 2661 0 3058 4209 1 3060 2662 0 3059 3060 1
		 3061 2663 0 3060 3061 1 3062 2664 0 3061 3062 1 3063 2665 0 3062 3063 1 3064 2656 0
		 3063 3064 1 3065 2657 0 3064 3065 1 3065 3056 1 3066 2668 0 3067 2669 0 3066 3067 1
		 3068 2670 0 3067 3068 1 3069 2671 0 3068 4211 1 3070 2672 0 3069 3070 1 3071 2673 0
		 3070 3071 1 3072 2674 0 3071 3072 1 3073 2675 0 3072 3073 1 3074 2666 0 3073 3074 1
		 3075 2667 0 3074 3075 1 3075 3066 1 3076 2655 0 3077 2654 0 3076 3077 1 3078 2653 0
		 3077 3078 1 3079 2652 0 3078 4207 1 3080 2651 0 3079 3080 1 3081 2650 0 3080 3081 1
		 3082 2649 0 3081 3082 1 3083 2648 0 3082 3083 1 3084 2647 0 3083 3084 1 3085 2646 0
		 3084 3085 1 3085 3076 1 3039 3086 1 3040 3087 1 3086 3087 0 3023 3088 1 3086 3631 0
		 3024 3089 1 3088 3089 0 3087 3614 0 3043 3090 1 3044 3091 1 3090 3091 0 3017 3092 1
		 3090 3619 0 3018 3093 1 3092 3093 0 3091 3622 0 3086 3094 0 3087 3095 0 3094 3095 0
		 3088 3096 0 3094 3612 0 3089 3097 0 3096 3097 0 3095 3613 0 3090 3098 0 3091 3099 0
		 3098 3099 0 3092 3100 0 3098 3620 0 3093 3101 0 3100 3101 0 3099 3621 0 3050 3102 1
		 3051 3103 1 3102 3103 0 3040 3104 1 3102 3839 0 3041 3105 1 3104 3105 0 3103 3932 0
		 3052 3106 1 3053 3107 1 3106 3107 0 3042 3108 1 3106 3935 0 3043 3109 1 3108 3109 0
		 3107 4028 0 3102 3110 0 3103 3111 0 3110 3111 0 3104 3112 0 3110 3840 0 3105 3113 0
		 3112 3113 0 3111 3931 0 3106 3114 0 3107 3115 0 3114 3115 0 3108 3116 0 3114 3936 0
		 3109 3117 0 3116 3117 0 3115 4027 0 3110 3118 0 3111 3119 0 3118 3119 0 3112 3120 0
		 3118 3841 0 3113 3121 0 3120 3121 0 3119 3930 0 3114 3122 0 3115 3123 0 3122 3123 0
		 3116 3124 0 3122 3937 0 3117 3125 0 3124 3125 0 3123 4026 0 3118 3126 1 3119 3127 1
		 3126 3127 1 3120 3128 1 3126 3842 1 3121 3129 1 3128 3129 1 3127 3929 1 3122 3130 1
		 3123 3131 1 3130 3131 1 3124 3132 1 3130 3938 1 3125 3133 1 3132 3133 1 3131 4025 1
		 3126 3134 1 3127 3135 1 3134 3135 0 3128 3136 1 3134 3843 0 3129 3137 1 3136 3137 0
		 3135 3928 0 3130 3138 1 3131 3139 1 3138 3139 0 3132 3140 1 3138 3939 0 3133 3141 1;
	setAttr ".ed[6308:6473]" 3140 3141 0 3139 4024 0 3134 3142 0 3135 3143 0 3142 3143 0
		 3136 3144 0 3142 3844 0 3137 3145 0 3144 3145 0 3143 3927 0 3138 3146 0 3139 3147 0
		 3146 3147 0 3140 3148 0 3146 3940 0 3141 3149 0 3148 3149 0 3147 4023 0 3142 3150 0
		 3143 3151 0 3150 3151 0 3144 3152 0 3150 3845 0 3145 3153 0 3152 3153 0 3151 3926 0
		 3146 3154 0 3147 3155 0 3154 3155 0 3148 3156 0 3154 3941 0 3149 3157 0 3156 3157 0
		 3155 4022 0 3151 3436 0 3153 3437 0 3158 3923 0 3150 3435 0 3160 3158 0 3152 3434 0
		 3160 3848 0 3161 3159 0 3154 3389 0 3156 3388 0 3162 3944 0 3157 3387 0 3163 3164 0
		 3155 3386 0 3165 4019 0 3162 3165 0 3158 3166 0 3159 3167 0 3166 3922 0 3160 3168 0
		 3168 3166 0 3161 3169 0 3168 3849 0 3169 3167 0 3162 3170 0 3163 3171 0 3170 3945 0
		 3164 3172 0 3171 3172 0 3165 3173 0 3173 4018 0 3170 3173 0 3166 3174 1 3167 3175 1
		 3174 3921 0 3168 3176 1 3176 3174 0 3169 3177 1 3176 3850 0 3177 3175 0 3170 3178 1
		 3171 3179 1 3178 3946 0 3172 3180 1 3179 3180 0 3173 3181 1 3181 4017 0 3178 3181 0
		 3174 3182 0 3175 3183 0 3182 3920 0 3176 3184 0 3184 3182 0 3177 3185 0 3184 3851 0
		 3185 3183 0 3178 3186 0 3179 3187 0 3186 3947 0 3180 3188 0 3187 3188 0 3181 3189 0
		 3189 4016 0 3186 3189 0 3182 3190 1 3183 3191 1 3190 3919 0 3184 3192 1 3192 3190 0
		 3185 3193 1 3192 3852 0 3193 3191 0 3186 3194 1 3187 3195 1 3194 3948 0 3188 3196 1
		 3195 3196 0 3189 3197 1 3197 4015 0 3194 3197 0 3190 3444 0 3191 3445 0 3198 3916 0
		 3192 3443 0 3200 3198 0 3193 3442 0 3200 3855 0 3201 3199 0 3194 3397 0 3195 3396 0
		 3202 3951 0 3196 3395 0 3203 3204 0 3197 3394 0 3205 4012 0 3202 3205 0 3198 3206 0
		 3199 3207 0 3206 3915 0 3200 3208 0 3208 3206 0 3201 3209 0 3208 3856 0 3209 3207 0
		 3202 3210 0 3203 3211 0 3210 3952 0 3204 3212 0 3211 3212 0 3205 3213 0 3213 4011 0
		 3210 3213 0 3206 3214 1 3207 3215 1 3214 3914 0 3208 3216 1 3216 3214 0 3209 3217 1
		 3216 3857 0 3217 3215 0 3210 3218 1 3211 3219 1 3218 3953 0 3212 3220 1 3219 3220 0
		 3213 3221 1 3221 4010 0 3218 3221 0 3214 3222 0 3215 3223 0 3222 3913 0 3216 3224 0;
	setAttr ".ed[6474:6639]" 3224 3222 0 3217 3225 0 3224 3858 0 3225 3223 0 3218 3226 0
		 3219 3227 0 3226 3954 0 3220 3228 0 3227 3228 0 3221 3229 0 3229 4009 0 3226 3229 0
		 3222 3230 1 3223 3231 1 3230 3912 0 3224 3232 1 3232 3230 0 3225 3233 1 3232 3859 0
		 3233 3231 0 3226 3234 1 3227 3235 1 3234 3955 0 3228 3236 1 3235 3236 0 3229 3237 1
		 3237 4008 0 3234 3237 0 3230 3238 0 3231 3239 0 3238 3911 0 3232 3240 0 3240 3238 0
		 3233 3241 0 3240 3860 0 3241 3239 0 3234 3242 0 3235 3243 0 3242 3956 0 3236 3244 0
		 3243 3244 0 3237 3245 0 3245 4007 0 3242 3245 0 3238 3452 0 3239 3453 0 3246 3908 0
		 3240 3451 0 3248 3246 1 3241 3450 0 3248 3863 0 3249 3247 0 3242 3405 0 3243 3404 0
		 3250 3959 0 3244 3403 0 3251 3252 0 3245 3402 0 3253 4004 0 3250 3253 1 3246 3254 0
		 3247 3255 0 3254 3907 0 3248 3256 0 3256 3254 0 3249 3257 0 3256 3864 0 3257 3255 0
		 3250 3258 0 3251 3259 0 3258 3960 0 3252 3260 0 3259 3260 0 3253 3261 0 3261 4003 0
		 3258 3261 0 3254 3262 1 3255 3263 1 3262 3906 0 3256 3264 1 3264 3262 0 3257 3265 1
		 3264 3865 0 3265 3263 0 3258 3266 1 3259 3267 1 3266 3961 0 3260 3268 1 3267 3268 0
		 3261 3269 1 3269 4002 0 3266 3269 0 3262 3270 0 3263 3271 0 3270 3905 0 3264 3272 0
		 3272 3270 0 3265 3273 0 3272 3866 0 3273 3271 0 3266 3274 0 3267 3275 0 3274 3962 0
		 3268 3276 0 3275 3276 0 3269 3277 0 3277 4001 0 3274 3277 0 3270 3278 1 3271 3279 1
		 3278 3904 0 3272 3280 1 3280 3278 0 3273 3281 1 3280 3867 0 3281 3279 0 3274 3282 1
		 3275 3283 1 3282 3963 0 3276 3284 1 3283 3284 0 3277 3285 1 3285 4000 0 3282 3285 0
		 3278 3460 0 3279 3461 0 3286 3901 0 3280 3459 0 3288 3286 1 3281 3458 0 3288 3870 0
		 3289 3287 0 3282 3413 0 3283 3412 0 3290 3966 0 3284 3411 0 3291 3292 0 3285 3410 0
		 3293 3997 0 3290 3293 1 3286 3294 0 3287 3295 0 3294 3900 0 3288 3296 0 3296 3294 0
		 3289 3297 0 3296 3871 0 3297 3295 0 3290 3298 0 3291 3299 0 3298 3967 0 3292 3300 0
		 3299 3300 0 3293 3301 0 3301 3996 0 3298 3301 0 3294 3302 1 3295 3303 1 3302 3899 0
		 3296 3304 1 3304 3302 0 3297 3305 1 3304 3872 0 3305 3303 0 3298 3306 1 3299 3307 1;
	setAttr ".ed[6640:6805]" 3306 3968 0 3300 3308 1 3307 3308 0 3301 3309 1 3309 3995 0
		 3306 3309 0 3302 3310 0 3303 3311 0 3310 3898 0 3304 3312 0 3312 3310 0 3305 3313 0
		 3312 3873 0 3313 3311 0 3306 3314 0 3307 3315 0 3314 3969 0 3308 3316 0 3315 3316 0
		 3309 3317 0 3317 3994 0 3314 3317 0 3310 3318 1 3311 3319 1 3318 3897 0 3312 3320 1
		 3320 3318 0 3313 3321 1 3320 3874 0 3321 3319 0 3314 3322 1 3315 3323 1 3322 3970 0
		 3316 3324 1 3323 3324 0 3317 3325 1 3325 3993 0 3322 3325 0 3318 3468 0 3319 3469 0
		 3326 3894 0 3320 3467 0 3328 3326 1 3321 3466 0 3328 3877 0 3329 3327 0 3322 3421 0
		 3323 3420 0 3330 3973 0 3324 3419 0 3331 3332 0 3325 3418 0 3333 3990 0 3330 3333 1
		 3326 3334 0 3327 3335 0 3334 3893 0 3328 3336 0 3336 3334 0 3329 3337 0 3336 3878 0
		 3337 3335 0 3330 3338 0 3331 3339 0 3338 3974 0 3332 3340 0 3339 3340 0 3333 3341 0
		 3341 3989 0 3338 3341 0 3334 3342 1 3335 3343 1 3342 3892 0 3336 3344 1 3344 3342 0
		 3337 3345 1 3344 3879 0 3345 3343 0 3338 3346 1 3339 3347 1 3346 3975 0 3340 3348 1
		 3347 3348 0 3341 3349 1 3349 3988 0 3346 3349 0 3342 3350 0 3343 3351 0 3350 3891 0
		 3344 3352 0 3352 3350 0 3345 3353 0 3352 3880 0 3353 3351 0 3346 3354 0 3347 3355 0
		 3354 3976 0 3348 3356 0 3355 3356 0 3349 3357 0 3357 3987 0 3354 3357 0 3350 3358 1
		 3351 3359 1 3358 3890 0 3352 3360 1 3360 3358 0 3353 3361 1 3360 3881 0 3361 3359 0
		 3354 3362 1 3355 3363 1 3362 3977 0 3356 3364 1 3363 3364 0 3357 3365 1 3365 3986 0
		 3362 3365 0 3358 3476 0 3359 3477 0 3366 3887 0 3360 3475 0 3368 3366 1 3361 3474 0
		 3368 3884 1 3369 3367 0 3362 3429 0 3363 3428 0 3370 3980 0 3364 3427 0 3371 3372 0
		 3365 3426 0 3373 3983 1 3370 3373 1 3366 3374 0 3367 3375 0 3374 3886 0 3368 3376 0
		 3376 3374 0 3369 3377 0 3376 3885 0 3377 3375 0 3370 3378 0 3371 3379 0 3378 3981 0
		 3372 3380 0 3379 3380 0 3373 3381 0 3381 3982 0 3378 3381 0 3382 3165 0 3383 3164 0
		 3382 4020 1 3384 3163 0 3383 3384 1 3385 3162 0 3384 3743 1 3385 3382 1 3386 3382 0
		 3387 3383 0 3386 4021 1 3388 3384 0 3387 3388 1 3389 3385 0 3388 3742 1 3389 3386 1;
	setAttr ".ed[6806:6971]" 3390 3205 0 3391 3204 0 3390 4013 1 3392 3203 0 3391 3392 1
		 3393 3202 0 3392 3750 1 3393 3390 1 3394 3390 0 3395 3391 0 3394 4014 1 3396 3392 0
		 3395 3396 1 3397 3393 0 3396 3749 1 3397 3394 1 3398 3253 0 3399 3252 0 3398 4005 1
		 3400 3251 0 3399 3400 1 3401 3250 0 3400 3758 1 3401 3398 1 3402 3398 0 3403 3399 0
		 3402 4006 1 3404 3400 0 3403 3404 1 3405 3401 0 3404 3757 1 3405 3402 1 3406 3293 0
		 3407 3292 0 3406 3998 1 3408 3291 0 3407 3408 1 3409 3290 0 3408 3765 1 3409 3406 1
		 3410 3406 0 3411 3407 0 3410 3999 1 3412 3408 0 3411 3412 1 3413 3409 0 3412 3764 1
		 3413 3410 1 3414 3333 0 3415 3332 0 3414 3991 1 3416 3331 0 3415 3416 1 3417 3330 0
		 3416 3772 1 3417 3414 1 3418 3414 0 3419 3415 0 3418 3992 1 3420 3416 0 3419 3420 1
		 3421 3417 0 3420 3771 1 3421 3418 1 3422 3373 0 3423 3372 0 3422 3984 1 3424 3371 0
		 3423 3424 1 3425 3370 0 3424 3779 1 3425 3422 1 3426 3422 0 3427 3423 0 3426 3985 1
		 3428 3424 0 3427 3428 1 3429 3425 0 3428 3778 1 3429 3426 1 3430 3161 0 3431 3160 0
		 3430 3647 1 3432 3158 0 3431 3432 1 3433 3159 0 3432 3924 1 3433 3430 1 3434 3430 0
		 3435 3431 0 3434 3646 1 3436 3432 0 3435 3436 1 3437 3433 0 3436 3925 1 3437 3434 1
		 3438 3201 0 3439 3200 0 3438 3654 1 3440 3198 0 3439 3440 1 3441 3199 0 3440 3917 1
		 3441 3438 1 3442 3438 0 3443 3439 0 3442 3653 1 3444 3440 0 3443 3444 1 3445 3441 0
		 3444 3918 1 3445 3442 1 3446 3249 0 3447 3248 0 3446 3662 1 3448 3246 0 3447 3448 1
		 3449 3247 0 3448 3909 1 3449 3446 1 3450 3446 0 3451 3447 0 3450 3661 1 3452 3448 0
		 3451 3452 1 3453 3449 0 3452 3910 1 3453 3450 1 3454 3289 0 3455 3288 0 3454 3669 1
		 3456 3286 0 3455 3456 1 3457 3287 0 3456 3902 1 3457 3454 1 3458 3454 0 3459 3455 0
		 3458 3668 1 3460 3456 0 3459 3460 1 3461 3457 0 3460 3903 1 3461 3458 1 3462 3329 0
		 3463 3328 0 3462 3676 1 3464 3326 0 3463 3464 1 3465 3327 0 3464 3895 1 3465 3462 1
		 3466 3462 0 3467 3463 0 3466 3675 1 3468 3464 0 3467 3468 1 3469 3465 0 3468 3896 1
		 3469 3466 1 3470 3369 0 3471 3368 0 3470 3683 1 3472 3366 0 3471 3472 1 3473 3367 0;
	setAttr ".ed[6972:7137]" 3472 3888 1 3473 3470 1 3474 3470 0 3475 3471 0 3474 3682 1
		 3476 3472 0 3475 3476 1 3477 3473 0 3476 3889 1 3477 3474 1 3478 2969 0 3479 2970 0
		 3480 2726 0 3479 3480 1 3481 2727 0 3480 3481 1 3482 2728 0 3481 3482 1 3483 2729 0
		 3482 3483 1 3484 2730 0 3483 4224 1 3485 2731 0 3484 3485 1 3486 2732 0 3485 3486 1
		 3487 2733 0 3486 3487 1 3488 2973 0 3487 3488 1 3489 2974 0 3490 2734 0 3489 3490 1
		 3491 2735 0 3490 3491 1 3491 3478 1 3492 3478 0 3493 3479 0 3494 3480 0 3493 3494 1
		 3495 3481 0 3494 3495 1 3496 3482 0 3495 3496 1 3497 3483 0 3496 3497 1 3498 3484 0
		 3497 4225 1 3499 3485 0 3498 3499 1 3500 3486 0 3499 3500 1 3501 3487 0 3500 3501 1
		 3502 3488 0 3501 3502 1 3503 3489 0 3504 3490 0 3503 3504 1 3505 3491 0 3504 3505 1
		 3505 3492 1 774 3479 0 3493 772 0 773 2970 0 2967 771 0 2968 770 0 768 2969 0 769 3478 0
		 775 3492 0 1782 3502 0 3488 1784 0 2973 1783 0 1781 2972 0 1780 2971 0 2974 1778 0
		 3489 1779 0 3503 1785 0 3506 3014 0 3507 3015 0 3506 3507 1 3508 3006 0 3507 3508 1
		 3509 3007 0 3508 3509 1 3510 3008 0 3509 4202 1 3511 3009 0 3510 3511 1 3512 3010 0
		 3511 3512 1 3513 3011 0 3512 3513 1 3514 3012 0 3513 3514 1 3515 3013 0 3514 3515 1
		 3515 3506 1 3516 3506 0 3517 3507 0 3516 3517 1 3518 3508 0 3517 3518 1 3519 3509 0
		 3518 3519 1 3520 3510 0 3519 4203 1 3521 3511 0 3520 3521 1 3522 3512 0 3521 3522 1
		 3523 3513 0 3522 3523 1 3524 3514 0 3523 3524 1 3525 3515 0 3524 3525 1 3525 3516 1
		 529 3084 0 2656 535 0 534 2665 0 3083 528 0 501 3064 0 2666 507 0 506 2675 0 3063 500 0
		 473 3074 0 2685 479 0 478 2684 0 3073 472 0 3007 3526 1 3008 3527 1 3526 3561 0 3509 3528 1
		 3528 3526 0 3510 3529 1 3529 3527 0 3528 4201 0 3526 3530 0 3527 3531 0 3530 3560 0
		 3528 3532 0 3532 3530 0 3529 3533 0 3533 3531 0 3532 4200 0 3530 4610 0 3531 4615 0
		 3534 3559 0 3532 4611 0 3536 3534 0 3533 4614 0 3537 3535 0 3536 4199 0 3534 3538 0
		 3535 3539 0 3538 3558 0 3536 3540 0 3540 3538 0 3537 3541 0 3541 3539 0 3540 4198 0
		 3538 4116 0 3539 4119 0 3542 3557 0 3540 4117 0;
	setAttr ".ed[7138:7303]" 3544 3542 0 3541 4118 0 3545 3543 0 3544 4186 0 3546 3543 0
		 3547 3539 0 3546 4060 1 3548 3535 0 3547 3548 1 3549 3531 0 3548 4608 1 3550 3527 0
		 3549 3550 1 3551 3008 1 3550 3551 1 3552 3027 1 3551 3552 1 3553 3047 1 3552 4046 1
		 3554 3037 1 3553 3834 1 3555 3021 1 3554 3627 1 3556 2980 0 3555 3582 1 3557 3546 0
		 3558 3547 0 3557 4061 1 3559 3548 0 3558 3559 1 3560 3549 0 3559 4609 1 3561 3550 0
		 3560 3561 1 3562 3551 1 3561 3562 1 3563 3552 1 3562 3563 1 3564 3553 1 3563 4045 1
		 3565 3554 1 3564 3833 1 3566 3555 1 3565 3626 1 3567 3556 0 3566 3581 1 3568 2979 0
		 3569 3567 1 3568 3569 1 3570 3556 1 3569 3570 1 3571 2980 0 3570 3571 1 3572 2981 0
		 3571 3572 1 3573 2982 0 3572 3573 1 3574 2983 0 3573 3574 1 3575 2984 0 3574 3575 1
		 3576 2975 0 3575 3576 1 3577 2976 0 3576 3577 1 3578 2977 0 3577 3578 1 3579 2978 0
		 3578 3579 1 3579 3568 1 3580 3568 0 3581 3569 1 3580 3581 1 3582 3570 1 3581 3582 1
		 3583 3571 0 3582 3583 1 3584 3572 0 3583 3584 1 3585 3573 0 3584 3585 1 3586 3574 0
		 3585 3586 1 3587 3575 0 3586 3587 1 3588 3576 0 3587 3588 1 3589 3577 0 3588 3589 1
		 3590 3578 0 3589 3590 1 3591 3579 0 3590 3591 1 3591 3580 1 3592 3096 0 3593 3097 0
		 3592 3593 1 3594 3089 0 3593 3594 1 3595 3024 1 3594 3595 1 3596 3025 0 3595 3596 1
		 3597 3016 0 3596 3597 1 3598 3017 1 3597 3598 1 3599 3092 0 3598 3599 1 3600 3100 0
		 3599 3600 1 3601 3101 0 3600 3601 1 3602 3093 0 3601 3602 1 3603 3018 0 3602 3603 1
		 3604 3019 0 3603 3604 1 3605 3020 0 3604 3605 1 3606 3566 1 3605 3606 1 3607 3555 1
		 3606 3607 1 3608 3021 0 3607 3608 1 3609 3022 0 3608 3609 1 3610 3023 0 3609 3610 1
		 3611 3088 0 3610 3611 1 3611 3592 1 3612 3592 0 3613 3593 0 3612 3613 1 3614 3594 0
		 3613 3614 1 3615 3595 1 3614 3615 1 3616 3596 0 3615 3616 1 3617 3597 0 3616 3617 1
		 3618 3598 1 3617 3618 1 3619 3599 0 3618 3619 1 3620 3600 0 3619 3620 1 3621 3601 0
		 3620 3621 1 3622 3602 0 3621 3622 1 3623 3603 0 3622 3623 1 3624 3604 0 3623 3624 1
		 3625 3605 0 3624 3625 1 3626 3606 1 3625 3626 1 3627 3607 1 3626 3627 1 3628 3608 0;
	setAttr ".ed[7304:7469]" 3627 3628 1 3629 3609 0 3628 3629 1 3630 3610 0 3629 3630 1
		 3631 3611 0 3630 3631 1 3631 3612 1 3632 3036 0 3633 3565 1 3632 3633 1 3634 3554 1
		 3633 3634 1 3635 3037 0 3634 3635 1 3636 3038 0 3635 3636 1 3637 3039 0 3636 3637 1
		 3638 3040 1 3637 3638 1 3639 3104 0 3638 3639 1 3640 3112 0 3639 3640 1 3641 3120 0
		 3640 3641 1 3642 3128 1 3641 3642 1 3643 3136 0 3642 3643 1 3644 3144 0 3643 3644 1
		 3645 3152 0 3644 3645 0 3646 3846 1 3645 3646 1 3647 3847 1 3646 3647 1 3648 3161 0
		 3647 3648 1 3649 3169 0 3648 3649 1 3650 3177 0 3649 3650 1 3651 3185 0 3650 3651 1
		 3652 3193 0 3651 3652 1 3653 3853 1 3652 3653 1 3654 3854 1 3653 3654 1 3655 3201 0
		 3654 3655 1 3656 3209 0 3655 3656 1 3657 3217 0 3656 3657 1 3658 3225 0 3657 3658 1
		 3659 3233 0 3658 3659 1 3660 3241 0 3659 3660 0 3661 3861 1 3660 3661 1 3662 3862 1
		 3661 3662 1 3663 3249 0 3662 3663 1 3664 3257 0 3663 3664 1 3665 3265 0 3664 3665 1
		 3666 3273 0 3665 3666 1 3667 3281 0 3666 3667 1 3668 3868 1 3667 3668 1 3669 3869 1
		 3668 3669 1 3670 3289 0 3669 3670 1 3671 3297 0 3670 3671 1 3672 3305 0 3671 3672 1
		 3673 3313 0 3672 3673 1 3674 3321 0 3673 3674 1 3675 3875 1 3674 3675 1 3676 3876 1
		 3675 3676 1 3677 3329 0 3676 3677 1 3678 3337 0 3677 3678 1 3679 3345 0 3678 3679 1
		 3680 3353 0 3679 3680 1 3681 3361 0 3680 3681 1 3682 3882 1 3681 3682 1 3683 3883 1
		 3682 3683 1 3684 3369 1 3683 3684 1 3685 3377 0 3684 3685 1 3686 3375 0 3685 3686 1
		 3687 3367 0 3686 3687 1 3688 3473 1 3687 3688 1 3689 3477 1 3688 3689 1 3690 3359 0
		 3689 3690 1 3691 3351 0 3690 3691 1 3692 3343 0 3691 3692 1 3693 3335 0 3692 3693 1
		 3694 3327 0 3693 3694 1 3695 3465 1 3694 3695 1 3696 3469 1 3695 3696 1 3697 3319 0
		 3696 3697 1 3698 3311 0 3697 3698 1 3699 3303 0 3698 3699 1 3700 3295 0 3699 3700 1
		 3701 3287 0 3700 3701 1 3702 3457 1 3701 3702 1 3703 3461 1 3702 3703 1 3704 3279 0
		 3703 3704 1 3705 3271 0 3704 3705 1 3706 3263 0 3705 3706 1 3707 3255 0 3706 3707 1
		 3708 3247 0 3707 3708 1 3709 3449 1 3708 3709 1 3710 3453 1 3709 3710 1 3711 3239 0;
	setAttr ".ed[7470:7635]" 3710 3711 1 3712 3231 0 3711 3712 0 3713 3223 0 3712 3713 1
		 3714 3215 0 3713 3714 1 3715 3207 0 3714 3715 1 3716 3199 0 3715 3716 1 3717 3441 1
		 3716 3717 1 3718 3445 1 3717 3718 1 3719 3191 0 3718 3719 1 3720 3183 0 3719 3720 1
		 3721 3175 0 3720 3721 1 3722 3167 0 3721 3722 1 3723 3159 0 3722 3723 1 3724 3433 1
		 3723 3724 1 3725 3437 1 3724 3725 1 3726 3153 0 3725 3726 1 3727 3145 0 3726 3727 0
		 3728 3137 0 3727 3728 1 3729 3129 1 3728 3729 1 3730 3121 0 3729 3730 1 3731 3113 0
		 3730 3731 1 3732 3105 0 3731 3732 1 3733 3041 0 3732 3733 1 3734 3042 0 3733 3734 1
		 3735 3108 0 3734 3735 1 3736 3116 0 3735 3736 1 3737 3124 0 3736 3737 1 3738 3132 1
		 3737 3738 1 3739 3140 0 3738 3739 1 3740 3148 0 3739 3740 1 3741 3156 0 3740 3741 0
		 3742 3942 1 3741 3742 1 3743 3943 1 3742 3743 1 3744 3163 0 3743 3744 1 3745 3171 0
		 3744 3745 1 3746 3179 0 3745 3746 1 3747 3187 0 3746 3747 1 3748 3195 0 3747 3748 1
		 3749 3949 1 3748 3749 1 3750 3950 1 3749 3750 1 3751 3203 0 3750 3751 1 3752 3211 0
		 3751 3752 1 3753 3219 0 3752 3753 1 3754 3227 0 3753 3754 1 3755 3235 0 3754 3755 1
		 3756 3243 0 3755 3756 0 3757 3957 1 3756 3757 1 3758 3958 1 3757 3758 1 3759 3251 0
		 3758 3759 1 3760 3259 0 3759 3760 1 3761 3267 0 3760 3761 1 3762 3275 0 3761 3762 1
		 3763 3283 0 3762 3763 1 3764 3964 1 3763 3764 1 3765 3965 1 3764 3765 1 3766 3291 0
		 3765 3766 1 3767 3299 0 3766 3767 1 3768 3307 0 3767 3768 1 3769 3315 0 3768 3769 1
		 3770 3323 0 3769 3770 1 3771 3971 1 3770 3771 1 3772 3972 1 3771 3772 1 3773 3331 0
		 3772 3773 1 3774 3339 0 3773 3774 1 3775 3347 0 3774 3775 1 3776 3355 0 3775 3776 1
		 3777 3363 0 3776 3777 1 3778 3978 1 3777 3778 1 3779 3979 1 3778 3779 1 3780 3371 0
		 3779 3780 1 3781 3379 0 3780 3781 1 3782 3380 0 3781 3782 1 3783 3372 1 3782 3783 1
		 3784 3423 1 3783 3784 1 3785 3427 1 3784 3785 1 3786 3364 0 3785 3786 1 3787 3356 0
		 3786 3787 1 3788 3348 0 3787 3788 1 3789 3340 0 3788 3789 1 3790 3332 0 3789 3790 1
		 3791 3415 1 3790 3791 1 3792 3419 1 3791 3792 1 3793 3324 0 3792 3793 1 3794 3316 0;
	setAttr ".ed[7636:7801]" 3793 3794 1 3795 3308 0 3794 3795 1 3796 3300 0 3795 3796 1
		 3797 3292 0 3796 3797 1 3798 3407 1 3797 3798 1 3799 3411 1 3798 3799 1 3800 3284 0
		 3799 3800 1 3801 3276 0 3800 3801 1 3802 3268 0 3801 3802 1 3803 3260 0 3802 3803 1
		 3804 3252 0 3803 3804 1 3805 3399 1 3804 3805 1 3806 3403 1 3805 3806 1 3807 3244 0
		 3806 3807 1 3808 3236 0 3807 3808 0 3809 3228 0 3808 3809 1 3810 3220 0 3809 3810 1
		 3811 3212 0 3810 3811 1 3812 3204 0 3811 3812 1 3813 3391 1 3812 3813 1 3814 3395 1
		 3813 3814 1 3815 3196 0 3814 3815 1 3816 3188 0 3815 3816 1 3817 3180 0 3816 3817 1
		 3818 3172 0 3817 3818 1 3819 3164 0 3818 3819 1 3820 3383 1 3819 3820 1 3821 3387 1
		 3820 3821 1 3822 3157 0 3821 3822 1 3823 3149 0 3822 3823 0 3824 3141 0 3823 3824 1
		 3825 3133 1 3824 3825 1 3826 3125 0 3825 3826 1 3827 3117 0 3826 3827 1 3828 3109 0
		 3827 3828 1 3829 3043 1 3828 3829 1 3830 3044 0 3829 3830 1 3831 3045 0 3830 3831 1
		 3831 3632 1 3832 3632 0 3833 3633 1 3832 3833 1 3834 3634 1 3833 3834 1 3835 3635 0
		 3834 3835 1 3836 3636 0 3835 3836 1 3837 3637 0 3836 3837 1 3838 3638 1 3837 3838 1
		 3839 3639 0 3838 3839 1 3840 3640 0 3839 3840 1 3841 3641 0 3840 3841 1 3842 3642 1
		 3841 3842 1 3843 3643 0 3842 3843 1 3844 3644 0 3843 3844 1 3845 3645 0 3844 3845 0
		 3846 3435 1 3845 3846 1 3847 3431 1 3846 3847 1 3848 3648 0 3847 3848 1 3849 3649 0
		 3848 3849 1 3850 3650 0 3849 3850 1 3851 3651 0 3850 3851 1 3852 3652 0 3851 3852 1
		 3853 3443 1 3852 3853 1 3854 3439 1 3853 3854 1 3855 3655 0 3854 3855 1 3856 3656 0
		 3855 3856 1 3857 3657 0 3856 3857 1 3858 3658 0 3857 3858 1 3859 3659 0 3858 3859 1
		 3860 3660 0 3859 3860 0 3861 3451 1 3860 3861 1 3862 3447 1 3861 3862 1 3863 3663 0
		 3862 3863 1 3864 3664 0 3863 3864 1 3865 3665 0 3864 3865 1 3866 3666 0 3865 3866 1
		 3867 3667 0 3866 3867 1 3868 3459 1 3867 3868 1 3869 3455 1 3868 3869 1 3870 3670 0
		 3869 3870 1 3871 3671 0 3870 3871 1 3872 3672 0 3871 3872 1 3873 3673 0 3872 3873 1
		 3874 3674 0 3873 3874 1 3875 3467 1 3874 3875 1 3876 3463 1 3875 3876 1 3877 3677 0;
	setAttr ".ed[7802:7967]" 3876 3877 1 3878 3678 0 3877 3878 1 3879 3679 0 3878 3879 1
		 3880 3680 0 3879 3880 1 3881 3681 0 3880 3881 1 3882 3475 1 3881 3882 1 3883 3471 1
		 3882 3883 1 3884 3684 1 3883 3884 1 3885 3685 0 3884 3885 1 3886 3686 0 3885 3886 1
		 3887 3687 0 3886 3887 1 3888 3688 1 3887 3888 1 3889 3689 1 3888 3889 1 3890 3690 0
		 3889 3890 1 3891 3691 0 3890 3891 1 3892 3692 0 3891 3892 1 3893 3693 0 3892 3893 1
		 3894 3694 0 3893 3894 1 3895 3695 1 3894 3895 1 3896 3696 1 3895 3896 1 3897 3697 0
		 3896 3897 1 3898 3698 0 3897 3898 1 3899 3699 0 3898 3899 1 3900 3700 0 3899 3900 1
		 3901 3701 0 3900 3901 1 3902 3702 1 3901 3902 1 3903 3703 1 3902 3903 1 3904 3704 0
		 3903 3904 1 3905 3705 0 3904 3905 1 3906 3706 0 3905 3906 1 3907 3707 0 3906 3907 1
		 3908 3708 0 3907 3908 1 3909 3709 1 3908 3909 1 3910 3710 1 3909 3910 1 3911 3711 0
		 3910 3911 1 3912 3712 0 3911 3912 0 3913 3713 0 3912 3913 1 3914 3714 0 3913 3914 1
		 3915 3715 0 3914 3915 1 3916 3716 0 3915 3916 1 3917 3717 1 3916 3917 1 3918 3718 1
		 3917 3918 1 3919 3719 0 3918 3919 1 3920 3720 0 3919 3920 1 3921 3721 0 3920 3921 1
		 3922 3722 0 3921 3922 1 3923 3723 0 3922 3923 1 3924 3724 1 3923 3924 1 3925 3725 1
		 3924 3925 1 3926 3726 0 3925 3926 1 3927 3727 0 3926 3927 0 3928 3728 0 3927 3928 1
		 3929 3729 1 3928 3929 1 3930 3730 0 3929 3930 1 3931 3731 0 3930 3931 1 3932 3732 0
		 3931 3932 1 3933 3733 0 3932 3933 1 3934 3734 0 3933 3934 1 3935 3735 0 3934 3935 1
		 3936 3736 0 3935 3936 1 3937 3737 0 3936 3937 1 3938 3738 1 3937 3938 1 3939 3739 0
		 3938 3939 1 3940 3740 0 3939 3940 1 3941 3741 0 3940 3941 0 3942 3389 1 3941 3942 1
		 3943 3385 1 3942 3943 1 3944 3744 0 3943 3944 1 3945 3745 0 3944 3945 1 3946 3746 0
		 3945 3946 1 3947 3747 0 3946 3947 1 3948 3748 0 3947 3948 1 3949 3397 1 3948 3949 1
		 3950 3393 1 3949 3950 1 3951 3751 0 3950 3951 1 3952 3752 0 3951 3952 1 3953 3753 0
		 3952 3953 1 3954 3754 0 3953 3954 1 3955 3755 0 3954 3955 1 3956 3756 0 3955 3956 0
		 3957 3405 1 3956 3957 1 3958 3401 1 3957 3958 1 3959 3759 0 3958 3959 1 3960 3760 0;
	setAttr ".ed[7968:8133]" 3959 3960 1 3961 3761 0 3960 3961 1 3962 3762 0 3961 3962 1
		 3963 3763 0 3962 3963 1 3964 3413 1 3963 3964 1 3965 3409 1 3964 3965 1 3966 3766 0
		 3965 3966 1 3967 3767 0 3966 3967 1 3968 3768 0 3967 3968 1 3969 3769 0 3968 3969 1
		 3970 3770 0 3969 3970 1 3971 3421 1 3970 3971 1 3972 3417 1 3971 3972 1 3973 3773 0
		 3972 3973 1 3974 3774 0 3973 3974 1 3975 3775 0 3974 3975 1 3976 3776 0 3975 3976 1
		 3977 3777 0 3976 3977 1 3978 3429 1 3977 3978 1 3979 3425 1 3978 3979 1 3980 3780 0
		 3979 3980 1 3981 3781 0 3980 3981 1 3982 3782 0 3981 3982 1 3983 3783 1 3982 3983 1
		 3984 3784 1 3983 3984 1 3985 3785 1 3984 3985 1 3986 3786 0 3985 3986 1 3987 3787 0
		 3986 3987 1 3988 3788 0 3987 3988 1 3989 3789 0 3988 3989 1 3990 3790 0 3989 3990 1
		 3991 3791 1 3990 3991 1 3992 3792 1 3991 3992 1 3993 3793 0 3992 3993 1 3994 3794 0
		 3993 3994 1 3995 3795 0 3994 3995 1 3996 3796 0 3995 3996 1 3997 3797 0 3996 3997 1
		 3998 3798 1 3997 3998 1 3999 3799 1 3998 3999 1 4000 3800 0 3999 4000 1 4001 3801 0
		 4000 4001 1 4002 3802 0 4001 4002 1 4003 3803 0 4002 4003 1 4004 3804 0 4003 4004 1
		 4005 3805 1 4004 4005 1 4006 3806 1 4005 4006 1 4007 3807 0 4006 4007 1 4008 3808 0
		 4007 4008 0 4009 3809 0 4008 4009 1 4010 3810 0 4009 4010 1 4011 3811 0 4010 4011 1
		 4012 3812 0 4011 4012 1 4013 3813 1 4012 4013 1 4014 3814 1 4013 4014 1 4015 3815 0
		 4014 4015 1 4016 3816 0 4015 4016 1 4017 3817 0 4016 4017 1 4018 3818 0 4017 4018 1
		 4019 3819 0 4018 4019 1 4020 3820 1 4019 4020 1 4021 3821 1 4020 4021 1 4022 3822 0
		 4021 4022 1 4023 3823 0 4022 4023 0 4024 3824 0 4023 4024 1 4025 3825 1 4024 4025 1
		 4026 3826 0 4025 4026 1 4027 3827 0 4026 4027 1 4028 3828 0 4027 4028 1 4029 3829 1
		 4028 4029 1 4030 3830 0 4029 4030 1 4031 3831 0 4030 4031 1 4031 3832 1 4032 3046 0
		 4033 3564 1 4032 4033 1 4034 3553 1 4033 4034 1 4035 3047 0 4034 4035 1 4036 3048 0
		 4035 4036 1 4037 3049 0 4036 4037 1 4038 3050 0 4037 4038 1 4039 3051 0 4038 4039 1
		 4040 3052 0 4039 4040 1 4041 3053 0 4040 4041 1 4042 3054 0 4041 4042 1 4043 3055 0;
	setAttr ".ed[8134:8299]" 4042 4043 1 4043 4032 1 4044 4032 0 4045 4033 1 4044 4045 1
		 4046 4034 1 4045 4046 1 4047 4035 0 4046 4047 1 4048 4036 0 4047 4048 1 4049 4037 0
		 4048 4049 1 4050 4038 0 4049 4050 1 4051 4039 0 4050 4051 1 4052 4040 0 4051 4052 1
		 4053 4041 0 4052 4053 1 4054 4042 0 4053 4054 1 4055 4043 0 4054 4055 1 4055 4044 1
		 4056 3542 0 4057 3544 0 4056 4057 1 4058 3545 0 4057 4187 1 4059 3543 0 4058 4059 1
		 4060 4066 1 4059 4060 1 4061 4067 1 4060 4061 1 4061 4056 1 4062 4056 0 4063 4057 0
		 4062 4063 1 4064 4058 0 4063 4188 1 4065 4059 0 4064 4065 1 4066 4072 1 4065 4066 1
		 4067 4073 1 4066 4067 1 4067 4062 1 4068 4062 0 4069 4063 0 4068 4069 1 4070 4064 0
		 4069 4189 1 4071 4065 0 4070 4071 1 4072 4078 1 4071 4072 1 4073 4079 1 4072 4073 1
		 4073 4068 1 4074 4068 0 4075 4069 0 4074 4075 1 4076 4070 0 4075 4190 1 4077 4071 0
		 4076 4077 1 4078 4084 1 4077 4078 1 4079 4085 1 4078 4079 1 4079 4074 1 4080 4074 0
		 4081 4075 0 4080 4081 1 4082 4076 0 4081 4191 1 4083 4077 0 4082 4083 1 4084 4090 1
		 4083 4084 1 4085 4091 1 4084 4085 1 4085 4080 1 4086 4080 0 4087 4081 0 4086 4087 1
		 4088 4082 0 4087 4192 1 4089 4083 0 4088 4089 1 4090 4096 1 4089 4090 1 4091 4097 1
		 4090 4091 1 4091 4086 1 4092 4086 0 4093 4087 0 4092 4093 1 4094 4088 0 4093 4193 1
		 4095 4089 0 4094 4095 1 4096 4102 1 4095 4096 1 4097 4103 1 4096 4097 1 4097 4092 1
		 4098 4092 0 4099 4093 0 4098 4099 1 4100 4094 0 4099 4194 1 4101 4095 0 4100 4101 1
		 4102 4108 1 4101 4102 1 4103 4109 1 4102 4103 1 4103 4098 1 4104 4098 0 4105 4099 0
		 4104 4105 1 4106 4100 0 4105 4195 1 4107 4101 0 4106 4107 1 4108 4114 1 4107 4108 1
		 4109 4115 1 4108 4109 1 4109 4104 1 4110 4104 0 4111 4105 0 4110 4111 1 4112 4106 0
		 4111 4196 1 4113 4107 0 4112 4113 1 4114 4120 1 4113 4114 1 4115 4121 1 4114 4115 1
		 4115 4110 1 4116 4110 0 4117 4111 0 4116 4117 1 4118 4112 0 4117 4197 1 4119 4113 0
		 4118 4119 1 4120 3547 1 4119 4120 1 4121 3558 1 4120 4121 1 4121 4116 1 4122 3545 0
		 4123 4058 1 4122 4123 1 4124 4064 1 4123 4124 1 4125 4070 1 4124 4125 1 4126 4076 1;
	setAttr ".ed[8300:8465]" 4125 4126 1 4127 4082 1 4126 4127 1 4128 4088 1 4127 4128 1
		 4129 4094 1 4128 4129 1 4130 4100 1 4129 4130 1 4131 4106 1 4130 4131 1 4132 4112 1
		 4131 4132 1 4133 4118 1 4132 4133 1 4134 3541 0 4133 4134 1 4135 3537 0 4134 4135 0
		 4136 3533 0 4135 4605 1 4137 3529 0 4136 4137 1 4138 3510 1 4137 4138 1 4139 3520 1
		 4138 4139 1 4140 2997 1 4139 4140 1 4141 2991 1 4140 4141 1 4142 4206 1 4141 4142 1
		 4143 3079 1 4142 4143 1 4144 2661 1 4143 4144 1 4145 3059 1 4144 4145 1 4146 2671 1
		 4145 4146 1 4147 3069 1 4146 4147 1 4148 2680 1 4147 4148 1 4149 2689 1 4148 4149 1
		 4150 2697 1 4149 4150 1 4151 2707 1 4150 4151 1 4152 4216 1 4151 4152 1 4153 2768 1
		 4152 4153 1 4154 2757 1 4153 4154 1 4155 4219 1 4154 4155 1 4156 4220 1 4155 4156 1
		 4157 4221 0 4156 4157 1 4158 2720 1 4157 4158 1 4159 2730 1 4158 4159 1 4160 3484 1
		 4159 4160 1 4161 3498 1 4160 4161 1 4162 4226 1 4161 4162 1 4163 2740 1 4162 4163 1
		 4164 4228 1 4163 4164 1 4165 2750 1 4164 4165 1 4166 4230 1 4165 4166 1 4167 4231 1
		 4166 4167 1 4168 4232 1 4167 4168 1 4169 4233 1 4168 4169 1 4170 4234 1 4169 4170 1
		 4171 4235 0 4170 4171 1 4172 4236 1 4171 4172 1 4173 4237 1 4172 4173 1 4174 4238 1
		 4173 4174 1 4175 4239 1 4174 4175 1 4176 4240 1 4175 4176 1 4177 4241 1 4176 4177 1
		 4178 4242 1 4177 4178 1 4179 4243 1 4178 4179 1 4180 4244 1 4179 4180 1 4181 4245 1
		 4180 4181 1 4182 4246 1 4181 4182 1 4183 4247 1 4182 4183 1 4184 4248 1 4183 4184 1
		 4185 4249 0 4184 4185 1 4186 4122 0 4187 4123 1 4186 4187 1 4188 4124 1 4187 4188 1
		 4189 4125 1 4188 4189 1 4190 4126 1 4189 4190 1 4191 4127 1 4190 4191 1 4192 4128 1
		 4191 4192 1 4193 4129 1 4192 4193 1 4194 4130 1 4193 4194 1 4195 4131 1 4194 4195 1
		 4196 4132 1 4195 4196 1 4197 4133 1 4196 4197 1 4198 4134 0 4197 4198 1 4199 4135 0
		 4198 4199 0 4200 4136 0 4199 4604 1 4201 4137 0 4200 4201 1 4202 4138 1 4201 4202 1
		 4203 4139 1 4202 4203 1 4204 4140 1 4203 4204 1 4205 4141 1 4204 4205 1 4206 2653 1
		 4205 4206 1 4207 4143 1 4206 4207 1 4208 4144 1 4207 4208 1 4209 4145 1 4208 4209 1;
	setAttr ".ed[8466:8631]" 4210 4146 1 4209 4210 1 4211 4147 1 4210 4211 1 4212 4148 1
		 4211 4212 1 4213 4149 1 4212 4213 1 4214 4150 1 4213 4214 1 4215 4151 1 4214 4215 1
		 4216 2643 1 4215 4216 1 4217 4153 1 4216 4217 1 4218 4154 1 4217 4218 1 4219 2633 1
		 4218 4219 1 4220 2623 1 4219 4220 1 4221 2613 0 4220 4221 1 4222 4158 1 4221 4222 1
		 4223 4159 1 4222 4223 1 4224 4160 1 4223 4224 1 4225 4161 1 4224 4225 1 4226 2603 1
		 4225 4226 1 4227 4163 1 4226 4227 1 4228 2593 1 4227 4228 1 4229 4165 1 4228 4229 1
		 4230 2583 1 4229 4230 1 4231 2802 1 4230 4231 1 4232 2792 1 4231 4232 1 4233 2812 1
		 4232 4233 1 4234 2783 1 4233 4234 1 4235 2823 0 4234 4235 1 4236 2843 1 4235 4236 1
		 4237 2855 1 4236 4237 1 4238 2864 1 4237 4238 1 4239 2874 1 4238 4239 1 4240 2885 1
		 4239 4240 1 4241 2895 1 4240 4241 1 4242 2905 1 4241 4242 1 4243 2915 1 4242 4243 1
		 4244 2925 1 4243 4244 1 4245 2935 1 4244 4245 1 4246 2945 1 4245 4246 1 4247 2947 1
		 4246 4247 1 4248 2957 1 4247 4248 1 4249 2834 0 4248 4249 1 3546 4122 0 4186 3557 0
		 3555 4250 1 3021 4251 1 4250 4251 0 3582 4252 1 4250 4252 0 3583 4253 1 4252 4253 0
		 4251 4253 0 3553 4254 1 3047 4255 1 4254 4255 0 3834 4256 1 4254 4256 0 3835 4257 1
		 4256 4257 0 4255 4257 0 3564 4258 1 3833 4259 1 4258 4259 0 3046 4260 1 4260 4258 0
		 3832 4261 1 4260 4261 0 4261 4259 0 3566 4262 1 3581 4263 1 4262 4263 0 3020 4264 1
		 4264 4262 0 3580 4265 1 4264 4265 0 4265 4263 0 3569 4266 1 3567 4267 1 4266 4267 0
		 3568 4268 1 4268 4266 0 2979 4269 1 4268 4269 0 4269 4267 0 3570 4270 1 3571 4271 1
		 4270 4271 0 3556 4272 1 4270 4272 0 2980 4273 1 4272 4273 0 4271 4273 0 3626 4274 1
		 3606 4275 1 4274 4275 0 3625 4276 1 4276 4274 0 3605 4277 1 4276 4277 0 4277 4275 0
		 3627 4278 1 3628 4279 1 4278 4279 0 3607 4280 1 4278 4280 0 3608 4281 1 4280 4281 0
		 4279 4281 0 3633 4282 1 3565 4283 1 4282 4283 0 3632 4284 1 4284 4282 0 3036 4285 1
		 4284 4285 0 4285 4283 0 3634 4286 1 3635 4287 1 4286 4287 0 3554 4288 1 4286 4288 0
		 3037 4289 1 4288 4289 0 4287 4289 0 4045 4290 1 4033 4291 1 4290 4291 0 4044 4292 1;
	setAttr ".ed[8632:8797]" 4292 4290 0 4032 4293 1 4292 4293 0 4293 4291 0 4046 4294 1
		 4047 4295 1 4294 4295 0 4034 4296 1 4294 4296 0 4035 4297 1 4296 4297 0 4295 4297 0
		 4065 4298 1 4066 4299 1 4298 4299 0 4059 4300 1 4298 4300 0 4060 4301 1 4300 4301 0
		 4301 4299 0 4067 4302 1 4062 4303 1 4302 4303 0 4061 4304 1 4304 4302 0 4056 4305 1
		 4304 4305 0 4303 4305 0 4077 4306 1 4078 4307 1 4306 4307 0 4071 4308 1 4306 4308 0
		 4072 4309 1 4308 4309 0 4309 4307 0 4079 4310 1 4074 4311 1 4310 4311 0 4073 4312 1
		 4312 4310 0 4068 4313 1 4312 4313 0 4311 4313 0 4089 4314 1 4090 4315 1 4314 4315 0
		 4083 4316 1 4314 4316 0 4084 4317 1 4316 4317 0 4317 4315 0 4091 4318 1 4086 4319 1
		 4318 4319 0 4085 4320 1 4320 4318 0 4080 4321 1 4320 4321 0 4319 4321 0 4101 4322 1
		 4102 4323 1 4322 4323 0 4095 4324 1 4322 4324 0 4096 4325 1 4324 4325 0 4325 4323 0
		 4103 4326 1 4098 4327 1 4326 4327 0 4097 4328 1 4328 4326 0 4092 4329 1 4328 4329 0
		 4327 4329 0 4113 4330 1 4114 4331 1 4330 4331 0 4107 4332 1 4330 4332 0 4108 4333 1
		 4332 4333 0 4333 4331 0 4115 4334 1 4110 4335 1 4334 4335 0 4109 4336 1 4336 4334 0
		 4104 4337 1 4336 4337 0 4335 4337 0 4250 4338 0 4251 4339 0 4338 4339 1 4252 4340 0
		 4338 4340 1 4253 4341 0 4340 4341 1 4339 4341 1 4254 4342 0 4255 4343 0 4342 4343 1
		 4256 4344 0 4342 4344 0 4257 4345 0 4344 4345 1 4343 4345 0 4258 4346 0 4259 4347 0
		 4346 4347 0 4260 4348 0 4348 4346 1 4261 4349 0 4348 4349 0 4349 4347 1 4262 4350 0
		 4263 4351 0 4350 4351 1 4264 4352 0 4352 4350 1 4265 4353 0 4352 4353 1 4353 4351 1
		 4266 4354 0 4267 4355 0 4354 4355 1 4268 4356 0 4356 4354 1 4269 4357 0 4356 4357 1
		 4357 4355 1 4270 4358 0 4271 4359 0 4358 4359 1 4272 4360 0 4358 4360 1 4273 4361 0
		 4360 4361 1 4359 4361 1 4274 4362 0 4275 4363 0 4362 4363 0 4276 4364 0 4364 4362 1
		 4277 4365 0 4364 4365 0 4365 4363 1 4278 4366 0 4279 4367 0 4366 4367 1 4280 4368 0
		 4366 4368 0 4281 4369 0 4368 4369 1 4367 4369 0 4282 4370 0 4283 4371 0 4370 4371 0
		 4284 4372 0 4372 4370 1 4285 4373 0 4372 4373 0 4373 4371 1 4286 4374 0 4287 4375 0;
	setAttr ".ed[8798:8963]" 4374 4375 1 4288 4376 0 4374 4376 0 4289 4377 0 4376 4377 1
		 4375 4377 0 4290 4378 0 4291 4379 0 4378 4379 0 4292 4380 0 4380 4378 1 4293 4381 0
		 4380 4381 0 4381 4379 1 4294 4382 0 4295 4383 0 4382 4383 1 4296 4384 0 4382 4384 0
		 4297 4385 0 4384 4385 1 4383 4385 0 4298 4386 0 4299 4387 0 4386 4387 1 4300 4388 0
		 4386 4388 1 4301 4389 0 4388 4389 1 4389 4387 1 4302 4390 0 4303 4391 0 4390 4391 1
		 4304 4392 0 4392 4390 1 4305 4393 0 4392 4393 1 4391 4393 1 4306 4394 0 4307 4395 0
		 4394 4395 1 4308 4396 0 4394 4396 1 4309 4397 0 4396 4397 1 4397 4395 1 4310 4398 0
		 4311 4399 0 4398 4399 1 4312 4400 0 4400 4398 1 4313 4401 0 4400 4401 1 4399 4401 1
		 4314 4402 0 4315 4403 0 4402 4403 1 4316 4404 0 4402 4404 1 4317 4405 0 4404 4405 1
		 4405 4403 1 4318 4406 0 4319 4407 0 4406 4407 1 4320 4408 0 4408 4406 1 4321 4409 0
		 4408 4409 1 4407 4409 1 4322 4410 0 4323 4411 0 4410 4411 1 4324 4412 0 4410 4412 0
		 4325 4413 0 4412 4413 1 4413 4411 0 4326 4414 0 4327 4415 0 4414 4415 1 4328 4416 0
		 4416 4414 0 4329 4417 0 4416 4417 1 4415 4417 0 4330 4418 0 4331 4419 0 4418 4419 1
		 4332 4420 0 4418 4420 0 4333 4421 0 4420 4421 1 4421 4419 0 4334 4422 0 4335 4423 0
		 4422 4423 1 4336 4424 0 4424 4422 0 4337 4425 0 4424 4425 1 4423 4425 0 4338 4523 0
		 4339 4524 0 4426 4427 0 4340 4522 0 4426 4428 0 4341 4525 0 4428 4429 0 4427 4429 0
		 4342 4535 0 4343 4536 0 4430 4431 0 4344 4534 0 4430 4432 0 4345 4537 0 4432 4433 0
		 4431 4433 0 4346 4556 0 4347 4557 0 4434 4435 0 4348 4555 0 4436 4434 0 4349 4554 0
		 4436 4437 0 4437 4435 0 4350 4540 0 4351 4541 0 4438 4439 0 4352 4539 0 4440 4438 0
		 4353 4538 0 4440 4441 0 4441 4439 0 4354 4516 0 4355 4517 0 4442 4443 0 4356 4515 0
		 4444 4442 0 4357 4514 0 4444 4445 0 4445 4443 0 4358 4519 0 4359 4520 0 4446 4447 0
		 4360 4518 0 4446 4448 0 4361 4521 0 4448 4449 0 4447 4449 0 4362 4548 0 4363 4549 0
		 4450 4451 0 4364 4547 0 4452 4450 0 4365 4546 0 4452 4453 0 4453 4451 0 4366 4527 0
		 4367 4528 0 4454 4455 0 4368 4526 0 4454 4456 0 4369 4529 0 4456 4457 0 4455 4457 0;
	setAttr ".ed[8964:9129]" 4370 4552 0 4371 4553 0 4458 4459 0 4372 4551 0 4460 4458 0
		 4373 4550 0 4460 4461 0 4461 4459 0 4374 4531 0 4375 4532 0 4462 4463 0 4376 4530 0
		 4462 4464 0 4377 4533 0 4464 4465 0 4463 4465 0 4378 4560 0 4379 4561 0 4466 4467 0
		 4380 4559 0 4468 4466 0 4381 4558 0 4468 4469 0 4469 4467 0 4382 4543 0 4383 4544 0
		 4470 4471 0 4384 4542 0 4470 4472 0 4385 4545 0 4472 4473 0 4471 4473 0 4386 4596 0
		 4387 4597 0 4474 4475 0 4388 4595 0 4474 4476 0 4389 4594 0 4476 4477 0 4477 4475 0
		 4390 4600 0 4391 4601 0 4478 4479 0 4392 4599 0 4480 4478 0 4393 4598 0 4480 4481 0
		 4479 4481 0 4394 4588 0 4395 4589 0 4482 4483 0 4396 4587 0 4482 4484 0 4397 4586 0
		 4484 4485 0 4485 4483 0 4398 4592 0 4399 4593 0 4486 4487 0 4400 4591 0 4488 4486 0
		 4401 4590 0 4488 4489 0 4487 4489 0 4402 4580 0 4403 4581 0 4490 4491 0 4404 4579 0
		 4490 4492 0 4405 4578 0 4492 4493 0 4493 4491 0 4406 4584 0 4407 4585 0 4494 4495 0
		 4408 4583 0 4496 4494 0 4409 4582 0 4496 4497 0 4495 4497 0 4410 4572 0 4411 4573 0
		 4498 4499 0 4412 4571 0 4498 4500 0 4413 4570 0 4500 4501 0 4501 4499 0 4414 4576 0
		 4415 4577 0 4502 4503 0 4416 4575 0 4504 4502 0 4417 4574 0 4504 4505 0 4503 4505 0
		 4418 4564 0 4419 4565 0 4506 4507 0 4420 4563 0 4506 4508 0 4421 4562 0 4508 4509 0
		 4509 4507 0 4422 4568 0 4423 4569 0 4510 4511 0 4424 4567 0 4512 4510 0 4425 4566 0
		 4512 4513 0 4511 4513 0 4514 4445 0 4515 4444 0 4514 4515 1 4516 4442 0 4515 4516 1
		 4517 4443 0 4516 4517 1 4517 4514 1 4518 4448 0 4519 4446 0 4518 4519 1 4520 4447 0
		 4519 4520 1 4521 4449 0 4520 4521 1 4521 4518 1 4522 4428 0 4523 4426 0 4522 4523 1
		 4524 4427 0 4523 4524 1 4525 4429 0 4524 4525 1 4525 4522 1 4526 4456 0 4527 4454 0
		 4526 4527 1 4528 4455 0 4527 4528 1 4529 4457 0 4528 4529 1 4529 4526 1 4530 4464 0
		 4531 4462 0 4530 4531 1 4532 4463 0 4531 4532 1 4533 4465 0 4532 4533 1 4533 4530 1
		 4534 4432 0 4535 4430 0 4534 4535 1 4536 4431 0 4535 4536 1 4537 4433 0 4536 4537 1
		 4537 4534 1 4538 4441 0 4539 4440 0 4538 4539 1 4540 4438 0 4539 4540 1 4541 4439 0;
	setAttr ".ed[9130:9295]" 4540 4541 1 4541 4538 1 4542 4472 0 4543 4470 0 4542 4543 1
		 4544 4471 0 4543 4544 1 4545 4473 0 4544 4545 1 4545 4542 1 4546 4453 0 4547 4452 0
		 4546 4547 1 4548 4450 0 4547 4548 1 4549 4451 0 4548 4549 1 4549 4546 1 4550 4461 0
		 4551 4460 0 4550 4551 1 4552 4458 0 4551 4552 1 4553 4459 0 4552 4553 1 4553 4550 1
		 4554 4437 0 4555 4436 0 4554 4555 1 4556 4434 0 4555 4556 1 4557 4435 0 4556 4557 1
		 4557 4554 1 4558 4469 0 4559 4468 0 4558 4559 1 4560 4466 0 4559 4560 1 4561 4467 0
		 4560 4561 1 4561 4558 1 4562 4509 0 4563 4508 0 4562 4563 1 4564 4506 0 4563 4564 1
		 4565 4507 0 4564 4565 1 4565 4562 1 4566 4513 0 4567 4512 0 4566 4567 1 4568 4510 0
		 4567 4568 1 4569 4511 0 4568 4569 1 4569 4566 1 4570 4501 0 4571 4500 0 4570 4571 1
		 4572 4498 0 4571 4572 1 4573 4499 0 4572 4573 1 4573 4570 1 4574 4505 0 4575 4504 0
		 4574 4575 1 4576 4502 0 4575 4576 1 4577 4503 0 4576 4577 1 4577 4574 1 4578 4493 0
		 4579 4492 0 4578 4579 1 4580 4490 0 4579 4580 1 4581 4491 0 4580 4581 1 4581 4578 1
		 4582 4497 0 4583 4496 0 4582 4583 1 4584 4494 0 4583 4584 1 4585 4495 0 4584 4585 1
		 4585 4582 1 4586 4485 0 4587 4484 0 4586 4587 1 4588 4482 0 4587 4588 1 4589 4483 0
		 4588 4589 1 4589 4586 1 4590 4489 0 4591 4488 0 4590 4591 1 4592 4486 0 4591 4592 1
		 4593 4487 0 4592 4593 1 4593 4590 1 4594 4477 0 4595 4476 0 4594 4595 1 4596 4474 0
		 4595 4596 1 4597 4475 0 4596 4597 1 4597 4594 1 4598 4481 0 4599 4480 0 4598 4599 1
		 4600 4478 0 4599 4600 1 4601 4479 0 4600 4601 1 4601 4598 1 4602 3534 0 4603 3536 0
		 4602 4603 1 4604 4612 1 4603 4604 1 4605 4613 1 4604 4605 1 4606 3537 0 4605 4606 1
		 4607 3535 0 4606 4607 1 4608 4616 1 4607 4608 1 4609 4617 1 4608 4609 1 4609 4602 1
		 4610 4602 0 4611 4603 0 4610 4611 1 4612 4200 1 4611 4612 1 4613 4136 1 4612 4613 1
		 4614 4606 0 4613 4614 1 4615 4607 0 4614 4615 1 4616 3549 1 4615 4616 1 4617 3560 1
		 4616 4617 1 4617 4610 1 2975 4618 1 2976 4619 1 4618 4619 1 2985 4620 1 4618 4620 1
		 2977 4621 1 4619 4621 1 2978 4622 1 4621 4622 1 4622 4620 1 2981 4623 1 2982 4624 1;
	setAttr ".ed[9296:9367]" 4623 4624 1 2985 4625 1 4623 4625 1 2983 4626 1 4624 4626 1
		 2984 4627 1 4626 4627 1 4627 4625 1 4618 4628 1 4619 4629 1 4628 4629 0 4620 4630 1
		 4628 4630 0 4621 4631 1 4629 4631 0 4622 4632 1 4631 4632 0 4632 4630 0 4623 4633 1
		 4624 4634 1 4633 4634 0 4625 4635 1 4633 4635 0 4626 4636 1 4634 4636 0 4627 4637 1
		 4636 4637 0 4637 4635 0 4628 4638 0 4629 4639 0 4638 4639 0 4630 4640 0 4638 4640 0
		 4631 4641 0 4639 4641 0 4632 4642 0 4641 4642 0 4642 4640 0 4633 4643 0 4634 4644 0
		 4643 4644 0 4635 4645 0 4643 4645 0 4636 4646 0 4644 4646 0 4637 4647 0 4646 4647 0
		 4647 4645 0 4638 4648 1 4639 4649 1 4648 4649 1 4640 4650 1 4649 4650 0 4648 4650 1
		 4641 4651 1 4649 4651 1 4651 4650 0 4642 4652 1 4651 4652 1 4652 4650 1 4643 4653 1
		 4644 4654 1 4653 4654 1 4645 4655 1 4654 4655 0 4653 4655 1 4646 4656 1 4654 4656 1
		 4656 4655 0 4647 4657 1 4656 4657 1 4657 4655 1;
	setAttr -s 4704 -ch 18736 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 32 -175 -34 -1
		mu 0 4 8 116 114 9
		f 4 33 -173 -35 -2
		mu 0 4 9 114 113 10
		f 4 34 -171 -36 -3
		mu 0 4 10 113 112 11
		f 4 35 -169 -37 -4
		mu 0 4 11 112 111 12
		f 4 36 -167 -38 -5
		mu 0 4 12 111 110 13
		f 4 37 -165 -39 -6
		mu 0 4 13 110 109 14
		f 4 38 -163 -40 -7
		mu 0 4 14 109 108 15
		f 4 39 -176 -33 -8
		mu 0 4 15 108 115 16
		f 4 40 16 -42 -9
		mu 0 4 17 26 27 18
		f 4 41 17 -43 -10
		mu 0 4 18 27 28 19
		f 4 42 18 -44 -11
		mu 0 4 19 28 29 20
		f 4 43 19 -45 -12
		mu 0 4 20 29 30 21
		f 4 44 20 -46 -13
		mu 0 4 21 30 31 22
		f 4 45 21 -47 -14
		mu 0 4 22 31 32 23
		f 4 46 22 -48 -15
		mu 0 4 23 32 33 24
		f 4 47 23 -41 -16
		mu 0 4 24 33 34 25
		f 4 48 24 -50 -17
		mu 0 4 26 35 36 27
		f 4 49 25 -51 -18
		mu 0 4 27 36 37 28
		f 4 50 26 -52 -19
		mu 0 4 28 37 38 29
		f 4 51 27 -53 -20
		mu 0 4 29 38 39 30
		f 4 52 28 -54 -21
		mu 0 4 30 39 40 31
		f 4 53 29 -55 -22
		mu 0 4 31 40 41 32
		f 4 54 30 -56 -23
		mu 0 4 32 41 42 33
		f 4 55 31 -49 -24
		mu 0 4 33 42 43 34
		f 3 -216 214 212
		mu 0 3 133 135 134
		f 3 -231 229 227
		mu 0 3 139 141 140
		f 3 -234 230 232
		mu 0 3 142 141 139
		f 3 -237 233 235
		mu 0 3 143 141 142
		f 3 -240 236 238
		mu 0 3 144 141 143
		f 3 -221 219 218
		mu 0 3 136 135 137
		f 3 -224 220 222
		mu 0 3 138 135 136
		f 3 -215 223 224
		mu 0 3 134 135 138
		f 3 56 -58 -25
		mu 0 3 50 53 49
		f 3 57 -59 -26
		mu 0 3 49 53 48
		f 3 58 -60 -27
		mu 0 3 48 53 47
		f 3 59 -61 -28
		mu 0 3 47 53 46
		f 3 60 -62 -29
		mu 0 3 46 53 45
		f 3 61 -63 -30
		mu 0 3 45 53 44
		f 3 62 -64 -31
		mu 0 3 44 53 51
		f 3 63 -57 -32
		mu 0 3 51 53 50
		f 4 65 -139 -65 66
		mu 0 4 56 96 94 54
		f 4 67 -141 -66 68
		mu 0 4 57 97 95 55
		f 4 69 -143 -68 70
		mu 0 4 58 98 97 57
		f 4 71 -144 -70 72
		mu 0 4 59 90 98 58
		f 4 73 -131 -72 74
		mu 0 4 60 91 90 59
		f 4 75 -133 -74 76
		mu 0 4 61 92 91 60
		f 4 77 -135 -76 78
		mu 0 4 62 93 92 61
		f 4 64 -137 -78 79
		mu 0 4 54 94 93 62
		f 4 81 -101 -81 82
		mu 0 4 64 74 73 63
		f 4 83 -103 -82 84
		mu 0 4 65 75 74 64
		f 4 85 -105 -84 86
		mu 0 4 66 76 75 65
		f 4 87 -107 -86 88
		mu 0 4 67 77 76 66
		f 4 89 -109 -88 90
		mu 0 4 69 79 77 67
		f 4 91 -111 -90 92
		mu 0 4 70 80 78 68
		f 4 93 -112 -92 94
		mu 0 4 71 72 80 70
		f 4 80 -99 -94 95
		mu 0 4 63 73 72 71
		f 4 97 -128 -97 98
		mu 0 4 73 81 89 72
		f 4 99 -115 -98 100
		mu 0 4 74 82 81 73
		f 4 101 -117 -100 102
		mu 0 4 75 83 82 74
		f 4 103 -119 -102 104
		mu 0 4 76 84 83 75
		f 4 105 -121 -104 106
		mu 0 4 77 85 84 76
		f 4 107 -123 -106 108
		mu 0 4 79 87 85 77
		f 4 109 -125 -108 110
		mu 0 4 80 88 86 78
		f 4 96 -127 -110 111
		mu 0 4 72 89 88 80
		f 4 113 -151 -113 114
		mu 0 4 82 102 101 81
		f 4 115 -153 -114 116
		mu 0 4 83 103 102 82
		f 4 117 -155 -116 118
		mu 0 4 84 104 103 83
		f 4 119 -157 -118 120
		mu 0 4 85 105 104 84
		f 4 121 -159 -120 122
		mu 0 4 87 107 105 85
		f 4 123 -160 -122 124
		mu 0 4 88 99 106 86
		f 4 125 -147 -124 126
		mu 0 4 89 100 99 88
		f 4 112 -149 -126 127
		mu 0 4 81 101 100 89
		f 4 129 -83 -129 130
		mu 0 4 91 64 63 90
		f 4 131 -85 -130 132
		mu 0 4 92 65 64 91
		f 4 133 -87 -132 134
		mu 0 4 93 66 65 92
		f 4 135 -89 -134 136
		mu 0 4 94 67 66 93
		f 4 137 -91 -136 138
		mu 0 4 96 69 67 94
		f 4 139 -93 -138 140
		mu 0 4 97 70 68 95
		f 4 141 -95 -140 142
		mu 0 4 98 71 70 97
		f 4 128 -96 -142 143
		mu 0 4 90 63 71 98
		f 4 145 14 -145 146
		mu 0 4 100 23 24 99
		f 4 147 13 -146 148
		mu 0 4 101 22 23 100
		f 4 149 12 -148 150
		mu 0 4 102 21 22 101
		f 4 151 11 -150 152
		mu 0 4 103 20 21 102
		f 4 153 10 -152 154
		mu 0 4 104 19 20 103
		f 4 155 9 -154 156
		mu 0 4 105 18 19 104
		f 4 157 8 -156 158
		mu 0 4 107 17 18 105
		f 4 144 15 -158 159
		mu 0 4 99 24 25 106
		f 4 161 -71 -161 162
		mu 0 4 109 58 57 108
		f 4 163 -73 -162 164
		mu 0 4 110 59 58 109
		f 4 165 -75 -164 166
		mu 0 4 111 60 59 110
		f 4 167 -77 -166 168
		mu 0 4 112 61 60 111
		f 4 169 -79 -168 170
		mu 0 4 113 62 61 112
		f 4 171 -80 -170 172
		mu 0 4 114 54 62 113
		f 4 173 -67 -172 174
		mu 0 4 116 56 54 114
		f 4 160 -69 -174 175
		mu 0 4 108 57 55 115
		f 4 177 -210 -177 0
		mu 0 4 1 125 132 0
		f 4 180 -197 -178 1
		mu 0 4 2 126 125 1
		f 4 182 -199 -181 2
		mu 0 4 3 127 126 2
		f 4 184 -201 -183 3
		mu 0 4 4 128 127 3
		f 4 186 -203 -185 4
		mu 0 4 5 129 128 4
		f 4 189 -205 -187 5
		mu 0 4 6 130 129 5
		f 4 191 -207 -190 6
		mu 0 4 7 131 130 6
		f 4 176 -209 -192 7
		mu 0 4 0 132 131 7
		f 4 195 -182 -195 196
		mu 0 4 126 119 117 125
		f 4 197 -184 -196 198
		mu 0 4 127 120 119 126
		f 4 199 -186 -198 200
		mu 0 4 128 121 120 127
		f 4 201 -188 -200 202
		mu 0 4 129 122 121 128
		f 4 203 -191 -202 204
		mu 0 4 130 123 122 129
		f 4 205 -193 -204 206
		mu 0 4 131 124 123 130
		f 4 207 -194 -206 208
		mu 0 4 132 118 124 131
		f 4 194 -179 -208 209
		mu 0 4 125 117 118 132
		f 4 211 -213 -211 178
		mu 0 4 117 133 134 118
		f 4 213 215 -212 -180
		mu 0 4 52 135 133 117
		f 4 217 -219 -217 190
		mu 0 4 123 136 137 122
		f 4 216 -220 -214 188
		mu 0 4 122 137 135 52
		f 4 221 -223 -218 192
		mu 0 4 124 138 136 123
		f 4 210 -225 -222 193
		mu 0 4 118 134 138 124
		f 4 226 -228 -226 181
		mu 0 4 119 139 140 117
		f 4 225 -230 -229 179
		mu 0 4 117 140 141 52
		f 4 231 -233 -227 183
		mu 0 4 120 142 139 119
		f 4 234 -236 -232 185
		mu 0 4 121 143 142 120
		f 4 237 -239 -235 187
		mu 0 4 122 144 143 121
		f 4 228 239 -238 -189
		mu 0 4 52 141 144 122
		f 4 240 273 414 -273
		mu 0 4 145 146 147 148
		f 4 241 274 412 -274
		mu 0 4 146 149 150 147
		f 4 242 275 410 -275
		mu 0 4 149 151 152 150
		f 4 243 276 408 -276
		mu 0 4 151 153 154 152
		f 4 244 277 406 -277
		mu 0 4 153 155 156 154
		f 4 245 278 404 -278
		mu 0 4 155 157 158 156
		f 4 246 279 402 -279
		mu 0 4 157 159 160 158
		f 4 247 272 415 -280
		mu 0 4 159 161 162 160
		f 4 248 281 -257 -281
		mu 0 4 163 164 165 166
		f 4 249 282 -258 -282
		mu 0 4 164 167 168 165
		f 4 250 283 -259 -283
		mu 0 4 167 169 170 168
		f 4 251 284 -260 -284
		mu 0 4 169 171 172 170
		f 4 252 285 -261 -285
		mu 0 4 171 173 174 172
		f 4 253 286 -262 -286
		mu 0 4 173 175 176 174
		f 4 254 287 -263 -287
		mu 0 4 175 177 178 176
		f 4 255 280 -264 -288
		mu 0 4 177 179 180 178
		f 4 256 289 -265 -289
		mu 0 4 166 165 181 182
		f 4 257 290 -266 -290
		mu 0 4 165 168 183 181
		f 4 258 291 -267 -291
		mu 0 4 168 170 184 183
		f 4 259 292 -268 -292
		mu 0 4 170 172 185 184
		f 4 260 293 -269 -293
		mu 0 4 172 174 186 185
		f 4 261 294 -270 -294
		mu 0 4 174 176 187 186
		f 4 262 295 -271 -295
		mu 0 4 176 178 188 187
		f 4 263 288 -272 -296
		mu 0 4 178 180 189 188
		f 3 -453 -455 455
		mu 0 3 190 191 192
		f 3 -468 -470 470
		mu 0 3 193 194 195
		f 3 -473 -471 473
		mu 0 3 196 193 195
		f 3 -476 -474 476
		mu 0 3 197 196 195
		f 3 -479 -477 479
		mu 0 3 198 197 195
		f 3 -459 -460 460
		mu 0 3 199 200 192
		f 3 -463 -461 463
		mu 0 3 201 199 192
		f 3 -465 -464 454
		mu 0 3 191 201 192
		f 3 264 297 -297
		mu 0 3 202 203 204
		f 3 265 298 -298
		mu 0 3 203 205 204
		f 3 266 299 -299
		mu 0 3 205 206 204
		f 3 267 300 -300
		mu 0 3 206 207 204
		f 3 268 301 -301
		mu 0 3 207 208 204
		f 3 269 302 -302
		mu 0 3 208 209 204
		f 3 270 303 -303
		mu 0 3 209 210 204
		f 3 271 296 -304
		mu 0 3 210 202 204
		f 4 -307 304 378 -306
		mu 0 4 211 212 213 214
		f 4 -309 305 380 -308
		mu 0 4 215 216 217 218
		f 4 -311 307 382 -310
		mu 0 4 219 215 218 220
		f 4 -313 309 383 -312
		mu 0 4 221 219 220 222
		f 4 -315 311 370 -314
		mu 0 4 223 221 222 224
		f 4 -317 313 372 -316
		mu 0 4 225 223 224 226
		f 4 -319 315 374 -318
		mu 0 4 227 225 226 228
		f 4 -320 317 376 -305
		mu 0 4 212 227 228 213
		f 4 -323 320 340 -322
		mu 0 4 229 230 231 232
		f 4 -325 321 342 -324
		mu 0 4 233 229 232 234
		f 4 -327 323 344 -326
		mu 0 4 235 233 234 236
		f 4 -329 325 346 -328
		mu 0 4 237 235 236 238
		f 4 -331 327 348 -330
		mu 0 4 239 237 238 240
		f 4 -333 329 350 -332
		mu 0 4 241 242 243 244
		f 4 -335 331 351 -334
		mu 0 4 245 241 244 246
		f 4 -336 333 338 -321
		mu 0 4 230 245 246 231
		f 4 -339 336 367 -338
		mu 0 4 231 246 247 248
		f 4 -341 337 354 -340
		mu 0 4 232 231 248 249
		f 4 -343 339 356 -342
		mu 0 4 234 232 249 250
		f 4 -345 341 358 -344
		mu 0 4 236 234 250 251
		f 4 -347 343 360 -346
		mu 0 4 238 236 251 252
		f 4 -349 345 362 -348
		mu 0 4 240 238 252 253
		f 4 -351 347 364 -350
		mu 0 4 244 243 254 255
		f 4 -352 349 366 -337
		mu 0 4 246 244 255 247
		f 4 -355 352 390 -354
		mu 0 4 249 248 256 257
		f 4 -357 353 392 -356
		mu 0 4 250 249 257 258
		f 4 -359 355 394 -358
		mu 0 4 251 250 258 259
		f 4 -361 357 396 -360
		mu 0 4 252 251 259 260
		f 4 -363 359 398 -362
		mu 0 4 253 252 260 261
		f 4 -365 361 399 -364
		mu 0 4 255 254 262 263
		f 4 -367 363 386 -366
		mu 0 4 247 255 263 264
		f 4 -368 365 388 -353
		mu 0 4 248 247 264 256
		f 4 -371 368 322 -370
		mu 0 4 224 222 230 229
		f 4 -373 369 324 -372
		mu 0 4 226 224 229 233
		f 4 -375 371 326 -374
		mu 0 4 228 226 233 235
		f 4 -377 373 328 -376
		mu 0 4 213 228 235 237
		f 4 -379 375 330 -378
		mu 0 4 214 213 237 239
		f 4 -381 377 332 -380
		mu 0 4 218 217 242 241
		f 4 -383 379 334 -382
		mu 0 4 220 218 241 245
		f 4 -384 381 335 -369
		mu 0 4 222 220 245 230
		f 4 -387 384 -255 -386
		mu 0 4 264 263 177 175
		f 4 -389 385 -254 -388
		mu 0 4 256 264 175 173
		f 4 -391 387 -253 -390
		mu 0 4 257 256 173 171
		f 4 -393 389 -252 -392
		mu 0 4 258 257 171 169
		f 4 -395 391 -251 -394
		mu 0 4 259 258 169 167
		f 4 -397 393 -250 -396
		mu 0 4 260 259 167 164
		f 4 -399 395 -249 -398
		mu 0 4 261 260 164 163
		f 4 -400 397 -256 -385
		mu 0 4 263 262 179 177
		f 4 -403 400 310 -402
		mu 0 4 158 160 215 219
		f 4 -405 401 312 -404
		mu 0 4 156 158 219 221
		f 4 -407 403 314 -406
		mu 0 4 154 156 221 223
		f 4 -409 405 316 -408
		mu 0 4 152 154 223 225
		f 4 -411 407 318 -410
		mu 0 4 150 152 225 227
		f 4 -413 409 319 -412
		mu 0 4 147 150 227 212
		f 4 -415 411 306 -414
		mu 0 4 148 147 212 211
		f 4 -416 413 308 -401
		mu 0 4 160 162 216 215
		f 4 -241 416 449 -418
		mu 0 4 265 266 267 268
		f 4 -242 417 436 -421
		mu 0 4 269 265 268 270
		f 4 -243 420 438 -423
		mu 0 4 271 269 270 272
		f 4 -244 422 440 -425
		mu 0 4 273 271 272 274
		f 4 -245 424 442 -427
		mu 0 4 275 273 274 276
		f 4 -246 426 444 -430
		mu 0 4 277 275 276 278
		f 4 -247 429 446 -432
		mu 0 4 279 277 278 280
		f 4 -248 431 448 -417
		mu 0 4 266 279 280 267
		f 4 -437 434 421 -436
		mu 0 4 270 268 281 282
		f 4 -439 435 423 -438
		mu 0 4 272 270 282 283
		f 4 -441 437 425 -440
		mu 0 4 274 272 283 284
		f 4 -443 439 427 -442
		mu 0 4 276 274 284 285
		f 4 -445 441 430 -444
		mu 0 4 278 276 285 286
		f 4 -447 443 432 -446
		mu 0 4 280 278 286 287
		f 4 -449 445 433 -448
		mu 0 4 267 280 287 288
		f 4 -450 447 418 -435
		mu 0 4 268 267 288 281
		f 4 -419 450 452 -452
		mu 0 4 281 288 191 190
		f 4 419 451 -456 -454
		mu 0 4 289 281 190 192
		f 4 -431 456 458 -458
		mu 0 4 286 285 200 199
		f 4 -429 453 459 -457
		mu 0 4 285 289 192 200
		f 4 -433 457 462 -462
		mu 0 4 287 286 199 201
		f 4 -434 461 464 -451
		mu 0 4 288 287 201 191
		f 4 -422 465 467 -467
		mu 0 4 282 281 194 193
		f 4 -420 468 469 -466
		mu 0 4 281 289 195 194
		f 4 -424 466 472 -472
		mu 0 4 283 282 193 196
		f 4 -426 471 475 -475
		mu 0 4 284 283 196 197
		f 4 -428 474 478 -478
		mu 0 4 285 284 197 198
		f 4 428 477 -480 -469
		mu 0 4 289 285 198 195
		f 4 480 513 578 -513
		mu 0 4 290 291 292 293
		f 4 481 514 591 -514
		mu 0 4 291 294 295 292
		f 4 482 515 590 -515
		mu 0 4 294 296 297 295
		f 4 483 516 588 -516
		mu 0 4 296 298 299 297
		f 4 484 517 586 -517
		mu 0 4 298 300 301 299
		f 4 485 518 584 -518
		mu 0 4 300 302 303 301
		f 4 486 519 582 -519
		mu 0 4 302 304 305 303
		f 4 487 512 580 -520
		mu 0 4 304 306 307 305
		f 4 488 521 -497 -521
		mu 0 4 308 309 310 311
		f 4 489 522 -498 -522
		mu 0 4 309 312 313 310
		f 4 490 523 -499 -523
		mu 0 4 312 314 315 313
		f 4 491 524 -500 -524
		mu 0 4 314 316 317 315
		f 4 492 525 -501 -525
		mu 0 4 316 318 319 317
		f 4 493 526 -502 -526
		mu 0 4 318 320 321 319
		f 4 494 527 -503 -527
		mu 0 4 320 322 323 321
		f 4 495 520 -504 -528
		mu 0 4 322 324 325 323
		f 4 496 529 -505 -529
		mu 0 4 311 310 326 327
		f 4 497 530 -506 -530
		mu 0 4 310 313 328 326
		f 4 498 531 -507 -531
		mu 0 4 313 315 329 328
		f 4 499 532 -508 -532
		mu 0 4 315 317 330 329
		f 4 500 533 -509 -533
		mu 0 4 317 319 331 330
		f 4 501 534 -510 -534
		mu 0 4 319 321 332 331
		f 4 502 535 -511 -535
		mu 0 4 321 323 333 332
		f 4 503 528 -512 -536
		mu 0 4 323 325 334 333
		f 3 -677 -679 679
		mu 0 3 335 336 337
		f 3 -692 -694 694
		mu 0 3 338 339 340
		f 3 -697 -695 697
		mu 0 3 341 338 340
		f 3 -700 -698 700
		mu 0 3 342 341 340
		f 3 -703 -701 703
		mu 0 3 343 342 340
		f 3 -683 -684 684
		mu 0 3 344 345 337
		f 3 -687 -685 687
		mu 0 3 346 344 337
		f 3 -689 -688 678
		mu 0 3 336 346 337
		f 3 504 537 -537
		mu 0 3 347 348 349
		f 3 505 538 -538
		mu 0 3 348 350 349
		f 3 506 539 -539
		mu 0 3 350 351 349
		f 3 507 540 -540
		mu 0 3 351 352 349
		f 3 508 541 -541
		mu 0 3 352 353 349
		f 3 509 542 -542
		mu 0 3 353 354 349
		f 3 510 543 -543
		mu 0 3 354 355 349
		f 3 511 536 -544
		mu 0 3 355 347 349
		f 4 -547 544 -489 -546
		mu 0 4 356 357 309 308
		f 4 -549 545 -496 -548
		mu 0 4 358 359 324 322
		f 4 -551 547 -495 -550
		mu 0 4 360 358 322 320
		f 4 -553 549 -494 -552
		mu 0 4 361 360 320 318
		f 4 -555 551 -493 -554
		mu 0 4 362 361 318 316
		f 4 -557 553 -492 -556
		mu 0 4 363 362 316 314
		f 4 -559 555 -491 -558
		mu 0 4 364 363 314 312
		f 4 -560 557 -490 -545
		mu 0 4 357 364 312 309
		f 4 -563 560 718 -562
		mu 0 4 365 366 367 368
		f 4 -565 561 719 -564
		mu 0 4 369 370 371 372
		f 4 -567 563 706 -566
		mu 0 4 373 369 372 374
		f 4 -569 565 708 -568
		mu 0 4 375 373 374 376
		f 4 -571 567 710 -570
		mu 0 4 377 375 376 378
		f 4 -573 569 712 -572
		mu 0 4 379 377 378 380
		f 4 -575 571 714 -574
		mu 0 4 381 379 380 382
		f 4 -576 573 716 -561
		mu 0 4 366 381 382 367
		f 4 -579 576 636 -578
		mu 0 4 293 292 383 384
		f 4 -581 577 638 -580
		mu 0 4 305 307 385 386
		f 4 -583 579 639 -582
		mu 0 4 303 305 386 387
		f 4 -585 581 626 -584
		mu 0 4 301 303 387 388
		f 4 -587 583 628 -586
		mu 0 4 299 301 388 389
		f 4 -589 585 630 -588
		mu 0 4 297 299 389 390
		f 4 -591 587 632 -590
		mu 0 4 295 297 390 391
		f 4 -592 589 634 -577
		mu 0 4 292 295 391 383
		f 4 -595 592 610 -594
		mu 0 4 392 393 394 395
		f 4 -597 593 612 -596
		mu 0 4 396 392 395 397
		f 4 -599 595 614 -598
		mu 0 4 398 396 397 399
		f 4 -601 597 616 -600
		mu 0 4 400 398 399 401
		f 4 -603 599 618 -602
		mu 0 4 402 400 401 403
		f 4 -605 601 620 -604
		mu 0 4 404 405 406 407
		f 4 -607 603 622 -606
		mu 0 4 408 404 407 409
		f 4 -608 605 623 -593
		mu 0 4 393 408 409 394
		f 4 -611 608 570 -610
		mu 0 4 395 394 375 377
		f 4 -613 609 572 -612
		mu 0 4 397 395 377 379
		f 4 -615 611 574 -614
		mu 0 4 399 397 379 381
		f 4 -617 613 575 -616
		mu 0 4 401 399 381 366
		f 4 -619 615 562 -618
		mu 0 4 403 401 366 365
		f 4 -621 617 564 -620
		mu 0 4 407 406 370 369
		f 4 -623 619 566 -622
		mu 0 4 409 407 369 373
		f 4 -624 621 568 -609
		mu 0 4 394 409 373 375
		f 4 -627 624 607 -626
		mu 0 4 388 387 408 393
		f 4 -629 625 594 -628
		mu 0 4 389 388 393 392
		f 4 -631 627 596 -630
		mu 0 4 390 389 392 396
		f 4 -633 629 598 -632
		mu 0 4 391 390 396 398
		f 4 -635 631 600 -634
		mu 0 4 383 391 398 400
		f 4 -637 633 602 -636
		mu 0 4 384 383 400 402
		f 4 -639 635 604 -638
		mu 0 4 386 385 405 404
		f 4 -640 637 606 -625
		mu 0 4 387 386 404 408
		f 4 -481 640 673 -642
		mu 0 4 410 411 412 413
		f 4 -482 641 660 -645
		mu 0 4 414 410 413 415
		f 4 -483 644 662 -647
		mu 0 4 416 414 415 417
		f 4 -484 646 664 -649
		mu 0 4 418 416 417 419
		f 4 -485 648 666 -651
		mu 0 4 420 418 419 421
		f 4 -486 650 668 -654
		mu 0 4 422 420 421 423
		f 4 -487 653 670 -656
		mu 0 4 424 422 423 425
		f 4 -488 655 672 -641
		mu 0 4 411 424 425 412
		f 4 -661 658 645 -660
		mu 0 4 415 413 426 427
		f 4 -663 659 647 -662
		mu 0 4 417 415 427 428
		f 4 -665 661 649 -664
		mu 0 4 419 417 428 429
		f 4 -667 663 651 -666
		mu 0 4 421 419 429 430
		f 4 -669 665 654 -668
		mu 0 4 423 421 430 431
		f 4 -671 667 656 -670
		mu 0 4 425 423 431 432
		f 4 -673 669 657 -672
		mu 0 4 412 425 432 433
		f 4 -674 671 642 -659
		mu 0 4 413 412 433 426
		f 4 -643 674 676 -676
		mu 0 4 426 433 336 335
		f 4 643 675 -680 -678
		mu 0 4 434 426 335 337
		f 4 -655 680 682 -682
		mu 0 4 431 430 345 344
		f 4 -653 677 683 -681
		mu 0 4 430 434 337 345
		f 4 -657 681 686 -686
		mu 0 4 432 431 344 346
		f 4 -658 685 688 -675
		mu 0 4 433 432 346 336
		f 4 -646 689 691 -691
		mu 0 4 427 426 339 338
		f 4 -644 692 693 -690
		mu 0 4 426 434 340 339
		f 4 -648 690 696 -696
		mu 0 4 428 427 338 341
		f 4 -650 695 699 -699
		mu 0 4 429 428 341 342
		f 4 -652 698 702 -702
		mu 0 4 430 429 342 343
		f 4 652 701 -704 -693
		mu 0 4 434 430 343 340
		f 4 -707 704 550 -706
		mu 0 4 374 372 358 360
		f 4 -709 705 552 -708
		mu 0 4 376 374 360 361
		f 4 -711 707 554 -710
		mu 0 4 378 376 361 362
		f 4 -713 709 556 -712
		mu 0 4 380 378 362 363
		f 4 -715 711 558 -714
		mu 0 4 382 380 363 364
		f 4 -717 713 559 -716
		mu 0 4 367 382 364 357
		f 4 -719 715 546 -718
		mu 0 4 368 367 357 356
		f 4 -720 717 548 -705
		mu 0 4 372 371 359 358
		f 4 752 -819 -754 -721
		mu 0 4 435 438 437 436
		f 4 753 -832 -755 -722
		mu 0 4 436 437 440 439
		f 4 754 -831 -756 -723
		mu 0 4 439 440 442 441
		f 4 755 -829 -757 -724
		mu 0 4 441 442 444 443
		f 4 756 -827 -758 -725
		mu 0 4 443 444 446 445
		f 4 757 -825 -759 -726
		mu 0 4 445 446 448 447
		f 4 758 -823 -760 -727
		mu 0 4 447 448 450 449
		f 4 759 -821 -753 -728
		mu 0 4 449 450 452 451
		f 4 760 736 -762 -729
		mu 0 4 453 456 455 454
		f 4 761 737 -763 -730
		mu 0 4 454 455 458 457
		f 4 762 738 -764 -731
		mu 0 4 457 458 460 459
		f 4 763 739 -765 -732
		mu 0 4 459 460 462 461
		f 4 764 740 -766 -733
		mu 0 4 461 462 464 463
		f 4 765 741 -767 -734
		mu 0 4 463 464 466 465
		f 4 766 742 -768 -735
		mu 0 4 465 466 468 467
		f 4 767 743 -761 -736
		mu 0 4 467 468 470 469
		f 4 768 744 -770 -737
		mu 0 4 456 472 471 455
		f 4 769 745 -771 -738
		mu 0 4 455 471 473 458
		f 4 770 746 -772 -739
		mu 0 4 458 473 474 460
		f 4 771 747 -773 -740
		mu 0 4 460 474 475 462
		f 4 772 748 -774 -741
		mu 0 4 462 475 476 464
		f 4 773 749 -775 -742
		mu 0 4 464 476 477 466
		f 4 774 750 -776 -743
		mu 0 4 466 477 478 468
		f 4 775 751 -769 -744
		mu 0 4 468 478 479 470
		f 3 -920 918 916
		mu 0 3 480 482 481
		f 3 -935 933 931
		mu 0 3 483 485 484
		f 3 -938 934 936
		mu 0 3 486 485 483
		f 3 -941 937 939
		mu 0 3 487 485 486
		f 3 -944 940 942
		mu 0 3 488 485 487
		f 3 -925 923 922
		mu 0 3 489 482 490
		f 3 -928 924 926
		mu 0 3 491 482 489
		f 3 -919 927 928
		mu 0 3 481 482 491
		f 3 776 -778 -745
		mu 0 3 492 494 493
		f 3 777 -779 -746
		mu 0 3 493 494 495
		f 3 778 -780 -747
		mu 0 3 495 494 496
		f 3 779 -781 -748
		mu 0 3 496 494 497
		f 3 780 -782 -749
		mu 0 3 497 494 498
		f 3 781 -783 -750
		mu 0 3 498 494 499
		f 3 782 -784 -751
		mu 0 3 499 494 500
		f 3 783 -777 -752
		mu 0 3 500 494 492
		f 4 785 728 -785 786
		mu 0 4 501 453 454 502
		f 4 787 735 -786 788
		mu 0 4 503 467 469 504
		f 4 789 734 -788 790
		mu 0 4 505 465 467 503
		f 4 791 733 -790 792
		mu 0 4 506 463 465 505
		f 4 793 732 -792 794
		mu 0 4 507 461 463 506
		f 4 795 731 -794 796
		mu 0 4 508 459 461 507
		f 4 797 730 -796 798
		mu 0 4 509 457 459 508
		f 4 784 729 -798 799
		mu 0 4 502 454 457 509
		f 4 801 -959 -801 802
		mu 0 4 510 513 512 511
		f 4 803 -960 -802 804
		mu 0 4 514 517 516 515
		f 4 805 -947 -804 806
		mu 0 4 518 519 517 514
		f 4 807 -949 -806 808
		mu 0 4 520 521 519 518
		f 4 809 -951 -808 810
		mu 0 4 522 523 521 520
		f 4 811 -953 -810 812
		mu 0 4 524 525 523 522
		f 4 813 -955 -812 814
		mu 0 4 526 527 525 524
		f 4 800 -957 -814 815
		mu 0 4 511 512 527 526
		f 4 817 -877 -817 818
		mu 0 4 438 529 528 437
		f 4 819 -879 -818 820
		mu 0 4 450 531 530 452
		f 4 821 -880 -820 822
		mu 0 4 448 532 531 450
		f 4 823 -867 -822 824
		mu 0 4 446 533 532 448
		f 4 825 -869 -824 826
		mu 0 4 444 534 533 446
		f 4 827 -871 -826 828
		mu 0 4 442 535 534 444
		f 4 829 -873 -828 830
		mu 0 4 440 536 535 442
		f 4 816 -875 -830 831
		mu 0 4 437 528 536 440
		f 4 833 -851 -833 834
		mu 0 4 537 540 539 538
		f 4 835 -853 -834 836
		mu 0 4 541 542 540 537
		f 4 837 -855 -836 838
		mu 0 4 543 544 542 541
		f 4 839 -857 -838 840
		mu 0 4 545 546 544 543
		f 4 841 -859 -840 842
		mu 0 4 547 548 546 545
		f 4 843 -861 -842 844
		mu 0 4 549 552 551 550
		f 4 845 -863 -844 846
		mu 0 4 553 554 552 549
		f 4 832 -864 -846 847
		mu 0 4 538 539 554 553
		f 4 849 -811 -849 850
		mu 0 4 540 522 520 539
		f 4 851 -813 -850 852
		mu 0 4 542 524 522 540
		f 4 853 -815 -852 854
		mu 0 4 544 526 524 542
		f 4 855 -816 -854 856
		mu 0 4 546 511 526 544
		f 4 857 -803 -856 858
		mu 0 4 548 510 511 546
		f 4 859 -805 -858 860
		mu 0 4 552 514 515 551
		f 4 861 -807 -860 862
		mu 0 4 554 518 514 552
		f 4 848 -809 -862 863
		mu 0 4 539 520 518 554
		f 4 865 -848 -865 866
		mu 0 4 533 538 553 532
		f 4 867 -835 -866 868
		mu 0 4 534 537 538 533
		f 4 869 -837 -868 870
		mu 0 4 535 541 537 534
		f 4 871 -839 -870 872
		mu 0 4 536 543 541 535
		f 4 873 -841 -872 874
		mu 0 4 528 545 543 536
		f 4 875 -843 -874 876
		mu 0 4 529 547 545 528
		f 4 877 -845 -876 878
		mu 0 4 531 549 550 530
		f 4 864 -847 -878 879
		mu 0 4 532 553 549 531
		f 4 881 -914 -881 720
		mu 0 4 555 558 557 556
		f 4 884 -901 -882 721
		mu 0 4 559 560 558 555
		f 4 886 -903 -885 722
		mu 0 4 561 562 560 559
		f 4 888 -905 -887 723
		mu 0 4 563 564 562 561
		f 4 890 -907 -889 724
		mu 0 4 565 566 564 563
		f 4 893 -909 -891 725
		mu 0 4 567 568 566 565
		f 4 895 -911 -894 726
		mu 0 4 569 570 568 567
		f 4 880 -913 -896 727
		mu 0 4 556 557 570 569
		f 4 899 -886 -899 900
		mu 0 4 560 572 571 558
		f 4 901 -888 -900 902
		mu 0 4 562 573 572 560
		f 4 903 -890 -902 904
		mu 0 4 564 574 573 562
		f 4 905 -892 -904 906
		mu 0 4 566 575 574 564
		f 4 907 -895 -906 908
		mu 0 4 568 576 575 566
		f 4 909 -897 -908 910
		mu 0 4 570 577 576 568
		f 4 911 -898 -910 912
		mu 0 4 557 578 577 570
		f 4 898 -883 -912 913
		mu 0 4 558 571 578 557
		f 4 915 -917 -915 882
		mu 0 4 571 480 481 578
		f 4 917 919 -916 -884
		mu 0 4 579 482 480 571
		f 4 921 -923 -921 894
		mu 0 4 576 489 490 575
		f 4 920 -924 -918 892
		mu 0 4 575 490 482 579
		f 4 925 -927 -922 896
		mu 0 4 577 491 489 576
		f 4 914 -929 -926 897
		mu 0 4 578 481 491 577
		f 4 930 -932 -930 885
		mu 0 4 572 483 484 571
		f 4 929 -934 -933 883
		mu 0 4 571 484 485 579
		f 4 935 -937 -931 887
		mu 0 4 573 486 483 572
		f 4 938 -940 -936 889
		mu 0 4 574 487 486 573
		f 4 941 -943 -939 891
		mu 0 4 575 488 487 574
		f 4 932 943 -942 -893
		mu 0 4 579 485 488 575
		f 4 945 -791 -945 946
		mu 0 4 519 505 503 517
		f 4 947 -793 -946 948
		mu 0 4 521 506 505 519
		f 4 949 -795 -948 950
		mu 0 4 523 507 506 521
		f 4 951 -797 -950 952
		mu 0 4 525 508 507 523
		f 4 953 -799 -952 954
		mu 0 4 527 509 508 525
		f 4 955 -800 -954 956
		mu 0 4 512 502 509 527
		f 4 957 -787 -956 958
		mu 0 4 513 501 502 512
		f 4 944 -789 -958 959
		mu 0 4 517 503 504 516
		f 4 1010 1009 -962 -1008
		mu 0 4 580 581 582 583
		f 4 961 967 -963 -967
		mu 0 4 583 582 584 585
		f 4 962 969 1006 -969
		mu 0 4 585 584 586 587
		f 4 1011 -970 -968 -1010
		mu 0 4 581 588 589 582;
	setAttr ".fc[500:999]"
		f 4 1008 1007 966 968
		mu 0 4 590 580 583 591
		f 4 -975 972 -964 -974
		mu 0 4 592 593 594 595
		f 4 970 964 -977 973
		mu 0 4 596 597 598 599
		f 4 960 965 -979 -965
		mu 0 4 597 600 601 598
		f 4 -972 -973 -980 -966
		mu 0 4 600 602 603 601
		f 4 -983 980 974 -982
		mu 0 4 604 605 593 592
		f 4 976 975 -985 981
		mu 0 4 599 598 606 607
		f 4 978 977 -987 -976
		mu 0 4 598 601 608 606
		f 4 979 -981 -988 -978
		mu 0 4 601 603 609 608
		f 4 -991 988 982 -990
		mu 0 4 610 611 605 604
		f 4 984 983 -993 989
		mu 0 4 607 606 612 613
		f 4 986 985 -995 -984
		mu 0 4 606 608 614 612
		f 4 987 -989 -996 -986
		mu 0 4 608 609 615 614
		f 4 -999 996 990 -998
		mu 0 4 616 617 611 610
		f 4 992 991 -1001 997
		mu 0 4 613 612 618 619
		f 4 994 993 -1003 -992
		mu 0 4 612 614 620 618
		f 4 995 -997 -1004 -994
		mu 0 4 614 615 621 620
		f 4 -1007 1004 998 -1006
		mu 0 4 587 586 617 616
		f 4 1000 999 -1009 1005
		mu 0 4 619 618 580 590
		f 4 1002 1001 -1011 -1000
		mu 0 4 618 620 581 580
		f 4 1003 -1005 -1012 -1002
		mu 0 4 620 621 588 581
		f 4 1062 1061 -1014 -1060
		mu 0 4 622 623 624 625
		f 4 1013 1019 -1015 -1019
		mu 0 4 625 624 626 627
		f 4 1014 1021 1058 -1021
		mu 0 4 627 626 628 629
		f 4 1063 -1022 -1020 -1062
		mu 0 4 623 630 631 624
		f 4 1060 1059 1018 1020
		mu 0 4 632 622 625 633
		f 4 -1027 1024 -1016 -1026
		mu 0 4 634 635 636 637
		f 4 1022 1016 -1029 1025
		mu 0 4 638 639 640 641
		f 4 1012 1017 -1031 -1017
		mu 0 4 639 642 643 640
		f 4 -1024 -1025 -1032 -1018
		mu 0 4 642 644 645 643
		f 4 -1035 1032 1026 -1034
		mu 0 4 646 647 635 634
		f 4 1028 1027 -1037 1033
		mu 0 4 641 640 648 649
		f 4 1030 1029 -1039 -1028
		mu 0 4 640 643 650 648
		f 4 1031 -1033 -1040 -1030
		mu 0 4 643 645 651 650
		f 4 -1043 1040 1034 -1042
		mu 0 4 652 653 647 646
		f 4 1036 1035 -1045 1041
		mu 0 4 649 648 654 655
		f 4 1038 1037 -1047 -1036
		mu 0 4 648 650 656 654
		f 4 1039 -1041 -1048 -1038
		mu 0 4 650 651 657 656
		f 4 -1051 1048 1042 -1050
		mu 0 4 658 659 653 652
		f 4 1044 1043 -1053 1049
		mu 0 4 655 654 660 661
		f 4 1046 1045 -1055 -1044
		mu 0 4 654 656 662 660
		f 4 1047 -1049 -1056 -1046
		mu 0 4 656 657 663 662
		f 4 -1059 1056 1050 -1058
		mu 0 4 629 628 659 658
		f 4 1052 1051 -1061 1057
		mu 0 4 661 660 622 632
		f 4 1054 1053 -1063 -1052
		mu 0 4 660 662 623 622
		f 4 1055 -1057 -1064 -1054
		mu 0 4 662 663 630 623
		f 4 1114 1113 -1066 -1112
		mu 0 4 664 665 666 667
		f 4 1065 1071 -1067 -1071
		mu 0 4 667 666 668 669
		f 4 1066 1073 1110 -1073
		mu 0 4 669 668 670 671
		f 4 1115 -1074 -1072 -1114
		mu 0 4 665 672 673 666
		f 4 1112 1111 1070 1072
		mu 0 4 674 664 667 675
		f 4 -1079 1076 -1068 -1078
		mu 0 4 676 677 678 679
		f 4 1074 1068 -1081 1077
		mu 0 4 680 681 682 683
		f 4 1064 1069 -1083 -1069
		mu 0 4 681 684 685 682
		f 4 -1076 -1077 -1084 -1070
		mu 0 4 684 686 687 685
		f 4 -1087 1084 1078 -1086
		mu 0 4 688 689 677 676
		f 4 1080 1079 -1089 1085
		mu 0 4 683 682 690 691
		f 4 1082 1081 -1091 -1080
		mu 0 4 682 685 692 690
		f 4 1083 -1085 -1092 -1082
		mu 0 4 685 687 693 692
		f 4 -1095 1092 1086 -1094
		mu 0 4 694 695 689 688
		f 4 1088 1087 -1097 1093
		mu 0 4 691 690 696 697
		f 4 1090 1089 -1099 -1088
		mu 0 4 690 692 698 696
		f 4 1091 -1093 -1100 -1090
		mu 0 4 692 693 699 698
		f 4 -1103 1100 1094 -1102
		mu 0 4 700 701 695 694
		f 4 1096 1095 -1105 1101
		mu 0 4 697 696 702 703
		f 4 1098 1097 -1107 -1096
		mu 0 4 696 698 704 702
		f 4 1099 -1101 -1108 -1098
		mu 0 4 698 699 705 704
		f 4 -1111 1108 1102 -1110
		mu 0 4 671 670 701 700
		f 4 1104 1103 -1113 1109
		mu 0 4 703 702 664 674
		f 4 1106 1105 -1115 -1104
		mu 0 4 702 704 665 664
		f 4 1107 -1109 -1116 -1106
		mu 0 4 704 705 672 665
		f 4 -1256 1257 1256 -1118
		mu 0 4 706 707 708 709
		f 4 -1121 1117 1148 -1120
		mu 0 4 710 706 709 711
		f 4 -1123 1119 1150 1254
		mu 0 4 712 710 711 713
		f 4 -1124 1121 1151 -1117
		mu 0 4 714 715 716 717
		f 4 2518 2517 2884 -2516
		mu 0 4 718 719 720 721
		f 4 2516 2515 2886 -2514
		mu 0 4 722 723 724 725
		f 4 2514 2513 2888 -2512
		mu 0 4 726 727 728 729
		f 4 2524 2523 2878 -2522
		mu 0 4 730 731 732 733
		f 4 -1265 1267 1266 -1130
		mu 0 4 734 735 736 737
		f 4 -1133 1129 1156 -1132
		mu 0 4 738 734 737 739
		f 4 -1179 1180 1182 1276
		mu 0 4 740 741 742 743
		f 4 -1136 1133 1159 -1129
		mu 0 4 744 745 746 747
		f 4 -1259 1261 1260 -1138
		mu 0 4 748 749 750 751
		f 4 -1141 1137 1164 -1140
		mu 0 4 752 748 751 753
		f 4 -1143 1139 1166 1246
		mu 0 4 754 752 753 755
		f 4 -1144 1141 1167 -1137
		mu 0 4 756 757 758 759
		f 4 -1257 1259 1258 -1146
		mu 0 4 709 708 749 748
		f 4 -1149 1145 1140 -1148
		mu 0 4 711 709 748 752
		f 4 -1187 1188 1190 1250
		mu 0 4 760 761 762 763
		f 4 -1152 1149 1143 -1145
		mu 0 4 717 716 757 756
		f 4 -1267 1269 1268 -1154
		mu 0 4 737 736 764 765
		f 4 -1157 1153 1125 -1156
		mu 0 4 739 737 765 766
		f 4 -1159 1155 1126 1272
		mu 0 4 767 739 766 768
		f 4 -1160 1157 1127 -1153
		mu 0 4 747 746 769 770
		f 4 -1261 1263 1262 -1162
		mu 0 4 751 750 771 772
		f 4 -1165 1161 1172 -1164
		mu 0 4 753 751 772 773
		f 4 1283 -1195 1196 1198
		mu 0 4 774 775 776 777
		f 4 -1168 1165 1175 -1161
		mu 0 4 759 758 778 779
		f 4 -1263 1265 1264 -1170
		mu 0 4 772 771 735 734
		f 4 -1173 1169 1132 -1172
		mu 0 4 773 772 734 738
		f 4 -1175 1171 1134 1280
		mu 0 4 780 773 738 781
		f 4 -1176 1173 1135 -1169
		mu 0 4 779 778 745 744
		f 4 -1135 1176 1376 1375
		mu 0 4 781 738 782 783
		f 4 1131 1179 1374 -1177
		mu 0 4 738 739 784 782
		f 4 1158 1274 1372 -1180
		mu 0 4 739 767 785 784
		f 4 -1134 1177 1366 -1182
		mu 0 4 746 745 786 787
		f 4 -1151 1184 1358 1357
		mu 0 4 713 711 788 789
		f 4 1147 1187 1356 -1185
		mu 0 4 711 752 790 788
		f 4 1142 1248 1354 -1188
		mu 0 4 752 754 791 790
		f 4 -1150 1185 1363 -1190
		mu 0 4 757 716 792 793
		f 4 -1167 1192 1344 1343
		mu 0 4 755 753 794 795
		f 4 1163 1195 1342 -1193
		mu 0 4 753 773 796 794
		f 4 1174 1282 1340 -1196
		mu 0 4 773 780 797 796
		f 4 -1166 1193 1334 -1198
		mu 0 4 778 758 798 799
		f 4 -1202 -1346 1347 -1194
		mu 0 4 758 800 801 798
		f 4 -1204 -1205 1201 -1142
		mu 0 4 757 802 800 758
		f 4 -1207 1203 1189 1350
		mu 0 4 803 802 757 793
		f 4 -1208 -1209 1205 -1192
		mu 0 4 804 805 806 807
		f 4 -1210 -1360 1362 -1186
		mu 0 4 716 808 809 792
		f 4 -1212 -1213 1209 -1122
		mu 0 4 715 810 808 716
		f 4 -1216 -1119 1116 1146
		mu 0 4 811 812 714 717
		f 4 -1218 -1147 1144 1138
		mu 0 4 813 811 717 756
		f 4 -1220 -1139 1136 1162
		mu 0 4 814 813 756 759
		f 4 -1222 -1163 1160 1170
		mu 0 4 815 814 759 779
		f 4 -1224 -1171 1168 1130
		mu 0 4 816 815 779 744
		f 4 -1226 -1131 1128 1154
		mu 0 4 817 816 744 747
		f 4 -1228 -1155 1152 1124
		mu 0 4 818 817 747 770
		f 4 2522 2521 2880 -2520
		mu 0 4 819 820 821 822
		f 4 2526 2525 2876 -2524
		mu 0 4 823 824 825 826
		f 4 -1230 -1231 1228 -1158
		mu 0 4 746 827 828 769
		f 4 -1233 1229 1181 1368
		mu 0 4 829 827 746 787
		f 4 -1234 -1235 1231 -1184
		mu 0 4 830 831 832 833
		f 4 -1236 -1378 1379 -1178
		mu 0 4 745 834 835 786
		f 4 -1238 -1239 1235 -1174
		mu 0 4 778 836 834 745
		f 4 1336 -1241 1237 1197
		mu 0 4 799 837 836 778
		f 4 -1201 -1242 1239 -1200
		mu 0 4 838 839 840 841
		f 4 -1244 -1344 1346 1345
		mu 0 4 800 755 795 801
		f 4 -1246 -1247 1243 1204
		mu 0 4 802 754 755 800
		f 4 1352 -1249 1245 1206
		mu 0 4 803 791 754 802
		f 4 -3125 -3127 3128 3129
		mu 0 4 842 843 844 845
		f 4 -1252 -1358 1360 1359
		mu 0 4 808 713 789 809
		f 4 -1254 -1255 1251 1212
		mu 0 4 810 712 713 808
		f 4 -1258 -1214 1215 1214
		mu 0 4 708 707 812 811
		f 4 -1260 -1215 1217 1216
		mu 0 4 749 708 811 813
		f 4 -1262 -1217 1219 1218
		mu 0 4 750 749 813 814
		f 4 -3045 -3047 3048 3049
		mu 0 4 846 847 848 849
		f 4 -1266 -1221 1223 1222
		mu 0 4 735 771 815 816
		f 4 -1268 -1223 1225 1224
		mu 0 4 736 735 816 817
		f 4 -1270 -1225 1227 1226
		mu 0 4 764 736 817 818
		f 4 2520 2519 2882 -2518
		mu 0 4 850 851 852 853
		f 4 -1272 -1273 1270 1230
		mu 0 4 827 767 768 828
		f 4 -1275 1271 1232 1370
		mu 0 4 785 767 827 829
		f 4 -3133 -3135 3136 3137
		mu 0 4 854 855 856 857
		f 4 -1278 -1376 1378 1377
		mu 0 4 834 781 783 835
		f 4 -1280 -1281 1277 1238
		mu 0 4 836 780 781 834
		f 4 -1283 1279 1240 1338
		mu 0 4 797 780 836 837
		f 4 3140 -3143 -3145 3145
		mu 0 4 858 859 860 861
		f 4 -1287 1284 -1191 -1286
		mu 0 4 862 863 763 762
		f 4 -1289 1285 -1189 -1288
		mu 0 4 864 862 762 761
		f 4 -1291 1287 1186 1252
		mu 0 4 865 864 761 760
		f 4 -1293 -1253 1249 1210
		mu 0 4 866 865 760 805
		f 4 -1295 -1211 1207 -1294
		mu 0 4 867 866 805 804
		f 4 -1297 1293 1191 -1296
		mu 0 4 868 867 804 807
		f 4 -1298 -1299 1295 -1206
		mu 0 4 806 869 868 807
		f 4 -1285 -1300 1297 -1248
		mu 0 4 763 863 869 806
		f 4 -1302 -1303 1300 -1282
		mu 0 4 774 870 871 840
		f 4 -1305 1301 -1199 -1304
		mu 0 4 872 870 774 777
		f 4 -1307 1303 -1197 -1306
		mu 0 4 873 872 777 776
		f 4 -1309 1305 1194 1244
		mu 0 4 874 873 776 775
		f 4 -1311 -1245 1242 1202
		mu 0 4 875 874 775 839
		f 4 -1313 -1203 1200 -1312
		mu 0 4 876 875 839 838
		f 4 -1315 1311 1199 -1314
		mu 0 4 877 876 838 841
		f 4 -1301 -1316 1313 -1240
		mu 0 4 840 871 877 841
		f 4 -1318 -1319 1316 -1232
		mu 0 4 832 878 879 833
		f 4 -1320 -1321 1317 -1274
		mu 0 4 743 880 878 832
		f 4 -1323 1319 -1183 -1322
		mu 0 4 881 880 743 742
		f 4 -1325 1321 -1181 -1324
		mu 0 4 882 881 742 741
		f 4 -1327 1323 1178 1278
		mu 0 4 883 882 741 740
		f 4 -1329 -1279 1275 1236
		mu 0 4 884 883 740 831
		f 4 -1331 -1237 1233 -1330
		mu 0 4 885 884 831 830
		f 4 -1332 1329 1183 -1317
		mu 0 4 879 885 830 833
		f 4 2560 2559 2842 -2558
		mu 0 4 886 887 888 889
		f 4 2844 -2556 2558 2557
		mu 0 4 889 890 891 886
		f 4 2482 2481 2920 -2480
		mu 0 4 892 893 894 895
		f 4 2480 2479 2922 -2478
		mu 0 4 896 892 895 897
		f 4 2478 2477 2924 2923
		mu 0 4 898 896 897 899
		f 4 2562 -2838 2840 -2560
		mu 0 4 887 900 901 888
		f 4 -2578 2580 2579 2822
		mu 0 4 902 903 904 905
		f 4 2460 2459 2942 -2458
		mu 0 4 906 907 908 909
		f 4 2458 2457 2944 -2456
		mu 0 4 910 906 909 911
		f 4 2456 2455 2946 2945
		mu 0 4 912 910 911 913
		f 4 2584 -2816 2818 -2582
		mu 0 4 914 915 916 917
		f 4 2582 2581 2820 -2580
		mu 0 4 904 914 917 905
		f 4 2542 2541 2860 -2540
		mu 0 4 918 919 920 921
		f 4 -2538 2540 2539 2862
		mu 0 4 922 923 918 921
		f 4 2500 2499 2902 -2498
		mu 0 4 924 925 926 927
		f 4 2498 2497 2904 -2496
		mu 0 4 928 924 927 929
		f 4 2496 2495 2906 2905
		mu 0 4 930 928 929 931
		f 4 2544 -2856 2858 -2542
		mu 0 4 919 932 933 920
		f 4 -2460 2462 2461 2940
		mu 0 4 908 907 934 935
		f 4 -1353 1382 1462 -1382
		mu 0 4 791 803 936 937
		f 4 2577 2824 2823 2578
		mu 0 4 903 902 938 939
		f 4 1299 1380 1465 -1384
		mu 0 4 869 863 940 941
		f 4 -2482 2484 2483 2918
		mu 0 4 894 893 942 943
		f 4 -1339 1386 1502 -1386
		mu 0 4 797 837 944 945
		f 4 2555 2846 2845 2556
		mu 0 4 891 890 946 947
		f 4 1302 1384 1505 -1388
		mu 0 4 871 870 948 949
		f 4 -1400 1404 -1404 1405
		mu 0 4 950 951 952 953
		f 4 -1391 -1398 -1402 -1405
		mu 0 4 954 955 956 957
		f 4 -1403 -1406 -1396 -1394
		mu 0 4 958 959 960 961
		f 4 -1271 1407 1544 -1407
		mu 0 4 828 768 962 963
		f 4 2511 2890 2889 2512
		mu 0 4 726 729 964 965
		f 4 1403 1409 1540 -1409
		mu 0 4 953 952 966 967
		f 4 -2526 2528 2527 2874
		mu 0 4 825 824 968 969
		f 4 -2830 2832 2831 2570
		mu 0 4 970 971 972 973
		f 4 -1415 1411 1446 -1414
		mu 0 4 974 975 976 977
		f 4 -2468 2470 2469 2932
		mu 0 4 978 979 980 981
		f 4 -1418 1415 1449 -1411
		mu 0 4 982 983 984 985
		f 4 -2826 2828 2827 2574
		mu 0 4 986 987 988 989
		f 4 -1423 1419 1438 -1422
		mu 0 4 990 991 992 993
		f 4 -2464 2466 2465 2936
		mu 0 4 994 995 996 997
		f 4 -1426 1423 1441 -1419
		mu 0 4 998 999 1000 1001
		f 4 -2834 2836 2835 2566
		mu 0 4 1002 1003 1004 1005
		f 4 -1431 1427 1454 -1430
		mu 0 4 1006 1007 1008 1009
		f 4 -2472 2474 2473 2928
		mu 0 4 1010 1011 1012 1013
		f 4 -1434 1431 1457 -1427
		mu 0 4 1014 1015 1016 1017
		f 4 -2828 2830 2829 2572
		mu 0 4 989 988 971 970
		f 4 -1439 1435 1414 -1438
		mu 0 4 993 992 975 974
		f 4 -2466 2468 2467 2934
		mu 0 4 997 996 979 978
		f 4 -1442 1439 1417 -1435
		mu 0 4 1001 1000 983 982
		f 4 -2832 2834 2833 2568
		mu 0 4 973 972 1003 1002
		f 4 -1447 1443 1430 -1446
		mu 0 4 977 976 1007 1006
		f 4 -2470 2472 2471 2930
		mu 0 4 981 980 1011 1010
		f 4 -1450 1447 1433 -1443
		mu 0 4 985 984 1015 1014
		f 4 -2836 2838 2837 2564
		mu 0 4 1005 1004 901 900
		f 4 -1455 1451 -1347 -1454
		mu 0 4 1009 1008 801 795
		f 4 -2474 2476 -2924 2926
		mu 0 4 1013 1012 898 899
		f 4 -1458 1455 1310 -1451
		mu 0 4 1017 1016 874 875
		f 4 -2824 2826 2825 2576
		mu 0 4 939 938 987 986
		f 4 -1463 1459 1422 -1462
		mu 0 4 937 936 991 990
		f 4 -2462 2464 2463 2938
		mu 0 4 935 934 995 994
		f 4 -1466 1463 1425 -1459
		mu 0 4 941 940 999 998
		f 4 -2850 2852 2851 2550
		mu 0 4 1018 1019 1020 1021
		f 4 -1471 1467 1486 -1470
		mu 0 4 1022 1023 1024 1025
		f 4 -2488 2490 2489 2912
		mu 0 4 1026 1027 1028 1029
		f 4 -1474 1471 1489 -1467
		mu 0 4 1030 1031 1032 1033
		f 4 -2854 2856 2855 2546
		mu 0 4 1034 1035 933 932
		f 4 -1479 1475 -1379 -1478
		mu 0 4 1036 1037 835 783
		f 4 -2492 2494 -2906 2908
		mu 0 4 1038 1039 930 931
		f 4 -1482 1479 1328 -1475
		mu 0 4 1040 1041 883 884
		f 4 -2852 2854 2853 2548
		mu 0 4 1021 1020 1035 1034
		f 4 -1487 1483 1478 -1486
		mu 0 4 1025 1024 1037 1036
		f 4 -2490 2492 2491 2910
		mu 0 4 1029 1028 1039 1038
		f 4 -1490 1487 1481 -1483
		mu 0 4 1033 1032 1041 1040
		f 4 -2848 2850 2849 2552
		mu 0 4 1042 1043 1019 1018
		f 4 -1495 1491 1470 -1494
		mu 0 4 1044 1045 1023 1022
		f 4 -2486 2488 2487 2914
		mu 0 4 1046 1047 1027 1026
		f 4 -1498 1495 1473 -1491
		mu 0 4 1048 1049 1031 1030
		f 4 -2846 2848 2847 2554
		mu 0 4 947 946 1043 1042
		f 4 -1503 1499 1494 -1502
		mu 0 4 945 944 1045 1044
		f 4 -2484 2486 2485 2916
		mu 0 4 943 942 1047 1046
		f 4 -1506 1503 1497 -1499
		mu 0 4 949 948 1049 1048
		f 4 -1509 1506 1320 -1508
		mu 0 4 1050 1051 878 880
		f 4 -2898 2900 -2500 2502
		mu 0 4 1052 1053 926 925
		f 4 -1513 1509 -1371 -1512
		mu 0 4 1054 1055 785 829
		f 4 -2536 2538 2537 2864
		mu 0 4 1056 1057 923 922
		f 4 -1517 1514 1508 -1516
		mu 0 4 1058 1059 1051 1050
		f 4 -2896 2898 2897 2504
		mu 0 4 1060 1061 1053 1052
		f 4 -1521 1517 1512 -1520
		mu 0 4 1062 1063 1055 1054
		f 4 -2534 2536 2535 2866
		mu 0 4 1064 1065 1057 1056
		f 4 -1525 1522 1516 -1524
		mu 0 4 1066 1067 1059 1058
		f 4 -2894 2896 2895 2506
		mu 0 4 1068 1069 1061 1060
		f 4 -1529 1525 1520 -1528
		mu 0 4 1070 1071 1063 1062
		f 4 -2532 2534 2533 2868
		mu 0 4 1072 1073 1065 1064
		f 4 -1533 1530 1524 -1532
		mu 0 4 1074 1075 1067 1066
		f 4 -2892 2894 2893 2508
		mu 0 4 1076 1077 1069 1068
		f 4 -1537 1533 1528 -1536
		mu 0 4 1078 1079 1071 1070
		f 4 -2530 2532 2531 2870
		mu 0 4 1080 1081 1073 1072
		f 4 -1541 1538 1532 -1540
		mu 0 4 967 966 1075 1074
		f 4 -2890 2892 2891 2510
		mu 0 4 965 964 1077 1076
		f 4 -1545 1541 1536 -1544
		mu 0 4 963 962 1079 1078
		f 4 -2528 2530 2529 2872
		mu 0 4 969 968 1081 1080
		f 4 2424 2423 2977 -2422
		mu 0 4 1082 1083 1084 1085
		f 4 2430 2429 2972 -2428
		mu 0 4 1086 1087 1088 1089
		f 4 2432 2431 2970 -2430
		mu 0 4 1090 1091 1092 1093
		f 4 2422 2421 2792 -2420
		mu 0 4 1094 1095 1096 1097
		f 4 2420 2419 2794 -2418
		mu 0 4 1098 1099 1100 1101
		f 4 2426 2425 2976 -2424
		mu 0 4 1102 1103 1104 1105
		f 4 2428 2427 2974 -2426
		mu 0 4 1106 1107 1108 1109
		f 4 -1565 1562 -1556 -1564
		mu 0 4 1110 1111 1112 1113
		f 4 -1567 1563 -1558 -1566
		mu 0 4 1114 1115 1116 1117
		f 4 -1569 1565 -1561 -1568
		mu 0 4 1118 1119 1120 1121
		f 4 -1571 1567 -1554 -1570
		mu 0 4 1122 1123 1124 1125
		f 4 -1573 1569 -1552 -1572
		mu 0 4 1126 1127 1128 1129
		f 4 -1575 1571 -1562 -1574
		mu 0 4 1130 1131 1132 1133
		f 4 -1577 1573 -1560 -1576
		mu 0 4 1134 1135 1136 1137
		f 4 -1578 1575 -1549 -1563
		mu 0 4 1138 1139 1140 1141
		f 4 2417 2796 2795 2418
		mu 0 4 1098 1101 1142 1143
		f 4 1568 1580 1661 -1580
		mu 0 4 1119 1118 1144 1145
		f 4 -2432 2434 2433 2968
		mu 0 4 1092 1091 1146 1147
		f 4 1253 1578 1658 -1582
		mu 0 4 712 810 1148 1149
		f 4 -2814 2816 2815 2586
		mu 0 4 1150 1151 916 915
		f 4 -1587 1583 -1361 -1586
		mu 0 4 1152 1153 809 789
		f 4 -2452 2454 -2946 2948
		mu 0 4 1154 1155 912 913
		f 4 -1590 1587 1292 -1583
		mu 0 4 1156 1157 865 866
		f 4 -2812 2814 2813 2588
		mu 0 4 1158 1159 1151 1150
		f 4 -1595 1591 1586 -1594
		mu 0 4 1160 1161 1153 1152
		f 4 -2450 2452 2451 2950
		mu 0 4 1162 1163 1155 1154
		f 4 -1598 1595 1589 -1591
		mu 0 4 1164 1165 1157 1156
		f 4 -2810 2812 2811 2590
		mu 0 4 1166 1167 1159 1158
		f 4 -1603 1599 1594 -1602
		mu 0 4 1168 1169 1161 1160
		f 4 -2448 2450 2449 2952
		mu 0 4 1170 1171 1163 1162
		f 4 -1606 1603 1597 -1599
		mu 0 4 1172 1173 1165 1164
		f 4 -2808 2810 2809 2592
		mu 0 4 1174 1175 1167 1166
		f 4 -1611 1607 1602 -1610
		mu 0 4 1176 1177 1169 1168
		f 4 -2446 2448 2447 2954
		mu 0 4 1178 1179 1171 1170
		f 4 -1614 1611 1605 -1607
		mu 0 4 1180 1181 1173 1172
		f 4 -2806 2808 2807 2594
		mu 0 4 1182 1183 1175 1174
		f 4 -1619 1615 1610 -1618
		mu 0 4 1184 1185 1177 1176
		f 4 -2444 2446 2445 2956
		mu 0 4 1186 1187 1179 1178
		f 4 -1622 1619 1613 -1615
		mu 0 4 1188 1189 1181 1180
		f 4 -2804 2806 2805 2596
		mu 0 4 1190 1191 1183 1182
		f 4 -1627 1623 1618 -1626
		mu 0 4 1192 1193 1185 1184
		f 4 -2442 2444 2443 2958
		mu 0 4 1194 1195 1187 1186
		f 4 -1630 1627 1621 -1623
		mu 0 4 1196 1197 1189 1188
		f 4 -2802 2804 2803 2598
		mu 0 4 1198 1199 1191 1190
		f 4 -1635 1631 1626 -1634
		mu 0 4 1200 1201 1193 1192
		f 4 -2440 2442 2441 2960
		mu 0 4 1202 1203 1195 1194
		f 4 -1638 1635 1629 -1631
		mu 0 4 1204 1205 1197 1196
		f 4 -2800 2802 2801 2600
		mu 0 4 1206 1207 1199 1198
		f 4 -1643 1639 1634 -1642
		mu 0 4 1208 1209 1201 1200
		f 4 -2438 2440 2439 2962
		mu 0 4 1210 1211 1203 1202
		f 4 -1646 1643 1637 -1639
		mu 0 4 1212 1213 1205 1204
		f 4 2601 -2798 2800 2799
		mu 0 4 1206 1214 1215 1207
		f 4 -1651 1647 1642 -1650
		mu 0 4 1216 1217 1209 1208
		f 4 -2436 2438 2437 2964
		mu 0 4 1218 1219 1211 1210
		f 4 -1654 1651 1645 -1647
		mu 0 4 1220 1221 1213 1212
		f 4 -2796 2798 2797 2416
		mu 0 4 1143 1142 1215 1214
		f 4 -1659 1655 1650 -1658
		mu 0 4 1149 1148 1217 1216
		f 4 -2434 2436 2435 2966
		mu 0 4 1147 1146 1219 1218
		f 4 -1662 1659 1653 -1655
		mu 0 4 1145 1144 1221 1220
		f 4 -1664 -1665 1662 -1656
		mu 0 4 1222 1223 1224 1225
		f 4 1556 -1667 1663 -1579
		mu 0 4 1226 1227 1223 1222
		f 4 1211 1554 -1669 -1557
		mu 0 4 1226 1228 1229 1227
		f 4 1123 1546 -1671 -1555
		mu 0 4 1230 1231 1232 1233
		f 4 1118 1547 -1673 -1547
		mu 0 4 1234 1235 1236 1237
		f 4 1213 1558 -1675 -1548
		mu 0 4 1238 1239 1240 1241
		f 4 1255 1549 -1677 -1559
		mu 0 4 1242 1243 1244 1245
		f 4 1120 1550 -1679 -1550
		mu 0 4 1246 1247 1248 1249
		f 4 1122 1552 -1681 -1551
		mu 0 4 1250 1251 1252 1253
		f 4 -1683 -1553 1581 1660
		mu 0 4 1254 1252 1251 1255
		f 4 -1685 -1661 1657 1652
		mu 0 4 1256 1254 1255 1257
		f 4 -1687 -1653 1649 1644
		mu 0 4 1258 1256 1257 1259
		f 4 -1689 -1645 1641 1636
		mu 0 4 1260 1258 1259 1261
		f 4 -1691 -1637 1633 1628
		mu 0 4 1262 1260 1261 1263
		f 4 -1693 -1629 1625 1620
		mu 0 4 1264 1262 1263 1265
		f 4 -1695 -1621 1617 1612
		mu 0 4 1266 1264 1265 1267
		f 4 -1697 -1613 1609 1604
		mu 0 4 1268 1266 1267 1269
		f 4 -1699 -1605 1601 1596
		mu 0 4 1270 1268 1269 1271
		f 4 -1701 -1597 1593 1588
		mu 0 4 1272 1270 1271 1273
		f 4 -1703 -1589 1585 -1702
		mu 0 4 1274 1272 1273 789
		f 4 -1359 1355 -1705 1701
		mu 0 4 789 788 1275 1274
		f 4 -1357 1353 -1707 -1356
		mu 0 4 788 790 1276 1275
		f 4 -1355 1351 -1709 -1354
		mu 0 4 790 791 1277 1276
		f 4 -1711 -1352 1381 1464
		mu 0 4 1278 1277 791 937
		f 4 -1713 -1465 1461 1424
		mu 0 4 1279 1278 937 990
		f 4 -1715 -1425 1421 1440
		mu 0 4 1280 1279 990 993
		f 4 -1717 -1441 1437 1416
		mu 0 4 1281 1280 993 974
		f 4 -1719 -1417 1413 1448
		mu 0 4 1282 1281 974 977
		f 4 -1721 -1449 1445 1432
		mu 0 4 1283 1282 977 1006
		f 4 -1723 -1433 1429 1456
		mu 0 4 1284 1283 1006 1009
		f 4 -1725 -1457 1453 -1724
		mu 0 4 1285 1284 1009 795
		f 4 -1345 1341 -1727 1723
		mu 0 4 795 794 1286 1285
		f 4 -1343 1339 -1729 -1342
		mu 0 4 794 796 1287 1286
		f 4 -1341 1337 -1731 -1340
		mu 0 4 796 797 1288 1287
		f 4 -1733 -1338 1385 1504
		mu 0 4 1289 1288 797 945
		f 4 -1735 -1505 1501 1496
		mu 0 4 1290 1289 945 1044
		f 4 -1737 -1497 1493 1472
		mu 0 4 1291 1290 1044 1022
		f 4 -1739 -1473 1469 1488
		mu 0 4 1292 1291 1022 1025
		f 4 -1741 -1489 1485 1480
		mu 0 4 1293 1292 1025 1036
		f 4 -1743 -1481 1477 -1742
		mu 0 4 1294 1293 1036 783
		f 4 -1377 1373 -1745 1741
		mu 0 4 783 782 1295 1294
		f 4 -1375 1371 -1747 -1374
		mu 0 4 782 784 1296 1295
		f 4 -1373 1369 -1749 -1372
		mu 0 4 784 785 1297 1296
		f 4 -1750 -1751 -1370 -1510
		mu 0 4 1055 1298 1297 785
		f 4 -1752 -1753 1749 -1518
		mu 0 4 1063 1299 1298 1055
		f 4 -1754 -1755 1751 -1526
		mu 0 4 1071 1300 1299 1063
		f 4 -1756 -1757 1753 -1534
		mu 0 4 1079 1301 1300 1071
		f 4 -1758 -1759 1755 -1542
		mu 0 4 962 1302 1301 1079
		f 4 1394 -1761 1757 -1408
		mu 0 4 768 1303 1302 962
		f 4 -1127 1392 -1763 -1395
		mu 0 4 768 766 1304 1303
		f 4 -1126 1391 -1765 -1393
		mu 0 4 766 765 1305 1306
		f 4 -1269 1398 -1767 -1392
		mu 0 4 765 764 1307 1308
		f 4 -1227 1389 -1769 -1399
		mu 0 4 764 818 1309 1310
		f 4 -1125 1388 -1771 -1390
		mu 0 4 818 770 1311 1312
		f 4 -1128 1396 -1773 -1389
		mu 0 4 770 769 1313 1314
		f 4 -1229 1400 -1775 -1397
		mu 0 4 769 828 1315 1316
		f 4 -1777 -1401 1406 1545
		mu 0 4 1317 1315 828 963
		f 4 -1779 -1546 1543 1537
		mu 0 4 1318 1317 963 1078
		f 4 -1781 -1538 1535 1529
		mu 0 4 1319 1318 1078 1070
		f 4 -1783 -1530 1527 1521
		mu 0 4 1320 1319 1070 1062
		f 4 -1785 -1522 1519 1513
		mu 0 4 1321 1320 1062 1054
		f 4 -1787 -1514 1511 1367
		mu 0 4 1322 1321 1054 829
		f 4 -1789 -1368 -1369 1365
		mu 0 4 1323 1322 829 787
		f 4 -1367 1364 -1791 -1366
		mu 0 4 787 786 1324 1323
		f 4 -1380 -1792 -1793 -1365
		mu 0 4 786 835 1325 1324
		f 4 -1794 -1795 1791 -1476
		mu 0 4 1037 1326 1325 835
		f 4 -1796 -1797 1793 -1484
		mu 0 4 1024 1327 1326 1037
		f 4 -1798 -1799 1795 -1468
		mu 0 4 1023 1328 1327 1024
		f 4 -1800 -1801 1797 -1492
		mu 0 4 1045 1329 1328 1023
		f 4 -1802 -1803 1799 -1500
		mu 0 4 944 1330 1329 1045
		f 4 1335 -1805 1801 -1387
		mu 0 4 837 1331 1330 944
		f 4 -1807 -1336 -1337 1333
		mu 0 4 1332 1331 837 799
		f 4 -1335 1332 -1809 -1334
		mu 0 4 799 798 1333 1332
		f 4 -1348 -1810 -1811 -1333
		mu 0 4 798 801 1334 1333
		f 4 -1812 -1813 1809 -1452
		mu 0 4 1008 1335 1334 801
		f 4 -1814 -1815 1811 -1428
		mu 0 4 1007 1336 1335 1008
		f 4 -1816 -1817 1813 -1444
		mu 0 4 976 1337 1336 1007
		f 4 -1818 -1819 1815 -1412
		mu 0 4 975 1338 1337 976
		f 4 -1820 -1821 1817 -1436
		mu 0 4 992 1339 1338 975
		f 4 -1822 -1823 1819 -1420
		mu 0 4 991 1340 1339 992
		f 4 -1824 -1825 1821 -1460
		mu 0 4 936 1341 1340 991
		f 4 1349 -1827 1823 -1383
		mu 0 4 803 1342 1341 936
		f 4 -1829 -1350 -1351 1348
		mu 0 4 1343 1342 803 793
		f 4 -1364 1361 -1831 -1349
		mu 0 4 793 792 1344 1343
		f 4 -1363 -1832 -1833 -1362
		mu 0 4 792 809 1345 1344
		f 4 -1834 -1835 1831 -1584
		mu 0 4 1346 1347 1345 809
		f 4 -1836 -1837 1833 -1592
		mu 0 4 1348 1349 1347 1346
		f 4 -1838 -1839 1835 -1600
		mu 0 4 1350 1351 1349 1348
		f 4 -1840 -1841 1837 -1608
		mu 0 4 1352 1353 1351 1350
		f 4 -1842 -1843 1839 -1616
		mu 0 4 1354 1355 1353 1352
		f 4 -1844 -1845 1841 -1624
		mu 0 4 1356 1357 1355 1354
		f 4 -1846 -1847 1843 -1632
		mu 0 4 1358 1359 1357 1356
		f 4 -1848 -1849 1845 -1640
		mu 0 4 1360 1361 1359 1358
		f 4 -1663 -1850 1847 -1648
		mu 0 4 1225 1224 1361 1360
		f 4 -1852 -1853 1850 1664
		mu 0 4 1223 1362 1363 1224
		f 4 1665 -1855 1851 1666
		mu 0 4 1227 1364 1362 1223
		f 4 1668 1667 -1857 -1666
		mu 0 4 1227 1229 1365 1364
		f 4 1670 1669 -1859 -1668
		mu 0 4 1233 1232 1366 1367
		f 4 1672 1671 -1861 -1670
		mu 0 4 1237 1236 1368 1369
		f 4 1674 1673 -1863 -1672
		mu 0 4 1241 1240 1370 1371
		f 4 1676 1675 -1865 -1674
		mu 0 4 1245 1244 1372 1373
		f 4 1678 1677 -1867 -1676
		mu 0 4 1249 1248 1374 1375
		f 4 1680 1679 -1869 -1678
		mu 0 4 1253 1252 1376 1377
		f 4 -1871 -1680 1682 1681
		mu 0 4 1378 1376 1252 1254
		f 4 -1873 -1682 1684 1683
		mu 0 4 1379 1378 1254 1256
		f 4 -1875 -1684 1686 1685
		mu 0 4 1380 1379 1256 1258
		f 4 -1877 -1686 1688 1687
		mu 0 4 1381 1380 1258 1260
		f 4 -1879 -1688 1690 1689
		mu 0 4 1382 1381 1260 1262
		f 4 -1881 -1690 1692 1691
		mu 0 4 1383 1382 1262 1264
		f 4 -1883 -1692 1694 1693
		mu 0 4 1384 1383 1264 1266
		f 4 -1885 -1694 1696 1695
		mu 0 4 1385 1384 1266 1268
		f 4 -1887 -1696 1698 1697
		mu 0 4 1386 1385 1268 1270
		f 4 -1889 -1698 1700 1699
		mu 0 4 1387 1386 1270 1272
		f 4 -1891 -1700 1702 -1890
		mu 0 4 1388 1387 1272 1274
		f 4 1704 1703 -1893 1889
		mu 0 4 1274 1275 1389 1388
		f 4 1706 1705 -1895 -1704
		mu 0 4 1275 1276 1390 1389
		f 4 1708 1707 -1897 -1706
		mu 0 4 1276 1277 1391 1390
		f 4 -1899 -1708 1710 1709
		mu 0 4 1392 1391 1277 1278
		f 4 -1901 -1710 1712 1711
		mu 0 4 1393 1392 1278 1279
		f 4 -1903 -1712 1714 1713
		mu 0 4 1394 1393 1279 1280
		f 4 -1905 -1714 1716 1715
		mu 0 4 1395 1394 1280 1281
		f 4 -1907 -1716 1718 1717
		mu 0 4 1396 1395 1281 1282
		f 4 -1909 -1718 1720 1719
		mu 0 4 1397 1396 1282 1283
		f 4 -1911 -1720 1722 1721
		mu 0 4 1398 1397 1283 1284
		f 4 -1913 -1722 1724 -1912
		mu 0 4 1399 1398 1284 1285
		f 4 1726 1725 -1915 1911
		mu 0 4 1285 1286 1400 1399
		f 4 1728 1727 -1917 -1726
		mu 0 4 1286 1287 1401 1400
		f 4 1730 1729 -1919 -1728
		mu 0 4 1287 1288 1402 1401
		f 4 -1921 -1730 1732 1731
		mu 0 4 1403 1402 1288 1289
		f 4 -1923 -1732 1734 1733
		mu 0 4 1404 1403 1289 1290
		f 4 -1925 -1734 1736 1735
		mu 0 4 1405 1404 1290 1291
		f 4 -1927 -1736 1738 1737
		mu 0 4 1406 1405 1291 1292
		f 4 -1929 -1738 1740 1739
		mu 0 4 1407 1406 1292 1293
		f 4 -1931 -1740 1742 -1930
		mu 0 4 1408 1407 1293 1294
		f 4 1744 1743 -1933 1929
		mu 0 4 1294 1295 1409 1408
		f 4 1746 1745 -1935 -1744
		mu 0 4 1295 1296 1410 1409
		f 4 1748 1747 -1937 -1746
		mu 0 4 1296 1297 1411 1410
		f 4 -1938 -1939 -1748 1750
		mu 0 4 1298 1412 1411 1297
		f 4 -1940 -1941 1937 1752
		mu 0 4 1299 1413 1412 1298
		f 4 -1942 -1943 1939 1754
		mu 0 4 1300 1414 1413 1299
		f 4 -1944 -1945 1941 1756
		mu 0 4 1301 1415 1414 1300
		f 4 -1946 -1947 1943 1758
		mu 0 4 1302 1416 1415 1301
		f 4 1759 -1949 1945 1760
		mu 0 4 1303 1417 1416 1302
		f 4 1762 1761 -1951 -1760
		mu 0 4 1303 1304 1418 1417
		f 4 1764 1763 -1953 -1762
		mu 0 4 1306 1305 1419 1420
		f 4 1766 1765 -1955 -1764
		mu 0 4 1308 1307 1421 1422
		f 4 1768 1767 -1957 -1766
		mu 0 4 1310 1309 1423 1424
		f 4 1770 1769 -1959 -1768
		mu 0 4 1312 1311 1425 1426
		f 4 1772 1771 -1961 -1770
		mu 0 4 1314 1313 1427 1428
		f 4 1774 1773 -1963 -1772
		mu 0 4 1316 1315 1429 1430
		f 4 -1965 -1774 1776 1775
		mu 0 4 1431 1429 1315 1317
		f 4 -1967 -1776 1778 1777
		mu 0 4 1432 1431 1317 1318
		f 4 -1969 -1778 1780 1779
		mu 0 4 1433 1432 1318 1319
		f 4 -1971 -1780 1782 1781
		mu 0 4 1434 1433 1319 1320
		f 4 -1973 -1782 1784 1783
		mu 0 4 1435 1434 1320 1321
		f 4 -1975 -1784 1786 1785
		mu 0 4 1436 1435 1321 1322
		f 4 -1977 -1786 1788 1787
		mu 0 4 1437 1436 1322 1323
		f 4 1790 1789 -1979 -1788
		mu 0 4 1323 1324 1438 1437;
	setAttr ".fc[1000:1499]"
		f 4 1792 -1980 -1981 -1790
		mu 0 4 1324 1325 1439 1438
		f 4 -1982 -1983 1979 1794
		mu 0 4 1326 1440 1439 1325
		f 4 -1984 -1985 1981 1796
		mu 0 4 1327 1441 1440 1326
		f 4 -1986 -1987 1983 1798
		mu 0 4 1328 1442 1441 1327
		f 4 -1988 -1989 1985 1800
		mu 0 4 1329 1443 1442 1328
		f 4 -1990 -1991 1987 1802
		mu 0 4 1330 1444 1443 1329
		f 4 1803 -1993 1989 1804
		mu 0 4 1331 1445 1444 1330
		f 4 -1995 -1804 1806 1805
		mu 0 4 1446 1445 1331 1332
		f 4 1808 1807 -1997 -1806
		mu 0 4 1332 1333 1447 1446
		f 4 1810 -1998 -1999 -1808
		mu 0 4 1333 1334 1448 1447
		f 4 -2000 -2001 1997 1812
		mu 0 4 1335 1449 1448 1334
		f 4 -2002 -2003 1999 1814
		mu 0 4 1336 1450 1449 1335
		f 4 -2004 -2005 2001 1816
		mu 0 4 1337 1451 1450 1336
		f 4 -2006 -2007 2003 1818
		mu 0 4 1338 1452 1451 1337
		f 4 -2008 -2009 2005 1820
		mu 0 4 1339 1453 1452 1338
		f 4 -2010 -2011 2007 1822
		mu 0 4 1340 1454 1453 1339
		f 4 -2012 -2013 2009 1824
		mu 0 4 1341 1455 1454 1340
		f 4 1825 -2015 2011 1826
		mu 0 4 1342 1456 1455 1341
		f 4 -2017 -1826 1828 1827
		mu 0 4 1457 1456 1342 1343
		f 4 1830 1829 -2019 -1828
		mu 0 4 1343 1344 1458 1457
		f 4 1832 -2020 -2021 -1830
		mu 0 4 1344 1345 1459 1458
		f 4 -2022 -2023 2019 1834
		mu 0 4 1347 1460 1459 1345
		f 4 -2024 -2025 2021 1836
		mu 0 4 1349 1461 1460 1347
		f 4 -2026 -2027 2023 1838
		mu 0 4 1351 1462 1461 1349
		f 4 -2028 -2029 2025 1840
		mu 0 4 1353 1463 1462 1351
		f 4 -2030 -2031 2027 1842
		mu 0 4 1355 1464 1463 1353
		f 4 -2032 -2033 2029 1844
		mu 0 4 1357 1465 1464 1355
		f 4 -2034 -2035 2031 1846
		mu 0 4 1359 1466 1465 1357
		f 4 -2036 -2037 2033 1848
		mu 0 4 1361 1467 1466 1359
		f 4 1849 -1851 -2038 2035
		mu 0 4 1361 1224 1363 1467
		f 4 -2040 -2041 2038 1852
		mu 0 4 1362 1468 1469 1363
		f 4 1853 -2043 2039 1854
		mu 0 4 1364 1470 1468 1362
		f 4 1856 1855 -2045 -1854
		mu 0 4 1364 1365 1471 1470
		f 4 1858 1857 -2047 -1856
		mu 0 4 1367 1366 1472 1473
		f 4 1860 1859 -2049 -1858
		mu 0 4 1369 1368 1474 1475
		f 4 1862 1861 -2051 -1860
		mu 0 4 1371 1370 1476 1477
		f 4 1864 1863 -2053 -1862
		mu 0 4 1373 1372 1478 1479
		f 4 1866 1865 -2055 -1864
		mu 0 4 1375 1374 1480 1481
		f 4 1868 1867 -2057 -1866
		mu 0 4 1377 1376 1482 1483
		f 4 -2059 -1868 1870 1869
		mu 0 4 1484 1482 1376 1378
		f 4 -2061 -1870 1872 1871
		mu 0 4 1485 1484 1378 1379
		f 4 -2063 -1872 1874 1873
		mu 0 4 1486 1485 1379 1380
		f 4 -2065 -1874 1876 1875
		mu 0 4 1487 1486 1380 1381
		f 4 -2067 -1876 1878 1877
		mu 0 4 1488 1487 1381 1382
		f 4 -2069 -1878 1880 1879
		mu 0 4 1489 1488 1382 1383
		f 4 -2071 -1880 1882 1881
		mu 0 4 1490 1489 1383 1384
		f 4 -2073 -1882 1884 1883
		mu 0 4 1491 1490 1384 1385
		f 4 -2075 -1884 1886 1885
		mu 0 4 1492 1491 1385 1386
		f 4 -2077 -1886 1888 1887
		mu 0 4 1493 1492 1386 1387
		f 4 -2079 -1888 1890 -2078
		mu 0 4 1494 1493 1387 1388
		f 4 1892 1891 -2081 2077
		mu 0 4 1388 1389 1495 1494
		f 4 1894 1893 -2083 -1892
		mu 0 4 1389 1390 1496 1495
		f 4 1896 1895 -2085 -1894
		mu 0 4 1390 1391 1497 1496
		f 4 -2087 -1896 1898 1897
		mu 0 4 1498 1497 1391 1392
		f 4 -2089 -1898 1900 1899
		mu 0 4 1499 1498 1392 1393
		f 4 -2091 -1900 1902 1901
		mu 0 4 1500 1499 1393 1394
		f 4 -2093 -1902 1904 1903
		mu 0 4 1501 1500 1394 1395
		f 4 -2095 -1904 1906 1905
		mu 0 4 1502 1501 1395 1396
		f 4 -2097 -1906 1908 1907
		mu 0 4 1503 1502 1396 1397
		f 4 -2099 -1908 1910 1909
		mu 0 4 1504 1503 1397 1398
		f 4 -2101 -1910 1912 -2100
		mu 0 4 1505 1504 1398 1399
		f 4 1914 1913 -2103 2099
		mu 0 4 1399 1400 1506 1505
		f 4 1916 1915 -2105 -1914
		mu 0 4 1400 1401 1507 1506
		f 4 1918 1917 -2107 -1916
		mu 0 4 1401 1402 1508 1507
		f 4 -2109 -1918 1920 1919
		mu 0 4 1509 1508 1402 1403
		f 4 -2111 -1920 1922 1921
		mu 0 4 1510 1509 1403 1404
		f 4 -2113 -1922 1924 1923
		mu 0 4 1511 1510 1404 1405
		f 4 -2115 -1924 1926 1925
		mu 0 4 1512 1511 1405 1406
		f 4 -2117 -1926 1928 1927
		mu 0 4 1513 1512 1406 1407
		f 4 -2119 -1928 1930 -2118
		mu 0 4 1514 1513 1407 1408
		f 4 1932 1931 -2121 2117
		mu 0 4 1408 1409 1515 1514
		f 4 1934 1933 -2123 -1932
		mu 0 4 1409 1410 1516 1515
		f 4 1936 1935 -2125 -1934
		mu 0 4 1410 1411 1517 1516
		f 4 -2126 -2127 -1936 1938
		mu 0 4 1412 1518 1517 1411
		f 4 -2128 -2129 2125 1940
		mu 0 4 1413 1519 1518 1412
		f 4 -2130 -2131 2127 1942
		mu 0 4 1414 1520 1519 1413
		f 4 -2132 -2133 2129 1944
		mu 0 4 1415 1521 1520 1414
		f 4 -2134 -2135 2131 1946
		mu 0 4 1416 1522 1521 1415
		f 4 1947 -2137 2133 1948
		mu 0 4 1417 1523 1522 1416
		f 4 1950 1949 -2139 -1948
		mu 0 4 1417 1418 1524 1523
		f 4 1952 1951 -2141 -1950
		mu 0 4 1420 1419 1525 1526
		f 4 1954 1953 -2143 -1952
		mu 0 4 1422 1421 1527 1528
		f 4 1956 1955 -2145 -1954
		mu 0 4 1424 1423 1529 1530
		f 4 1958 1957 -2147 -1956
		mu 0 4 1426 1425 1531 1532
		f 4 1960 1959 -2149 -1958
		mu 0 4 1428 1427 1533 1534
		f 4 1962 1961 -2151 -1960
		mu 0 4 1430 1429 1535 1536
		f 4 -2153 -1962 1964 1963
		mu 0 4 1537 1535 1429 1431
		f 4 -2155 -1964 1966 1965
		mu 0 4 1538 1537 1431 1432
		f 4 -2157 -1966 1968 1967
		mu 0 4 1539 1538 1432 1433
		f 4 -2159 -1968 1970 1969
		mu 0 4 1540 1539 1433 1434
		f 4 -2161 -1970 1972 1971
		mu 0 4 1541 1540 1434 1435
		f 4 -2163 -1972 1974 1973
		mu 0 4 1542 1541 1435 1436
		f 4 -2165 -1974 1976 1975
		mu 0 4 1543 1542 1436 1437
		f 4 1978 1977 -2167 -1976
		mu 0 4 1437 1438 1544 1543
		f 4 1980 -2168 -2169 -1978
		mu 0 4 1438 1439 1545 1544
		f 4 -2170 -2171 2167 1982
		mu 0 4 1440 1546 1545 1439
		f 4 -2172 -2173 2169 1984
		mu 0 4 1441 1547 1546 1440
		f 4 -2174 -2175 2171 1986
		mu 0 4 1442 1548 1547 1441
		f 4 -2176 -2177 2173 1988
		mu 0 4 1443 1549 1548 1442
		f 4 -2178 -2179 2175 1990
		mu 0 4 1444 1550 1549 1443
		f 4 1991 -2181 2177 1992
		mu 0 4 1445 1551 1550 1444
		f 4 -2183 -1992 1994 1993
		mu 0 4 1552 1551 1445 1446
		f 4 1996 1995 -2185 -1994
		mu 0 4 1446 1447 1553 1552
		f 4 1998 -2186 -2187 -1996
		mu 0 4 1447 1448 1554 1553
		f 4 -2188 -2189 2185 2000
		mu 0 4 1449 1555 1554 1448
		f 4 -2190 -2191 2187 2002
		mu 0 4 1450 1556 1555 1449
		f 4 -2192 -2193 2189 2004
		mu 0 4 1451 1557 1556 1450
		f 4 -2194 -2195 2191 2006
		mu 0 4 1452 1558 1557 1451
		f 4 -2196 -2197 2193 2008
		mu 0 4 1453 1559 1558 1452
		f 4 -2198 -2199 2195 2010
		mu 0 4 1454 1560 1559 1453
		f 4 -2200 -2201 2197 2012
		mu 0 4 1455 1561 1560 1454
		f 4 2013 -2203 2199 2014
		mu 0 4 1456 1562 1561 1455
		f 4 -2205 -2014 2016 2015
		mu 0 4 1563 1562 1456 1457
		f 4 2018 2017 -2207 -2016
		mu 0 4 1457 1458 1564 1563
		f 4 2020 -2208 -2209 -2018
		mu 0 4 1458 1459 1565 1564
		f 4 -2210 -2211 2207 2022
		mu 0 4 1460 1566 1565 1459
		f 4 -2212 -2213 2209 2024
		mu 0 4 1461 1567 1566 1460
		f 4 -2214 -2215 2211 2026
		mu 0 4 1462 1568 1567 1461
		f 4 -2216 -2217 2213 2028
		mu 0 4 1463 1569 1568 1462
		f 4 -2218 -2219 2215 2030
		mu 0 4 1464 1570 1569 1463
		f 4 -2220 -2221 2217 2032
		mu 0 4 1465 1571 1570 1464
		f 4 -2222 -2223 2219 2034
		mu 0 4 1466 1572 1571 1465
		f 4 -2224 -2225 2221 2036
		mu 0 4 1467 1573 1572 1466
		f 4 2037 -2039 -2226 2223
		mu 0 4 1467 1363 1469 1573
		f 4 -2228 -2229 2226 2040
		mu 0 4 1468 1574 1575 1469
		f 4 2041 -2231 2227 2042
		mu 0 4 1470 1576 1574 1468
		f 4 2044 2043 -2233 -2042
		mu 0 4 1470 1471 1577 1576
		f 4 2046 2045 -2235 -2044
		mu 0 4 1473 1472 1578 1579
		f 4 2048 2047 -2237 -2046
		mu 0 4 1475 1474 1580 1581
		f 4 2050 2049 -2239 -2048
		mu 0 4 1477 1476 1582 1583
		f 4 2052 2051 -2241 -2050
		mu 0 4 1479 1478 1584 1585
		f 4 2054 2053 -2243 -2052
		mu 0 4 1481 1480 1586 1587
		f 4 2056 2055 -2245 -2054
		mu 0 4 1483 1482 1588 1589
		f 4 -2247 -2056 2058 2057
		mu 0 4 1590 1588 1482 1484
		f 4 -2249 -2058 2060 2059
		mu 0 4 1591 1590 1484 1485
		f 4 -2251 -2060 2062 2061
		mu 0 4 1592 1591 1485 1486
		f 4 -2253 -2062 2064 2063
		mu 0 4 1593 1592 1486 1487
		f 4 -2255 -2064 2066 2065
		mu 0 4 1594 1593 1487 1488
		f 4 -2257 -2066 2068 2067
		mu 0 4 1595 1594 1488 1489
		f 4 -2259 -2068 2070 2069
		mu 0 4 1596 1595 1489 1490
		f 4 -2261 -2070 2072 2071
		mu 0 4 1597 1596 1490 1491
		f 4 -2263 -2072 2074 2073
		mu 0 4 1598 1597 1491 1492
		f 4 -2265 -2074 2076 2075
		mu 0 4 1599 1598 1492 1493
		f 4 -2267 -2076 2078 -2266
		mu 0 4 1600 1599 1493 1494
		f 4 2080 2079 -2269 2265
		mu 0 4 1494 1495 1601 1600
		f 4 2082 2081 -2271 -2080
		mu 0 4 1495 1496 1602 1601
		f 4 2084 2083 -2273 -2082
		mu 0 4 1496 1497 1603 1602
		f 4 -2275 -2084 2086 2085
		mu 0 4 1604 1603 1497 1498
		f 4 -2277 -2086 2088 2087
		mu 0 4 1605 1604 1498 1499
		f 4 -2279 -2088 2090 2089
		mu 0 4 1606 1605 1499 1500
		f 4 -2281 -2090 2092 2091
		mu 0 4 1607 1606 1500 1501
		f 4 -2283 -2092 2094 2093
		mu 0 4 1608 1607 1501 1502
		f 4 -2285 -2094 2096 2095
		mu 0 4 1609 1608 1502 1503
		f 4 -2287 -2096 2098 2097
		mu 0 4 1610 1609 1503 1504
		f 4 -2289 -2098 2100 -2288
		mu 0 4 1611 1610 1504 1505
		f 4 2102 2101 -2291 2287
		mu 0 4 1505 1506 1612 1611
		f 4 2104 2103 -2293 -2102
		mu 0 4 1506 1507 1613 1612
		f 4 2106 2105 -2295 -2104
		mu 0 4 1507 1508 1614 1613
		f 4 -2297 -2106 2108 2107
		mu 0 4 1615 1614 1508 1509
		f 4 -2299 -2108 2110 2109
		mu 0 4 1616 1615 1509 1510
		f 4 -2301 -2110 2112 2111
		mu 0 4 1617 1616 1510 1511
		f 4 -2303 -2112 2114 2113
		mu 0 4 1618 1617 1511 1512
		f 4 -2305 -2114 2116 2115
		mu 0 4 1619 1618 1512 1513
		f 4 -2307 -2116 2118 -2306
		mu 0 4 1620 1619 1513 1514
		f 4 2120 2119 -2309 2305
		mu 0 4 1514 1515 1621 1620
		f 4 2122 2121 -2311 -2120
		mu 0 4 1515 1516 1622 1621
		f 4 2124 2123 -2313 -2122
		mu 0 4 1516 1517 1623 1622
		f 4 -2314 -2315 -2124 2126
		mu 0 4 1518 1624 1623 1517
		f 4 -2316 -2317 2313 2128
		mu 0 4 1519 1625 1624 1518
		f 4 -2318 -2319 2315 2130
		mu 0 4 1520 1626 1625 1519
		f 4 -2320 -2321 2317 2132
		mu 0 4 1521 1627 1626 1520
		f 4 -2322 -2323 2319 2134
		mu 0 4 1522 1628 1627 1521
		f 4 2135 -2325 2321 2136
		mu 0 4 1523 1629 1628 1522
		f 4 2138 2137 -2327 -2136
		mu 0 4 1523 1524 1630 1629
		f 4 2140 2139 -2329 -2138
		mu 0 4 1526 1525 1631 1632
		f 4 2142 2141 -2331 -2140
		mu 0 4 1528 1527 1633 1634
		f 4 2144 2143 -2333 -2142
		mu 0 4 1530 1529 1635 1636
		f 4 2146 2145 -2335 -2144
		mu 0 4 1532 1531 1637 1638
		f 4 2148 2147 -2337 -2146
		mu 0 4 1534 1533 1639 1640
		f 4 2150 2149 -2339 -2148
		mu 0 4 1536 1535 1641 1642
		f 4 -2341 -2150 2152 2151
		mu 0 4 1643 1641 1535 1537
		f 4 -2343 -2152 2154 2153
		mu 0 4 1644 1643 1537 1538
		f 4 -2345 -2154 2156 2155
		mu 0 4 1645 1644 1538 1539
		f 4 -2347 -2156 2158 2157
		mu 0 4 1646 1645 1539 1540
		f 4 -2349 -2158 2160 2159
		mu 0 4 1647 1646 1540 1541
		f 4 -2351 -2160 2162 2161
		mu 0 4 1648 1647 1541 1542
		f 4 -2353 -2162 2164 2163
		mu 0 4 1649 1648 1542 1543
		f 4 2166 2165 -2355 -2164
		mu 0 4 1543 1544 1650 1649
		f 4 2168 -2356 -2357 -2166
		mu 0 4 1544 1545 1651 1650
		f 4 -2358 -2359 2355 2170
		mu 0 4 1546 1652 1651 1545
		f 4 -2360 -2361 2357 2172
		mu 0 4 1547 1653 1652 1546
		f 4 -2362 -2363 2359 2174
		mu 0 4 1548 1654 1653 1547
		f 4 -2364 -2365 2361 2176
		mu 0 4 1549 1655 1654 1548
		f 4 -2366 -2367 2363 2178
		mu 0 4 1550 1656 1655 1549
		f 4 2179 -2369 2365 2180
		mu 0 4 1551 1657 1656 1550
		f 4 -2371 -2180 2182 2181
		mu 0 4 1658 1657 1551 1552
		f 4 2184 2183 -2373 -2182
		mu 0 4 1552 1553 1659 1658
		f 4 2186 -2374 -2375 -2184
		mu 0 4 1553 1554 1660 1659
		f 4 -2376 -2377 2373 2188
		mu 0 4 1555 1661 1660 1554
		f 4 -2378 -2379 2375 2190
		mu 0 4 1556 1662 1661 1555
		f 4 -2380 -2381 2377 2192
		mu 0 4 1557 1663 1662 1556
		f 4 -2382 -2383 2379 2194
		mu 0 4 1558 1664 1663 1557
		f 4 -2384 -2385 2381 2196
		mu 0 4 1559 1665 1664 1558
		f 4 -2386 -2387 2383 2198
		mu 0 4 1560 1666 1665 1559
		f 4 -2388 -2389 2385 2200
		mu 0 4 1561 1667 1666 1560
		f 4 2201 -2391 2387 2202
		mu 0 4 1562 1668 1667 1561
		f 4 -2393 -2202 2204 2203
		mu 0 4 1669 1668 1562 1563
		f 4 2206 2205 -2395 -2204
		mu 0 4 1563 1564 1670 1669
		f 4 2208 -2396 -2397 -2206
		mu 0 4 1564 1565 1671 1670
		f 4 -2398 -2399 2395 2210
		mu 0 4 1566 1672 1671 1565
		f 4 -2400 -2401 2397 2212
		mu 0 4 1567 1673 1672 1566
		f 4 -2402 -2403 2399 2214
		mu 0 4 1568 1674 1673 1567
		f 4 -2404 -2405 2401 2216
		mu 0 4 1569 1675 1674 1568
		f 4 -2406 -2407 2403 2218
		mu 0 4 1570 1676 1675 1569
		f 4 -2408 -2409 2405 2220
		mu 0 4 1571 1677 1676 1570
		f 4 -2410 -2411 2407 2222
		mu 0 4 1572 1678 1677 1571
		f 4 -2412 -2413 2409 2224
		mu 0 4 1573 1679 1678 1572
		f 4 2225 -2227 -2414 2411
		mu 0 4 1573 1469 1575 1679
		f 4 -2416 -2417 2414 2228
		mu 0 4 1574 1143 1214 1575
		f 4 2229 -2419 2415 2230
		mu 0 4 1576 1098 1143 1574
		f 4 2232 2231 -2421 -2230
		mu 0 4 1576 1577 1099 1098
		f 4 2234 2233 -2423 -2232
		mu 0 4 1579 1578 1095 1094
		f 4 2236 2235 -2425 -2234
		mu 0 4 1581 1580 1083 1082
		f 4 2238 2237 -2427 -2236
		mu 0 4 1583 1582 1103 1102
		f 4 2240 2239 -2429 -2238
		mu 0 4 1585 1584 1107 1106
		f 4 2242 2241 -2431 -2240
		mu 0 4 1587 1586 1087 1086
		f 4 2244 2243 -2433 -2242
		mu 0 4 1589 1588 1091 1090
		f 4 -2435 -2244 2246 2245
		mu 0 4 1146 1091 1588 1590
		f 4 -2437 -2246 2248 2247
		mu 0 4 1219 1146 1590 1591
		f 4 -2439 -2248 2250 2249
		mu 0 4 1211 1219 1591 1592
		f 4 -2441 -2250 2252 2251
		mu 0 4 1203 1211 1592 1593
		f 4 -2443 -2252 2254 2253
		mu 0 4 1195 1203 1593 1594
		f 4 -2445 -2254 2256 2255
		mu 0 4 1187 1195 1594 1595
		f 4 -2447 -2256 2258 2257
		mu 0 4 1179 1187 1595 1596
		f 4 -2449 -2258 2260 2259
		mu 0 4 1171 1179 1596 1597
		f 4 -2451 -2260 2262 2261
		mu 0 4 1163 1171 1597 1598
		f 4 -2453 -2262 2264 2263
		mu 0 4 1155 1163 1598 1599
		f 4 -2455 -2264 2266 -2454
		mu 0 4 912 1155 1599 1600
		f 4 2268 2267 -2457 2453
		mu 0 4 1600 1601 910 912
		f 4 2270 2269 -2459 -2268
		mu 0 4 1601 1602 906 910
		f 4 2272 2271 -2461 -2270
		mu 0 4 1602 1603 907 906
		f 4 -2463 -2272 2274 2273
		mu 0 4 934 907 1603 1604
		f 4 -2465 -2274 2276 2275
		mu 0 4 995 934 1604 1605
		f 4 -2467 -2276 2278 2277
		mu 0 4 996 995 1605 1606
		f 4 -2469 -2278 2280 2279
		mu 0 4 979 996 1606 1607
		f 4 -2471 -2280 2282 2281
		mu 0 4 980 979 1607 1608
		f 4 -2473 -2282 2284 2283
		mu 0 4 1011 980 1608 1609
		f 4 -2475 -2284 2286 2285
		mu 0 4 1012 1011 1609 1610
		f 4 -2477 -2286 2288 -2476
		mu 0 4 898 1012 1610 1611
		f 4 2290 2289 -2479 2475
		mu 0 4 1611 1612 896 898
		f 4 2292 2291 -2481 -2290
		mu 0 4 1612 1613 892 896
		f 4 2294 2293 -2483 -2292
		mu 0 4 1613 1614 893 892
		f 4 -2485 -2294 2296 2295
		mu 0 4 942 893 1614 1615
		f 4 -2487 -2296 2298 2297
		mu 0 4 1047 942 1615 1616
		f 4 -2489 -2298 2300 2299
		mu 0 4 1027 1047 1616 1617
		f 4 -2491 -2300 2302 2301
		mu 0 4 1028 1027 1617 1618
		f 4 -2493 -2302 2304 2303
		mu 0 4 1039 1028 1618 1619
		f 4 -2495 -2304 2306 -2494
		mu 0 4 930 1039 1619 1620
		f 4 2308 2307 -2497 2493
		mu 0 4 1620 1621 928 930
		f 4 2310 2309 -2499 -2308
		mu 0 4 1621 1622 924 928
		f 4 2312 2311 -2501 -2310
		mu 0 4 1622 1623 925 924
		f 4 -2502 -2503 -2312 2314
		mu 0 4 1624 1052 925 1623
		f 4 -2504 -2505 2501 2316
		mu 0 4 1625 1060 1052 1624
		f 4 -2506 -2507 2503 2318
		mu 0 4 1626 1068 1060 1625
		f 4 -2508 -2509 2505 2320
		mu 0 4 1627 1076 1068 1626
		f 4 -2510 -2511 2507 2322
		mu 0 4 1628 965 1076 1627
		f 4 2323 -2513 2509 2324
		mu 0 4 1629 726 965 1628
		f 4 2326 2325 -2515 -2324
		mu 0 4 1629 1630 727 726
		f 4 2328 2327 -2517 -2326
		mu 0 4 1632 1631 723 722
		f 4 2330 2329 -2519 -2328
		mu 0 4 1634 1633 719 718
		f 4 2332 2331 -2521 -2330
		mu 0 4 1636 1635 851 850
		f 4 2334 2333 -2523 -2332
		mu 0 4 1638 1637 820 819
		f 4 2336 2335 -2525 -2334
		mu 0 4 1640 1639 731 730
		f 4 2338 2337 -2527 -2336
		mu 0 4 1642 1641 824 823
		f 4 -2529 -2338 2340 2339
		mu 0 4 968 824 1641 1643
		f 4 -2531 -2340 2342 2341
		mu 0 4 1081 968 1643 1644
		f 4 -2533 -2342 2344 2343
		mu 0 4 1073 1081 1644 1645
		f 4 -2535 -2344 2346 2345
		mu 0 4 1065 1073 1645 1646
		f 4 -2537 -2346 2348 2347
		mu 0 4 1057 1065 1646 1647
		f 4 -2539 -2348 2350 2349
		mu 0 4 923 1057 1647 1648
		f 4 -2541 -2350 2352 2351
		mu 0 4 918 923 1648 1649
		f 4 2354 2353 -2543 -2352
		mu 0 4 1649 1650 919 918
		f 4 2356 -2544 -2545 -2354
		mu 0 4 1650 1651 932 919
		f 4 -2546 -2547 2543 2358
		mu 0 4 1652 1034 932 1651
		f 4 -2548 -2549 2545 2360
		mu 0 4 1653 1021 1034 1652
		f 4 -2550 -2551 2547 2362
		mu 0 4 1654 1018 1021 1653
		f 4 -2552 -2553 2549 2364
		mu 0 4 1655 1042 1018 1654
		f 4 -2554 -2555 2551 2366
		mu 0 4 1656 947 1042 1655
		f 4 2367 -2557 2553 2368
		mu 0 4 1657 891 947 1656
		f 4 -2559 -2368 2370 2369
		mu 0 4 886 891 1657 1658
		f 4 2372 2371 -2561 -2370
		mu 0 4 1658 1659 887 886
		f 4 2374 -2562 -2563 -2372
		mu 0 4 1659 1660 900 887
		f 4 -2564 -2565 2561 2376
		mu 0 4 1661 1005 900 1660
		f 4 -2566 -2567 2563 2378
		mu 0 4 1662 1002 1005 1661
		f 4 -2568 -2569 2565 2380
		mu 0 4 1663 973 1002 1662
		f 4 -2570 -2571 2567 2382
		mu 0 4 1664 970 973 1663
		f 4 -2572 -2573 2569 2384
		mu 0 4 1665 989 970 1664
		f 4 -2574 -2575 2571 2386
		mu 0 4 1666 986 989 1665
		f 4 -2576 -2577 2573 2388
		mu 0 4 1667 939 986 1666
		f 4 2389 -2579 2575 2390
		mu 0 4 1668 903 939 1667
		f 4 -2581 -2390 2392 2391
		mu 0 4 904 903 1668 1669
		f 4 2394 2393 -2583 -2392
		mu 0 4 1669 1670 914 904
		f 4 2396 -2584 -2585 -2394
		mu 0 4 1670 1671 915 914
		f 4 -2586 -2587 2583 2398
		mu 0 4 1672 1150 915 1671
		f 4 -2588 -2589 2585 2400
		mu 0 4 1673 1158 1150 1672
		f 4 -2590 -2591 2587 2402
		mu 0 4 1674 1166 1158 1673
		f 4 -2592 -2593 2589 2404
		mu 0 4 1675 1174 1166 1674
		f 4 -2594 -2595 2591 2406
		mu 0 4 1676 1182 1174 1675
		f 4 -2596 -2597 2593 2408
		mu 0 4 1677 1190 1182 1676
		f 4 -2598 -2599 2595 2410
		mu 0 4 1678 1198 1190 1677
		f 4 -2600 -2601 2597 2412
		mu 0 4 1679 1206 1198 1678
		f 4 2413 -2415 -2602 2599
		mu 0 4 1679 1575 1214 1206
		f 4 -2605 2602 1390 -2604
		mu 0 4 1680 1681 1682 1683
		f 4 -2607 2603 1399 -2606
		mu 0 4 1684 1685 1686 1687
		f 4 -2609 2605 1402 -2608
		mu 0 4 1688 1689 1690 1691
		f 4 -2611 2607 1393 -2610
		mu 0 4 1692 1693 1694 1695
		f 4 -2613 2609 1395 -2612
		mu 0 4 1696 1697 1698 1699
		f 4 -2615 2611 1408 1542
		mu 0 4 1700 1696 1699 1701
		f 4 -2617 -1543 1539 1534
		mu 0 4 1702 1700 1701 1703
		f 4 -2619 -1535 1531 1526
		mu 0 4 1704 1702 1703 1705
		f 4 -2621 -1527 1523 1518
		mu 0 4 1706 1704 1705 1707
		f 4 -2623 -1519 1515 1510
		mu 0 4 1708 1706 1707 1709
		f 4 -2625 -1511 1507 -2624
		mu 0 4 1710 1708 1709 880
		f 4 -2627 2623 1322 -2626
		mu 0 4 1711 1710 880 881
		f 4 -2629 2625 1324 -2628
		mu 0 4 1712 1711 881 882
		f 4 -2631 2627 1326 1325
		mu 0 4 1713 1712 882 883
		f 4 -2632 -2633 -1326 -1480
		mu 0 4 1041 1714 1713 883
		f 4 -2634 -2635 2631 -1488
		mu 0 4 1032 1715 1714 1041
		f 4 -2636 -2637 2633 -1472
		mu 0 4 1031 1716 1715 1032
		f 4 -2638 -2639 2635 -1496
		mu 0 4 1049 1717 1716 1031
		f 4 -2640 -2641 2637 -1504
		mu 0 4 948 1718 1717 1049
		f 4 -2642 -2643 2639 -1385
		mu 0 4 870 1719 1718 948
		f 4 -2645 2641 1304 -2644
		mu 0 4 1720 1719 870 872
		f 4 -2647 2643 1306 -2646
		mu 0 4 1721 1720 872 873
		f 4 -2649 2645 1308 1307
		mu 0 4 1722 1721 873 874
		f 4 -2650 -2651 -1308 -1456
		mu 0 4 1016 1723 1722 874
		f 4 -2652 -2653 2649 -1432
		mu 0 4 1015 1724 1723 1016
		f 4 -2654 -2655 2651 -1448
		mu 0 4 984 1725 1724 1015
		f 4 -2656 -2657 2653 -1416
		mu 0 4 983 1726 1725 984
		f 4 -2658 -2659 2655 -1440
		mu 0 4 1000 1727 1726 983
		f 4 -2660 -2661 2657 -1424
		mu 0 4 999 1728 1727 1000
		f 4 -2662 -2663 2659 -1464
		mu 0 4 940 1729 1728 999
		f 4 -2664 -2665 2661 -1381
		mu 0 4 863 1730 1729 940
		f 4 -2667 2663 1286 -2666
		mu 0 4 1731 1730 863 862
		f 4 -2669 2665 1288 -2668
		mu 0 4 1732 1731 862 864
		f 4 -2671 2667 1290 1289
		mu 0 4 1733 1732 864 865
		f 4 -2672 -2673 -1290 -1588
		mu 0 4 1734 1735 1733 865
		f 4 -2674 -2675 2671 -1596
		mu 0 4 1736 1737 1735 1734
		f 4 -2676 -2677 2673 -1604
		mu 0 4 1738 1739 1737 1736
		f 4 -2678 -2679 2675 -1612
		mu 0 4 1740 1741 1739 1738
		f 4 -2680 -2681 2677 -1620
		mu 0 4 1742 1743 1741 1740
		f 4 -2682 -2683 2679 -1628
		mu 0 4 1744 1745 1743 1742
		f 4 -2684 -2685 2681 -1636
		mu 0 4 1746 1747 1745 1744
		f 4 -2686 -2687 2683 -1644
		mu 0 4 1748 1749 1747 1746
		f 4 -2688 -2689 2685 -1652
		mu 0 4 1750 1751 1749 1748
		f 4 -2690 -2691 2687 -1660
		mu 0 4 1752 1753 1751 1750
		f 4 -2692 -2693 2689 -1581
		mu 0 4 1123 1754 1753 1752
		f 4 -2695 2691 1570 -2694
		mu 0 4 1755 1754 1123 1122
		f 4 -2697 2693 1572 -2696
		mu 0 4 1756 1757 1127 1126
		f 4 -2699 2695 1574 -2698
		mu 0 4 1758 1759 1131 1130
		f 4 -2701 2697 1576 -2700
		mu 0 4 1760 1761 1135 1134
		f 4 -2703 2699 1577 -2702
		mu 0 4 1762 1763 1139 1138
		f 4 -2705 2701 1564 -2704
		mu 0 4 1764 1765 1111 1110
		f 4 -2707 2703 1566 -2706
		mu 0 4 1766 1767 1115 1114
		f 4 -2709 2705 1579 1656
		mu 0 4 1768 1766 1114 1769
		f 4 -2711 -1657 1654 1648
		mu 0 4 1770 1768 1769 1771
		f 4 -2713 -1649 1646 1640
		mu 0 4 1772 1770 1771 1773
		f 4 -2715 -1641 1638 1632
		mu 0 4 1774 1772 1773 1775
		f 4 -2717 -1633 1630 1624
		mu 0 4 1776 1774 1775 1777
		f 4 -2719 -1625 1622 1616
		mu 0 4 1778 1776 1777 1779
		f 4 -2721 -1617 1614 1608
		mu 0 4 1780 1778 1779 1781
		f 4 -2723 -1609 1606 1600
		mu 0 4 1782 1780 1781 1783
		f 4 -2725 -1601 1598 1592
		mu 0 4 1784 1782 1783 1785
		f 4 -2727 -1593 1590 1584
		mu 0 4 1786 1784 1785 1787
		f 4 -2729 -1585 1582 1291
		mu 0 4 1788 1786 1787 866
		f 4 -2731 -1292 1294 -2730
		mu 0 4 1789 1788 866 867
		f 4 -2733 2729 1296 -2732
		mu 0 4 1790 1789 867 868
		f 4 -2734 -2735 2731 1298
		mu 0 4 869 1791 1790 868
		f 4 -2737 2733 1383 1460
		mu 0 4 1792 1791 869 941
		f 4 -2739 -1461 1458 1420
		mu 0 4 1793 1792 941 998
		f 4 -2741 -1421 1418 1436
		mu 0 4 1794 1793 998 1001
		f 4 -2743 -1437 1434 1412
		mu 0 4 1795 1794 1001 982
		f 4 -2745 -1413 1410 1444
		mu 0 4 1796 1795 982 985
		f 4 -2747 -1445 1442 1428
		mu 0 4 1797 1796 985 1014
		f 4 -2749 -1429 1426 1452
		mu 0 4 1798 1797 1014 1017
		f 4 -2751 -1453 1450 1309
		mu 0 4 1799 1798 1017 875
		f 4 -2753 -1310 1312 -2752
		mu 0 4 1800 1799 875 876
		f 4 -2755 2751 1314 -2754
		mu 0 4 1801 1800 876 877
		f 4 1315 -2756 -2757 2753
		mu 0 4 877 871 1802 1801
		f 4 -2759 2755 1387 1500
		mu 0 4 1803 1802 871 949
		f 4 -2761 -1501 1498 1492
		mu 0 4 1804 1803 949 1048
		f 4 -2763 -1493 1490 1468
		mu 0 4 1805 1804 1048 1030
		f 4 -2765 -1469 1466 1484
		mu 0 4 1806 1805 1030 1033
		f 4 -2767 -1485 1482 1476
		mu 0 4 1807 1806 1033 1040
		f 4 -2769 -1477 1474 1327
		mu 0 4 1808 1807 1040 884
		f 4 -2771 -1328 1330 -2770
		mu 0 4 1809 1808 884 885
		f 4 -2773 2769 1331 -2772
		mu 0 4 1810 1809 885 879
		f 4 -2774 -2775 2771 1318
		mu 0 4 878 1811 1810 879
		f 4 -2776 -2777 2773 -1507
		mu 0 4 1812 1813 1811 878
		f 4 -2778 -2779 2775 -1515
		mu 0 4 1814 1815 1813 1812
		f 4 -2780 -2781 2777 -1523
		mu 0 4 1816 1817 1815 1814
		f 4 -2782 -2783 2779 -1531
		mu 0 4 1818 1819 1817 1816
		f 4 -2784 -2785 2781 -1539
		mu 0 4 1820 1821 1819 1818
		f 4 -2786 -2787 2783 -1410
		mu 0 4 1822 1823 1821 1820
		f 4 -2789 2785 1401 -2788
		mu 0 4 1824 1823 1822 1825
		f 4 -2790 2787 1397 -2603
		mu 0 4 1826 1827 1828 1829
		f 4 -2793 2790 2704 -2792
		mu 0 4 1097 1096 1765 1764
		f 4 -2795 2791 2706 -2794
		mu 0 4 1101 1100 1767 1766
		f 4 -2797 2793 2708 2707
		mu 0 4 1142 1101 1766 1768
		f 4 -2799 -2708 2710 2709
		mu 0 4 1215 1142 1768 1770
		f 4 -2801 -2710 2712 2711
		mu 0 4 1207 1215 1770 1772
		f 4 -2803 -2712 2714 2713
		mu 0 4 1199 1207 1772 1774
		f 4 -2805 -2714 2716 2715
		mu 0 4 1191 1199 1774 1776
		f 4 -2807 -2716 2718 2717
		mu 0 4 1183 1191 1776 1778
		f 4 -2809 -2718 2720 2719
		mu 0 4 1175 1183 1778 1780
		f 4 -2811 -2720 2722 2721
		mu 0 4 1167 1175 1780 1782
		f 4 -2813 -2722 2724 2723
		mu 0 4 1159 1167 1782 1784
		f 4 -2815 -2724 2726 2725
		mu 0 4 1151 1159 1784 1786
		f 4 -2817 -2726 2728 2727
		mu 0 4 916 1151 1786 1788
		f 4 -2819 -2728 2730 -2818
		mu 0 4 917 916 1788 1789
		f 4 -2821 2817 2732 -2820
		mu 0 4 905 917 1789 1790
		f 4 -2822 -2823 2819 2734
		mu 0 4 1791 902 905 1790
		f 4 -2825 2821 2736 2735
		mu 0 4 938 902 1791 1792
		f 4 -2827 -2736 2738 2737
		mu 0 4 987 938 1792 1793
		f 4 -2829 -2738 2740 2739
		mu 0 4 988 987 1793 1794
		f 4 -2831 -2740 2742 2741
		mu 0 4 971 988 1794 1795
		f 4 -2833 -2742 2744 2743
		mu 0 4 972 971 1795 1796
		f 4 -2835 -2744 2746 2745
		mu 0 4 1003 972 1796 1797
		f 4 -2837 -2746 2748 2747
		mu 0 4 1004 1003 1797 1798
		f 4 -2839 -2748 2750 2749
		mu 0 4 901 1004 1798 1799
		f 4 -2841 -2750 2752 -2840
		mu 0 4 888 901 1799 1800
		f 4 -2843 2839 2754 -2842
		mu 0 4 889 888 1800 1801
		f 4 2756 -2844 -2845 2841
		mu 0 4 1801 1802 890 889
		f 4 -2847 2843 2758 2757
		mu 0 4 946 890 1802 1803
		f 4 -2849 -2758 2760 2759
		mu 0 4 1043 946 1803 1804
		f 4 -2851 -2760 2762 2761
		mu 0 4 1019 1043 1804 1805
		f 4 -2853 -2762 2764 2763
		mu 0 4 1020 1019 1805 1806
		f 4 -2855 -2764 2766 2765
		mu 0 4 1035 1020 1806 1807
		f 4 -2857 -2766 2768 2767
		mu 0 4 933 1035 1807 1808
		f 4 -2859 -2768 2770 -2858
		mu 0 4 920 933 1808 1809
		f 4 -2861 2857 2772 -2860
		mu 0 4 921 920 1809 1810
		f 4 -2862 -2863 2859 2774
		mu 0 4 1811 922 921 1810
		f 4 -2864 -2865 2861 2776
		mu 0 4 1813 1056 922 1811
		f 4 -2866 -2867 2863 2778
		mu 0 4 1815 1064 1056 1813
		f 4 -2868 -2869 2865 2780
		mu 0 4 1817 1072 1064 1815
		f 4 -2870 -2871 2867 2782
		mu 0 4 1819 1080 1072 1817
		f 4 -2872 -2873 2869 2784
		mu 0 4 1821 969 1080 1819
		f 4 -2874 -2875 2871 2786
		mu 0 4 1823 825 969 1821
		f 4 -2877 2873 2788 -2876
		mu 0 4 826 825 1823 1824
		f 4 -2879 2875 2789 -2878
		mu 0 4 733 732 1827 1826
		f 4 -2881 2877 2604 -2880
		mu 0 4 822 821 1681 1680
		f 4 -2883 2879 2606 -2882
		mu 0 4 853 852 1685 1684
		f 4 -2885 2881 2608 -2884
		mu 0 4 721 720 1689 1688
		f 4 -2887 2883 2610 -2886
		mu 0 4 725 724 1693 1692
		f 4 -2889 2885 2612 -2888
		mu 0 4 729 728 1697 1696
		f 4 -2891 2887 2614 2613
		mu 0 4 964 729 1696 1700
		f 4 -2893 -2614 2616 2615
		mu 0 4 1077 964 1700 1702
		f 4 -2895 -2616 2618 2617
		mu 0 4 1069 1077 1702 1704
		f 4 -2897 -2618 2620 2619
		mu 0 4 1061 1069 1704 1706
		f 4 -2899 -2620 2622 2621
		mu 0 4 1053 1061 1706 1708
		f 4 -2901 -2622 2624 -2900
		mu 0 4 926 1053 1708 1710
		f 4 -2903 2899 2626 -2902
		mu 0 4 927 926 1710 1711
		f 4 -2905 2901 2628 -2904
		mu 0 4 929 927 1711 1712
		f 4 -2907 2903 2630 2629
		mu 0 4 931 929 1712 1713
		f 4 -2908 -2909 -2630 2632
		mu 0 4 1714 1038 931 1713
		f 4 -2910 -2911 2907 2634
		mu 0 4 1715 1029 1038 1714
		f 4 -2912 -2913 2909 2636
		mu 0 4 1716 1026 1029 1715
		f 4 -2914 -2915 2911 2638
		mu 0 4 1717 1046 1026 1716
		f 4 -2916 -2917 2913 2640
		mu 0 4 1718 943 1046 1717
		f 4 -2918 -2919 2915 2642
		mu 0 4 1719 894 943 1718
		f 4 -2921 2917 2644 -2920
		mu 0 4 895 894 1719 1720
		f 4 -2923 2919 2646 -2922
		mu 0 4 897 895 1720 1721
		f 4 -2925 2921 2648 2647
		mu 0 4 899 897 1721 1722
		f 4 -2926 -2927 -2648 2650
		mu 0 4 1723 1013 899 1722
		f 4 -2928 -2929 2925 2652
		mu 0 4 1724 1010 1013 1723
		f 4 -2930 -2931 2927 2654
		mu 0 4 1725 981 1010 1724
		f 4 -2932 -2933 2929 2656
		mu 0 4 1726 978 981 1725
		f 4 -2934 -2935 2931 2658
		mu 0 4 1727 997 978 1726
		f 4 -2936 -2937 2933 2660
		mu 0 4 1728 994 997 1727
		f 4 -2938 -2939 2935 2662
		mu 0 4 1729 935 994 1728
		f 4 -2940 -2941 2937 2664
		mu 0 4 1730 908 935 1729
		f 4 -2943 2939 2666 -2942
		mu 0 4 909 908 1730 1731
		f 4 -2945 2941 2668 -2944
		mu 0 4 911 909 1731 1732
		f 4 -2947 2943 2670 2669
		mu 0 4 913 911 1732 1733
		f 4 -2948 -2949 -2670 2672
		mu 0 4 1735 1154 913 1733
		f 4 -2950 -2951 2947 2674
		mu 0 4 1737 1162 1154 1735
		f 4 -2952 -2953 2949 2676
		mu 0 4 1739 1170 1162 1737
		f 4 -2954 -2955 2951 2678
		mu 0 4 1741 1178 1170 1739
		f 4 -2956 -2957 2953 2680
		mu 0 4 1743 1186 1178 1741
		f 4 -2958 -2959 2955 2682
		mu 0 4 1745 1194 1186 1743
		f 4 -2960 -2961 2957 2684
		mu 0 4 1747 1202 1194 1745
		f 4 -2962 -2963 2959 2686
		mu 0 4 1749 1210 1202 1747
		f 4 -2964 -2965 2961 2688
		mu 0 4 1751 1218 1210 1749
		f 4 -2966 -2967 2963 2690
		mu 0 4 1753 1147 1218 1751
		f 4 -2968 -2969 2965 2692
		mu 0 4 1754 1092 1147 1753
		f 4 -2971 2967 2694 -2970
		mu 0 4 1093 1092 1754 1755
		f 4 -2973 2969 2696 -2972
		mu 0 4 1089 1088 1757 1756
		f 4 -2975 2971 2698 -2974
		mu 0 4 1109 1108 1759 1758
		f 4 -2977 2973 2700 -2976
		mu 0 4 1105 1104 1761 1760
		f 4 -2978 2975 2702 -2791
		mu 0 4 1085 1084 1763 1762;
	setAttr ".fc[1500:1999]"
		f 4 -1264 2978 2980 -2980
		mu 0 4 771 750 1830 1831
		f 4 -1219 2981 2982 -2979
		mu 0 4 750 814 1832 1830
		f 4 1221 2983 -2985 -2982
		mu 0 4 814 815 1833 1832
		f 4 1220 2979 -2986 -2984
		mu 0 4 815 771 1831 1833
		f 4 -2981 2986 2988 -2988
		mu 0 4 1831 1830 1834 1835
		f 4 -2983 2989 2990 -2987
		mu 0 4 1830 1832 1836 1834
		f 4 2984 2991 -2993 -2990
		mu 0 4 1832 1833 1837 1836
		f 4 2985 2987 -2994 -2992
		mu 0 4 1833 1831 1835 1837
		f 4 -2989 2994 2996 -2996
		mu 0 4 1835 1834 1838 1839
		f 4 -2991 2997 2998 -2995
		mu 0 4 1834 1836 1840 1838
		f 4 2992 2999 -3001 -2998
		mu 0 4 1836 1837 1841 1840
		f 4 2993 2995 -3002 -3000
		mu 0 4 1837 1835 1839 1841
		f 4 -2997 3002 3004 -3004
		mu 0 4 1839 1838 1842 1843
		f 4 -2999 3005 3006 -3003
		mu 0 4 1838 1840 1844 1842
		f 4 3000 3007 -3009 -3006
		mu 0 4 1840 1841 1845 1844
		f 4 3001 3003 -3010 -3008
		mu 0 4 1841 1839 1843 1845
		f 4 -3005 3010 3012 -3012
		mu 0 4 1843 1842 1846 1847
		f 4 -3007 3013 3014 -3011
		mu 0 4 1842 1844 1848 1846
		f 4 3008 3015 -3017 -3014
		mu 0 4 1844 1845 1849 1848
		f 4 3009 3011 -3018 -3016
		mu 0 4 1845 1843 1847 1849
		f 4 -3013 3018 3020 -3020
		mu 0 4 1847 1846 1850 1851
		f 4 -3015 3021 3022 -3019
		mu 0 4 1846 1848 1852 1850
		f 4 3016 3023 -3025 -3022
		mu 0 4 1848 1849 1853 1852
		f 4 3017 3019 -3026 -3024
		mu 0 4 1849 1847 1851 1853
		f 4 -3021 3026 3028 -3028
		mu 0 4 1851 1850 1854 1855
		f 4 -3023 3029 3030 -3027
		mu 0 4 1850 1852 1856 1854
		f 4 3024 3031 -3033 -3030
		mu 0 4 1852 1853 1857 1856
		f 4 3025 3027 -3034 -3032
		mu 0 4 1853 1851 1855 1857
		f 4 -3029 3034 3036 -3036
		mu 0 4 1855 1854 1858 1859
		f 4 -3031 3037 3038 -3035
		mu 0 4 1854 1856 1860 1858
		f 4 3032 3039 -3041 -3038
		mu 0 4 1856 1857 1861 1860
		f 4 3033 3035 -3042 -3040
		mu 0 4 1857 1855 1859 1861
		f 4 -3037 3042 3044 -3044
		mu 0 4 1859 1858 847 846
		f 4 -3039 3045 3046 -3043
		mu 0 4 1858 1860 848 847
		f 4 3040 3047 -3049 -3046
		mu 0 4 1860 1861 849 848
		f 4 3041 3043 -3050 -3048
		mu 0 4 1861 1859 846 849
		f 4 -1250 3050 3052 -3052
		mu 0 4 805 760 1862 1863
		f 4 -1251 3053 3054 -3051
		mu 0 4 760 763 1864 1862
		f 4 1247 3055 -3057 -3054
		mu 0 4 763 806 1865 1864
		f 4 1208 3051 -3058 -3056
		mu 0 4 806 805 1863 1865
		f 4 -1276 3058 3060 -3060
		mu 0 4 831 740 1866 1867
		f 4 -1277 3061 3062 -3059
		mu 0 4 740 743 1868 1866
		f 4 1273 3063 -3065 -3062
		mu 0 4 743 832 1869 1868
		f 4 1234 3059 -3066 -3064
		mu 0 4 832 831 1867 1869
		f 4 1241 3067 -3069 -3067
		mu 0 4 840 839 1870 1871
		f 4 -1243 3069 3070 -3068
		mu 0 4 839 775 1872 1870
		f 4 -1284 3071 3072 -3070
		mu 0 4 775 774 1873 1872
		f 4 1281 3066 -3074 -3072
		mu 0 4 774 840 1871 1873
		f 4 -3053 3074 3076 -3076
		mu 0 4 1863 1862 1874 1875
		f 4 -3055 3077 3078 -3075
		mu 0 4 1862 1864 1876 1874
		f 4 3056 3079 -3081 -3078
		mu 0 4 1864 1865 1877 1876
		f 4 3057 3075 -3082 -3080
		mu 0 4 1865 1863 1875 1877
		f 4 -3061 3082 3084 -3084
		mu 0 4 1867 1866 1878 1879
		f 4 -3063 3085 3086 -3083
		mu 0 4 1866 1868 1880 1878
		f 4 3064 3087 -3089 -3086
		mu 0 4 1868 1869 1881 1880
		f 4 3065 3083 -3090 -3088
		mu 0 4 1869 1867 1879 1881
		f 4 3068 3091 -3093 -3091
		mu 0 4 1871 1870 1882 1883
		f 4 -3071 3093 3094 -3092
		mu 0 4 1870 1872 1884 1882
		f 4 -3073 3095 3096 -3094
		mu 0 4 1872 1873 1885 1884
		f 4 3073 3090 -3098 -3096
		mu 0 4 1873 1871 1883 1885
		f 4 -3077 3098 3100 -3100
		mu 0 4 1875 1874 1886 1887
		f 4 -3079 3101 3102 -3099
		mu 0 4 1874 1876 1888 1886
		f 4 3080 3103 -3105 -3102
		mu 0 4 1876 1877 1889 1888
		f 4 3081 3099 -3106 -3104
		mu 0 4 1877 1875 1887 1889
		f 4 -3085 3106 3108 -3108
		mu 0 4 1879 1878 1890 1891
		f 4 -3087 3109 3110 -3107
		mu 0 4 1878 1880 1892 1890
		f 4 3088 3111 -3113 -3110
		mu 0 4 1880 1881 1893 1892
		f 4 3089 3107 -3114 -3112
		mu 0 4 1881 1879 1891 1893
		f 4 3092 3115 -3117 -3115
		mu 0 4 1883 1882 1894 1895
		f 4 -3095 3117 3118 -3116
		mu 0 4 1882 1884 1896 1894
		f 4 -3097 3119 3120 -3118
		mu 0 4 1884 1885 1897 1896
		f 4 3097 3114 -3122 -3120
		mu 0 4 1885 1883 1895 1897
		f 4 -3101 3122 3124 -3124
		mu 0 4 1887 1886 843 842
		f 4 -3103 3125 3126 -3123
		mu 0 4 1886 1888 844 843
		f 4 3104 3127 -3129 -3126
		mu 0 4 1888 1889 845 844
		f 4 3105 3123 -3130 -3128
		mu 0 4 1889 1887 842 845
		f 4 -3109 3130 3132 -3132
		mu 0 4 1891 1890 855 854
		f 4 -3111 3133 3134 -3131
		mu 0 4 1890 1892 856 855
		f 4 3112 3135 -3137 -3134
		mu 0 4 1892 1893 857 856
		f 4 3113 3131 -3138 -3136
		mu 0 4 1893 1891 854 857
		f 4 3116 3139 -3141 -3139
		mu 0 4 1895 1894 859 858
		f 4 -3119 3141 3142 -3140
		mu 0 4 1894 1896 860 859
		f 4 -3121 3143 3144 -3142
		mu 0 4 1896 1897 861 860
		f 4 3121 3138 -3146 -3144
		mu 0 4 1897 1895 858 861
		f 4 3147 -3287 -3288 3285
		mu 0 4 1898 1899 1900 1901
		f 4 3149 -3179 -3148 3150
		mu 0 4 1902 1903 1899 1898
		f 4 -3285 -3181 -3150 3152
		mu 0 4 1904 1905 1903 1902
		f 4 3146 -3182 -3152 3153
		mu 0 4 1906 1907 1908 1909
		f 4 4545 -4915 -4548 -4549
		mu 0 4 1910 1911 1912 1913
		f 4 4543 -4917 -4546 -4547
		mu 0 4 1914 1915 1916 1917
		f 4 4541 -4919 -4544 -4545
		mu 0 4 1918 1919 1920 1921
		f 4 4551 -4909 -4554 -4555
		mu 0 4 1922 1923 1924 1925
		f 4 3159 -3297 -3298 3294
		mu 0 4 1926 1927 1928 1929
		f 4 3161 -3187 -3160 3162
		mu 0 4 1930 1931 1927 1926
		f 4 -3307 -3213 -3211 3208
		mu 0 4 1932 1933 1934 1935
		f 4 3158 -3190 -3164 3165
		mu 0 4 1936 1937 1938 1939
		f 4 3167 -3291 -3292 3288
		mu 0 4 1940 1941 1942 1943
		f 4 3169 -3195 -3168 3170
		mu 0 4 1944 1945 1941 1940
		f 4 -3277 -3197 -3170 3172
		mu 0 4 1946 1947 1945 1944
		f 4 3166 -3198 -3172 3173
		mu 0 4 1948 1949 1950 1951
		f 4 3175 -3289 -3290 3286
		mu 0 4 1899 1940 1943 1900
		f 4 3177 -3171 -3176 3178
		mu 0 4 1903 1944 1940 1899
		f 4 -3281 -3221 -3219 3216
		mu 0 4 1952 1953 1954 1955
		f 4 3174 -3174 -3180 3181
		mu 0 4 1907 1948 1951 1908
		f 4 3183 -3299 -3300 3296
		mu 0 4 1927 1956 1957 1928
		f 4 3185 -3156 -3184 3186
		mu 0 4 1931 1958 1956 1927
		f 4 -3303 -3157 -3186 3188
		mu 0 4 1959 1960 1958 1931
		f 4 3182 -3158 -3188 3189
		mu 0 4 1937 1961 1962 1938
		f 4 3191 -3293 -3294 3290
		mu 0 4 1941 1963 1964 1942
		f 4 3193 -3203 -3192 3194
		mu 0 4 1945 1965 1963 1941
		f 4 -3229 -3227 3224 -3314
		mu 0 4 1966 1967 1968 1969
		f 4 3190 -3206 -3196 3197
		mu 0 4 1949 1970 1971 1950
		f 4 3199 -3295 -3296 3292
		mu 0 4 1963 1926 1929 1964
		f 4 3201 -3163 -3200 3202
		mu 0 4 1965 1930 1926 1963
		f 4 -3311 -3165 -3202 3204
		mu 0 4 1972 1973 1930 1965
		f 4 3198 -3166 -3204 3205
		mu 0 4 1970 1936 1939 1971
		f 4 -3406 -3407 -3207 3164
		mu 0 4 1973 1974 1975 1930
		f 4 3206 -3405 -3210 -3162
		mu 0 4 1930 1975 1976 1931
		f 4 3209 -3403 -3305 -3189
		mu 0 4 1931 1976 1977 1959
		f 4 3211 -3397 -3208 3163
		mu 0 4 1938 1978 1979 1939
		f 4 -3388 -3389 -3215 3180
		mu 0 4 1905 1980 1981 1903
		f 4 3214 -3387 -3218 -3178
		mu 0 4 1903 1981 1982 1944
		f 4 3217 -3385 -3279 -3173
		mu 0 4 1944 1982 1983 1946
		f 4 3219 -3394 -3216 3179
		mu 0 4 1951 1984 1985 1908
		f 4 -3374 -3375 -3223 3196
		mu 0 4 1947 1986 1987 1945
		f 4 3222 -3373 -3226 -3194
		mu 0 4 1945 1987 1988 1965
		f 4 3225 -3371 -3313 -3205
		mu 0 4 1965 1988 1989 1972
		f 4 3227 -3365 -3224 3195
		mu 0 4 1971 1990 1991 1950
		f 4 3223 -3378 3375 3231
		mu 0 4 1950 1991 1992 1993
		f 4 3171 -3232 3234 3233
		mu 0 4 1951 1950 1993 1994
		f 4 -3381 -3220 -3234 3236
		mu 0 4 1995 1984 1951 1994
		f 4 3221 -3236 3238 3237
		mu 0 4 1996 1997 1998 1999
		f 4 3215 -3393 3389 3239
		mu 0 4 1908 1985 2000 2001
		f 4 3151 -3240 3242 3241
		mu 0 4 1909 1908 2001 2002
		f 4 -3177 -3147 3148 3245
		mu 0 4 2003 1907 1906 2004
		f 4 -3169 -3175 3176 3247
		mu 0 4 2005 1948 1907 2003
		f 4 -3193 -3167 3168 3249
		mu 0 4 2006 1949 1948 2005
		f 4 -3201 -3191 3192 3251
		mu 0 4 2007 1970 1949 2006
		f 4 -3161 -3199 3200 3253
		mu 0 4 2008 1936 1970 2007
		f 4 -3185 -3159 3160 3255
		mu 0 4 2009 1937 1936 2008
		f 4 -3155 -3183 3184 3257
		mu 0 4 2010 1961 1937 2009
		f 4 4549 -4911 -4552 -4553
		mu 0 4 2011 2012 2013 2014
		f 4 4553 -4907 -4556 -4557
		mu 0 4 2015 2016 2017 2018
		f 4 3187 -3259 3260 3259
		mu 0 4 1938 1962 2019 2020
		f 4 -3399 -3212 -3260 3262
		mu 0 4 2021 1978 1938 2020
		f 4 3213 -3262 3264 3263
		mu 0 4 2022 2023 2024 2025
		f 4 3207 -3410 3407 3265
		mu 0 4 1939 1979 2026 2027
		f 4 3203 -3266 3268 3267
		mu 0 4 1971 1939 2027 2028
		f 4 -3228 -3268 3270 -3367
		mu 0 4 1990 1971 2028 2029
		f 4 3229 -3270 3271 3230
		mu 0 4 2030 2031 2032 2033
		f 4 -3376 -3377 3373 3273
		mu 0 4 1993 1992 1986 1947
		f 4 -3235 -3274 3276 3275
		mu 0 4 1994 1993 1947 1946
		f 4 -3237 -3276 3278 -3383
		mu 0 4 1995 1994 1946 1983
		f 4 -5160 -5159 5156 5154
		mu 0 4 2034 2035 2036 2037
		f 4 -3390 -3391 3387 3281
		mu 0 4 2001 2000 1980 1905
		f 4 -3243 -3282 3284 3283
		mu 0 4 2002 2001 1905 1904
		f 4 -3245 -3246 3243 3287
		mu 0 4 1900 2003 2004 1901
		f 4 -3247 -3248 3244 3289
		mu 0 4 1943 2005 2003 1900
		f 4 -3249 -3250 3246 3291
		mu 0 4 1942 2006 2005 1943
		f 4 -5080 -5079 5076 5074
		mu 0 4 2038 2039 2040 2041
		f 4 -3253 -3254 3250 3295
		mu 0 4 1929 2008 2007 1964
		f 4 -3255 -3256 3252 3297
		mu 0 4 1928 2009 2008 1929
		f 4 -3257 -3258 3254 3299
		mu 0 4 1957 2010 2009 1928
		f 4 4547 -4913 -4550 -4551
		mu 0 4 2042 2043 2044 2045
		f 4 -3261 -3301 3302 3301
		mu 0 4 2020 2019 1960 1959
		f 4 -3401 -3263 -3302 3304
		mu 0 4 1977 2021 2020 1959
		f 4 -5168 -5167 5164 5162
		mu 0 4 2046 2047 2048 2049
		f 4 -3408 -3409 3405 3307
		mu 0 4 2027 2026 1974 1973
		f 4 -3269 -3308 3310 3309
		mu 0 4 2028 2027 1973 1972
		f 4 -3369 -3271 -3310 3312
		mu 0 4 1989 2029 2028 1972
		f 4 -5176 5174 5172 -5171
		mu 0 4 2050 2051 2052 2053
		f 4 3315 3220 -3315 3316
		mu 0 4 2054 1954 1953 2055
		f 4 3317 3218 -3316 3318
		mu 0 4 2056 1955 1954 2054
		f 4 -3283 -3217 -3318 3320
		mu 0 4 2057 1952 1955 2056
		f 4 -3241 -3280 3282 3322
		mu 0 4 2058 1999 1952 2057
		f 4 3323 -3238 3240 3324
		mu 0 4 2059 1996 1999 2058
		f 4 3325 -3222 -3324 3326
		mu 0 4 2060 1997 1996 2059
		f 4 3235 -3326 3328 3327
		mu 0 4 1998 1997 2060 2061
		f 4 3277 -3328 3329 3314
		mu 0 4 1953 1998 2061 2055
		f 4 3311 -3331 3332 3331
		mu 0 4 1966 2032 2062 2063
		f 4 3333 3228 -3332 3334
		mu 0 4 2064 1967 1966 2063
		f 4 3335 3226 -3334 3336
		mu 0 4 2065 1968 1967 2064
		f 4 -3275 -3225 -3336 3338
		mu 0 4 2066 1969 1968 2065
		f 4 -3233 -3273 3274 3340
		mu 0 4 2067 2033 1969 2066
		f 4 3341 -3231 3232 3342
		mu 0 4 2068 2030 2033 2067
		f 4 3343 -3230 -3342 3344
		mu 0 4 2069 2031 2030 2068
		f 4 3269 -3344 3345 3330
		mu 0 4 2032 2031 2069 2062
		f 4 3261 -3347 3348 3347
		mu 0 4 2024 2023 2070 2071
		f 4 3303 -3348 3350 3349
		mu 0 4 1933 2024 2071 2072
		f 4 3351 3212 -3350 3352
		mu 0 4 2073 1934 1933 2072
		f 4 3353 3210 -3352 3354
		mu 0 4 2074 1935 1934 2073
		f 4 -3309 -3209 -3354 3356
		mu 0 4 2075 1932 1935 2074
		f 4 -3267 -3306 3308 3358
		mu 0 4 2076 2025 1932 2075
		f 4 3359 -3264 3266 3360
		mu 0 4 2077 2022 2025 2076
		f 4 3346 -3214 -3360 3361
		mu 0 4 2070 2023 2022 2077
		f 4 4587 -4873 -4590 -4591
		mu 0 4 2078 2079 2080 2081
		f 4 -4588 -4589 4585 -4875
		mu 0 4 2079 2078 2082 2083
		f 4 4509 -4951 -4512 -4513
		mu 0 4 2084 2085 2086 2087
		f 4 4507 -4953 -4510 -4511
		mu 0 4 2088 2089 2085 2084
		f 4 -4954 -4955 -4508 -4509
		mu 0 4 2090 2091 2089 2088
		f 4 4589 -4871 4867 -4593
		mu 0 4 2081 2080 2092 2093
		f 4 -4853 -4610 -4611 4607
		mu 0 4 2094 2095 2096 2097
		f 4 4487 -4973 -4490 -4491
		mu 0 4 2098 2099 2100 2101
		f 4 4485 -4975 -4488 -4489
		mu 0 4 2102 2103 2099 2098
		f 4 -4976 -4977 -4486 -4487
		mu 0 4 2104 2105 2103 2102
		f 4 4611 -4849 4845 -4615
		mu 0 4 2106 2107 2108 2109
		f 4 4609 -4851 -4612 -4613
		mu 0 4 2096 2095 2107 2106
		f 4 4569 -4891 -4572 -4573
		mu 0 4 2110 2111 2112 2113
		f 4 -4893 -4570 -4571 4567
		mu 0 4 2114 2111 2110 2115
		f 4 4527 -4933 -4530 -4531
		mu 0 4 2116 2117 2118 2119
		f 4 4525 -4935 -4528 -4529
		mu 0 4 2120 2121 2117 2116
		f 4 -4936 -4937 -4526 -4527
		mu 0 4 2122 2123 2121 2120
		f 4 4571 -4889 4885 -4575
		mu 0 4 2113 2112 2124 2125
		f 4 -4971 -4492 -4493 4489
		mu 0 4 2100 2126 2127 2101
		f 4 3411 -3493 -3413 3382
		mu 0 4 1983 2128 2129 1995
		f 4 -4609 -4854 -4855 -4608
		mu 0 4 2097 2130 2131 2094
		f 4 3413 -3496 -3411 -3330
		mu 0 4 2061 2132 2133 2055
		f 4 -4949 -4514 -4515 4511
		mu 0 4 2086 2134 2135 2087
		f 4 3415 -3533 -3417 3368
		mu 0 4 1989 2136 2137 2029
		f 4 -4587 -4876 -4877 -4586
		mu 0 4 2082 2138 2139 2083
		f 4 3417 -3536 -3415 -3333
		mu 0 4 2062 2140 2141 2063
		f 4 -3436 3433 -3435 3429
		mu 0 4 2142 2143 2144 2145
		f 4 3434 3431 3427 3420
		mu 0 4 2146 2147 2148 2149
		f 4 3423 3425 3435 3432
		mu 0 4 2150 2151 2152 2153
		f 4 3436 -3575 -3438 3300
		mu 0 4 2019 2154 2155 1960
		f 4 -4543 -4920 -4921 -4542
		mu 0 4 1918 2156 2157 1919
		f 4 3438 -3571 -3440 -3434
		mu 0 4 2143 2158 2159 2144
		f 4 -4905 -4558 -4559 4555
		mu 0 4 2017 2160 2161 2018
		f 4 -4601 -4862 -4863 4859
		mu 0 4 2162 2163 2164 2165
		f 4 3443 -3477 -3442 3444
		mu 0 4 2166 2167 2168 2169
		f 4 -4963 -4500 -4501 4497
		mu 0 4 2170 2171 2172 2173
		f 4 3440 -3480 -3446 3447
		mu 0 4 2174 2175 2176 2177
		f 4 -4605 -4858 -4859 4855
		mu 0 4 2178 2179 2180 2181
		f 4 3451 -3469 -3450 3452
		mu 0 4 2182 2183 2184 2185
		f 4 -4967 -4496 -4497 4493
		mu 0 4 2186 2187 2188 2189
		f 4 3448 -3472 -3454 3455
		mu 0 4 2190 2191 2192 2193
		f 4 -4597 -4866 -4867 4863
		mu 0 4 2194 2195 2196 2197
		f 4 3459 -3485 -3458 3460
		mu 0 4 2198 2199 2200 2201
		f 4 -4959 -4504 -4505 4501
		mu 0 4 2202 2203 2204 2205
		f 4 3456 -3488 -3462 3463
		mu 0 4 2206 2207 2208 2209
		f 4 -4603 -4860 -4861 4857
		mu 0 4 2179 2162 2165 2180
		f 4 3467 -3445 -3466 3468
		mu 0 4 2183 2166 2169 2184
		f 4 -4965 -4498 -4499 4495
		mu 0 4 2187 2170 2173 2188
		f 4 3464 -3448 -3470 3471
		mu 0 4 2191 2174 2177 2192
		f 4 -4599 -4864 -4865 4861
		mu 0 4 2163 2194 2197 2164
		f 4 3475 -3461 -3474 3476
		mu 0 4 2167 2198 2201 2168
		f 4 -4961 -4502 -4503 4499
		mu 0 4 2171 2202 2205 2172
		f 4 3472 -3464 -3478 3479
		mu 0 4 2175 2206 2209 2176
		f 4 -4595 -4868 -4869 4865
		mu 0 4 2195 2093 2092 2196
		f 4 3483 3376 -3482 3484
		mu 0 4 2199 1986 1992 2200
		f 4 -4957 4953 -4507 4503
		mu 0 4 2203 2091 2090 2204
		f 4 3480 -3341 -3486 3487
		mu 0 4 2207 2067 2066 2208
		f 4 -4607 -4856 -4857 4853
		mu 0 4 2130 2178 2181 2131
		f 4 3491 -3453 -3490 3492
		mu 0 4 2128 2182 2185 2129
		f 4 -4969 -4494 -4495 4491
		mu 0 4 2126 2186 2189 2127
		f 4 3488 -3456 -3494 3495
		mu 0 4 2132 2190 2193 2133
		f 4 -4581 -4882 -4883 4879
		mu 0 4 2210 2211 2212 2213
		f 4 3499 -3517 -3498 3500
		mu 0 4 2214 2215 2216 2217
		f 4 -4943 -4520 -4521 4517
		mu 0 4 2218 2219 2220 2221
		f 4 3496 -3520 -3502 3503
		mu 0 4 2222 2223 2224 2225
		f 4 -4577 -4886 -4887 4883
		mu 0 4 2226 2125 2124 2227
		f 4 3507 3408 -3506 3508
		mu 0 4 2228 1974 2026 2229
		f 4 -4939 4935 -4525 4521
		mu 0 4 2230 2123 2122 2231
		f 4 3504 -3359 -3510 3511
		mu 0 4 2232 2076 2075 2233
		f 4 -4579 -4884 -4885 4881
		mu 0 4 2211 2226 2227 2212
		f 4 3515 -3509 -3514 3516
		mu 0 4 2215 2228 2229 2216
		f 4 -4941 -4522 -4523 4519
		mu 0 4 2219 2230 2231 2220
		f 4 3512 -3512 -3518 3519
		mu 0 4 2223 2232 2233 2224
		f 4 -4583 -4880 -4881 4877
		mu 0 4 2234 2210 2213 2235
		f 4 3523 -3501 -3522 3524
		mu 0 4 2236 2214 2217 2237
		f 4 -4945 -4518 -4519 4515
		mu 0 4 2238 2218 2221 2239
		f 4 3520 -3504 -3526 3527
		mu 0 4 2240 2222 2225 2241
		f 4 -4585 -4878 -4879 4875
		mu 0 4 2138 2234 2235 2139
		f 4 3531 -3525 -3530 3532
		mu 0 4 2136 2236 2237 2137
		f 4 -4947 -4516 -4517 4513
		mu 0 4 2134 2238 2239 2135
		f 4 3528 -3528 -3534 3535
		mu 0 4 2140 2240 2241 2141
		f 4 3537 -3351 -3537 3538
		mu 0 4 2242 2072 2071 2243
		f 4 -4533 4529 -4931 4927
		mu 0 4 2244 2119 2118 2245
		f 4 3541 3400 -3540 3542
		mu 0 4 2246 2021 1977 2247
		f 4 -4895 -4568 -4569 4565
		mu 0 4 2248 2114 2115 2249
		f 4 3545 -3539 -3545 3546
		mu 0 4 2250 2242 2243 2251
		f 4 -4535 -4928 -4929 4925
		mu 0 4 2252 2244 2245 2253
		f 4 3549 -3543 -3548 3550
		mu 0 4 2254 2246 2247 2255
		f 4 -4897 -4566 -4567 4563
		mu 0 4 2256 2248 2249 2257
		f 4 3553 -3547 -3553 3554
		mu 0 4 2258 2250 2251 2259
		f 4 -4537 -4926 -4927 4923
		mu 0 4 2260 2252 2253 2261
		f 4 3557 -3551 -3556 3558
		mu 0 4 2262 2254 2255 2263
		f 4 -4899 -4564 -4565 4561
		mu 0 4 2264 2256 2257 2265
		f 4 3561 -3555 -3561 3562
		mu 0 4 2266 2258 2259 2267
		f 4 -4539 -4924 -4925 4921
		mu 0 4 2268 2260 2261 2269
		f 4 3565 -3559 -3564 3566
		mu 0 4 2270 2262 2263 2271
		f 4 -4901 -4562 -4563 4559
		mu 0 4 2272 2264 2265 2273
		f 4 3569 -3563 -3569 3570
		mu 0 4 2158 2266 2267 2159
		f 4 -4541 -4922 -4923 4919
		mu 0 4 2156 2268 2269 2157
		f 4 3573 -3567 -3572 3574
		mu 0 4 2154 2270 2271 2155
		f 4 -4903 -4560 -4561 4557
		mu 0 4 2160 2272 2273 2161
		f 4 4451 -5008 -4454 -4455
		mu 0 4 2274 2275 2276 2277
		f 4 4457 -5003 -4460 -4461
		mu 0 4 2278 2279 2280 2281
		f 4 4459 -5001 -4462 -4463
		mu 0 4 2282 2283 2284 2285
		f 4 4449 -4823 -4452 -4453
		mu 0 4 2286 2287 2288 2289
		f 4 4447 -4825 -4450 -4451
		mu 0 4 2290 2291 2292 2293
		f 4 4453 -5007 -4456 -4457
		mu 0 4 2294 2295 2296 2297
		f 4 4455 -5005 -4458 -4459
		mu 0 4 2298 2299 2300 2301
		f 4 3593 3585 -3593 3594
		mu 0 4 2302 2303 2304 2305
		f 4 3595 3587 -3594 3596
		mu 0 4 2306 2307 2308 2309
		f 4 3597 3590 -3596 3598
		mu 0 4 2310 2311 2312 2313
		f 4 3599 3583 -3598 3600
		mu 0 4 2314 2315 2316 2317
		f 4 3601 3581 -3600 3602
		mu 0 4 2318 2319 2320 2321
		f 4 3603 3591 -3602 3604
		mu 0 4 2322 2323 2324 2325
		f 4 3605 3589 -3604 3606
		mu 0 4 2326 2327 2328 2329
		f 4 3592 3578 -3606 3607
		mu 0 4 2330 2331 2332 2333
		f 4 -4449 -4826 -4827 -4448
		mu 0 4 2290 2334 2335 2291
		f 4 3609 -3692 -3611 -3599
		mu 0 4 2313 2336 2337 2310
		f 4 -4999 -4464 -4465 4461
		mu 0 4 2284 2338 2339 2285
		f 4 3611 -3689 -3609 -3284
		mu 0 4 1904 2340 2341 2002
		f 4 -4617 -4846 -4847 4843
		mu 0 4 2342 2109 2108 2343
		f 4 3615 3390 -3614 3616
		mu 0 4 2344 1980 2000 2345
		f 4 -4979 4975 -4485 4481
		mu 0 4 2346 2105 2104 2347
		f 4 3612 -3323 -3618 3619
		mu 0 4 2348 2058 2057 2349
		f 4 -4619 -4844 -4845 4841
		mu 0 4 2350 2342 2343 2351
		f 4 3623 -3617 -3622 3624
		mu 0 4 2352 2344 2345 2353
		f 4 -4981 -4482 -4483 4479
		mu 0 4 2354 2346 2347 2355
		f 4 3620 -3620 -3626 3627
		mu 0 4 2356 2348 2349 2357
		f 4 -4621 -4842 -4843 4839
		mu 0 4 2358 2350 2351 2359
		f 4 3631 -3625 -3630 3632
		mu 0 4 2360 2352 2353 2361
		f 4 -4983 -4480 -4481 4477
		mu 0 4 2362 2354 2355 2363
		f 4 3628 -3628 -3634 3635
		mu 0 4 2364 2356 2357 2365
		f 4 -4623 -4840 -4841 4837
		mu 0 4 2366 2358 2359 2367
		f 4 3639 -3633 -3638 3640
		mu 0 4 2368 2360 2361 2369
		f 4 -4985 -4478 -4479 4475
		mu 0 4 2370 2362 2363 2371
		f 4 3636 -3636 -3642 3643
		mu 0 4 2372 2364 2365 2373
		f 4 -4625 -4838 -4839 4835
		mu 0 4 2374 2366 2367 2375
		f 4 3647 -3641 -3646 3648
		mu 0 4 2376 2368 2369 2377
		f 4 -4987 -4476 -4477 4473
		mu 0 4 2378 2370 2371 2379
		f 4 3644 -3644 -3650 3651
		mu 0 4 2380 2372 2373 2381
		f 4 -4627 -4836 -4837 4833
		mu 0 4 2382 2374 2375 2383
		f 4 3655 -3649 -3654 3656
		mu 0 4 2384 2376 2377 2385
		f 4 -4989 -4474 -4475 4471
		mu 0 4 2386 2378 2379 2387
		f 4 3652 -3652 -3658 3659
		mu 0 4 2388 2380 2381 2389
		f 4 -4629 -4834 -4835 4831
		mu 0 4 2390 2382 2383 2391
		f 4 3663 -3657 -3662 3664
		mu 0 4 2392 2384 2385 2393
		f 4 -4991 -4472 -4473 4469
		mu 0 4 2394 2386 2387 2395
		f 4 3660 -3660 -3666 3667
		mu 0 4 2396 2388 2389 2397
		f 4 -4631 -4832 -4833 4829
		mu 0 4 2398 2390 2391 2399
		f 4 3671 -3665 -3670 3672
		mu 0 4 2400 2392 2393 2401
		f 4 -4993 -4470 -4471 4467
		mu 0 4 2402 2394 2395 2403
		f 4 3668 -3668 -3674 3675
		mu 0 4 2404 2396 2397 2405
		f 4 -4830 -4831 4827 -4632
		mu 0 4 2398 2399 2406 2407
		f 4 3679 -3673 -3678 3680
		mu 0 4 2408 2400 2401 2409
		f 4 -4995 -4468 -4469 4465
		mu 0 4 2410 2402 2403 2411
		f 4 3676 -3676 -3682 3683
		mu 0 4 2412 2404 2405 2413
		f 4 -4447 -4828 -4829 4825
		mu 0 4 2334 2407 2406 2335
		f 4 3687 -3681 -3686 3688
		mu 0 4 2340 2408 2409 2341
		f 4 -4997 -4466 -4467 4463
		mu 0 4 2338 2410 2411 2339
		f 4 3684 -3684 -3690 3691
		mu 0 4 2336 2412 2413 2337
		f 4 3685 -3693 3694 3693
		mu 0 4 2414 2415 2416 2417
		f 4 3608 -3694 3696 -3587
		mu 0 4 2418 2414 2417 2419
		f 4 3586 3698 -3585 -3242
		mu 0 4 2418 2419 2420 2421
		f 4 3584 3700 -3577 -3154
		mu 0 4 2422 2423 2424 2425
		f 4 3576 3702 -3578 -3149
		mu 0 4 2426 2427 2428 2429
		f 4 3577 3704 -3589 -3244
		mu 0 4 2430 2431 2432 2433
		f 4 3588 3706 -3580 -3286
		mu 0 4 2434 2435 2436 2437
		f 4 3579 3708 -3581 -3151
		mu 0 4 2438 2439 2440 2441
		f 4 3580 3710 -3583 -3153
		mu 0 4 2442 2443 2444 2445
		f 4 -3691 -3612 3582 3712
		mu 0 4 2446 2447 2445 2444
		f 4 -3683 -3688 3690 3714
		mu 0 4 2448 2449 2447 2446
		f 4 -3675 -3680 3682 3716
		mu 0 4 2450 2451 2449 2448
		f 4 -3667 -3672 3674 3718
		mu 0 4 2452 2453 2451 2450
		f 4 -3659 -3664 3666 3720
		mu 0 4 2454 2455 2453 2452
		f 4 -3651 -3656 3658 3722
		mu 0 4 2456 2457 2455 2454
		f 4 -3643 -3648 3650 3724
		mu 0 4 2458 2459 2457 2456
		f 4 -3635 -3640 3642 3726
		mu 0 4 2460 2461 2459 2458
		f 4 -3627 -3632 3634 3728
		mu 0 4 2462 2463 2461 2460
		f 4 -3619 -3624 3626 3730
		mu 0 4 2464 2465 2463 2462
		f 4 3731 -3616 3618 3732
		mu 0 4 2466 1980 2465 2464
		f 4 -3732 3734 -3386 3388
		mu 0 4 1980 2466 2467 1981
		f 4 3385 3736 -3384 3386
		mu 0 4 1981 2467 2468 1982
		f 4 3383 3738 -3382 3384
		mu 0 4 1982 2468 2469 1983
		f 4 -3495 -3412 3381 3740
		mu 0 4 2470 2128 1983 2469
		f 4 -3455 -3492 3494 3742
		mu 0 4 2471 2182 2128 2470
		f 4 -3471 -3452 3454 3744
		mu 0 4 2472 2183 2182 2471
		f 4 -3447 -3468 3470 3746
		mu 0 4 2473 2166 2183 2472
		f 4 -3479 -3444 3446 3748
		mu 0 4 2474 2167 2166 2473
		f 4 -3463 -3476 3478 3750
		mu 0 4 2475 2198 2167 2474
		f 4 -3487 -3460 3462 3752
		mu 0 4 2476 2199 2198 2475
		f 4 3753 -3484 3486 3754
		mu 0 4 2477 1986 2199 2476
		f 4 -3754 3756 -3372 3374
		mu 0 4 1986 2477 2478 1987
		f 4 3371 3758 -3370 3372
		mu 0 4 1987 2478 2479 1988
		f 4 3369 3760 -3368 3370
		mu 0 4 1988 2479 2480 1989
		f 4 -3535 -3416 3367 3762
		mu 0 4 2481 2136 1989 2480
		f 4 -3527 -3532 3534 3764
		mu 0 4 2482 2236 2136 2481
		f 4 -3503 -3524 3526 3766
		mu 0 4 2483 2214 2236 2482
		f 4 -3519 -3500 3502 3768
		mu 0 4 2484 2215 2214 2483
		f 4 -3511 -3516 3518 3770
		mu 0 4 2485 2228 2215 2484
		f 4 3771 -3508 3510 3772
		mu 0 4 2486 1974 2228 2485
		f 4 -3772 3774 -3404 3406
		mu 0 4 1974 2486 2487 1975
		f 4 3403 3776 -3402 3404
		mu 0 4 1975 2487 2488 1976
		f 4 3401 3778 -3400 3402
		mu 0 4 1976 2488 2489 1977
		f 4 3539 3399 3780 3779
		mu 0 4 2247 1977 2489 2490
		f 4 3547 -3780 3782 3781
		mu 0 4 2255 2247 2490 2491
		f 4 3555 -3782 3784 3783
		mu 0 4 2263 2255 2491 2492
		f 4 3563 -3784 3786 3785
		mu 0 4 2271 2263 2492 2493
		f 4 3571 -3786 3788 3787
		mu 0 4 2155 2271 2493 2494
		f 4 3437 -3788 3790 -3425
		mu 0 4 1960 2155 2494 2495
		f 4 3424 3792 -3423 3156
		mu 0 4 1960 2495 2496 1958
		f 4 3422 3794 -3422 3155
		mu 0 4 1958 2497 2498 1956
		f 4 3421 3796 -3429 3298
		mu 0 4 1956 2499 2500 1957
		f 4 3428 3798 -3420 3256
		mu 0 4 1957 2501 2502 2010
		f 4 3419 3800 -3419 3154
		mu 0 4 2010 2503 2504 1961
		f 4 3418 3802 -3427 3157
		mu 0 4 1961 2505 2506 1962
		f 4 3426 3804 -3431 3258
		mu 0 4 1962 2507 2508 2019
		f 4 -3576 -3437 3430 3806
		mu 0 4 2509 2154 2019 2508
		f 4 -3568 -3574 3575 3808
		mu 0 4 2510 2270 2154 2509
		f 4 -3560 -3566 3567 3810
		mu 0 4 2511 2262 2270 2510
		f 4 -3552 -3558 3559 3812
		mu 0 4 2512 2254 2262 2511
		f 4 -3544 -3550 3551 3814
		mu 0 4 2513 2246 2254 2512
		f 4 -3398 -3542 3543 3816
		mu 0 4 2514 2021 2246 2513
		f 4 -3396 3398 3397 3818
		mu 0 4 2515 1978 2021 2514
		f 4 3395 3820 -3395 3396
		mu 0 4 1978 2515 2516 1979
		f 4 3394 3822 3821 3409
		mu 0 4 1979 2516 2517 2026
		f 4 3505 -3822 3824 3823
		mu 0 4 2229 2026 2517 2518
		f 4 3513 -3824 3826 3825
		mu 0 4 2216 2229 2518 2519
		f 4 3497 -3826 3828 3827
		mu 0 4 2217 2216 2519 2520
		f 4 3521 -3828 3830 3829
		mu 0 4 2237 2217 2520 2521
		f 4 3529 -3830 3832 3831
		mu 0 4 2137 2237 2521 2522
		f 4 3416 -3832 3834 -3366
		mu 0 4 2029 2137 2522 2523
		f 4 -3364 3366 3365 3836
		mu 0 4 2524 1990 2029 2523
		f 4 3363 3838 -3363 3364
		mu 0 4 1990 2524 2525 1991
		f 4 3362 3840 3839 3377
		mu 0 4 1991 2525 2526 1992
		f 4 3481 -3840 3842 3841
		mu 0 4 2200 1992 2526 2527
		f 4 3457 -3842 3844 3843
		mu 0 4 2201 2200 2527 2528
		f 4 3473 -3844 3846 3845
		mu 0 4 2168 2201 2528 2529
		f 4 3441 -3846 3848 3847
		mu 0 4 2169 2168 2529 2530
		f 4 3465 -3848 3850 3849
		mu 0 4 2184 2169 2530 2531
		f 4 3449 -3850 3852 3851
		mu 0 4 2185 2184 2531 2532
		f 4 3489 -3852 3854 3853
		mu 0 4 2129 2185 2532 2533
		f 4 3412 -3854 3856 -3380
		mu 0 4 1995 2129 2533 2534
		f 4 -3379 3380 3379 3858
		mu 0 4 2535 1984 1995 2534
		f 4 3378 3860 -3392 3393
		mu 0 4 1984 2535 2536 1985
		f 4 3391 3862 3861 3392
		mu 0 4 1985 2536 2537 2000
		f 4 3613 -3862 3864 3863
		mu 0 4 2538 2000 2537 2539
		f 4 3621 -3864 3866 3865
		mu 0 4 2540 2538 2539 2541
		f 4 3629 -3866 3868 3867
		mu 0 4 2542 2540 2541 2543
		f 4 3637 -3868 3870 3869
		mu 0 4 2544 2542 2543 2545
		f 4 3645 -3870 3872 3871
		mu 0 4 2546 2544 2545 2547
		f 4 3653 -3872 3874 3873
		mu 0 4 2548 2546 2547 2549
		f 4 3661 -3874 3876 3875
		mu 0 4 2550 2548 2549 2551
		f 4 3669 -3876 3878 3877
		mu 0 4 2552 2550 2551 2553
		f 4 3677 -3878 3879 3692
		mu 0 4 2415 2552 2553 2416
		f 4 -3695 -3881 3882 3881
		mu 0 4 2417 2416 2554 2555
		f 4 -3697 -3882 3884 -3696
		mu 0 4 2419 2417 2555 2556
		f 4 3695 3886 -3698 -3699
		mu 0 4 2419 2556 2557 2420
		f 4 3697 3888 -3700 -3701
		mu 0 4 2423 2558 2559 2424
		f 4 3699 3890 -3702 -3703
		mu 0 4 2427 2560 2561 2428
		f 4 3701 3892 -3704 -3705
		mu 0 4 2431 2562 2563 2432
		f 4 3703 3894 -3706 -3707
		mu 0 4 2435 2564 2565 2436
		f 4 3705 3896 -3708 -3709
		mu 0 4 2439 2566 2567 2440
		f 4 3707 3898 -3710 -3711
		mu 0 4 2443 2568 2569 2444
		f 4 -3712 -3713 3709 3900
		mu 0 4 2570 2446 2444 2569
		f 4 -3714 -3715 3711 3902
		mu 0 4 2571 2448 2446 2570
		f 4 -3716 -3717 3713 3904
		mu 0 4 2572 2450 2448 2571
		f 4 -3718 -3719 3715 3906
		mu 0 4 2573 2452 2450 2572
		f 4 -3720 -3721 3717 3908
		mu 0 4 2574 2454 2452 2573
		f 4 -3722 -3723 3719 3910
		mu 0 4 2575 2456 2454 2574
		f 4 -3724 -3725 3721 3912
		mu 0 4 2576 2458 2456 2575
		f 4 -3726 -3727 3723 3914
		mu 0 4 2577 2460 2458 2576
		f 4 -3728 -3729 3725 3916
		mu 0 4 2578 2462 2460 2577
		f 4 -3730 -3731 3727 3918
		mu 0 4 2579 2464 2462 2578
		f 4 3919 -3733 3729 3920
		mu 0 4 2580 2466 2464 2579
		f 4 -3920 3922 -3734 -3735
		mu 0 4 2466 2580 2581 2467
		f 4 3733 3924 -3736 -3737
		mu 0 4 2467 2581 2582 2468
		f 4 3735 3926 -3738 -3739
		mu 0 4 2468 2582 2583 2469
		f 4 -3740 -3741 3737 3928
		mu 0 4 2584 2470 2469 2583
		f 4 -3742 -3743 3739 3930
		mu 0 4 2585 2471 2470 2584
		f 4 -3744 -3745 3741 3932
		mu 0 4 2586 2472 2471 2585
		f 4 -3746 -3747 3743 3934
		mu 0 4 2587 2473 2472 2586
		f 4 -3748 -3749 3745 3936
		mu 0 4 2588 2474 2473 2587
		f 4 -3750 -3751 3747 3938
		mu 0 4 2589 2475 2474 2588
		f 4 -3752 -3753 3749 3940
		mu 0 4 2590 2476 2475 2589
		f 4 3941 -3755 3751 3942
		mu 0 4 2591 2477 2476 2590
		f 4 -3942 3944 -3756 -3757
		mu 0 4 2477 2591 2592 2478
		f 4 3755 3946 -3758 -3759
		mu 0 4 2478 2592 2593 2479
		f 4 3757 3948 -3760 -3761
		mu 0 4 2479 2593 2594 2480
		f 4 -3762 -3763 3759 3950
		mu 0 4 2595 2481 2480 2594
		f 4 -3764 -3765 3761 3952
		mu 0 4 2596 2482 2481 2595
		f 4 -3766 -3767 3763 3954
		mu 0 4 2597 2483 2482 2596
		f 4 -3768 -3769 3765 3956
		mu 0 4 2598 2484 2483 2597
		f 4 -3770 -3771 3767 3958
		mu 0 4 2599 2485 2484 2598
		f 4 3959 -3773 3769 3960
		mu 0 4 2600 2486 2485 2599
		f 4 -3960 3962 -3774 -3775
		mu 0 4 2486 2600 2601 2487
		f 4 3773 3964 -3776 -3777
		mu 0 4 2487 2601 2602 2488
		f 4 3775 3966 -3778 -3779
		mu 0 4 2488 2602 2603 2489
		f 4 -3781 3777 3968 3967
		mu 0 4 2490 2489 2603 2604
		f 4 -3783 -3968 3970 3969
		mu 0 4 2491 2490 2604 2605
		f 4 -3785 -3970 3972 3971
		mu 0 4 2492 2491 2605 2606
		f 4 -3787 -3972 3974 3973
		mu 0 4 2493 2492 2606 2607
		f 4 -3789 -3974 3976 3975
		mu 0 4 2494 2493 2607 2608
		f 4 -3791 -3976 3978 -3790
		mu 0 4 2495 2494 2608 2609
		f 4 3789 3980 -3792 -3793
		mu 0 4 2495 2609 2610 2496
		f 4 3791 3982 -3794 -3795
		mu 0 4 2497 2611 2612 2498;
	setAttr ".fc[2000:2499]"
		f 4 3793 3984 -3796 -3797
		mu 0 4 2499 2613 2614 2500
		f 4 3795 3986 -3798 -3799
		mu 0 4 2501 2615 2616 2502
		f 4 3797 3988 -3800 -3801
		mu 0 4 2503 2617 2618 2504
		f 4 3799 3990 -3802 -3803
		mu 0 4 2505 2619 2620 2506
		f 4 3801 3992 -3804 -3805
		mu 0 4 2507 2621 2622 2508
		f 4 -3806 -3807 3803 3994
		mu 0 4 2623 2509 2508 2622
		f 4 -3808 -3809 3805 3996
		mu 0 4 2624 2510 2509 2623
		f 4 -3810 -3811 3807 3998
		mu 0 4 2625 2511 2510 2624
		f 4 -3812 -3813 3809 4000
		mu 0 4 2626 2512 2511 2625
		f 4 -3814 -3815 3811 4002
		mu 0 4 2627 2513 2512 2626
		f 4 -3816 -3817 3813 4004
		mu 0 4 2628 2514 2513 2627
		f 4 -3818 -3819 3815 4006
		mu 0 4 2629 2515 2514 2628
		f 4 3817 4008 -3820 -3821
		mu 0 4 2515 2629 2630 2516
		f 4 3819 4010 4009 -3823
		mu 0 4 2516 2630 2631 2517
		f 4 -3825 -4010 4012 4011
		mu 0 4 2518 2517 2631 2632
		f 4 -3827 -4012 4014 4013
		mu 0 4 2519 2518 2632 2633
		f 4 -3829 -4014 4016 4015
		mu 0 4 2520 2519 2633 2634
		f 4 -3831 -4016 4018 4017
		mu 0 4 2521 2520 2634 2635
		f 4 -3833 -4018 4020 4019
		mu 0 4 2522 2521 2635 2636
		f 4 -3835 -4020 4022 -3834
		mu 0 4 2523 2522 2636 2637
		f 4 -3836 -3837 3833 4024
		mu 0 4 2638 2524 2523 2637
		f 4 3835 4026 -3838 -3839
		mu 0 4 2524 2638 2639 2525
		f 4 3837 4028 4027 -3841
		mu 0 4 2525 2639 2640 2526
		f 4 -3843 -4028 4030 4029
		mu 0 4 2527 2526 2640 2641
		f 4 -3845 -4030 4032 4031
		mu 0 4 2528 2527 2641 2642
		f 4 -3847 -4032 4034 4033
		mu 0 4 2529 2528 2642 2643
		f 4 -3849 -4034 4036 4035
		mu 0 4 2530 2529 2643 2644
		f 4 -3851 -4036 4038 4037
		mu 0 4 2531 2530 2644 2645
		f 4 -3853 -4038 4040 4039
		mu 0 4 2532 2531 2645 2646
		f 4 -3855 -4040 4042 4041
		mu 0 4 2533 2532 2646 2647
		f 4 -3857 -4042 4044 -3856
		mu 0 4 2534 2533 2647 2648
		f 4 -3858 -3859 3855 4046
		mu 0 4 2649 2535 2534 2648
		f 4 3857 4048 -3860 -3861
		mu 0 4 2535 2649 2650 2536
		f 4 3859 4050 4049 -3863
		mu 0 4 2536 2650 2651 2537
		f 4 -3865 -4050 4052 4051
		mu 0 4 2539 2537 2651 2652
		f 4 -3867 -4052 4054 4053
		mu 0 4 2541 2539 2652 2653
		f 4 -3869 -4054 4056 4055
		mu 0 4 2543 2541 2653 2654
		f 4 -3871 -4056 4058 4057
		mu 0 4 2545 2543 2654 2655
		f 4 -3873 -4058 4060 4059
		mu 0 4 2547 2545 2655 2656
		f 4 -3875 -4060 4062 4061
		mu 0 4 2549 2547 2656 2657
		f 4 -3877 -4062 4064 4063
		mu 0 4 2551 2549 2657 2658
		f 4 -3879 -4064 4066 4065
		mu 0 4 2553 2551 2658 2659
		f 4 -4066 4067 3880 -3880
		mu 0 4 2553 2659 2554 2416
		f 4 -3883 -4069 4070 4069
		mu 0 4 2555 2554 2660 2661
		f 4 -3885 -4070 4072 -3884
		mu 0 4 2556 2555 2661 2662
		f 4 3883 4074 -3886 -3887
		mu 0 4 2556 2662 2663 2557
		f 4 3885 4076 -3888 -3889
		mu 0 4 2558 2664 2665 2559
		f 4 3887 4078 -3890 -3891
		mu 0 4 2560 2666 2667 2561
		f 4 3889 4080 -3892 -3893
		mu 0 4 2562 2668 2669 2563
		f 4 3891 4082 -3894 -3895
		mu 0 4 2564 2670 2671 2565
		f 4 3893 4084 -3896 -3897
		mu 0 4 2566 2672 2673 2567
		f 4 3895 4086 -3898 -3899
		mu 0 4 2568 2674 2675 2569
		f 4 -3900 -3901 3897 4088
		mu 0 4 2676 2570 2569 2675
		f 4 -3902 -3903 3899 4090
		mu 0 4 2677 2571 2570 2676
		f 4 -3904 -3905 3901 4092
		mu 0 4 2678 2572 2571 2677
		f 4 -3906 -3907 3903 4094
		mu 0 4 2679 2573 2572 2678
		f 4 -3908 -3909 3905 4096
		mu 0 4 2680 2574 2573 2679
		f 4 -3910 -3911 3907 4098
		mu 0 4 2681 2575 2574 2680
		f 4 -3912 -3913 3909 4100
		mu 0 4 2682 2576 2575 2681
		f 4 -3914 -3915 3911 4102
		mu 0 4 2683 2577 2576 2682
		f 4 -3916 -3917 3913 4104
		mu 0 4 2684 2578 2577 2683
		f 4 -3918 -3919 3915 4106
		mu 0 4 2685 2579 2578 2684
		f 4 4107 -3921 3917 4108
		mu 0 4 2686 2580 2579 2685
		f 4 -4108 4110 -3922 -3923
		mu 0 4 2580 2686 2687 2581
		f 4 3921 4112 -3924 -3925
		mu 0 4 2581 2687 2688 2582
		f 4 3923 4114 -3926 -3927
		mu 0 4 2582 2688 2689 2583
		f 4 -3928 -3929 3925 4116
		mu 0 4 2690 2584 2583 2689
		f 4 -3930 -3931 3927 4118
		mu 0 4 2691 2585 2584 2690
		f 4 -3932 -3933 3929 4120
		mu 0 4 2692 2586 2585 2691
		f 4 -3934 -3935 3931 4122
		mu 0 4 2693 2587 2586 2692
		f 4 -3936 -3937 3933 4124
		mu 0 4 2694 2588 2587 2693
		f 4 -3938 -3939 3935 4126
		mu 0 4 2695 2589 2588 2694
		f 4 -3940 -3941 3937 4128
		mu 0 4 2696 2590 2589 2695
		f 4 4129 -3943 3939 4130
		mu 0 4 2697 2591 2590 2696
		f 4 -4130 4132 -3944 -3945
		mu 0 4 2591 2697 2698 2592
		f 4 3943 4134 -3946 -3947
		mu 0 4 2592 2698 2699 2593
		f 4 3945 4136 -3948 -3949
		mu 0 4 2593 2699 2700 2594
		f 4 -3950 -3951 3947 4138
		mu 0 4 2701 2595 2594 2700
		f 4 -3952 -3953 3949 4140
		mu 0 4 2702 2596 2595 2701
		f 4 -3954 -3955 3951 4142
		mu 0 4 2703 2597 2596 2702
		f 4 -3956 -3957 3953 4144
		mu 0 4 2704 2598 2597 2703
		f 4 -3958 -3959 3955 4146
		mu 0 4 2705 2599 2598 2704
		f 4 4147 -3961 3957 4148
		mu 0 4 2706 2600 2599 2705
		f 4 -4148 4150 -3962 -3963
		mu 0 4 2600 2706 2707 2601
		f 4 3961 4152 -3964 -3965
		mu 0 4 2601 2707 2708 2602
		f 4 3963 4154 -3966 -3967
		mu 0 4 2602 2708 2709 2603
		f 4 -3969 3965 4156 4155
		mu 0 4 2604 2603 2709 2710
		f 4 -3971 -4156 4158 4157
		mu 0 4 2605 2604 2710 2711
		f 4 -3973 -4158 4160 4159
		mu 0 4 2606 2605 2711 2712
		f 4 -3975 -4160 4162 4161
		mu 0 4 2607 2606 2712 2713
		f 4 -3977 -4162 4164 4163
		mu 0 4 2608 2607 2713 2714
		f 4 -3979 -4164 4166 -3978
		mu 0 4 2609 2608 2714 2715
		f 4 3977 4168 -3980 -3981
		mu 0 4 2609 2715 2716 2610
		f 4 3979 4170 -3982 -3983
		mu 0 4 2611 2717 2718 2612
		f 4 3981 4172 -3984 -3985
		mu 0 4 2613 2719 2720 2614
		f 4 3983 4174 -3986 -3987
		mu 0 4 2615 2721 2722 2616
		f 4 3985 4176 -3988 -3989
		mu 0 4 2617 2723 2724 2618
		f 4 3987 4178 -3990 -3991
		mu 0 4 2619 2725 2726 2620
		f 4 3989 4180 -3992 -3993
		mu 0 4 2621 2727 2728 2622
		f 4 -3994 -3995 3991 4182
		mu 0 4 2729 2623 2622 2728
		f 4 -3996 -3997 3993 4184
		mu 0 4 2730 2624 2623 2729
		f 4 -3998 -3999 3995 4186
		mu 0 4 2731 2625 2624 2730
		f 4 -4000 -4001 3997 4188
		mu 0 4 2732 2626 2625 2731
		f 4 -4002 -4003 3999 4190
		mu 0 4 2733 2627 2626 2732
		f 4 -4004 -4005 4001 4192
		mu 0 4 2734 2628 2627 2733
		f 4 -4006 -4007 4003 4194
		mu 0 4 2735 2629 2628 2734
		f 4 4005 4196 -4008 -4009
		mu 0 4 2629 2735 2736 2630
		f 4 4007 4198 4197 -4011
		mu 0 4 2630 2736 2737 2631
		f 4 -4013 -4198 4200 4199
		mu 0 4 2632 2631 2737 2738
		f 4 -4015 -4200 4202 4201
		mu 0 4 2633 2632 2738 2739
		f 4 -4017 -4202 4204 4203
		mu 0 4 2634 2633 2739 2740
		f 4 -4019 -4204 4206 4205
		mu 0 4 2635 2634 2740 2741
		f 4 -4021 -4206 4208 4207
		mu 0 4 2636 2635 2741 2742
		f 4 -4023 -4208 4210 -4022
		mu 0 4 2637 2636 2742 2743
		f 4 -4024 -4025 4021 4212
		mu 0 4 2744 2638 2637 2743
		f 4 4023 4214 -4026 -4027
		mu 0 4 2638 2744 2745 2639
		f 4 4025 4216 4215 -4029
		mu 0 4 2639 2745 2746 2640
		f 4 -4031 -4216 4218 4217
		mu 0 4 2641 2640 2746 2747
		f 4 -4033 -4218 4220 4219
		mu 0 4 2642 2641 2747 2748
		f 4 -4035 -4220 4222 4221
		mu 0 4 2643 2642 2748 2749
		f 4 -4037 -4222 4224 4223
		mu 0 4 2644 2643 2749 2750
		f 4 -4039 -4224 4226 4225
		mu 0 4 2645 2644 2750 2751
		f 4 -4041 -4226 4228 4227
		mu 0 4 2646 2645 2751 2752
		f 4 -4043 -4228 4230 4229
		mu 0 4 2647 2646 2752 2753
		f 4 -4045 -4230 4232 -4044
		mu 0 4 2648 2647 2753 2754
		f 4 -4046 -4047 4043 4234
		mu 0 4 2755 2649 2648 2754
		f 4 4045 4236 -4048 -4049
		mu 0 4 2649 2755 2756 2650
		f 4 4047 4238 4237 -4051
		mu 0 4 2650 2756 2757 2651
		f 4 -4053 -4238 4240 4239
		mu 0 4 2652 2651 2757 2758
		f 4 -4055 -4240 4242 4241
		mu 0 4 2653 2652 2758 2759
		f 4 -4057 -4242 4244 4243
		mu 0 4 2654 2653 2759 2760
		f 4 -4059 -4244 4246 4245
		mu 0 4 2655 2654 2760 2761
		f 4 -4061 -4246 4248 4247
		mu 0 4 2656 2655 2761 2762
		f 4 -4063 -4248 4250 4249
		mu 0 4 2657 2656 2762 2763
		f 4 -4065 -4250 4252 4251
		mu 0 4 2658 2657 2763 2764
		f 4 -4067 -4252 4254 4253
		mu 0 4 2659 2658 2764 2765
		f 4 -4254 4255 4068 -4068
		mu 0 4 2659 2765 2660 2554
		f 4 -4071 -4257 4258 4257
		mu 0 4 2661 2660 2766 2767
		f 4 -4073 -4258 4260 -4072
		mu 0 4 2662 2661 2767 2768
		f 4 4071 4262 -4074 -4075
		mu 0 4 2662 2768 2769 2663
		f 4 4073 4264 -4076 -4077
		mu 0 4 2664 2770 2771 2665
		f 4 4075 4266 -4078 -4079
		mu 0 4 2666 2772 2773 2667
		f 4 4077 4268 -4080 -4081
		mu 0 4 2668 2774 2775 2669
		f 4 4079 4270 -4082 -4083
		mu 0 4 2670 2776 2777 2671
		f 4 4081 4272 -4084 -4085
		mu 0 4 2672 2778 2779 2673
		f 4 4083 4274 -4086 -4087
		mu 0 4 2674 2780 2781 2675
		f 4 -4088 -4089 4085 4276
		mu 0 4 2782 2676 2675 2781
		f 4 -4090 -4091 4087 4278
		mu 0 4 2783 2677 2676 2782
		f 4 -4092 -4093 4089 4280
		mu 0 4 2784 2678 2677 2783
		f 4 -4094 -4095 4091 4282
		mu 0 4 2785 2679 2678 2784
		f 4 -4096 -4097 4093 4284
		mu 0 4 2786 2680 2679 2785
		f 4 -4098 -4099 4095 4286
		mu 0 4 2787 2681 2680 2786
		f 4 -4100 -4101 4097 4288
		mu 0 4 2788 2682 2681 2787
		f 4 -4102 -4103 4099 4290
		mu 0 4 2789 2683 2682 2788
		f 4 -4104 -4105 4101 4292
		mu 0 4 2790 2684 2683 2789
		f 4 -4106 -4107 4103 4294
		mu 0 4 2791 2685 2684 2790
		f 4 4295 -4109 4105 4296
		mu 0 4 2792 2686 2685 2791
		f 4 -4296 4298 -4110 -4111
		mu 0 4 2686 2792 2793 2687
		f 4 4109 4300 -4112 -4113
		mu 0 4 2687 2793 2794 2688
		f 4 4111 4302 -4114 -4115
		mu 0 4 2688 2794 2795 2689
		f 4 -4116 -4117 4113 4304
		mu 0 4 2796 2690 2689 2795
		f 4 -4118 -4119 4115 4306
		mu 0 4 2797 2691 2690 2796
		f 4 -4120 -4121 4117 4308
		mu 0 4 2798 2692 2691 2797
		f 4 -4122 -4123 4119 4310
		mu 0 4 2799 2693 2692 2798
		f 4 -4124 -4125 4121 4312
		mu 0 4 2800 2694 2693 2799
		f 4 -4126 -4127 4123 4314
		mu 0 4 2801 2695 2694 2800
		f 4 -4128 -4129 4125 4316
		mu 0 4 2802 2696 2695 2801
		f 4 4317 -4131 4127 4318
		mu 0 4 2803 2697 2696 2802
		f 4 -4318 4320 -4132 -4133
		mu 0 4 2697 2803 2804 2698
		f 4 4131 4322 -4134 -4135
		mu 0 4 2698 2804 2805 2699
		f 4 4133 4324 -4136 -4137
		mu 0 4 2699 2805 2806 2700
		f 4 -4138 -4139 4135 4326
		mu 0 4 2807 2701 2700 2806
		f 4 -4140 -4141 4137 4328
		mu 0 4 2808 2702 2701 2807
		f 4 -4142 -4143 4139 4330
		mu 0 4 2809 2703 2702 2808
		f 4 -4144 -4145 4141 4332
		mu 0 4 2810 2704 2703 2809
		f 4 -4146 -4147 4143 4334
		mu 0 4 2811 2705 2704 2810
		f 4 4335 -4149 4145 4336
		mu 0 4 2812 2706 2705 2811
		f 4 -4336 4338 -4150 -4151
		mu 0 4 2706 2812 2813 2707
		f 4 4149 4340 -4152 -4153
		mu 0 4 2707 2813 2814 2708
		f 4 4151 4342 -4154 -4155
		mu 0 4 2708 2814 2815 2709
		f 4 -4157 4153 4344 4343
		mu 0 4 2710 2709 2815 2816
		f 4 -4159 -4344 4346 4345
		mu 0 4 2711 2710 2816 2817
		f 4 -4161 -4346 4348 4347
		mu 0 4 2712 2711 2817 2818
		f 4 -4163 -4348 4350 4349
		mu 0 4 2713 2712 2818 2819
		f 4 -4165 -4350 4352 4351
		mu 0 4 2714 2713 2819 2820
		f 4 -4167 -4352 4354 -4166
		mu 0 4 2715 2714 2820 2821
		f 4 4165 4356 -4168 -4169
		mu 0 4 2715 2821 2822 2716
		f 4 4167 4358 -4170 -4171
		mu 0 4 2717 2823 2824 2718
		f 4 4169 4360 -4172 -4173
		mu 0 4 2719 2825 2826 2720
		f 4 4171 4362 -4174 -4175
		mu 0 4 2721 2827 2828 2722
		f 4 4173 4364 -4176 -4177
		mu 0 4 2723 2829 2830 2724
		f 4 4175 4366 -4178 -4179
		mu 0 4 2725 2831 2832 2726
		f 4 4177 4368 -4180 -4181
		mu 0 4 2727 2833 2834 2728
		f 4 -4182 -4183 4179 4370
		mu 0 4 2835 2729 2728 2834
		f 4 -4184 -4185 4181 4372
		mu 0 4 2836 2730 2729 2835
		f 4 -4186 -4187 4183 4374
		mu 0 4 2837 2731 2730 2836
		f 4 -4188 -4189 4185 4376
		mu 0 4 2838 2732 2731 2837
		f 4 -4190 -4191 4187 4378
		mu 0 4 2839 2733 2732 2838
		f 4 -4192 -4193 4189 4380
		mu 0 4 2840 2734 2733 2839
		f 4 -4194 -4195 4191 4382
		mu 0 4 2841 2735 2734 2840
		f 4 4193 4384 -4196 -4197
		mu 0 4 2735 2841 2842 2736
		f 4 4195 4386 4385 -4199
		mu 0 4 2736 2842 2843 2737
		f 4 -4201 -4386 4388 4387
		mu 0 4 2738 2737 2843 2844
		f 4 -4203 -4388 4390 4389
		mu 0 4 2739 2738 2844 2845
		f 4 -4205 -4390 4392 4391
		mu 0 4 2740 2739 2845 2846
		f 4 -4207 -4392 4394 4393
		mu 0 4 2741 2740 2846 2847
		f 4 -4209 -4394 4396 4395
		mu 0 4 2742 2741 2847 2848
		f 4 -4211 -4396 4398 -4210
		mu 0 4 2743 2742 2848 2849
		f 4 -4212 -4213 4209 4400
		mu 0 4 2850 2744 2743 2849
		f 4 4211 4402 -4214 -4215
		mu 0 4 2744 2850 2851 2745
		f 4 4213 4404 4403 -4217
		mu 0 4 2745 2851 2852 2746
		f 4 -4219 -4404 4406 4405
		mu 0 4 2747 2746 2852 2853
		f 4 -4221 -4406 4408 4407
		mu 0 4 2748 2747 2853 2854
		f 4 -4223 -4408 4410 4409
		mu 0 4 2749 2748 2854 2855
		f 4 -4225 -4410 4412 4411
		mu 0 4 2750 2749 2855 2856
		f 4 -4227 -4412 4414 4413
		mu 0 4 2751 2750 2856 2857
		f 4 -4229 -4414 4416 4415
		mu 0 4 2752 2751 2857 2858
		f 4 -4231 -4416 4418 4417
		mu 0 4 2753 2752 2858 2859
		f 4 -4233 -4418 4420 -4232
		mu 0 4 2754 2753 2859 2860
		f 4 -4234 -4235 4231 4422
		mu 0 4 2861 2755 2754 2860
		f 4 4233 4424 -4236 -4237
		mu 0 4 2755 2861 2862 2756
		f 4 4235 4426 4425 -4239
		mu 0 4 2756 2862 2863 2757
		f 4 -4241 -4426 4428 4427
		mu 0 4 2758 2757 2863 2864
		f 4 -4243 -4428 4430 4429
		mu 0 4 2759 2758 2864 2865
		f 4 -4245 -4430 4432 4431
		mu 0 4 2760 2759 2865 2866
		f 4 -4247 -4432 4434 4433
		mu 0 4 2761 2760 2866 2867
		f 4 -4249 -4434 4436 4435
		mu 0 4 2762 2761 2867 2868
		f 4 -4251 -4436 4438 4437
		mu 0 4 2763 2762 2868 2869
		f 4 -4253 -4438 4440 4439
		mu 0 4 2764 2763 2869 2870
		f 4 -4255 -4440 4442 4441
		mu 0 4 2765 2764 2870 2871
		f 4 -4442 4443 4256 -4256
		mu 0 4 2765 2871 2766 2660
		f 4 -4259 -4445 4446 4445
		mu 0 4 2767 2766 2407 2334
		f 4 -4261 -4446 4448 -4260
		mu 0 4 2768 2767 2334 2290
		f 4 4259 4450 -4262 -4263
		mu 0 4 2768 2290 2293 2769
		f 4 4261 4452 -4264 -4265
		mu 0 4 2770 2286 2289 2771
		f 4 4263 4454 -4266 -4267
		mu 0 4 2772 2274 2277 2773
		f 4 4265 4456 -4268 -4269
		mu 0 4 2774 2294 2297 2775
		f 4 4267 4458 -4270 -4271
		mu 0 4 2776 2298 2301 2777
		f 4 4269 4460 -4272 -4273
		mu 0 4 2778 2278 2281 2779
		f 4 4271 4462 -4274 -4275
		mu 0 4 2780 2282 2285 2781
		f 4 -4276 -4277 4273 4464
		mu 0 4 2339 2782 2781 2285
		f 4 -4278 -4279 4275 4466
		mu 0 4 2411 2783 2782 2339
		f 4 -4280 -4281 4277 4468
		mu 0 4 2403 2784 2783 2411
		f 4 -4282 -4283 4279 4470
		mu 0 4 2395 2785 2784 2403
		f 4 -4284 -4285 4281 4472
		mu 0 4 2387 2786 2785 2395
		f 4 -4286 -4287 4283 4474
		mu 0 4 2379 2787 2786 2387
		f 4 -4288 -4289 4285 4476
		mu 0 4 2371 2788 2787 2379
		f 4 -4290 -4291 4287 4478
		mu 0 4 2363 2789 2788 2371
		f 4 -4292 -4293 4289 4480
		mu 0 4 2355 2790 2789 2363
		f 4 -4294 -4295 4291 4482
		mu 0 4 2347 2791 2790 2355
		f 4 4483 -4297 4293 4484
		mu 0 4 2104 2792 2791 2347
		f 4 -4484 4486 -4298 -4299
		mu 0 4 2792 2104 2102 2793
		f 4 4297 4488 -4300 -4301
		mu 0 4 2793 2102 2098 2794
		f 4 4299 4490 -4302 -4303
		mu 0 4 2794 2098 2101 2795
		f 4 -4304 -4305 4301 4492
		mu 0 4 2127 2796 2795 2101
		f 4 -4306 -4307 4303 4494
		mu 0 4 2189 2797 2796 2127
		f 4 -4308 -4309 4305 4496
		mu 0 4 2188 2798 2797 2189
		f 4 -4310 -4311 4307 4498
		mu 0 4 2173 2799 2798 2188
		f 4 -4312 -4313 4309 4500
		mu 0 4 2172 2800 2799 2173
		f 4 -4314 -4315 4311 4502
		mu 0 4 2205 2801 2800 2172
		f 4 -4316 -4317 4313 4504
		mu 0 4 2204 2802 2801 2205
		f 4 4505 -4319 4315 4506
		mu 0 4 2090 2803 2802 2204
		f 4 -4506 4508 -4320 -4321
		mu 0 4 2803 2090 2088 2804
		f 4 4319 4510 -4322 -4323
		mu 0 4 2804 2088 2084 2805
		f 4 4321 4512 -4324 -4325
		mu 0 4 2805 2084 2087 2806
		f 4 -4326 -4327 4323 4514
		mu 0 4 2135 2807 2806 2087
		f 4 -4328 -4329 4325 4516
		mu 0 4 2239 2808 2807 2135
		f 4 -4330 -4331 4327 4518
		mu 0 4 2221 2809 2808 2239
		f 4 -4332 -4333 4329 4520
		mu 0 4 2220 2810 2809 2221
		f 4 -4334 -4335 4331 4522
		mu 0 4 2231 2811 2810 2220
		f 4 4523 -4337 4333 4524
		mu 0 4 2122 2812 2811 2231
		f 4 -4524 4526 -4338 -4339
		mu 0 4 2812 2122 2120 2813
		f 4 4337 4528 -4340 -4341
		mu 0 4 2813 2120 2116 2814
		f 4 4339 4530 -4342 -4343
		mu 0 4 2814 2116 2119 2815
		f 4 -4345 4341 4532 4531
		mu 0 4 2816 2815 2119 2244
		f 4 -4347 -4532 4534 4533
		mu 0 4 2817 2816 2244 2252
		f 4 -4349 -4534 4536 4535
		mu 0 4 2818 2817 2252 2260
		f 4 -4351 -4536 4538 4537
		mu 0 4 2819 2818 2260 2268
		f 4 -4353 -4538 4540 4539
		mu 0 4 2820 2819 2268 2156
		f 4 -4355 -4540 4542 -4354
		mu 0 4 2821 2820 2156 1918
		f 4 4353 4544 -4356 -4357
		mu 0 4 2821 1918 1921 2822
		f 4 4355 4546 -4358 -4359
		mu 0 4 2823 1914 1917 2824
		f 4 4357 4548 -4360 -4361
		mu 0 4 2825 1910 1913 2826
		f 4 4359 4550 -4362 -4363
		mu 0 4 2827 2042 2045 2828
		f 4 4361 4552 -4364 -4365
		mu 0 4 2829 2011 2014 2830
		f 4 4363 4554 -4366 -4367
		mu 0 4 2831 1922 1925 2832
		f 4 4365 4556 -4368 -4369
		mu 0 4 2833 2015 2018 2834
		f 4 -4370 -4371 4367 4558
		mu 0 4 2161 2835 2834 2018
		f 4 -4372 -4373 4369 4560
		mu 0 4 2273 2836 2835 2161
		f 4 -4374 -4375 4371 4562
		mu 0 4 2265 2837 2836 2273
		f 4 -4376 -4377 4373 4564
		mu 0 4 2257 2838 2837 2265
		f 4 -4378 -4379 4375 4566
		mu 0 4 2249 2839 2838 2257
		f 4 -4380 -4381 4377 4568
		mu 0 4 2115 2840 2839 2249
		f 4 -4382 -4383 4379 4570
		mu 0 4 2110 2841 2840 2115
		f 4 4381 4572 -4384 -4385
		mu 0 4 2841 2110 2113 2842
		f 4 4383 4574 4573 -4387
		mu 0 4 2842 2113 2125 2843
		f 4 -4389 -4574 4576 4575
		mu 0 4 2844 2843 2125 2226
		f 4 -4391 -4576 4578 4577
		mu 0 4 2845 2844 2226 2211
		f 4 -4393 -4578 4580 4579
		mu 0 4 2846 2845 2211 2210
		f 4 -4395 -4580 4582 4581
		mu 0 4 2847 2846 2210 2234
		f 4 -4397 -4582 4584 4583
		mu 0 4 2848 2847 2234 2138
		f 4 -4399 -4584 4586 -4398
		mu 0 4 2849 2848 2138 2082
		f 4 -4400 -4401 4397 4588
		mu 0 4 2078 2850 2849 2082
		f 4 4399 4590 -4402 -4403
		mu 0 4 2850 2078 2081 2851
		f 4 4401 4592 4591 -4405
		mu 0 4 2851 2081 2093 2852
		f 4 -4407 -4592 4594 4593
		mu 0 4 2853 2852 2093 2195
		f 4 -4409 -4594 4596 4595
		mu 0 4 2854 2853 2195 2194
		f 4 -4411 -4596 4598 4597
		mu 0 4 2855 2854 2194 2163
		f 4 -4413 -4598 4600 4599
		mu 0 4 2856 2855 2163 2162
		f 4 -4415 -4600 4602 4601
		mu 0 4 2857 2856 2162 2179
		f 4 -4417 -4602 4604 4603
		mu 0 4 2858 2857 2179 2178
		f 4 -4419 -4604 4606 4605
		mu 0 4 2859 2858 2178 2130
		f 4 -4421 -4606 4608 -4420
		mu 0 4 2860 2859 2130 2097
		f 4 -4422 -4423 4419 4610
		mu 0 4 2096 2861 2860 2097
		f 4 4421 4612 -4424 -4425
		mu 0 4 2861 2096 2106 2862
		f 4 4423 4614 4613 -4427
		mu 0 4 2862 2106 2109 2863
		f 4 -4429 -4614 4616 4615
		mu 0 4 2864 2863 2109 2342
		f 4 -4431 -4616 4618 4617
		mu 0 4 2865 2864 2342 2350
		f 4 -4433 -4618 4620 4619
		mu 0 4 2866 2865 2350 2358
		f 4 -4435 -4620 4622 4621
		mu 0 4 2867 2866 2358 2366
		f 4 -4437 -4622 4624 4623
		mu 0 4 2868 2867 2366 2374
		f 4 -4439 -4624 4626 4625
		mu 0 4 2869 2868 2374 2382
		f 4 -4441 -4626 4628 4627
		mu 0 4 2870 2869 2382 2390
		f 4 -4443 -4628 4630 4629
		mu 0 4 2871 2870 2390 2398
		f 4 -4630 4631 4444 -4444
		mu 0 4 2871 2398 2407 2766
		f 4 4633 -3421 -4633 4634
		mu 0 4 2872 2873 2874 2875
		f 4 4635 -3430 -4634 4636
		mu 0 4 2876 2877 2878 2879
		f 4 4637 -3433 -4636 4638
		mu 0 4 2880 2881 2882 2883
		f 4 4639 -3424 -4638 4640
		mu 0 4 2884 2885 2886 2887
		f 4 4641 -3426 -4640 4642
		mu 0 4 2888 2889 2890 2891
		f 4 -3573 -3439 -4642 4644
		mu 0 4 2892 2893 2889 2888
		f 4 -3565 -3570 3572 4646
		mu 0 4 2894 2895 2893 2892
		f 4 -3557 -3562 3564 4648
		mu 0 4 2896 2897 2895 2894
		f 4 -3549 -3554 3556 4650
		mu 0 4 2898 2899 2897 2896
		f 4 -3541 -3546 3548 4652
		mu 0 4 2900 2901 2899 2898
		f 4 4653 -3538 3540 4654
		mu 0 4 2902 2072 2901 2900
		f 4 4655 -3353 -4654 4656
		mu 0 4 2903 2073 2072 2902
		f 4 4657 -3355 -4656 4658
		mu 0 4 2904 2074 2073 2903
		f 4 -3356 -3357 -4658 4660
		mu 0 4 2905 2075 2074 2904
		f 4 3509 3355 4662 4661
		mu 0 4 2233 2075 2905 2906
		f 4 3517 -4662 4664 4663
		mu 0 4 2224 2233 2906 2907
		f 4 3501 -4664 4666 4665
		mu 0 4 2225 2224 2907 2908
		f 4 3525 -4666 4668 4667
		mu 0 4 2241 2225 2908 2909
		f 4 3533 -4668 4670 4669
		mu 0 4 2141 2241 2909 2910
		f 4 3414 -4670 4672 4671
		mu 0 4 2063 2141 2910 2911
		f 4 4673 -3335 -4672 4674
		mu 0 4 2912 2064 2063 2911
		f 4 4675 -3337 -4674 4676
		mu 0 4 2913 2065 2064 2912
		f 4 -3338 -3339 -4676 4678
		mu 0 4 2914 2066 2065 2913
		f 4 3485 3337 4680 4679
		mu 0 4 2208 2066 2914 2915
		f 4 3461 -4680 4682 4681
		mu 0 4 2209 2208 2915 2916
		f 4 3477 -4682 4684 4683
		mu 0 4 2176 2209 2916 2917
		f 4 3445 -4684 4686 4685
		mu 0 4 2177 2176 2917 2918
		f 4 3469 -4686 4688 4687
		mu 0 4 2192 2177 2918 2919
		f 4 3453 -4688 4690 4689
		mu 0 4 2193 2192 2919 2920
		f 4 3493 -4690 4692 4691
		mu 0 4 2133 2193 2920 2921
		f 4 3410 -4692 4694 4693
		mu 0 4 2055 2133 2921 2922
		f 4 4695 -3317 -4694 4696
		mu 0 4 2923 2054 2055 2922
		f 4 4697 -3319 -4696 4698
		mu 0 4 2924 2056 2054 2923
		f 4 -3320 -3321 -4698 4700
		mu 0 4 2925 2057 2056 2924
		f 4 3617 3319 4702 4701
		mu 0 4 2926 2057 2925 2927
		f 4 3625 -4702 4704 4703
		mu 0 4 2928 2926 2927 2929
		f 4 3633 -4704 4706 4705
		mu 0 4 2930 2928 2929 2931
		f 4 3641 -4706 4708 4707
		mu 0 4 2932 2930 2931 2933
		f 4 3649 -4708 4710 4709
		mu 0 4 2934 2932 2933 2935
		f 4 3657 -4710 4712 4711
		mu 0 4 2936 2934 2935 2937
		f 4 3665 -4712 4714 4713
		mu 0 4 2938 2936 2937 2939
		f 4 3673 -4714 4716 4715
		mu 0 4 2940 2938 2939 2941
		f 4 3681 -4716 4718 4717
		mu 0 4 2942 2940 2941 2943
		f 4 3689 -4718 4720 4719
		mu 0 4 2944 2942 2943 2945
		f 4 3610 -4720 4722 4721
		mu 0 4 2317 2944 2945 2946
		f 4 4723 -3601 -4722 4724
		mu 0 4 2947 2314 2317 2946
		f 4 4725 -3603 -4724 4726
		mu 0 4 2948 2318 2321 2949
		f 4 4727 -3605 -4726 4728
		mu 0 4 2950 2322 2325 2951
		f 4 4729 -3607 -4728 4730
		mu 0 4 2952 2326 2329 2953
		f 4 4731 -3608 -4730 4732
		mu 0 4 2954 2330 2333 2955
		f 4 4733 -3595 -4732 4734
		mu 0 4 2956 2302 2305 2957
		f 4 4735 -3597 -4734 4736
		mu 0 4 2958 2306 2309 2959
		f 4 -3687 -3610 -4736 4738
		mu 0 4 2960 2961 2306 2958
		f 4 -3679 -3685 3686 4740
		mu 0 4 2962 2963 2961 2960
		f 4 -3671 -3677 3678 4742
		mu 0 4 2964 2965 2963 2962
		f 4 -3663 -3669 3670 4744
		mu 0 4 2966 2967 2965 2964
		f 4 -3655 -3661 3662 4746
		mu 0 4 2968 2969 2967 2966
		f 4 -3647 -3653 3654 4748
		mu 0 4 2970 2971 2969 2968
		f 4 -3639 -3645 3646 4750
		mu 0 4 2972 2973 2971 2970
		f 4 -3631 -3637 3638 4752
		mu 0 4 2974 2975 2973 2972
		f 4 -3623 -3629 3630 4754
		mu 0 4 2976 2977 2975 2974
		f 4 -3615 -3621 3622 4756
		mu 0 4 2978 2979 2977 2976
		f 4 -3322 -3613 3614 4758
		mu 0 4 2980 2058 2979 2978
		f 4 4759 -3325 3321 4760
		mu 0 4 2981 2059 2058 2980
		f 4 4761 -3327 -4760 4762
		mu 0 4 2982 2060 2059 2981
		f 4 -3329 -4762 4764 4763
		mu 0 4 2061 2060 2982 2983
		f 4 -3491 -3414 -4764 4766
		mu 0 4 2984 2132 2061 2983
		f 4 -3451 -3489 3490 4768
		mu 0 4 2985 2190 2132 2984
		f 4 -3467 -3449 3450 4770
		mu 0 4 2986 2191 2190 2985
		f 4 -3443 -3465 3466 4772
		mu 0 4 2987 2174 2191 2986
		f 4 -3475 -3441 3442 4774
		mu 0 4 2988 2175 2174 2987
		f 4 -3459 -3473 3474 4776
		mu 0 4 2989 2206 2175 2988
		f 4 -3483 -3457 3458 4778
		mu 0 4 2990 2207 2206 2989
		f 4 -3340 -3481 3482 4780
		mu 0 4 2991 2067 2207 2990
		f 4 4781 -3343 3339 4782
		mu 0 4 2992 2068 2067 2991
		f 4 4783 -3345 -4782 4784
		mu 0 4 2993 2069 2068 2992
		f 4 -4784 4786 4785 -3346
		mu 0 4 2069 2993 2994 2062
		f 4 -3531 -3418 -4786 4788
		mu 0 4 2995 2140 2062 2994
		f 4 -3523 -3529 3530 4790
		mu 0 4 2996 2240 2140 2995
		f 4 -3499 -3521 3522 4792
		mu 0 4 2997 2222 2240 2996
		f 4 -3515 -3497 3498 4794
		mu 0 4 2998 2223 2222 2997
		f 4 -3507 -3513 3514 4796
		mu 0 4 2999 2232 2223 2998
		f 4 -3358 -3505 3506 4798
		mu 0 4 3000 2076 2232 2999
		f 4 4799 -3361 3357 4800
		mu 0 4 3001 2077 2076 3000
		f 4 4801 -3362 -4800 4802
		mu 0 4 3002 2070 2077 3001
		f 4 -3349 -4802 4804 4803
		mu 0 4 2071 2070 3002 3003
		f 4 3536 -4804 4806 4805
		mu 0 4 3004 2071 3003 3005
		f 4 3544 -4806 4808 4807
		mu 0 4 3006 3004 3005 3007
		f 4 3552 -4808 4810 4809
		mu 0 4 3008 3006 3007 3009
		f 4 3560 -4810 4812 4811
		mu 0 4 3010 3008 3009 3011
		f 4 3568 -4812 4814 4813
		mu 0 4 3012 3010 3011 3013
		f 4 3439 -4814 4816 4815
		mu 0 4 3014 3012 3013 3015
		f 4 4817 -3432 -4816 4818
		mu 0 4 3016 3017 3014 3015
		f 4 4632 -3428 -4818 4819
		mu 0 4 3018 3019 3020 3021
		f 4 4821 -4735 -4821 4822
		mu 0 4 2287 2956 2957 2288
		f 4 4823 -4737 -4822 4824
		mu 0 4 2291 2958 2959 2292
		f 4 -4738 -4739 -4824 4826
		mu 0 4 2335 2960 2958 2291
		f 4 -4740 -4741 4737 4828
		mu 0 4 2406 2962 2960 2335
		f 4 -4742 -4743 4739 4830
		mu 0 4 2399 2964 2962 2406
		f 4 -4744 -4745 4741 4832
		mu 0 4 2391 2966 2964 2399
		f 4 -4746 -4747 4743 4834
		mu 0 4 2383 2968 2966 2391
		f 4 -4748 -4749 4745 4836
		mu 0 4 2375 2970 2968 2383
		f 4 -4750 -4751 4747 4838
		mu 0 4 2367 2972 2970 2375
		f 4 -4752 -4753 4749 4840
		mu 0 4 2359 2974 2972 2367
		f 4 -4754 -4755 4751 4842
		mu 0 4 2351 2976 2974 2359
		f 4 -4756 -4757 4753 4844
		mu 0 4 2343 2978 2976 2351
		f 4 -4758 -4759 4755 4846
		mu 0 4 2108 2980 2978 2343
		f 4 4847 -4761 4757 4848
		mu 0 4 2107 2981 2980 2108
		f 4 4849 -4763 -4848 4850
		mu 0 4 2095 2982 2981 2107
		f 4 -4765 -4850 4852 4851
		mu 0 4 2983 2982 2095 2094
		f 4 -4766 -4767 -4852 4854
		mu 0 4 2131 2984 2983 2094
		f 4 -4768 -4769 4765 4856
		mu 0 4 2181 2985 2984 2131
		f 4 -4770 -4771 4767 4858
		mu 0 4 2180 2986 2985 2181
		f 4 -4772 -4773 4769 4860
		mu 0 4 2165 2987 2986 2180
		f 4 -4774 -4775 4771 4862
		mu 0 4 2164 2988 2987 2165
		f 4 -4776 -4777 4773 4864
		mu 0 4 2197 2989 2988 2164
		f 4 -4778 -4779 4775 4866
		mu 0 4 2196 2990 2989 2197
		f 4 -4780 -4781 4777 4868
		mu 0 4 2092 2991 2990 2196
		f 4 4869 -4783 4779 4870
		mu 0 4 2080 2992 2991 2092
		f 4 4871 -4785 -4870 4872
		mu 0 4 2079 2993 2992 2080
		f 4 -4872 4874 4873 -4787
		mu 0 4 2993 2079 2083 2994
		f 4 -4788 -4789 -4874 4876
		mu 0 4 2139 2995 2994 2083
		f 4 -4790 -4791 4787 4878
		mu 0 4 2235 2996 2995 2139
		f 4 -4792 -4793 4789 4880
		mu 0 4 2213 2997 2996 2235
		f 4 -4794 -4795 4791 4882
		mu 0 4 2212 2998 2997 2213
		f 4 -4796 -4797 4793 4884
		mu 0 4 2227 2999 2998 2212
		f 4 -4798 -4799 4795 4886
		mu 0 4 2124 3000 2999 2227
		f 4 4887 -4801 4797 4888
		mu 0 4 2112 3001 3000 2124
		f 4 4889 -4803 -4888 4890
		mu 0 4 2111 3002 3001 2112
		f 4 -4805 -4890 4892 4891
		mu 0 4 3003 3002 2111 2114
		f 4 -4807 -4892 4894 4893
		mu 0 4 3005 3003 2114 2248
		f 4 -4809 -4894 4896 4895
		mu 0 4 3007 3005 2248 2256
		f 4 -4811 -4896 4898 4897
		mu 0 4 3009 3007 2256 2264
		f 4 -4813 -4898 4900 4899
		mu 0 4 3011 3009 2264 2272
		f 4 -4815 -4900 4902 4901
		mu 0 4 3013 3011 2272 2160
		f 4 -4817 -4902 4904 4903
		mu 0 4 3015 3013 2160 2017
		f 4 4905 -4819 -4904 4906
		mu 0 4 2016 3016 3015 2017
		f 4 4907 -4820 -4906 4908
		mu 0 4 1923 3018 3021 1924
		f 4 4909 -4635 -4908 4910
		mu 0 4 2012 2872 2875 2013
		f 4 4911 -4637 -4910 4912
		mu 0 4 2043 2876 2879 2044
		f 4 4913 -4639 -4912 4914
		mu 0 4 1911 2880 2883 1912
		f 4 4915 -4641 -4914 4916
		mu 0 4 1915 2884 2887 1916
		f 4 4917 -4643 -4916 4918
		mu 0 4 1919 2888 2891 1920
		f 4 -4644 -4645 -4918 4920
		mu 0 4 2157 2892 2888 1919
		f 4 -4646 -4647 4643 4922
		mu 0 4 2269 2894 2892 2157
		f 4 -4648 -4649 4645 4924
		mu 0 4 2261 2896 2894 2269
		f 4 -4650 -4651 4647 4926
		mu 0 4 2253 2898 2896 2261
		f 4 -4652 -4653 4649 4928
		mu 0 4 2245 2900 2898 2253
		f 4 4929 -4655 4651 4930
		mu 0 4 2118 2902 2900 2245
		f 4 4931 -4657 -4930 4932
		mu 0 4 2117 2903 2902 2118
		f 4 4933 -4659 -4932 4934
		mu 0 4 2121 2904 2903 2117
		f 4 -4660 -4661 -4934 4936
		mu 0 4 2123 2905 2904 2121
		f 4 -4663 4659 4938 4937
		mu 0 4 2906 2905 2123 2230
		f 4 -4665 -4938 4940 4939
		mu 0 4 2907 2906 2230 2219
		f 4 -4667 -4940 4942 4941
		mu 0 4 2908 2907 2219 2218
		f 4 -4669 -4942 4944 4943
		mu 0 4 2909 2908 2218 2238
		f 4 -4671 -4944 4946 4945
		mu 0 4 2910 2909 2238 2134
		f 4 -4673 -4946 4948 4947
		mu 0 4 2911 2910 2134 2086
		f 4 4949 -4675 -4948 4950
		mu 0 4 2085 2912 2911 2086
		f 4 4951 -4677 -4950 4952
		mu 0 4 2089 2913 2912 2085
		f 4 -4678 -4679 -4952 4954
		mu 0 4 2091 2914 2913 2089
		f 4 -4681 4677 4956 4955
		mu 0 4 2915 2914 2091 2203
		f 4 -4683 -4956 4958 4957
		mu 0 4 2916 2915 2203 2202
		f 4 -4685 -4958 4960 4959
		mu 0 4 2917 2916 2202 2171
		f 4 -4687 -4960 4962 4961
		mu 0 4 2918 2917 2171 2170
		f 4 -4689 -4962 4964 4963
		mu 0 4 2919 2918 2170 2187
		f 4 -4691 -4964 4966 4965
		mu 0 4 2920 2919 2187 2186
		f 4 -4693 -4966 4968 4967
		mu 0 4 2921 2920 2186 2126
		f 4 -4695 -4968 4970 4969
		mu 0 4 2922 2921 2126 2100
		f 4 4971 -4697 -4970 4972
		mu 0 4 2099 2923 2922 2100
		f 4 4973 -4699 -4972 4974
		mu 0 4 2103 2924 2923 2099
		f 4 -4700 -4701 -4974 4976
		mu 0 4 2105 2925 2924 2103
		f 4 -4703 4699 4978 4977
		mu 0 4 2927 2925 2105 2346
		f 4 -4705 -4978 4980 4979
		mu 0 4 2929 2927 2346 2354
		f 4 -4707 -4980 4982 4981
		mu 0 4 2931 2929 2354 2362;
	setAttr ".fc[2500:2999]"
		f 4 -4709 -4982 4984 4983
		mu 0 4 2933 2931 2362 2370
		f 4 -4711 -4984 4986 4985
		mu 0 4 2935 2933 2370 2378
		f 4 -4713 -4986 4988 4987
		mu 0 4 2937 2935 2378 2386
		f 4 -4715 -4988 4990 4989
		mu 0 4 2939 2937 2386 2394
		f 4 -4717 -4990 4992 4991
		mu 0 4 2941 2939 2394 2402
		f 4 -4719 -4992 4994 4993
		mu 0 4 2943 2941 2402 2410
		f 4 -4721 -4994 4996 4995
		mu 0 4 2945 2943 2410 2338
		f 4 -4723 -4996 4998 4997
		mu 0 4 2946 2945 2338 2284
		f 4 4999 -4725 -4998 5000
		mu 0 4 2283 2947 2946 2284
		f 4 5001 -4727 -5000 5002
		mu 0 4 2279 2948 2949 2280
		f 4 5003 -4729 -5002 5004
		mu 0 4 2299 2950 2951 2300
		f 4 5005 -4731 -5004 5006
		mu 0 4 2295 2952 2953 2296
		f 4 4820 -4733 -5006 5007
		mu 0 4 2275 2954 2955 2276
		f 4 5009 -5011 -5009 3293
		mu 0 4 1964 3022 3023 1942
		f 4 5008 -5013 -5012 3248
		mu 0 4 1942 3023 3024 2006
		f 4 5011 5014 -5014 -3252
		mu 0 4 2006 3024 3025 2007
		f 4 5013 5015 -5010 -3251
		mu 0 4 2007 3025 3022 1964
		f 4 5017 -5019 -5017 5010
		mu 0 4 3022 3026 3027 3023
		f 4 5016 -5021 -5020 5012
		mu 0 4 3023 3027 3028 3024
		f 4 5019 5022 -5022 -5015
		mu 0 4 3024 3028 3029 3025
		f 4 5021 5023 -5018 -5016
		mu 0 4 3025 3029 3026 3022
		f 4 5025 -5027 -5025 5018
		mu 0 4 3026 3030 3031 3027
		f 4 5024 -5029 -5028 5020
		mu 0 4 3027 3031 3032 3028
		f 4 5027 5030 -5030 -5023
		mu 0 4 3028 3032 3033 3029
		f 4 5029 5031 -5026 -5024
		mu 0 4 3029 3033 3030 3026
		f 4 5033 -5035 -5033 5026
		mu 0 4 3030 3034 3035 3031
		f 4 5032 -5037 -5036 5028
		mu 0 4 3031 3035 3036 3032
		f 4 5035 5038 -5038 -5031
		mu 0 4 3032 3036 3037 3033
		f 4 5037 5039 -5034 -5032
		mu 0 4 3033 3037 3034 3030
		f 4 5041 -5043 -5041 5034
		mu 0 4 3034 3038 3039 3035
		f 4 5040 -5045 -5044 5036
		mu 0 4 3035 3039 3040 3036
		f 4 5043 5046 -5046 -5039
		mu 0 4 3036 3040 3041 3037
		f 4 5045 5047 -5042 -5040
		mu 0 4 3037 3041 3038 3034
		f 4 5049 -5051 -5049 5042
		mu 0 4 3038 3042 3043 3039
		f 4 5048 -5053 -5052 5044
		mu 0 4 3039 3043 3044 3040
		f 4 5051 5054 -5054 -5047
		mu 0 4 3040 3044 3045 3041
		f 4 5053 5055 -5050 -5048
		mu 0 4 3041 3045 3042 3038
		f 4 5057 -5059 -5057 5050
		mu 0 4 3042 3046 3047 3043
		f 4 5056 -5061 -5060 5052
		mu 0 4 3043 3047 3048 3044
		f 4 5059 5062 -5062 -5055
		mu 0 4 3044 3048 3049 3045
		f 4 5061 5063 -5058 -5056
		mu 0 4 3045 3049 3046 3042
		f 4 5065 -5067 -5065 5058
		mu 0 4 3046 3050 3051 3047
		f 4 5064 -5069 -5068 5060
		mu 0 4 3047 3051 3052 3048
		f 4 5067 5070 -5070 -5063
		mu 0 4 3048 3052 3053 3049
		f 4 5069 5071 -5066 -5064
		mu 0 4 3049 3053 3050 3046
		f 4 5073 -5075 -5073 5066
		mu 0 4 3050 2038 2041 3051
		f 4 5072 -5077 -5076 5068
		mu 0 4 3051 2041 2040 3052
		f 4 5075 5078 -5078 -5071
		mu 0 4 3052 2040 2039 3053
		f 4 5077 5079 -5074 -5072
		mu 0 4 3053 2039 2038 3050
		f 4 5081 -5083 -5081 3279
		mu 0 4 1999 3054 3055 1952
		f 4 5080 -5085 -5084 3280
		mu 0 4 1952 3055 3056 1953
		f 4 5083 5086 -5086 -3278
		mu 0 4 1953 3056 3057 1998
		f 4 5085 5087 -5082 -3239
		mu 0 4 1998 3057 3054 1999
		f 4 5089 -5091 -5089 3305
		mu 0 4 2025 3058 3059 1932
		f 4 5088 -5093 -5092 3306
		mu 0 4 1932 3059 3060 1933
		f 4 5091 5094 -5094 -3304
		mu 0 4 1933 3060 3061 2024
		f 4 5093 5095 -5090 -3265
		mu 0 4 2024 3061 3058 2025
		f 4 5096 5098 -5098 -3272
		mu 0 4 2032 3062 3063 2033
		f 4 5097 -5101 -5100 3272
		mu 0 4 2033 3063 3064 1969
		f 4 5099 -5103 -5102 3313
		mu 0 4 1969 3064 3065 1966
		f 4 5101 5103 -5097 -3312
		mu 0 4 1966 3065 3062 2032
		f 4 5105 -5107 -5105 5082
		mu 0 4 3054 3066 3067 3055
		f 4 5104 -5109 -5108 5084
		mu 0 4 3055 3067 3068 3056
		f 4 5107 5110 -5110 -5087
		mu 0 4 3056 3068 3069 3057
		f 4 5109 5111 -5106 -5088
		mu 0 4 3057 3069 3066 3054
		f 4 5113 -5115 -5113 5090
		mu 0 4 3058 3070 3071 3059
		f 4 5112 -5117 -5116 5092
		mu 0 4 3059 3071 3072 3060
		f 4 5115 5118 -5118 -5095
		mu 0 4 3060 3072 3073 3061
		f 4 5117 5119 -5114 -5096
		mu 0 4 3061 3073 3070 3058
		f 4 5120 5122 -5122 -5099
		mu 0 4 3062 3074 3075 3063
		f 4 5121 -5125 -5124 5100
		mu 0 4 3063 3075 3076 3064
		f 4 5123 -5127 -5126 5102
		mu 0 4 3064 3076 3077 3065
		f 4 5125 5127 -5121 -5104
		mu 0 4 3065 3077 3074 3062
		f 4 5129 -5131 -5129 5106
		mu 0 4 3066 3078 3079 3067
		f 4 5128 -5133 -5132 5108
		mu 0 4 3067 3079 3080 3068
		f 4 5131 5134 -5134 -5111
		mu 0 4 3068 3080 3081 3069
		f 4 5133 5135 -5130 -5112
		mu 0 4 3069 3081 3078 3066
		f 4 5137 -5139 -5137 5114
		mu 0 4 3070 3082 3083 3071
		f 4 5136 -5141 -5140 5116
		mu 0 4 3071 3083 3084 3072
		f 4 5139 5142 -5142 -5119
		mu 0 4 3072 3084 3085 3073
		f 4 5141 5143 -5138 -5120
		mu 0 4 3073 3085 3082 3070
		f 4 5144 5146 -5146 -5123
		mu 0 4 3074 3086 3087 3075
		f 4 5145 -5149 -5148 5124
		mu 0 4 3075 3087 3088 3076
		f 4 5147 -5151 -5150 5126
		mu 0 4 3076 3088 3089 3077
		f 4 5149 5151 -5145 -5128
		mu 0 4 3077 3089 3086 3074
		f 4 5153 -5155 -5153 5130
		mu 0 4 3078 2034 2037 3079
		f 4 5152 -5157 -5156 5132
		mu 0 4 3079 2037 2036 3080
		f 4 5155 5158 -5158 -5135
		mu 0 4 3080 2036 2035 3081
		f 4 5157 5159 -5154 -5136
		mu 0 4 3081 2035 2034 3078
		f 4 5161 -5163 -5161 5138
		mu 0 4 3082 2046 2049 3083
		f 4 5160 -5165 -5164 5140
		mu 0 4 3083 2049 2048 3084
		f 4 5163 5166 -5166 -5143
		mu 0 4 3084 2048 2047 3085
		f 4 5165 5167 -5162 -5144
		mu 0 4 3085 2047 2046 3082
		f 4 5168 5170 -5170 -5147
		mu 0 4 3086 2050 2053 3087
		f 4 5169 -5173 -5172 5148
		mu 0 4 3087 2053 2052 3088
		f 4 5171 -5175 -5174 5150
		mu 0 4 3088 2052 2051 3089
		f 4 5173 5175 -5169 -5152
		mu 0 4 3089 2051 2050 3086
		f 4 5176 5217 5525 -5217
		mu 0 4 3090 3091 3092 3093
		f 4 5177 5218 5524 -5218
		mu 0 4 3091 3094 3095 3092
		f 4 5178 5219 5522 -5219
		mu 0 4 3094 3096 3097 3095
		f 4 5179 5220 5520 -5220
		mu 0 4 3096 3098 3099 3097
		f 4 5180 5221 5518 -5221
		mu 0 4 3098 3100 3101 3099
		f 4 5181 5222 5516 -5222
		mu 0 4 3100 3102 3103 3101
		f 4 8506 5223 5514 8507
		mu 0 4 3104 3105 3106 3107
		f 4 5183 5224 5512 -5224
		mu 0 4 3105 3108 3109 3106
		f 4 5184 5225 5510 -5225
		mu 0 4 3108 3110 3111 3109
		f 4 5185 5216 5508 -5226
		mu 0 4 3110 3112 3113 3111
		f 4 5186 5227 5505 -5227
		mu 0 4 3114 3115 3116 3117
		f 4 5187 5228 5504 -5228
		mu 0 4 3115 3118 3119 3116
		f 4 5188 5229 5502 -5229
		mu 0 4 3118 3120 3121 3119
		f 4 5189 5230 5500 -5230
		mu 0 4 3120 3122 3123 3121
		f 4 5190 5231 5498 -5231
		mu 0 4 3122 3124 3125 3123
		f 4 5191 5232 5496 -5232
		mu 0 4 3124 3126 3127 3125
		f 4 8502 5233 5494 8503
		mu 0 4 3128 3129 3130 3131
		f 4 5193 5234 5492 -5234
		mu 0 4 3129 3132 3133 3130
		f 4 5194 5235 5490 -5235
		mu 0 4 3132 3134 3135 3133
		f 4 5195 5226 5488 -5236
		mu 0 4 3134 3136 3137 3135
		f 4 5197 5238 7032 -5238
		mu 0 4 3138 3139 3140 3141
		f 4 5199 5240 7025 -5240
		mu 0 4 3142 3143 3144 3145
		f 4 5200 5241 7023 -5241
		mu 0 4 3143 3146 3147 3144
		f 4 5201 5242 7021 -5242
		mu 0 4 3146 3148 3149 3147
		f 4 8498 5243 7019 8499
		mu 0 4 3150 3151 3152 3153
		f 4 5203 5244 7017 -5244
		mu 0 4 3151 3154 3155 3152
		f 4 5204 5245 7015 -5245
		mu 0 4 3154 3156 3157 3155
		f 4 5205 5236 7013 -5246
		mu 0 4 3156 3158 3159 3157
		f 3 -5669 -5670 5670
		mu 0 3 3160 3161 3162
		f 3 -5673 -5671 5673
		mu 0 3 3163 3160 3162
		f 3 -5676 -5674 5676
		mu 0 3 3164 3163 3162
		f 3 -5679 -5677 5679
		mu 0 3 3165 3164 3162
		f 3 -5682 -5680 5682
		mu 0 3 3166 3165 3162
		f 3 -5685 -5683 5685
		mu 0 3 3167 3166 3162
		f 5 -8545 -8418 -5688 -5686 5688
		mu 0 5 3168 3169 3170 3167 3162
		f 3 -5691 -5689 5691
		mu 0 3 3171 3168 3162
		f 3 -5694 -5692 5694
		mu 0 3 3172 3171 3162
		f 3 -5696 -5695 5669
		mu 0 3 3161 3172 3162
		f 4 5206 5247 -5249 -5247
		mu 0 4 3173 3174 3175 3176
		f 4 5207 5249 -5251 -5248
		mu 0 4 3174 3177 3178 3175
		f 4 5208 5251 -5253 -5250
		mu 0 4 3177 3179 3180 3178
		f 4 5209 5253 -5255 -5252
		mu 0 4 3179 3181 3182 3180
		f 4 5210 5255 -5257 -5254
		mu 0 4 3181 3183 3184 3182
		f 4 5211 5257 -5259 -5256
		mu 0 4 3183 3185 3186 3184
		f 4 8488 5259 -8487 8489
		mu 0 4 3187 3188 3189 3190
		f 4 5213 5261 -5263 -5260
		mu 0 4 3188 3191 3192 3189
		f 4 5214 5263 -5265 -5262
		mu 0 4 3191 3193 3194 3192
		f 4 5215 5246 -5266 -5264
		mu 0 4 3193 3173 3176 3194
		f 4 5248 5267 -5269 -5267
		mu 0 4 3176 3175 3195 3196
		f 4 5250 5269 -5271 -5268
		mu 0 4 3175 3178 3197 3195
		f 4 5252 5271 -5273 -5270
		mu 0 4 3178 3180 3198 3197
		f 4 5254 5273 -5275 -5272
		mu 0 4 3180 3182 3199 3198
		f 4 5256 5275 -5277 -5274
		mu 0 4 3182 3184 3200 3199
		f 4 5258 5277 -5279 -5276
		mu 0 4 3184 3186 3201 3200
		f 4 8486 5279 -8485 8487
		mu 0 4 3190 3189 3202 3203
		f 4 5262 5281 -5283 -5280
		mu 0 4 3189 3192 3204 3202
		f 4 5264 5283 -5285 -5282
		mu 0 4 3192 3194 3205 3204
		f 4 5265 5266 -5286 -5284
		mu 0 4 3194 3176 3196 3205
		f 4 5268 5287 5540 -5287
		mu 0 4 3196 3195 3206 3207
		f 4 5270 5289 5538 -5288
		mu 0 4 3195 3197 3208 3206
		f 4 5272 5291 5536 -5290
		mu 0 4 3197 3198 3209 3208
		f 4 5274 5293 5534 -5292
		mu 0 4 3198 3199 3210 3209
		f 4 5276 5295 5532 -5294
		mu 0 4 3199 3200 3211 3210
		f 4 5278 5297 5530 -5296
		mu 0 4 3200 3201 3212 3211
		f 4 8484 5299 5528 8485
		mu 0 4 3203 3202 3213 3214
		f 4 5282 5301 5545 -5300
		mu 0 4 3202 3204 3215 3213
		f 4 5284 5303 5544 -5302
		mu 0 4 3204 3205 3216 3215
		f 4 5285 5286 5542 -5304
		mu 0 4 3205 3196 3207 3216
		f 4 5288 5307 5440 -5307
		mu 0 4 3217 3218 3219 3220
		f 4 5290 5309 5438 -5308
		mu 0 4 3218 3221 3222 3219
		f 4 5292 5311 5436 -5310
		mu 0 4 3221 3223 3224 3222
		f 4 5294 5313 5434 -5312
		mu 0 4 3223 3225 3226 3224
		f 4 5296 5315 5432 -5314
		mu 0 4 3225 3227 3228 3226
		f 4 5298 5317 5430 -5316
		mu 0 4 3227 3229 3230 3228
		f 4 8478 5319 5428 8479
		mu 0 4 3231 3232 3233 3234
		f 4 5302 5321 5445 -5320
		mu 0 4 3232 3235 3236 3233
		f 4 5304 5323 5444 -5322
		mu 0 4 3235 3237 3238 3236
		f 4 5305 5306 5442 -5324
		mu 0 4 3237 3217 3220 3238
		f 4 -5329 5326 6196 -5328
		mu 0 4 3239 3240 3241 3242
		f 4 -5331 5327 6197 -5330
		mu 0 4 3243 3239 3242 3244
		f 4 -5333 5329 6180 -5332
		mu 0 4 3245 3243 3244 3246
		f 4 -5335 5331 6182 -5334
		mu 0 4 3247 3245 3246 3248
		f 4 -5337 5333 6184 8463
		mu 0 4 3249 3247 3248 3250
		f 4 -5339 5335 6186 -5338
		mu 0 4 3251 3252 3253 3254
		f 4 -5341 5337 6188 -5340
		mu 0 4 3255 3251 3254 3256
		f 4 -5343 5339 6190 -5342
		mu 0 4 3257 3255 3256 3258
		f 4 -5345 5341 6192 -5344
		mu 0 4 3259 3257 3258 3260
		f 4 -5349 5346 6156 -5348
		mu 0 4 3261 3262 3263 3264
		f 4 -5351 5347 6157 -5350
		mu 0 4 3265 3261 3264 3266
		f 4 -5353 5349 6140 -5352
		mu 0 4 3267 3265 3266 3268
		f 4 -5355 5351 6142 -5354
		mu 0 4 3269 3267 3268 3270
		f 4 -5357 5353 6144 8467
		mu 0 4 3271 3269 3270 3272
		f 4 -5359 5355 6146 -5358
		mu 0 4 3273 3274 3275 3276
		f 4 -5361 5357 6148 -5360
		mu 0 4 3277 3273 3276 3278
		f 4 -5363 5359 6150 -5362
		mu 0 4 3279 3277 3278 3280
		f 4 -5365 5361 6152 -5364
		mu 0 4 3281 3279 3280 3282
		f 4 -5369 5366 6177 -5368
		mu 0 4 3283 3284 3285 3286
		f 4 -5371 5367 6160 -5370
		mu 0 4 3287 3283 3286 3288
		f 4 -5373 5369 6162 -5372
		mu 0 4 3289 3287 3288 3290
		f 4 -5375 5371 6164 8471
		mu 0 4 3291 3289 3290 3292
		f 4 -5377 5373 6166 -5376
		mu 0 4 3293 3294 3295 3296
		f 4 -5379 5375 6168 -5378
		mu 0 4 3297 3293 3296 3298
		f 4 -5381 5377 6170 -5380
		mu 0 4 3299 3297 3298 3300
		f 4 -5383 5379 6172 -5382
		mu 0 4 3301 3299 3300 3302
		f 4 -5386 5383 6176 -5367
		mu 0 4 3284 3303 3304 3285
		f 4 -5389 5386 5370 -5388
		mu 0 4 3305 3306 3283 3287
		f 4 -5391 5387 5372 -5390
		mu 0 4 3307 3305 3287 3289
		f 4 -5393 5389 5374 8473
		mu 0 4 3308 3307 3289 3291
		f 4 -5395 5391 5376 -5394
		mu 0 4 3309 3310 3294 3293
		f 4 -5397 5393 5378 -5396
		mu 0 4 3311 3309 3293 3297
		f 4 -5399 5395 5380 -5398
		mu 0 4 3312 3311 3297 3299
		f 4 -5401 5397 5382 -5400
		mu 0 4 3313 3312 3299 3301
		f 4 -5403 5399 5384 -5402
		mu 0 4 3314 3313 3301 3303
		f 4 -5405 5401 5385 -5404
		mu 0 4 3315 3314 3303 3284
		f 4 -5406 5403 5368 -5387
		mu 0 4 3306 3315 3284 3283
		f 4 -5409 5406 5392 8475
		mu 0 4 3316 3317 3307 3308
		f 4 -5411 5407 5394 -5410
		mu 0 4 3318 3319 3310 3309
		f 4 -5413 5409 5396 -5412
		mu 0 4 3320 3318 3309 3311
		f 4 -5415 5411 5398 -5414
		mu 0 4 3321 3320 3311 3312
		f 4 -5417 5413 5400 -5416
		mu 0 4 3322 3321 3312 3313
		f 4 -5419 5415 5402 -5418
		mu 0 4 3323 3322 3313 3314
		f 4 -5421 5417 5404 -5420
		mu 0 4 3324 3323 3314 3315
		f 4 -5423 5419 5405 -5422
		mu 0 4 3325 3324 3315 3306
		f 4 -5425 5421 5388 -5424
		mu 0 4 3326 3325 3306 3305
		f 4 -5426 5423 5390 -5407
		mu 0 4 3317 3326 3305 3307
		f 4 -5429 5426 5408 8477
		mu 0 4 3234 3233 3317 3316
		f 4 -5431 5427 5410 -5430
		mu 0 4 3228 3230 3319 3318
		f 4 -5433 5429 5412 -5432
		mu 0 4 3226 3228 3318 3320
		f 4 -5435 5431 5414 -5434
		mu 0 4 3224 3226 3320 3321
		f 4 -5437 5433 5416 -5436
		mu 0 4 3222 3224 3321 3322
		f 4 -5439 5435 5418 -5438
		mu 0 4 3219 3222 3322 3323
		f 4 -5441 5437 5420 -5440
		mu 0 4 3220 3219 3323 3324
		f 4 -5443 5439 5422 -5442
		mu 0 4 3238 3220 3324 3325
		f 4 -5445 5441 5424 -5444
		mu 0 4 3236 3238 3325 3326
		f 4 -5446 5443 5425 -5427
		mu 0 4 3233 3236 3326 3317
		f 4 -5449 5446 -5216 -5448
		mu 0 4 3327 3328 3329 3330
		f 4 -5451 5447 -5215 -5450
		mu 0 4 3331 3327 3330 3332
		f 4 -5453 5449 -5214 -5452
		mu 0 4 3333 3331 3332 3334
		f 4 -5455 5451 -8489 8491
		mu 0 4 3335 3333 3334 3336
		f 4 -5457 5453 -5212 -5456
		mu 0 4 3337 3338 3339 3340
		f 4 -5459 5455 -5211 -5458
		mu 0 4 3341 3337 3340 3342
		f 4 -5461 5457 -5210 -5460
		mu 0 4 3343 3341 3342 3344
		f 4 -5463 5459 -5209 -5462
		mu 0 4 3345 3343 3344 3346
		f 4 -5465 5461 -5208 -5464
		mu 0 4 3347 3345 3346 3348
		f 4 -5466 5463 -5207 -5447
		mu 0 4 3349 3347 3348 3350
		f 4 -5469 5466 5448 -5468
		mu 0 4 3351 3352 3328 3327
		f 4 -5471 5467 5450 -5470
		mu 0 4 3353 3351 3327 3331
		f 4 -5473 5469 5452 -5472
		mu 0 4 3354 3353 3331 3333
		f 4 -5475 5471 5454 8493
		mu 0 4 3355 3354 3333 3335
		f 4 -5477 5473 5456 -5476
		mu 0 4 3356 3357 3338 3337
		f 4 -5479 5475 5458 -5478
		mu 0 4 3358 3356 3337 3341
		f 4 -5481 5477 5460 -5480
		mu 0 4 3359 3358 3341 3343
		f 4 -5483 5479 5462 -5482
		mu 0 4 3360 3359 3343 3345
		f 4 -5485 5481 5464 -5484
		mu 0 4 3361 3360 3345 3347
		f 4 -5486 5483 5465 -5467
		mu 0 4 3362 3361 3347 3349
		f 4 -5489 5486 -5206 -5488
		mu 0 4 3135 3137 3158 3156
		f 4 -5491 5487 -5205 -5490
		mu 0 4 3133 3135 3156 3154
		f 4 -5493 5489 -5204 -5492
		mu 0 4 3130 3133 3154 3151
		f 4 -5495 5491 -8499 8501
		mu 0 4 3131 3130 3151 3150
		f 4 -5497 5493 -5202 -5496
		mu 0 4 3125 3127 3148 3146
		f 4 -5499 5495 -5201 -5498
		mu 0 4 3123 3125 3146 3143
		f 4 -5501 5497 -5200 -5500
		mu 0 4 3121 3123 3143 3142
		f 4 -5503 5499 -5199 -5502
		mu 0 4 3119 3121 3142 3139
		f 4 -5505 5501 -5198 -5504
		mu 0 4 3116 3119 3139 3138
		f 4 -5506 5503 -5197 -5487
		mu 0 4 3117 3116 3138 3363
		f 4 -5509 5506 -5196 -5508
		mu 0 4 3111 3113 3136 3134
		f 4 -5511 5507 -5195 -5510
		mu 0 4 3109 3111 3134 3132
		f 4 -5513 5509 -5194 -5512
		mu 0 4 3106 3109 3132 3129
		f 4 -5515 5511 -8503 8505
		mu 0 4 3107 3106 3129 3128
		f 4 -5517 5513 -5192 -5516
		mu 0 4 3101 3103 3126 3124
		f 4 -5519 5515 -5191 -5518
		mu 0 4 3099 3101 3124 3122
		f 4 -5521 5517 -5190 -5520
		mu 0 4 3097 3099 3122 3120
		f 4 -5523 5519 -5189 -5522
		mu 0 4 3095 3097 3120 3118
		f 4 -5525 5521 -5188 -5524
		mu 0 4 3092 3095 3118 3115
		f 4 -5526 5523 -5187 -5507
		mu 0 4 3093 3092 3115 3114
		f 4 -5529 5526 5550 8483
		mu 0 4 3214 3213 3364 3365
		f 4 -5531 5527 5552 -5530
		mu 0 4 3211 3212 3366 3367
		f 4 -5533 5529 5554 -5532
		mu 0 4 3210 3211 3367 3368
		f 4 -5535 5531 5556 -5534
		mu 0 4 3209 3210 3368 3369
		f 4 -5537 5533 5558 -5536
		mu 0 4 3208 3209 3369 3370
		f 4 -5539 5535 5560 -5538
		mu 0 4 3206 3208 3370 3371
		f 4 -5541 5537 5562 -5540
		mu 0 4 3207 3206 3371 3372
		f 4 -5543 5539 5564 -5542
		mu 0 4 3216 3207 3372 3373
		f 4 -5545 5541 5565 -5544
		mu 0 4 3215 3216 3373 3374
		f 4 -5546 5543 5548 -5527
		mu 0 4 3213 3215 3374 3364
		f 4 -5549 5546 -5303 -5548
		mu 0 4 3364 3374 3235 3232
		f 4 -5551 5547 -8479 8481
		mu 0 4 3365 3364 3232 3231
		f 4 -5553 5549 -5299 -5552
		mu 0 4 3367 3366 3229 3227
		f 4 -5555 5551 -5297 -5554
		mu 0 4 3368 3367 3227 3225
		f 4 -5557 5553 -5295 -5556
		mu 0 4 3369 3368 3225 3223
		f 4 -5559 5555 -5293 -5558
		mu 0 4 3370 3369 3223 3221
		f 4 -5561 5557 -5291 -5560
		mu 0 4 3371 3370 3221 3218
		f 4 -5563 5559 -5289 -5562
		mu 0 4 3372 3371 3218 3217
		f 4 -5565 5561 -5306 -5564
		mu 0 4 3373 3372 3217 3237
		f 4 -5566 5563 -5305 -5547
		mu 0 4 3374 3373 3237 3235
		f 4 -5177 5566 5625 -5568
		mu 0 4 3375 3376 3377 3378
		f 4 -5178 5567 5608 -5570
		mu 0 4 3379 3375 3378 3380
		f 4 -5179 5569 5610 -5572
		mu 0 4 3381 3379 3380 3382
		f 4 -5180 5571 5612 -5574
		mu 0 4 3383 3381 3382 3384
		f 4 -5181 5573 5614 -5576
		mu 0 4 3385 3383 3384 3386
		f 4 -5182 5575 5616 -5578
		mu 0 4 3387 3385 3386 3388
		f 4 -8507 8509 8508 -5580
		mu 0 4 3389 3390 3391 3392
		f 4 -5184 5579 5620 -5582
		mu 0 4 3393 3389 3392 3394
		f 4 -5185 5581 5622 -5584
		mu 0 4 3395 3393 3394 3396
		f 4 -5186 5583 5624 -5567
		mu 0 4 3376 3395 3396 3377
		f 4 -5589 5586 5628 -5588
		mu 0 4 3397 3398 3399 3400
		f 4 -5591 5587 5630 -5590
		mu 0 4 3401 3397 3400 3402
		f 4 -5593 5589 5632 -5592
		mu 0 4 3403 3401 3402 3404
		f 4 -5595 5591 5634 -5594
		mu 0 4 3405 3403 3404 3406
		f 4 -5597 5593 5636 -5596
		mu 0 4 3407 3405 3406 3408
		f 4 -8511 8513 8512 -5598
		mu 0 4 3409 3410 3411 3412
		f 4 -5601 5597 5640 -5600
		mu 0 4 3413 3409 3412 3414
		f 4 -5603 5599 5642 -5602
		mu 0 4 3415 3413 3414 3416
		f 4 -5605 5601 5644 -5604
		mu 0 4 3417 3415 3416 3418
		f 4 -5606 5603 5645 -5587
		mu 0 4 3398 3417 3418 3399
		f 4 -5609 5606 5588 -5608
		mu 0 4 3380 3378 3398 3397
		f 4 -5611 5607 5590 -5610
		mu 0 4 3382 3380 3397 3401
		f 4 -5613 5609 5592 -5612
		mu 0 4 3384 3382 3401 3403
		f 4 -5615 5611 5594 -5614
		mu 0 4 3386 3384 3403 3405
		f 4 -5617 5613 5596 -5616
		mu 0 4 3388 3386 3405 3407
		f 4 -8509 8511 8510 -5618
		mu 0 4 3392 3391 3410 3409
		f 4 -5621 5617 5600 -5620
		mu 0 4 3394 3392 3409 3413
		f 4 -5623 5619 5602 -5622
		mu 0 4 3396 3394 3413 3415
		f 4 -5625 5621 5604 -5624
		mu 0 4 3377 3396 3415 3417
		f 4 -5626 5623 5605 -5607
		mu 0 4 3378 3377 3417 3398
		f 4 -5629 5626 5570 -5628
		mu 0 4 3400 3399 3419 3420
		f 4 -5631 5627 5572 -5630
		mu 0 4 3402 3400 3420 3421
		f 4 -5633 5629 5574 -5632
		mu 0 4 3404 3402 3421 3422
		f 4 -5635 5631 5576 -5634
		mu 0 4 3406 3404 3422 3423
		f 4 -5637 5633 5578 -5636
		mu 0 4 3408 3406 3423 3424
		f 4 -8513 8515 8514 -5638
		mu 0 4 3412 3411 3425 3426
		f 4 -5641 5637 5582 -5640
		mu 0 4 3414 3412 3426 3427
		f 4 -5643 5639 5584 -5642
		mu 0 4 3416 3414 3427 3428
		f 4 -5645 5641 5585 -5644
		mu 0 4 3418 3416 3428 3429
		f 4 -5646 5643 5568 -5627
		mu 0 4 3399 3418 3429 3419
		f 4 -5569 5646 5648 -5648
		mu 0 4 3419 3429 3430 3431
		f 4 -5571 5647 5650 -5650
		mu 0 4 3420 3419 3431 3432
		f 4 -5573 5649 5652 -5652
		mu 0 4 3421 3420 3432 3433
		f 4 -5575 5651 5654 -5654
		mu 0 4 3422 3421 3433 3434
		f 4 -5577 5653 5656 -5656
		mu 0 4 3423 3422 3434 3435
		f 4 -5579 5655 5658 -5658
		mu 0 4 3424 3423 3435 3436
		f 4 -8515 8517 8516 -5660
		mu 0 4 3426 3425 3437 3438
		f 4 -5583 5659 5662 -5662
		mu 0 4 3427 3426 3438 3439
		f 4 -5585 5661 5664 -5664
		mu 0 4 3428 3427 3439 3440
		f 4 -5586 5663 5665 -5647
		mu 0 4 3429 3428 3440 3430
		f 4 -5649 5666 5715 -5668
		mu 0 4 3431 3430 3441 3442
		f 4 -5651 5667 5698 -5672
		mu 0 4 3432 3431 3442 3443
		f 4 -5653 5671 5700 -5675
		mu 0 4 3433 3432 3443 3444
		f 4 -5655 5674 5702 -5678
		mu 0 4 3434 3433 3444 3445
		f 4 -5657 5677 5704 -5681
		mu 0 4 3435 3434 3445 3446
		f 4 -5659 5680 5706 -5684
		mu 0 4 3436 3435 3446 3447
		f 4 -8517 8519 8518 -5687
		mu 0 4 3438 3437 3448 3449
		f 4 -5663 5686 5710 -5690
		mu 0 4 3439 3438 3449 3450
		f 4 -5665 5689 5712 -5693
		mu 0 4 3440 3439 3450 3451
		f 4 -5666 5692 5714 -5667
		mu 0 4 3430 3440 3451 3441
		f 4 -5699 5696 5722 -5698
		mu 0 4 3443 3442 3452 3453
		f 4 -5701 5697 5724 -5700
		mu 0 4 3444 3443 3453 3454
		f 4 -5703 5699 5726 -5702
		mu 0 4 3445 3444 3454 3455
		f 4 -5705 5701 5728 -5704
		mu 0 4 3446 3445 3455 3456
		f 4 -5707 5703 5730 -5706
		mu 0 4 3447 3446 3456 3457
		f 4 -8519 8521 8520 -5708
		mu 0 4 3449 3448 3458 3459
		f 4 -5711 5707 5734 -5710
		mu 0 4 3450 3449 3459 3460
		f 4 -5713 5709 5735 -5712
		mu 0 4 3451 3450 3460 3461
		f 4 -5715 5711 5718 -5714
		mu 0 4 3441 3451 3461 3462
		f 4 -5716 5713 5720 -5697
		mu 0 4 3442 3441 3462 3452
		f 4 -5719 5716 5755 -5718
		mu 0 4 3462 3461 3463 3464
		f 4 -5721 5717 5738 -5720
		mu 0 4 3452 3462 3464 3465
		f 4 -5723 5719 5740 -5722
		mu 0 4 3453 3452 3465 3466
		f 4 -5725 5721 5742 -5724
		mu 0 4 3454 3453 3466 3467
		f 4 -5727 5723 5744 -5726
		mu 0 4 3455 3454 3467 3468
		f 4 -5729 5725 5746 -5728
		mu 0 4 3456 3455 3468 3469
		f 4 -5731 5727 5748 -5730
		mu 0 4 3457 3456 3469 3470
		f 4 -8521 8523 8522 -5732
		mu 0 4 3459 3458 3471 3472
		f 4 -5735 5731 5752 -5734
		mu 0 4 3460 3459 3472 3473
		f 4 -5736 5733 5754 -5717
		mu 0 4 3461 3460 3473 3463
		f 4 -5739 5736 5758 -5738
		mu 0 4 3465 3464 3474 3475
		f 4 -5741 5737 5760 -5740
		mu 0 4 3466 3465 3475 3476
		f 4 -5743 5739 5762 -5742
		mu 0 4 3467 3466 3476 3477
		f 4 -5745 5741 5764 -5744
		mu 0 4 3468 3467 3477 3478
		f 4 -5747 5743 5766 -5746
		mu 0 4 3469 3468 3478 3479
		f 4 -5749 5745 5768 -5748
		mu 0 4 3470 3469 3479 3480
		f 4 -8523 8525 8524 -5750
		mu 0 4 3472 3471 3481 3482
		f 4 -5753 5749 5772 -5752
		mu 0 4 3473 3472 3482 3483
		f 4 -5755 5751 5774 -5754
		mu 0 4 3463 3473 3483 3484
		f 4 -5756 5753 5775 -5737
		mu 0 4 3464 3463 3484 3474
		f 4 -5759 5756 5780 -5758
		mu 0 4 3475 3474 3485 3486
		f 4 -5761 5757 5782 -5760
		mu 0 4 3476 3475 3486 3487
		f 4 -5763 5759 5784 -5762
		mu 0 4 3477 3476 3487 3488
		f 4 -5765 5761 5786 -5764
		mu 0 4 3478 3477 3488 3489
		f 4 -5767 5763 5788 -5766
		mu 0 4 3479 3478 3489 3490
		f 4 -5769 5765 5790 -5768
		mu 0 4 3480 3479 3490 3491
		f 4 -8525 8527 8526 -5770
		mu 0 4 3482 3481 3492 3493
		f 4 -5773 5769 5794 -5772
		mu 0 4 3483 3482 3493 3494
		f 4 -5775 5771 5795 -5774
		mu 0 4 3484 3483 3494 3495
		f 4 -5776 5773 5778 -5757
		mu 0 4 3474 3484 3495 3485
		f 4 -5779 5776 5798 -5778
		mu 0 4 3485 3495 3496 3497
		f 4 -5781 5777 5800 -5780
		mu 0 4 3486 3485 3497 3498
		f 4 -5783 5779 5802 -5782
		mu 0 4 3487 3486 3498 3499
		f 4 -5785 5781 5804 -5784
		mu 0 4 3488 3487 3499 3500
		f 4 -5787 5783 5806 -5786
		mu 0 4 3489 3488 3500 3501
		f 4 -5789 5785 5808 -5788
		mu 0 4 3490 3489 3501 3502
		f 4 -5791 5787 5810 -5790
		mu 0 4 3491 3490 3502 3503
		f 4 -8527 8529 8528 -5792
		mu 0 4 3493 3492 3504 3505
		f 4 -5795 5791 5814 -5794
		mu 0 4 3494 3493 3505 3506
		f 4 -5796 5793 5815 -5777
		mu 0 4 3495 3494 3506 3496
		f 4 -5799 5796 5818 -5798
		mu 0 4 3497 3496 3507 3508
		f 4 -5801 5797 5820 -5800
		mu 0 4 3498 3497 3508 3509
		f 4 -5803 5799 5822 -5802
		mu 0 4 3499 3498 3509 3510
		f 4 -5805 5801 5824 -5804
		mu 0 4 3500 3499 3510 3511
		f 4 -5807 5803 5826 -5806
		mu 0 4 3501 3500 3511 3512
		f 4 -5809 5805 5828 -5808
		mu 0 4 3502 3501 3512 3513
		f 4 -5811 5807 5830 -5810
		mu 0 4 3503 3502 3513 3514
		f 4 -8529 8531 8530 -5812
		mu 0 4 3505 3504 3515 3516
		f 4 -5815 5811 5834 -5814
		mu 0 4 3506 3505 3516 3517
		f 4 -5816 5813 5835 -5797
		mu 0 4 3496 3506 3517 3507
		f 4 -5819 5816 5838 -5818
		mu 0 4 3508 3507 3518 3519
		f 4 -5821 5817 5840 -5820
		mu 0 4 3509 3508 3519 3520
		f 4 -5823 5819 5842 -5822
		mu 0 4 3510 3509 3520 3521
		f 4 -5825 5821 5844 -5824
		mu 0 4 3511 3510 3521 3522
		f 4 -5827 5823 5846 -5826
		mu 0 4 3512 3511 3522 3523
		f 4 -5829 5825 5848 -5828
		mu 0 4 3513 3512 3523 3524
		f 4 -5831 5827 5850 -5830
		mu 0 4 3514 3513 3524 3525
		f 4 -8531 8533 8532 -5832
		mu 0 4 3516 3515 3526 3527
		f 4 -5835 5831 5854 -5834
		mu 0 4 3517 3516 3527 3528
		f 4 -5836 5833 5855 -5817
		mu 0 4 3507 3517 3528 3518
		f 4 -5839 5836 5858 -5838
		mu 0 4 3519 3518 3529 3530
		f 4 -5841 5837 5860 -5840
		mu 0 4 3520 3519 3530 3531
		f 4 -5843 5839 5862 -5842
		mu 0 4 3521 3520 3531 3532
		f 4 -5845 5841 5864 -5844
		mu 0 4 3522 3521 3532 3533
		f 4 -5847 5843 5866 -5846
		mu 0 4 3523 3522 3533 3534
		f 4 -5849 5845 5868 -5848
		mu 0 4 3524 3523 3534 3535
		f 4 -5851 5847 5870 -5850
		mu 0 4 3525 3524 3535 3536
		f 4 -8533 8535 8534 -5852
		mu 0 4 3527 3526 3537 3538
		f 4 -5855 5851 5874 -5854
		mu 0 4 3528 3527 3538 3539
		f 4 -5856 5853 5875 -5837
		mu 0 4 3518 3528 3539 3529
		f 4 -5859 5856 5878 -5858
		mu 0 4 3530 3529 3540 3541
		f 4 -5861 5857 5880 -5860
		mu 0 4 3531 3530 3541 3542
		f 4 -5863 5859 5882 -5862
		mu 0 4 3532 3531 3542 3543
		f 4 -5865 5861 5884 -5864
		mu 0 4 3533 3532 3543 3544
		f 4 -5867 5863 5886 -5866
		mu 0 4 3534 3533 3544 3545
		f 4 -5869 5865 5888 -5868
		mu 0 4 3535 3534 3545 3546
		f 4 -5871 5867 5890 -5870
		mu 0 4 3536 3535 3546 3547
		f 4 -8535 8537 8536 -5872
		mu 0 4 3538 3537 3548 3549
		f 4 -5875 5871 5894 -5874
		mu 0 4 3539 3538 3549 3550
		f 4 -5876 5873 5895 -5857
		mu 0 4 3529 3539 3550 3540
		f 4 -5879 5876 5898 -5878
		mu 0 4 3541 3540 3551 3552
		f 4 -5881 5877 5900 -5880
		mu 0 4 3542 3541 3552 3553
		f 4 -5883 5879 5902 -5882
		mu 0 4 3543 3542 3553 3554
		f 4 -5885 5881 5904 -5884
		mu 0 4 3544 3543 3554 3555
		f 4 -5887 5883 5906 -5886
		mu 0 4 3545 3544 3555 3556
		f 4 -5889 5885 5908 -5888
		mu 0 4 3546 3545 3556 3557
		f 4 -5891 5887 5910 -5890
		mu 0 4 3547 3546 3557 3558
		f 4 -8537 8539 8538 -5892
		mu 0 4 3549 3548 3559 3560
		f 4 -5895 5891 5914 -5894
		mu 0 4 3550 3549 3560 3561
		f 4 -5896 5893 5915 -5877
		mu 0 4 3540 3550 3561 3551
		f 4 -5899 5896 5922 -5898
		mu 0 4 3552 3551 3562 3563
		f 4 -5901 5897 5924 -5900
		mu 0 4 3553 3552 3563 3564
		f 4 -5903 5899 5926 -5902
		mu 0 4 3554 3553 3564 3565
		f 4 -5905 5901 5928 -5904
		mu 0 4 3555 3554 3565 3566
		f 4 -5907 5903 5930 -5906
		mu 0 4 3556 3555 3566 3567
		f 4 -5909 5905 5932 -5908
		mu 0 4 3557 3556 3567 3568
		f 4 -5911 5907 5934 -5910
		mu 0 4 3558 3557 3568 3569
		f 4 -8539 8541 8540 -5912
		mu 0 4 3560 3559 3570 3571
		f 4 -5915 5911 5918 -5914
		mu 0 4 3561 3560 3571 3572
		f 4 -5916 5913 5920 -5897
		mu 0 4 3551 3561 3572 3562
		f 4 -5919 5916 5938 -5918
		mu 0 4 3572 3571 3573 3574
		f 4 -5921 5917 5940 -5920
		mu 0 4 3562 3572 3574 3575
		f 4 -5923 5919 5942 -5922
		mu 0 4 3563 3562 3575 3576
		f 4 -5925 5921 5944 -5924
		mu 0 4 3564 3563 3576 3577
		f 4 -5927 5923 5946 -5926
		mu 0 4 3565 3564 3577 3578
		f 4 -5929 5925 5948 -5928
		mu 0 4 3566 3565 3578 3579
		f 4 -5931 5927 5950 -5930
		mu 0 4 3567 3566 3579 3580
		f 4 -5933 5929 5952 -5932
		mu 0 4 3568 3567 3580 3581
		f 4 -5935 5931 5954 -5934
		mu 0 4 3569 3568 3581 3582
		f 4 -8541 8543 8542 -5917
		mu 0 4 3571 3570 3583 3573
		f 4 -5939 5936 5690 -5938
		mu 0 4 3574 3573 3168 3171
		f 4 -5941 5937 5693 -5940
		mu 0 4 3575 3574 3171 3172
		f 4 -5943 5939 5695 -5942
		mu 0 4 3576 3575 3172 3161
		f 4 -5945 5941 5668 -5944
		mu 0 4 3577 3576 3161 3160
		f 4 -5947 5943 5672 -5946
		mu 0 4 3578 3577 3160 3163
		f 4 -5949 5945 5675 -5948
		mu 0 4 3579 3578 3163 3164
		f 4 -5951 5947 5678 -5950
		mu 0 4 3580 3579 3164 3165
		f 4 -5953 5949 5681 -5952
		mu 0 4 3581 3580 3165 3166
		f 4 -5955 5951 5684 -5954
		mu 0 4 3582 3581 3166 3167
		f 4 -8543 8545 8544 -5937
		mu 0 4 3573 3583 3169 3168
		f 4 5196 5957 -5959 -5957
		mu 0 4 3363 3138 3584 3585
		f 4 5237 7033 -5961 -5958
		mu 0 4 3138 3141 3586 3584
		f 4 5485 5961 -5963 -5960
		mu 0 4 3361 3362 3587 3588
		f 4 -5237 5956 5963 7011
		mu 0 4 3589 3363 3585 3590
		f 4 5198 5965 -5967 -5965
		mu 0 4 3139 3142 3591 3592
		f 4 5239 7027 -5969 -5966
		mu 0 4 3142 3145 3593 3591
		f 4 5482 5969 -5971 -5968
		mu 0 4 3359 3360 3594 3595
		f 4 -5239 5964 5971 7030
		mu 0 4 3140 3139 3592 3596
		f 4 5308 5973 6000 -5973
		mu 0 4 3597 3598 3599 3600
		f 4 5310 5974 6017 -5974
		mu 0 4 3598 3601 3602 3599
		f 4 5312 5975 6016 -5975
		mu 0 4 3601 3603 3604 3602
		f 4 5314 5976 6014 -5976
		mu 0 4 3603 3605 3606 3604
		f 4 5316 5977 6012 -5977
		mu 0 4 3605 3607 3608 3606
		f 4 5318 5978 6010 -5978
		mu 0 4 3607 3609 3610 3608
		f 4 8458 5979 6008 8459
		mu 0 4 3611 3612 3613 3614
		f 4 5322 5980 6006 -5980
		mu 0 4 3612 3615 3616 3613
		f 4 5324 5981 6004 -5981
		mu 0 4 3615 3617 3618 3616
		f 4 5325 5972 6002 -5982
		mu 0 4 3617 3597 3600 3618;
	setAttr ".fc[3000:3499]"
		f 3 9346 9348 -9350
		mu 0 3 3619 3620 3621
		f 3 9351 9352 -9349
		mu 0 3 3622 3623 3624
		f 3 9354 9355 -9353
		mu 0 3 3625 3626 3627
		f 3 5985 5994 -5994
		mu 0 3 3628 3629 3630
		f 5 5986 7182 7161 5995 -5995
		mu 0 5 3631 3632 3633 3634 3635
		f 3 5987 5996 -5996
		mu 0 3 3636 3637 3638
		f 3 9358 9360 -9362
		mu 0 3 3639 3640 3641
		f 3 9363 9364 -9361
		mu 0 3 3642 3643 3644
		f 3 9366 9367 -9365
		mu 0 3 3645 3646 3647
		f 3 5991 5992 -5998
		mu 0 3 3648 3649 3650
		f 4 -6001 5998 6032 -6000
		mu 0 4 3600 3599 3651 3652
		f 4 -6003 5999 6034 -6002
		mu 0 4 3618 3600 3652 3653
		f 4 -6005 6001 6036 -6004
		mu 0 4 3616 3618 3653 3654
		f 4 -6007 6003 6037 -6006
		mu 0 4 3613 3616 3654 3655
		f 4 -6009 6005 6020 8457
		mu 0 4 3614 3613 3655 3656
		f 4 -6011 6007 6022 -6010
		mu 0 4 3608 3610 3657 3658
		f 4 -6013 6009 6024 -6012
		mu 0 4 3606 3608 3658 3659
		f 4 -6015 6011 6026 -6014
		mu 0 4 3604 3606 3659 3660
		f 4 -6017 6013 6028 -6016
		mu 0 4 3602 3604 3660 3661
		f 4 -6018 6015 6030 -5999
		mu 0 4 3599 3602 3661 3651
		f 4 -6021 6018 7078 8455
		mu 0 4 3656 3655 3662 3663
		f 4 -6023 6019 7080 -6022
		mu 0 4 3658 3657 3664 3665
		f 4 -6025 6021 7082 -6024
		mu 0 4 3659 3658 3665 3666
		f 4 -6027 6023 7084 -6026
		mu 0 4 3660 3659 3666 3667
		f 4 -6029 6025 7086 -6028
		mu 0 4 3661 3660 3667 3668
		f 4 -6031 6027 7088 -6030
		mu 0 4 3651 3661 3668 3669
		f 4 -6033 6029 7089 -6032
		mu 0 4 3652 3651 3669 3670
		f 4 -6035 6031 7072 -6034
		mu 0 4 3653 3652 3670 3671
		f 4 -6037 6033 7074 -6036
		mu 0 4 3654 3653 3671 3672
		f 4 -6038 6035 7076 -6019
		mu 0 4 3655 3654 3672 3662
		f 4 -6041 6038 6097 -6040
		mu 0 4 3673 3674 3675 3676
		f 4 -7152 7154 7153 -6042
		mu 0 4 3677 3678 3679 3680
		f 4 -6045 6041 6082 -6044
		mu 0 4 3681 3677 3680 3682
		f 4 -6047 6043 6084 -6046
		mu 0 4 3683 3681 3682 3684
		f 4 -6049 6045 6086 -6048
		mu 0 4 3685 3683 3684 3686
		f 4 -6051 6047 6088 -6050
		mu 0 4 3687 3685 3686 3688
		f 4 -6053 6049 6090 -6052
		mu 0 4 3689 3687 3688 3690
		f 4 -6055 6051 6092 -6054
		mu 0 4 3691 3689 3690 3692
		f 4 -6057 6053 6094 -6056
		mu 0 4 3693 3691 3692 3694
		f 4 -6058 6055 6096 -6039
		mu 0 4 3674 3693 3694 3675
		f 4 -6061 6058 7226 -6060
		mu 0 4 3695 3696 3697 3698
		f 4 -6063 6059 7228 -6062
		mu 0 4 3699 3695 3698 3700
		f 4 -6065 6061 7230 -6064
		mu 0 4 3701 3699 3700 3702
		f 4 -6067 6063 7231 -6066
		mu 0 4 3703 3701 3702 3704
		f 4 -8903 8904 8906 -8908
		mu 0 4 3705 3706 3707 3708
		f 4 -6071 6067 7216 -6070
		mu 0 4 3709 3710 3711 3712
		f 4 -6073 6069 7218 -6072
		mu 0 4 3713 3709 3712 3714
		f 4 -6075 6071 7220 -6074
		mu 0 4 3715 3713 3714 3716
		f 4 -6077 6073 7222 -6076
		mu 0 4 3717 3715 3716 3718
		f 4 -6078 6075 7224 -6059
		mu 0 4 3696 3717 3718 3697
		f 4 -7154 7156 8142 -6080
		mu 0 4 3680 3679 3719 3720
		f 4 -6083 6079 8144 -6082
		mu 0 4 3682 3680 3720 3721
		f 4 -6085 6081 8146 -6084
		mu 0 4 3684 3682 3721 3722
		f 4 -6087 6083 8148 -6086
		mu 0 4 3686 3684 3722 3723
		f 4 -6089 6085 8150 -6088
		mu 0 4 3688 3686 3723 3724
		f 4 -6091 6087 8152 -6090
		mu 0 4 3690 3688 3724 3725
		f 4 -6093 6089 8154 -6092
		mu 0 4 3692 3690 3725 3726
		f 4 -6095 6091 8156 -6094
		mu 0 4 3694 3692 3726 3727
		f 4 -6097 6093 8158 -6096
		mu 0 4 3675 3694 3727 3728
		f 4 -6098 6095 8159 -6079
		mu 0 4 3676 3675 3728 3729
		f 4 -7158 7160 7304 -6100
		mu 0 4 3730 3731 3732 3733
		f 4 -6103 6099 7306 -6102
		mu 0 4 3734 3730 3733 3735
		f 4 -6105 6101 7308 -6104
		mu 0 4 3736 3734 3735 3737
		f 4 -6217 6218 7274 -6222
		mu 0 4 3738 3739 3740 3741
		f 4 -6109 6105 7280 -6108
		mu 0 4 3742 3743 3744 3745
		f 4 -6111 6107 7282 -6110
		mu 0 4 3746 3742 3745 3747
		f 4 -6113 6109 7284 -6112
		mu 0 4 3748 3746 3747 3749
		f 4 -6225 6226 7290 -6230
		mu 0 4 3750 3751 3752 3753
		f 4 -6117 6113 7296 -6116
		mu 0 4 3754 3755 3756 3757
		f 4 -6118 6115 7298 -6099
		mu 0 4 3758 3754 3757 3759
		f 4 -8911 8912 8914 -8916
		mu 0 4 3760 3761 3762 3763
		f 4 -6123 6119 7720 -6122
		mu 0 4 3764 3765 3766 3767
		f 4 -6125 6121 7722 -6124
		mu 0 4 3768 3764 3767 3769
		f 4 -6127 6123 7724 -6126
		mu 0 4 3770 3768 3769 3771
		f 4 -6131 6127 7916 -6130
		mu 0 4 3772 3773 3774 3775
		f 4 -6135 6131 8108 -6134
		mu 0 4 3776 3777 3778 3779
		f 4 -6137 6133 8110 -6136
		mu 0 4 3780 3776 3779 3781
		f 4 -6138 6135 8111 -6119
		mu 0 4 3782 3780 3781 3783
		f 4 -6141 6138 5332 -6140
		mu 0 4 3268 3266 3243 3245
		f 4 -6143 6139 5334 -6142
		mu 0 4 3270 3268 3245 3247
		f 4 -6145 6141 5336 8465
		mu 0 4 3272 3270 3247 3249
		f 4 -6147 6143 5338 -6146
		mu 0 4 3276 3275 3252 3251
		f 4 -6149 6145 5340 -6148
		mu 0 4 3278 3276 3251 3255
		f 4 -6151 6147 5342 -6150
		mu 0 4 3280 3278 3255 3257
		f 4 -6153 6149 5344 -6152
		mu 0 4 3282 3280 3257 3259
		f 4 -6155 6151 5345 -6154
		mu 0 4 3263 3282 3259 3240
		f 4 -6157 6153 5328 -6156
		mu 0 4 3264 3263 3240 3239
		f 4 -6158 6155 5330 -6139
		mu 0 4 3266 3264 3239 3243
		f 4 -6161 6158 5352 -6160
		mu 0 4 3288 3286 3265 3267
		f 4 -6163 6159 5354 -6162
		mu 0 4 3290 3288 3267 3269
		f 4 -6165 6161 5356 8469
		mu 0 4 3292 3290 3269 3271
		f 4 -6167 6163 5358 -6166
		mu 0 4 3296 3295 3274 3273
		f 4 -6169 6165 5360 -6168
		mu 0 4 3298 3296 3273 3277
		f 4 -6171 6167 5362 -6170
		mu 0 4 3300 3298 3277 3279
		f 4 -6173 6169 5364 -6172
		mu 0 4 3302 3300 3279 3281
		f 4 -6175 6171 5365 -6174
		mu 0 4 3304 3302 3281 3262
		f 4 -6177 6173 5348 -6176
		mu 0 4 3285 3304 3262 3261
		f 4 -6178 6175 5350 -6159
		mu 0 4 3286 3285 3261 3265
		f 4 -6181 6178 -5325 -6180
		mu 0 4 3246 3244 3617 3615
		f 4 -6183 6179 -5323 -6182
		mu 0 4 3248 3246 3615 3612
		f 4 -6185 6181 -8459 8461
		mu 0 4 3250 3248 3612 3611
		f 4 -6187 6183 -5319 -6186
		mu 0 4 3254 3253 3609 3607
		f 4 -6189 6185 -5317 -6188
		mu 0 4 3256 3254 3607 3605
		f 4 -6191 6187 -5315 -6190
		mu 0 4 3258 3256 3605 3603
		f 4 -6193 6189 -5313 -6192
		mu 0 4 3260 3258 3603 3601
		f 4 -6195 6191 -5311 -6194
		mu 0 4 3241 3260 3601 3598
		f 4 -6197 6193 -5309 -6196
		mu 0 4 3242 3241 3598 3597
		f 4 -6198 6195 -5326 -6179
		mu 0 4 3244 3242 3597 3617
		f 4 -6107 6198 6200 -6200
		mu 0 4 3743 3736 3784 3785
		f 4 6103 7310 -6203 -6199
		mu 0 4 3736 3737 3786 3784
		f 4 6074 6203 -6205 -6202
		mu 0 4 3713 3715 3787 3788
		f 4 -6106 6199 6205 7278
		mu 0 4 3744 3743 3785 3789
		f 4 -6115 6206 6208 -6208
		mu 0 4 3755 3748 3790 3791
		f 4 6111 7286 -6211 -6207
		mu 0 4 3748 3749 3792 3790
		f 4 6062 6211 -6213 -6210
		mu 0 4 3695 3699 3793 3794
		f 4 -6114 6207 6213 7294
		mu 0 4 3756 3755 3791 3795
		f 4 -6201 6214 6216 -6216
		mu 0 4 3785 3784 3739 3738
		f 4 6202 7311 -6219 -6215
		mu 0 4 3784 3786 3740 3739
		f 4 6204 6219 -6221 -6218
		mu 0 4 3788 3787 3796 3797
		f 4 -6206 6215 6221 7276
		mu 0 4 3789 3785 3738 3741
		f 4 -6209 6222 6224 -6224
		mu 0 4 3791 3790 3751 3750
		f 4 6210 7288 -6227 -6223
		mu 0 4 3790 3792 3752 3751
		f 4 6212 6227 -6229 -6226
		mu 0 4 3794 3793 3798 3799
		f 4 -6214 6223 6229 7292
		mu 0 4 3795 3791 3750 3753
		f 4 -6129 6230 6232 -6232
		mu 0 4 3773 3770 3800 3801
		f 4 6125 7726 -6235 -6231
		mu 0 4 3770 3771 3802 3800
		f 4 6108 6235 -6237 -6234
		mu 0 4 3743 3742 3803 3804
		f 4 -6128 6231 6237 7914
		mu 0 4 3774 3773 3801 3805
		f 4 -6133 6238 6240 -6240
		mu 0 4 3777 3772 3806 3807
		f 4 6129 7918 -6243 -6239
		mu 0 4 3772 3775 3808 3806
		f 4 6112 6243 -6245 -6242
		mu 0 4 3746 3748 3809 3810
		f 4 -6132 6239 6245 8106
		mu 0 4 3778 3777 3807 3811
		f 4 -6233 6246 6248 -6248
		mu 0 4 3801 3800 3812 3813
		f 4 6234 7728 -6251 -6247
		mu 0 4 3800 3802 3814 3812
		f 4 6236 6251 -6253 -6250
		mu 0 4 3804 3803 3815 3816
		f 4 -6238 6247 6253 7912
		mu 0 4 3805 3801 3813 3817
		f 4 -6241 6254 6256 -6256
		mu 0 4 3807 3806 3818 3819
		f 4 6242 7920 -6259 -6255
		mu 0 4 3806 3808 3820 3818
		f 4 6244 6259 -6261 -6258
		mu 0 4 3810 3809 3821 3822
		f 4 -6246 6255 6261 8104
		mu 0 4 3811 3807 3819 3823
		f 4 -6249 6262 6264 -6264
		mu 0 4 3813 3812 3824 3825
		f 4 6250 7730 -6267 -6263
		mu 0 4 3812 3814 3826 3824
		f 4 6252 6267 -6269 -6266
		mu 0 4 3816 3815 3827 3828
		f 4 -6254 6263 6269 7910
		mu 0 4 3817 3813 3825 3829
		f 4 -6257 6270 6272 -6272
		mu 0 4 3819 3818 3830 3831
		f 4 6258 7922 -6275 -6271
		mu 0 4 3818 3820 3832 3830
		f 4 6260 6275 -6277 -6274
		mu 0 4 3822 3821 3833 3834
		f 4 -6262 6271 6277 8102
		mu 0 4 3823 3819 3831 3835
		f 4 -6265 6278 6280 -6280
		mu 0 4 3825 3824 3836 3837
		f 4 6266 7732 -6283 -6279
		mu 0 4 3824 3826 3838 3836
		f 4 6268 6283 -6285 -6282
		mu 0 4 3828 3827 3839 3840
		f 4 -6270 6279 6285 7908
		mu 0 4 3829 3825 3837 3841
		f 4 -6273 6286 6288 -6288
		mu 0 4 3831 3830 3842 3843
		f 4 6274 7924 -6291 -6287
		mu 0 4 3830 3832 3844 3842
		f 4 6276 6291 -6293 -6290
		mu 0 4 3834 3833 3845 3846
		f 4 -6278 6287 6293 8100
		mu 0 4 3835 3831 3843 3847
		f 4 -6281 6294 6296 -6296
		mu 0 4 3837 3836 3848 3849
		f 4 6282 7734 -6299 -6295
		mu 0 4 3836 3838 3850 3848
		f 4 6284 6299 -6301 -6298
		mu 0 4 3840 3839 3851 3852
		f 4 -6286 6295 6301 7906
		mu 0 4 3841 3837 3849 3853
		f 4 -6289 6302 6304 -6304
		mu 0 4 3843 3842 3854 3855
		f 4 6290 7926 -6307 -6303
		mu 0 4 3842 3844 3856 3854
		f 4 6292 6307 -6309 -6306
		mu 0 4 3846 3845 3857 3858
		f 4 -6294 6303 6309 8098
		mu 0 4 3847 3843 3855 3859
		f 4 -6297 6310 6312 -6312
		mu 0 4 3860 3861 3862 3863
		f 4 6298 7736 -6315 -6311
		mu 0 4 3864 3865 3866 3867
		f 4 6300 6315 -6317 -6314
		mu 0 4 3868 3869 3870 3871
		f 4 -6302 6311 6317 7904
		mu 0 4 3872 3873 3874 3875
		f 4 -6305 6318 6320 -6320
		mu 0 4 3876 3877 3878 3879
		f 4 6306 7928 -6323 -6319
		mu 0 4 3880 3881 3882 3883
		f 4 6308 6323 -6325 -6322
		mu 0 4 3884 3885 3886 3887
		f 4 -6310 6319 6325 8096
		mu 0 4 3888 3889 3890 3891
		f 4 -6313 6326 6328 -6328
		mu 0 4 3892 3893 3894 3895
		f 4 6314 7738 -6331 -6327
		mu 0 4 3896 3897 3898 3899
		f 4 6316 6331 -6333 -6330
		mu 0 4 3900 3901 3902 3903
		f 4 -6318 6327 6333 7902
		mu 0 4 3904 3905 3906 3907
		f 4 -6321 6334 6336 -6336
		mu 0 4 3908 3909 3910 3911
		f 4 6322 7930 -6339 -6335
		mu 0 4 3912 3913 3914 3915
		f 4 6324 6339 -6341 -6338
		mu 0 4 3916 3917 3918 3919
		f 4 -6326 6335 6341 8094
		mu 0 4 3920 3921 3922 3923
		f 4 -6777 -6779 6780 7820
		mu 0 4 3924 3925 3926 3927
		f 4 6784 8012 -6789 -6790
		mu 0 4 3928 3929 3930 3931
		f 4 -6334 6342 6900 7900
		mu 0 4 3932 3933 3934 3935
		f 4 -6329 6345 6898 -6343
		mu 0 4 3933 3936 3937 3934
		f 4 6330 7740 7739 -6346
		mu 0 4 3936 3938 3939 3937
		f 4 6332 6343 6901 -6348
		mu 0 4 3940 3941 3942 3943
		f 4 6338 7932 7931 -6351
		mu 0 4 3944 3945 3946 3947
		f 4 6340 6353 6802 -6352
		mu 0 4 3948 3949 3950 3951
		f 4 -6342 6355 6800 8092
		mu 0 4 3952 3953 3954 3955
		f 4 -6337 6350 6805 -6356
		mu 0 4 3953 3944 3947 3954
		f 4 -6345 6358 6360 7894
		mu 0 4 3956 3957 3958 3959
		f 4 -6347 6361 6362 -6359
		mu 0 4 3957 3960 3961 3958
		f 4 6348 7746 -6365 -6362
		mu 0 4 3960 3962 3963 3961
		f 4 6349 6359 -6366 -6364
		mu 0 4 3964 3965 3966 3967
		f 4 6352 7938 -6369 -6367
		mu 0 4 3968 3969 3970 3971
		f 4 6354 6369 -6371 -6368
		mu 0 4 3972 3973 3974 3975
		f 4 -6357 6371 6372 8086
		mu 0 4 3976 3977 3978 3979
		f 4 -6358 6366 6373 -6372
		mu 0 4 3977 3968 3971 3978
		f 4 -6361 6374 6376 7892
		mu 0 4 3959 3958 3980 3981
		f 4 -6363 6377 6378 -6375
		mu 0 4 3958 3961 3982 3980
		f 4 6364 7748 -6381 -6378
		mu 0 4 3961 3963 3983 3982
		f 4 6365 6375 -6382 -6380
		mu 0 4 3967 3966 3984 3985
		f 4 6368 7940 -6385 -6383
		mu 0 4 3971 3970 3986 3987
		f 4 6370 6385 -6387 -6384
		mu 0 4 3975 3974 3988 3989
		f 4 -6373 6387 6388 8084
		mu 0 4 3979 3978 3990 3991
		f 4 -6374 6382 6389 -6388
		mu 0 4 3978 3971 3987 3990
		f 4 -6377 6390 6392 7890
		mu 0 4 3981 3980 3992 3993
		f 4 -6379 6393 6394 -6391
		mu 0 4 3980 3982 3994 3992
		f 4 6380 7750 -6397 -6394
		mu 0 4 3982 3983 3995 3994
		f 4 6381 6391 -6398 -6396
		mu 0 4 3985 3984 3996 3997
		f 4 6384 7942 -6401 -6399
		mu 0 4 3987 3986 3998 3999
		f 4 6386 6401 -6403 -6400
		mu 0 4 3989 3988 4000 4001
		f 4 -6389 6403 6404 8082
		mu 0 4 3991 3990 4002 4003
		f 4 -6390 6398 6405 -6404
		mu 0 4 3990 3987 3999 4002
		f 4 -6393 6406 6408 7888
		mu 0 4 3993 3992 4004 4005
		f 4 -6395 6409 6410 -6407
		mu 0 4 3992 3994 4006 4004
		f 4 6396 7752 -6413 -6410
		mu 0 4 3994 3995 4007 4006
		f 4 6397 6407 -6414 -6412
		mu 0 4 3997 3996 4008 4009
		f 4 6400 7944 -6417 -6415
		mu 0 4 3999 3998 4010 4011
		f 4 6402 6417 -6419 -6416
		mu 0 4 4001 4000 4012 4013
		f 4 -6405 6419 6420 8080
		mu 0 4 4003 4002 4014 4015
		f 4 -6406 6414 6421 -6420
		mu 0 4 4002 3999 4011 4014
		f 4 -6409 6422 6916 7886
		mu 0 4 4005 4004 4016 4017
		f 4 -6411 6425 6914 -6423
		mu 0 4 4004 4006 4018 4016
		f 4 6412 7754 7753 -6426
		mu 0 4 4006 4007 4019 4018
		f 4 6413 6423 6917 -6428
		mu 0 4 4009 4008 4020 4021
		f 4 6416 7946 7945 -6431
		mu 0 4 4011 4010 4022 4023
		f 4 6418 6433 6818 -6432
		mu 0 4 4013 4012 4024 4025
		f 4 -6421 6435 6816 8078
		mu 0 4 4015 4014 4026 4027
		f 4 -6422 6430 6821 -6436
		mu 0 4 4014 4011 4023 4026
		f 4 -6425 6438 6440 7880
		mu 0 4 4028 4029 4030 4031
		f 4 -6427 6441 6442 -6439
		mu 0 4 4029 4032 4033 4030
		f 4 6428 7760 -6445 -6442
		mu 0 4 4032 4034 4035 4033
		f 4 6429 6439 -6446 -6444
		mu 0 4 4036 4037 4038 4039
		f 4 6432 7952 -6449 -6447
		mu 0 4 4040 4041 4042 4043
		f 4 6434 6449 -6451 -6448
		mu 0 4 4044 4045 4046 4047
		f 4 -6437 6451 6452 8072
		mu 0 4 4048 4049 4050 4051
		f 4 -6438 6446 6453 -6452
		mu 0 4 4049 4040 4043 4050
		f 4 -6441 6454 6456 7878
		mu 0 4 4031 4030 4052 4053
		f 4 -6443 6457 6458 -6455
		mu 0 4 4030 4033 4054 4052
		f 4 6444 7762 -6461 -6458
		mu 0 4 4033 4035 4055 4054
		f 4 6445 6455 -6462 -6460
		mu 0 4 4039 4038 4056 4057
		f 4 6448 7954 -6465 -6463
		mu 0 4 4043 4042 4058 4059
		f 4 6450 6465 -6467 -6464
		mu 0 4 4047 4046 4060 4061
		f 4 -6453 6467 6468 8070
		mu 0 4 4051 4050 4062 4063
		f 4 -6454 6462 6469 -6468
		mu 0 4 4050 4043 4059 4062
		f 4 -6457 6470 6472 7876
		mu 0 4 4053 4052 4064 4065
		f 4 -6459 6473 6474 -6471
		mu 0 4 4052 4054 4066 4064
		f 4 6460 7764 -6477 -6474
		mu 0 4 4054 4055 4067 4066
		f 4 6461 6471 -6478 -6476
		mu 0 4 4057 4056 4068 4069
		f 4 6464 7956 -6481 -6479
		mu 0 4 4059 4058 4070 4071
		f 4 6466 6481 -6483 -6480
		mu 0 4 4061 4060 4072 4073
		f 4 -6469 6483 6484 8068
		mu 0 4 4063 4062 4074 4075
		f 4 -6470 6478 6485 -6484
		mu 0 4 4062 4059 4071 4074
		f 4 -6473 6486 6488 7874
		mu 0 4 4065 4064 4076 4077
		f 4 -6475 6489 6490 -6487
		mu 0 4 4064 4066 4078 4076
		f 4 6476 7766 -6493 -6490
		mu 0 4 4066 4067 4079 4078
		f 4 6477 6487 -6494 -6492
		mu 0 4 4069 4068 4080 4081
		f 4 6480 7958 -6497 -6495
		mu 0 4 4071 4070 4082 4083
		f 4 6482 6497 -6499 -6496
		mu 0 4 4073 4072 4084 4085
		f 4 -6485 6499 6500 8066
		mu 0 4 4075 4074 4086 4087
		f 4 -6486 6494 6501 -6500
		mu 0 4 4074 4071 4083 4086
		f 4 -6489 6502 6504 7872
		mu 0 4 4077 4076 4088 4089
		f 4 -6491 6505 6506 -6503
		mu 0 4 4076 4078 4090 4088
		f 4 6492 7768 -6509 -6506
		mu 0 4 4078 4079 4091 4090
		f 4 6493 6503 -6510 -6508
		mu 0 4 4081 4080 4092 4093
		f 4 6496 7960 -6513 -6511
		mu 0 4 4083 4082 4094 4095
		f 4 6498 6513 -6515 -6512
		mu 0 4 4085 4084 4096 4097
		f 4 -6501 6515 6516 8064
		mu 0 4 4087 4086 4098 4099
		f 4 -6502 6510 6517 -6516
		mu 0 4 4086 4083 4095 4098
		f 4 -6505 6518 6932 7870
		mu 0 4 4089 4088 4100 4101
		f 4 -6507 6521 6930 -6519
		mu 0 4 4088 4090 4102 4100
		f 4 6508 7770 7769 -6522
		mu 0 4 4090 4091 4103 4102
		f 4 6509 6519 6933 -6524
		mu 0 4 4093 4092 4104 4105
		f 4 6512 7962 7961 -6527
		mu 0 4 4095 4094 4106 4107
		f 4 6514 6529 6834 -6528
		mu 0 4 4097 4096 4108 4109
		f 4 -6517 6531 6832 8062
		mu 0 4 4099 4098 4110 4111
		f 4 -6518 6526 6837 -6532
		mu 0 4 4098 4095 4107 4110
		f 4 -6521 6534 6536 7864
		mu 0 4 4112 4113 4114 4115
		f 4 -6523 6537 6538 -6535
		mu 0 4 4113 4116 4117 4114
		f 4 6524 7776 -6541 -6538
		mu 0 4 4116 4118 4119 4117
		f 4 6525 6535 -6542 -6540
		mu 0 4 4120 4121 4122 4123
		f 4 6528 7968 -6545 -6543
		mu 0 4 4124 4125 4126 4127
		f 4 6530 6545 -6547 -6544
		mu 0 4 4128 4129 4130 4131
		f 4 -6533 6547 6548 8056
		mu 0 4 4132 4133 4134 4135
		f 4 -6534 6542 6549 -6548
		mu 0 4 4133 4124 4127 4134
		f 4 -6537 6550 6552 7862
		mu 0 4 4115 4114 4136 4137
		f 4 -6539 6553 6554 -6551
		mu 0 4 4114 4117 4138 4136
		f 4 6540 7778 -6557 -6554
		mu 0 4 4117 4119 4139 4138
		f 4 6541 6551 -6558 -6556
		mu 0 4 4123 4122 4140 4141
		f 4 6544 7970 -6561 -6559
		mu 0 4 4127 4126 4142 4143
		f 4 6546 6561 -6563 -6560
		mu 0 4 4131 4130 4144 4145
		f 4 -6549 6563 6564 8054
		mu 0 4 4135 4134 4146 4147
		f 4 -6550 6558 6565 -6564
		mu 0 4 4134 4127 4143 4146
		f 4 -6553 6566 6568 7860
		mu 0 4 4137 4136 4148 4149
		f 4 -6555 6569 6570 -6567
		mu 0 4 4136 4138 4150 4148
		f 4 6556 7780 -6573 -6570
		mu 0 4 4138 4139 4151 4150
		f 4 6557 6567 -6574 -6572
		mu 0 4 4141 4140 4152 4153
		f 4 6560 7972 -6577 -6575
		mu 0 4 4143 4142 4154 4155
		f 4 6562 6577 -6579 -6576
		mu 0 4 4145 4144 4156 4157
		f 4 -6565 6579 6580 8052
		mu 0 4 4147 4146 4158 4159
		f 4 -6566 6574 6581 -6580
		mu 0 4 4146 4143 4155 4158
		f 4 -6569 6582 6584 7858
		mu 0 4 4149 4148 4160 4161
		f 4 -6571 6585 6586 -6583
		mu 0 4 4148 4150 4162 4160
		f 4 6572 7782 -6589 -6586
		mu 0 4 4150 4151 4163 4162
		f 4 6573 6583 -6590 -6588
		mu 0 4 4153 4152 4164 4165
		f 4 6576 7974 -6593 -6591
		mu 0 4 4155 4154 4166 4167
		f 4 6578 6593 -6595 -6592
		mu 0 4 4157 4156 4168 4169
		f 4 -6581 6595 6596 8050
		mu 0 4 4159 4158 4170 4171
		f 4 -6582 6590 6597 -6596
		mu 0 4 4158 4155 4167 4170
		f 4 -6585 6598 6948 7856
		mu 0 4 4161 4160 4172 4173
		f 4 -6587 6601 6946 -6599
		mu 0 4 4160 4162 4174 4172
		f 4 6588 7784 7783 -6602
		mu 0 4 4162 4163 4175 4174
		f 4 6589 6599 6949 -6604
		mu 0 4 4165 4164 4176 4177
		f 4 6592 7976 7975 -6607
		mu 0 4 4167 4166 4178 4179
		f 4 6594 6609 6850 -6608
		mu 0 4 4169 4168 4180 4181
		f 4 -6597 6611 6848 8048
		mu 0 4 4171 4170 4182 4183
		f 4 -6598 6606 6853 -6612
		mu 0 4 4170 4167 4179 4182
		f 4 -6601 6614 6616 7850
		mu 0 4 4184 4185 4186 4187
		f 4 -6603 6617 6618 -6615
		mu 0 4 4185 4188 4189 4186
		f 4 6604 7790 -6621 -6618
		mu 0 4 4188 4190 4191 4189
		f 4 6605 6615 -6622 -6620
		mu 0 4 4192 4193 4194 4195
		f 4 6608 7982 -6625 -6623
		mu 0 4 4196 4197 4198 4199
		f 4 6610 6625 -6627 -6624
		mu 0 4 4200 4201 4202 4203
		f 4 -6613 6627 6628 8042
		mu 0 4 4204 4205 4206 4207
		f 4 -6614 6622 6629 -6628
		mu 0 4 4205 4196 4199 4206
		f 4 -6617 6630 6632 7848
		mu 0 4 4187 4186 4208 4209
		f 4 -6619 6633 6634 -6631
		mu 0 4 4186 4189 4210 4208
		f 4 6620 7792 -6637 -6634
		mu 0 4 4189 4191 4211 4210
		f 4 6621 6631 -6638 -6636
		mu 0 4 4195 4194 4212 4213
		f 4 6624 7984 -6641 -6639
		mu 0 4 4199 4198 4214 4215
		f 4 6626 6641 -6643 -6640
		mu 0 4 4203 4202 4216 4217
		f 4 -6629 6643 6644 8040
		mu 0 4 4207 4206 4218 4219
		f 4 -6630 6638 6645 -6644
		mu 0 4 4206 4199 4215 4218
		f 4 -6633 6646 6648 7846
		mu 0 4 4209 4208 4220 4221
		f 4 -6635 6649 6650 -6647
		mu 0 4 4208 4210 4222 4220
		f 4 6636 7794 -6653 -6650
		mu 0 4 4210 4211 4223 4222
		f 4 6637 6647 -6654 -6652
		mu 0 4 4213 4212 4224 4225
		f 4 6640 7986 -6657 -6655
		mu 0 4 4215 4214 4226 4227
		f 4 6642 6657 -6659 -6656
		mu 0 4 4217 4216 4228 4229
		f 4 -6645 6659 6660 8038
		mu 0 4 4219 4218 4230 4231
		f 4 -6646 6654 6661 -6660
		mu 0 4 4218 4215 4227 4230
		f 4 -6649 6662 6664 7844
		mu 0 4 4221 4220 4232 4233
		f 4 -6651 6665 6666 -6663
		mu 0 4 4220 4222 4234 4232
		f 4 6652 7796 -6669 -6666
		mu 0 4 4222 4223 4235 4234
		f 4 6653 6663 -6670 -6668
		mu 0 4 4225 4224 4236 4237
		f 4 6656 7988 -6673 -6671
		mu 0 4 4227 4226 4238 4239
		f 4 6658 6673 -6675 -6672
		mu 0 4 4229 4228 4240 4241
		f 4 -6661 6675 6676 8036
		mu 0 4 4231 4230 4242 4243
		f 4 -6662 6670 6677 -6676
		mu 0 4 4230 4227 4239 4242
		f 4 -6665 6678 6964 7842
		mu 0 4 4233 4232 4244 4245
		f 4 -6667 6681 6962 -6679
		mu 0 4 4232 4234 4246 4244
		f 4 6668 7798 7797 -6682
		mu 0 4 4234 4235 4247 4246
		f 4 6669 6679 6965 -6684
		mu 0 4 4237 4236 4248 4249
		f 4 6672 7990 7989 -6687
		mu 0 4 4239 4238 4250 4251
		f 4 6674 6689 6866 -6688
		mu 0 4 4241 4240 4252 4253
		f 4 -6677 6691 6864 8034
		mu 0 4 4243 4242 4254 4255
		f 4 -6678 6686 6869 -6692
		mu 0 4 4242 4239 4251 4254
		f 4 -6681 6694 6696 7836
		mu 0 4 4256 4257 4258 4259
		f 4 -6683 6697 6698 -6695
		mu 0 4 4257 4260 4261 4258
		f 4 6684 7804 -6701 -6698
		mu 0 4 4260 4262 4263 4261
		f 4 6685 6695 -6702 -6700
		mu 0 4 4264 4265 4266 4267
		f 4 6688 7996 -6705 -6703
		mu 0 4 4268 4269 4270 4271
		f 4 6690 6705 -6707 -6704
		mu 0 4 4272 4273 4274 4275
		f 4 -6693 6707 6708 8028
		mu 0 4 4276 4277 4278 4279
		f 4 -6694 6702 6709 -6708
		mu 0 4 4277 4268 4271 4278
		f 4 -6697 6710 6712 7834
		mu 0 4 4259 4258 4280 4281
		f 4 -6699 6713 6714 -6711
		mu 0 4 4258 4261 4282 4280
		f 4 6700 7806 -6717 -6714
		mu 0 4 4261 4263 4283 4282
		f 4 6701 6711 -6718 -6716
		mu 0 4 4267 4266 4284 4285
		f 4 6704 7998 -6721 -6719
		mu 0 4 4271 4270 4286 4287
		f 4 6706 6721 -6723 -6720
		mu 0 4 4275 4274 4288 4289
		f 4 -6709 6723 6724 8026
		mu 0 4 4279 4278 4290 4291
		f 4 -6710 6718 6725 -6724
		mu 0 4 4278 4271 4287 4290
		f 4 -6713 6726 6728 7832
		mu 0 4 4281 4280 4292 4293
		f 4 -6715 6729 6730 -6727
		mu 0 4 4280 4282 4294 4292
		f 4 6716 7808 -6733 -6730
		mu 0 4 4282 4283 4295 4294
		f 4 6717 6727 -6734 -6732
		mu 0 4 4285 4284 4296 4297
		f 4 6720 8000 -6737 -6735
		mu 0 4 4287 4286 4298 4299
		f 4 6722 6737 -6739 -6736
		mu 0 4 4289 4288 4300 4301
		f 4 -6725 6739 6740 8024
		mu 0 4 4291 4290 4302 4303
		f 4 -6726 6734 6741 -6740
		mu 0 4 4290 4287 4299 4302
		f 4 -6729 6742 6744 7830
		mu 0 4 4293 4292 4304 4305
		f 4 -6731 6745 6746 -6743
		mu 0 4 4292 4294 4306 4304
		f 4 6732 7810 -6749 -6746
		mu 0 4 4294 4295 4307 4306
		f 4 6733 6743 -6750 -6748
		mu 0 4 4297 4296 4308 4309
		f 4 6736 8002 -6753 -6751
		mu 0 4 4299 4298 4310 4311
		f 4 6738 6753 -6755 -6752
		mu 0 4 4301 4300 4312 4313
		f 4 -6741 6755 6756 8022
		mu 0 4 4303 4302 4314 4315
		f 4 -6742 6750 6757 -6756
		mu 0 4 4302 4299 4311 4314
		f 4 -6745 6758 6980 7828
		mu 0 4 4305 4304 4316 4317
		f 4 -6747 6761 6978 -6759
		mu 0 4 4304 4306 4318 4316
		f 4 6748 7812 7811 -6762
		mu 0 4 4306 4307 4319 4318
		f 4 6749 6759 6981 -6764
		mu 0 4 4309 4308 4320 4321
		f 4 6752 8004 8003 -6767
		mu 0 4 4311 4310 4322 4323
		f 4 6754 6769 6882 -6768
		mu 0 4 4313 4312 4324 4325
		f 4 -6757 6771 6880 8020
		mu 0 4 4315 4314 4326 4327
		f 4 -6758 6766 6885 -6772
		mu 0 4 4314 4311 4323 4326
		f 4 -6761 6774 6776 7822
		mu 0 4 4328 4329 3925 3924
		f 4 -6763 6777 6778 -6775
		mu 0 4 4329 4330 3926 3925
		f 4 6764 7818 -6781 -6778
		mu 0 4 4330 4331 3927 3926
		f 4 6765 6775 -6782 -6780
		mu 0 4 4332 4333 4334 4335
		f 4 6768 8010 -6785 -6783
		mu 0 4 4336 4337 3929 3928
		f 4 6770 6785 -6787 -6784
		mu 0 4 4338 4339 4340 4341
		f 4 -6773 6787 6788 8014
		mu 0 4 4342 4343 3931 3930
		f 4 -6774 6782 6789 -6788
		mu 0 4 4343 4336 3928 3931
		f 4 -6793 6790 6356 8088
		mu 0 4 4344 4345 3977 3976
		f 4 -6795 6791 -6355 -6794
		mu 0 4 4346 4347 3973 3972
		f 4 -7934 7936 -6353 -6796
		mu 0 4 4348 4349 3969 3968
		f 4 -6798 6795 6357 -6791
		mu 0 4 4345 4348 3968 3977
		f 4 -6801 6798 6792 8090
		mu 0 4 3955 3954 4345 4344
		f 4 -6803 6799 6794 -6802
		mu 0 4 3951 3950 4347 4346
		f 4 -7932 7934 7933 -6804
		mu 0 4 3947 3946 4349 4348
		f 4 -6806 6803 6797 -6799
		mu 0 4 3954 3947 4348 4345
		f 4 -6809 6806 6436 8074
		mu 0 4 4350 4351 4049 4048
		f 4 -6811 6807 -6435 -6810
		mu 0 4 4352 4353 4045 4044
		f 4 -7948 7950 -6433 -6812
		mu 0 4 4354 4355 4041 4040
		f 4 -6814 6811 6437 -6807
		mu 0 4 4351 4354 4040 4049
		f 4 -6817 6814 6808 8076
		mu 0 4 4027 4026 4351 4350
		f 4 -6819 6815 6810 -6818
		mu 0 4 4025 4024 4353 4352
		f 4 -7946 7948 7947 -6820
		mu 0 4 4023 4022 4355 4354
		f 4 -6822 6819 6813 -6815
		mu 0 4 4026 4023 4354 4351
		f 4 -6825 6822 6532 8058
		mu 0 4 4356 4357 4133 4132
		f 4 -6827 6823 -6531 -6826
		mu 0 4 4358 4359 4129 4128
		f 4 -7964 7966 -6529 -6828
		mu 0 4 4360 4361 4125 4124
		f 4 -6830 6827 6533 -6823
		mu 0 4 4357 4360 4124 4133
		f 4 -6833 6830 6824 8060
		mu 0 4 4111 4110 4357 4356
		f 4 -6835 6831 6826 -6834
		mu 0 4 4109 4108 4359 4358
		f 4 -7962 7964 7963 -6836
		mu 0 4 4107 4106 4361 4360
		f 4 -6838 6835 6829 -6831
		mu 0 4 4110 4107 4360 4357
		f 4 -6841 6838 6612 8044
		mu 0 4 4362 4363 4205 4204
		f 4 -6843 6839 -6611 -6842
		mu 0 4 4364 4365 4201 4200
		f 4 -7978 7980 -6609 -6844
		mu 0 4 4366 4367 4197 4196
		f 4 -6846 6843 6613 -6839
		mu 0 4 4363 4366 4196 4205
		f 4 -6849 6846 6840 8046
		mu 0 4 4183 4182 4363 4362
		f 4 -6851 6847 6842 -6850
		mu 0 4 4181 4180 4365 4364
		f 4 -7976 7978 7977 -6852
		mu 0 4 4179 4178 4367 4366
		f 4 -6854 6851 6845 -6847
		mu 0 4 4182 4179 4366 4363
		f 4 -6857 6854 6692 8030
		mu 0 4 4368 4369 4277 4276
		f 4 -6859 6855 -6691 -6858
		mu 0 4 4370 4371 4273 4272
		f 4 -7992 7994 -6689 -6860
		mu 0 4 4372 4373 4269 4268
		f 4 -6862 6859 6693 -6855
		mu 0 4 4369 4372 4268 4277
		f 4 -6865 6862 6856 8032
		mu 0 4 4255 4254 4369 4368
		f 4 -6867 6863 6858 -6866
		mu 0 4 4253 4252 4371 4370
		f 4 -7990 7992 7991 -6868
		mu 0 4 4251 4250 4373 4372
		f 4 -6870 6867 6861 -6863
		mu 0 4 4254 4251 4372 4369
		f 4 -6873 6870 6772 8016
		mu 0 4 4374 4375 4343 4342
		f 4 -6875 6871 -6771 -6874
		mu 0 4 4376 4377 4339 4338
		f 4 -8006 8008 -6769 -6876
		mu 0 4 4378 4379 4337 4336
		f 4 -6878 6875 6773 -6871
		mu 0 4 4375 4378 4336 4343
		f 4 -6881 6878 6872 8018
		mu 0 4 4327 4326 4375 4374
		f 4 -6883 6879 6874 -6882
		mu 0 4 4325 4324 4377 4376
		f 4 -8004 8006 8005 -6884
		mu 0 4 4323 4322 4379 4378
		f 4 -6886 6883 6877 -6879
		mu 0 4 4326 4323 4378 4375
		f 4 -7742 7744 -6349 -6888
		mu 0 4 4380 4381 3962 3960
		f 4 -6891 6887 6346 -6890
		mu 0 4 4382 4380 3960 3957
		f 4 -6893 6889 6344 7896
		mu 0 4 4383 4382 3957 3956
		f 4 -6894 6891 -6350 -6887
		mu 0 4 4384 4385 3965 3964
		f 4 -7740 7742 7741 -6896
		mu 0 4 3937 3939 4381 4380
		f 4 -6899 6895 6890 -6898
		mu 0 4 3934 3937 4380 4382
		f 4 -6901 6897 6892 7898
		mu 0 4 3935 3934 4382 4383
		f 4 -6902 6899 6893 -6895
		mu 0 4 3943 3942 4385 4384
		f 4 -7756 7758 -6429 -6904
		mu 0 4 4386 4387 4034 4032
		f 4 -6907 6903 6426 -6906
		mu 0 4 4388 4386 4032 4029
		f 4 -6909 6905 6424 7882
		mu 0 4 4389 4388 4029 4028
		f 4 -6910 6907 -6430 -6903
		mu 0 4 4390 4391 4037 4036
		f 4 -7754 7756 7755 -6912
		mu 0 4 4018 4019 4387 4386
		f 4 -6915 6911 6906 -6914
		mu 0 4 4016 4018 4386 4388
		f 4 -6917 6913 6908 7884
		mu 0 4 4017 4016 4388 4389
		f 4 -6918 6915 6909 -6911
		mu 0 4 4021 4020 4391 4390
		f 4 -7772 7774 -6525 -6920
		mu 0 4 4392 4393 4118 4116
		f 4 -6923 6919 6522 -6922
		mu 0 4 4394 4392 4116 4113
		f 4 -6925 6921 6520 7866
		mu 0 4 4395 4394 4113 4112
		f 4 -6926 6923 -6526 -6919
		mu 0 4 4396 4397 4121 4120
		f 4 -7770 7772 7771 -6928
		mu 0 4 4102 4103 4393 4392
		f 4 -6931 6927 6922 -6930
		mu 0 4 4100 4102 4392 4394
		f 4 -6933 6929 6924 7868
		mu 0 4 4101 4100 4394 4395
		f 4 -6934 6931 6925 -6927
		mu 0 4 4105 4104 4397 4396
		f 4 -7786 7788 -6605 -6936
		mu 0 4 4398 4399 4190 4188
		f 4 -6939 6935 6602 -6938
		mu 0 4 4400 4398 4188 4185
		f 4 -6941 6937 6600 7852
		mu 0 4 4401 4400 4185 4184
		f 4 -6942 6939 -6606 -6935
		mu 0 4 4402 4403 4193 4192
		f 4 -7784 7786 7785 -6944
		mu 0 4 4174 4175 4399 4398
		f 4 -6947 6943 6938 -6946
		mu 0 4 4172 4174 4398 4400
		f 4 -6949 6945 6940 7854
		mu 0 4 4173 4172 4400 4401
		f 4 -6950 6947 6941 -6943
		mu 0 4 4177 4176 4403 4402
		f 4 -7800 7802 -6685 -6952
		mu 0 4 4404 4405 4262 4260
		f 4 -6955 6951 6682 -6954
		mu 0 4 4406 4404 4260 4257
		f 4 -6957 6953 6680 7838
		mu 0 4 4407 4406 4257 4256
		f 4 -6958 6955 -6686 -6951
		mu 0 4 4408 4409 4265 4264
		f 4 -7798 7800 7799 -6960
		mu 0 4 4246 4247 4405 4404
		f 4 -6963 6959 6954 -6962
		mu 0 4 4244 4246 4404 4406
		f 4 -6965 6961 6956 7840
		mu 0 4 4245 4244 4406 4407
		f 4 -6966 6963 6957 -6959
		mu 0 4 4249 4248 4409 4408
		f 4 -7814 7816 -6765 -6968
		mu 0 4 4410 4411 4331 4330
		f 4 -6971 6967 6762 -6970
		mu 0 4 4412 4410 4330 4329
		f 4 -6973 6969 6760 7824
		mu 0 4 4413 4412 4329 4328
		f 4 -6974 6971 -6766 -6967
		mu 0 4 4414 4415 4333 4332
		f 4 -7812 7814 7813 -6976
		mu 0 4 4318 4319 4411 4410
		f 4 -6979 6975 6970 -6978
		mu 0 4 4316 4318 4410 4412;
	setAttr ".fc[3500:3999]"
		f 4 -6981 6977 6972 7826
		mu 0 4 4317 4316 4412 4413
		f 4 -6982 6979 6973 -6975
		mu 0 4 4321 4320 4415 4414
		f 4 -6985 -6986 6983 -5962
		mu 0 4 3362 4416 4417 3587
		f 4 -6988 6984 5468 -6987
		mu 0 4 4418 4419 3352 3351
		f 4 -6990 6986 5470 -6989
		mu 0 4 4420 4418 3351 3353
		f 4 -6992 6988 5472 -6991
		mu 0 4 4421 4420 3353 3354
		f 4 -6994 6990 5474 8495
		mu 0 4 4422 4421 3354 3355
		f 4 -6996 6992 5476 -6995
		mu 0 4 4423 4424 3357 3356
		f 4 -6998 6994 5478 -6997
		mu 0 4 4425 4423 3356 3358
		f 4 -7000 6996 5480 -6999
		mu 0 4 4426 4425 3358 3359
		f 4 -7002 6998 5967 -7001
		mu 0 4 4427 4426 3359 3595
		f 4 -7004 -7005 7002 -5970
		mu 0 4 3360 4428 4429 3594
		f 4 -7007 7003 5484 -7006
		mu 0 4 4430 4428 3360 3361
		f 4 -7008 7005 5959 -6983
		mu 0 4 4431 4430 3361 3588
		f 4 -7011 -7012 7009 6985
		mu 0 4 4416 3589 3590 4417
		f 4 -7014 7010 6987 -7013
		mu 0 4 3157 3159 4419 4418
		f 4 -7016 7012 6989 -7015
		mu 0 4 3155 3157 4418 4420
		f 4 -7018 7014 6991 -7017
		mu 0 4 3152 3155 4420 4421
		f 4 -7020 7016 6993 8497
		mu 0 4 3153 3152 4421 4422
		f 4 -7022 7018 6995 -7021
		mu 0 4 3147 3149 4424 4423
		f 4 -7024 7020 6997 -7023
		mu 0 4 3144 3147 4423 4425
		f 4 -7026 7022 6999 -7025
		mu 0 4 3145 3144 4425 4426
		f 4 -7028 7024 7001 -7027
		mu 0 4 3593 3145 4426 4427
		f 4 -7030 -7031 7028 7004
		mu 0 4 4428 3140 3596 4429
		f 4 -7033 7029 7006 -7032
		mu 0 4 3141 3140 4428 4430
		f 4 -7034 7031 7007 -7009
		mu 0 4 3586 3141 4430 4431
		f 4 1560 7034 -7010 7035
		mu 0 4 4432 4433 4434 4435
		f 4 1557 7036 -6984 -7035
		mu 0 4 4436 4437 4438 4439
		f 4 1553 -7036 -5964 7037
		mu 0 4 4440 4441 4442 4443
		f 4 1551 -7038 5958 7038
		mu 0 4 4444 4445 4446 4447
		f 4 1555 7039 5962 -7037
		mu 0 4 4448 4449 4450 4451
		f 4 1548 7040 6982 -7040
		mu 0 4 4452 4453 4454 4455
		f 4 1559 7041 7008 -7041
		mu 0 4 4456 4457 4458 4459
		f 4 1561 -7039 5960 -7042
		mu 0 4 4460 4461 4462 4463
		f 4 -3591 7042 7026 7043
		mu 0 4 4464 4465 4466 4467
		f 4 -3588 -7044 7000 7044
		mu 0 4 4468 4469 4470 4471
		f 4 -3584 7045 5968 -7043
		mu 0 4 4472 4473 4474 4475
		f 4 -3582 7046 5966 -7046
		mu 0 4 4476 4477 4478 4479
		f 4 -3586 -7045 5970 7047
		mu 0 4 4480 4481 4482 4483
		f 4 -3579 -7048 -7003 7048
		mu 0 4 4484 4485 4486 4487
		f 4 -3590 -7049 -7029 7049
		mu 0 4 4488 4489 4490 4491
		f 4 -5972 -7047 -3592 -7050
		mu 0 4 4492 4493 4494 4495
		f 4 -7053 7050 6056 -7052
		mu 0 4 4496 4497 3691 3693
		f 4 -7055 7051 6057 -7054
		mu 0 4 4498 4496 3693 3674
		f 4 -7057 7053 6040 -7056
		mu 0 4 4499 4498 3674 3673
		f 4 -7061 7057 6044 -7060
		mu 0 4 4500 4501 3677 3681
		f 4 -7063 7059 6046 -7062
		mu 0 4 4502 4500 3681 3683
		f 4 -7065 7061 6048 -7064
		mu 0 4 4503 4502 3683 3685
		f 4 -7067 7063 6050 -7066
		mu 0 4 4504 4503 3685 3687
		f 4 -7069 7065 6052 -7068
		mu 0 4 4505 4504 3687 3689
		f 4 -7070 7067 6054 -7051
		mu 0 4 4497 4505 3689 3691
		f 4 -7073 7070 7052 -7072
		mu 0 4 3671 3670 4497 4496
		f 4 -7075 7071 7054 -7074
		mu 0 4 3672 3671 4496 4498
		f 4 -7077 7073 7056 -7076
		mu 0 4 3662 3672 4498 4499
		f 4 -7079 7075 7058 8453
		mu 0 4 3663 3662 4499 4506
		f 4 -7081 7077 7060 -7080
		mu 0 4 3665 3664 4501 4500
		f 4 -7083 7079 7062 -7082
		mu 0 4 3666 3665 4500 4502
		f 4 -7085 7081 7064 -7084
		mu 0 4 3667 3666 4502 4503
		f 4 -7087 7083 7066 -7086
		mu 0 4 3668 3667 4503 4504
		f 4 -7089 7085 7068 -7088
		mu 0 4 3669 3668 4504 4505
		f 4 -7090 7087 7069 -7071
		mu 0 4 3670 3669 4505 4497
		f 4 1075 7090 -5327 7091
		mu 0 4 4507 4508 4509 4510
		f 4 -1075 7092 5343 7093
		mu 0 4 4511 4512 4513 4514
		f 4 1067 -7092 -5346 -7093
		mu 0 4 4515 4516 4517 4518
		f 4 -1065 -7094 6194 -7091
		mu 0 4 4519 4520 4521 4522
		f 4 1023 7094 -5347 7095
		mu 0 4 4523 4524 4525 4526
		f 4 -1023 7096 5363 7097
		mu 0 4 4527 4528 4529 4530
		f 4 1015 -7096 -5366 -7097
		mu 0 4 4531 4532 4533 4534
		f 4 -1013 -7098 6154 -7095
		mu 0 4 4535 4536 4537 4538
		f 4 971 7098 -5384 7099
		mu 0 4 4539 4540 4541 4542
		f 4 -971 7100 5381 7101
		mu 0 4 4543 4544 4545 4546
		f 4 963 -7100 -5385 -7101
		mu 0 4 4547 4548 4549 4550
		f 4 -961 -7102 6174 -7099
		mu 0 4 4551 4552 4553 4554
		f 4 7151 7103 -7150 7152
		mu 0 4 4555 4556 4557 4558
		f 4 7055 7102 -7107 -7106
		mu 0 4 4559 4560 4561 4562
		f 4 -7058 7107 7108 -7104
		mu 0 4 4563 4564 4565 4566
		f 4 -7059 7105 7109 8451
		mu 0 4 4567 4568 4569 4570
		f 4 7149 7111 -7148 7150
		mu 0 4 4571 4572 4573 4574
		f 4 7106 7110 -7115 -7114
		mu 0 4 4575 4576 4577 4578
		f 4 -7109 7115 7116 -7112
		mu 0 4 4579 4580 4581 4582
		f 4 -7110 7113 7117 8449
		mu 0 4 4583 4584 4585 4586
		f 4 7147 7119 9280 9279
		mu 0 4 4587 4588 4589 4590
		f 4 7114 7118 9270 -7122
		mu 0 4 4591 4592 4593 4594
		f 4 -7117 7123 9278 -7120
		mu 0 4 4595 4596 4597 4598
		f 4 -7118 7121 9272 9271
		mu 0 4 4599 4600 4601 4602
		f 4 7145 7127 -7144 7146
		mu 0 4 4603 4604 4605 4606
		f 4 7122 7126 -7131 -7130
		mu 0 4 4607 4608 4609 4610
		f 4 -7125 7131 7132 -7128
		mu 0 4 4611 4612 4613 4614
		f 4 -7126 7129 7133 8445
		mu 0 4 4615 4616 4617 4618
		f 4 7143 7135 8288 8287
		mu 0 4 4619 4620 4621 4622
		f 4 7130 7134 8282 -7138
		mu 0 4 4623 4624 4625 4626
		f 4 -7133 7139 8286 -7136
		mu 0 4 4627 4628 4629 4630
		f 4 -7134 7137 8284 8443
		mu 0 4 4631 4632 4633 4634
		f 4 7164 -8288 8290 8289
		mu 0 4 4635 4619 4622 4636
		f 4 7166 -7147 -7165 7167
		mu 0 4 4637 4603 4606 4638
		f 4 7168 -9280 9282 9281
		mu 0 4 4639 4587 4590 4640
		f 4 7170 -7151 -7169 7171
		mu 0 4 4641 4571 4574 4642
		f 4 7172 -7153 -7171 7173
		mu 0 4 4643 4555 4558 4644
		f 4 -7155 -7173 7175 7174
		mu 0 4 3679 3678 4645 4646
		f 4 -7157 -7175 7177 8140
		mu 0 4 3719 3679 4646 4647
		f 4 -7159 -7177 7179 7716
		mu 0 4 4648 4649 4650 4651
		f 4 -7161 -7179 7181 7302
		mu 0 4 3732 3731 4652 4653
		f 4 -7163 -7181 7183 7212
		mu 0 4 4654 4655 4656 4657
		f 4 7128 -8290 8291 -7135
		mu 0 4 4658 4635 4636 4659
		f 4 7120 -7168 -7129 -7127
		mu 0 4 4660 4637 4638 4661
		f 4 7112 -9282 9283 -7119
		mu 0 4 4662 4639 4640 4663
		f 4 7104 -7172 -7113 -7111
		mu 0 4 4664 4641 4642 4665
		f 4 6042 -7174 -7105 -7103
		mu 0 4 4666 4643 4644 4667
		f 4 -7176 -6043 6039 6080
		mu 0 4 4646 4645 3673 3676
		f 4 -7178 -6081 6078 8138
		mu 0 4 4647 4646 3676 3729
		f 4 -8919 -8921 8922 8923
		mu 0 4 4668 4669 4670 4671
		f 4 -7182 -6101 6098 7300
		mu 0 4 4653 4652 3758 3759
		f 4 -8927 -8929 8930 8931
		mu 0 4 4672 4673 4674 4675
		f 4 -8935 -8937 8938 8939
		mu 0 4 4676 4677 4678 4679
		f 4 -7188 -7189 7185 7182
		mu 0 4 4680 4681 4682 4683
		f 4 -8943 8944 8946 -8948
		mu 0 4 4684 4685 4686 4687
		f 4 -7193 7189 5987 -7192
		mu 0 4 4688 4689 4690 4691
		f 4 -7195 7191 5988 -7194
		mu 0 4 4692 4688 4691 4693
		f 4 -7197 7193 5989 -7196
		mu 0 4 4694 4692 4693 4695
		f 4 -7199 7195 5990 -7198
		mu 0 4 4696 4694 4695 4697
		f 4 -7201 7197 5991 -7200
		mu 0 4 4698 4696 4697 4699
		f 4 -7203 7199 5982 -7202
		mu 0 4 4700 4698 4699 4701
		f 4 -7205 7201 5983 -7204
		mu 0 4 4702 4700 4701 4703
		f 4 -7207 7203 5984 -7206
		mu 0 4 4704 4702 4703 4705
		f 4 -7208 7205 5985 -7185
		mu 0 4 4706 4704 4705 4707
		f 4 -7210 -7211 7208 7186
		mu 0 4 4682 4657 3704 4706
		f 4 -7212 -7213 7209 7188
		mu 0 4 4681 4654 4657 4682
		f 4 -7215 7211 7190 -7214
		mu 0 4 3711 4654 4681 4689
		f 4 -7217 7213 7192 -7216
		mu 0 4 3712 3711 4689 4688
		f 4 -7219 7215 7194 -7218
		mu 0 4 3714 3712 4688 4692
		f 4 -7221 7217 7196 -7220
		mu 0 4 3716 3714 4692 4694
		f 4 -7223 7219 7198 -7222
		mu 0 4 3718 3716 4694 4696
		f 4 -7225 7221 7200 -7224
		mu 0 4 3697 3718 4696 4698
		f 4 -7227 7223 7202 -7226
		mu 0 4 3698 3697 4698 4700
		f 4 -7229 7225 7204 -7228
		mu 0 4 3700 3698 4700 4702
		f 4 -7231 7227 7206 -7230
		mu 0 4 3702 3700 4702 4704
		f 4 -7232 7229 7207 -7209
		mu 0 4 3704 3702 4704 4706
		f 4 -7235 7232 6220 -7234
		mu 0 4 4708 4709 3797 3796
		f 4 -7236 -7237 7233 -6220
		mu 0 4 3787 4710 4708 3796
		f 4 -7238 -7239 7235 -6204
		mu 0 4 3715 4711 4710 3787
		f 4 -7241 7237 6076 -7240
		mu 0 4 4712 4711 3715 3717
		f 4 -7243 7239 6077 -7242
		mu 0 4 4713 4712 3717 3696
		f 4 -7245 7241 6060 -7244
		mu 0 4 4714 4713 3696 3695
		f 4 -7247 7243 6209 -7246
		mu 0 4 4715 4714 3695 3794
		f 4 -7249 7245 6225 -7248
		mu 0 4 4716 4715 3794 3799
		f 4 -7251 7247 6228 -7250
		mu 0 4 4717 4716 3799 3798
		f 4 -7252 -7253 7249 -6228
		mu 0 4 3793 4718 4717 3798
		f 4 -7254 -7255 7251 -6212
		mu 0 4 3699 4719 4718 3793
		f 4 -7257 7253 6064 -7256
		mu 0 4 4720 4719 3699 3701
		f 4 -7259 7255 6066 -7258
		mu 0 4 4721 4720 3701 3703
		f 4 -7260 -7261 7257 6068
		mu 0 4 4656 4722 4721 3703
		f 4 -7262 -7263 7259 7180
		mu 0 4 4655 4723 4722 4656
		f 4 -7265 7261 7159 -7264
		mu 0 4 4724 4723 4655 3710
		f 4 -7267 7263 6070 -7266
		mu 0 4 4725 4724 3710 3709
		f 4 -7269 7265 6072 -7268
		mu 0 4 4726 4725 3709 3713
		f 4 -7271 7267 6201 -7270
		mu 0 4 4727 4726 3713 3788
		f 4 -7272 7269 6217 -7233
		mu 0 4 4709 4727 3788 3797
		f 4 -7275 7272 7234 -7274
		mu 0 4 3741 3740 4709 4708
		f 4 -7276 -7277 7273 7236
		mu 0 4 4710 3789 3741 4708
		f 4 -7278 -7279 7275 7238
		mu 0 4 4711 3744 3789 4710
		f 4 -7281 7277 7240 -7280
		mu 0 4 3745 3744 4711 4712
		f 4 -7283 7279 7242 -7282
		mu 0 4 3747 3745 4712 4713
		f 4 -7285 7281 7244 -7284
		mu 0 4 3749 3747 4713 4714
		f 4 -7287 7283 7246 -7286
		mu 0 4 3792 3749 4714 4715
		f 4 -7289 7285 7248 -7288
		mu 0 4 3752 3792 4715 4716
		f 4 -7291 7287 7250 -7290
		mu 0 4 3753 3752 4716 4717
		f 4 -7292 -7293 7289 7252
		mu 0 4 4718 3795 3753 4717
		f 4 -7294 -7295 7291 7254
		mu 0 4 4719 3756 3795 4718
		f 4 -7297 7293 7256 -7296
		mu 0 4 3757 3756 4719 4720
		f 4 -7299 7295 7258 -7298
		mu 0 4 3759 3757 4720 4721
		f 4 -8951 -8953 8954 8955
		mu 0 4 4728 4729 4730 4731
		f 4 -7302 -7303 7299 7262
		mu 0 4 4723 3732 4653 4722
		f 4 -8959 8960 8962 -8964
		mu 0 4 4732 4733 4734 4735
		f 4 -7307 7303 7266 -7306
		mu 0 4 3735 3733 4724 4725
		f 4 -7309 7305 7268 -7308
		mu 0 4 3737 3735 4725 4726
		f 4 -7311 7307 7270 -7310
		mu 0 4 3786 3737 4726 4727
		f 4 -7312 7309 7271 -7273
		mu 0 4 3740 3786 4727 4709
		f 4 -8967 -8969 8970 8971
		mu 0 4 4736 4737 4738 4739
		f 4 -7316 -7317 7313 7178
		mu 0 4 3731 4740 4741 4652
		f 4 -8975 8976 8978 -8980
		mu 0 4 4742 4743 4744 4745
		f 4 -7321 7317 6102 -7320
		mu 0 4 4746 4747 3730 3734
		f 4 -7323 7319 6104 -7322
		mu 0 4 4748 4746 3734 3736
		f 4 -7325 7321 6106 -7324
		mu 0 4 4749 4748 3736 3743
		f 4 -7327 7323 6233 -7326
		mu 0 4 4750 4749 3743 3804
		f 4 -7329 7325 6249 -7328
		mu 0 4 4751 4750 3804 3816
		f 4 -7331 7327 6265 -7330
		mu 0 4 4752 4751 3816 3828
		f 4 -7333 7329 6281 -7332
		mu 0 4 4753 4752 3828 3840
		f 4 -7335 7331 6297 -7334
		mu 0 4 4754 4753 3840 3852
		f 4 -7337 7333 6313 -7336
		mu 0 4 4755 4756 4757 4758
		f 4 -7339 7335 6329 -7338
		mu 0 4 4759 4760 4761 4762
		f 4 -7341 7337 6347 6896
		mu 0 4 4763 4764 3940 3943
		f 4 -7343 -6897 6894 6888
		mu 0 4 4765 4763 3943 4384
		f 4 -7345 -6889 6886 -7344
		mu 0 4 4766 4765 4384 3964
		f 4 -7347 7343 6363 -7346
		mu 0 4 4767 4766 3964 3967
		f 4 -7349 7345 6379 -7348
		mu 0 4 4768 4767 3967 3985
		f 4 -7351 7347 6395 -7350
		mu 0 4 4769 4768 3985 3997
		f 4 -7353 7349 6411 -7352
		mu 0 4 4770 4769 3997 4009
		f 4 -7355 7351 6427 6912
		mu 0 4 4771 4770 4009 4021
		f 4 -7357 -6913 6910 6904
		mu 0 4 4772 4771 4021 4390
		f 4 -7359 -6905 6902 -7358
		mu 0 4 4773 4772 4390 4036
		f 4 -7361 7357 6443 -7360
		mu 0 4 4774 4773 4036 4039
		f 4 -7363 7359 6459 -7362
		mu 0 4 4775 4774 4039 4057
		f 4 -7365 7361 6475 -7364
		mu 0 4 4776 4775 4057 4069
		f 4 -7367 7363 6491 -7366
		mu 0 4 4777 4776 4069 4081
		f 4 -7369 7365 6507 -7368
		mu 0 4 4778 4777 4081 4093
		f 4 -7371 7367 6523 6928
		mu 0 4 4779 4778 4093 4105
		f 4 -7373 -6929 6926 6920
		mu 0 4 4780 4779 4105 4396
		f 4 -7375 -6921 6918 -7374
		mu 0 4 4781 4780 4396 4120
		f 4 -7377 7373 6539 -7376
		mu 0 4 4782 4781 4120 4123
		f 4 -7379 7375 6555 -7378
		mu 0 4 4783 4782 4123 4141
		f 4 -7381 7377 6571 -7380
		mu 0 4 4784 4783 4141 4153
		f 4 -7383 7379 6587 -7382
		mu 0 4 4785 4784 4153 4165
		f 4 -7385 7381 6603 6944
		mu 0 4 4786 4785 4165 4177
		f 4 -7387 -6945 6942 6936
		mu 0 4 4787 4786 4177 4402
		f 4 -7389 -6937 6934 -7388
		mu 0 4 4788 4787 4402 4192
		f 4 -7391 7387 6619 -7390
		mu 0 4 4789 4788 4192 4195
		f 4 -7393 7389 6635 -7392
		mu 0 4 4790 4789 4195 4213
		f 4 -7395 7391 6651 -7394
		mu 0 4 4791 4790 4213 4225
		f 4 -7397 7393 6667 -7396
		mu 0 4 4792 4791 4225 4237
		f 4 -7399 7395 6683 6960
		mu 0 4 4793 4792 4237 4249
		f 4 -7401 -6961 6958 6952
		mu 0 4 4794 4793 4249 4408
		f 4 -7403 -6953 6950 -7402
		mu 0 4 4795 4794 4408 4264
		f 4 -7405 7401 6699 -7404
		mu 0 4 4796 4795 4264 4267
		f 4 -7407 7403 6715 -7406
		mu 0 4 4797 4796 4267 4285
		f 4 -7409 7405 6731 -7408
		mu 0 4 4798 4797 4285 4297
		f 4 -7411 7407 6747 -7410
		mu 0 4 4799 4798 4297 4309
		f 4 -7413 7409 6763 6976
		mu 0 4 4800 4799 4309 4321
		f 4 -7415 -6977 6974 6968
		mu 0 4 4801 4800 4321 4414
		f 4 -7417 -6969 6966 -7416
		mu 0 4 4802 4801 4414 4332
		f 4 -7419 7415 6779 -7418
		mu 0 4 4803 4802 4332 4335
		f 4 -7420 -7421 7417 6781
		mu 0 4 4334 4804 4803 4335
		f 4 -7422 -7423 7419 -6776
		mu 0 4 4333 4805 4804 4334
		f 4 -7424 -7425 7421 -6972
		mu 0 4 4415 4806 4805 4333
		f 4 -7426 -7427 7423 -6980
		mu 0 4 4320 4807 4806 4415
		f 4 -7428 -7429 7425 -6760
		mu 0 4 4308 4808 4807 4320
		f 4 -7430 -7431 7427 -6744
		mu 0 4 4296 4809 4808 4308
		f 4 -7432 -7433 7429 -6728
		mu 0 4 4284 4810 4809 4296
		f 4 -7434 -7435 7431 -6712
		mu 0 4 4266 4811 4810 4284
		f 4 -7436 -7437 7433 -6696
		mu 0 4 4265 4812 4811 4266
		f 4 -7438 -7439 7435 -6956
		mu 0 4 4409 4813 4812 4265
		f 4 -7440 -7441 7437 -6964
		mu 0 4 4248 4814 4813 4409
		f 4 -7442 -7443 7439 -6680
		mu 0 4 4236 4815 4814 4248
		f 4 -7444 -7445 7441 -6664
		mu 0 4 4224 4816 4815 4236
		f 4 -7446 -7447 7443 -6648
		mu 0 4 4212 4817 4816 4224
		f 4 -7448 -7449 7445 -6632
		mu 0 4 4194 4818 4817 4212
		f 4 -7450 -7451 7447 -6616
		mu 0 4 4193 4819 4818 4194
		f 4 -7452 -7453 7449 -6940
		mu 0 4 4403 4820 4819 4193
		f 4 -7454 -7455 7451 -6948
		mu 0 4 4176 4821 4820 4403
		f 4 -7456 -7457 7453 -6600
		mu 0 4 4164 4822 4821 4176
		f 4 -7458 -7459 7455 -6584
		mu 0 4 4152 4823 4822 4164
		f 4 -7460 -7461 7457 -6568
		mu 0 4 4140 4824 4823 4152
		f 4 -7462 -7463 7459 -6552
		mu 0 4 4122 4825 4824 4140
		f 4 -7464 -7465 7461 -6536
		mu 0 4 4121 4826 4825 4122
		f 4 -7466 -7467 7463 -6924
		mu 0 4 4397 4827 4826 4121
		f 4 -7468 -7469 7465 -6932
		mu 0 4 4104 4828 4827 4397
		f 4 -7470 -7471 7467 -6520
		mu 0 4 4092 4829 4828 4104
		f 4 -7472 -7473 7469 -6504
		mu 0 4 4080 4830 4829 4092
		f 4 -7474 -7475 7471 -6488
		mu 0 4 4068 4831 4830 4080
		f 4 -7476 -7477 7473 -6472
		mu 0 4 4056 4832 4831 4068
		f 4 -7478 -7479 7475 -6456
		mu 0 4 4038 4833 4832 4056
		f 4 -7480 -7481 7477 -6440
		mu 0 4 4037 4834 4833 4038
		f 4 -7482 -7483 7479 -6908
		mu 0 4 4391 4835 4834 4037
		f 4 -7484 -7485 7481 -6916
		mu 0 4 4020 4836 4835 4391
		f 4 -7486 -7487 7483 -6424
		mu 0 4 4008 4837 4836 4020
		f 4 -7488 -7489 7485 -6408
		mu 0 4 3996 4838 4837 4008
		f 4 -7490 -7491 7487 -6392
		mu 0 4 3984 4839 4838 3996
		f 4 -7492 -7493 7489 -6376
		mu 0 4 3966 4840 4839 3984
		f 4 -7494 -7495 7491 -6360
		mu 0 4 3965 4841 4840 3966
		f 4 -7496 -7497 7493 -6892
		mu 0 4 4385 4842 4841 3965
		f 4 -7498 -7499 7495 -6900
		mu 0 4 3942 4843 4842 4385
		f 4 -7500 -7501 7497 -6344
		mu 0 4 3941 4844 4843 3942
		f 4 -7502 -7503 7499 -6332
		mu 0 4 4845 4846 4847 4848
		f 4 -7504 -7505 7501 -6316
		mu 0 4 4849 4850 4851 4852
		f 4 -7506 -7507 7503 -6300
		mu 0 4 3839 4853 4854 3851
		f 4 -7508 -7509 7505 -6284
		mu 0 4 3827 4855 4853 3839
		f 4 -7510 -7511 7507 -6268
		mu 0 4 3815 4856 4855 3827
		f 4 -7512 -7513 7509 -6252
		mu 0 4 3803 4857 4856 3815
		f 4 -7514 -7515 7511 -6236
		mu 0 4 3742 4858 4857 3803
		f 4 -7517 7513 6110 -7516
		mu 0 4 4859 4858 3742 3746
		f 4 -7519 7515 6241 -7518
		mu 0 4 4860 4859 3746 3810
		f 4 -7521 7517 6257 -7520
		mu 0 4 4861 4860 3810 3822
		f 4 -7523 7519 6273 -7522
		mu 0 4 4862 4861 3822 3834
		f 4 -7525 7521 6289 -7524
		mu 0 4 4863 4862 3834 3846
		f 4 -7527 7523 6305 -7526
		mu 0 4 4864 4863 3846 3858
		f 4 -7529 7525 6321 -7528
		mu 0 4 4865 4866 4867 4868
		f 4 -7531 7527 6337 -7530
		mu 0 4 4869 4870 4871 4872
		f 4 -7533 7529 6351 6804
		mu 0 4 4873 4874 3948 3951
		f 4 -7535 -6805 6801 6796
		mu 0 4 4875 4873 3951 4346
		f 4 -7537 -6797 6793 -7536
		mu 0 4 4876 4875 4346 3972
		f 4 -7539 7535 6367 -7538
		mu 0 4 4877 4876 3972 3975
		f 4 -7541 7537 6383 -7540
		mu 0 4 4878 4877 3975 3989
		f 4 -7543 7539 6399 -7542
		mu 0 4 4879 4878 3989 4001
		f 4 -7545 7541 6415 -7544
		mu 0 4 4880 4879 4001 4013
		f 4 -7547 7543 6431 6820
		mu 0 4 4881 4880 4013 4025
		f 4 -7549 -6821 6817 6812
		mu 0 4 4882 4881 4025 4352
		f 4 -7551 -6813 6809 -7550
		mu 0 4 4883 4882 4352 4044
		f 4 -7553 7549 6447 -7552
		mu 0 4 4884 4883 4044 4047
		f 4 -7555 7551 6463 -7554
		mu 0 4 4885 4884 4047 4061
		f 4 -7557 7553 6479 -7556
		mu 0 4 4886 4885 4061 4073
		f 4 -7559 7555 6495 -7558
		mu 0 4 4887 4886 4073 4085
		f 4 -7561 7557 6511 -7560
		mu 0 4 4888 4887 4085 4097
		f 4 -7563 7559 6527 6836
		mu 0 4 4889 4888 4097 4109
		f 4 -7565 -6837 6833 6828
		mu 0 4 4890 4889 4109 4358
		f 4 -7567 -6829 6825 -7566
		mu 0 4 4891 4890 4358 4128
		f 4 -7569 7565 6543 -7568
		mu 0 4 4892 4891 4128 4131
		f 4 -7571 7567 6559 -7570
		mu 0 4 4893 4892 4131 4145
		f 4 -7573 7569 6575 -7572
		mu 0 4 4894 4893 4145 4157
		f 4 -7575 7571 6591 -7574
		mu 0 4 4895 4894 4157 4169
		f 4 -7577 7573 6607 6852
		mu 0 4 4896 4895 4169 4181
		f 4 -7579 -6853 6849 6844
		mu 0 4 4897 4896 4181 4364
		f 4 -7581 -6845 6841 -7580
		mu 0 4 4898 4897 4364 4200
		f 4 -7583 7579 6623 -7582
		mu 0 4 4899 4898 4200 4203
		f 4 -7585 7581 6639 -7584
		mu 0 4 4900 4899 4203 4217
		f 4 -7587 7583 6655 -7586
		mu 0 4 4901 4900 4217 4229
		f 4 -7589 7585 6671 -7588
		mu 0 4 4902 4901 4229 4241
		f 4 -7591 7587 6687 6868
		mu 0 4 4903 4902 4241 4253
		f 4 -7593 -6869 6865 6860
		mu 0 4 4904 4903 4253 4370
		f 4 -7595 -6861 6857 -7594
		mu 0 4 4905 4904 4370 4272
		f 4 -7597 7593 6703 -7596
		mu 0 4 4906 4905 4272 4275
		f 4 -7599 7595 6719 -7598
		mu 0 4 4907 4906 4275 4289
		f 4 -7601 7597 6735 -7600
		mu 0 4 4908 4907 4289 4301
		f 4 -7603 7599 6751 -7602
		mu 0 4 4909 4908 4301 4313
		f 4 -7605 7601 6767 6884
		mu 0 4 4910 4909 4313 4325
		f 4 -7607 -6885 6881 6876
		mu 0 4 4911 4910 4325 4376
		f 4 -7609 -6877 6873 -7608
		mu 0 4 4912 4911 4376 4338
		f 4 -7611 7607 6783 -7610
		mu 0 4 4913 4912 4338 4341
		f 4 -7613 7609 6786 -7612
		mu 0 4 4914 4913 4341 4340
		f 4 -7614 -7615 7611 -6786
		mu 0 4 4339 4915 4914 4340
		f 4 -7616 -7617 7613 -6872
		mu 0 4 4377 4916 4915 4339
		f 4 -7618 -7619 7615 -6880
		mu 0 4 4324 4917 4916 4377
		f 4 -7620 -7621 7617 -6770
		mu 0 4 4312 4918 4917 4324
		f 4 -7622 -7623 7619 -6754
		mu 0 4 4300 4919 4918 4312
		f 4 -7624 -7625 7621 -6738
		mu 0 4 4288 4920 4919 4300
		f 4 -7626 -7627 7623 -6722
		mu 0 4 4274 4921 4920 4288
		f 4 -7628 -7629 7625 -6706
		mu 0 4 4273 4922 4921 4274
		f 4 -7630 -7631 7627 -6856
		mu 0 4 4371 4923 4922 4273
		f 4 -7632 -7633 7629 -6864
		mu 0 4 4252 4924 4923 4371
		f 4 -7634 -7635 7631 -6690
		mu 0 4 4240 4925 4924 4252
		f 4 -7636 -7637 7633 -6674
		mu 0 4 4228 4926 4925 4240
		f 4 -7638 -7639 7635 -6658
		mu 0 4 4216 4927 4926 4228
		f 4 -7640 -7641 7637 -6642
		mu 0 4 4202 4928 4927 4216
		f 4 -7642 -7643 7639 -6626
		mu 0 4 4201 4929 4928 4202
		f 4 -7644 -7645 7641 -6840
		mu 0 4 4365 4930 4929 4201
		f 4 -7646 -7647 7643 -6848
		mu 0 4 4180 4931 4930 4365
		f 4 -7648 -7649 7645 -6610
		mu 0 4 4168 4932 4931 4180
		f 4 -7650 -7651 7647 -6594
		mu 0 4 4156 4933 4932 4168
		f 4 -7652 -7653 7649 -6578
		mu 0 4 4144 4934 4933 4156
		f 4 -7654 -7655 7651 -6562
		mu 0 4 4130 4935 4934 4144
		f 4 -7656 -7657 7653 -6546
		mu 0 4 4129 4936 4935 4130
		f 4 -7658 -7659 7655 -6824
		mu 0 4 4359 4937 4936 4129
		f 4 -7660 -7661 7657 -6832
		mu 0 4 4108 4938 4937 4359
		f 4 -7662 -7663 7659 -6530
		mu 0 4 4096 4939 4938 4108
		f 4 -7664 -7665 7661 -6514
		mu 0 4 4084 4940 4939 4096
		f 4 -7666 -7667 7663 -6498
		mu 0 4 4072 4941 4940 4084
		f 4 -7668 -7669 7665 -6482
		mu 0 4 4060 4942 4941 4072
		f 4 -7670 -7671 7667 -6466
		mu 0 4 4046 4943 4942 4060
		f 4 -7672 -7673 7669 -6450
		mu 0 4 4045 4944 4943 4046
		f 4 -7674 -7675 7671 -6808
		mu 0 4 4353 4945 4944 4045
		f 4 -7676 -7677 7673 -6816
		mu 0 4 4024 4946 4945 4353
		f 4 -7678 -7679 7675 -6434
		mu 0 4 4012 4947 4946 4024
		f 4 -7680 -7681 7677 -6418
		mu 0 4 4000 4948 4947 4012
		f 4 -7682 -7683 7679 -6402
		mu 0 4 3988 4949 4948 4000
		f 4 -7684 -7685 7681 -6386
		mu 0 4 3974 4950 4949 3988
		f 4 -7686 -7687 7683 -6370
		mu 0 4 3973 4951 4950 3974
		f 4 -7688 -7689 7685 -6792
		mu 0 4 4347 4952 4951 3973
		f 4 -7690 -7691 7687 -6800
		mu 0 4 3950 4953 4952 4347
		f 4 -7692 -7693 7689 -6354
		mu 0 4 3949 4954 4953 3950
		f 4 -7694 -7695 7691 -6340
		mu 0 4 4955 4956 4957 4958
		f 4 -7696 -7697 7693 -6324
		mu 0 4 4959 4960 4961 4962
		f 4 -7698 -7699 7695 -6308
		mu 0 4 3845 4963 4964 3857
		f 4 -7700 -7701 7697 -6292
		mu 0 4 3833 4965 4963 3845
		f 4 -7702 -7703 7699 -6276
		mu 0 4 3821 4966 4965 3833
		f 4 -7704 -7705 7701 -6260
		mu 0 4 3809 4967 4966 3821
		f 4 -7706 -7707 7703 -6244
		mu 0 4 3748 4968 4967 3809
		f 4 -7709 7705 6114 -7708
		mu 0 4 4969 4968 3748 3755
		f 4 -7711 7707 6116 -7710
		mu 0 4 4970 4969 3755 3754
		f 4 -7712 7709 6117 -7313
		mu 0 4 4971 4970 3754 3758
		f 4 -7714 -7715 7712 7314
		mu 0 4 4741 4651 3783 4971
		f 4 -7716 -7717 7713 7316
		mu 0 4 4740 4648 4651 4741
		f 4 -7719 7715 7318 -7718
		mu 0 4 3766 4648 4740 4747
		f 4 -7721 7717 7320 -7720
		mu 0 4 3767 3766 4747 4746
		f 4 -7723 7719 7322 -7722
		mu 0 4 3769 3767 4746 4748
		f 4 -7725 7721 7324 -7724
		mu 0 4 3771 3769 4748 4749
		f 4 -7727 7723 7326 -7726
		mu 0 4 3802 3771 4749 4750
		f 4 -7729 7725 7328 -7728
		mu 0 4 3814 3802 4750 4751
		f 4 -7731 7727 7330 -7730
		mu 0 4 3826 3814 4751 4752
		f 4 -7733 7729 7332 -7732
		mu 0 4 3838 3826 4752 4753
		f 4 -7735 7731 7334 -7734
		mu 0 4 3850 3838 4753 4754
		f 4 -7737 7733 7336 -7736
		mu 0 4 3866 3865 4756 4755
		f 4 -7739 7735 7338 -7738
		mu 0 4 3898 3897 4760 4759
		f 4 -7741 7737 7340 7339
		mu 0 4 3939 3938 4764 4763
		f 4 -7743 -7340 7342 7341
		mu 0 4 4381 3939 4763 4765
		f 4 -7745 -7342 7344 -7744
		mu 0 4 3962 4381 4765 4766
		f 4 -7747 7743 7346 -7746
		mu 0 4 3963 3962 4766 4767
		f 4 -7749 7745 7348 -7748
		mu 0 4 3983 3963 4767 4768
		f 4 -7751 7747 7350 -7750
		mu 0 4 3995 3983 4768 4769
		f 4 -7753 7749 7352 -7752
		mu 0 4 4007 3995 4769 4770
		f 4 -7755 7751 7354 7353
		mu 0 4 4019 4007 4770 4771
		f 4 -7757 -7354 7356 7355
		mu 0 4 4387 4019 4771 4772
		f 4 -7759 -7356 7358 -7758
		mu 0 4 4034 4387 4772 4773
		f 4 -7761 7757 7360 -7760
		mu 0 4 4035 4034 4773 4774
		f 4 -7763 7759 7362 -7762
		mu 0 4 4055 4035 4774 4775
		f 4 -7765 7761 7364 -7764
		mu 0 4 4067 4055 4775 4776
		f 4 -7767 7763 7366 -7766
		mu 0 4 4079 4067 4776 4777
		f 4 -7769 7765 7368 -7768
		mu 0 4 4091 4079 4777 4778
		f 4 -7771 7767 7370 7369
		mu 0 4 4103 4091 4778 4779
		f 4 -7773 -7370 7372 7371
		mu 0 4 4393 4103 4779 4780
		f 4 -7775 -7372 7374 -7774
		mu 0 4 4118 4393 4780 4781
		f 4 -7777 7773 7376 -7776
		mu 0 4 4119 4118 4781 4782
		f 4 -7779 7775 7378 -7778
		mu 0 4 4139 4119 4782 4783
		f 4 -7781 7777 7380 -7780
		mu 0 4 4151 4139 4783 4784
		f 4 -7783 7779 7382 -7782
		mu 0 4 4163 4151 4784 4785
		f 4 -7785 7781 7384 7383
		mu 0 4 4175 4163 4785 4786
		f 4 -7787 -7384 7386 7385
		mu 0 4 4399 4175 4786 4787
		f 4 -7789 -7386 7388 -7788
		mu 0 4 4190 4399 4787 4788
		f 4 -7791 7787 7390 -7790
		mu 0 4 4191 4190 4788 4789
		f 4 -7793 7789 7392 -7792
		mu 0 4 4211 4191 4789 4790
		f 4 -7795 7791 7394 -7794
		mu 0 4 4223 4211 4790 4791
		f 4 -7797 7793 7396 -7796
		mu 0 4 4235 4223 4791 4792
		f 4 -7799 7795 7398 7397
		mu 0 4 4247 4235 4792 4793
		f 4 -7801 -7398 7400 7399
		mu 0 4 4405 4247 4793 4794
		f 4 -7803 -7400 7402 -7802
		mu 0 4 4262 4405 4794 4795
		f 4 -7805 7801 7404 -7804
		mu 0 4 4263 4262 4795 4796
		f 4 -7807 7803 7406 -7806
		mu 0 4 4283 4263 4796 4797
		f 4 -7809 7805 7408 -7808
		mu 0 4 4295 4283 4797 4798
		f 4 -7811 7807 7410 -7810
		mu 0 4 4307 4295 4798 4799
		f 4 -7813 7809 7412 7411
		mu 0 4 4319 4307 4799 4800
		f 4 -7815 -7412 7414 7413
		mu 0 4 4411 4319 4800 4801
		f 4 -7817 -7414 7416 -7816
		mu 0 4 4331 4411 4801 4802
		f 4 -7819 7815 7418 -7818
		mu 0 4 3927 4331 4802 4803
		f 4 -7820 -7821 7817 7420
		mu 0 4 4804 3924 3927 4803
		f 4 -7822 -7823 7819 7422
		mu 0 4 4805 4328 3924 4804
		f 4 -7824 -7825 7821 7424
		mu 0 4 4806 4413 4328 4805
		f 4 -7826 -7827 7823 7426
		mu 0 4 4807 4317 4413 4806
		f 4 -7828 -7829 7825 7428
		mu 0 4 4808 4305 4317 4807
		f 4 -7830 -7831 7827 7430
		mu 0 4 4809 4293 4305 4808
		f 4 -7832 -7833 7829 7432
		mu 0 4 4810 4281 4293 4809
		f 4 -7834 -7835 7831 7434
		mu 0 4 4811 4259 4281 4810
		f 4 -7836 -7837 7833 7436
		mu 0 4 4812 4256 4259 4811
		f 4 -7838 -7839 7835 7438
		mu 0 4 4813 4407 4256 4812
		f 4 -7840 -7841 7837 7440
		mu 0 4 4814 4245 4407 4813
		f 4 -7842 -7843 7839 7442
		mu 0 4 4815 4233 4245 4814
		f 4 -7844 -7845 7841 7444
		mu 0 4 4816 4221 4233 4815
		f 4 -7846 -7847 7843 7446
		mu 0 4 4817 4209 4221 4816
		f 4 -7848 -7849 7845 7448
		mu 0 4 4818 4187 4209 4817
		f 4 -7850 -7851 7847 7450
		mu 0 4 4819 4184 4187 4818
		f 4 -7852 -7853 7849 7452
		mu 0 4 4820 4401 4184 4819
		f 4 -7854 -7855 7851 7454
		mu 0 4 4821 4173 4401 4820
		f 4 -7856 -7857 7853 7456
		mu 0 4 4822 4161 4173 4821
		f 4 -7858 -7859 7855 7458
		mu 0 4 4823 4149 4161 4822
		f 4 -7860 -7861 7857 7460
		mu 0 4 4824 4137 4149 4823
		f 4 -7862 -7863 7859 7462
		mu 0 4 4825 4115 4137 4824
		f 4 -7864 -7865 7861 7464
		mu 0 4 4826 4112 4115 4825
		f 4 -7866 -7867 7863 7466
		mu 0 4 4827 4395 4112 4826
		f 4 -7868 -7869 7865 7468
		mu 0 4 4828 4101 4395 4827
		f 4 -7870 -7871 7867 7470
		mu 0 4 4829 4089 4101 4828
		f 4 -7872 -7873 7869 7472
		mu 0 4 4830 4077 4089 4829
		f 4 -7874 -7875 7871 7474
		mu 0 4 4831 4065 4077 4830
		f 4 -7876 -7877 7873 7476
		mu 0 4 4832 4053 4065 4831
		f 4 -7878 -7879 7875 7478
		mu 0 4 4833 4031 4053 4832
		f 4 -7880 -7881 7877 7480
		mu 0 4 4834 4028 4031 4833
		f 4 -7882 -7883 7879 7482
		mu 0 4 4835 4389 4028 4834
		f 4 -7884 -7885 7881 7484
		mu 0 4 4836 4017 4389 4835
		f 4 -7886 -7887 7883 7486
		mu 0 4 4837 4005 4017 4836
		f 4 -7888 -7889 7885 7488
		mu 0 4 4838 3993 4005 4837
		f 4 -7890 -7891 7887 7490
		mu 0 4 4839 3981 3993 4838
		f 4 -7892 -7893 7889 7492
		mu 0 4 4840 3959 3981 4839
		f 4 -7894 -7895 7891 7494
		mu 0 4 4841 3956 3959 4840
		f 4 -7896 -7897 7893 7496
		mu 0 4 4842 4383 3956 4841
		f 4 -7898 -7899 7895 7498
		mu 0 4 4843 3935 4383 4842
		f 4 -7900 -7901 7897 7500
		mu 0 4 4844 3932 3935 4843
		f 4 -7902 -7903 7899 7502
		mu 0 4 4846 3904 3907 4847
		f 4 -7904 -7905 7901 7504
		mu 0 4 4850 3872 3875 4851
		f 4 -7906 -7907 7903 7506
		mu 0 4 4853 3841 3853 4854
		f 4 -7908 -7909 7905 7508
		mu 0 4 4855 3829 3841 4853
		f 4 -7910 -7911 7907 7510
		mu 0 4 4856 3817 3829 4855
		f 4 -7912 -7913 7909 7512
		mu 0 4 4857 3805 3817 4856
		f 4 -7914 -7915 7911 7514
		mu 0 4 4858 3774 3805 4857
		f 4 -7917 7913 7516 -7916
		mu 0 4 3775 3774 4858 4859
		f 4 -7919 7915 7518 -7918
		mu 0 4 3808 3775 4859 4860
		f 4 -7921 7917 7520 -7920
		mu 0 4 3820 3808 4860 4861
		f 4 -7923 7919 7522 -7922
		mu 0 4 3832 3820 4861 4862
		f 4 -7925 7921 7524 -7924
		mu 0 4 3844 3832 4862 4863
		f 4 -7927 7923 7526 -7926
		mu 0 4 3856 3844 4863 4864
		f 4 -7929 7925 7528 -7928
		mu 0 4 3882 3881 4866 4865
		f 4 -7931 7927 7530 -7930
		mu 0 4 3914 3913 4870 4869
		f 4 -7933 7929 7532 7531
		mu 0 4 3946 3945 4874 4873
		f 4 -7935 -7532 7534 7533
		mu 0 4 4349 3946 4873 4875
		f 4 -7937 -7534 7536 -7936
		mu 0 4 3969 4349 4875 4876
		f 4 -7939 7935 7538 -7938
		mu 0 4 3970 3969 4876 4877
		f 4 -7941 7937 7540 -7940
		mu 0 4 3986 3970 4877 4878
		f 4 -7943 7939 7542 -7942
		mu 0 4 3998 3986 4878 4879
		f 4 -7945 7941 7544 -7944
		mu 0 4 4010 3998 4879 4880
		f 4 -7947 7943 7546 7545
		mu 0 4 4022 4010 4880 4881
		f 4 -7949 -7546 7548 7547
		mu 0 4 4355 4022 4881 4882
		f 4 -7951 -7548 7550 -7950
		mu 0 4 4041 4355 4882 4883
		f 4 -7953 7949 7552 -7952
		mu 0 4 4042 4041 4883 4884
		f 4 -7955 7951 7554 -7954
		mu 0 4 4058 4042 4884 4885
		f 4 -7957 7953 7556 -7956
		mu 0 4 4070 4058 4885 4886
		f 4 -7959 7955 7558 -7958
		mu 0 4 4082 4070 4886 4887;
	setAttr ".fc[4000:4499]"
		f 4 -7961 7957 7560 -7960
		mu 0 4 4094 4082 4887 4888
		f 4 -7963 7959 7562 7561
		mu 0 4 4106 4094 4888 4889
		f 4 -7965 -7562 7564 7563
		mu 0 4 4361 4106 4889 4890
		f 4 -7967 -7564 7566 -7966
		mu 0 4 4125 4361 4890 4891
		f 4 -7969 7965 7568 -7968
		mu 0 4 4126 4125 4891 4892
		f 4 -7971 7967 7570 -7970
		mu 0 4 4142 4126 4892 4893
		f 4 -7973 7969 7572 -7972
		mu 0 4 4154 4142 4893 4894
		f 4 -7975 7971 7574 -7974
		mu 0 4 4166 4154 4894 4895
		f 4 -7977 7973 7576 7575
		mu 0 4 4178 4166 4895 4896
		f 4 -7979 -7576 7578 7577
		mu 0 4 4367 4178 4896 4897
		f 4 -7981 -7578 7580 -7980
		mu 0 4 4197 4367 4897 4898
		f 4 -7983 7979 7582 -7982
		mu 0 4 4198 4197 4898 4899
		f 4 -7985 7981 7584 -7984
		mu 0 4 4214 4198 4899 4900
		f 4 -7987 7983 7586 -7986
		mu 0 4 4226 4214 4900 4901
		f 4 -7989 7985 7588 -7988
		mu 0 4 4238 4226 4901 4902
		f 4 -7991 7987 7590 7589
		mu 0 4 4250 4238 4902 4903
		f 4 -7993 -7590 7592 7591
		mu 0 4 4373 4250 4903 4904
		f 4 -7995 -7592 7594 -7994
		mu 0 4 4269 4373 4904 4905
		f 4 -7997 7993 7596 -7996
		mu 0 4 4270 4269 4905 4906
		f 4 -7999 7995 7598 -7998
		mu 0 4 4286 4270 4906 4907
		f 4 -8001 7997 7600 -8000
		mu 0 4 4298 4286 4907 4908
		f 4 -8003 7999 7602 -8002
		mu 0 4 4310 4298 4908 4909
		f 4 -8005 8001 7604 7603
		mu 0 4 4322 4310 4909 4910
		f 4 -8007 -7604 7606 7605
		mu 0 4 4379 4322 4910 4911
		f 4 -8009 -7606 7608 -8008
		mu 0 4 4337 4379 4911 4912
		f 4 -8011 8007 7610 -8010
		mu 0 4 3929 4337 4912 4913
		f 4 -8013 8009 7612 -8012
		mu 0 4 3930 3929 4913 4914
		f 4 -8014 -8015 8011 7614
		mu 0 4 4915 4342 3930 4914
		f 4 -8016 -8017 8013 7616
		mu 0 4 4916 4374 4342 4915
		f 4 -8018 -8019 8015 7618
		mu 0 4 4917 4327 4374 4916
		f 4 -8020 -8021 8017 7620
		mu 0 4 4918 4315 4327 4917
		f 4 -8022 -8023 8019 7622
		mu 0 4 4919 4303 4315 4918
		f 4 -8024 -8025 8021 7624
		mu 0 4 4920 4291 4303 4919
		f 4 -8026 -8027 8023 7626
		mu 0 4 4921 4279 4291 4920
		f 4 -8028 -8029 8025 7628
		mu 0 4 4922 4276 4279 4921
		f 4 -8030 -8031 8027 7630
		mu 0 4 4923 4368 4276 4922
		f 4 -8032 -8033 8029 7632
		mu 0 4 4924 4255 4368 4923
		f 4 -8034 -8035 8031 7634
		mu 0 4 4925 4243 4255 4924
		f 4 -8036 -8037 8033 7636
		mu 0 4 4926 4231 4243 4925
		f 4 -8038 -8039 8035 7638
		mu 0 4 4927 4219 4231 4926
		f 4 -8040 -8041 8037 7640
		mu 0 4 4928 4207 4219 4927
		f 4 -8042 -8043 8039 7642
		mu 0 4 4929 4204 4207 4928
		f 4 -8044 -8045 8041 7644
		mu 0 4 4930 4362 4204 4929
		f 4 -8046 -8047 8043 7646
		mu 0 4 4931 4183 4362 4930
		f 4 -8048 -8049 8045 7648
		mu 0 4 4932 4171 4183 4931
		f 4 -8050 -8051 8047 7650
		mu 0 4 4933 4159 4171 4932
		f 4 -8052 -8053 8049 7652
		mu 0 4 4934 4147 4159 4933
		f 4 -8054 -8055 8051 7654
		mu 0 4 4935 4135 4147 4934
		f 4 -8056 -8057 8053 7656
		mu 0 4 4936 4132 4135 4935
		f 4 -8058 -8059 8055 7658
		mu 0 4 4937 4356 4132 4936
		f 4 -8060 -8061 8057 7660
		mu 0 4 4938 4111 4356 4937
		f 4 -8062 -8063 8059 7662
		mu 0 4 4939 4099 4111 4938
		f 4 -8064 -8065 8061 7664
		mu 0 4 4940 4087 4099 4939
		f 4 -8066 -8067 8063 7666
		mu 0 4 4941 4075 4087 4940
		f 4 -8068 -8069 8065 7668
		mu 0 4 4942 4063 4075 4941
		f 4 -8070 -8071 8067 7670
		mu 0 4 4943 4051 4063 4942
		f 4 -8072 -8073 8069 7672
		mu 0 4 4944 4048 4051 4943
		f 4 -8074 -8075 8071 7674
		mu 0 4 4945 4350 4048 4944
		f 4 -8076 -8077 8073 7676
		mu 0 4 4946 4027 4350 4945
		f 4 -8078 -8079 8075 7678
		mu 0 4 4947 4015 4027 4946
		f 4 -8080 -8081 8077 7680
		mu 0 4 4948 4003 4015 4947
		f 4 -8082 -8083 8079 7682
		mu 0 4 4949 3991 4003 4948
		f 4 -8084 -8085 8081 7684
		mu 0 4 4950 3979 3991 4949
		f 4 -8086 -8087 8083 7686
		mu 0 4 4951 3976 3979 4950
		f 4 -8088 -8089 8085 7688
		mu 0 4 4952 4344 3976 4951
		f 4 -8090 -8091 8087 7690
		mu 0 4 4953 3955 4344 4952
		f 4 -8092 -8093 8089 7692
		mu 0 4 4954 3952 3955 4953
		f 4 -8094 -8095 8091 7694
		mu 0 4 4956 3920 3923 4957
		f 4 -8096 -8097 8093 7696
		mu 0 4 4960 3888 3891 4961
		f 4 -8098 -8099 8095 7698
		mu 0 4 4963 3847 3859 4964
		f 4 -8100 -8101 8097 7700
		mu 0 4 4965 3835 3847 4963
		f 4 -8102 -8103 8099 7702
		mu 0 4 4966 3823 3835 4965
		f 4 -8104 -8105 8101 7704
		mu 0 4 4967 3811 3823 4966
		f 4 -8106 -8107 8103 7706
		mu 0 4 4968 3778 3811 4967
		f 4 -8109 8105 7708 -8108
		mu 0 4 3779 3778 4968 4969
		f 4 -8111 8107 7710 -8110
		mu 0 4 3781 3779 4969 4970
		f 4 -8112 8109 7711 -7713
		mu 0 4 3783 3781 4970 4971
		f 4 -8114 -8115 8112 6120
		mu 0 4 4650 4972 4973 3782
		f 4 -8116 -8117 8113 7176
		mu 0 4 4649 4974 4972 4650
		f 4 -8119 8115 7155 -8118
		mu 0 4 4975 4974 4649 3765
		f 4 -8121 8117 6122 -8120
		mu 0 4 4976 4975 3765 3764
		f 4 -8123 8119 6124 -8122
		mu 0 4 4977 4976 3764 3768
		f 4 -8125 8121 6126 -8124
		mu 0 4 4978 4977 3768 3770
		f 4 -8127 8123 6128 -8126
		mu 0 4 4979 4978 3770 3773
		f 4 -8129 8125 6130 -8128
		mu 0 4 4980 4979 3773 3772
		f 4 -8131 8127 6132 -8130
		mu 0 4 4981 4980 3772 3777
		f 4 -8133 8129 6134 -8132
		mu 0 4 4982 4981 3777 3776
		f 4 -8135 8131 6136 -8134
		mu 0 4 4983 4982 3776 3780
		f 4 -8136 8133 6137 -8113
		mu 0 4 4973 4983 3780 3782
		f 4 -8983 -8985 8986 8987
		mu 0 4 4984 4985 4986 4987
		f 4 -8140 -8141 8137 8116
		mu 0 4 4974 3719 4647 4972
		f 4 -8991 8992 8994 -8996
		mu 0 4 4988 4989 4990 4991
		f 4 -8145 8141 8120 -8144
		mu 0 4 3721 3720 4975 4976
		f 4 -8147 8143 8122 -8146
		mu 0 4 3722 3721 4976 4977
		f 4 -8149 8145 8124 -8148
		mu 0 4 3723 3722 4977 4978
		f 4 -8151 8147 8126 -8150
		mu 0 4 3724 3723 4978 4979
		f 4 -8153 8149 8128 -8152
		mu 0 4 3725 3724 4979 4980
		f 4 -8155 8151 8130 -8154
		mu 0 4 3726 3725 4980 4981
		f 4 -8157 8153 8132 -8156
		mu 0 4 3727 3726 4981 4982
		f 4 -8159 8155 8134 -8158
		mu 0 4 3728 3727 4982 4983
		f 4 -8160 8157 8135 -8137
		mu 0 4 3729 3728 4983 4973
		f 4 -8163 8160 -7139 -8162
		mu 0 4 4992 4993 4994 4995
		f 4 -8165 8161 7141 8421
		mu 0 4 4996 4997 4998 4999
		f 4 -8167 8163 7140 -8166
		mu 0 4 5000 5001 5002 5003
		f 4 -8169 8165 -7143 7144
		mu 0 4 5004 5005 5006 5007
		f 4 -8171 -7145 -7164 7165
		mu 0 4 5008 5004 5007 5009
		f 4 -8172 -7166 -7137 -8161
		mu 0 4 5010 5008 5009 5011
		f 4 -8175 8172 8162 -8174
		mu 0 4 5012 5013 4993 4992
		f 4 -8177 8173 8164 8423
		mu 0 4 5014 5015 4997 4996
		f 4 -8179 8175 8166 -8178
		mu 0 4 5016 5017 5001 5000
		f 4 -8999 9000 9002 9003
		mu 0 4 5018 5019 5020 5021
		f 4 -8183 -8168 8170 8169
		mu 0 4 5022 5023 5004 5008
		f 4 -9007 -9009 9010 -9012
		mu 0 4 5024 5025 5026 5027
		f 4 -8187 8184 8174 -8186
		mu 0 4 5028 5029 5013 5012
		f 4 -8189 8185 8176 8425
		mu 0 4 5030 5031 5015 5014
		f 4 -8191 8187 8178 -8190
		mu 0 4 5032 5033 5017 5016
		f 4 -8193 8189 8180 8179
		mu 0 4 5034 5035 5036 5023
		f 4 -8195 -8180 8182 8181
		mu 0 4 5037 5034 5023 5022
		f 4 -8196 -8182 8183 -8185
		mu 0 4 5038 5037 5022 5039
		f 4 -8199 8196 8186 -8198
		mu 0 4 5040 5041 5029 5028
		f 4 -8201 8197 8188 8427
		mu 0 4 5042 5043 5031 5030
		f 4 -8203 8199 8190 -8202
		mu 0 4 5044 5045 5033 5032
		f 4 -9015 9016 9018 9019
		mu 0 4 5046 5047 5048 5049
		f 4 -8207 -8192 8194 8193
		mu 0 4 5050 5051 5034 5037
		f 4 -9023 -9025 9026 -9028
		mu 0 4 5052 5053 5054 5055
		f 4 -8211 8208 8198 -8210
		mu 0 4 5056 5057 5041 5040
		f 4 -8213 8209 8200 8429
		mu 0 4 5058 5059 5043 5042
		f 4 -8215 8211 8202 -8214
		mu 0 4 5060 5061 5045 5044
		f 4 -8217 8213 8204 8203
		mu 0 4 5062 5063 5064 5051
		f 4 -8219 -8204 8206 8205
		mu 0 4 5065 5062 5051 5050
		f 4 -8220 -8206 8207 -8209
		mu 0 4 5066 5065 5050 5067
		f 4 -8223 8220 8210 -8222
		mu 0 4 5068 5069 5057 5056
		f 4 -8225 8221 8212 8431
		mu 0 4 5070 5071 5059 5058
		f 4 -8227 8223 8214 -8226
		mu 0 4 5072 5073 5061 5060
		f 4 -9031 9032 9034 9035
		mu 0 4 5074 5075 5076 5077
		f 4 -8231 -8216 8218 8217
		mu 0 4 5078 5079 5062 5065
		f 4 -9039 -9041 9042 -9044
		mu 0 4 5080 5081 5082 5083
		f 4 -8235 8232 8222 -8234
		mu 0 4 5084 5085 5069 5068
		f 4 -8237 8233 8224 8433
		mu 0 4 5086 5087 5071 5070
		f 4 -8239 8235 8226 -8238
		mu 0 4 5088 5089 5073 5072
		f 4 -8241 8237 8228 8227
		mu 0 4 5090 5091 5092 5079
		f 4 -8243 -8228 8230 8229
		mu 0 4 5093 5090 5079 5078
		f 4 -8244 -8230 8231 -8233
		mu 0 4 5094 5093 5078 5095
		f 4 -8247 8244 8234 -8246
		mu 0 4 5096 5097 5085 5084
		f 4 -8249 8245 8236 8435
		mu 0 4 5098 5099 5087 5086
		f 4 -8251 8247 8238 -8250
		mu 0 4 5100 5101 5089 5088
		f 4 -9047 9048 9050 9051
		mu 0 4 5102 5103 5104 5105
		f 4 -8255 -8240 8242 8241
		mu 0 4 5106 5107 5090 5093
		f 4 -9055 -9057 9058 -9060
		mu 0 4 5108 5109 5110 5111
		f 4 -8259 8256 8246 -8258
		mu 0 4 5112 5113 5097 5096
		f 4 -8261 8257 8248 8437
		mu 0 4 5114 5115 5099 5098
		f 4 -8263 8259 8250 -8262
		mu 0 4 5116 5117 5101 5100
		f 4 -8265 8261 8252 8251
		mu 0 4 5118 5119 5120 5107
		f 4 -8267 -8252 8254 8253
		mu 0 4 5121 5118 5107 5106
		f 4 -8268 -8254 8255 -8257
		mu 0 4 5122 5121 5106 5123
		f 4 -8271 8268 8258 -8270
		mu 0 4 5124 5125 5113 5112
		f 4 -8273 8269 8260 8439
		mu 0 4 5126 5127 5115 5114
		f 4 -8275 8271 8262 -8274
		mu 0 4 5128 5129 5117 5116
		f 4 -9063 9064 9066 9067
		mu 0 4 5130 5131 5132 5133
		f 4 -8279 -8264 8266 8265
		mu 0 4 5134 5135 5118 5121
		f 4 -9071 -9073 9074 -9076
		mu 0 4 5136 5137 5138 5139
		f 4 -8283 8280 8270 -8282
		mu 0 4 4626 4625 5125 5124
		f 4 -8285 8281 8272 8441
		mu 0 4 4634 4633 5127 5126
		f 4 -8287 8283 8274 -8286
		mu 0 4 4630 4629 5129 5128
		f 4 -8289 8285 8276 8275
		mu 0 4 4622 4621 5140 5135
		f 4 -8291 -8276 8278 8277
		mu 0 4 4636 4622 5135 5134
		f 4 -8292 -8278 8279 -8281
		mu 0 4 4659 4636 5134 5141
		f 4 -8294 -8295 8292 -8164
		mu 0 4 5142 5143 5144 5145
		f 4 -8296 -8297 8293 -8176
		mu 0 4 5146 5147 5143 5142
		f 4 -8298 -8299 8295 -8188
		mu 0 4 5148 5149 5147 5146
		f 4 -8300 -8301 8297 -8200
		mu 0 4 5150 5151 5149 5148
		f 4 -8302 -8303 8299 -8212
		mu 0 4 5152 5153 5151 5150
		f 4 -8304 -8305 8301 -8224
		mu 0 4 5154 5155 5153 5152
		f 4 -8306 -8307 8303 -8236
		mu 0 4 5156 5157 5155 5154
		f 4 -8308 -8309 8305 -8248
		mu 0 4 5158 5159 5157 5156
		f 4 -8310 -8311 8307 -8260
		mu 0 4 5160 5161 5159 5158
		f 4 -8312 -8313 8309 -8272
		mu 0 4 5162 5163 5161 5160
		f 4 -8314 -8315 8311 -8284
		mu 0 4 5164 5165 5163 5162
		f 4 -8316 -8317 8313 -7140
		mu 0 4 5166 5167 5165 5164
		f 4 -8318 -8319 8315 -7132
		mu 0 4 5168 5169 5170 5171
		f 4 -8320 -9274 9276 -7124
		mu 0 4 5172 5173 5174 5175
		f 4 -8322 -8323 8319 -7116
		mu 0 4 5176 5177 5178 5179
		f 4 -8324 -8325 8321 -7108
		mu 0 4 5180 5181 5182 5183
		f 4 -8326 -8327 8323 -7078
		mu 0 4 3664 5184 5185 4501
		f 4 -8328 -8329 8325 -6020
		mu 0 4 3657 5186 5184 3664
		f 4 -8330 -8331 8327 -6008
		mu 0 4 3610 5187 5186 3657
		f 4 5320 -8333 8329 -5979
		mu 0 4 3609 5188 5187 3610
		f 4 -8334 -8335 -5321 -6184
		mu 0 4 3253 5189 5188 3609
		f 4 -8336 -8337 8333 -5336
		mu 0 4 3252 5190 5189 3253
		f 4 -8338 -8339 8335 -6144
		mu 0 4 3275 5191 5190 3252
		f 4 -8340 -8341 8337 -5356
		mu 0 4 3274 5192 5191 3275
		f 4 -8342 -8343 8339 -6164
		mu 0 4 3295 5193 5192 3274
		f 4 -8344 -8345 8341 -5374
		mu 0 4 3294 5194 5193 3295
		f 4 -8346 -8347 8343 -5392
		mu 0 4 3310 5195 5194 3294
		f 4 -8348 -8349 8345 -5408
		mu 0 4 3319 5196 5195 3310
		f 4 -8350 -8351 8347 -5428
		mu 0 4 3230 5197 5196 3319
		f 4 5300 -8353 8349 -5318
		mu 0 4 3229 5198 5197 3230
		f 4 -8354 -8355 -5301 -5550
		mu 0 4 3366 5199 5198 3229
		f 4 -8356 -8357 8353 -5528
		mu 0 4 3212 5200 5199 3366
		f 4 5280 -8359 8355 -5298
		mu 0 4 3201 5201 5200 3212
		f 4 5260 -8361 -5281 -5278
		mu 0 4 3186 5202 5201 3201
		f 4 5212 -8363 -5261 -5258
		mu 0 4 3185 5203 5202 3186
		f 4 -8364 -8365 -5213 -5454
		mu 0 4 3338 5204 5205 3339
		f 4 -8366 -8367 8363 -5474
		mu 0 4 3357 5206 5204 3338
		f 4 -8368 -8369 8365 -6993
		mu 0 4 4424 5207 5206 3357
		f 4 -8370 -8371 8367 -7019
		mu 0 4 3149 5208 5207 4424
		f 4 5202 -8373 8369 -5243
		mu 0 4 3148 5209 5208 3149
		f 4 -8374 -8375 -5203 -5494
		mu 0 4 3127 5210 5209 3148
		f 4 5192 -8377 8373 -5233
		mu 0 4 3126 5211 5210 3127
		f 4 -8378 -8379 -5193 -5514
		mu 0 4 3103 5212 5211 3126
		f 4 5182 -8381 8377 -5223
		mu 0 4 3102 5213 5212 3103
		f 4 -8383 -5183 5577 5618
		mu 0 4 5214 5215 3387 3388
		f 4 -8385 -5619 5615 5598
		mu 0 4 5216 5214 3388 3407
		f 4 -8387 -5599 5595 5638
		mu 0 4 5217 5216 3407 3408
		f 4 -8389 -5639 5635 5580
		mu 0 4 5218 5217 3408 3424
		f 4 -8391 -5581 5657 5660
		mu 0 4 5219 5218 3424 3436
		f 4 -8393 -5661 5683 5708
		mu 0 4 5220 5219 3436 3447
		f 4 -8395 -5709 5705 5732
		mu 0 4 5221 5220 3447 3457
		f 4 -8397 -5733 5729 5750
		mu 0 4 5222 5221 3457 3470
		f 4 -8399 -5751 5747 5770
		mu 0 4 5223 5222 3470 3480
		f 4 -8401 -5771 5767 5792
		mu 0 4 5224 5223 3480 3491
		f 4 -8403 -5793 5789 5812
		mu 0 4 5225 5224 3491 3503
		f 4 -8405 -5813 5809 5832
		mu 0 4 5226 5225 3503 3514
		f 4 -8407 -5833 5829 5852
		mu 0 4 5227 5226 3514 3525
		f 4 -8409 -5853 5849 5872
		mu 0 4 5228 5227 3525 3536
		f 4 -8411 -5873 5869 5892
		mu 0 4 5229 5228 3536 3547
		f 4 -8413 -5893 5889 5912
		mu 0 4 5230 5229 3547 3558
		f 4 -8415 -5913 5909 5935
		mu 0 4 5231 5230 3558 3569
		f 4 -8417 -5936 5933 5955
		mu 0 4 5232 5231 3569 3582
		f 4 -8419 -5956 5953 5687
		mu 0 4 3170 5232 3582 3167
		f 4 -8421 -8422 8419 8294
		mu 0 4 5143 4996 4999 5144
		f 4 -8423 -8424 8420 8296
		mu 0 4 5147 5014 4996 5143
		f 4 -8425 -8426 8422 8298
		mu 0 4 5149 5030 5014 5147
		f 4 -8427 -8428 8424 8300
		mu 0 4 5151 5042 5030 5149
		f 4 -8429 -8430 8426 8302
		mu 0 4 5153 5058 5042 5151
		f 4 -8431 -8432 8428 8304
		mu 0 4 5155 5070 5058 5153
		f 4 -8433 -8434 8430 8306
		mu 0 4 5157 5086 5070 5155
		f 4 -8435 -8436 8432 8308
		mu 0 4 5159 5098 5086 5157
		f 4 -8437 -8438 8434 8310
		mu 0 4 5161 5114 5098 5159
		f 4 -8439 -8440 8436 8312
		mu 0 4 5163 5126 5114 5161
		f 4 -8441 -8442 8438 8314
		mu 0 4 5165 4634 5126 5163
		f 4 -8443 -8444 8440 8316
		mu 0 4 5167 4631 4634 5165
		f 4 -8445 -8446 8442 8318
		mu 0 4 5169 4615 4618 5170
		f 4 -8447 -9272 9274 9273
		mu 0 4 5173 4599 4602 5174
		f 4 -8449 -8450 8446 8322
		mu 0 4 5177 4583 4586 5178
		f 4 -8451 -8452 8448 8324
		mu 0 4 5181 4567 4570 5182
		f 4 -8453 -8454 8450 8326
		mu 0 4 5184 3663 4506 5185
		f 4 -8455 -8456 8452 8328
		mu 0 4 5186 3656 3663 5184
		f 4 -8457 -8458 8454 8330
		mu 0 4 5187 3614 3656 5186
		f 4 8331 -8460 8456 8332
		mu 0 4 5188 3611 3614 5187
		f 4 -8461 -8462 -8332 8334
		mu 0 4 5189 3250 3611 5188
		f 4 -8463 -8464 8460 8336
		mu 0 4 5190 3249 3250 5189
		f 4 -8465 -8466 8462 8338
		mu 0 4 5191 3272 3249 5190
		f 4 -8467 -8468 8464 8340
		mu 0 4 5192 3271 3272 5191
		f 4 -8469 -8470 8466 8342
		mu 0 4 5193 3292 3271 5192
		f 4 -8471 -8472 8468 8344
		mu 0 4 5194 3291 3292 5193
		f 4 -8473 -8474 8470 8346
		mu 0 4 5195 3308 3291 5194
		f 4 -8475 -8476 8472 8348
		mu 0 4 5196 3316 3308 5195
		f 4 -8477 -8478 8474 8350
		mu 0 4 5197 3234 3316 5196
		f 4 8351 -8480 8476 8352
		mu 0 4 5198 3231 3234 5197
		f 4 -8481 -8482 -8352 8354
		mu 0 4 5199 3365 3231 5198
		f 4 -8483 -8484 8480 8356
		mu 0 4 5200 3214 3365 5199
		f 4 8357 -8486 8482 8358
		mu 0 4 5201 3203 3214 5200
		f 4 8359 -8488 -8358 8360
		mu 0 4 5202 3190 3203 5201
		f 4 8361 -8490 -8360 8362
		mu 0 4 5203 3187 3190 5202
		f 4 -8491 -8492 -8362 8364
		mu 0 4 5204 3335 3336 5205
		f 4 -8493 -8494 8490 8366
		mu 0 4 5206 3355 3335 5204
		f 4 -8495 -8496 8492 8368
		mu 0 4 5207 4422 3355 5206
		f 4 -8497 -8498 8494 8370
		mu 0 4 5208 3153 4422 5207
		f 4 8371 -8500 8496 8372
		mu 0 4 5209 3150 3153 5208
		f 4 -8501 -8502 -8372 8374
		mu 0 4 5210 3131 3150 5209
		f 4 8375 -8504 8500 8376
		mu 0 4 5211 3128 3131 5210
		f 4 -8505 -8506 -8376 8378
		mu 0 4 5212 3107 3128 5211
		f 4 8379 -8508 8504 8380
		mu 0 4 5213 3104 3107 5212
		f 4 -8510 -8380 8382 8381
		mu 0 4 3391 3390 5215 5214
		f 4 -8512 -8382 8384 8383
		mu 0 4 3410 3391 5214 5216
		f 4 -8514 -8384 8386 8385
		mu 0 4 3411 3410 5216 5217
		f 4 -8516 -8386 8388 8387
		mu 0 4 3425 3411 5217 5218
		f 4 -8518 -8388 8390 8389
		mu 0 4 3437 3425 5218 5219
		f 4 -8520 -8390 8392 8391
		mu 0 4 3448 3437 5219 5220
		f 4 -8522 -8392 8394 8393
		mu 0 4 3458 3448 5220 5221
		f 4 -8524 -8394 8396 8395
		mu 0 4 3471 3458 5221 5222
		f 4 -8526 -8396 8398 8397
		mu 0 4 3481 3471 5222 5223
		f 4 -8528 -8398 8400 8399
		mu 0 4 3492 3481 5223 5224
		f 4 -8530 -8400 8402 8401
		mu 0 4 3504 3492 5224 5225
		f 4 -8532 -8402 8404 8403
		mu 0 4 3515 3504 5225 5226
		f 4 -8534 -8404 8406 8405
		mu 0 4 3526 3515 5226 5227
		f 4 -8536 -8406 8408 8407
		mu 0 4 3537 3526 5227 5228
		f 4 -8538 -8408 8410 8409
		mu 0 4 3548 3537 5228 5229
		f 4 -8540 -8410 8412 8411
		mu 0 4 3559 3548 5229 5230
		f 4 -8542 -8412 8414 8413
		mu 0 4 3570 3559 5230 5231
		f 4 -8544 -8414 8416 8415
		mu 0 4 3583 3570 5231 5232
		f 4 -8546 -8416 8418 8417
		mu 0 4 3169 3583 5232 3170
		f 4 7163 8546 -8420 8547
		mu 0 4 5233 5234 5235 5236
		f 4 7142 -7141 -8293 -8547
		mu 0 4 5237 5238 5239 5240
		f 4 7136 -8548 -7142 7138
		mu 0 4 5241 5242 5243 5244
		f 4 -7160 8548 8550 -8550
		mu 0 4 3710 4655 5245 5246
		f 4 7162 8551 -8553 -8549
		mu 0 4 4655 4654 5247 5245
		f 4 7214 8553 -8555 -8552
		mu 0 4 4654 3711 5248 5247
		f 4 -6068 8549 8555 -8554
		mu 0 4 3711 3710 5246 5248
		f 4 -7156 8556 8558 -8558
		mu 0 4 3765 4649 5249 5250
		f 4 7158 8559 -8561 -8557
		mu 0 4 4649 4648 5251 5249
		f 4 7718 8561 -8563 -8560
		mu 0 4 4648 3766 5252 5251
		f 4 -6120 8557 8563 -8562
		mu 0 4 3766 3765 5250 5252
		f 4 -7180 8564 8566 -8566
		mu 0 4 4651 4650 5253 5254
		f 4 -6121 8567 8568 -8565
		mu 0 4 4650 3782 5255 5253
		f 4 6118 8569 -8571 -8568
		mu 0 4 3782 3783 5256 5255
		f 4 7714 8565 -8572 -8570
		mu 0 4 3783 4651 5254 5256
		f 4 -7184 8572 8574 -8574
		mu 0 4 4657 4656 5257 5258
		f 4 -6069 8575 8576 -8573
		mu 0 4 4656 3703 5259 5257
		f 4 6065 8577 -8579 -8576
		mu 0 4 3703 3704 5260 5259
		f 4 7210 8573 -8580 -8578
		mu 0 4 3704 4657 5258 5260
		f 4 -7186 8580 8582 -8582
		mu 0 4 4683 4682 5261 5262
		f 4 -7187 8583 8584 -8581
		mu 0 4 4682 4706 5263 5261
		f 4 7184 8585 -8587 -8584
		mu 0 4 4706 4707 5264 5263
		f 4 5986 8581 -8588 -8586
		mu 0 4 4707 4683 5262 5264
		f 4 -7191 8588 8590 -8590
		mu 0 4 4689 4681 5265 5266
		f 4 7187 8591 -8593 -8589
		mu 0 4 4681 4680 5267 5265
		f 4 7161 8593 -8595 -8592
		mu 0 4 4680 4690 5268 5267
		f 4 -7190 8589 8595 -8594
		mu 0 4 4690 4689 5266 5268
		f 4 -7300 8596 8598 -8598
		mu 0 4 4722 4653 5269 5270
		f 4 -7301 8599 8600 -8597
		mu 0 4 4653 3759 5271 5269
		f 4 7297 8601 -8603 -8600
		mu 0 4 3759 4721 5272 5271
		f 4 7260 8597 -8604 -8602
		mu 0 4 4721 4722 5270 5272
		f 4 -7305 8604 8606 -8606
		mu 0 4 3733 3732 5273 5274
		f 4 7301 8607 -8609 -8605
		mu 0 4 3732 4723 5275 5273
		f 4 7264 8609 -8611 -8608
		mu 0 4 4723 4724 5276 5275
		f 4 -7304 8605 8611 -8610
		mu 0 4 4724 3733 5274 5276
		f 4 -7314 8612 8614 -8614
		mu 0 4 4652 4741 5277 5278
		f 4 -7315 8615 8616 -8613
		mu 0 4 4741 4971 5279 5277
		f 4 7312 8617 -8619 -8616
		mu 0 4 4971 3758 5280 5279
		f 4 6100 8613 -8620 -8618
		mu 0 4 3758 4652 5278 5280
		f 4 -7319 8620 8622 -8622
		mu 0 4 4747 4740 5281 5282
		f 4 7315 8623 -8625 -8621
		mu 0 4 4740 3731 5283 5281
		f 4 7157 8625 -8627 -8624
		mu 0 4 3731 3730 5284 5283
		f 4 -7318 8621 8627 -8626
		mu 0 4 3730 4747 5282 5284
		f 4 -8138 8628 8630 -8630
		mu 0 4 4972 4647 5285 5286
		f 4 -8139 8631 8632 -8629
		mu 0 4 4647 3729 5287 5285
		f 4 8136 8633 -8635 -8632
		mu 0 4 3729 4973 5288 5287
		f 4 8114 8629 -8636 -8634
		mu 0 4 4973 4972 5286 5288
		f 4 -8143 8636 8638 -8638
		mu 0 4 3720 3719 5289 5290
		f 4 8139 8639 -8641 -8637
		mu 0 4 3719 4974 5291 5289
		f 4 8118 8641 -8643 -8640
		mu 0 4 4974 4975 5292 5291
		f 4 -8142 8637 8643 -8642
		mu 0 4 4975 3720 5290 5292
		f 4 -8181 8644 8646 -8646
		mu 0 4 5023 5036 5293 5294
		f 4 8177 8647 -8649 -8645
		mu 0 4 5036 5005 5295 5293
		f 4 8168 8649 -8651 -8648
		mu 0 4 5005 5004 5296 5295
		f 4 8167 8645 -8652 -8650
		mu 0 4 5004 5023 5294 5296
		f 4 -8184 8652 8654 -8654
		mu 0 4 5039 5022 5297 5298
		f 4 -8170 8655 8656 -8653
		mu 0 4 5022 5008 5299 5297
		f 4 8171 8657 -8659 -8656
		mu 0 4 5008 5010 5300 5299
		f 4 -8173 8653 8659 -8658
		mu 0 4 5010 5039 5298 5300
		f 4 -8205 8660 8662 -8662
		mu 0 4 5051 5064 5301 5302
		f 4 8201 8663 -8665 -8661
		mu 0 4 5064 5035 5303 5301
		f 4 8192 8665 -8667 -8664
		mu 0 4 5035 5034 5304 5303
		f 4 8191 8661 -8668 -8666
		mu 0 4 5034 5051 5302 5304
		f 4 -8208 8668 8670 -8670
		mu 0 4 5067 5050 5305 5306
		f 4 -8194 8671 8672 -8669
		mu 0 4 5050 5037 5307 5305
		f 4 8195 8673 -8675 -8672
		mu 0 4 5037 5038 5308 5307
		f 4 -8197 8669 8675 -8674
		mu 0 4 5038 5067 5306 5308
		f 4 -8229 8676 8678 -8678
		mu 0 4 5079 5092 5309 5310
		f 4 8225 8679 -8681 -8677
		mu 0 4 5092 5063 5311 5309
		f 4 8216 8681 -8683 -8680
		mu 0 4 5063 5062 5312 5311
		f 4 8215 8677 -8684 -8682
		mu 0 4 5062 5079 5310 5312
		f 4 -8232 8684 8686 -8686
		mu 0 4 5095 5078 5313 5314
		f 4 -8218 8687 8688 -8685
		mu 0 4 5078 5065 5315 5313
		f 4 8219 8689 -8691 -8688
		mu 0 4 5065 5066 5316 5315
		f 4 -8221 8685 8691 -8690
		mu 0 4 5066 5095 5314 5316
		f 4 -8253 8692 8694 -8694
		mu 0 4 5107 5120 5317 5318
		f 4 8249 8695 -8697 -8693
		mu 0 4 5120 5091 5319 5317
		f 4 8240 8697 -8699 -8696
		mu 0 4 5091 5090 5320 5319
		f 4 8239 8693 -8700 -8698
		mu 0 4 5090 5107 5318 5320
		f 4 -8256 8700 8702 -8702
		mu 0 4 5123 5106 5321 5322
		f 4 -8242 8703 8704 -8701
		mu 0 4 5106 5093 5323 5321
		f 4 8243 8705 -8707 -8704
		mu 0 4 5093 5094 5324 5323
		f 4 -8245 8701 8707 -8706
		mu 0 4 5094 5123 5322 5324
		f 4 -8277 8708 8710 -8710
		mu 0 4 5135 5140 5325 5326
		f 4 8273 8711 -8713 -8709
		mu 0 4 5140 5119 5327 5325
		f 4 8264 8713 -8715 -8712
		mu 0 4 5119 5118 5328 5327
		f 4 8263 8709 -8716 -8714
		mu 0 4 5118 5135 5326 5328
		f 4 -8280 8716 8718 -8718
		mu 0 4 5141 5134 5329 5330
		f 4 -8266 8719 8720 -8717
		mu 0 4 5134 5121 5331 5329
		f 4 8267 8721 -8723 -8720
		mu 0 4 5121 5122 5332 5331
		f 4 -8269 8717 8723 -8722
		mu 0 4 5122 5141 5330 5332
		f 4 -8551 8724 8726 -8726
		mu 0 4 5246 5245 5333 5334
		f 4 8552 8727 -8729 -8725
		mu 0 4 5245 5247 5335 5333
		f 4 8554 8729 -8731 -8728
		mu 0 4 5247 5248 5336 5335
		f 4 -8556 8725 8731 -8730
		mu 0 4 5248 5246 5334 5336
		f 4 -8559 8732 8734 -8734
		mu 0 4 5250 5249 5337 5338
		f 4 8560 8735 -8737 -8733
		mu 0 4 5249 5251 5339 5337
		f 4 8562 8737 -8739 -8736
		mu 0 4 5251 5252 5340 5339
		f 4 -8564 8733 8739 -8738
		mu 0 4 5252 5250 5338 5340
		f 4 -8567 8740 8742 -8742
		mu 0 4 5254 5253 5341 5342
		f 4 -8569 8743 8744 -8741
		mu 0 4 5253 5255 5343 5341
		f 4 8570 8745 -8747 -8744
		mu 0 4 5255 5256 5344 5343
		f 4 8571 8741 -8748 -8746
		mu 0 4 5256 5254 5342 5344
		f 4 -8575 8748 8750 -8750
		mu 0 4 5258 5257 5345 5346
		f 4 -8577 8751 8752 -8749
		mu 0 4 5257 5259 5347 5345
		f 4 8578 8753 -8755 -8752
		mu 0 4 5259 5260 5348 5347
		f 4 8579 8749 -8756 -8754
		mu 0 4 5260 5258 5346 5348
		f 4 -8583 8756 8758 -8758
		mu 0 4 5262 5261 5349 5350
		f 4 -8585 8759 8760 -8757
		mu 0 4 5261 5263 5351 5349
		f 4 8586 8761 -8763 -8760
		mu 0 4 5263 5264 5352 5351
		f 4 8587 8757 -8764 -8762
		mu 0 4 5264 5262 5350 5352
		f 4 -8591 8764 8766 -8766
		mu 0 4 5266 5265 5353 5354
		f 4 8592 8767 -8769 -8765
		mu 0 4 5265 5267 5355 5353
		f 4 8594 8769 -8771 -8768
		mu 0 4 5267 5268 5356 5355
		f 4 -8596 8765 8771 -8770
		mu 0 4 5268 5266 5354 5356
		f 4 -8599 8772 8774 -8774
		mu 0 4 5270 5269 5357 5358
		f 4 -8601 8775 8776 -8773
		mu 0 4 5269 5271 5359 5357
		f 4 8602 8777 -8779 -8776
		mu 0 4 5271 5272 5360 5359
		f 4 8603 8773 -8780 -8778
		mu 0 4 5272 5270 5358 5360
		f 4 -8607 8780 8782 -8782
		mu 0 4 5274 5273 5361 5362
		f 4 8608 8783 -8785 -8781
		mu 0 4 5273 5275 5363 5361
		f 4 8610 8785 -8787 -8784
		mu 0 4 5275 5276 5364 5363
		f 4 -8612 8781 8787 -8786
		mu 0 4 5276 5274 5362 5364
		f 4 -8615 8788 8790 -8790
		mu 0 4 5278 5277 5365 5366
		f 4 -8617 8791 8792 -8789
		mu 0 4 5277 5279 5367 5365
		f 4 8618 8793 -8795 -8792
		mu 0 4 5279 5280 5368 5367
		f 4 8619 8789 -8796 -8794
		mu 0 4 5280 5278 5366 5368
		f 4 -8623 8796 8798 -8798
		mu 0 4 5282 5281 5369 5370
		f 4 8624 8799 -8801 -8797
		mu 0 4 5281 5283 5371 5369
		f 4 8626 8801 -8803 -8800
		mu 0 4 5283 5284 5372 5371
		f 4 -8628 8797 8803 -8802
		mu 0 4 5284 5282 5370 5372
		f 4 -8631 8804 8806 -8806
		mu 0 4 5286 5285 5373 5374
		f 4 -8633 8807 8808 -8805
		mu 0 4 5285 5287 5375 5373
		f 4 8634 8809 -8811 -8808
		mu 0 4 5287 5288 5376 5375
		f 4 8635 8805 -8812 -8810
		mu 0 4 5288 5286 5374 5376
		f 4 -8639 8812 8814 -8814
		mu 0 4 5290 5289 5377 5378
		f 4 8640 8815 -8817 -8813
		mu 0 4 5289 5291 5379 5377
		f 4 8642 8817 -8819 -8816
		mu 0 4 5291 5292 5380 5379
		f 4 -8644 8813 8819 -8818
		mu 0 4 5292 5290 5378 5380
		f 4 -8647 8820 8822 -8822
		mu 0 4 5294 5293 5381 5382
		f 4 8648 8823 -8825 -8821
		mu 0 4 5293 5295 5383 5381
		f 4 8650 8825 -8827 -8824
		mu 0 4 5295 5296 5384 5383
		f 4 8651 8821 -8828 -8826
		mu 0 4 5296 5294 5382 5384
		f 4 -8655 8828 8830 -8830
		mu 0 4 5298 5297 5385 5386
		f 4 -8657 8831 8832 -8829
		mu 0 4 5297 5299 5387 5385
		f 4 8658 8833 -8835 -8832
		mu 0 4 5299 5300 5388 5387
		f 4 -8660 8829 8835 -8834
		mu 0 4 5300 5298 5386 5388
		f 4 -8663 8836 8838 -8838
		mu 0 4 5302 5301 5389 5390
		f 4 8664 8839 -8841 -8837
		mu 0 4 5301 5303 5391 5389
		f 4 8666 8841 -8843 -8840
		mu 0 4 5303 5304 5392 5391
		f 4 8667 8837 -8844 -8842
		mu 0 4 5304 5302 5390 5392
		f 4 -8671 8844 8846 -8846
		mu 0 4 5306 5305 5393 5394
		f 4 -8673 8847 8848 -8845
		mu 0 4 5305 5307 5395 5393
		f 4 8674 8849 -8851 -8848
		mu 0 4 5307 5308 5396 5395
		f 4 -8676 8845 8851 -8850
		mu 0 4 5308 5306 5394 5396
		f 4 -8679 8852 8854 -8854
		mu 0 4 5310 5309 5397 5398
		f 4 8680 8855 -8857 -8853
		mu 0 4 5309 5311 5399 5397
		f 4 8682 8857 -8859 -8856
		mu 0 4 5311 5312 5400 5399
		f 4 8683 8853 -8860 -8858
		mu 0 4 5312 5310 5398 5400
		f 4 -8687 8860 8862 -8862
		mu 0 4 5314 5313 5401 5402
		f 4 -8689 8863 8864 -8861
		mu 0 4 5313 5315 5403 5401
		f 4 8690 8865 -8867 -8864
		mu 0 4 5315 5316 5404 5403
		f 4 -8692 8861 8867 -8866
		mu 0 4 5316 5314 5402 5404
		f 4 -8695 8868 8870 -8870
		mu 0 4 5318 5317 5405 5406
		f 4 8696 8871 -8873 -8869
		mu 0 4 5317 5319 5407 5405
		f 4 8698 8873 -8875 -8872
		mu 0 4 5319 5320 5408 5407
		f 4 8699 8869 -8876 -8874
		mu 0 4 5320 5318 5406 5408
		f 4 -8703 8876 8878 -8878
		mu 0 4 5322 5321 5409 5410
		f 4 -8705 8879 8880 -8877
		mu 0 4 5321 5323 5411 5409
		f 4 8706 8881 -8883 -8880
		mu 0 4 5323 5324 5412 5411
		f 4 -8708 8877 8883 -8882
		mu 0 4 5324 5322 5410 5412
		f 4 -8711 8884 8886 -8886
		mu 0 4 5326 5325 5413 5414
		f 4 8712 8887 -8889 -8885
		mu 0 4 5325 5327 5415 5413
		f 4 8714 8889 -8891 -8888
		mu 0 4 5327 5328 5416 5415
		f 4 8715 8885 -8892 -8890
		mu 0 4 5328 5326 5414 5416
		f 4 -8719 8892 8894 -8894
		mu 0 4 5330 5329 5417 5418
		f 4 -8721 8895 8896 -8893
		mu 0 4 5329 5331 5419 5417
		f 4 8722 8897 -8899 -8896
		mu 0 4 5331 5332 5420 5419
		f 4 -8724 8893 8899 -8898
		mu 0 4 5332 5330 5418 5420
		f 4 -8727 8900 9096 -8902
		mu 0 4 5334 5333 5421 5422
		f 4 8728 8903 9094 -8901
		mu 0 4 5333 5335 5423 5421
		f 4 8730 8905 9099 -8904
		mu 0 4 5335 5336 5424 5423
		f 4 -8732 8901 9098 -8906
		mu 0 4 5336 5334 5422 5424
		f 4 -8735 8908 9120 -8910
		mu 0 4 5338 5337 5425 5426
		f 4 8736 8911 9118 -8909
		mu 0 4 5337 5339 5427 5425
		f 4 8738 8913 9123 -8912
		mu 0 4 5339 5340 5428 5427
		f 4 -8740 8909 9122 -8914
		mu 0 4 5340 5338 5426 5428
		f 4 -8743 8916 9162 -8918
		mu 0 4 5342 5341 5429 5430
		f 4 -8745 8919 9160 -8917
		mu 0 4 5341 5343 5431 5429
		f 4 8746 8921 9158 -8920
		mu 0 4 5343 5344 5432 5431
		f 4 8747 8917 9163 -8922
		mu 0 4 5344 5342 5430 5432
		f 4 -8751 8924 9130 -8926
		mu 0 4 5346 5345 5433 5434
		f 4 -8753 8927 9128 -8925
		mu 0 4 5345 5347 5435 5433
		f 4 8754 8929 9126 -8928
		mu 0 4 5347 5348 5436 5435
		f 4 8755 8925 9131 -8930
		mu 0 4 5348 5346 5434 5436
		f 4 -8759 8932 9082 -8934
		mu 0 4 5350 5349 5437 5438
		f 4 -8761 8935 9080 -8933
		mu 0 4 5349 5351 5439 5437
		f 4 8762 8937 9078 -8936
		mu 0 4 5351 5352 5440 5439
		f 4 8763 8933 9083 -8938
		mu 0 4 5352 5350 5438 5440
		f 4 -8767 8940 9088 -8942
		mu 0 4 5354 5353 5441 5442
		f 4 8768 8943 9086 -8941
		mu 0 4 5353 5355 5443 5441
		f 4 8770 8945 9091 -8944
		mu 0 4 5355 5356 5444 5443
		f 4 -8772 8941 9090 -8946
		mu 0 4 5356 5354 5442 5444
		f 4 -8775 8948 9146 -8950
		mu 0 4 5358 5357 5445 5446
		f 4 -8777 8951 9144 -8949
		mu 0 4 5357 5359 5447 5445
		f 4 8778 8953 9142 -8952
		mu 0 4 5359 5360 5448 5447
		f 4 8779 8949 9147 -8954
		mu 0 4 5360 5358 5446 5448;
	setAttr ".fc[4500:4703]"
		f 4 -8783 8956 9104 -8958
		mu 0 4 5362 5361 5449 5450
		f 4 8784 8959 9102 -8957
		mu 0 4 5361 5363 5451 5449
		f 4 8786 8961 9107 -8960
		mu 0 4 5363 5364 5452 5451
		f 4 -8788 8957 9106 -8962
		mu 0 4 5364 5362 5450 5452
		f 4 -8791 8964 9154 -8966
		mu 0 4 5366 5365 5453 5454
		f 4 -8793 8967 9152 -8965
		mu 0 4 5365 5367 5455 5453
		f 4 8794 8969 9150 -8968
		mu 0 4 5367 5368 5456 5455
		f 4 8795 8965 9155 -8970
		mu 0 4 5368 5366 5454 5456
		f 4 -8799 8972 9112 -8974
		mu 0 4 5370 5369 5457 5458
		f 4 8800 8975 9110 -8973
		mu 0 4 5369 5371 5459 5457
		f 4 8802 8977 9115 -8976
		mu 0 4 5371 5372 5460 5459
		f 4 -8804 8973 9114 -8978
		mu 0 4 5372 5370 5458 5460
		f 4 -8807 8980 9170 -8982
		mu 0 4 5374 5373 5461 5462
		f 4 -8809 8983 9168 -8981
		mu 0 4 5373 5375 5463 5461
		f 4 8810 8985 9166 -8984
		mu 0 4 5375 5376 5464 5463
		f 4 8811 8981 9171 -8986
		mu 0 4 5376 5374 5462 5464
		f 4 -8815 8988 9136 -8990
		mu 0 4 5378 5377 5465 5466
		f 4 8816 8991 9134 -8989
		mu 0 4 5377 5379 5467 5465
		f 4 8818 8993 9139 -8992
		mu 0 4 5379 5380 5468 5467
		f 4 -8820 8989 9138 -8994
		mu 0 4 5380 5378 5466 5468
		f 4 -8823 8996 9242 -8998
		mu 0 4 5382 5381 5469 5470
		f 4 8824 8999 9240 -8997
		mu 0 4 5381 5383 5471 5469
		f 4 8826 9001 9238 -9000
		mu 0 4 5383 5384 5472 5471
		f 4 8827 8997 9243 -9002
		mu 0 4 5384 5382 5470 5472
		f 4 -8831 9004 9250 -9006
		mu 0 4 5386 5385 5473 5474
		f 4 -8833 9007 9248 -9005
		mu 0 4 5385 5387 5475 5473
		f 4 8834 9009 9246 -9008
		mu 0 4 5387 5388 5476 5475
		f 4 -8836 9005 9251 -9010
		mu 0 4 5388 5386 5474 5476
		f 4 -8839 9012 9226 -9014
		mu 0 4 5390 5389 5477 5478
		f 4 8840 9015 9224 -9013
		mu 0 4 5389 5391 5479 5477
		f 4 8842 9017 9222 -9016
		mu 0 4 5391 5392 5480 5479
		f 4 8843 9013 9227 -9018
		mu 0 4 5392 5390 5478 5480
		f 4 -8847 9020 9234 -9022
		mu 0 4 5394 5393 5481 5482
		f 4 -8849 9023 9232 -9021
		mu 0 4 5393 5395 5483 5481
		f 4 8850 9025 9230 -9024
		mu 0 4 5395 5396 5484 5483
		f 4 -8852 9021 9235 -9026
		mu 0 4 5396 5394 5482 5484
		f 4 -8855 9028 9210 -9030
		mu 0 4 5398 5397 5485 5486
		f 4 8856 9031 9208 -9029
		mu 0 4 5397 5399 5487 5485
		f 4 8858 9033 9206 -9032
		mu 0 4 5399 5400 5488 5487
		f 4 8859 9029 9211 -9034
		mu 0 4 5400 5398 5486 5488
		f 4 -8863 9036 9218 -9038
		mu 0 4 5402 5401 5489 5490
		f 4 -8865 9039 9216 -9037
		mu 0 4 5401 5403 5491 5489
		f 4 8866 9041 9214 -9040
		mu 0 4 5403 5404 5492 5491
		f 4 -8868 9037 9219 -9042
		mu 0 4 5404 5402 5490 5492
		f 4 -8871 9044 9194 -9046
		mu 0 4 5406 5405 5493 5494
		f 4 8872 9047 9192 -9045
		mu 0 4 5405 5407 5495 5493
		f 4 8874 9049 9190 -9048
		mu 0 4 5407 5408 5496 5495
		f 4 8875 9045 9195 -9050
		mu 0 4 5408 5406 5494 5496
		f 4 -8879 9052 9202 -9054
		mu 0 4 5410 5409 5497 5498
		f 4 -8881 9055 9200 -9053
		mu 0 4 5409 5411 5499 5497
		f 4 8882 9057 9198 -9056
		mu 0 4 5411 5412 5500 5499
		f 4 -8884 9053 9203 -9058
		mu 0 4 5412 5410 5498 5500
		f 4 -8887 9060 9178 -9062
		mu 0 4 5414 5413 5501 5502
		f 4 8888 9063 9176 -9061
		mu 0 4 5413 5415 5503 5501
		f 4 8890 9065 9174 -9064
		mu 0 4 5415 5416 5504 5503
		f 4 8891 9061 9179 -9066
		mu 0 4 5416 5414 5502 5504
		f 4 -8895 9068 9186 -9070
		mu 0 4 5418 5417 5505 5506
		f 4 -8897 9071 9184 -9069
		mu 0 4 5417 5419 5507 5505
		f 4 8898 9073 9182 -9072
		mu 0 4 5419 5420 5508 5507
		f 4 -8900 9069 9187 -9074
		mu 0 4 5420 5418 5506 5508
		f 4 -9079 9076 -8939 -9078
		mu 0 4 5439 5440 4679 4678
		f 4 -9081 9077 8936 -9080
		mu 0 4 5437 5439 4678 4677
		f 4 -9083 9079 8934 -9082
		mu 0 4 5438 5437 4677 4676
		f 4 -9084 9081 -8940 -9077
		mu 0 4 5440 5438 4676 4679
		f 4 -9087 9084 -8945 -9086
		mu 0 4 5441 5443 4686 4685
		f 4 -9089 9085 8942 -9088
		mu 0 4 5442 5441 4685 4684
		f 4 -9091 9087 8947 -9090
		mu 0 4 5444 5442 4684 4687
		f 4 -9092 9089 -8947 -9085
		mu 0 4 5443 5444 4687 4686
		f 4 -9095 9092 -8905 -9094
		mu 0 4 5421 5423 3707 3706
		f 4 -9097 9093 8902 -9096
		mu 0 4 5422 5421 3706 3705
		f 4 -9099 9095 8907 -9098
		mu 0 4 5424 5422 3705 3708
		f 4 -9100 9097 -8907 -9093
		mu 0 4 5423 5424 3708 3707
		f 4 -9103 9100 -8961 -9102
		mu 0 4 5449 5451 4734 4733
		f 4 -9105 9101 8958 -9104
		mu 0 4 5450 5449 4733 4732
		f 4 -9107 9103 8963 -9106
		mu 0 4 5452 5450 4732 4735
		f 4 -9108 9105 -8963 -9101
		mu 0 4 5451 5452 4735 4734
		f 4 -9111 9108 -8977 -9110
		mu 0 4 5457 5459 4744 4743
		f 4 -9113 9109 8974 -9112
		mu 0 4 5458 5457 4743 4742
		f 4 -9115 9111 8979 -9114
		mu 0 4 5460 5458 4742 4745
		f 4 -9116 9113 -8979 -9109
		mu 0 4 5459 5460 4745 4744
		f 4 -9119 9116 -8913 -9118
		mu 0 4 5425 5427 3762 3761
		f 4 -9121 9117 8910 -9120
		mu 0 4 5426 5425 3761 3760
		f 4 -9123 9119 8915 -9122
		mu 0 4 5428 5426 3760 3763
		f 4 -9124 9121 -8915 -9117
		mu 0 4 5427 5428 3763 3762
		f 4 -9127 9124 -8931 -9126
		mu 0 4 5435 5436 4675 4674
		f 4 -9129 9125 8928 -9128
		mu 0 4 5433 5435 4674 4673
		f 4 -9131 9127 8926 -9130
		mu 0 4 5434 5433 4673 4672
		f 4 -9132 9129 -8932 -9125
		mu 0 4 5436 5434 4672 4675
		f 4 -9135 9132 -8993 -9134
		mu 0 4 5465 5467 4990 4989
		f 4 -9137 9133 8990 -9136
		mu 0 4 5466 5465 4989 4988
		f 4 -9139 9135 8995 -9138
		mu 0 4 5468 5466 4988 4991
		f 4 -9140 9137 -8995 -9133
		mu 0 4 5467 5468 4991 4990
		f 4 -9143 9140 -8955 -9142
		mu 0 4 5447 5448 4731 4730
		f 4 -9145 9141 8952 -9144
		mu 0 4 5445 5447 4730 4729
		f 4 -9147 9143 8950 -9146
		mu 0 4 5446 5445 4729 4728
		f 4 -9148 9145 -8956 -9141
		mu 0 4 5448 5446 4728 4731
		f 4 -9151 9148 -8971 -9150
		mu 0 4 5455 5456 4739 4738
		f 4 -9153 9149 8968 -9152
		mu 0 4 5453 5455 4738 4737
		f 4 -9155 9151 8966 -9154
		mu 0 4 5454 5453 4737 4736
		f 4 -9156 9153 -8972 -9149
		mu 0 4 5456 5454 4736 4739
		f 4 -9159 9156 -8923 -9158
		mu 0 4 5431 5432 4671 4670
		f 4 -9161 9157 8920 -9160
		mu 0 4 5429 5431 4670 4669
		f 4 -9163 9159 8918 -9162
		mu 0 4 5430 5429 4669 4668
		f 4 -9164 9161 -8924 -9157
		mu 0 4 5432 5430 4668 4671
		f 4 -9167 9164 -8987 -9166
		mu 0 4 5463 5464 4987 4986
		f 4 -9169 9165 8984 -9168
		mu 0 4 5461 5463 4986 4985
		f 4 -9171 9167 8982 -9170
		mu 0 4 5462 5461 4985 4984
		f 4 -9172 9169 -8988 -9165
		mu 0 4 5464 5462 4984 4987
		f 4 -9175 9172 -9067 -9174
		mu 0 4 5503 5504 5133 5132
		f 4 -9177 9173 -9065 -9176
		mu 0 4 5501 5503 5132 5131
		f 4 -9179 9175 9062 -9178
		mu 0 4 5502 5501 5131 5130
		f 4 -9180 9177 -9068 -9173
		mu 0 4 5504 5502 5130 5133
		f 4 -9183 9180 -9075 -9182
		mu 0 4 5507 5508 5139 5138
		f 4 -9185 9181 9072 -9184
		mu 0 4 5505 5507 5138 5137
		f 4 -9187 9183 9070 -9186
		mu 0 4 5506 5505 5137 5136
		f 4 -9188 9185 9075 -9181
		mu 0 4 5508 5506 5136 5139
		f 4 -9191 9188 -9051 -9190
		mu 0 4 5495 5496 5105 5104
		f 4 -9193 9189 -9049 -9192
		mu 0 4 5493 5495 5104 5103
		f 4 -9195 9191 9046 -9194
		mu 0 4 5494 5493 5103 5102
		f 4 -9196 9193 -9052 -9189
		mu 0 4 5496 5494 5102 5105
		f 4 -9199 9196 -9059 -9198
		mu 0 4 5499 5500 5111 5110
		f 4 -9201 9197 9056 -9200
		mu 0 4 5497 5499 5110 5109
		f 4 -9203 9199 9054 -9202
		mu 0 4 5498 5497 5109 5108
		f 4 -9204 9201 9059 -9197
		mu 0 4 5500 5498 5108 5111
		f 4 -9207 9204 -9035 -9206
		mu 0 4 5487 5488 5077 5076
		f 4 -9209 9205 -9033 -9208
		mu 0 4 5485 5487 5076 5075
		f 4 -9211 9207 9030 -9210
		mu 0 4 5486 5485 5075 5074
		f 4 -9212 9209 -9036 -9205
		mu 0 4 5488 5486 5074 5077
		f 4 -9215 9212 -9043 -9214
		mu 0 4 5491 5492 5083 5082
		f 4 -9217 9213 9040 -9216
		mu 0 4 5489 5491 5082 5081
		f 4 -9219 9215 9038 -9218
		mu 0 4 5490 5489 5081 5080
		f 4 -9220 9217 9043 -9213
		mu 0 4 5492 5490 5080 5083
		f 4 -9223 9220 -9019 -9222
		mu 0 4 5479 5480 5049 5048
		f 4 -9225 9221 -9017 -9224
		mu 0 4 5477 5479 5048 5047
		f 4 -9227 9223 9014 -9226
		mu 0 4 5478 5477 5047 5046
		f 4 -9228 9225 -9020 -9221
		mu 0 4 5480 5478 5046 5049
		f 4 -9231 9228 -9027 -9230
		mu 0 4 5483 5484 5055 5054
		f 4 -9233 9229 9024 -9232
		mu 0 4 5481 5483 5054 5053
		f 4 -9235 9231 9022 -9234
		mu 0 4 5482 5481 5053 5052
		f 4 -9236 9233 9027 -9229
		mu 0 4 5484 5482 5052 5055
		f 4 -9239 9236 -9003 -9238
		mu 0 4 5471 5472 5021 5020
		f 4 -9241 9237 -9001 -9240
		mu 0 4 5469 5471 5020 5019
		f 4 -9243 9239 8998 -9242
		mu 0 4 5470 5469 5019 5018
		f 4 -9244 9241 -9004 -9237
		mu 0 4 5472 5470 5018 5021
		f 4 -9247 9244 -9011 -9246
		mu 0 4 5475 5476 5027 5026
		f 4 -9249 9245 9008 -9248
		mu 0 4 5473 5475 5026 5025
		f 4 -9251 9247 9006 -9250
		mu 0 4 5474 5473 5025 5024
		f 4 -9252 9249 9011 -9245
		mu 0 4 5476 5474 5024 5027
		f 4 -9255 9252 -7123 -9254
		mu 0 4 5509 5510 5511 5512
		f 4 -9257 9253 7125 8447
		mu 0 4 5513 5514 5515 5516
		f 4 -9259 -8448 8444 8320
		mu 0 4 5517 5513 5516 5518
		f 4 -9261 -8321 8317 -9260
		mu 0 4 5519 5517 5518 5520
		f 4 -9263 9259 7124 -9262
		mu 0 4 5521 5522 5523 5524
		f 4 -9265 9261 -7146 7148
		mu 0 4 5525 5526 5527 5528
		f 4 -9267 -7149 -7167 7169
		mu 0 4 5529 5525 5528 5530
		f 4 -9268 -7170 -7121 -9253
		mu 0 4 5531 5529 5530 5532
		f 4 -9271 9268 9254 -9270
		mu 0 4 4594 4593 5510 5509
		f 4 -9273 9269 9256 9255
		mu 0 4 4602 4601 5514 5513
		f 4 -9275 -9256 9258 9257
		mu 0 4 5174 4602 5513 5517
		f 4 -9277 -9258 9260 -9276
		mu 0 4 5175 5174 5517 5519
		f 4 -9279 9275 9262 -9278
		mu 0 4 4598 4597 5522 5521
		f 4 -9281 9277 9264 9263
		mu 0 4 4590 4589 5526 5525
		f 4 -9283 -9264 9266 9265
		mu 0 4 4640 4590 5525 5529
		f 4 -9284 -9266 9267 -9269
		mu 0 4 4663 4640 5529 5531
		f 4 5982 9285 -9287 -9285
		mu 0 4 5533 5534 5535 5536
		f 4 -5993 9284 9288 -9288
		mu 0 4 5537 5533 5536 5538
		f 4 5983 9289 -9291 -9286
		mu 0 4 5539 5540 5541 5542
		f 4 5984 9291 -9293 -9290
		mu 0 4 5543 5544 5545 5546
		f 4 5993 9287 -9294 -9292
		mu 0 4 5544 5547 5548 5545
		f 4 5988 9295 -9297 -9295
		mu 0 4 5549 5550 5551 5552
		f 4 -5997 9294 9298 -9298
		mu 0 4 5553 5549 5552 5554
		f 4 5989 9299 -9301 -9296
		mu 0 4 5555 5556 5557 5558
		f 4 5990 9301 -9303 -9300
		mu 0 4 5559 5560 5561 5562
		f 4 5997 9297 -9304 -9302
		mu 0 4 5560 5563 5564 5561
		f 4 9286 9305 -9307 -9305
		mu 0 4 5536 5535 5565 5566
		f 4 -9289 9304 9308 -9308
		mu 0 4 5538 5536 5566 5567
		f 4 9290 9309 -9311 -9306
		mu 0 4 5542 5541 5568 5569
		f 4 9292 9311 -9313 -9310
		mu 0 4 5546 5545 5570 5571
		f 4 9293 9307 -9314 -9312
		mu 0 4 5545 5548 5572 5570
		f 4 9296 9315 -9317 -9315
		mu 0 4 5552 5551 5573 5574
		f 4 -9299 9314 9318 -9318
		mu 0 4 5554 5552 5574 5575
		f 4 9300 9319 -9321 -9316
		mu 0 4 5558 5557 5576 5577
		f 4 9302 9321 -9323 -9320
		mu 0 4 5562 5561 5578 5579
		f 4 9303 9317 -9324 -9322
		mu 0 4 5561 5564 5580 5578
		f 4 9306 9325 -9327 -9325
		mu 0 4 5566 5565 5581 5582
		f 4 -9309 9324 9328 -9328
		mu 0 4 5567 5566 5582 5583
		f 4 9310 9329 -9331 -9326
		mu 0 4 5569 5568 5584 5585
		f 4 9312 9331 -9333 -9330
		mu 0 4 5571 5570 5586 5587
		f 4 9313 9327 -9334 -9332
		mu 0 4 5570 5572 5588 5586
		f 4 9316 9335 -9337 -9335
		mu 0 4 5574 5573 5589 5590
		f 4 -9319 9334 9338 -9338
		mu 0 4 5575 5574 5590 5591
		f 4 9320 9339 -9341 -9336
		mu 0 4 5577 5576 5592 5593
		f 4 9322 9341 -9343 -9340
		mu 0 4 5579 5578 5594 5595
		f 4 9323 9337 -9344 -9342
		mu 0 4 5578 5580 5596 5594
		f 4 9326 9345 -9347 -9345
		mu 0 4 5582 5581 3620 3619
		f 4 -9329 9344 9349 -9348
		mu 0 4 5583 5582 3619 3621
		f 4 9330 9350 -9352 -9346
		mu 0 4 5585 5584 3623 3622
		f 4 9332 9353 -9355 -9351
		mu 0 4 5587 5586 3626 3625
		f 4 9333 9347 -9356 -9354
		mu 0 4 5586 5588 3627 3626
		f 4 9336 9357 -9359 -9357
		mu 0 4 5590 5589 3640 3639
		f 4 -9339 9356 9361 -9360
		mu 0 4 5591 5590 3639 3641
		f 4 9340 9362 -9364 -9358
		mu 0 4 5593 5592 3643 3642
		f 4 9342 9365 -9367 -9363
		mu 0 4 5595 5594 3646 3645
		f 4 9343 9359 -9368 -9366
		mu 0 4 5594 5596 3647 3646;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dsm" 2;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyBridgeEdge -n "polyBridgeEdge3";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n"
		+ "                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n"
		+ "                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n"
		+ "                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n"
		+ "\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n"
		+ "                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n"
		+ "                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n"
		+ "                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 20 -size 50 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode polyBridgeEdge -n "polyBridgeEdge5";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode polyBridgeEdge -n "polyBridgeEdge6";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode polyBridgeEdge -n "polyBridgeEdge7";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode polyBridgeEdge -n "polyBridgeEdge8";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode polyBridgeEdge -n "polyBridgeEdge10";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".ctp" 2;
createNode dagPose -n "bindPose1";
	setAttr -s 76 ".wm";
	setAttr -s 76 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 1.1529652440010061 -0.9886062077219514 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.13258817776724011 -0.69456488186227983 0.13258817776724002 0.69456488186227983 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.24538830067369513 5.5511151231257827e-17
		 2.7243574138157718e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.13389062378738772 0.99099611546253008 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.23814629946066615 -2.3245294578089215e-16
		 1.4967200499594025e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.075373258383361855 0.99715539005747489 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.30588477541161446 -2.688821387764051e-16
		 1.0268731790536412e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.15362938896845563 0.98812853963701452 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.24538830067369519 -3.7470027081099033e-16
		 2.2549296748198123e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.093856511178324373 0.70085016609075046 -0.093856511178324262 0.70085016609075124 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.52377600000000046 -0.25724667002199708
		 0.28904665159600329 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.60038525601255632 -0.37354724515479631 0.60038525601255577 0.37354724515479681 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.43511264373809511 4.9540737723230421e-07
		 -0.26523900000000045 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.8742473774705829 0.48548071329951742 -2.9727120079501395e-17 5.3532212624115862e-17 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.50761985884711636 -1.8362925932602361e-07
		 2.2204460492503131e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.93135391303305237 0.36411521346686609 -2.2295626534652638e-17 5.7028979423464494e-17 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.55758082856725666 5.015260550744749e-07
		 -3.3306690738754696e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.8969766015830456 0.44207802050376832 -1.2523051104796601e-16 2.5409279132744202e-16 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.49064359058770346 1.2665694293367835e-07
		 -1.1102230246251563e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.83745009993519404 0.54651379682358758 -2.7616519880203833e-16 4.2318158238563964e-16 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.2883029352961769 -1.363597230685798e-07
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.064373973957750075 0.99792584467829404 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.41208519983042458
		 8.4799830418758049e-07 -2.2204460492503131e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 -0.15187263403413179 0.98840007235508365 1 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.52377584149921264
		 -0.2572482942473433 0.28904660846252356 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.37354724515479643 -0.60038525601255532 -0.37354724515479681 0.60038525601255643 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.43511071753373309 -1.4546006916321921e-16
		 0.26523951716053296 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.8742473774705829 -0.48548071329951725 -2.9727120079501389e-17 5.3532212624115862e-17 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.50761988408658043 2.7755575615628914e-17
		 3.0224347071822627e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.93135391303305237 -0.36411521346686609 -1.0314544525609636e-16 2.6383109108827059e-16 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.55758138040937488 0
		 1.8432305941154501e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.89697660158304571 -0.44207802050376827 -1.2523051104796601e-16 2.5409279132744202e-16 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.4906438205440069 2.4980018054066022e-16
		 2.9518080394967184e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.83745009993519404 -0.54651379682358736 -2.7616519880203823e-16 4.2318158238563964e-16 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.2883024504093622 4.163336342344337e-17
		 3.1162159025049709e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.064373973957750075 0.99792584467829404 1
		 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.41208512608818071 1.1102230246251563e-16
		 5.5520466532492054e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.15187263403413179 0.98840007235508365 1
		 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.2226763195609237e-16
		 -0.52026198141731472 0.72942580012829483 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.32787195864987512 -0.62649818733264828 0.32787195864987545 0.62649818733264917 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.44541451074890959 1.1102230246251563e-16
		 9.890188906711774e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.91511987804353179 0.40318185575443688 2.4687768456198054e-17 5.6034931474106368e-17 1
		 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.34168132455793188 1.3877787807814457e-17
		 -2.3210376645145092e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.045852966482156567 0.9989481995903422 1
		 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.24425406655418497 -3.1918911957973251e-16
		 -1.391974141508299e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.9332114643369912 0.35932765386205462 2.2002473057364664e-17 5.7142721636395528e-17 1
		 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.30959416214628238 1.5265566588595902e-16
		 3.8428311334273713e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.026855556914237709 0.99963932448800064 1
		 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.45380956936195688 -1.0269562977782698e-15
		 5.4538421436736283e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.19306627529066997 0.98118571807043109 1
		 1 1 yes;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.46542591259097249 -3.8857805861880479e-16
		 3.8236764484063499e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.21577306484217937 0.97644353881246659 1
		 1 1 yes;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.59774383133660369 -5.5511151231257827e-17
		 1.5183413529484198e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.20289942775337336 0.97919958242298788 1
		 1 1 yes;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.41266186039400354 -1.1796119636642288e-16
		 -1.3386082181459268e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.15483277775498561 0.98794069201176005 1
		 1 1 yes;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.69219996153522045 -1.2490009027033011e-16
		 8.0240173731590486e-18 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.31869536287433942 0.9478571968827334 1
		 1 1 yes;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.28781450355232341 -0.74326884051930053
		 -1.1142417147510092e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.48199277518156208 0.87617519062843596 1
		 1 1 yes;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.46653122221885679 -0.0024785635548406296
		 2.6868761336959783e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.62995011670381096 0.77663559695963924 1
		 1 1 yes;
	setAttr ".xm[31]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.37851099999999999
		 0.23360332997800293 0.070279251596004205 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.95757286958677978 -0.28819125495292186 -8.1637938244888241e-17 2.7125831699881483e-16 1
		 1 1 yes;
	setAttr ".xm[32]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.37974876386915701
		 4.8833652042112874e-06 0.088901600000000011 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 -0.28819125495292186 0.95757286958677978 1 1 1 yes;
	setAttr ".xm[33]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.11265499999999984
		 -0.27624000000000049 -0.12363499999999999 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0.27943683030606981 0.96016407861807485 1 1 1 yes;
	setAttr ".xm[34]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.4234749720598772 1.769585487254588e-06
		 -1.118125 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.046621022900942215 0.99891264894567733 1
		 1 1 yes;
	setAttr ".xm[35]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.512750183237014 4.047272146201486e-06
		 -1.0676800000000002 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.072748211512944874 0.9973503385078224 1
		 1 1 yes;
	setAttr ".xm[36]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.2427448179173457 -6.2913759331095775e-06
		 -0.75026999999999955 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.0093771669464004764 0.99995603340349892 1
		 1 1 yes;
	setAttr ".xm[37]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.218140346101289 1.0011754186578514e-05
		 -0.66760000000000064 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.10759485228299916 0.99419482384601021 1
		 1 1 yes;
	setAttr ".xm[38]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.37851062424888671 0.23359942401301792
		 0.070279211438350447 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.28819125495292142 0.95757286958678001 1
		 1 1 yes;
	setAttr ".xm[39]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.37975203443956446 -1.851611311248381e-16
		 -0.088901559842347044 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.28819125495292164 0.95757286958678001 1
		 1 1 yes;
	setAttr ".xm[40]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.11265490816986444 0.27623314628330564
		 0.12363478153895333 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.27943683030606953 0.96016407861807485 1
		 1 1 yes;
	setAttr ".xm[41]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.4234792680488011 1.1102230246251563e-16
		 1.1181257735475969 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.046621022900942818 0.99891264894567733 1
		 1 1 yes;
	setAttr ".xm[42]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.5127453880645758 2.55351295663786e-15
		 1.0676837260835546 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.072748211512945637 0.99735033850782229 1
		 1 1 yes;
	setAttr ".xm[43]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.2427509267179035 -1.1102230246251563e-15
		 0.75026423995060654 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.0093771669464004764 0.99995603340349892 1
		 1 1 yes;
	setAttr ".xm[44]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.2181366538591647 4.4408920985006262e-16
		 0.66760359396195135 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.10759485228299918 0.99419482384601021 1
		 1 1 yes;
	setAttr ".xm[45]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.45801838103092513
		 -0.10410188052769632 -0.78745900000000013 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0.70214527613447641 0.71203371493352741 1 1 1 yes;
	setAttr ".xm[46]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.75837495635502972
		 0.0080467142055987706 -2.2204460492503131e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.83364522820305076 0.55230031096703491 -3.3818640399693355e-17 5.1046048017166549e-17 1
		 1 1 yes;
	setAttr ".xm[47]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.94160417935156515
		 0.096758632750082341 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.90272188313455404 0.43022459449687966 -2.6343658628253584e-17 5.5275773235050132e-17 1
		 1 1 yes;
	setAttr ".xm[48]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.40642897545192558
		 -5.3942862832112581e-06 2.2204460492503131e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0.25006609277229613 0.9682287690652438 1 1 1 yes;
	setAttr ".xm[49]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.45045871654979897
		 -5.5470019533210291e-07 -4.4408920985006262e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0.073230317654889798 0.99731505582547164 1 1 1 yes;
	setAttr ".xm[50]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.45802177066836885
		 -0.10410030995911156 0.7874589001175466 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.71203371493352763 0.70214527613447619 1
		 1 1 yes;
	setAttr ".xm[51]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.75837837553336396 -0.0080392663526717735
		 3.3589511569740233e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.83364522820305109 -0.55230031096703447 -3.3818640399693337e-17 5.1046048017166568e-17 1
		 1 1 yes;
	setAttr ".xm[52]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.94160514283718399 -0.0967567261282965
		 -1.2239232402705165e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.90272188313455404 -0.43022459449687939 -1.2187270874234494e-16 2.5572029713284248e-16 1
		 1 1 yes;
	setAttr ".xm[53]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.40642846863574894 1.3877787807814457e-16
		 1.9958175884823629e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.25006609277229602 0.9682287690652438 1
		 1 1 yes;
	setAttr ".xm[54]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.45046253740780662 -3.6082248300317583e-16
		 3.8405944553516366e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.073230317654890145 0.99731505582547164 1
		 1 1 yes;
	setAttr ".xm[55]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.99360810061403837
		 -0.2268748621903236 -1.4809491343243099e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.12458623031928276 0.99220878408469604 3.5292406699261321e-17 2.810698730409925e-16 1
		 1 1 yes;
	setAttr ".xm[56]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.38119042319612584 -1.9428902930940239e-16
		 -4.2320638459894564e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.079385769149469126 0.99684396956421795 1
		 1 1 yes;
	setAttr ".xm[57]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.44613674074625614 1.1102230246251563e-16
		 -7.6344426782389483e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.10648451975726994 0.994314360276499 1
		 1 1 yes;
	setAttr ".xm[58]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.44377555791987405 4.0245584642661925e-16
		 -1.0862258348131464e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.069942197860357228 0.99755104579087217 1
		 1 1 yes;
	setAttr ".xm[59]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.30246759947492302 1.0408340855860845e-16
		 -8.688284922214761e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.00018759930467611148 0.99999998240325028 1
		 1 1 yes;
	setAttr ".xm[60]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.28558103032272147 1.6618650899857812e-14
		 -8.200177136905706e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.018722486654438882 0.99982471888500257 1
		 1 1 yes;
	setAttr ".xm[61]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.32326261801635897 2.7061686225238191e-16
		 -9.6198229817496722e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.021725513380746048 0.99976397317984156 1
		 1 1 yes;
	setAttr ".xm[62]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.28192559994910044 1.8735013540549517e-16
		 -8.7166405739069115e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.040467489498082355 0.99918085564812675 1
		 1 1 yes;
	setAttr ".xm[63]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.23507160834271887 -1.2490009027033011e-16
		 -6.7494781107231327e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.041962899056475184 0.99911916962030911 1
		 1 1 yes;
	setAttr ".xm[64]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.24714342126531019 1.6566609195578508e-16
		 -7.6604552442459446e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.020248030233765181 0.99979498762078844 1
		 1 1 yes;
	setAttr ".xm[65]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.20843445062795024 -1.5287250632045613e-16
		 -6.6741710029423564e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.0083887265972863202 0.99996481401401116 1
		 1 1 yes;
	setAttr ".xm[66]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.25667941856880172 -2.3399251286582157e-15
		 -8.1116692840212736e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.0074998359400927448 0.99997187583495151 1
		 1 1 yes;
	setAttr ".xm[67]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.15229412739847523
		 -9.679968981379794e-07 -1.1102230246251563e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.43273486143835121 0.55923209823465125 0.43273486143834994 0.55923209823465292 1
		 1 1 yes;
	setAttr ".xm[68]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.15229713988894664 -4.163336342344337e-17
		 1.0488679957907088e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.43273486143835088 0.5592320982346517 0.43273486143835038 0.55923209823465225 1
		 1 1 yes;
	setAttr ".xm[69]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.4855936764422708 7.7715611723760958e-16
		 -1.1382134089193288e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.16559571061770745 0.68744313264808765 0.16559571061770736 0.68744313264808798 1
		 1 1 yes;
	setAttr ".xm[70]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.1754589667152957 3.0531133177191805e-16
		 4.6386043210879119e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.040863312312889775 0.70592505955435447 0.040863312312889741 0.70592505955435503 1
		 1 1 yes;
	setAttr ".xm[71]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.60239260989130461
		 -6.9529070803042714e-06 -0.1984799999999991 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 -0.13939663663659349 0.9902366271222276 1 1 1 yes;
	setAttr ".xm[72]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.60239795159369236 -8.3266726846886741e-17
		 0.19847674415084929 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.13939663663659332 0.9902366271222276 1
		 1 1 yes;
	setAttr ".xm[73]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.2174614494487066 1.9151035934861227e-06
		 -1.1102230246251563e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.28710352750847196 0.64619777505976606 0.2871035275084719 0.64619777505976617 1
		 1 1 yes;
	setAttr ".xm[74]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.21745704222775336 9.7144514654701197e-17
		 2.0013268999840669e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.28710352750847168 0.64619777505976606 0.28710352750847151 0.64619777505976639 1
		 1 1 yes;
	setAttr ".xm[75]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.77389787893785933 5.4825935458246988e-15
		 -2.4746619656529099e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.002644982321025691 -0.7071018342986537 -0.0026449823210256867 0.70710183429865481 1
		 1 1 yes;
	setAttr -s 76 ".m";
	setAttr -s 76 ".p";
	setAttr ".bp" yes;
createNode groupId -n "groupId1";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
	setAttr ".outf" 8;
	setAttr ".imfkey" -type "string" "jpg";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "dragon_joints.s" "joint30.is";
connectAttr "joint30.s" "joint31.is";
connectAttr "joint31.s" "joint32.is";
connectAttr "joint32.s" "joint33.is";
connectAttr "joint33.s" "joint21.is";
connectAttr "joint21.s" "joint22.is";
connectAttr "joint22.s" "joint23.is";
connectAttr "joint23.s" "joint24.is";
connectAttr "joint24.s" "joint25.is";
connectAttr "joint25.s" "joint26.is";
connectAttr "joint26.s" "joint27.is";
connectAttr "joint27.s" "joint28.is";
connectAttr "joint33.s" "joint13.is";
connectAttr "joint13.s" "joint14.is";
connectAttr "joint14.s" "joint15.is";
connectAttr "joint15.s" "joint16.is";
connectAttr "joint16.s" "joint17.is";
connectAttr "joint17.s" "joint18.is";
connectAttr "joint18.s" "joint19.is";
connectAttr "joint19.s" "joint20.is";
connectAttr "joint33.s" "joint34.is";
connectAttr "joint34.s" "joint35.is";
connectAttr "joint35.s" "joint36.is";
connectAttr "joint36.s" "joint37.is";
connectAttr "joint37.s" "joint38.is";
connectAttr "joint38.s" "joint39.is";
connectAttr "joint39.s" "joint40.is";
connectAttr "joint40.s" "joint41.is";
connectAttr "joint41.s" "joint42.is";
connectAttr "joint42.s" "joint43.is";
connectAttr "joint43.s" "joint44.is";
connectAttr "joint43.s" "joint45.is";
connectAttr "joint45.s" "joint46.is";
connectAttr "joint46.s" "joint47.is";
connectAttr "joint33.s" "joint69.is";
connectAttr "joint69.s" "joint70.is";
connectAttr "joint70.s" "joint71.is";
connectAttr "joint71.s" "joint72.is";
connectAttr "joint72.s" "joint73.is";
connectAttr "joint73.s" "joint74.is";
connectAttr "joint74.s" "joint75.is";
connectAttr "joint75.s" "joint76.is";
connectAttr "joint33.s" "joint67.is";
connectAttr "joint67.s" "joint68.is";
connectAttr "joint68.s" "joint61.is";
connectAttr "joint61.s" "joint62.is";
connectAttr "joint62.s" "joint63.is";
connectAttr "joint63.s" "joint64.is";
connectAttr "joint64.s" "joint65.is";
connectAttr "joint65.s" "joint66.is";
connectAttr "dragon_joints.s" "joint7.is";
connectAttr "joint7.s" "joint8.is";
connectAttr "joint8.s" "joint9.is";
connectAttr "joint9.s" "joint10.is";
connectAttr "joint10.s" "joint11.is";
connectAttr "joint11.s" "joint12.is";
connectAttr "dragon_joints.s" "joint1.is";
connectAttr "joint1.s" "joint2.is";
connectAttr "joint2.s" "joint3.is";
connectAttr "joint3.s" "joint4.is";
connectAttr "joint4.s" "joint5.is";
connectAttr "joint5.s" "joint6.is";
connectAttr "dragon_joints.s" "joint48.is";
connectAttr "joint48.s" "joint49.is";
connectAttr "joint49.s" "joint50.is";
connectAttr "joint50.s" "joint51.is";
connectAttr "joint51.s" "joint52.is";
connectAttr "joint52.s" "joint53.is";
connectAttr "joint53.s" "joint54.is";
connectAttr "joint54.s" "joint55.is";
connectAttr "joint55.s" "joint56.is";
connectAttr "joint56.s" "joint57.is";
connectAttr "joint57.s" "joint58.is";
connectAttr "joint58.s" "joint59.is";
connectAttr "joint59.s" "joint60.is";
connectAttr "groupId1.id" "dragon_geometryShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "dragon_geometryShape.iog.og[0].gco";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "dragon_joints.msg" "bindPose1.m[0]";
connectAttr "joint30.msg" "bindPose1.m[1]";
connectAttr "joint31.msg" "bindPose1.m[2]";
connectAttr "joint32.msg" "bindPose1.m[3]";
connectAttr "joint33.msg" "bindPose1.m[4]";
connectAttr "joint21.msg" "bindPose1.m[5]";
connectAttr "joint22.msg" "bindPose1.m[6]";
connectAttr "joint23.msg" "bindPose1.m[7]";
connectAttr "joint24.msg" "bindPose1.m[8]";
connectAttr "joint25.msg" "bindPose1.m[9]";
connectAttr "joint26.msg" "bindPose1.m[10]";
connectAttr "joint27.msg" "bindPose1.m[11]";
connectAttr "joint13.msg" "bindPose1.m[12]";
connectAttr "joint14.msg" "bindPose1.m[13]";
connectAttr "joint15.msg" "bindPose1.m[14]";
connectAttr "joint16.msg" "bindPose1.m[15]";
connectAttr "joint17.msg" "bindPose1.m[16]";
connectAttr "joint18.msg" "bindPose1.m[17]";
connectAttr "joint19.msg" "bindPose1.m[18]";
connectAttr "joint34.msg" "bindPose1.m[19]";
connectAttr "joint35.msg" "bindPose1.m[20]";
connectAttr "joint36.msg" "bindPose1.m[21]";
connectAttr "joint37.msg" "bindPose1.m[22]";
connectAttr "joint38.msg" "bindPose1.m[23]";
connectAttr "joint39.msg" "bindPose1.m[24]";
connectAttr "joint40.msg" "bindPose1.m[25]";
connectAttr "joint41.msg" "bindPose1.m[26]";
connectAttr "joint42.msg" "bindPose1.m[27]";
connectAttr "joint43.msg" "bindPose1.m[28]";
connectAttr "joint45.msg" "bindPose1.m[29]";
connectAttr "joint46.msg" "bindPose1.m[30]";
connectAttr "joint69.msg" "bindPose1.m[31]";
connectAttr "joint70.msg" "bindPose1.m[32]";
connectAttr "joint71.msg" "bindPose1.m[33]";
connectAttr "joint72.msg" "bindPose1.m[34]";
connectAttr "joint73.msg" "bindPose1.m[35]";
connectAttr "joint74.msg" "bindPose1.m[36]";
connectAttr "joint75.msg" "bindPose1.m[37]";
connectAttr "joint67.msg" "bindPose1.m[38]";
connectAttr "joint68.msg" "bindPose1.m[39]";
connectAttr "joint61.msg" "bindPose1.m[40]";
connectAttr "joint62.msg" "bindPose1.m[41]";
connectAttr "joint63.msg" "bindPose1.m[42]";
connectAttr "joint64.msg" "bindPose1.m[43]";
connectAttr "joint65.msg" "bindPose1.m[44]";
connectAttr "joint7.msg" "bindPose1.m[45]";
connectAttr "joint8.msg" "bindPose1.m[46]";
connectAttr "joint9.msg" "bindPose1.m[47]";
connectAttr "joint10.msg" "bindPose1.m[48]";
connectAttr "joint11.msg" "bindPose1.m[49]";
connectAttr "joint1.msg" "bindPose1.m[50]";
connectAttr "joint2.msg" "bindPose1.m[51]";
connectAttr "joint3.msg" "bindPose1.m[52]";
connectAttr "joint4.msg" "bindPose1.m[53]";
connectAttr "joint5.msg" "bindPose1.m[54]";
connectAttr "joint48.msg" "bindPose1.m[55]";
connectAttr "joint49.msg" "bindPose1.m[56]";
connectAttr "joint50.msg" "bindPose1.m[57]";
connectAttr "joint51.msg" "bindPose1.m[58]";
connectAttr "joint52.msg" "bindPose1.m[59]";
connectAttr "joint53.msg" "bindPose1.m[60]";
connectAttr "joint54.msg" "bindPose1.m[61]";
connectAttr "joint55.msg" "bindPose1.m[62]";
connectAttr "joint56.msg" "bindPose1.m[63]";
connectAttr "joint57.msg" "bindPose1.m[64]";
connectAttr "joint58.msg" "bindPose1.m[65]";
connectAttr "joint59.msg" "bindPose1.m[66]";
connectAttr "joint28.msg" "bindPose1.m[67]";
connectAttr "joint20.msg" "bindPose1.m[68]";
connectAttr "joint44.msg" "bindPose1.m[69]";
connectAttr "joint47.msg" "bindPose1.m[70]";
connectAttr "joint76.msg" "bindPose1.m[71]";
connectAttr "joint66.msg" "bindPose1.m[72]";
connectAttr "joint12.msg" "bindPose1.m[73]";
connectAttr "joint6.msg" "bindPose1.m[74]";
connectAttr "joint60.msg" "bindPose1.m[75]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[6]" "bindPose1.p[7]";
connectAttr "bindPose1.m[7]" "bindPose1.p[8]";
connectAttr "bindPose1.m[8]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[4]" "bindPose1.p[12]";
connectAttr "bindPose1.m[12]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[14]" "bindPose1.p[15]";
connectAttr "bindPose1.m[15]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[17]" "bindPose1.p[18]";
connectAttr "bindPose1.m[4]" "bindPose1.p[19]";
connectAttr "bindPose1.m[19]" "bindPose1.p[20]";
connectAttr "bindPose1.m[20]" "bindPose1.p[21]";
connectAttr "bindPose1.m[21]" "bindPose1.p[22]";
connectAttr "bindPose1.m[22]" "bindPose1.p[23]";
connectAttr "bindPose1.m[23]" "bindPose1.p[24]";
connectAttr "bindPose1.m[24]" "bindPose1.p[25]";
connectAttr "bindPose1.m[25]" "bindPose1.p[26]";
connectAttr "bindPose1.m[26]" "bindPose1.p[27]";
connectAttr "bindPose1.m[27]" "bindPose1.p[28]";
connectAttr "bindPose1.m[28]" "bindPose1.p[29]";
connectAttr "bindPose1.m[29]" "bindPose1.p[30]";
connectAttr "bindPose1.m[4]" "bindPose1.p[31]";
connectAttr "bindPose1.m[31]" "bindPose1.p[32]";
connectAttr "bindPose1.m[32]" "bindPose1.p[33]";
connectAttr "bindPose1.m[33]" "bindPose1.p[34]";
connectAttr "bindPose1.m[34]" "bindPose1.p[35]";
connectAttr "bindPose1.m[35]" "bindPose1.p[36]";
connectAttr "bindPose1.m[36]" "bindPose1.p[37]";
connectAttr "bindPose1.m[4]" "bindPose1.p[38]";
connectAttr "bindPose1.m[38]" "bindPose1.p[39]";
connectAttr "bindPose1.m[39]" "bindPose1.p[40]";
connectAttr "bindPose1.m[40]" "bindPose1.p[41]";
connectAttr "bindPose1.m[41]" "bindPose1.p[42]";
connectAttr "bindPose1.m[42]" "bindPose1.p[43]";
connectAttr "bindPose1.m[43]" "bindPose1.p[44]";
connectAttr "bindPose1.m[0]" "bindPose1.p[45]";
connectAttr "bindPose1.m[45]" "bindPose1.p[46]";
connectAttr "bindPose1.m[46]" "bindPose1.p[47]";
connectAttr "bindPose1.m[47]" "bindPose1.p[48]";
connectAttr "bindPose1.m[48]" "bindPose1.p[49]";
connectAttr "bindPose1.m[0]" "bindPose1.p[50]";
connectAttr "bindPose1.m[50]" "bindPose1.p[51]";
connectAttr "bindPose1.m[51]" "bindPose1.p[52]";
connectAttr "bindPose1.m[52]" "bindPose1.p[53]";
connectAttr "bindPose1.m[53]" "bindPose1.p[54]";
connectAttr "bindPose1.m[0]" "bindPose1.p[55]";
connectAttr "bindPose1.m[55]" "bindPose1.p[56]";
connectAttr "bindPose1.m[56]" "bindPose1.p[57]";
connectAttr "bindPose1.m[57]" "bindPose1.p[58]";
connectAttr "bindPose1.m[58]" "bindPose1.p[59]";
connectAttr "bindPose1.m[59]" "bindPose1.p[60]";
connectAttr "bindPose1.m[60]" "bindPose1.p[61]";
connectAttr "bindPose1.m[61]" "bindPose1.p[62]";
connectAttr "bindPose1.m[62]" "bindPose1.p[63]";
connectAttr "bindPose1.m[63]" "bindPose1.p[64]";
connectAttr "bindPose1.m[64]" "bindPose1.p[65]";
connectAttr "bindPose1.m[65]" "bindPose1.p[66]";
connectAttr "bindPose1.m[11]" "bindPose1.p[67]";
connectAttr "bindPose1.m[18]" "bindPose1.p[68]";
connectAttr "bindPose1.m[28]" "bindPose1.p[69]";
connectAttr "bindPose1.m[30]" "bindPose1.p[70]";
connectAttr "bindPose1.m[37]" "bindPose1.p[71]";
connectAttr "bindPose1.m[44]" "bindPose1.p[72]";
connectAttr "bindPose1.m[49]" "bindPose1.p[73]";
connectAttr "bindPose1.m[54]" "bindPose1.p[74]";
connectAttr "bindPose1.m[66]" "bindPose1.p[75]";
connectAttr "dragon_joints.bps" "bindPose1.wm[0]";
connectAttr "joint30.bps" "bindPose1.wm[1]";
connectAttr "joint31.bps" "bindPose1.wm[2]";
connectAttr "joint32.bps" "bindPose1.wm[3]";
connectAttr "joint33.bps" "bindPose1.wm[4]";
connectAttr "joint21.bps" "bindPose1.wm[5]";
connectAttr "joint22.bps" "bindPose1.wm[6]";
connectAttr "joint23.bps" "bindPose1.wm[7]";
connectAttr "joint24.bps" "bindPose1.wm[8]";
connectAttr "joint25.bps" "bindPose1.wm[9]";
connectAttr "joint26.bps" "bindPose1.wm[10]";
connectAttr "joint27.bps" "bindPose1.wm[11]";
connectAttr "joint13.bps" "bindPose1.wm[12]";
connectAttr "joint14.bps" "bindPose1.wm[13]";
connectAttr "joint15.bps" "bindPose1.wm[14]";
connectAttr "joint16.bps" "bindPose1.wm[15]";
connectAttr "joint17.bps" "bindPose1.wm[16]";
connectAttr "joint18.bps" "bindPose1.wm[17]";
connectAttr "joint19.bps" "bindPose1.wm[18]";
connectAttr "joint34.bps" "bindPose1.wm[19]";
connectAttr "joint35.bps" "bindPose1.wm[20]";
connectAttr "joint36.bps" "bindPose1.wm[21]";
connectAttr "joint37.bps" "bindPose1.wm[22]";
connectAttr "joint38.bps" "bindPose1.wm[23]";
connectAttr "joint39.bps" "bindPose1.wm[24]";
connectAttr "joint40.bps" "bindPose1.wm[25]";
connectAttr "joint41.bps" "bindPose1.wm[26]";
connectAttr "joint42.bps" "bindPose1.wm[27]";
connectAttr "joint43.bps" "bindPose1.wm[28]";
connectAttr "joint45.bps" "bindPose1.wm[29]";
connectAttr "joint46.bps" "bindPose1.wm[30]";
connectAttr "joint69.bps" "bindPose1.wm[31]";
connectAttr "joint70.bps" "bindPose1.wm[32]";
connectAttr "joint71.bps" "bindPose1.wm[33]";
connectAttr "joint72.bps" "bindPose1.wm[34]";
connectAttr "joint73.bps" "bindPose1.wm[35]";
connectAttr "joint74.bps" "bindPose1.wm[36]";
connectAttr "joint75.bps" "bindPose1.wm[37]";
connectAttr "joint67.bps" "bindPose1.wm[38]";
connectAttr "joint68.bps" "bindPose1.wm[39]";
connectAttr "joint61.bps" "bindPose1.wm[40]";
connectAttr "joint62.bps" "bindPose1.wm[41]";
connectAttr "joint63.bps" "bindPose1.wm[42]";
connectAttr "joint64.bps" "bindPose1.wm[43]";
connectAttr "joint65.bps" "bindPose1.wm[44]";
connectAttr "joint7.bps" "bindPose1.wm[45]";
connectAttr "joint8.bps" "bindPose1.wm[46]";
connectAttr "joint9.bps" "bindPose1.wm[47]";
connectAttr "joint10.bps" "bindPose1.wm[48]";
connectAttr "joint11.bps" "bindPose1.wm[49]";
connectAttr "joint1.bps" "bindPose1.wm[50]";
connectAttr "joint2.bps" "bindPose1.wm[51]";
connectAttr "joint3.bps" "bindPose1.wm[52]";
connectAttr "joint4.bps" "bindPose1.wm[53]";
connectAttr "joint5.bps" "bindPose1.wm[54]";
connectAttr "joint48.bps" "bindPose1.wm[55]";
connectAttr "joint49.bps" "bindPose1.wm[56]";
connectAttr "joint50.bps" "bindPose1.wm[57]";
connectAttr "joint51.bps" "bindPose1.wm[58]";
connectAttr "joint52.bps" "bindPose1.wm[59]";
connectAttr "joint53.bps" "bindPose1.wm[60]";
connectAttr "joint54.bps" "bindPose1.wm[61]";
connectAttr "joint55.bps" "bindPose1.wm[62]";
connectAttr "joint56.bps" "bindPose1.wm[63]";
connectAttr "joint57.bps" "bindPose1.wm[64]";
connectAttr "joint58.bps" "bindPose1.wm[65]";
connectAttr "joint59.bps" "bindPose1.wm[66]";
connectAttr "joint28.bps" "bindPose1.wm[67]";
connectAttr "joint20.bps" "bindPose1.wm[68]";
connectAttr "joint44.bps" "bindPose1.wm[69]";
connectAttr "joint47.bps" "bindPose1.wm[70]";
connectAttr "joint76.bps" "bindPose1.wm[71]";
connectAttr "joint66.bps" "bindPose1.wm[72]";
connectAttr "joint12.bps" "bindPose1.wm[73]";
connectAttr "joint6.bps" "bindPose1.wm[74]";
connectAttr "joint60.bps" "bindPose1.wm[75]";
connectAttr "dragon_geometryShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of dragon.ma
