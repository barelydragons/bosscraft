//Maya ASCII 2014 scene
//Name: drPhilGoode_001.ma
//Last modified: Sat, Jul 12, 2014 02:36:23 PM
//Codeset: UTF-8
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010035-864206";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.6982223499779838 28.692343716856222 31.685454414025422 ;
	setAttr ".r" -type "double3" -15.599999999998985 3.2000000000004944 -9.954755190426019e-17 ;
	setAttr ".rpt" -type "double3" 2.4244594218142998e-16 -4.7155027760968075e-18 8.1631458052365179e-18 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 32.265872371886019;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.36614379298811406 22.8635019405923 0.11060718517453338 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".t" -type "double3" 2.3879995168450816 26.117956612044139 102.91353518876389 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 12.33053321406136;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".t" -type "double3" 101.55486171813166 23.536744131674851 0.11060718517455559 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 24.267321128840781;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "frontIMG_PLN";
	setAttr ".t" -type "double3" 0 15.533315233255657 -14.425582803448723 ;
	setAttr ".s" -type "double3" 1.5461949938255606 1.5461949938255606 1.5461949938255606 ;
	setAttr ".rp" -type "double3" 0 0 2.7465940523284626e-15 ;
	setAttr ".sp" -type "double3" 0 0 1.7763568394002505e-15 ;
	setAttr ".spt" -type "double3" 0 0 9.7023721292821211e-16 ;
createNode imagePlane -n "frontIMG_PLNShape" -p "frontIMG_PLN";
	setAttr -k off ".v";
	setAttr ".fc" 76;
	setAttr ".imn" -type "string" "/Users/oneanimefan/Desktop/Game Dev Workshop I_II/GMAP 378/orthos/DoctorPhilGoode_Healer_front.png";
	setAttr ".cov" -type "short2" 1256 1886 ;
	setAttr ".dlc" no;
	setAttr ".w" 12.56;
	setAttr ".h" 18.860000000000003;
createNode transform -n "sideIMG_PLN";
	setAttr ".t" -type "double3" -12.065213033516329 16.550875662448597 -5.233834853762148 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 1.5472345499495799 1.5472345499495799 1.5472345499495799 ;
createNode imagePlane -n "sideIMG_PLNShape" -p "sideIMG_PLN";
	setAttr -k off ".v";
	setAttr ".fc" 76;
	setAttr ".imn" -type "string" "/Users/oneanimefan/Desktop/Game Dev Workshop I_II/GMAP 378/orthos/DoctorPhilGoode_Healer_side.png";
	setAttr ".cov" -type "short2" 940 1694 ;
	setAttr ".dlc" no;
	setAttr ".w" 9.4;
	setAttr ".h" 16.94;
createNode transform -n "pCube1";
	setAttr ".t" -type "double3" -0.36614374183070919 23.571017086221776 -5.8965527181719217 ;
	setAttr ".s" -type "double3" 0.85827883142482431 0.85827883142482431 0.85827883142482431 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".pt";
	setAttr ".pt[56]" -type "float3" -0.36017418 -0.14049326 0.10508269 ;
	setAttr ".pt[57]" -type "float3" 0.36017424 -0.14049309 0.10508271 ;
	setAttr ".pt[58]" -type "float3" 0.36017424 -0.77251446 0.70062363 ;
	setAttr ".pt[59]" -type "float3" -0.36017424 -0.77251446 0.70062363 ;
	setAttr ".pt[60]" -type "float3" 0.24609879 0.26958579 0 ;
	setAttr ".pt[61]" -type "float3" 0.24622278 0.00038951635 -0.00013919547 ;
	setAttr ".pt[62]" -type "float3" -0.24612364 -4.4703484e-08 -3.3527613e-08 ;
	setAttr ".pt[63]" -type "float3" -0.2461236 0.26958579 0 ;
	setAttr ".pt[64]" -type "float3" 0 7.4505806e-09 4.4703484e-08 ;
	setAttr ".pt[65]" -type "float3" 9.1601523e-06 4.5945351e-05 3.5744706e-05 ;
	setAttr ".pt[66]" -type "float3" 5.4370612e-06 -0.21779205 0.034490995 ;
	setAttr ".pt[67]" -type "float3" 2.0302832e-06 -0.21779373 0.034493502 ;
	setAttr ".pt[68]" -type "float3" 3.2116386e-05 -0.18662815 -0.15973933 ;
	setAttr ".pt[69]" -type "float3" 3.2110798e-05 -0.186628 -0.15973912 ;
	setAttr ".pt[71]" -type "float3" 2.6810449e-06 0.00051017571 -6.6362496e-05 ;
	setAttr ".pt[72]" -type "float3" 1.2818001e-05 -0.15534736 -0.0026420606 ;
	setAttr ".pt[73]" -type "float3" 1.9925999e-06 -0.15539388 -0.0026673244 ;
	setAttr ".pt[74]" -type "float3" -3.608875e-09 -5.9571903e-09 -5.1484676e-08 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube2";
	setAttr ".t" -type "double3" -0.23962664690467328 14.786818135829167 -5.4553447044513721 ;
	setAttr ".s" -type "double3" 0.59221188772972422 0.70327108243586967 0.70327108243586967 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0.51848245 1.8578959 ;
	setAttr ".pt[1]" -type "float3" 0 0.51848245 1.8578959 ;
	setAttr ".pt[2]" -type "float3" 0 0.51848245 1.8578959 ;
	setAttr ".pt[3]" -type "float3" 0 0.51848245 1.8578959 ;
	setAttr ".pt[4]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[5]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[6]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[7]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[8]" -type "float3" 0 0 0.30244809 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.30244809 ;
	setAttr ".pt[10]" -type "float3" 0 0 0.30244809 ;
	setAttr ".pt[11]" -type "float3" 0 0 0.30244809 ;
	setAttr ".pt[12]" -type "float3" 0 -1.0369649 -4.3638949 ;
	setAttr ".pt[13]" -type "float3" 0 -1.0369649 -4.3638949 ;
	setAttr ".pt[14]" -type "float3" 0 -1.0369649 -4.3638949 ;
	setAttr ".pt[15]" -type "float3" 0 -1.0369649 -4.3638949 ;
	setAttr ".pt[16]" -type "float3" 0 -0.086413741 -1.8578954 ;
	setAttr ".pt[17]" -type "float3" 0 -0.086413741 -1.8578954 ;
	setAttr ".pt[18]" -type "float3" 0 -0.086413741 -1.8578954 ;
	setAttr ".pt[19]" -type "float3" 0 -0.086413741 -1.8578954 ;
	setAttr ".pt[20]" -type "float3" 0 -0.086413741 -5.9604645e-08 ;
	setAttr ".pt[21]" -type "float3" 0 -0.086413741 -5.9604645e-08 ;
	setAttr ".pt[22]" -type "float3" 0 -0.086413741 -5.9604645e-08 ;
	setAttr ".pt[23]" -type "float3" 0 -0.086413741 -5.9604645e-08 ;
	setAttr ".pt[24]" -type "float3" 0 -0.99375826 1.9875159 ;
	setAttr ".pt[25]" -type "float3" 0 -0.99375826 1.9875159 ;
	setAttr ".pt[26]" -type "float3" 0 -0.99375826 1.9875159 ;
	setAttr ".pt[27]" -type "float3" 0 -0.99375826 1.9875159 ;
	setAttr ".pt[28]" -type "float3" 0 0 -0.43206871 ;
	setAttr ".pt[29]" -type "float3" 0 0 -0.43206871 ;
	setAttr ".pt[30]" -type "float3" 0 0 -0.43206871 ;
	setAttr ".pt[31]" -type "float3" 0 0 -0.43206871 ;
	setAttr ".pt[32]" -type "float3" 0 0 -0.60489619 ;
	setAttr ".pt[33]" -type "float3" 0 0 -0.60489619 ;
	setAttr ".pt[34]" -type "float3" 0 0 -0.60489619 ;
	setAttr ".pt[35]" -type "float3" 0 0 -0.60489619 ;
	setAttr ".pt[36]" -type "float3" 0 0.30244809 -0.25924122 ;
	setAttr ".pt[37]" -type "float3" 0 0.30244809 -0.25924122 ;
	setAttr ".pt[38]" -type "float3" 0 0.30244809 -0.25924122 ;
	setAttr ".pt[39]" -type "float3" 0 0.30244809 -0.25924122 ;
	setAttr ".pt[44]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[45]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[46]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[47]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[49]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[51]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[53]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[55]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[56]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[57]" -type "float3" 0 0 -2.07393 ;
	setAttr ".pt[58]" -type "float3" 0 0 -2.07393 ;
	setAttr ".pt[59]" -type "float3" 0 0 -2.07393 ;
	setAttr ".pt[60]" -type "float3" 0 0 -2.07393 ;
	setAttr ".pt[61]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[63]" -type "float3" 0 0 0.69130993 ;
	setAttr ".pt[64]" -type "float3" 0 0 0.69130993 ;
	setAttr ".pt[65]" -type "float3" 0 0 0.69130993 ;
	setAttr ".pt[66]" -type "float3" 0 0 0.69130993 ;
	setAttr ".pt[68]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[69]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[70]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[71]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[72]" -type "float3" 0 0 1.8578959 ;
	setAttr ".pt[73]" -type "float3" 0 0 -1.4690332 ;
	setAttr ".pt[75]" -type "float3" 0 0.31890568 -0.36991304 ;
	setAttr ".pt[76]" -type "float3" 0 0.31890568 -0.36991304 ;
	setAttr ".pt[77]" -type "float3" 0 0.31890568 -0.36991304 ;
	setAttr ".pt[78]" -type "float3" 0 0.31890568 -0.36991304 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube3";
	setAttr ".t" -type "double3" 6.2462183425855757 16.602250820798321 -5.9886845748225976 ;
	setAttr ".s" -type "double3" 0.71227812158379933 0.71227812158379933 0.71227812158379933 ;
	setAttr ".rp" -type "double3" -2.4613297289920277 2.6065058649681285 8.8817841970012513e-16 ;
	setAttr ".sp" -type "double3" -3.4555739596761654 3.6593934110630619 1.2469545150779016e-15 ;
	setAttr ".spt" -type "double3" 0.99424423068414447 -1.0528875460949583 -3.5877609537777651e-16 ;
createNode transform -n "transform4" -p "pCube3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:93]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 120 ".uvst[0].uvsp[0:119]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.45833334 0.83333325 0.54166669 0.83333325 0.54166669
		 0.91666657 0.45833334 0.91666657 0.45833334 0.83333325 0.54166669 0.83333325 0.54166669
		 0.91666657 0.45833334 0.91666657 0.54166669 0.91666657 0.54166669 0.83333325 0.45833334
		 0.83333325 0.45833334 0.91666657 0.54166669 0.91666651 0.54166669 0.83333325 0.45833331
		 0.83333325 0.45833331 0.91666651 0.54166669 0.015731644 0.45833334 0.015731644 0.37500003
		 0.015731644 0.29166669 0.015731644 0.20833334 0.015731644 0.125 0.015731644 0.375
		 0.73426831 0.45833334 0.73426831 0.54166669 0.73426831 0.625 0.73426831 0.875 0.015731644
		 0.79166669 0.015731644 0.70833337 0.015731644 0.625 0.015731644 0.625 0.24103205
		 0.54166669 0.24103205 0.45833334 0.24103205 0.375 0.24103205 0.29166669 0.24103205
		 0.20833334 0.24103205 0.125 0.24103205 0.375 0.50896794 0.45833334 0.50896794 0.54166669
		 0.50896794 0.625 0.50896794 0.875 0.24103205 0.79166669 0.24103205 0.70833337 0.24103205;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 96 ".pt[0:95]" -type "float3"  1.7532692 -2.5917194 -0.76562595 
		1.3593372 -2.3323894 -1.0892276 0.77353162 -1.946748 -1.0892276 0.37959766 -1.6874175 
		-0.76562595 1.1895039 -3.4344139 -0.76562595 0.79556996 -3.1750839 -1.0892276 0.20976403 
		-2.7894423 -1.0892276 -0.18416943 -2.5301118 -0.76562595 0.090958357 -4.3014421 -0.76562619 
		-0.34815311 -3.9349151 -1.0892274 -1.0014695 -3.3899555 -1.0892274 -1.4410052 -3.0235467 
		-0.76562619 -1.2571477 -3.9961824 -0.76562625 -2.7404566 -3.2550571 -1.0892273 -4.9428315 
		-2.6470416 -1.0892274 -5.9040198 -1.7552848 -0.76562619 -0.62368882 -4.427361 -0.36307573 
		-2.7404575 -3.2550588 -0.3630757 -4.9428315 -2.6470416 -0.36307573 -6.3721843 -1.3209436 
		-0.36307573 -0.62368882 -4.4273615 0.36307561 -2.7404575 -3.2550595 0.36307558 -4.9428315 
		-2.6470416 0.36307561 -6.3721852 -1.3209436 0.36307561 -1.2571477 -3.996182 0.76562589 
		-2.740458 -3.2550597 1.0892278 -4.9428315 -2.6470418 1.0892274 -5.9040198 -1.7552848 
		0.76562589 0.090958357 -4.3014421 0.76562577 -0.34815311 -3.9349151 1.0892276 -1.0014695 
		-3.3899555 1.0892276 -1.4410052 -3.0235467 0.76562577 1.1895039 -3.4344139 0.76562607 
		0.79556996 -3.1750839 1.0892276 0.20976403 -2.7894423 1.0892276 -0.18416937 -2.5301123 
		0.76562601 1.7532692 -2.5917194 0.76562607 1.3593372 -2.3323894 1.0892276 0.77353162 
		-1.946748 1.0892276 0.37959766 -1.6874175 0.76562607 1.9451451 -2.7180312 0.36307567 
		1.3593372 -2.3323894 0.36307567 0.77353162 -1.946748 0.36307567 0.18772562 -1.5611062 
		0.36307567 1.9451451 -2.7180312 -0.36307579 1.3593372 -2.3323894 -0.36307579 0.77353162 
		-1.946748 -0.36307579 0.18772562 -1.5611062 -0.36307579 -0.37604165 -2.403801 0.36307567 
		-0.37604165 -2.403801 -0.36307579 -1.6551456 -2.845099 0.36307529 -1.6551456 -2.845099 
		-0.36307621 1.3813752 -3.560725 0.36307567 1.3813752 -3.560725 -0.36307579 0.30476737 
		-4.4799871 0.36307529 0.30476737 -4.4799871 -0.36307621 1.3000385 -2.2933533 0.28957018 
		0.83282924 -1.985785 0.28957018 0.83282924 -1.985785 -0.28957039 1.3000385 -2.2933533 
		-0.28957039 1.5705451 -1.7943022 0.2350433 1.1913148 -1.544651 0.2350433 1.1913148 
		-1.544651 -0.23504338 1.5705451 -1.7943022 -0.23504338 1.1239505 -1.5506318 -0.28957039 
		1.1239505 -1.5506318 0.28957018 1.5911589 -1.8581996 0.28957018 1.5911589 -1.8581996 
		-0.28957039 0.84344047 -1.9699237 -0.28957039 0.84344047 -1.9699237 0.28957018 1.3106494 
		-2.277492 0.28957018 1.3106494 -2.277492 -0.28957039 0.66710299 -2.1058316 -1.0892276 
		1.2529082 -2.4914725 -1.0892276 1.6468428 -2.7508025 -0.76562595 1.8387145 -2.877116 
		-0.36307579 1.8387145 -2.877116 0.36307567 1.6468428 -2.750803 0.76562607 1.2529083 
		-2.491473 1.0892276 0.66710299 -2.1058316 1.0892276 0.27316955 -1.8465028 0.76562607 
		0.081297547 -1.7201911 0.36307567 0.081297547 -1.7201911 -0.36307579 0.27316946 -1.8465023 
		-0.76562595 -2.5393264 -3.5029452 -0.76562619 -2.0960233 -3.8680434 -1.0892274 -1.4368021 
		-4.4109707 -1.0892274 -0.9984175 -4.7760706 -0.76562619 -0.35347921 -4.9538965 -0.36307549 
		-0.35347921 -4.9538965 0.36307594 -0.9984175 -4.7760706 0.76562577 -1.4368021 -4.4109707 
		1.0892276 -2.0960233 -3.8680434 1.0892276 -2.5393264 -3.5029452 0.76562577 -2.7552454 
		-3.3251173 0.36307594 -2.7552454 -3.3251173 -0.36307549;
	setAttr -s 96 ".vt[0:95]"  -1.89609432 -6.14870453 1.85174227 -0.80859375 -6.14870453 2.63440418
		 0.8085928 -6.14870453 2.63440418 1.89609337 -6.14870453 1.85174227 -1.89609432 -2.049568176 1.85174227
		 -0.80859375 -2.049568176 2.63440418 0.8085928 -2.049568176 2.63440418 1.89609337 -2.049568176 1.85174227
		 -1.89609432 2.049566269 1.85174227 -0.80859375 2.049566269 2.63440418 0.8085928 2.049566269 2.63440418
		 1.89609337 2.049566269 1.85174227 -1.89609432 6.14870262 1.85174227 -0.80859375 6.14870262 2.63440418
		 0.8085928 6.14870262 2.63440418 1.89609337 6.14870262 1.85174227 -2.4257822 6.14870262 0.87813425
		 -0.80859375 6.14870262 0.87813425 0.8085928 6.14870262 0.87813425 2.4257803 6.14870262 0.87813425
		 -2.4257822 6.14870262 -0.87813473 -0.80859375 6.14870262 -0.87813473 0.8085928 6.14870262 -0.87813473
		 2.4257803 6.14870262 -0.87813473 -1.89609432 6.14870262 -1.85174179 -0.80859375 6.14870262 -2.63440418
		 0.8085928 6.14870262 -2.63440418 1.89609337 6.14870262 -1.85174179 -1.89609432 2.049566269 -1.85174179
		 -0.80859375 2.049566269 -2.63440418 0.8085928 2.049566269 -2.63440418 1.89609337 2.049566269 -1.85174179
		 -1.89609432 -2.049568176 -1.85174179 -0.80859375 -2.049568176 -2.63440418 0.8085928 -2.049568176 -2.63440418
		 1.89609337 -2.049568176 -1.85174179 -1.89609432 -6.14870453 -1.85174179 -0.80859375 -6.14870453 -2.63440418
		 0.8085928 -6.14870453 -2.63440418 1.89609337 -6.14870453 -1.85174179 -2.4257822 -6.14870453 -0.87813377
		 -0.80859375 -6.14870453 -0.87813377 0.8085928 -6.14870453 -0.87813377 2.4257803 -6.14870453 -0.87813377
		 -2.4257822 -6.14870453 0.8781352 -0.80859375 -6.14870453 0.8781352 0.8085928 -6.14870453 0.8781352
		 2.4257803 -6.14870453 0.8781352 2.4257803 -2.049568176 -0.87813377 2.4257803 -2.049568176 0.8781352
		 2.4257803 2.049566269 -0.87813377 2.4257803 2.049566269 0.8781352 -2.4257822 -2.049568176 -0.87813377
		 -2.4257822 -2.049568176 0.8781352 -2.4257822 2.049566269 -0.87813377 -2.4257822 2.049566269 0.8781352
		 -0.64489269 -6.14870453 -0.70035362 0.64489174 -6.14870453 -0.70035362 0.64489174 -6.14870453 0.70035505
		 -0.64489269 -6.14870453 0.70035505 -0.52345759 -8.43538857 -0.56847483 0.52345663 -8.43538857 -0.56847483
		 0.52345663 -8.43538857 0.56847626 -0.52345759 -8.43538857 0.56847626 0.64489174 -8.26543045 0.70035505
		 0.64489174 -8.26543045 -0.70035362 -0.64489269 -8.26543045 -0.70035362 -0.64489269 -8.26543045 0.70035505
		 0.64489168 -6.22585678 0.70035505 0.64489168 -6.22585678 -0.70035362 -0.64489269 -6.22585678 -0.70035362
		 -0.64489269 -6.22585678 0.70035505 0.80859286 -5.37487078 2.63440418 -0.80859375 -5.37487078 2.63440418
		 -1.89609432 -5.37487078 1.85174227 -2.4257822 -5.37487078 0.8781352 -2.4257822 -5.37487078 -0.87813377
		 -1.89609432 -5.3748703 -1.85174179 -0.80859375 -5.3748703 -2.63440418 0.8085928 -5.3748703 -2.63440418
		 1.89609337 -5.3748703 -1.85174179 2.4257803 -5.37487078 -0.87813377 2.4257803 -5.37487078 0.8781352
		 1.89609337 -5.37487078 1.85174227 1.89609337 5.70757198 1.85174227 0.8085928 5.70757198 2.63440418
		 -0.80859375 5.70757198 2.63440418 -1.89609432 5.70757198 1.85174227 -2.4257822 5.70757198 0.87813437
		 -2.4257822 5.70757198 -0.87813461 -1.89609432 5.70757198 -1.85174179 -0.80859375 5.70757198 -2.63440418
		 0.8085928 5.70757198 -2.63440418 1.89609337 5.70757198 -1.85174179 2.4257803 5.70757198 -0.87813461
		 2.4257803 5.70757198 0.87813437;
	setAttr -s 188 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1 9 10 1 10 11 1
		 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1 24 25 0 25 26 0
		 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0 38 39 0 40 41 1
		 41 42 0 42 43 1 44 45 1 45 46 0 46 47 1 0 74 0 1 73 1 2 72 1 3 83 0 4 8 0 5 9 1 6 10 1
		 7 11 0 8 87 0 9 86 1 10 85 1 11 84 0 12 16 0 13 17 1 14 18 1 15 19 0 16 20 0 17 21 1
		 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 90 0 25 91 1 26 92 1 27 93 0 28 32 0
		 29 33 1 30 34 1 31 35 0 32 77 0 33 78 1 34 79 1 35 80 0 36 40 0 37 41 1 38 42 1 39 43 0
		 40 44 0 41 45 0 42 46 0 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1 48 49 1 49 7 1
		 31 50 1 50 51 1 51 11 1 43 81 1 47 82 1 48 50 1 49 51 1 50 94 1 51 95 1 32 52 1 52 53 1
		 53 4 1 28 54 1 54 55 1 55 8 1 40 76 1 44 75 1 52 54 1 53 55 1 54 89 1 55 88 1 41 56 0
		 42 57 0 56 57 0 46 58 0 57 58 0 45 59 0 59 58 0 56 59 0 56 70 0 57 69 0 60 61 0 58 68 0
		 61 62 0 59 71 0 63 62 0 60 63 0 64 62 0 65 61 0 64 65 1 66 60 0 65 66 1 67 63 0 66 67 1
		 67 64 1 68 64 0 69 65 0 68 69 1 70 66 0 69 70 1 71 67 0 70 71 1 71 68 1 72 6 1 73 5 1
		 72 73 1 74 4 0 73 74 1 75 53 1 74 75 1 76 52 1 75 76 1 77 36 0 76 77 1 78 37 1 77 78 1
		 79 38 1 78 79 1 80 39 0 79 80 1 81 48 1 80 81 1 82 49 1 81 82 1 83 7 0 82 83 1 83 72 1
		 84 15 0 85 14 1;
	setAttr ".ed[166:187]" 84 85 1 86 13 1 85 86 1 87 12 0 86 87 1 88 16 1 87 88 1
		 89 20 1 88 89 1 90 28 0 89 90 1 91 29 1 90 91 1 92 30 1 91 92 1 93 31 0 92 93 1 94 23 1
		 93 94 1 95 19 1 94 95 1 95 84 1;
	setAttr -s 94 -ch 376 ".fc[0:93]" -type "polyFaces" 
		f 4 0 37 144 -37
		mu 0 4 0 1 93 94
		f 4 1 38 142 -38
		mu 0 4 1 2 92 93
		f 4 2 39 163 -39
		mu 0 4 2 3 105 92
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 170 -45
		mu 0 4 8 9 108 109
		f 4 7 46 168 -46
		mu 0 4 9 10 107 108
		f 4 8 47 166 -47
		mu 0 4 10 11 106 107
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 178 177 -22 -176
		mu 0 4 113 114 29 28
		f 4 180 179 -23 -178
		mu 0 4 114 115 30 29
		f 4 182 181 -24 -180
		mu 0 4 115 116 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 152 151 -28 -150
		mu 0 4 98 99 37 36
		f 4 154 153 -29 -152
		mu 0 4 99 100 38 37
		f 4 156 155 -30 -154
		mu 0 4 100 101 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 118 120 -123 -124
		mu 0 4 80 81 82 83
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -156 158 -91
		mu 0 4 53 52 102 103
		f 4 -80 90 160 -92
		mu 0 4 54 53 103 104
		f 4 -84 91 162 -40
		mu 0 4 3 54 104 105
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -182 184 -95
		mu 0 4 59 58 117 118
		f 4 -89 94 186 -96
		mu 0 4 60 59 118 119
		f 4 -90 95 187 -48
		mu 0 4 11 60 119 106
		f 4 72 102 150 149
		mu 0 4 64 65 96 97
		f 4 76 103 148 -103
		mu 0 4 65 66 95 96
		f 4 80 36 146 -104
		mu 0 4 66 0 94 95
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 176 175
		mu 0 4 70 71 111 112
		f 4 100 107 174 -107
		mu 0 4 71 72 110 111
		f 4 101 44 172 -108
		mu 0 4 72 8 109 110
		f 4 31 109 -111 -109
		mu 0 4 41 42 77 76
		f 4 78 111 -113 -110
		mu 0 4 42 46 78 77
		f 4 -35 113 114 -112
		mu 0 4 46 45 79 78
		f 4 -78 108 115 -114
		mu 0 4 45 41 76 79
		f 4 110 117 136 -117
		mu 0 4 76 77 89 90
		f 4 112 119 134 -118
		mu 0 4 77 78 88 89
		f 4 -115 121 139 -120
		mu 0 4 78 79 91 88
		f 4 -116 116 138 -122
		mu 0 4 79 76 90 91
		f 4 -127 124 -121 -126
		mu 0 4 85 84 82 81
		f 4 -129 125 -119 -128
		mu 0 4 86 85 81 80
		f 4 -131 127 123 -130
		mu 0 4 87 86 80 83
		f 4 -132 129 122 -125
		mu 0 4 84 87 83 82
		f 4 -135 132 126 -134
		mu 0 4 89 88 84 85
		f 4 -137 133 128 -136
		mu 0 4 90 89 85 86
		f 4 -139 135 130 -138
		mu 0 4 91 90 86 87
		f 4 -140 137 131 -133
		mu 0 4 88 91 87 84
		f 4 -143 140 -5 -142
		mu 0 4 93 92 6 5
		f 4 -145 141 -4 -144
		mu 0 4 94 93 5 4
		f 4 -147 143 -99 -146
		mu 0 4 95 94 4 69
		f 4 -149 145 -98 -148
		mu 0 4 96 95 69 68
		f 4 -151 147 -97 68
		mu 0 4 97 96 68 67
		f 4 24 69 -153 -69
		mu 0 4 32 33 99 98
		f 4 25 70 -155 -70
		mu 0 4 33 34 100 99
		f 4 26 71 -157 -71
		mu 0 4 34 35 101 100
		f 4 -159 -72 84 -158
		mu 0 4 103 102 55 56
		f 4 -161 157 85 -160
		mu 0 4 104 103 56 57
		f 4 -163 159 86 -162
		mu 0 4 105 104 57 7
		f 4 -164 161 -6 -141
		mu 0 4 92 105 7 6
		f 4 -167 164 -12 -166
		mu 0 4 107 106 15 14
		f 4 -169 165 -11 -168
		mu 0 4 108 107 14 13
		f 4 -171 167 -10 -170
		mu 0 4 109 108 13 12
		f 4 -173 169 48 -172
		mu 0 4 110 109 12 75
		f 4 -175 171 52 -174
		mu 0 4 111 110 75 74
		f 4 -177 173 56 60
		mu 0 4 112 111 74 73
		f 4 18 61 -179 -61
		mu 0 4 24 25 114 113
		f 4 19 62 -181 -62
		mu 0 4 25 26 115 114
		f 4 20 63 -183 -63
		mu 0 4 26 27 116 115
		f 4 -185 -64 -60 -184
		mu 0 4 118 117 61 62
		f 4 -187 183 -56 -186
		mu 0 4 119 118 62 63
		f 4 -188 185 -52 -165
		mu 0 4 106 119 63 15;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube5";
	setAttr ".t" -type "double3" 7.6225879033293218 9.2343418729343565 -5.9875785319270642 ;
	setAttr ".s" -type "double3" 0.3391905970557359 0.46936087007365579 0.31006592361370461 ;
createNode transform -n "transform3" -p "pCube5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape4" -p "transform3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube6";
	setAttr ".t" -type "double3" -8.0704845854955494 9.2343418729343529 -5.9875785319270607 ;
	setAttr ".r" -type "double3" 0 -180 0 ;
	setAttr ".s" -type "double3" 0.3391905970557359 0.46936087007365579 0.31006592361370461 ;
createNode transform -n "transform2" -p "pCube6";
	setAttr ".v" no;
createNode mesh -n "pCubeShape6" -p "transform2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:143]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 175 ".uvst[0].uvsp[0:174]" -type "float2" 0.375 0 0.5 0 0.625
		 0 0.375 0.125 0.5 0.125 0.625 0.125 0.375 0.25 0.5 0.25 0.625 0.25 0.375 0.375 0.5
		 0.375 0.625 0.375 0.375 0.5 0.5 0.5 0.625 0.5 0.375 0.625 0.5 0.625 0.625 0.625 0.375
		 0.75 0.5 0.75 0.625 0.75 0.375 0.875 0.5 0.875 0.625 0.875 0.375 1 0.5 1 0.625 1
		 0.875 0 0.75 0 0.875 0.125 0.75 0.125 0.875 0.25 0.75 0.25 0.125 0 0.25 0 0.125 0.125
		 0.25 0.125 0.125 0.25 0.25 0.25 0.581792 0 0.581792 1 0.581792 0.875 0.581792 0.75
		 0.581792 0.625 0.581792 0.5 0.581792 0.375 0.581792 0.25 0.581792 0.125 0.41820797
		 0.125 0.41820797 0.25 0.41820797 0.375 0.41820797 0.5 0.41820797 0.625 0.41820797
		 0.75 0.41820797 0.875 0.41820797 0 0.41820797 1 0.54011309 0 0.54011309 1 0.54011309
		 0.875 0.54011309 0.75 0.54011309 0.625 0.54011309 0.5 0.54011309 0.375 0.54011309
		 0.25 0.54011309 0.125 0.45988694 0.125 0.45988694 0.25 0.45988694 0.375 0.45988694
		 0.5 0.45988694 0.625 0.45988694 0.75 0.45988694 0.875 0.45988694 0 0.45988694 1 0.125
		 0 0.25 0 0.125 0.125 0.375 0 0.375 0.125 0.25 0.25 0.125 0.25 0.375 0.25 0.375 0.038564451
		 0.375 0.038564451 0.25 0.038564451 0.125 0.038564451 0.125 0.038564451 0.375 0.71143556
		 0.41820794 0.71143556 0.45988694 0.71143556 0.5 0.71143556 0.54011309 0.71143556
		 0.581792 0.71143556 0.625 0.71143556 0.875 0.038564451 0.75 0.038564451 0.625 0.038564451
		 0.581792 0.038564451 0.54011309 0.038564451 0.5 0.038564451 0.45988694 0.038564451
		 0.41820794 0.038564451 0.41820794 0.21143556 0.45988694 0.21143556 0.5 0.21143556
		 0.54011309 0.21143556 0.581792 0.21143556 0.625 0.21143556 0.75 0.21143556 0.625
		 0.53856444 0.875 0.21143556 0.581792 0.53856444 0.54011309 0.53856444 0.5 0.53856444
		 0.45988694 0.53856444 0.41820794 0.53856444 0.125 0.21143556 0.375 0.53856444 0.125
		 0.21143556 0.25 0.21143556 0.375 0.21143556 0.375 0.21143556 0.33844543 0.25 0.375
		 0.28655457 0.33844543 0.25 0.33844543 0.21143556 0.33844543 0.125 0.33844543 0.038564451
		 0.33844543 0 0.33844543 0 0.375 0.96344543 0.41820797 0.96344543 0.45988691 0.96344543
		 0.5 0.96344543 0.54011309 0.96344543 0.581792 0.96344543 0.625 0.96344543 0.66155457
		 0 0.66155457 0.038564451 0.66155457 0.125 0.66155457 0.21143556 0.625 0.28655457
		 0.66155457 0.25 0.581792 0.28655457 0.54011309 0.28655457 0.5 0.28655457 0.45988691
		 0.28655457 0.41820797 0.28655457 0.41820797 0.46344543 0.45988691 0.46344543 0.5
		 0.46344543 0.54011309 0.46344543 0.581792 0.46344543 0.625 0.46344543 0.83844543
		 0.25 0.83844543 0.21143556 0.83844543 0.125 0.83844543 0.038564451 0.625 0.78655457
		 0.83844543 0 0.581792 0.78655457 0.54011309 0.78655457 0.5 0.78655457 0.45988691
		 0.78655457 0.41820797 0.78655457 0.16155457 0 0.375 0.78655457 0.16155457 0 0.16155457
		 0.038564451 0.16155457 0.125 0.16155457 0.21143556 0.16155457 0.25 0.16155457 0.25
		 0.375 0.46344543;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 146 ".vt[0:145]"  -2.69624519 -1.88420796 3.0034270287 -0.14889431 -0.98884308 3.0034270287
		 1.72275925 -4.25181627 3.0034270287 -3.54969788 -1.44356298 3.0034270287 0.51172638 -0.2916131 3.0034270287
		 2.59081841 -3.84069347 3.0034270287 -4.40315056 -1.0029178858 3.0034270287 1.17234707 0.40561688 3.0034270287
		 3.45887947 -3.42956996 3.0034270287 -4.40315056 -1.0029178858 0 1.17234707 0.40561688 0
		 3.45887947 -3.42956996 0 -4.40315056 -1.0029178858 -3.0034265518 1.17234707 0.40561688 -3.0034265518
		 3.45887947 -3.42956996 -3.0034265518 -3.54969788 -1.44356298 -3.0034265518 0.51172638 -0.2916131 -3.0034265518
		 2.59081841 -3.84069347 -3.0034265518 -2.69624519 -1.88420796 -3.0034265518 -0.14889431 -0.98884308 -3.0034265518
		 1.72275925 -4.25181627 -3.0034265518 -2.69624519 -1.88420796 0 -0.14889431 -0.98884308 0
		 1.72275925 -4.25181627 0 2.59081936 -3.840693 0 1.43129349 -3.63640451 3.0034270287
		 1.43129349 -3.63640451 0 1.43129349 -3.63640451 -3.0034265518 2.2993536 -3.22528148 -3.0034265518
		 3.16741371 -2.81415868 -3.0034265518 3.16741371 -2.81415868 0 3.16741371 -2.81415868 3.0034270287
		 2.2993536 -3.22528148 3.0034270287 -3.23730278 -0.83850694 3.0034270287 -4.090754509 -0.39786184 3.0034270287
		 -4.090754509 -0.39786184 0 -4.090754509 -0.39786184 -3.0034265518 -3.23730278 -0.83850694 -3.0034265518
		 -2.3838501 -1.27915227 -3.0034265518 -2.3838501 -1.27915227 0 -2.3838501 -1.27915227 3.0034270287
		 0.74840927 -2.19453621 3.0034270287 0.74840927 -2.19453621 0 0.74840927 -2.19453621 -3.0034265518
		 1.61646843 -1.78341365 -3.0034265518 2.48452759 -1.37229133 -3.0034265518 2.48452759 -1.37229133 0
		 2.48452759 -1.37229133 3.0034270287 1.61646843 -1.78341365 3.0034270287 -1.45762444 0 3.0034270287
		 -1.45762444 0.96049428 3.0034270287 -1.45762444 0.96049428 0 -1.45762444 0.96049428 -3.0034265518
		 -1.45762444 0 -3.0034265518 -1.45762444 -0.96049428 -3.0034265518 -1.45762444 -0.96049428 0
		 -1.45762444 -0.96049428 3.0034270287 -2.79562998 -2.629704 -3.0034265518 -2.79562998 -2.629704 0
		 -3.71414852 -2.91054392 0 -3.71414876 -2.91054392 -3.0034265518 -2.79562998 -2.629704 3.0034270287
		 -3.71414948 -2.91054463 3.0034270287 -4.63266802 -3.19138455 0 -4.63266754 -3.19138384 -3.0034265518
		 -4.63266897 -3.19138479 3.0034270287 -2.95954871 -1.74826217 3.0034270287 -3.079007626 -2.71634769 3.0034270287
		 -3.079007149 -2.71634746 0 -3.079007149 -2.71634746 -3.0034265518 -2.95954871 -1.74826217 -3.0034265518
		 -2.64715362 -1.14320636 -3.0034265518 -1.45762444 -0.66416681 -3.0034265518 0.054917492 -0.77373677 -3.0034265518
		 1.016219139 -2.067698479 -3.0034265518 1.69910359 -3.50956678 -3.0034265518 1.99056911 -4.12497854 -3.0034265518
		 1.99056935 -4.12497854 0 1.99056911 -4.12497854 3.0034270287 1.69910359 -3.50956678 3.0034270287
		 1.016219139 -2.067698479 3.0034270287 0.054917492 -0.77373677 3.0034270287 -1.45762444 -0.66416681 3.0034270287
		 -2.64715362 -1.14320636 3.0034270287 -3.82745123 -0.53380775 3.0034270287 -1.45762444 0.66416681 3.0034270287
		 0.9685353 0.19051054 3.0034270287 2.21671772 -1.49912906 3.0034270287 2.89960384 -2.94099641 3.0034270287
		 3.19106913 -3.55640793 3.0034270287 3.19106913 -3.55640793 0 3.19106913 -3.55640793 -3.0034265518
		 2.89960384 -2.94099641 -3.0034265518 2.21671772 -1.49912906 -3.0034265518 0.9685353 0.19051054 -3.0034265518
		 -1.45762444 0.66416681 -3.0034265518 -3.82745123 -0.53380775 -3.0034265518 -4.13984728 -1.1388638 -3.0034265518
		 -4.34928989 -3.10474038 -3.0034265518 -4.34929037 -3.10474086 0 -4.34929132 -3.10474133 3.0034270287
		 -4.13984728 -1.1388638 3.0034270287 -4.40315056 -1.0029178858 2.12511492 -4.6326685 -3.19138479 2.12511492
		 -4.34929085 -3.1047411 2.12511492 -3.714149 -2.9105444 2.12511492 -3.079007626 -2.71634769 2.12511492
		 -2.79562998 -2.629704 2.12511492 -2.69624519 -1.88420796 2.12511492 -2.3838501 -1.27915227 2.12511492
		 -1.45762444 -0.96049428 2.12511492 -0.14889431 -0.98884308 2.12511492 0.74840927 -2.19453621 2.12511492
		 1.43129349 -3.63640451 2.12511492 1.72275925 -4.25181627 2.12511492 1.99056911 -4.12497854 2.12511492
		 2.59081864 -3.84069324 2.12511492 3.19106913 -3.55640793 2.12511492 3.45887947 -3.42956972 2.12511492
		 3.16741371 -2.81415868 2.12511492 2.48452759 -1.37229133 2.12511492 1.17234707 0.40561688 2.12511492
		 -1.45762444 0.96049428 2.12511492 -4.090754509 -0.39786184 2.12511492 -4.090754509 -0.39786184 -2.12511444
		 -1.45762444 0.96049428 -2.12511444 1.17234707 0.40561688 -2.12511444 2.48452759 -1.37229133 -2.12511444
		 3.16741371 -2.81415868 -2.12511444 3.45887947 -3.42956972 -2.12511444 3.19106913 -3.55640793 -2.12511444
		 2.59081864 -3.84069324 -2.12511444 1.99056911 -4.12497854 -2.12511444 1.72275925 -4.25181627 -2.12511444
		 1.43129349 -3.63640451 -2.12511444 0.74840927 -2.19453621 -2.12511444 -0.14889431 -0.98884308 -2.12511444
		 -1.45762444 -0.96049428 -2.12511444 -2.3838501 -1.27915227 -2.12511444 -2.69624519 -1.88420796 -2.12511444
		 -2.79562998 -2.629704 -2.12511444 -3.079007149 -2.71634746 -2.12511444 -3.71414852 -2.91054392 -2.12511444
		 -4.34928989 -3.10474062 -2.12511444 -4.63266754 -3.19138408 -2.12511444 -4.40315056 -1.0029178858 -2.12511444;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  0 40 0 1 41 0 3 33 1 4 48 1 6 34 0 7 47 0 9 35 1 10 46 1
		 12 36 0 13 45 0 15 37 1 16 44 1 18 38 0 19 43 0 21 39 1 22 42 1 0 66 1 1 81 1 2 78 0
		 3 101 1 4 86 1 5 89 0 6 102 1 7 121 1 8 118 0 9 145 1 10 126 1 11 129 0 12 97 1 13 94 1
		 14 91 0 15 70 1 16 73 1 17 76 0 18 139 1 19 136 1 20 133 0 21 108 1 22 111 1 23 114 0
		 17 131 1 24 116 1 23 77 1 24 90 1 25 2 0 26 23 1 25 113 1 27 20 0 26 134 1 28 17 1
		 27 75 1 29 14 0 28 92 1 30 11 1 29 128 1 31 8 0 30 119 1 32 5 1 31 88 1 32 79 1 33 49 1
		 34 50 0 33 84 1 35 51 1 34 123 1 36 52 0 35 124 1 37 53 1 36 96 1 38 54 0 37 71 1
		 39 55 1 38 138 1 40 56 0 39 109 1 40 83 1 41 25 0 42 26 1 41 112 1 43 27 0 42 135 1
		 44 28 1 43 74 1 45 29 0 44 93 1 46 30 1 45 127 1 47 31 0 46 120 1 48 32 1 47 87 1
		 48 80 1 49 4 1 50 7 0 49 85 1 51 10 1 50 122 1 52 13 0 51 125 1 53 16 1 52 95 1 54 19 0
		 53 72 1 55 22 1 54 137 1 56 1 0 55 110 1 56 82 1 18 57 0 21 58 1 57 140 0 58 68 1
		 15 60 1 60 142 1 60 69 0 0 61 0 58 107 0 3 62 1 61 67 0 59 105 1 9 63 1 59 99 1 12 64 0
		 63 144 0 64 98 0 6 65 0 62 100 0 65 103 0 66 3 1 67 62 0 66 67 1 68 59 1 67 106 1
		 69 57 0 68 141 1 70 18 1 69 70 1 71 38 1 70 71 1 72 54 1 71 72 1 73 19 1 72 73 1
		 74 44 1 73 74 1 75 28 1 74 75 1 76 20 0 75 76 1 77 24 1 76 132 1 78 5 0 77 115 1
		 79 25 1 78 79 1 80 41 1 79 80 1 81 4 1 80 81 1 82 49 1 81 82 1 83 33 1 82 83 1 83 66 1
		 84 34 1 85 50 1;
	setAttr ".ed[166:287]" 84 85 1 86 7 1 85 86 1 87 48 1 86 87 1 88 32 1 87 88 1
		 89 8 0 88 89 1 90 11 1 89 117 1 91 17 0 90 130 1 92 29 1 91 92 1 93 45 1 92 93 1
		 94 16 1 93 94 1 95 53 1 94 95 1 96 37 1 95 96 1 97 15 1 96 97 1 98 60 0 97 98 1 99 63 1
		 98 143 1 100 65 0 99 104 1 101 6 1 100 101 1 101 84 1 102 9 1 103 63 0 102 103 1
		 104 100 1 103 104 1 105 62 1 104 105 1 106 68 1 105 106 1 107 61 0 106 107 1 108 0 1
		 107 108 1 109 40 1 108 109 1 110 56 1 109 110 1 111 1 1 110 111 1 112 42 1 111 112 1
		 113 26 1 112 113 1 114 2 0 113 114 1 115 78 1 114 115 1 116 5 1 115 116 1 117 90 1
		 116 117 1 118 11 0 117 118 1 119 31 1 118 119 1 120 47 1 119 120 1 121 10 1 120 121 1
		 122 51 1 121 122 1 123 35 1 122 123 1 123 102 1 124 36 1 125 52 1 124 125 1 126 13 1
		 125 126 1 127 46 1 126 127 1 128 30 1 127 128 1 129 14 0 128 129 1 130 91 1 129 130 1
		 131 24 1 130 131 1 132 77 1 131 132 1 133 23 0 132 133 1 134 27 1 133 134 1 135 43 1
		 134 135 1 136 22 1 135 136 1 137 55 1 136 137 1 138 39 1 137 138 1 139 21 1 138 139 1
		 140 58 0 139 140 1 141 69 1 140 141 1 142 59 1 141 142 1 143 99 1 142 143 1 144 64 0
		 143 144 1 145 12 1 144 145 1 145 124 1;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 0 75 163 -17
		mu 0 4 0 55 102 83
		f 4 153 44 18 154
		mu 0 4 98 39 2 97
		f 4 199 164 -5 -198
		mu 0 4 122 103 49 6
		f 4 174 173 -56 58
		mu 0 4 107 108 8 46
		f 4 4 64 243 -23
		mu 0 4 6 49 148 124
		f 4 55 24 234 233
		mu 0 4 46 8 142 144
		f 4 287 244 -9 -286
		mu 0 4 174 149 51 12
		f 4 254 253 -52 54
		mu 0 4 153 154 14 44
		f 4 8 68 190 -29
		mu 0 4 12 51 116 118
		f 4 51 30 180 179
		mu 0 4 44 14 110 112
		f 4 138 137 -13 -136
		mu 0 4 88 89 53 18
		f 4 148 147 -48 50
		mu 0 4 93 94 20 42
		f 4 12 72 274 -35
		mu 0 4 18 53 165 167
		f 4 47 36 264 263
		mu 0 4 42 20 159 161
		f 4 214 213 -1 -212
		mu 0 4 131 132 56 24
		f 4 224 223 -45 46
		mu 0 4 136 137 26 40
		f 4 -37 -148 150 262
		mu 0 4 160 27 95 158
		f 4 -224 226 225 -19
		mu 0 4 2 138 139 97
		f 4 255 -31 -254 256
		mu 0 4 156 111 31 155
		f 4 176 232 -25 -174
		mu 0 4 108 141 143 8
		f 4 110 278 277 133
		mu 0 4 75 168 169 86
		f 4 209 118 132 210
		mu 0 4 129 78 84 128
		f 4 194 284 283 124
		mu 0 4 119 171 172 81
		f 4 203 195 127 204
		mu 0 4 126 121 82 125
		f 4 222 -47 -77 78
		mu 0 4 135 136 40 58
		f 4 79 -264 266 265
		mu 0 4 60 42 161 162
		f 4 146 -51 -80 82
		mu 0 4 92 93 42 60
		f 4 83 -180 182 181
		mu 0 4 62 44 112 113
		f 4 252 -55 -84 86
		mu 0 4 152 153 44 62
		f 4 87 -234 236 235
		mu 0 4 64 46 144 145
		f 4 172 -59 -88 90
		mu 0 4 106 107 46 64
		f 4 155 76 -154 156
		mu 0 4 99 57 39 98
		f 4 166 165 -62 -165
		mu 0 4 103 104 67 49
		f 4 -65 61 96 242
		mu 0 4 148 49 67 147
		f 4 -245 246 245 -66
		mu 0 4 51 149 150 69
		f 4 -69 65 100 188
		mu 0 4 116 51 69 115
		f 4 -138 140 139 -70
		mu 0 4 53 89 90 71
		f 4 -73 69 104 272
		mu 0 4 165 53 71 164
		f 4 -214 216 215 -74
		mu 0 4 56 132 133 74
		f 4 -76 73 107 162
		mu 0 4 102 55 73 101
		f 4 220 -79 -2 -218
		mu 0 4 134 135 58 25
		f 4 13 -266 268 -36
		mu 0 4 19 60 162 163
		f 4 144 -83 -14 -142
		mu 0 4 91 92 60 19
		f 4 9 -182 184 -30
		mu 0 4 13 62 113 114
		f 4 250 -87 -10 -248
		mu 0 4 151 152 62 13
		f 4 5 -236 238 -24
		mu 0 4 7 64 145 146
		f 4 170 -91 -6 -168
		mu 0 4 105 106 64 7
		f 4 1 -156 158 -18
		mu 0 4 1 57 99 100
		f 4 168 167 -94 -166
		mu 0 4 104 105 7 67
		f 4 -97 93 23 240
		mu 0 4 147 67 7 146
		f 4 -246 248 247 -98
		mu 0 4 69 150 151 13
		f 4 -101 97 29 186
		mu 0 4 115 69 13 114
		f 4 -140 142 141 -102
		mu 0 4 71 90 91 19
		f 4 -105 101 35 270
		mu 0 4 164 71 19 163
		f 4 -216 218 217 -106
		mu 0 4 74 133 134 25
		f 4 -108 105 17 160
		mu 0 4 101 73 1 100
		f 4 34 276 -111 -109
		mu 0 4 33 166 168 75
		f 4 135 108 -134 136
		mu 0 4 87 33 75 86
		f 4 211 115 -210 212
		mu 0 4 130 0 78 129
		f 4 16 130 -119 -116
		mu 0 4 0 83 84 78
		f 4 285 122 -284 286
		mu 0 4 173 37 81 172
		f 4 28 192 -125 -123
		mu 0 4 37 117 119 81
		f 4 197 125 -196 198
		mu 0 4 122 6 82 121
		f 4 22 202 -128 -126
		mu 0 4 6 123 125 82
		f 4 128 117 -130 -131
		mu 0 4 83 3 79 84
		f 4 -133 129 -206 208
		mu 0 4 128 84 79 127
		f 4 -278 280 -114 114
		mu 0 4 86 169 170 77
		f 4 31 -137 -115 -113
		mu 0 4 35 87 86 77
		f 4 10 70 -139 -32
		mu 0 4 15 52 89 88
		f 4 -141 -71 67 102
		mu 0 4 90 89 52 70
		f 4 -143 -103 99 32
		mu 0 4 91 90 70 16
		f 4 11 -144 -145 -33
		mu 0 4 16 61 92 91
		f 4 81 -146 -147 143
		mu 0 4 61 43 93 92
		f 4 49 33 -149 145
		mu 0 4 43 17 94 93
		f 4 -151 -34 40 260
		mu 0 4 158 95 29 157
		f 4 -226 228 227 -152
		mu 0 4 97 139 140 5
		f 4 59 -155 151 -58
		mu 0 4 47 98 97 5
		f 4 91 -157 -60 -90
		mu 0 4 65 99 98 47
		f 4 -159 -92 -4 -158
		mu 0 4 100 99 65 4
		f 4 -160 -161 157 -93
		mu 0 4 66 101 100 4
		f 4 -162 -163 159 -61
		mu 0 4 48 102 101 66
		f 4 -164 161 -3 -129
		mu 0 4 83 102 48 3
		f 4 60 94 -167 -63
		mu 0 4 48 66 104 103
		f 4 92 20 -169 -95
		mu 0 4 66 4 105 104
		f 4 3 -170 -171 -21
		mu 0 4 4 65 106 105
		f 4 89 -172 -173 169
		mu 0 4 65 47 107 106
		f 4 57 21 -175 171
		mu 0 4 47 5 108 107
		f 4 -228 230 -177 -22
		mu 0 4 5 140 141 108
		f 4 -41 -178 -256 258
		mu 0 4 157 29 111 156
		f 4 -181 177 -50 52
		mu 0 4 112 110 17 43
		f 4 -183 -53 -82 84
		mu 0 4 113 112 43 61
		f 4 -185 -85 -12 -184
		mu 0 4 114 113 61 16
		f 4 -186 -187 183 -100
		mu 0 4 70 115 114 16
		f 4 -188 -189 185 -68
		mu 0 4 52 116 115 70
		f 4 -191 187 -11 -190
		mu 0 4 118 116 52 15
		f 4 -193 189 112 -192
		mu 0 4 119 117 35 77
		f 4 113 282 -195 191
		mu 0 4 77 170 171 119
		f 4 205 126 -204 206
		mu 0 4 127 79 121 126
		f 4 19 -199 -127 -118
		mu 0 4 3 122 121 79
		f 4 2 62 -200 -20
		mu 0 4 3 48 103 122
		f 4 200 120 -202 -203
		mu 0 4 123 38 80 125
		f 4 196 -205 201 -194
		mu 0 4 120 126 125 80
		f 4 119 -207 -197 -122
		mu 0 4 36 127 126 120
		f 4 -208 -209 -120 -132
		mu 0 4 85 128 127 36
		f 4 116 -211 207 -112
		mu 0 4 76 129 128 85
		f 4 37 -213 -117 -110
		mu 0 4 34 130 129 76
		f 4 14 74 -215 -38
		mu 0 4 21 54 132 131
		f 4 -217 -75 71 106
		mu 0 4 133 132 54 72
		f 4 -219 -107 103 38
		mu 0 4 134 133 72 22
		f 4 15 -220 -221 -39
		mu 0 4 22 59 135 134
		f 4 77 -222 -223 219
		mu 0 4 59 41 136 135
		f 4 45 39 -225 221
		mu 0 4 41 23 137 136
		f 4 -227 -40 42 152
		mu 0 4 139 138 28 96
		f 4 -229 -153 149 41
		mu 0 4 140 139 96 30
		f 4 -231 -42 43 -230
		mu 0 4 141 140 30 109
		f 4 -233 229 175 -232
		mu 0 4 143 141 109 32
		f 4 -235 231 -54 56
		mu 0 4 144 142 11 45
		f 4 -237 -57 -86 88
		mu 0 4 145 144 45 63
		f 4 -239 -89 -8 -238
		mu 0 4 146 145 63 10
		f 4 -240 -241 237 -96
		mu 0 4 68 147 146 10
		f 4 -242 -243 239 -64
		mu 0 4 50 148 147 68
		f 4 -244 241 -7 -201
		mu 0 4 124 148 50 9
		f 4 -67 63 98 -247
		mu 0 4 149 50 68 150
		f 4 -249 -99 95 26
		mu 0 4 151 150 68 10
		f 4 7 -250 -251 -27
		mu 0 4 10 63 152 151
		f 4 85 -252 -253 249
		mu 0 4 63 45 153 152
		f 4 53 27 -255 251
		mu 0 4 45 11 154 153
		f 4 178 -257 -28 -176
		mu 0 4 109 156 155 32
		f 4 -258 -259 -179 -44
		mu 0 4 30 157 156 109
		f 4 -260 -261 257 -150
		mu 0 4 96 158 157 30
		f 4 -262 -263 259 -43
		mu 0 4 28 160 158 96
		f 4 -265 261 -46 48
		mu 0 4 161 159 23 41
		f 4 -267 -49 -78 80
		mu 0 4 162 161 41 59
		f 4 -269 -81 -16 -268
		mu 0 4 163 162 59 22
		f 4 -270 -271 267 -104
		mu 0 4 72 164 163 22
		f 4 -272 -273 269 -72
		mu 0 4 54 165 164 72
		f 4 -275 271 -15 -274
		mu 0 4 167 165 54 21
		f 4 -277 273 109 -276
		mu 0 4 168 166 34 76
		f 4 -279 275 111 134
		mu 0 4 169 168 76 85
		f 4 -281 -135 131 -280
		mu 0 4 170 169 85 36
		f 4 -283 279 121 -282
		mu 0 4 171 170 36 120
		f 4 -285 281 193 123
		mu 0 4 172 171 120 80
		f 4 25 -287 -124 -121
		mu 0 4 38 173 172 80
		f 4 6 66 -288 -26
		mu 0 4 9 50 149 174;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube7";
	setAttr ".t" -type "double3" 1.5939582136852362 3.782648193850326 -4.9486634903459237 ;
	setAttr ".s" -type "double3" 0.57722481095846279 0.64371105920198868 0.64371105920198868 ;
	setAttr ".rp" -type "double3" 0 3.5166013836860657 0.84773600101470947 ;
	setAttr ".sp" -type "double3" 0 3.5166013836860657 0.84773600101470947 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 40 ".pt";
	setAttr ".pt[52]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[53]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[54]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[55]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[56]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[57]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[58]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[59]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[64]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[65]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[66]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[79]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[80]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[81]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[82]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[95]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[96]" -type "float3" 0.19460107 0 0.31379429 ;
	setAttr ".pt[97]" -type "float3" 0.13784145 0 0.34415036 ;
	setAttr ".pt[98]" -type "float3" 0.14511831 0 0.29358763 ;
	setAttr ".pt[99]" -type "float3" 0.19460107 0 0.27817795 ;
	setAttr ".pt[100]" -type "float3" 0.12306769 0 0.34415036 ;
	setAttr ".pt[101]" -type "float3" 0.1291319 0 0.2935876 ;
	setAttr ".pt[102]" -type "float3" -0.13037887 0 0.34415036 ;
	setAttr ".pt[103]" -type "float3" -0.19460107 0 0.31203952 ;
	setAttr ".pt[104]" -type "float3" -0.19460107 0 0.27817795 ;
	setAttr ".pt[105]" -type "float3" -0.14511836 0 0.29358763 ;
	setAttr ".pt[106]" -type "float3" 0.14511831 0 -0.079839505 ;
	setAttr ".pt[107]" -type "float3" 0.19460107 0 -0.078637004 ;
	setAttr ".pt[108]" -type "float3" 0.1291319 0 -0.079839505 ;
	setAttr ".pt[109]" -type "float3" -0.19460107 0 -0.078637004 ;
	setAttr ".pt[110]" -type "float3" -0.14511836 0 -0.079839505 ;
	setAttr ".pt[111]" -type "float3" 0.15604143 0 -0.31354901 ;
	setAttr ".pt[112]" -type "float3" 0.17168775 0 -0.2703715 ;
	setAttr ".pt[113]" -type "float3" 0.10312125 0 -0.34415033 ;
	setAttr ".pt[114]" -type "float3" -0.17168777 0 -0.26861683 ;
	setAttr ".pt[115]" -type "float3" -0.14857887 0 -0.31354901 ;
	setAttr ".pt[116]" -type "float3" -0.099515408 0 0.34415036 ;
	setAttr ".pt[117]" -type "float3" -0.11331198 0 0.29358763 ;
	setAttr ".pt[118]" -type "float3" -0.11331198 0 -0.079839505 ;
	setAttr ".pt[119]" -type "float3" -0.079568967 0 -0.34415036 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube8";
	setAttr ".t" -type "double3" -2.3458334237147893 3.782648193850326 -4.9486634903459255 ;
	setAttr ".s" -type "double3" 0.57722481095846279 0.64371105920198868 0.64371105920198868 ;
	setAttr ".rp" -type "double3" 0 3.5166013836860657 0.84773600101470947 ;
	setAttr ".sp" -type "double3" 0 3.5166013836860657 0.84773600101470947 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 142 ".uvst[0].uvsp[0:141]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.45833334 0.33333334 0.54166669 0.33333334 0.54166669
		 0.41666669 0.45833334 0.41666669 0.54166669 0.33333334 0.45833334 0.33333334 0.45833334
		 0.41666669 0.54166669 0.41666669 0.54166669 0.33333334 0.45833331 0.33333334 0.45833331
		 0.41666669 0.54166669 0.41666669 0.46292338 0.33333334 0.46292338 0.41666666 0.46292338
		 0.41666666 0.46292335 0.41666666 0.46292338 0.41666666 0.46292338 0.5 0.46292338
		 0.58333331 0.46292338 0.66666663 0.46292338 0.74999994 0.46292338 0.83333319 0.46292338
		 0.91666651 0.46292338 0 0.46292338 0.99999982 0.46292338 0.083333336 0.46292338 0.16666667
		 0.46292338 0.25 0.46292338 0.33333334 0.46292335 0.33333334 0.46292338 0.33333334
		 0.53809762 0.33333334 0.53809762 0.41666669 0.53809762 0.41666669 0.53809762 0.41666669
		 0.53809762 0.41666669 0.53809762 0.5 0.53809762 0.58333331 0.53809762 0.66666663
		 0.53809762 0.74999994 0.53809762 0.83333325 0.53809762 0.91666657 0.53809762 0 0.53809762
		 0.99999988 0.53809762 0.083333336 0.53809762 0.16666667 0.53809762 0.25 0.53809762
		 0.33333334 0.53809762 0.33333334 0.53809762 0.33333334 0.375 0.74999994 0.45833334
		 0.74999994 0.375 0.83333325 0.46292338 0.74999994 0.54166669 0.74999994 0.625 0.74999994
		 0.625 0.83333325 0.375 0.91666657 0.625 0.91666657 0.45833334 0.99999988 0.375 0.99999988
		 0.46292338 0.99999982 0.625 0.99999988 0.54166669 0.99999988 0.53809762 0.74999994
		 0.53809762 0.99999988;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 40 ".pt";
	setAttr ".pt[52]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[53]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[54]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[55]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[56]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[57]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[58]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[59]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[64]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[65]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[66]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[79]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[80]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[81]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[82]" -type "float3" 0 0 -0.91993296 ;
	setAttr ".pt[95]" -type "float3" 0 0 1.0119264 ;
	setAttr ".pt[96]" -type "float3" 0.19460107 0 0.31379429 ;
	setAttr ".pt[97]" -type "float3" 0.13784145 0 0.34415036 ;
	setAttr ".pt[98]" -type "float3" 0.14511831 0 0.29358763 ;
	setAttr ".pt[99]" -type "float3" 0.19460107 0 0.27817795 ;
	setAttr ".pt[100]" -type "float3" 0.12306769 0 0.34415036 ;
	setAttr ".pt[101]" -type "float3" 0.1291319 0 0.2935876 ;
	setAttr ".pt[102]" -type "float3" -0.13037887 0 0.34415036 ;
	setAttr ".pt[103]" -type "float3" -0.19460107 0 0.31203952 ;
	setAttr ".pt[104]" -type "float3" -0.19460107 0 0.27817795 ;
	setAttr ".pt[105]" -type "float3" -0.14511836 0 0.29358763 ;
	setAttr ".pt[106]" -type "float3" 0.14511831 0 -0.079839505 ;
	setAttr ".pt[107]" -type "float3" 0.19460107 0 -0.078637004 ;
	setAttr ".pt[108]" -type "float3" 0.1291319 0 -0.079839505 ;
	setAttr ".pt[109]" -type "float3" -0.19460107 0 -0.078637004 ;
	setAttr ".pt[110]" -type "float3" -0.14511836 0 -0.079839505 ;
	setAttr ".pt[111]" -type "float3" 0.15604143 0 -0.31354901 ;
	setAttr ".pt[112]" -type "float3" 0.17168775 0 -0.2703715 ;
	setAttr ".pt[113]" -type "float3" 0.10312125 0 -0.34415033 ;
	setAttr ".pt[114]" -type "float3" -0.17168777 0 -0.26861683 ;
	setAttr ".pt[115]" -type "float3" -0.14857887 0 -0.31354901 ;
	setAttr ".pt[116]" -type "float3" -0.099515408 0 0.34415036 ;
	setAttr ".pt[117]" -type "float3" -0.11331198 0 0.29358763 ;
	setAttr ".pt[118]" -type "float3" -0.11331198 0 -0.079839505 ;
	setAttr ".pt[119]" -type "float3" -0.079568967 0 -0.34415036 ;
	setAttr -s 120 ".vt[0:119]"  -2.243999 -1.17322862 4.38155508 -2.039498806 -1.17322862 4.94589567
		 1.94196057 -1.17322862 4.94589567 2.24399877 -1.17322862 4.35862064 -2.50310755 -0.39107621 4.38155508
		 -2.039498806 -0.39107621 4.94589567 1.94196057 -0.39107621 4.94589567 2.50310731 -0.39107621 4.35862064
		 -2.50310755 0.39107621 4.38155508 -2.039498806 0.39107621 4.94589567 1.94196057 0.39107621 4.94589567
		 2.50310731 0.39107621 4.35862064 -2.2814734 1.17322862 3.89902759 -1.86368632 1.17322862 4.53402472
		 1.76614833 1.17322862 4.53402472 2.28147292 1.17322862 3.87829185 -2.58595872 1.17322862 1.87554049
		 -1.7585876 1.17322862 1.87401342 1.7585876 1.17322862 1.87401342 2.58595824 1.17322862 1.87554049
		 -2.58595872 1.17322862 -2.78811407 -1.7585876 1.17322862 -2.78138494 1.7585876 1.17322862 -2.78138494
		 2.58595824 1.17322862 -2.78811407 -2.58595872 1.17322862 -3.11224794 -1.80162001 1.17322862 -3.47096801
		 1.70408177 1.17322862 -3.47096801 2.58595824 1.17322862 -3.091512442 -2.83717227 0.39107621 -3.25362897
		 -1.80162001 0.39107621 -3.65038991 1.70408177 0.39107621 -3.65038991 2.83717179 0.39107621 -3.23069429
		 -2.83717227 -0.39107621 -3.25362897 -1.80162001 -0.39107621 -3.65038991 1.70408177 -0.39107621 -3.65038991
		 2.83717179 -0.39107621 -3.23069429 -2.54348302 -1.17322862 -3.25362897 -1.80162001 -1.17322862 -3.65038991
		 1.70408177 -1.17322862 -3.65038991 2.54348254 -1.17322862 -3.23069429 -2.54348302 -1.17322862 -2.78811407
		 2.54348254 -1.17322862 -2.78811407 -2.54348302 -1.17322862 1.87554049 2.54348254 -1.17322862 1.87554049
		 2.83717227 -0.39107621 -2.78811407 2.83717227 -0.39107621 1.87554049 2.83717227 0.39107621 -2.78811407
		 2.83717227 0.39107621 1.87554049 -2.83717227 -0.39107621 -2.78811407 -2.83717227 -0.39107621 1.87554049
		 -2.83717227 0.39107621 -2.78811407 -2.83717227 0.39107621 1.87554049 -2.17861104 8.20643139 2.56731415
		 2.17861104 8.20643139 2.56731415 2.17861104 8.20643139 -3.47468567 -2.17861104 8.20643139 -3.47468567
		 2.15693355 7.84344625 2.53153276 -2.15693355 7.84344625 2.53153276 -2.15693355 7.84344625 -3.43890429
		 2.15693355 7.84344625 -3.43890429 1.77138686 1.38754952 1.89514017 -1.77138686 1.38754952 1.89514017
		 -1.77138686 1.38754952 -2.80251169 1.77138686 1.38754952 -2.80251169 -1.93861294 8.20643139 2.56731415
		 -1.93861294 8.20643139 -3.47468543 -1.91932368 7.84344625 -3.43890429 -1.57624888 1.38754928 -2.80251169
		 -1.56485963 1.17322862 -2.78138494 -1.60852408 1.17322862 -3.47096777 -1.60852408 0.39107621 -3.65038967
		 -1.60852408 -0.39107621 -3.65038967 -1.60852408 -1.17322862 -3.65038967 -1.34781933 -1.17322862 5.34586143
		 -1.34781933 -0.39107621 5.34586143 -1.34781933 0.39107621 5.34586143 -1.34781933 1.17322862 4.82529259
		 -1.56485963 1.17322862 1.87401342 -1.57624888 1.38754952 1.89514017 -1.91932368 7.84344625 2.53153276
		 1.73874974 8.20643139 2.56731415 1.73874974 8.20643139 -3.47468567 1.71892858 7.84344625 -3.43890429
		 1.36640668 1.38754952 -2.80251169 1.35470414 1.17322862 -2.78138494 1.30068946 1.17322862 -3.47096801
		 1.30068946 0.39107621 -3.65038991 1.30068946 -0.39107621 -3.65038991 1.30068946 -1.17322862 -3.65038991
		 1.039984941 -1.17322862 5.34586191 1.039984941 -0.39107621 5.34586191 1.039984941 0.39107621 5.34586191
		 1.039984941 1.17322862 4.82529259 1.35470414 1.17322862 1.87401342 1.36640668 1.38754952 1.89514017
		 1.71892858 7.84344625 2.53153276 -2.54348302 -1.17322862 -3.25362897 -1.80162001 -1.17322862 -3.65038991
		 -1.89673066 -1.17322862 -2.98952198 -2.54348302 -1.17322862 -2.78811407 -1.60852408 -1.17322862 -3.65038967
		 -1.68778491 -1.17322862 -2.98952174 1.70408177 -1.17322862 -3.65038991 2.54348254 -1.17322862 -3.23069429
		 2.54348254 -1.17322862 -2.78811407 1.89673066 -1.17322862 -2.98952198 -1.89673066 -1.17322862 1.89125729
		 -2.54348302 -1.17322862 1.87554049 -1.68778491 -1.17322862 1.89125729 2.54348254 -1.17322862 1.87554049
		 1.89673066 -1.17322862 1.89125729 -2.039498806 -1.17322862 4.94589567 -2.243999 -1.17322862 4.38155508
		 -1.34781933 -1.17322862 5.34586143 2.24399877 -1.17322862 4.35862064 1.94196057 -1.17322862 4.94589567
		 1.30068946 -1.17322862 -3.65038991 1.48101401 -1.17322862 -2.98952198 1.48101401 -1.17322862 1.89125729
		 1.039984941 -1.17322862 5.34586191;
	setAttr -s 236 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 73 0 2 3 0 4 5 1 5 74 1 6 7 1 8 9 1 9 75 1
		 10 11 1 12 13 0 13 76 0 14 15 0 16 17 1 17 77 0 18 19 1 20 21 1 21 68 0 22 23 1 24 25 0
		 25 69 0 26 27 0 28 29 1 29 70 1 30 31 1 32 33 1 33 71 1 34 35 1 36 37 0 37 72 0 38 39 0
		 0 4 0 1 5 1 2 6 1 3 7 0 4 8 0 5 9 1 6 10 1 7 11 0 8 12 0 9 13 1 10 14 1 11 15 0 12 16 0
		 13 17 1 14 18 1 15 19 0 16 20 0 17 21 0 18 22 0 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0
		 24 28 0 25 29 1 26 30 1 27 31 0 28 32 0 29 33 1 30 34 1 31 35 0 32 36 0 33 37 1 34 38 1
		 35 39 0 36 40 0 39 41 0 40 42 0 41 43 0 42 0 0 43 3 0 35 44 1 44 45 1 45 7 1 31 46 1
		 46 47 1 47 11 1 41 44 1 43 45 1 44 46 1 45 47 1 46 23 1 47 19 1 32 48 1 48 49 1 49 4 1
		 28 50 1 50 51 1 51 8 1 40 48 1 42 49 1 48 50 1 49 51 1 50 20 1 51 16 1 17 61 0 18 60 0
		 52 64 0 22 63 0 53 54 0 21 62 0 55 65 0 52 55 0 56 53 0 57 52 0 56 95 1 58 55 0 57 58 1
		 59 54 0 58 66 1 59 56 1 60 56 0 61 57 0 60 94 1 62 58 0 61 62 1 63 59 0 62 67 1 63 60 1
		 64 80 0 65 81 0 64 65 1 66 82 1 65 66 1 67 83 1 66 67 1 68 84 0 67 68 1 69 85 0 68 69 1
		 70 86 1 69 70 1 71 87 1 70 71 1 72 88 0 71 72 1 73 89 0 74 90 1 73 74 1 75 91 1 74 75 1
		 76 92 0 75 76 1 77 93 0 76 77 1 78 61 1 77 78 1 79 57 1 78 79 1 79 64 1 80 53 0 81 54 0
		 80 81 1 82 59 1 81 82 1 83 63 1 82 83 1 84 22 0 83 84 1 85 26 0 84 85 1 86 30 1 85 86 1
		 87 34 1 86 87 1;
	setAttr ".ed[166:235]" 88 38 0 87 88 1 89 2 0 90 6 1 89 90 1 91 10 1 90 91 1
		 92 14 0 91 92 1 93 18 0 92 93 1 94 78 1 93 94 1 95 79 1 94 95 1 95 80 1 36 96 0 37 97 0
		 96 97 0 97 98 1 40 99 0 99 98 1 96 99 0 72 100 0 97 100 0 100 101 1 98 101 1 38 102 0
		 39 103 0 102 103 0 41 104 0 103 104 0 105 104 1 102 105 1 98 106 1 42 107 0 107 106 1
		 99 107 0 101 108 1 106 108 1 43 109 0 104 109 0 110 109 1 105 110 1 1 111 0 106 111 1
		 0 112 0 112 111 0 107 112 0 73 113 0 108 113 1 111 113 0 3 114 0 109 114 0 2 115 0
		 115 114 0 110 115 1 88 116 0 100 116 0 116 117 1 101 117 1 117 118 1 108 118 1 89 119 0
		 118 119 1 113 119 0 116 102 0 117 105 1 118 110 1 119 115 0;
	setAttr -s 118 -ch 472 ".fc[0:117]" -type "polyFaces" 
		f 4 0 31 -4 -31
		mu 0 4 0 1 5 4
		f 4 1 139 -5 -32
		mu 0 4 1 99 101 5
		f 4 2 33 -6 -33
		mu 0 4 2 3 7 6
		f 4 3 35 -7 -35
		mu 0 4 4 5 9 8
		f 4 4 141 -8 -36
		mu 0 4 5 101 102 9
		f 4 5 37 -9 -37
		mu 0 4 6 7 11 10
		f 4 6 39 -10 -39
		mu 0 4 8 9 13 12
		f 4 7 143 -11 -40
		mu 0 4 9 102 103 13
		f 4 8 41 -12 -41
		mu 0 4 10 11 15 14
		f 4 9 43 -13 -43
		mu 0 4 12 13 17 16
		f 4 10 145 -14 -44
		mu 0 4 13 103 104 17
		f 4 11 45 -15 -45
		mu 0 4 14 15 19 18
		f 4 12 47 -16 -47
		mu 0 4 16 17 21 20
		f 4 98 122 -103 -104
		mu 0 4 76 88 89 79
		f 4 14 49 -18 -49
		mu 0 4 18 19 23 22
		f 4 15 51 -19 -51
		mu 0 4 20 21 25 24
		f 4 16 130 -20 -52
		mu 0 4 21 92 93 25
		f 4 17 53 -21 -53
		mu 0 4 22 23 27 26
		f 4 18 55 -22 -55
		mu 0 4 24 25 29 28
		f 4 19 132 -23 -56
		mu 0 4 25 93 94 29
		f 4 20 57 -24 -57
		mu 0 4 26 27 31 30
		f 4 21 59 -25 -59
		mu 0 4 28 29 33 32
		f 4 22 134 -26 -60
		mu 0 4 29 94 95 33
		f 4 23 61 -27 -61
		mu 0 4 30 31 35 34
		f 4 24 63 -28 -63
		mu 0 4 32 33 37 36
		f 4 25 136 -29 -64
		mu 0 4 33 95 96 37
		f 4 26 65 -30 -65
		mu 0 4 34 35 39 38
		f 4 184 185 -188 -189
		mu 0 4 126 127 41 128
		f 4 190 191 -193 -186
		mu 0 4 127 129 97 41
		f 4 195 197 -199 -200
		mu 0 4 130 131 132 42
		f 4 187 200 -203 -204
		mu 0 4 128 41 45 133
		f 4 192 204 -206 -201
		mu 0 4 41 97 98 45
		f 4 198 207 -209 -210
		mu 0 4 42 132 134 46
		f 4 202 211 -214 -215
		mu 0 4 133 45 135 136
		f 4 205 216 -218 -212
		mu 0 4 45 98 137 135
		f 4 208 219 -222 -223
		mu 0 4 46 134 138 139
		f 4 -68 -66 72 -79
		mu 0 4 53 52 55 56
		f 4 -70 78 73 -80
		mu 0 4 54 53 56 57
		f 4 -72 79 74 -34
		mu 0 4 3 54 57 7
		f 4 -73 -62 75 -81
		mu 0 4 56 55 58 59
		f 4 -74 80 76 -82
		mu 0 4 57 56 59 60
		f 4 -75 81 77 -38
		mu 0 4 7 57 60 11
		f 4 -76 -58 -54 -83
		mu 0 4 59 58 61 62
		f 4 -77 82 -50 -84
		mu 0 4 60 59 62 63
		f 4 -78 83 -46 -42
		mu 0 4 11 60 63 15
		f 4 66 90 -85 62
		mu 0 4 64 65 68 67
		f 4 68 91 -86 -91
		mu 0 4 65 66 69 68
		f 4 70 30 -87 -92
		mu 0 4 66 0 4 69
		f 4 84 92 -88 58
		mu 0 4 67 68 71 70
		f 4 85 93 -89 -93
		mu 0 4 68 69 72 71
		f 4 86 34 -90 -94
		mu 0 4 69 4 8 72
		f 4 87 94 50 54
		mu 0 4 70 71 74 73
		f 4 88 95 46 -95
		mu 0 4 71 72 75 74
		f 4 89 38 42 -96
		mu 0 4 72 8 12 75
		f 4 13 147 146 -97
		mu 0 4 17 104 105 85
		f 4 48 99 119 -98
		mu 0 4 18 22 87 84
		f 4 -17 101 118 128
		mu 0 4 92 21 86 91
		f 4 -48 96 116 -102
		mu 0 4 21 17 85 86
		f 4 -149 150 -99 -106
		mu 0 4 81 106 88 76
		f 4 -109 105 103 -108
		mu 0 4 82 81 76 79
		f 4 -111 107 102 124
		mu 0 4 90 82 79 89
		f 4 -112 109 -101 -105
		mu 0 4 80 83 78 77
		f 4 -147 149 148 -114
		mu 0 4 85 105 106 81
		f 4 -117 113 108 -116
		mu 0 4 86 85 81 82
		f 4 -119 115 110 126
		mu 0 4 91 86 82 90
		f 4 -120 117 111 -113
		mu 0 4 84 87 83 80
		f 4 120 153 -122 -123
		mu 0 4 88 107 108 89
		f 4 -124 -125 121 155
		mu 0 4 109 90 89 108
		f 4 -126 -127 123 157
		mu 0 4 110 91 90 109
		f 4 -128 -129 125 159
		mu 0 4 111 92 91 110
		f 4 -131 127 161 -130
		mu 0 4 93 92 111 112
		f 4 -133 129 163 -132
		mu 0 4 94 93 112 113
		f 4 -135 131 165 -134
		mu 0 4 95 94 113 114
		f 4 -137 133 167 -136
		mu 0 4 96 95 114 115
		f 4 -192 224 225 -227
		mu 0 4 97 129 140 116
		f 4 -205 226 227 -229
		mu 0 4 98 97 116 117
		f 4 -217 228 230 -232
		mu 0 4 137 98 117 141
		f 4 -140 137 170 -139
		mu 0 4 101 99 118 120
		f 4 -142 138 172 -141
		mu 0 4 102 101 120 121
		f 4 -144 140 174 -143
		mu 0 4 103 102 121 122
		f 4 -146 142 176 -145
		mu 0 4 104 103 122 123
		f 4 -148 144 178 177
		mu 0 4 105 104 123 124
		f 4 -150 -178 180 179
		mu 0 4 106 105 124 125
		f 4 -151 -180 181 -121
		mu 0 4 88 106 125 107
		f 4 151 100 -153 -154
		mu 0 4 107 77 78 108
		f 4 -155 -156 152 -110
		mu 0 4 83 109 108 78
		f 4 -157 -158 154 -118
		mu 0 4 87 110 109 83
		f 4 -159 -160 156 -100
		mu 0 4 22 111 110 87
		f 4 -162 158 52 -161
		mu 0 4 112 111 22 26
		f 4 -164 160 56 -163
		mu 0 4 113 112 26 30
		f 4 -166 162 60 -165
		mu 0 4 114 113 30 34
		f 4 -168 164 64 -167
		mu 0 4 115 114 34 38
		f 4 -226 232 199 -234
		mu 0 4 116 140 130 42
		f 4 -228 233 209 -235
		mu 0 4 117 116 42 46
		f 4 -231 234 222 -236
		mu 0 4 141 117 46 139
		f 4 -171 168 32 -170
		mu 0 4 120 118 2 6
		f 4 -173 169 36 -172
		mu 0 4 121 120 6 10
		f 4 -175 171 40 -174
		mu 0 4 122 121 10 14
		f 4 -177 173 44 -176
		mu 0 4 123 122 14 18
		f 4 -179 175 97 114
		mu 0 4 124 123 18 84
		f 4 -181 -115 112 106
		mu 0 4 125 124 84 80
		f 4 -182 -107 104 -152
		mu 0 4 107 125 80 77
		f 4 27 183 -185 -183
		mu 0 4 36 37 127 126
		f 4 -67 182 188 -187
		mu 0 4 40 36 126 128
		f 4 28 189 -191 -184
		mu 0 4 37 96 129 127
		f 4 29 194 -196 -194
		mu 0 4 38 39 131 130
		f 4 67 196 -198 -195
		mu 0 4 39 43 132 131
		f 4 -69 186 203 -202
		mu 0 4 44 40 128 133
		f 4 69 206 -208 -197
		mu 0 4 43 47 134 132
		f 4 -1 212 213 -211
		mu 0 4 49 48 136 135
		f 4 -71 201 214 -213
		mu 0 4 48 44 133 136
		f 4 -2 210 217 -216
		mu 0 4 100 49 135 137
		f 4 71 218 -220 -207
		mu 0 4 47 51 138 134
		f 4 -3 220 221 -219
		mu 0 4 51 50 139 138
		f 4 135 223 -225 -190
		mu 0 4 96 115 140 129
		f 4 -138 215 231 -230
		mu 0 4 119 100 137 141
		f 4 166 193 -233 -224
		mu 0 4 115 38 130 140
		f 4 -169 229 235 -221
		mu 0 4 50 119 141 139;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "pCube9";
	setAttr ".t" -type "double3" -6.557478749898058 16.601846567263465 -6.1609810345063174 ;
	setAttr ".s" -type "double3" -0.71227812158379933 0.71227812158379933 0.71227812158379933 ;
	setAttr ".rp" -type "double3" 2.2579140489100467 2.4409881609838395 -1.0909031947257528e-15 ;
	setAttr ".sp" -type "double3" -3.1699893349095403 3.4270154971995117 -1.5315691464733647e-15 ;
	setAttr ".spt" -type "double3" 5.4279033838195856 -0.9860273362156855 4.4066595174761933e-16 ;
createNode transform -n "transform1" -p "pCube9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape9" -p "transform1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:93]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 120 ".uvst[0].uvsp[0:119]" -type "float2" 0.375 0 0.45833334
		 0 0.54166669 0 0.625 0 0.375 0.083333336 0.45833334 0.083333336 0.54166669 0.083333336
		 0.625 0.083333336 0.375 0.16666667 0.45833334 0.16666667 0.54166669 0.16666667 0.625
		 0.16666667 0.375 0.25 0.45833334 0.25 0.54166669 0.25 0.625 0.25 0.375 0.33333334
		 0.45833334 0.33333334 0.54166669 0.33333334 0.625 0.33333334 0.375 0.41666669 0.45833334
		 0.41666669 0.54166669 0.41666669 0.625 0.41666669 0.375 0.5 0.45833334 0.5 0.54166669
		 0.5 0.625 0.5 0.375 0.58333331 0.45833334 0.58333331 0.54166669 0.58333331 0.625
		 0.58333331 0.375 0.66666663 0.45833334 0.66666663 0.54166669 0.66666663 0.625 0.66666663
		 0.375 0.74999994 0.45833334 0.74999994 0.54166669 0.74999994 0.625 0.74999994 0.375
		 0.83333325 0.45833334 0.83333325 0.54166669 0.83333325 0.625 0.83333325 0.375 0.91666657
		 0.45833334 0.91666657 0.54166669 0.91666657 0.625 0.91666657 0.375 0.99999988 0.45833334
		 0.99999988 0.54166669 0.99999988 0.625 0.99999988 0.875 0 0.79166669 0 0.70833337
		 0 0.875 0.083333336 0.79166669 0.083333336 0.70833337 0.083333336 0.875 0.16666667
		 0.79166669 0.16666667 0.70833337 0.16666667 0.875 0.25 0.79166669 0.25 0.70833337
		 0.25 0.125 0 0.20833334 0 0.29166669 0 0.125 0.083333336 0.20833334 0.083333336 0.29166669
		 0.083333336 0.125 0.16666667 0.20833334 0.16666667 0.29166669 0.16666667 0.125 0.25
		 0.20833334 0.25 0.29166669 0.25 0.45833334 0.83333325 0.54166669 0.83333325 0.54166669
		 0.91666657 0.45833334 0.91666657 0.45833334 0.83333325 0.54166669 0.83333325 0.54166669
		 0.91666657 0.45833334 0.91666657 0.54166669 0.91666657 0.54166669 0.83333325 0.45833334
		 0.83333325 0.45833334 0.91666657 0.54166669 0.91666651 0.54166669 0.83333325 0.45833331
		 0.83333325 0.45833331 0.91666651 0.54166669 0.015731644 0.45833334 0.015731644 0.37500003
		 0.015731644 0.29166669 0.015731644 0.20833334 0.015731644 0.125 0.015731644 0.375
		 0.73426831 0.45833334 0.73426831 0.54166669 0.73426831 0.625 0.73426831 0.875 0.015731644
		 0.79166669 0.015731644 0.70833337 0.015731644 0.625 0.015731644 0.625 0.24103205
		 0.54166669 0.24103205 0.45833334 0.24103205 0.375 0.24103205 0.29166669 0.24103205
		 0.20833334 0.24103205 0.125 0.24103205 0.375 0.50896794 0.45833334 0.50896794 0.54166669
		 0.50896794 0.625 0.50896794 0.875 0.24103205 0.79166669 0.24103205 0.70833337 0.24103205;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 96 ".pt[0:95]" -type "float3"  1.7532692 -2.5917194 -0.76562595 
		1.3593372 -2.3323894 -1.0892276 0.77353162 -1.946748 -1.0892276 0.37959766 -1.6874175 
		-0.76562595 1.1895039 -3.4344139 -0.76562595 0.79556996 -3.1750839 -1.0892276 0.20976403 
		-2.7894423 -1.0892276 -0.18416943 -2.5301118 -0.76562595 0.090958357 -4.3014421 -0.76562619 
		-0.34815311 -3.9349151 -1.0892274 -1.0014695 -3.3899555 -1.0892274 -1.4410052 -3.0235467 
		-0.76562619 -1.2571477 -3.9961824 -0.76562625 -2.7404566 -3.2550571 -1.0892273 -4.9428315 
		-2.6470416 -1.0892274 -5.9040198 -1.7552848 -0.76562619 -0.62368882 -4.427361 -0.36307573 
		-2.7404575 -3.2550588 -0.3630757 -4.9428315 -2.6470416 -0.36307573 -6.3721843 -1.3209436 
		-0.36307573 -0.62368882 -4.4273615 0.36307561 -2.7404575 -3.2550595 0.36307558 -4.9428315 
		-2.6470416 0.36307561 -6.3721852 -1.3209436 0.36307561 -1.2571477 -3.996182 0.76562589 
		-2.740458 -3.2550597 1.0892278 -4.9428315 -2.6470418 1.0892274 -5.9040198 -1.7552848 
		0.76562589 0.090958357 -4.3014421 0.76562577 -0.34815311 -3.9349151 1.0892276 -1.0014695 
		-3.3899555 1.0892276 -1.4410052 -3.0235467 0.76562577 1.1895039 -3.4344139 0.76562607 
		0.79556996 -3.1750839 1.0892276 0.20976403 -2.7894423 1.0892276 -0.18416937 -2.5301123 
		0.76562601 1.7532692 -2.5917194 0.76562607 1.3593372 -2.3323894 1.0892276 0.77353162 
		-1.946748 1.0892276 0.37959766 -1.6874175 0.76562607 1.9451451 -2.7180312 0.36307567 
		1.3593372 -2.3323894 0.36307567 0.77353162 -1.946748 0.36307567 0.18772562 -1.5611062 
		0.36307567 1.9451451 -2.7180312 -0.36307579 1.3593372 -2.3323894 -0.36307579 0.77353162 
		-1.946748 -0.36307579 0.18772562 -1.5611062 -0.36307579 -0.37604165 -2.403801 0.36307567 
		-0.37604165 -2.403801 -0.36307579 -1.6551456 -2.845099 0.36307529 -1.6551456 -2.845099 
		-0.36307621 1.3813752 -3.560725 0.36307567 1.3813752 -3.560725 -0.36307579 0.30476737 
		-4.4799871 0.36307529 0.30476737 -4.4799871 -0.36307621 1.3000385 -2.2933533 0.28957018 
		0.83282924 -1.985785 0.28957018 0.83282924 -1.985785 -0.28957039 1.3000385 -2.2933533 
		-0.28957039 1.5705451 -1.7943022 0.2350433 1.1913148 -1.544651 0.2350433 1.1913148 
		-1.544651 -0.23504338 1.5705451 -1.7943022 -0.23504338 1.1239505 -1.5506318 -0.28957039 
		1.1239505 -1.5506318 0.28957018 1.5911589 -1.8581996 0.28957018 1.5911589 -1.8581996 
		-0.28957039 0.84344047 -1.9699237 -0.28957039 0.84344047 -1.9699237 0.28957018 1.3106494 
		-2.277492 0.28957018 1.3106494 -2.277492 -0.28957039 0.66710299 -2.1058316 -1.0892276 
		1.2529082 -2.4914725 -1.0892276 1.6468428 -2.7508025 -0.76562595 1.8387145 -2.877116 
		-0.36307579 1.8387145 -2.877116 0.36307567 1.6468428 -2.750803 0.76562607 1.2529083 
		-2.491473 1.0892276 0.66710299 -2.1058316 1.0892276 0.27316955 -1.8465028 0.76562607 
		0.081297547 -1.7201911 0.36307567 0.081297547 -1.7201911 -0.36307579 0.27316946 -1.8465023 
		-0.76562595 -2.5393264 -3.5029452 -0.76562619 -2.0960233 -3.8680434 -1.0892274 -1.4368021 
		-4.4109707 -1.0892274 -0.9984175 -4.7760706 -0.76562619 -0.35347921 -4.9538965 -0.36307549 
		-0.35347921 -4.9538965 0.36307594 -0.9984175 -4.7760706 0.76562577 -1.4368021 -4.4109707 
		1.0892276 -2.0960233 -3.8680434 1.0892276 -2.5393264 -3.5029452 0.76562577 -2.7552454 
		-3.3251173 0.36307594 -2.7552454 -3.3251173 -0.36307549;
	setAttr -s 96 ".vt[0:95]"  -1.89609432 -6.14870453 1.85174227 -0.80859375 -6.14870453 2.63440418
		 0.8085928 -6.14870453 2.63440418 1.89609337 -6.14870453 1.85174227 -1.89609432 -2.049568176 1.85174227
		 -0.80859375 -2.049568176 2.63440418 0.8085928 -2.049568176 2.63440418 1.89609337 -2.049568176 1.85174227
		 -1.89609432 2.049566269 1.85174227 -0.80859375 2.049566269 2.63440418 0.8085928 2.049566269 2.63440418
		 1.89609337 2.049566269 1.85174227 -1.89609432 6.14870262 1.85174227 -0.80859375 6.14870262 2.63440418
		 0.8085928 6.14870262 2.63440418 1.89609337 6.14870262 1.85174227 -2.4257822 6.14870262 0.87813425
		 -0.80859375 6.14870262 0.87813425 0.8085928 6.14870262 0.87813425 2.4257803 6.14870262 0.87813425
		 -2.4257822 6.14870262 -0.87813473 -0.80859375 6.14870262 -0.87813473 0.8085928 6.14870262 -0.87813473
		 2.4257803 6.14870262 -0.87813473 -1.89609432 6.14870262 -1.85174179 -0.80859375 6.14870262 -2.63440418
		 0.8085928 6.14870262 -2.63440418 1.89609337 6.14870262 -1.85174179 -1.89609432 2.049566269 -1.85174179
		 -0.80859375 2.049566269 -2.63440418 0.8085928 2.049566269 -2.63440418 1.89609337 2.049566269 -1.85174179
		 -1.89609432 -2.049568176 -1.85174179 -0.80859375 -2.049568176 -2.63440418 0.8085928 -2.049568176 -2.63440418
		 1.89609337 -2.049568176 -1.85174179 -1.89609432 -6.14870453 -1.85174179 -0.80859375 -6.14870453 -2.63440418
		 0.8085928 -6.14870453 -2.63440418 1.89609337 -6.14870453 -1.85174179 -2.4257822 -6.14870453 -0.87813377
		 -0.80859375 -6.14870453 -0.87813377 0.8085928 -6.14870453 -0.87813377 2.4257803 -6.14870453 -0.87813377
		 -2.4257822 -6.14870453 0.8781352 -0.80859375 -6.14870453 0.8781352 0.8085928 -6.14870453 0.8781352
		 2.4257803 -6.14870453 0.8781352 2.4257803 -2.049568176 -0.87813377 2.4257803 -2.049568176 0.8781352
		 2.4257803 2.049566269 -0.87813377 2.4257803 2.049566269 0.8781352 -2.4257822 -2.049568176 -0.87813377
		 -2.4257822 -2.049568176 0.8781352 -2.4257822 2.049566269 -0.87813377 -2.4257822 2.049566269 0.8781352
		 -0.64489269 -6.14870453 -0.70035362 0.64489174 -6.14870453 -0.70035362 0.64489174 -6.14870453 0.70035505
		 -0.64489269 -6.14870453 0.70035505 -0.52345759 -8.43538857 -0.56847483 0.52345663 -8.43538857 -0.56847483
		 0.52345663 -8.43538857 0.56847626 -0.52345759 -8.43538857 0.56847626 0.64489174 -8.26543045 0.70035505
		 0.64489174 -8.26543045 -0.70035362 -0.64489269 -8.26543045 -0.70035362 -0.64489269 -8.26543045 0.70035505
		 0.64489168 -6.22585678 0.70035505 0.64489168 -6.22585678 -0.70035362 -0.64489269 -6.22585678 -0.70035362
		 -0.64489269 -6.22585678 0.70035505 0.80859286 -5.37487078 2.63440418 -0.80859375 -5.37487078 2.63440418
		 -1.89609432 -5.37487078 1.85174227 -2.4257822 -5.37487078 0.8781352 -2.4257822 -5.37487078 -0.87813377
		 -1.89609432 -5.3748703 -1.85174179 -0.80859375 -5.3748703 -2.63440418 0.8085928 -5.3748703 -2.63440418
		 1.89609337 -5.3748703 -1.85174179 2.4257803 -5.37487078 -0.87813377 2.4257803 -5.37487078 0.8781352
		 1.89609337 -5.37487078 1.85174227 1.89609337 5.70757198 1.85174227 0.8085928 5.70757198 2.63440418
		 -0.80859375 5.70757198 2.63440418 -1.89609432 5.70757198 1.85174227 -2.4257822 5.70757198 0.87813437
		 -2.4257822 5.70757198 -0.87813461 -1.89609432 5.70757198 -1.85174179 -0.80859375 5.70757198 -2.63440418
		 0.8085928 5.70757198 -2.63440418 1.89609337 5.70757198 -1.85174179 2.4257803 5.70757198 -0.87813461
		 2.4257803 5.70757198 0.87813437;
	setAttr -s 188 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 4 5 1 5 6 1 6 7 1 8 9 1 9 10 1 10 11 1
		 12 13 0 13 14 0 14 15 0 16 17 1 17 18 1 18 19 1 20 21 1 21 22 1 22 23 1 24 25 0 25 26 0
		 26 27 0 28 29 1 29 30 1 30 31 1 32 33 1 33 34 1 34 35 1 36 37 0 37 38 0 38 39 0 40 41 1
		 41 42 0 42 43 1 44 45 1 45 46 0 46 47 1 0 74 0 1 73 1 2 72 1 3 83 0 4 8 0 5 9 1 6 10 1
		 7 11 0 8 87 0 9 86 1 10 85 1 11 84 0 12 16 0 13 17 1 14 18 1 15 19 0 16 20 0 17 21 1
		 18 22 1 19 23 0 20 24 0 21 25 1 22 26 1 23 27 0 24 90 0 25 91 1 26 92 1 27 93 0 28 32 0
		 29 33 1 30 34 1 31 35 0 32 77 0 33 78 1 34 79 1 35 80 0 36 40 0 37 41 1 38 42 1 39 43 0
		 40 44 0 41 45 0 42 46 0 43 47 0 44 0 0 45 1 1 46 2 1 47 3 0 35 48 1 48 49 1 49 7 1
		 31 50 1 50 51 1 51 11 1 43 81 1 47 82 1 48 50 1 49 51 1 50 94 1 51 95 1 32 52 1 52 53 1
		 53 4 1 28 54 1 54 55 1 55 8 1 40 76 1 44 75 1 52 54 1 53 55 1 54 89 1 55 88 1 41 56 0
		 42 57 0 56 57 0 46 58 0 57 58 0 45 59 0 59 58 0 56 59 0 56 70 0 57 69 0 60 61 0 58 68 0
		 61 62 0 59 71 0 63 62 0 60 63 0 64 62 0 65 61 0 64 65 1 66 60 0 65 66 1 67 63 0 66 67 1
		 67 64 1 68 64 0 69 65 0 68 69 1 70 66 0 69 70 1 71 67 0 70 71 1 71 68 1 72 6 1 73 5 1
		 72 73 1 74 4 0 73 74 1 75 53 1 74 75 1 76 52 1 75 76 1 77 36 0 76 77 1 78 37 1 77 78 1
		 79 38 1 78 79 1 80 39 0 79 80 1 81 48 1 80 81 1 82 49 1 81 82 1 83 7 0 82 83 1 83 72 1
		 84 15 0 85 14 1;
	setAttr ".ed[166:187]" 84 85 1 86 13 1 85 86 1 87 12 0 86 87 1 88 16 1 87 88 1
		 89 20 1 88 89 1 90 28 0 89 90 1 91 29 1 90 91 1 92 30 1 91 92 1 93 31 0 92 93 1 94 23 1
		 93 94 1 95 19 1 94 95 1 95 84 1;
	setAttr -s 94 -ch 376 ".fc[0:93]" -type "polyFaces" 
		f 4 0 37 144 -37
		mu 0 4 0 1 93 94
		f 4 1 38 142 -38
		mu 0 4 1 2 92 93
		f 4 2 39 163 -39
		mu 0 4 2 3 105 92
		f 4 3 41 -7 -41
		mu 0 4 4 5 9 8
		f 4 4 42 -8 -42
		mu 0 4 5 6 10 9
		f 4 5 43 -9 -43
		mu 0 4 6 7 11 10
		f 4 6 45 170 -45
		mu 0 4 8 9 108 109
		f 4 7 46 168 -46
		mu 0 4 9 10 107 108
		f 4 8 47 166 -47
		mu 0 4 10 11 106 107
		f 4 9 49 -13 -49
		mu 0 4 12 13 17 16
		f 4 10 50 -14 -50
		mu 0 4 13 14 18 17
		f 4 11 51 -15 -51
		mu 0 4 14 15 19 18
		f 4 12 53 -16 -53
		mu 0 4 16 17 21 20
		f 4 13 54 -17 -54
		mu 0 4 17 18 22 21
		f 4 14 55 -18 -55
		mu 0 4 18 19 23 22
		f 4 15 57 -19 -57
		mu 0 4 20 21 25 24
		f 4 16 58 -20 -58
		mu 0 4 21 22 26 25
		f 4 17 59 -21 -59
		mu 0 4 22 23 27 26
		f 4 178 177 -22 -176
		mu 0 4 113 114 29 28
		f 4 180 179 -23 -178
		mu 0 4 114 115 30 29
		f 4 182 181 -24 -180
		mu 0 4 115 116 31 30
		f 4 21 65 -25 -65
		mu 0 4 28 29 33 32
		f 4 22 66 -26 -66
		mu 0 4 29 30 34 33
		f 4 23 67 -27 -67
		mu 0 4 30 31 35 34
		f 4 152 151 -28 -150
		mu 0 4 98 99 37 36
		f 4 154 153 -29 -152
		mu 0 4 99 100 38 37
		f 4 156 155 -30 -154
		mu 0 4 100 101 39 38
		f 4 27 73 -31 -73
		mu 0 4 36 37 41 40
		f 4 28 74 -32 -74
		mu 0 4 37 38 42 41
		f 4 29 75 -33 -75
		mu 0 4 38 39 43 42
		f 4 30 77 -34 -77
		mu 0 4 40 41 45 44
		f 4 118 120 -123 -124
		mu 0 4 80 81 82 83
		f 4 32 79 -36 -79
		mu 0 4 42 43 47 46
		f 4 33 81 -1 -81
		mu 0 4 44 45 49 48
		f 4 34 82 -2 -82
		mu 0 4 45 46 50 49
		f 4 35 83 -3 -83
		mu 0 4 46 47 51 50
		f 4 -76 -156 158 -91
		mu 0 4 53 52 102 103
		f 4 -80 90 160 -92
		mu 0 4 54 53 103 104
		f 4 -84 91 162 -40
		mu 0 4 3 54 104 105
		f 4 -85 -68 87 -93
		mu 0 4 56 55 58 59
		f 4 -86 92 88 -94
		mu 0 4 57 56 59 60
		f 4 -87 93 89 -44
		mu 0 4 7 57 60 11
		f 4 -88 -182 184 -95
		mu 0 4 59 58 117 118
		f 4 -89 94 186 -96
		mu 0 4 60 59 118 119
		f 4 -90 95 187 -48
		mu 0 4 11 60 119 106
		f 4 72 102 150 149
		mu 0 4 64 65 96 97
		f 4 76 103 148 -103
		mu 0 4 65 66 95 96
		f 4 80 36 146 -104
		mu 0 4 66 0 94 95
		f 4 96 104 -100 64
		mu 0 4 67 68 71 70
		f 4 97 105 -101 -105
		mu 0 4 68 69 72 71
		f 4 98 40 -102 -106
		mu 0 4 69 4 8 72
		f 4 99 106 176 175
		mu 0 4 70 71 111 112
		f 4 100 107 174 -107
		mu 0 4 71 72 110 111
		f 4 101 44 172 -108
		mu 0 4 72 8 109 110
		f 4 31 109 -111 -109
		mu 0 4 41 42 77 76
		f 4 78 111 -113 -110
		mu 0 4 42 46 78 77
		f 4 -35 113 114 -112
		mu 0 4 46 45 79 78
		f 4 -78 108 115 -114
		mu 0 4 45 41 76 79
		f 4 110 117 136 -117
		mu 0 4 76 77 89 90
		f 4 112 119 134 -118
		mu 0 4 77 78 88 89
		f 4 -115 121 139 -120
		mu 0 4 78 79 91 88
		f 4 -116 116 138 -122
		mu 0 4 79 76 90 91
		f 4 -127 124 -121 -126
		mu 0 4 85 84 82 81
		f 4 -129 125 -119 -128
		mu 0 4 86 85 81 80
		f 4 -131 127 123 -130
		mu 0 4 87 86 80 83
		f 4 -132 129 122 -125
		mu 0 4 84 87 83 82
		f 4 -135 132 126 -134
		mu 0 4 89 88 84 85
		f 4 -137 133 128 -136
		mu 0 4 90 89 85 86
		f 4 -139 135 130 -138
		mu 0 4 91 90 86 87
		f 4 -140 137 131 -133
		mu 0 4 88 91 87 84
		f 4 -143 140 -5 -142
		mu 0 4 93 92 6 5
		f 4 -145 141 -4 -144
		mu 0 4 94 93 5 4
		f 4 -147 143 -99 -146
		mu 0 4 95 94 4 69
		f 4 -149 145 -98 -148
		mu 0 4 96 95 69 68
		f 4 -151 147 -97 68
		mu 0 4 97 96 68 67
		f 4 24 69 -153 -69
		mu 0 4 32 33 99 98
		f 4 25 70 -155 -70
		mu 0 4 33 34 100 99
		f 4 26 71 -157 -71
		mu 0 4 34 35 101 100
		f 4 -159 -72 84 -158
		mu 0 4 103 102 55 56
		f 4 -161 157 85 -160
		mu 0 4 104 103 56 57
		f 4 -163 159 86 -162
		mu 0 4 105 104 57 7
		f 4 -164 161 -6 -141
		mu 0 4 92 105 7 6
		f 4 -167 164 -12 -166
		mu 0 4 107 106 15 14
		f 4 -169 165 -11 -168
		mu 0 4 108 107 14 13
		f 4 -171 167 -10 -170
		mu 0 4 109 108 13 12
		f 4 -173 169 48 -172
		mu 0 4 110 109 12 75
		f 4 -175 171 52 -174
		mu 0 4 111 110 75 74
		f 4 -177 173 56 60
		mu 0 4 112 111 74 73
		f 4 18 61 -179 -61
		mu 0 4 24 25 114 113
		f 4 19 62 -181 -62
		mu 0 4 25 26 115 114
		f 4 20 63 -183 -63
		mu 0 4 26 27 116 115
		f 4 -185 -64 -60 -184
		mu 0 4 118 117 61 62
		f 4 -187 183 -56 -186
		mu 0 4 119 118 62 63
		f 4 -188 185 -52 -165
		mu 0 4 106 119 63 15;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode transform -n "polySurface1";
	setAttr ".rp" -type "double3" -4.312412417738102 18.861233774478258 0 ;
	setAttr ".sp" -type "double3" -4.312412417738102 18.861233774478258 0 ;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dsm" 2;
createNode transform -n "polySurface2";
	setAttr ".t" -type "double3" -0.25704712338953151 0 0 ;
	setAttr ".rp" -type "double3" 4.0886642799647674 18.812905378020211 -5.9886846542358398 ;
	setAttr ".sp" -type "double3" 4.0886642799647674 18.812905378020211 -5.9886846542358398 ;
createNode mesh -n "polySurfaceShape2" -p "polySurface2";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dsm" 2;
createNode transform -n "pCube10";
	setAttr ".t" -type "double3" 1.7710027309906784 27.486290336522512 -2.0609871724785886 ;
	setAttr ".r" -type "double3" -21.994462870282049 22.869288639221789 -8.921096545727897 ;
	setAttr ".s" -type "double3" 0.11111298350366663 0.11111298350366663 0.036609880851976515 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 3;
	setAttr -s 6 ".dli[2:5]"  1 2 3 4;
	setAttr -s 5 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 0\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 0\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n"
		+ "                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n"
		+ "                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n"
		+ "            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"perPolygonSort\" \n"
		+ "                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n"
		+ "            -transparencyAlgorithm \"perPolygonSort\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n"
		+ "                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n"
		+ "            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n"
		+ "                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n"
		+ "                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n"
		+ "                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n"
		+ "                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n"
		+ "                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n"
		+ "                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n"
		+ "                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"perPolygonSort\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"perPolygonSort\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube1";
	setAttr ".w" 11.442819847559019;
	setAttr ".h" 12.039273168682419;
	setAttr ".d" 11.0215297732707;
	setAttr ".sw" 3;
	setAttr ".sh" 3;
	setAttr ".sd" 3;
	setAttr ".cuv" 4;
createNode displayLayer -n "head";
	setAttr ".c" 15;
	setAttr ".do" 1;
createNode polyCube -n "polyCube2";
	setAttr ".w" 14.524934405491159;
	setAttr ".h" 13.351292181859431;
	setAttr ".d" 13.018805481247931;
	setAttr ".sw" 3;
	setAttr ".sh" 3;
	setAttr ".sd" 3;
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[44:47]" "e[60:63]" "e[94:95]" "e[106:107]";
	setAttr ".ix" -type "matrix" 0.70327108243586967 0 0 0 0 0.70327108243586967 0 0
		 0 0 0.70327108243586967 0 -0.23962664690467328 14.786818135829167 -5.4553447044513721 1;
	setAttr ".wt" 0.73845601081848145;
	setAttr ".dr" no;
	setAttr ".re" 95;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	setAttr ".uopa" yes;
	setAttr -s 48 ".tk";
	setAttr ".tk[0]" -type "float3" -1.4152938 -1.0293045 0 ;
	setAttr ".tk[1]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[2]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[3]" -type "float3" 1.0293046 -1.0293045 0 ;
	setAttr ".tk[4]" -type "float3" -1.4152938 0 0 ;
	setAttr ".tk[7]" -type "float3" 1.4796253 0 0 ;
	setAttr ".tk[8]" -type "float3" -0.99105507 0 0 ;
	setAttr ".tk[11]" -type "float3" 0.60506594 0 0 ;
	setAttr ".tk[12]" -type "float3" 2.5418639 1.9299459 0 ;
	setAttr ".tk[13]" -type "float3" 0.84728801 1.9299459 0 ;
	setAttr ".tk[14]" -type "float3" -0.84728801 1.9299459 0 ;
	setAttr ".tk[15]" -type "float3" -2.5418639 1.9299459 0 ;
	setAttr ".tk[16]" -type "float3" 2.5418639 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[17]" -type "float3" 0.84728801 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[18]" -type "float3" -0.84728801 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[19]" -type "float3" -2.5418639 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[20]" -type "float3" 2.5418639 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[21]" -type "float3" 0.84728801 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[22]" -type "float3" -0.84728801 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[23]" -type "float3" -2.5418639 1.9299459 -8.8817842e-15 ;
	setAttr ".tk[24]" -type "float3" 2.5418639 1.9299458 0 ;
	setAttr ".tk[25]" -type "float3" 0.84728801 1.9299458 0 ;
	setAttr ".tk[26]" -type "float3" -0.84728801 1.9299458 0 ;
	setAttr ".tk[27]" -type "float3" -2.5418639 1.9299458 0 ;
	setAttr ".tk[28]" -type "float3" -0.99105507 0 0 ;
	setAttr ".tk[31]" -type "float3" 0.60506594 0 0 ;
	setAttr ".tk[32]" -type "float3" -1.4152938 0 0 ;
	setAttr ".tk[35]" -type "float3" 1.4796253 0 0 ;
	setAttr ".tk[36]" -type "float3" -1.4152938 -1.0293045 0 ;
	setAttr ".tk[37]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[38]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[39]" -type "float3" 1.0293046 -1.0293045 0 ;
	setAttr ".tk[40]" -type "float3" -1.4152938 -1.0293045 0 ;
	setAttr ".tk[41]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[42]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[43]" -type "float3" 1.0293046 -1.0293045 0 ;
	setAttr ".tk[44]" -type "float3" -1.4152938 -1.0293045 0 ;
	setAttr ".tk[45]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[46]" -type "float3" 0 -0.25732613 0 ;
	setAttr ".tk[47]" -type "float3" 1.0293046 -1.0293045 0 ;
	setAttr ".tk[48]" -type "float3" 1.4796253 0 0 ;
	setAttr ".tk[49]" -type "float3" 1.4796253 0 0 ;
	setAttr ".tk[50]" -type "float3" 0.60506594 0 0 ;
	setAttr ".tk[51]" -type "float3" 0.60506594 0 0 ;
	setAttr ".tk[52]" -type "float3" -1.4152938 0 0 ;
	setAttr ".tk[53]" -type "float3" -1.4152938 0 0 ;
	setAttr ".tk[54]" -type "float3" -0.99105507 0 0 ;
	setAttr ".tk[55]" -type "float3" -0.99105507 0 0 ;
createNode polySplitRing -n "polySplitRing2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[36:39]" "e[68:71]" "e[90:91]" "e[102:103]";
	setAttr ".ix" -type "matrix" 0.70327108243586967 0 0 0 0 0.70327108243586967 0 0
		 0 0 0.70327108243586967 0 -0.23962664690467328 14.786818135829167 -5.4553447044513721 1;
	setAttr ".wt" 0.3223051130771637;
	setAttr ".dr" no;
	setAttr ".re" 91;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode displayLayer -n "body";
	setAttr ".c" 5;
	setAttr ".do" 2;
createNode displayLayer -n "arms";
	setAttr ".c" 15;
	setAttr ".do" 3;
createNode polyCube -n "polyCube3";
	setAttr ".w" 7.4679432423939716;
	setAttr ".h" 1.9209885707398449;
	setAttr ".d" 6.0068538319835767;
	setAttr ".sw" 2;
	setAttr ".sh" 2;
	setAttr ".sd" 2;
	setAttr ".cuv" 4;
createNode polyDuplicateEdge -n "polyDuplicateEdge1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[17]" "e[20]" "e[23]" "e[26]" "e[29]" "e[32]" "e[35]" "e[38]";
	setAttr ".of" 0.65433627367019653;
createNode polyDuplicateEdge -n "polyDuplicateEdge2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[17]" "e[20]" "e[23]" "e[26]" "e[29]" "e[32]" "e[35]" "e[38]";
	setAttr ".of" 0.49042761325836182;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	setAttr ".ics" -type "componentList" 1 "f[20:23]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 13.379182868152139 0.96049428536992243 -8.7449183028886885 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 9.8294849 -0.48306865 -8.7449179 ;
	setAttr ".rs" 697787817;
	setAttr ".lt" -type "double3" 0.86028162241708916 -2.5267940131417921e-16 2.0246766201768063 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 8.9760323096804591 -0.92371367857355902 -11.748345331544694 ;
	setAttr ".cbx" -type "double3" 10.682937197833535 -0.042423600410412043 -5.7414912742326827 ;
createNode polyTweak -n "polyTweak2";
	setAttr ".uopa" yes;
	setAttr -s 58 ".tk[0:57]" -type "float3"  1.037726045 -0.92371368 0
		 -0.14889389 -0.028348822 0 -2.011212349 -3.29132199 0 0.18427388 -1.44356298 0 0.51172668
		 -0.29161313 0 -1.14315283 -3.84069347 0 -0.66917896 -1.96341217 0 1.17234731 -0.5548774
		 0 -0.27509272 -4.39006424 0 -0.66917896 -1.96341217 0 1.17234731 -0.5548774 0 -0.27509272
		 -4.39006424 0 -0.66917896 -1.96341217 0 1.17234731 -0.5548774 0 -0.27509272 -4.39006424
		 0 0.18427388 -1.44356298 0 0.51172668 -0.29161313 0 -1.14315283 -3.84069347 0 1.037726045
		 -0.92371368 0 -0.14889389 -0.028348822 0 -2.011212349 -3.29132199 0 1.037726045 -0.92371368
		 0 -0.14889389 -0.028348822 0 -2.011212349 -3.29132199 0 -1.14315248 -3.840693 0 0.18427382
		 -1.44356298 0 -1.01197958 -2.67591023 0 -1.01197958 -2.67591023 0 -1.01197958 -2.67591023
		 0 -0.14391968 -3.22528148 0 0.72414005 -3.77465296 0 0.72414005 -3.77465296 0 0.72414005
		 -3.77465296 0 -0.14391968 -3.22528148 0 -0.7940293 -0.83850694 0 -1.64748168 -1.35835612
		 0 -1.64748168 -1.35835612 0 -1.64748168 -1.35835612 0 -0.7940293 -0.83850694 0 0.05942326
		 -0.31865796 0 0.05942326 -0.31865796 0 0.05942326 -0.31865796 0 -0.44983959 -1.23404193
		 0 -0.44983959 -1.23404193 0 -0.44983959 -1.23404193 0 0.41821998 -1.78341365 0 1.28627944
		 -2.33278561 0 1.28627944 -2.33278561 0 1.28627944 -2.33278561 0 0.41821998 -1.78341365
		 0 -0.25937548 0 0 -0.25937548 0 0 -0.25937548 0 0 -0.25937548 0 0 -0.25937548 0 0
		 -0.25937548 0 0 -0.25937548 0 0 -0.25937548 0 0;
createNode polyDuplicateEdge -n "polyDuplicateEdge3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 14 "e[2:3]" "e[10:11]" "e[40:41]" "e[49]" "e[57]" "e[60]" "e[67]" "e[81]" "e[89]" "e[92]" "e[99]" "e[112:113]" "e[117]" "e[119]";
	setAttr ".of" 0.69148439168930054;
createNode polyTweak -n "polyTweak3";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk[57:65]" -type "float3"  0.065066896 1.44821215 0 0.065066896
		 1.44821215 0 -1.0982811e-08 0.72672737 0 -1.9041106e-07 0.72672731 0 0.065066896
		 1.44821215 0 9.2156547e-08 0.72672659 0 -0.065066963 0.005241707 0 -0.065067053 0.0052425414
		 0 -0.065066867 0.0052410513 0;
createNode polyDuplicateEdge -n "polyDuplicateEdge4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 18 "e[6:7]" "e[14:15]" "e[42:43]" "e[45]" "e[53]" "e[63]" "e[71]" "e[77]" "e[85]" "e[95]" "e[103]" "e[109]" "e[111]" "e[120:121]" "e[131]" "e[149]" "e[175]" "e[193]";
	setAttr ".of" 0.70756334066390991;
createNode polyCube -n "polyCube4";
	setAttr ".w" 5.6743445329527873;
	setAttr ".h" 2.3464573556275039;
	setAttr ".d" 7.3007800488918591;
	setAttr ".sw" 3;
	setAttr ".sh" 3;
	setAttr ".sd" 3;
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	setAttr ".ics" -type "componentList" 1 "f[13]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.9811571690856766 1.173228677813752 0.33063772201734309 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.9811571 2.3464572 -0.12304804 ;
	setAttr ".rs" 1450159077;
	setAttr ".lt" -type "double3" -8.8817841970012523e-16 -2.038317334204844e-17 7.0332023481321224 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.2225695702850174 2.3464572992966009 -2.4507472228984284 ;
	setAttr ".cbx" -type "double3" 5.7397447678863358 2.3464572992966009 2.2046511459370208 ;
createNode polyTweak -n "polyTweak4";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk";
	setAttr ".tk[1]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[2]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[5]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[6]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[9]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[10]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[13]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[14]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[16]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[17]" -type "float3" -0.81286347 0 0.65721679 ;
	setAttr ".tk[18]" -type "float3" 0.81286353 0 0.65721679 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[20]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[21]" -type "float3" -0.81286347 0 -1.5645884 ;
	setAttr ".tk[22]" -type "float3" 0.81286353 0 -1.5645884 ;
	setAttr ".tk[23]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[25]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[26]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[29]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[33]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[37]" -type "float3" -0.85589582 0 0 ;
	setAttr ".tk[38]" -type "float3" 0.75835758 0 0 ;
	setAttr ".tk[40]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[41]" -type "float3" -0.95100653 0 -1.7727252 ;
	setAttr ".tk[42]" -type "float3" 0.95100653 0 -1.7727252 ;
	setAttr ".tk[43]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[44]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[45]" -type "float3" -0.95100653 0 0.67446071 ;
	setAttr ".tk[46]" -type "float3" 0.95100653 0 0.67446071 ;
	setAttr ".tk[47]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[48]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[49]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[50]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[51]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[52]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[53]" -type "float3" 0 0 0.6587438 ;
	setAttr ".tk[54]" -type "float3" 0 0 -1.5713176 ;
	setAttr ".tk[55]" -type "float3" 0 0 0.6587438 ;
createNode polySplitRing -n "polySplitRing3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[108:109]" "e[111]" "e[113]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.9811571690856766 1.173228677813752 0.33063772201734309 1;
	setAttr ".wt" 0.94838982820510864;
	setAttr ".dr" no;
	setAttr ".re" 109;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	setAttr ".uopa" yes;
	setAttr -s 32 ".tk";
	setAttr ".tk[0]" -type "float3" 0.29368928 0 -0.39676097 ;
	setAttr ".tk[3]" -type "float3" -0.29368928 0 -0.41969553 ;
	setAttr ".tk[4]" -type "float3" 0 0 -0.39676097 ;
	setAttr ".tk[7]" -type "float3" 0 0 -0.41969553 ;
	setAttr ".tk[8]" -type "float3" 0 0 -0.39676097 ;
	setAttr ".tk[11]" -type "float3" 0 0 -0.41969553 ;
	setAttr ".tk[12]" -type "float3" 0.25121355 0 -0.87928897 ;
	setAttr ".tk[13]" -type "float3" 0 0 -0.52056915 ;
	setAttr ".tk[14]" -type "float3" 0 0 -0.52056915 ;
	setAttr ".tk[15]" -type "float3" -0.25121355 0 -0.90002459 ;
	setAttr ".tk[16]" -type "float3" 0.25121355 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.25121355 0 0 ;
	setAttr ".tk[20]" -type "float3" 0.25121355 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.25121355 0 0 ;
	setAttr ".tk[24]" -type "float3" 0.25121355 0 0.53814191 ;
	setAttr ".tk[25]" -type "float3" 0 0 0.17942195 ;
	setAttr ".tk[26]" -type "float3" 0 0 0.17942195 ;
	setAttr ".tk[27]" -type "float3" -0.25121355 0 0.55887753 ;
	setAttr ".tk[28]" -type "float3" 0 0 0.39676097 ;
	setAttr ".tk[31]" -type "float3" 0 0 0.41969553 ;
	setAttr ".tk[32]" -type "float3" 0 0 0.39676097 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.41969553 ;
	setAttr ".tk[36]" -type "float3" 0.29368928 0 0.39676097 ;
	setAttr ".tk[39]" -type "float3" -0.29368928 0 0.41969553 ;
	setAttr ".tk[40]" -type "float3" 0.29368928 0 0 ;
	setAttr ".tk[43]" -type "float3" -0.29368928 0 0 ;
	setAttr ".tk[44]" -type "float3" 0.29368928 0 0 ;
	setAttr ".tk[47]" -type "float3" -0.29368928 0 0 ;
	setAttr ".tk[56]" -type "float3" -0.42002338 0 0.69330084 ;
	setAttr ".tk[57]" -type "float3" 0.42002338 0 0.69330084 ;
	setAttr ".tk[58]" -type "float3" 0.42002338 0 -0.69330084 ;
	setAttr ".tk[59]" -type "float3" -0.42002338 0 -0.69330084 ;
createNode polySplitRing -n "polySplitRing4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[108:109]" "e[111]" "e[113]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.9811571690856766 1.173228677813752 0.33063772201734309 1;
	setAttr ".wt" 0.032131005078554153;
	setAttr ".re" 109;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 18 "e[1]" "e[4]" "e[7]" "e[10]" "e[13]" "e[16]" "e[19]" "e[22]" "e[25]" "e[28]" "e[31]" "e[34]" "e[110]" "e[114]" "e[118]" "e[122]" "e[126]" "e[130]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.9811571690856766 1.173228677813752 0.33063772201734309 1;
	setAttr ".wt" 0.055080503225326538;
	setAttr ".re" 110;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 17 "e[118]" "e[126]" "e[132:133]" "e[135]" "e[137]" "e[139]" "e[141]" "e[143]" "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.9811571690856766 1.173228677813752 0.33063772201734309 1;
	setAttr ".wt" 0.95467495918273926;
	setAttr ".re" 132;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	setAttr ".ics" -type "componentList" 3 "f[27:35]" "f[74:76]" "f[92:94]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.9811571690856766 1.173228677813752 0.33063772201734309 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 3.9811568 5.6330904e-08 1.1783737 ;
	setAttr ".rs" 485823075;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.4376741502105546 5.633090283119202e-08 -3.3197521877269196 ;
	setAttr ".cbx" -type "double3" 6.5246397111236405 5.633090283119202e-08 5.6764996337910247 ;
createNode polyTweak -n "polyTweak6";
	setAttr ".uopa" yes;
	setAttr -s 38 ".tk";
	setAttr ".tk[0]" -type "float3" 0.29948398 0 1.1279262 ;
	setAttr ".tk[1]" -type "float3" -0.23787877 0 1.2955056 ;
	setAttr ".tk[2]" -type "float3" 0.23787877 0 1.2955056 ;
	setAttr ".tk[3]" -type "float3" -0.29948398 0 1.1279262 ;
	setAttr ".tk[4]" -type "float3" 0.33406463 0 1.1279262 ;
	setAttr ".tk[5]" -type "float3" -0.23787877 0 1.2955056 ;
	setAttr ".tk[6]" -type "float3" 0.23787877 0 1.2955056 ;
	setAttr ".tk[7]" -type "float3" -0.33406463 0 1.1279262 ;
	setAttr ".tk[8]" -type "float3" 0.33406463 0 1.1279262 ;
	setAttr ".tk[9]" -type "float3" -0.23787877 0 1.2955056 ;
	setAttr ".tk[10]" -type "float3" 0.23787877 0 1.2955056 ;
	setAttr ".tk[11]" -type "float3" -0.33406463 0 1.1279262 ;
	setAttr ".tk[12]" -type "float3" 0.30448535 0 1.1279262 ;
	setAttr ".tk[13]" -type "float3" -0.062066454 0 1.4042038 ;
	setAttr ".tk[14]" -type "float3" 0.062066454 0 1.4042038 ;
	setAttr ".tk[15]" -type "float3" -0.30448535 0 1.1279262 ;
	setAttr ".tk[79]" -type "float3" 0.26070482 0 1.6954719 ;
	setAttr ".tk[80]" -type "float3" 0.26070482 0 1.6954719 ;
	setAttr ".tk[81]" -type "float3" 0.26070482 0 1.6954719 ;
	setAttr ".tk[82]" -type "float3" 0.26070482 0 1.6954719 ;
	setAttr ".tk[86]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[87]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[88]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[89]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[90]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[91]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[92]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[93]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[94]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[95]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[96]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[97]" -type "float3" -0.51395309 0 1.6954719 ;
	setAttr ".tk[98]" -type "float3" -0.51395309 0 1.6954719 ;
	setAttr ".tk[99]" -type "float3" -0.51395309 0 1.6954719 ;
	setAttr ".tk[100]" -type "float3" -0.51395309 0 1.6954719 ;
	setAttr ".tk[101]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[102]" -type "float3" -0.25324821 0 0 ;
	setAttr ".tk[103]" -type "float3" -0.25324821 0 0 ;
createNode polyUnite -n "polyUnite1";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:237]";
createNode polyUnite -n "polyUnite2";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:143]";
createNode groupId -n "groupId9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:237]";
createNode displayLayer -n "legs";
	setAttr ".c" 5;
	setAttr ".do" 4;
createNode polyCube -n "polyCube5";
	setAttr ".w" 21.36779037979035;
	setAttr ".h" 21.36779037979035;
	setAttr ".d" 21.36779037979035;
	setAttr ".sw" 3;
	setAttr ".sh" 3;
	setAttr ".sd" 3;
	setAttr ".cuv" 4;
createNode polyDelEdge -n "polyDelEdge1";
	setAttr ".ics" -type "componentList" 8 "e[12:14]" "e[33:35]" "e[91]" "e[93]" "e[95]" "e[103]" "e[105]" "e[107]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak7";
	setAttr ".uopa" yes;
	setAttr -s 48 ".tk";
	setAttr ".tk[0]" -type "float3" 2.3463178 1.7182022 0 ;
	setAttr ".tk[1]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[2]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[3]" -type "float3" -2.3463178 1.7182022 0 ;
	setAttr ".tk[4]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[7]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[8]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[11]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[12]" -type "float3" 1.9964664 -1.7182022 0 ;
	setAttr ".tk[13]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[14]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[15]" -type "float3" -1.9964664 -1.7182022 0 ;
	setAttr ".tk[16]" -type "float3" 1.9964664 -1.7182022 0 ;
	setAttr ".tk[17]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[18]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[19]" -type "float3" -1.9964664 -1.7182022 0 ;
	setAttr ".tk[20]" -type "float3" 1.9964664 -1.7182022 0 ;
	setAttr ".tk[21]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[22]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[23]" -type "float3" -1.9964664 -1.7182022 0 ;
	setAttr ".tk[24]" -type "float3" 1.9964664 -1.7182022 0 ;
	setAttr ".tk[25]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[26]" -type "float3" 0 1.5525501 0 ;
	setAttr ".tk[27]" -type "float3" -1.9964664 -1.7182022 0 ;
	setAttr ".tk[28]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[31]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[32]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[35]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[36]" -type "float3" 2.3463178 1.7182022 0 ;
	setAttr ".tk[37]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[38]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[39]" -type "float3" -2.3463178 1.7182022 0 ;
	setAttr ".tk[40]" -type "float3" 2.3463178 1.7182022 0 ;
	setAttr ".tk[41]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[42]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[43]" -type "float3" -2.3463178 1.7182022 0 ;
	setAttr ".tk[44]" -type "float3" 2.3463178 1.7182022 0 ;
	setAttr ".tk[45]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[46]" -type "float3" 0 -1.5525501 0 ;
	setAttr ".tk[47]" -type "float3" -2.3463178 1.7182022 0 ;
	setAttr ".tk[48]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[49]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[50]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[51]" -type "float3" 1.1380942 0 0 ;
	setAttr ".tk[52]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[53]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[54]" -type "float3" -1.1380942 0 0 ;
	setAttr ".tk[55]" -type "float3" -1.1380942 0 0 ;
createNode polySplitRing -n "polySplitRing7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[46:49]" "e[62:65]" "e[70]" "e[72]" "e[77]" "e[79]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.32948337536781591 0 31.228960244397783 10.683895189895175 -19.185652343881188 1;
	setAttr ".wt" 0.84517276287078857;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	setAttr ".uopa" yes;
	setAttr -s 44 ".tk[0:43]" -type "float3"  -2.098216772 -2.25628424 -6.015178204
		 -0.8962276 -3.079395056 -6.015176773 0.8962276 -3.079395056 -6.015176773 2.098216772
		 -2.25628424 -6.015178204 -2.97509336 -0.89622748 -6.015176773 -0.89622778 -0.89622766
		 -12.860919 0.89622778 -0.89622766 -12.860919 2.97509336 -0.89622748 -6.015176773
		 -2.97509336 0.89622778 -6.015176773 -0.89622778 0.89622766 -12.860919 0.89622778
		 0.89622766 -12.860919 2.97509336 0.89622778 -6.015176773 -2.18625808 2.25628424 -6.015178204
		 -0.8962276 3.079395056 -6.015176773 0.8962276 3.079395056 -6.015176773 2.18625808
		 2.25628424 -6.015178204 -0.87652713 0.90460283 0.26128298 -0.35932112 1.23460925
		 0.26128298 0.35932112 1.23460925 0.26128298 0.87652713 0.90460283 0.26128298 0.4802531
		 -0.49563581 0 0.19687337 -0.67644799 0 -0.19687337 -0.67644799 0 -0.4802531 -0.49563581
		 0 0.65353549 -0.1968734 0 -0.31052798 0.31052798 -6.32798243 0.31052798 0.31052798
		 -6.32798243 -0.65353549 -0.1968734 0 0.65353549 0.1968734 0 -0.31052801 -0.31052801
		 -6.32798243 0.31052801 -0.31052801 -6.32798243 -0.65353549 0.1968734 0 0.46091294
		 0.49563581 0 0.19687337 0.67644763 0 -0.19687337 0.67644763 0 -0.46091294 0.49563581
		 0 -0.84122884 -0.90460336 0.26128298 -0.35932112 -1.23460937 0.26128298 0.35932112
		 -1.23460937 0.26128298 0.84122872 -0.904603 0.26128298 1.19279182 -0.359321 0.26128298
		 1.19279182 0.35932112 0.26128298 -1.19279182 -0.359321 0.26128298 -1.19279182 0.35932112
		 0.26128298;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	setAttr ".ics" -type "componentList" 1 "f[0:8]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.32948337536781591 0 31.228960244397783 10.683895189895175 -19.185652343881188 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 31.22896 10.683895 -18.775166 ;
	setAttr ".rs" 2074006964;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 16.431877343396806 -4.6319455312351963 -19.902945522635296 ;
	setAttr ".cbx" -type "double3" 46.026043145398759 25.999735911025546 -17.647387267231146 ;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	setAttr ".ics" -type "componentList" 1 "f[0:8]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.32948337536781591 0 31.228960244397783 10.683895189895175 -19.185652343881188 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 31.22896 10.683896 -18.775166 ;
	setAttr ".rs" 1467104308;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 17.42392846796956 -3.6051148580174228 -19.827334136558434 ;
	setAttr ".cbx" -type "double3" 45.033992020826005 24.972906191482089 -17.722997042931361 ;
createNode polyTweak -n "polyTweak9";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[52:67]" -type "float3"  0.6996541 0.75236273 -0.22948182
		 0.2988489 1.026830435 -0.22948182 0.2988489 0.29884896 0.2294818 0.99205077 0.29884896
		 -0.22948182 -0.2988489 1.026830435 -0.22948182 -0.2988489 0.29884896 0.2294818 -0.69965428
		 0.75236273 -0.22948182 -0.99205077 0.29884896 -0.22948182 0.2988489 -0.29884893 0.2294818
		 0.99205077 -0.29884893 -0.22948182 -0.2988489 -0.29884893 0.2294818 -0.99205077 -0.29884893
		 -0.22948182 0.2988489 -1.026830435 -0.22948182 0.72901195 -0.75236273 -0.22948182
		 -0.2988489 -1.026830435 -0.22948182 -0.72901195 -0.75236273 -0.22948182;
createNode polyDelEdge -n "polyDelEdge2";
	setAttr ".ics" -type "componentList" 11 "e[74]" "e[76]" "e[78]" "e[80]" "e[82]" "e[84]" "e[86]" "e[88]" "e[90]" "e[92]" "e[94:95]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak10";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk";
	setAttr ".tk[64]" -type "float3" 4.3240199 4.6497693 -10.30507 ;
	setAttr ".tk[65]" -type "float3" 1.8469534 6.3460431 -10.30507 ;
	setAttr ".tk[66]" -type "float3" 1.8469534 1.846954 -7.4685707 ;
	setAttr ".tk[67]" -type "float3" 6.1310968 1.8469539 -10.30507 ;
	setAttr ".tk[68]" -type "float3" -1.8469527 6.3460431 -10.30507 ;
	setAttr ".tk[69]" -type "float3" -1.8469527 1.846954 -7.4685707 ;
	setAttr ".tk[70]" -type "float3" -4.3240204 4.6497693 -10.30507 ;
	setAttr ".tk[71]" -type "float3" -6.1310968 1.8469539 -10.30507 ;
	setAttr ".tk[72]" -type "float3" 1.8469534 -1.8469536 -7.4685707 ;
	setAttr ".tk[73]" -type "float3" 6.1310968 -1.8469536 -10.30507 ;
	setAttr ".tk[74]" -type "float3" -1.8469527 -1.8469536 -7.4685707 ;
	setAttr ".tk[75]" -type "float3" -6.1310968 -1.8469536 -10.30507 ;
	setAttr ".tk[76]" -type "float3" 1.8469534 -6.3460431 -10.30507 ;
	setAttr ".tk[77]" -type "float3" 4.5054574 -4.6497688 -10.30507 ;
	setAttr ".tk[78]" -type "float3" -1.8469527 -6.3460431 -10.30507 ;
	setAttr ".tk[79]" -type "float3" -4.5054574 -4.6497688 -10.30507 ;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.85827883142482431 0 0 0 0 0.85827883142482431 0 0
		 0 0 0.85827883142482431 0 -0.36614374183070919 23.571017086221776 -5.8965527181719217 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.36614385 23.67132 -0.99582291 ;
	setAttr ".rs" 759382087;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.603421593311354 22.349136385827872 -1.1845202782462865 ;
	setAttr ".cbx" -type "double3" 0.87113390502031618 24.993503159980609 -0.80712555440704215 ;
createNode polyTweak -n "polyTweak11";
	setAttr ".uopa" yes;
	setAttr -s 56 ".tk[0:55]" -type "float3"  3.67755389 2.79750419 -0.77473176
		 0.6553179 1.98714685 0.099251211 -0.65531772 1.98714685 0.099251211 -3.67755389 2.79750419
		 -0.77473176 2.33330083 0.58290482 -0.45494193 0.46555635 0.58290482 0.41904104 -0.46555632
		 0.58290482 0.41904104 -2.33330083 0.58290482 -0.45494193 2.33330083 -0.39592212 -1.01457417
		 0.46555635 -0.34917492 -0.020669986 -0.46555632 -0.34917492 -0.020669986 -2.33330083
		 -0.39592212 -1.01457417 3.19379067 -1.79580617 -1.58665705 0.8316806 -1.47554469
		 -0.8358041 -0.83168042 -1.47554469 -0.8358041 -3.19379067 -1.79580617 -1.58665705
		 1.45703459 -0.83667827 -0.19743173 0.48567873 0.12693109 -0.19743173 -0.48567849
		 0.12693109 -0.19743173 -1.45703459 -0.83667827 -0.19743173 1.45703471 -0.83667827
		 0.32015678 0.48567873 0.12693109 0.32015678 -0.48567849 0.12693109 0.32015678 -1.45703471
		 -0.83667827 0.32015678 2.38059258 -1.58969367 2.69932652 0.48567873 -1.16639662 1.70690596
		 -0.48567849 -1.16639662 1.70690596 -2.38059258 -1.58969367 2.69932652 1.24326038
		 -0.0013005883 1.1551652 0.0017631426 0.060486436 0 -0.0017631352 0.060486436 0 -1.24326038
		 -0.0013005883 1.1551652 1.24326038 0.0013260096 1.1551652 0.0017631426 0.0013260096
		 0 -0.0017631352 0.0013260096 0 -1.24326038 0.0013260096 1.1551652 3.019994259 2.62900019
		 2.25570178 0.25257602 1.81864238 1.10053658 -0.25257599 1.81864238 1.10053658 -3.019994259
		 2.62900019 2.25570178 2.27319551 3.23902154 0.82443935 0.25257602 2.42866349 0.82443923
		 -0.25257599 2.42866349 0.82443923 -2.27319574 3.23902202 0.82443929 2.27319551 0.5761652
		 2.43839765 0.25257602 -0.23419257 2.43839765 -0.25257599 -0.23419257 2.43839765 -2.27319551
		 0.5761652 2.43839765 -0.0052895546 0.0013256073 0 -0.72172427 0.0013260096 6.5725203e-14
		 -0.72172427 -0.0013005883 7.5495166e-14 -0.72172427 -0.0013005883 7.4606987e-14 0.72172427
		 0.0013256073 5.9604645e-08 0.72172427 0.0013260096 6.5725203e-14 0.72172427 -0.0013005883
		 7.5495166e-14 0.72172427 -0.0013005883 7.4606987e-14;
createNode polySplitRing -n "polySplitRing8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[108:109]" "e[111]" "e[113]";
	setAttr ".ix" -type "matrix" 0.85827883142482431 0 0 0 0 0.85827883142482431 0 0
		 0 0 0.85827883142482431 0 -0.36614374183070919 23.571017086221776 -5.8965527181719217 1;
	setAttr ".wt" 0.61897069215774536;
	setAttr ".dr" no;
	setAttr ".re" 111;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak12";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[56:59]" -type "float3"  0.73794222 -0.15262306 1.17658305
		 -0.73794222 -0.15262306 1.17658305 -0.73794222 -1.72978711 1.56617498 0.73794222
		 -1.72978711 1.56617498;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	setAttr ".ics" -type "componentList" 2 "f[54]" "f[59]";
	setAttr ".ix" -type "matrix" 0.85827883142482431 0 0 0 0 0.85827883142482431 0 0
		 0 0 0.85827883142482431 0 -0.36614374183070919 23.571017086221776 -5.8965527181719217 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.36614379 22.090328 -0.3022075 ;
	setAttr ".rs" 1017801475;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.6034214909965443 22.078878875646289 -0.80712555440704215 ;
	setAttr ".cbx" -type "double3" 0.87113390502031618 22.101777850897292 0.20271056763480555 ;
createNode polyTweak -n "polyTweak13";
	setAttr ".uopa" yes;
	setAttr -s 11 ".tk";
	setAttr ".tk[5]" -type "float3" 0 -0.28820252 -1.4901161e-08 ;
	setAttr ".tk[6]" -type "float3" 0 -0.28820252 -1.4901161e-08 ;
	setAttr ".tk[56]" -type "float3" 0 -0.16225958 0 ;
	setAttr ".tk[57]" -type "float3" 0 -0.16225958 0 ;
	setAttr ".tk[58]" -type "float3" 0 0 0.10939008 ;
	setAttr ".tk[59]" -type "float3" 0 0 0.10939008 ;
	setAttr ".tk[60]" -type "float3" 0 -0.30297652 0 ;
	setAttr ".tk[61]" -type "float3" 0 -0.2061938 2.2351742e-08 ;
	setAttr ".tk[62]" -type "float3" 0 -0.2061938 2.2351742e-08 ;
	setAttr ".tk[63]" -type "float3" 0 -0.30297652 0 ;
createNode polySplitRing -n "polySplitRing9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[108:109]" "e[111]" "e[113]" "e[127]" "e[130]";
	setAttr ".ix" -type "matrix" 0.85827883142482431 0 0 0 0 0.85827883142482431 0 0
		 0 0 0.85827883142482431 0 -0.36614374183070919 23.571017086221776 -5.8965527181719217 1;
	setAttr ".wt" 0.39023658633232117;
	setAttr ".dr" no;
	setAttr ".re" 111;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak14";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[61]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".tk[62]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".tk[64]" -type "float3" 0.21975273 -0.0020333882 0.089678444 ;
	setAttr ".tk[65]" -type "float3" -0.21975273 -0.0020333882 0.089678444 ;
	setAttr ".tk[66]" -type "float3" -0.1501241 -0.00013447416 -0.021338185 ;
	setAttr ".tk[67]" -type "float3" 0.15012413 -0.00013447416 -0.021338185 ;
	setAttr ".tk[68]" -type "float3" -0.10726172 0.0020333882 -0.089678444 ;
	setAttr ".tk[69]" -type "float3" 0.10726172 0.0020333882 -0.089678444 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr -s 15 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 10 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr ":frontShape.msg" "frontIMG_PLNShape.ltc";
connectAttr ":sideShape.msg" "sideIMG_PLNShape.ltc";
connectAttr "head.di" "pCube1.do";
connectAttr "polySplitRing9.out" "pCubeShape1.i";
connectAttr "body.di" "pCube2.do";
connectAttr "polySplitRing2.out" "pCubeShape2.i";
connectAttr "arms.di" "pCube3.do";
connectAttr "groupId6.id" "pCubeShape3.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupId7.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "arms.di" "pCube5.do";
connectAttr "groupId8.id" "pCubeShape4.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape4.iog.og[1].gco";
connectAttr "groupParts2.og" "pCubeShape4.i";
connectAttr "groupId9.id" "pCubeShape4.ciog.cog[1].cgid";
connectAttr "arms.di" "pCube6.do";
connectAttr "groupId1.id" "pCubeShape6.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape6.iog.og[1].gco";
connectAttr "groupId2.id" "pCubeShape6.ciog.cog[1].cgid";
connectAttr "legs.di" "pCube7.do";
connectAttr "polyExtrudeFace3.out" "pCubeShape7.i";
connectAttr "legs.di" "pCube8.do";
connectAttr "arms.di" "pCube9.do";
connectAttr "groupId3.id" "pCubeShape9.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape9.iog.og[0].gco";
connectAttr "groupId4.id" "pCubeShape9.ciog.cog[0].cgid";
connectAttr "arms.di" "polySurface1.do";
connectAttr "groupParts1.og" "polySurfaceShape1.i";
connectAttr "groupId5.id" "polySurfaceShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape1.iog.og[0].gco";
connectAttr "arms.di" "polySurface2.do";
connectAttr "groupParts3.og" "polySurfaceShape2.i";
connectAttr "groupId10.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "polyDelEdge2.out" "pCubeShape10.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "layerManager.dli[2]" "head.id";
connectAttr "polyTweak1.out" "polySplitRing1.ip";
connectAttr "pCubeShape2.wm" "polySplitRing1.mp";
connectAttr "polyCube2.out" "polyTweak1.ip";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCubeShape2.wm" "polySplitRing2.mp";
connectAttr "layerManager.dli[3]" "body.id";
connectAttr "layerManager.dli[4]" "arms.id";
connectAttr "polyCube3.out" "polyDuplicateEdge1.ip";
connectAttr "polyDuplicateEdge1.out" "polyDuplicateEdge2.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape4.wm" "polyExtrudeFace1.mp";
connectAttr "polyDuplicateEdge2.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyDuplicateEdge3.ip";
connectAttr "polyExtrudeFace1.out" "polyTweak3.ip";
connectAttr "polyDuplicateEdge3.out" "polyDuplicateEdge4.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace2.mp";
connectAttr "polyCube4.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing3.ip";
connectAttr "pCubeShape7.wm" "polySplitRing3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak5.ip";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape7.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCubeShape7.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape7.wm" "polySplitRing6.mp";
connectAttr "polyTweak6.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace3.mp";
connectAttr "polySplitRing6.out" "polyTweak6.ip";
connectAttr "pCubeShape6.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape9.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape6.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape9.wm" "polyUnite1.im[1]";
connectAttr "polyUnite1.out" "groupParts1.ig";
connectAttr "groupId5.id" "groupParts1.gi";
connectAttr "pCubeShape3.o" "polyUnite2.ip[0]";
connectAttr "pCubeShape4.o" "polyUnite2.ip[1]";
connectAttr "pCubeShape3.wm" "polyUnite2.im[0]";
connectAttr "pCubeShape4.wm" "polyUnite2.im[1]";
connectAttr "polyDuplicateEdge4.out" "groupParts2.ig";
connectAttr "groupId8.id" "groupParts2.gi";
connectAttr "polyUnite2.out" "groupParts3.ig";
connectAttr "groupId10.id" "groupParts3.gi";
connectAttr "layerManager.dli[5]" "legs.id";
connectAttr "polyTweak7.out" "polyDelEdge1.ip";
connectAttr "polyCube5.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySplitRing7.ip";
connectAttr "pCubeShape10.wm" "polySplitRing7.mp";
connectAttr "polyDelEdge1.out" "polyTweak8.ip";
connectAttr "polySplitRing7.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape10.wm" "polyExtrudeFace4.mp";
connectAttr "polyTweak9.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape10.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyDelEdge2.ip";
connectAttr "polyExtrudeFace5.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyCube1.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polySplitRing8.out" "polyTweak13.ip";
connectAttr "polyTweak14.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak14.ip";
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.ciog.cog[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.ciog.cog[1]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of drPhilGoode_001.ma
