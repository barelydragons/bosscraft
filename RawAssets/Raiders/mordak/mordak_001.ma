//Maya ASCII 2014 scene
//Name: mordak_001.ma
//Last modified: Sun, Jul 20, 2014 07:03:43 PM
//Codeset: UTF-8
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.4 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010035-864206";
fileInfo "osv" "Mac OS X 10.9.4";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -20.792261424871626 26.003557747633792 28.20378527200501 ;
	setAttr ".r" -type "double3" -23.13835272940419 -398.59999999994733 -2.0348505213288364e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 38.690200410712151;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.9993581829660569 7.2798798567542455 -1.8895035617911402 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.0809334352585791 6.686564067900358 100.65364374326468 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 11.520765048367506;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.92564162781014 6.6892751827859449 1.5054244169989914 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 26.086816154876111;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "root";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0 5.4491582185681553 -0.33987859393488407 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 1.0000000000000002 0 0 -1.0000000000000002 2.2204460492503131e-16 0 0
		 0 0 1 0 0 5.4491582185681553 -0.33987859393488407 1;
	setAttr ".radi" 0.55293997215734669;
createNode joint -n "spine1" -p "root";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 2.0235061283753679 4.4930861883848816e-16 0 ;
	setAttr ".r" -type "double3" 19.846130434917857 -20.838831520576019 -7.3166356220315878 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.11902179966517203 0.92697494311205531 0.3557404475834397 0
		 -0.94832950200624166 2.3138813132344925e-16 0.31728718162666786 0 0.29411726713856468 -0.37512325286817727 0.87907768617371917 0
		 0 7.4726643469435237 -0.33987859393488407 1;
	setAttr ".radi" 0.54911079739801572;
createNode joint -n "neck" -p "spine1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.0991336804737264 0.11516769444217495 -0.55144286349475469 ;
	setAttr ".r" -type "double3" 0 -23.01783542374967 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 1.7714697400342121 ;
	setAttr ".bps" -type "matrix" -0.03249346812559066 0.9994442080869167 -0.0074464387923102726 0
		 0.9515556011638947 0.028655626416012183 -0.3061385192495793 0 -0.30575498756813496 -0.017033202758798605 -0.95195785493948581 0
		 -0.0215631219239375 9.62536771171035 -0.041351722219667653 1;
	setAttr ".radi" 0.57214504584342329;
createNode joint -n "head" -p "neck";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.6701197483486727 -0.022412921792497549 -0.16109101394102229 ;
	setAttr ".r" -type "double3" 0 -17.184025147914209 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 0.74843955136625628 ;
	setAttr ".bps" -type "matrix" -0.10949838929879629 0.95007361112851152 -0.29218185463542989 0
		 0.95189885943234454 0.01559807864630324 -0.30601513255710955 0 -0.28617942650087702 -0.31163573829035401 -0.90608195129792479 0
		 -0.047903865038048415 11.29666086059831 0.10642514806935952 1;
	setAttr ".radi" 0.5197651863548961;
createNode joint -n "hat1" -p "head";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.2754023665843861 0.11119272452583498 -0.026054209310318207 ;
	setAttr ".r" -type "double3" 0.16949275203881933 1.9395413166763229 -0.043621739387349527 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 -87.070095359390521 -178.97696981133203 ;
	setAttr ".bps" -type "matrix" -0.31311346415092162 -0.3602545576110941 -0.87873580346304569 0
		 -0.10441438992348301 0.93271114505762664 -0.34517757033474605 0 0.94395847034936708 -0.016327081972195095 -0.32966017753128252 0
		 -0.074257963555233758 12.518240808277032 -0.27664368832942476 1;
	setAttr ".radi" 0.5;
createNode joint -n "hat2" -p "hat1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0.097993152295449193 0.40518695407372785 0.11703644683499803 ;
	setAttr ".r" -type "double3" -33.284476829269053 -15.495382290736307 33.246318565017191 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -41.174921649179581 ;
	setAttr ".bps" -type "matrix" -0.032780148209784254 -0.47218419311516713 -0.88089020295126641 0
		 -0.66512523436318538 0.66816760856773871 -0.33340736265743631 0 0.74601198683175507 0.57497315992405118 -0.33596425534614432 0
		 -0.036770742218603547 12.858949852783228 -0.54119748392540457 1;
	setAttr ".radi" 0.5;
createNode joint -n "hat3" -p "hat2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.55223104303847681 4.4408920985006262e-16 -4.6808343636699594e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 44.104826289789081 -90 0 ;
	setAttr ".bps" -type "matrix" 0.74601198683175507 0.57497315992405118 -0.33596425534614432 0
		 -0.45479082334673576 0.80841714547943333 0.37366699866924791 0 0.48644775927424716 -0.12596659978675828 0.86458139769210107 0
		 -0.054872957655449209 12.598195083312957 -1.027652399503558 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_shoulder1" -p "spine1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.99305798714378879 0.48762142368715317 -0.5283782759173753 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 54.637538112930933 ;
	setAttr ".bps" -type "matrix" 0.7044859742513383 -0.53648397290854444 -0.46463368248051884 0
		 -0.64590598793793919 -0.75595468909840269 -0.1064798702856681 0 -0.29411726713856479 0.37512325286817733 -0.87907768617371906 0
		 -0.49963540761929043 8.5914111956901138 -0.29637722610746797 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_shoulder2" -p "R_shoulder1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -0.86815636083539294 0.26164768849594733 0.41102852761173653 ;
	setAttr ".r" -type "double3" -17.707386107146338 12.462162595427692 13.9783293739111 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -35.768808120378139 ;
	setAttr ".bps" -type "matrix" 0.93632490307378102 -0.29335462258927819 -0.19297342119933836 0
		 -0.29353742080764461 -0.95552841923980569 0.028305875903679251 0 -0.19269524765668364 0.030141423831350045 -0.98079561382581049 0
		 -1.4011297832717138 9.0135557305459724 -0.28218875813954258 1;
	setAttr ".radi" 0.54162034459520203;
createNode joint -n "R_elbow" -p "R_shoulder2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.6204449608153177 0.51334394850804088 0.29486052711679411 ;
	setAttr ".r" -type "double3" 1.5198785505179766 0.28999797335660427 -17.080525060076674 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 1.8295510799420704 ;
	setAttr ".bps" -type "matrix" 0.98152727650055172 -0.031826189826567312 -0.18865656396355016 0
		 -0.041868156333183817 -0.99789698501986823 -0.0494839850205884 0 -0.18668492968279427 0.056468583560959414 -0.98079561382581104 0
		 -3.1258966350727269 9.0072935349274008 -0.24415321176992771 1;
	setAttr ".radi" 0.5548909056404242;
createNode joint -n "R_wrist" -p "R_elbow";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.9909129089091606 0.018437670344332465 0.3655465866639333 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -1.4232048466328338 ;
	setAttr ".bps" -type "matrix" 0.98226436959005503 -0.007031548336599816 -0.18736932929869277 0
		 -0.017476992395205171 -0.99837961537614028 -0.054154393526369901 0 -0.18668492968279427 0.056468583560959414 -0.98079561382581104 0
		 -5.1490459503956636 9.0728997094215629 -0.22799328147656489 1;
	setAttr ".radi" 0.5548909056404242;
createNode joint -n "L_shoulder1" -p "spine1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.6006861650078306 -0.67833031464881477 -0.15936315138038804 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -54.637538112930919 ;
	setAttr ".bps" -type "matrix" 0.84225299464722292 0.53648397290854399 -0.052866244618377606 0
		 -0.45177973192555654 0.75595468909840269 0.47373788306553022 0 0.29411726713856468 -0.37512325286817727 0.87907768617371917 0
		 0.7869257429784331 9.0162411374250464 -0.12576788527805571 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_shoulder2" -p "L_shoulder1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.0223972869332421 -0.49611474100631509 0.39449258542711874 ;
	setAttr ".r" -type "double3" 9.8945427457644914 -2.5218939989568332 -40.531804150527279 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -35.768808120378125 ;
	setAttr ".bps" -type "matrix" 0.65071943456293002 -0.62331230476989086 -0.43364269647319964 0
		 0.74638212805996629 0.63003267853385103 0.21441208662791694 0 0.13956337769908342 -0.46318527038530499 0.87520367281182898 0
		 1.9882045854455532 9.0417172891375479 -0.068056908298209817 1;
	setAttr ".radi" 0.54162034459520203;
createNode joint -n "L_elbow" -p "L_shoulder2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.1599945484980763 0.98352890491756506 -0.0062502672026289148 ;
	setAttr ".r" -type "double3" 3.1157605526867553 0.19431197388385404 -34.530258173862826 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 1.8295510799420707 ;
	setAttr ".bps" -type "matrix" 0.14387546818535391 -0.86331965859585935 -0.48371377563221624 0
		 0.98579944684658927 0.16780922571970913 -0.006286044886120723 0 0.086598500283803378 -0.47594036479928942 0.8752036728118292 0
		 3.4762516707988427 8.9412287956776115 -0.36566984425267823 1;
	setAttr ".radi" 0.5548909056404242;
createNode joint -n "L_wrist" -p "L_elbow";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.25889777981505097 1.6887081396482226 0.1860065696888607 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -1.4232048466330167 ;
	setAttr ".bps" -type "matrix" 0.11934672815588346 -0.86722122265026202 -0.48340842924329552 0
		 0.98906878336498005 0.14631513981712244 -0.018298131959686284 0 0.086598500283803378 -0.47594036479928942 0.8752036728118292 0
		 5.1943361500104226 8.9125700235446139 -0.33872392903638049 1;
	setAttr ".radi" 0.5548909056404242;
createNode joint -n "R_hip" -p "root";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.36157217039139677 0.70326571742193122 0.036009361085176383 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 -1.7655625192200639e-31 163.7798063043972 ;
	setAttr ".bps" -type "matrix" -0.27932954083013145 -0.96019529660357572 3.081487911019578e-33 0
		 -0.9601952966035755 0.27932954083013145 1.2246467991473535e-16 0 -1.1759000965419126e-16 3.4208002808492039e-17 -1 0
		 -0.70326571742193145 5.0875860481767585 -0.30386923284970768 1;
	setAttr ".radi" 0.56680960267965363;
createNode joint -n "R_knee" -p "R_hip";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.2916523184733042 -8.8817841970012523e-16 -2.8064646765769431e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -13.472105515549019 ;
	setAttr ".bps" -type "matrix" -0.047944799368398933 -0.99884998684163018 -2.8530833734542421e-17 0
		 -0.99884998684162996 0.047944799368398877 1.1909487542724279e-16 0 -1.1759000965419126e-16 3.4208002808492039e-17 -1 0
		 -1.3433919072833849 2.8871522705280119 -0.3038692328497074 1;
	setAttr ".radi" 0.57776061239839238;
createNode joint -n "R_ankle" -p "R_knee";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.5033718397022513 1.2490009027033016e-16 -2.9813875739740733e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 87.25191181994613 ;
	setAttr ".bps" -type "matrix" 1 1.2698175844150228e-15 4.8746702605441323e-18 0 -1.3183898417423734e-15 1.0000000000000002 3.4208002808492193e-17 0
		 -4.874670260544083e-18 -3.4208002808492199e-17 1 0 -1.4634155678824092 0.38665934138171076 -0.30386923284970718 1;
	setAttr ".radi" 0.57776061239839238;
createNode joint -n "L_hip" -p "root";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.40010427143280314 -0.703266 0.035124195029551775 ;
	setAttr ".r" -type "double3" 15.292661235000544 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 16.220193695602813 ;
	setAttr ".bps" -type "matrix" -0.27932954083013134 0.96019529660357561 0 0 -0.92619594191811305 -0.26943881946708764 0.26374950187365676 0
		 0.25325103118062114 0.07367302725254439 0.9645912088866444 0 0.70326599999999995 5.0490539471353522 -0.30475439890533229 1;
	setAttr ".radi" 0.56680960267965363;
createNode joint -n "L_knee" -p "L_hip";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -2.2916480794997516 1.0474857785602865e-06 9.8607600000000006e-32 ;
	setAttr ".r" -type "double3" -0.041209307788979715 25.507124019960671 -6.8032122313017211 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.4787793342770447e-06 -2.3696978997167337e-23 -13.472105515549028 ;
	setAttr ".bps" -type "matrix" -0.055865694897517679 0.86545353467325881 -0.49786464370868966 0
		 -0.96578617028524016 0.079638343082102797 0.24680925346693786 0 0.25325105610715104 0.49461895801990896 0.8313940034351418 0
		 1.3433900356138406 2.8486239574957426 -0.30475412263148 1;
	setAttr ".radi" 0.57776061239839238;
createNode joint -n "L_ankle" -p "L_knee";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -2.4149139824186348 -0.36926563640998122 -1.388676236684746 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999945836933 -1.4770787096076167e-06 -92.74808818005387 ;
	setAttr ".bps" -type "matrix" 0.96735397954346125 -0.12104074127100875 -0.22265537768936697 0
		 0.0094970268402255944 -0.86064000266357221 0.50912532081644035 0 -0.25325103107909747 -0.49461896927616139 -0.83139400436228961 0
		 1.4832488046344112 0.04235481882279224 -0.34812910513341228 1;
	setAttr ".radi" 0.57776061239839238;
createNode transform -n "mordakTheFantastic";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -0.0079250335693359375 6.5104784965515137 -0.17559576034545898 ;
	setAttr ".sp" -type "double3" -0.0079250335693359375 6.5104784965515137 -0.17559576034545898 ;
createNode mesh -n "mordakTheFantasticShape" -p "mordakTheFantastic";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
createNode mesh -n "mordakTheFantasticShapeOrig" -p "mordakTheFantastic";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 2677 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.14200599 0.55317903 0.16083901
		 0.561055 0.156495 0.57177401 0.139963 0.56483299 0.151421 0.58460701 0.137214 0.57930201
		 0.121315 0.57750601 0.121316 0.56179798 0.121316 0.54957902 0.14621601 0.597552 0.141743
		 0.60796601 0.13248 0.60863298 0.13415401 0.59397399 0.121313 0.60896701 0.121314
		 0.59347802 0.110146 0.60863203 0.100882 0.60796398 0.096409999 0.59754801 0.108473
		 0.59397298 0.091205999 0.58460301 0.105414 0.57929999 0.086134002 0.571769 0.081790999
		 0.56104898 0.100625 0.55317599 0.102667 0.56483102 0.14613201 0.61759502 0.134065
		 0.62336498 0.15120301 0.62924802 0.137403 0.63815498 0.121308 0.64242202 0.121311
		 0.62488699 0.15717 0.63997602 0.16385899 0.64747298 0.158227 0.65894198 0.142956
		 0.651622 0.152539 0.67344099 0.121305 0.66204602 0.084381998 0.65893298 0.078752004
		 0.64746302 0.085445002 0.63996798 0.099656999 0.651618 0.091415003 0.629242 0.105214
		 0.63815099 0.090066001 0.67343402 0.096488997 0.61759102 0.108557 0.623362 0.176018
		 0.64223701 0.177743 0.65694201 0.18984599 0.63350898 0.19558901 0.64852703 0.20295499
		 0.66445899 0.179327 0.67312598 0.202338 0.62321597 0.212441 0.614263 0.22183301 0.62743902
		 0.210783 0.63730198 0.232712 0.63842303 0.22129101 0.65059 0.020779001 0.62741601
		 0.030175 0.61424297 0.040277001 0.62319797 0.031826999 0.637281 0.052765999 0.63349301
		 0.047019999 0.64850998 0.039650999 0.66443902 0.021318 0.650567 0.0098980004 0.63839698
		 0.066592999 0.64222503 0.064865001 0.65692902 0.063276999 0.67311198 0.520531 0.41821799
		 0.52397603 0.427037 0.518498 0.43322101 0.511114 0.42452699 0.51363403 0.442074 0.50308901
		 0.43496999 0.48674101 0.427953 0.50284898 0.413288 0.51804101 0.40637901 0.507967
		 0.450293 0.50413501 0.457755 0.49621201 0.457133 0.49851501 0.446816 0.48672399 0.45785099
		 0.48673201 0.44530201 0.47723699 0.45712 0.46931499 0.45773199 0.46549001 0.450266
		 0.47494599 0.44679999 0.45983201 0.44204 0.470384 0.43494901 0.45497799 0.433182
		 0.44950801 0.426992 0.45296201 0.41817701 0.462372 0.424496 0.45546499 0.40634 0.47064999
		 0.41326699 0.20473801 0.59846401 0.195218 0.60782999 0.183309 0.61922997 0.17458101
		 0.60550499 0.185507 0.59262902 0.19415601 0.58260602 0.170389 0.62970197 0.16315299
		 0.61808097 0.155651 0.60657197 0.163773 0.59351498 0.178611 0.57027102 0.17184401
		 0.58055001 0.064016998 0.570261 0.070782997 0.58054101 0.078850999 0.59350801 0.068039998
		 0.60549402 0.057115 0.59261602 0.048469 0.582591 0.086971998 0.60656703 0.079466
		 0.61807299 0.072227001 0.62969202 0.059308 0.61921602 0.037882999 0.59844601 0.047401
		 0.60781401 0.21707501 0.61052197 0.226533 0.62355101 0.554995 0.43751001 0.54935902
		 0.43573099 0.55004799 0.42248699 0.55562001 0.423796 0.55005699 0.409419 0.55539203
		 0.41004699 0.236847 0.63361901 0.19773 0.57867098 0.20880701 0.59454399 0.53911901
		 0.46268499 0.53502703 0.459472 0.543405 0.447909 0.54838902 0.450241 0.16283 0.55629998
		 0.181703 0.56609899 0.51823699 0.48581699 0.51564902 0.480813 0.52648801 0.47131699
		 0.52979898 0.475593 0.121316 0.54466897 0.14269 0.54821002 0.486707 0.49002299 0.48671001
		 0.484914 0.50164199 0.484557 0.503142 0.489687 0.079801999 0.55629402 0.099942997
		 0.54820597 0.455181 0.48577699 0.45777401 0.480777 0.47177801 0.48453799 0.47027099
		 0.489667 0.044895999 0.57865602 0.060924999 0.56608802 0.43432599 0.46262199 0.43842199
		 0.45941299 0.44694701 0.47126901 0.44363099 0.47554001 0.025543001 0.61049998 0.033815
		 0.59452498 0.41847801 0.43742901 0.42411599 0.43565699 0.43005699 0.44784099 0.42506999
		 0.45016801 0.0057629999 0.633591 0.01608 0.62352598 0.418111 0.40996501 0.423446
		 0.40934399 0.42344099 0.42241201 0.417867 0.423715 0.53496802 0.43057701 0.53551298
		 0.41877499 0.535276 0.40645599 0.52450597 0.45089099 0.53070402 0.44116399 0.50905597
		 0.46771899 0.51770699 0.46030101 0.48671699 0.47158799 0.49821401 0.47101 0.46438199
		 0.467691 0.47522101 0.47099501 0.44895199 0.450845 0.45573899 0.460262 0.438512 0.43051901
		 0.44276401 0.44111201 0.438232 0.40639701 0.43798 0.418717 0.36710799 0.51159197
		 0.37086999 0.49511001 0.372794 0.495152 0.36886901 0.51235598 0.38009501 0.49531099
		 0.375545 0.51525098 0.36405301 0.53217602 0.35895401 0.52695799 0.35760701 0.52557999
		 0.32485399 0.44817001 0.32493001 0.45008901 0.30854201 0.454142 0.30774999 0.452398
		 0.29475501 0.463902 0.293356 0.46258301 0.28804299 0.45757699 0.30474201 0.445777
		 0.32458901 0.44087601 0.36043999 0.46276301 0.35903701 0.46408299 0.345202 0.454254
		 0.34600699 0.45250601 0.32874501 0.450082 0.32883799 0.448154 0.32917199 0.44083199
		 0.34906599 0.44587499 0.36576501 0.457755 0.376461 0.475187 0.36966601 0.47779 0.36787599
		 0.478477 0.32678601 0.53999001 0.32678899 0.53806198 0.34337601 0.53472197 0.3441
		 0.53650302 0.346836 0.54323602 0.32677299 0.54728198 0.285887 0.47828999 0.28409699
		 0.47759899 0.282841 0.49491999 0.28091601 0.49495599 0.27361101 0.495094 0.27730301
		 0.474978 0.880032 0.18840601 0.87549102 0.18923099 0.87371999 0.17158499 0.87878299
		 0.169147 0.86526 0.162016 0.86381102 0.157143 0.865484 0.15486801 0.88496399 0.16716699
		 0.88439101 0.188051 0.905101 0.15597899 0.90664399 0.158345 0.89102399 0.16949201
		 0.90492201 0.163129 0.89594102 0.17221101 0.89321101 0.18973599 0.88872302 0.18865401
		 0.90922999 0.161277 0.90043402 0.17464399 0.91038197 0.16344699;
	setAttr ".uvst[0].uvsp[250:499]" 0.90457797 0.17645501 0.90256703 0.190731
		 0.897807 0.190548 0.91186398 0.16537499 0.90850103 0.177351 0.9131 0.168963 0.91235501
		 0.177303 0.91269302 0.187961 0.90751803 0.189896 0.91528201 0.16637801 0.91619802
		 0.176393 0.917992 0.16592599 0.920021 0.175107 0.92288399 0.181741 0.91792601 0.185056
		 0.84950203 0.17313001 0.85204899 0.164076 0.85473001 0.16468 0.85324901 0.174629
		 0.85676497 0.167383 0.857041 0.175751 0.85611302 0.186371 0.851035 0.183182 0.846264
		 0.17959701 0.85820103 0.16386899 0.86088997 0.17601299 0.85979003 0.162027 0.86485898
		 0.175337 0.86609101 0.189698 0.86118501 0.18858901 0.861063 0.159925 0.86909801 0.17376199
		 0.87085497 0.18978301 0.30672601 0.543145 0.289547 0.53201699 0.29466301 0.52681601
		 0.309486 0.53642201 0.29601601 0.525442 0.310215 0.53464299 0.27810499 0.51504999
		 0.28479001 0.51217699 0.28655499 0.51141798 0.91281098 0.158573 0.91156298 0.156093
		 0.911897 0.154228 0.915048 0.157022 0.85763699 0.15702599 0.85549098 0.155351 0.85879499
		 0.152738 0.85902399 0.15461899 0.856094 0.159631 0.853679 0.158767 0.91666502 0.16053399
		 0.91420603 0.16126101 0.827887 0.253562 0.83656299 0.242336 0.84652698 0.25068501
		 0.83964097 0.262793 0.85757899 0.25739899 0.85270798 0.27018499 0.849015 0.279919
		 0.83442301 0.27197301 0.82132798 0.26201999 0.80836099 0.23127601 0.81980902 0.222394
		 0.827721 0.23275501 0.81752199 0.242899 0.80983001 0.250494 0.79974002 0.2379 0.793733
		 0.20574 0.80686897 0.19978499 0.81287003 0.211357 0.80045599 0.21882699 0.79110199
		 0.224387 0.78382498 0.210161 0.95810002 0.235515 0.94713998 0.225898 0.95459998 0.21511699
		 0.96695501 0.22323801 0.96105498 0.203444 0.97453701 0.20973 0.98495501 0.21461999
		 0.97656202 0.22952101 0.966429 0.242975 0.95539898 0.25472799 0.94321501 0.26551801
		 0.93719602 0.25665799 0.94821298 0.246604 0.92919701 0.24493501 0.93861002 0.23584799
		 0.91143602 0.27184701 0.907318 0.2588 0.91874802 0.252716 0.92491001 0.26520699 0.92956799
		 0.27468601 0.91455102 0.28178301 0.881899 0.27706301 0.88228703 0.26363099 0.895024
		 0.26253301 0.89693302 0.27589899 0.89837497 0.28609899 0.88160098 0.28731701 0.86963499
		 0.26181701 0.86695498 0.27505001 0.864923 0.28514799 0.84591401 0.230534 0.85461801
		 0.217472 0.86090702 0.223464 0.85399902 0.238079 0.86780602 0.22819699 0.86291099
		 0.24412601 0.83208001 0.21281999 0.843463 0.20316 0.84889102 0.210556 0.83867902
		 0.221949 0.82084298 0.19331899 0.83351701 0.18726601 0.838278 0.195408 0.826123 0.20326801
		 0.93531603 0.215679 0.92442501 0.205423 0.92998201 0.19795001 0.94172198 0.206416
		 0.93513501 0.19006 0.94740099 0.196685 0.92051703 0.23262399 0.91252899 0.219099
		 0.91862202 0.212513 0.92823201 0.224447 0.90275502 0.24524499 0.89876199 0.229067
		 0.90591699 0.224729 0.91200602 0.23970699 0.882689 0.249643 0.88317603 0.23245899
		 0.89108902 0.231676 0.892869 0.248625 0.87258399 0.248055 0.875319 0.231233 0.86184299
		 0.202399 0.86668903 0.205137 0.871925 0.20708001 0.85224998 0.194051 0.85711402 0.19870199
		 0.84259802 0.182487 0.84730399 0.188531 0.91613197 0.19584601 0.92135 0.19059999
		 0.92636698 0.184818 0.90612298 0.203649 0.91103798 0.200222 0.89580297 0.207754 0.90113801
		 0.20611 0.883829 0.208729 0.889956 0.20857599 0.87771899 0.20823 0.397302 0.495682
		 0.391271 0.52206302 0.41401201 0.49603099 0.40654001 0.52865797 0.387701 0.55633098
		 0.37605599 0.54444999 0.32674101 0.564421 0.35327199 0.55906999 0.35951 0.57438701
		 0.32671201 0.58098501 0.293951 0.57424301 0.26584199 0.556077 0.27750999 0.54424602
		 0.300235 0.55895501 0.24708501 0.52834898 0.23969001 0.49570099 0.25640199 0.49541
		 0.26236299 0.52181101 0.27551499 0.445779 0.261289 0.46879601 0.24573299 0.46278301
		 0.26333901 0.43432099 0.32392901 0.42365199 0.297631 0.430168 0.29068199 0.41501999
		 0.32319599 0.40690601 0.37832499 0.44599101 0.356264 0.43028501 0.329974 0.42358899
		 0.33082101 0.406856 0.36328399 0.415167 0.39054599 0.43457901 0.39248899 0.46905401
		 0.40806201 0.46309599 0.915833 0.28607601 0.93151402 0.27881199 0.94575697 0.269393
		 0.94656801 0.27063999 0.93213397 0.28014201 0.91624099 0.28746101 0.881473 0.29170999
		 0.89895898 0.29048401 0.89914399 0.29189599 0.88143098 0.29312599 0.84747899 0.28412601
		 0.86408299 0.289489 0.86381501 0.290887 0.84698898 0.28548199 0.818542 0.26571 0.83222997
		 0.27596599 0.83152902 0.27725199 0.81765199 0.26689699 0.79602301 0.24078301 0.80653501
		 0.25380901 0.80548102 0.25487399 0.794833 0.241707 0.77952302 0.212052 0.787045 0.226788
		 0.785743 0.227552 0.77809799 0.21255299 0.97001201 0.246254 0.98075002 0.232316 0.98954803
		 0.216754 0.99107999 0.217336 0.98209798 0.23322 0.971156 0.247309 0.95845401 0.258295
		 0.95942599 0.25944099 0.898745 0.49279499 0.89833897 0.49913499 0.86534101 0.50379199
		 0.86329597 0.497796 0.83637601 0.52026999 0.83218002 0.51550001 0.395134 0.43029699
		 0.36593601 0.409495 0.331301 0.40057701 0.92040497 0.66517901 0.94936597 0.64868802
		 0.953574 0.65357399 0.92245501 0.67132902 0.26148599 0.56040001 0.291605 0.57982999
		 0.32671201 0.58703202 0.88700801 0.67635697 0.88741499 0.669846 0.81633002 0.54688901
		 0.81064302 0.544101 0.80750102 0.57901597 0.801153 0.57863301 0.42027599 0.49613601
		 0.41390899 0.46085101 0.969414 0.62206501 0.97825402 0.58993697;
	setAttr ".uvst[0].uvsp[500:749]" 0.98460102 0.59037697 0.97511601 0.62493199
		 0.233429 0.49577501 0.24136899 0.53073001 0.25876901 0.430022 0.288059 0.40933499
		 0.96187299 0.52379602 0.95710403 0.52799302 0.93047398 0.50796002 0.93326598 0.50227302
		 0.32275 0.40062299 0.81216502 0.61200702 0.80615199 0.61410701 0.828655 0.640962
		 0.823874 0.64525503 0.392066 0.56068802 0.41226199 0.53107601 0.85528499 0.66100597
		 0.85248703 0.66683698 0.36185801 0.57999402 0.239894 0.46051499 0.97359502 0.55694801
		 0.97959399 0.55491102 0.94337302 0.54007798 0.92243499 0.52432799 0.93079901 0.551148
		 0.91490501 0.54571599 0.89565802 0.54103398 0.89717102 0.51738799 0.87122899 0.521052
		 0.87597102 0.54322499 0.85951501 0.546588 0.84845501 0.53400701 0.83269399 0.55493498
		 0.85407501 0.562482 0.84938502 0.58173001 0.82574898 0.58019501 0.85157001 0.60142398
		 0.85493898 0.61787599 0.84237498 0.62890399 0.82941198 0.60613501 0.87083399 0.62333101
		 0.89009202 0.62801999 0.88858098 0.651604 0.863316 0.64465898 0.91452402 0.64793801
		 0.90979201 0.625826 0.92624998 0.62244099 0.937298 0.634978 0.93169099 0.60655099
		 0.95305902 0.61404401 0.93636799 0.587295 0.960006 0.58878201 0.95633799 0.56284398
		 0.93416899 0.56760502 0.91425103 0.58589298 0.89287603 0.58452803 0.894243 0.563151
		 0.91381598 0.56609899 0.87150198 0.583157 0.87445498 0.56358099 0.91130501 0.60547298
		 0.89150798 0.60590601 0.87192798 0.60295099 0.98579001 0.95596302 0.98780501 0.96272302
		 0.98490602 0.96337402 0.98369199 0.95657903 0.98209602 0.96379799 0.98156703 0.95703
		 0.98070902 0.95218098 0.98156601 0.950396 0.983257 0.95168501 0.97953999 0.96389699
		 0.97955102 0.95711797 0.97698402 0.96378797 0.977534 0.957021 0.97840601 0.95217597
		 0.97956097 0.95039201 0.97417402 0.96335399 0.975411 0.95656401 0.97127599 0.96269298
		 0.973315 0.95594001 0.97586101 0.951671 0.97755599 0.950387 0.96841198 0.96198702
		 0.97148401 0.954992 0.99067199 0.96202803 0.98762399 0.95502001 0.98303401 0.94981599
		 0.976089 0.94980198 0.97604102 0.98883998 0.97580999 0.98688602 0.97751498 0.98829198
		 0.97795999 0.99067497 0.97835499 0.98641801 0.97948998 0.98830003 0.98063201 0.98642302
		 0.98146403 0.98830003 0.98100698 0.99068099 0.98317498 0.98690099 0.98293602 0.988855
		 0.981121 0.94813299 0.97800797 0.94812697 0.982337 0.97075701 0.98209703 0.974163
		 0.97952098 0.97406101 0.97952801 0.97068501 0.97694403 0.97415298 0.97671902 0.97074401
		 0.97656298 0.96890002 0.979532 0.96891099 0.98250002 0.96891099 0.97379303 0.96888101
		 0.97388703 0.97087401 0.97411299 0.97454703 0.97119403 0.97515702 0.97108197 0.97121
		 0.97117501 0.96885997 0.96867901 0.96884 0.96829998 0.97131002 0.96831101 0.975806
		 0.990722 0.97585201 0.98784101 0.97519201 0.98796898 0.97124398 0.99075103 0.97135502
		 0.987885 0.96889299 0.99038202 0.96888298 0.985268 0.96890497 0.98516703 0.97089702
		 0.98492497 0.97456998 0.982337 0.96706998 0.97953498 0.96714097 0.97673303 0.96706003
		 0.97390801 0.966892 0.97111201 0.96650898 0.96833301 0.96636099 0.987957 0.96654099
		 0.99073702 0.96640301 0.98516101 0.966914 0.98153299 0.981309 0.979505 0.98121601
		 0.977476 0.9813 0.97533602 0.98172897 0.97322202 0.98231798 0.97136402 0.98322201
		 0.98578298 0.98234302 0.98763698 0.98325598 0.98367101 0.98174697 0.41554999 0.56909001
		 0.43875301 0.53833598 0.437756 0.57032901 0.41531101 0.58915699 0.43450901 0.60201198
		 0.413284 0.60928899 0.39879701 0.61038601 0.39390701 0.60420901 0.39774501 0.58914697
		 0.43028799 0.61346197 0.40956199 0.616373 0.431923 0.623833 0.40820301 0.62330699
		 0.39054701 0.61735803 0.393774 0.61340499 0.88306302 0.39292499 0.86309898 0.38413101
		 0.86050898 0.352492 0.88192397 0.372547 0.856592 0.32054299 0.87938899 0.35213199
		 0.89477599 0.374522 0.90112299 0.39022201 0.89628899 0.396777 0.407776 0.56626499
		 0.42796999 0.53174001 0.43728101 0.52622801 0.413645 0.56197602 0.38971701 0.59361899
		 0.38962901 0.58833802 0.52971202 0.594163 0.530366 0.58850801 0.53545201 0.612001
		 0.53040302 0.60843599 0.52811599 0.617993 0.52497 0.61361402 0.52061999 0.61079901
		 0.52603 0.60471499 0.52230901 0.58962899 0.38806799 0.608356 0.38328499 0.61127502
		 0.50613499 0.60990101 0.50928497 0.616979 0.51012599 0.62385499 0.486738 0.62419403
		 0.48859501 0.61387098 0.48458499 0.60238999 0.81344098 0.353459 0.81217802 0.385185
		 0.79306 0.394712 0.79346102 0.37429401 0.78000301 0.399113 0.77521598 0.39274999
		 0.780424 0.37688601 0.79483497 0.35384899 0.81589502 0.32138601 0.482797 0.52649099
		 0.49206799 0.53197199 0.511783 0.56642503 0.50585699 0.56263798 0.50444102 0.56971502
		 0.481242 0.53860199 0.481749 0.57064497 0.50456101 0.58974701 0.471147 0.597278 0.473665
		 0.61241502 0.47172499 0.62497598 0.45934701 0.62564498 0.45944101 0.61226398 0.45954499
		 0.59563398 0.83654702 0.33610299 0.83742201 0.37689501 0.82520598 0.379078 0.826047
		 0.34055099 0.82763797 0.30104101 0.835621 0.29394799 0.460197 0.50419402 0.46021801
		 0.498559 0.478277 0.50704497 0.46597701 0.50709897 0.46765301 0.51898003 0.46015999
		 0.511989 0.45980999 0.55252397 0.46910301 0.55748898 0.447925 0.59711498 0.44522801
		 0.61221403 0.44699499 0.62479502 0.84971797 0.37854901 0.84723097 0.34009299 0.84390497
		 0.30069 0.44207299 0.50689799 0.454382 0.50705701 0.452535 0.51890397 0.450436 0.557365
		 0.90315402 0.83985001 0.81699502 0.839248 0.81790102 0.75999302 0.897201 0.76492202
		 0.81858701 0.69973898 0.88754302 0.708202 0.93079102 0.73018903;
	setAttr ".uvst[0].uvsp[750:999]" 0.956411 0.77174699 0.96753299 0.840002 0.95485598
		 0.90751702 0.895509 0.91277403 0.928312 0.94712198 0.88459599 0.96640402 0.815485
		 0.97172701 0.816122 0.91572899 0.23048 0.692469 0.197358 0.694969 0.19730701 0.68656301
		 0.23045 0.684192 0.29618201 0.047552001 0.288261 0.053424001 0.27568901 0.032264002
		 0.28446001 0.027789 0.266453 0.009118 0.27559501 0.00581 0.264294 0.683052 0.26444
		 0.69130999 0.165675 0.69844598 0.135371 0.70292199 0.13477699 0.69424403 0.16540501
		 0.68992102 0.327153 0.078345999 0.3215 0.086534001 0.30370101 0.071529999 0.31056201
		 0.064400002 0.10595 0.70769602 0.077894002 0.71201199 0.077609003 0.703071 0.105229
		 0.69892699 0.36674601 0.096564002 0.36396301 0.106148 0.34187099 0.098188996 0.34614
		 0.089189 0.050997999 0.71304101 0.02499 0.70830601 0.027915999 0.69992602 0.052099999
		 0.70417303 0.413441 0.100986 0.413443 0.110828 0.38767901 0.10986 0.388998 0.100055
		 0.278227 0.061168 0.26453599 0.037967 0.260093 0.075162999 0.24430101 0.048167001
		 0.232581 0.018142 0.25460801 0.012406 0.31410301 0.097354002 0.29494199 0.080972999
		 0.300713 0.116835 0.27909201 0.098024003 0.359833 0.118618 0.33605 0.109995 0.35235801
		 0.141065 0.32548201 0.13127001 0.41344401 0.123387 0.38546801 0.122441 0.41344801
		 0.14618801 0.38148099 0.145227 0.22853599 0.099766999 0.20901801 0.066593997 0.18531001
		 0.133571 0.161081 0.093344003 0.140718 0.048618 0.193611 0.029581999 0.277466 0.150517
		 0.251524 0.127676 0.24631999 0.196123 0.21391299 0.167768 0.33945301 0.179979 0.30714199
		 0.16805901 0.32290301 0.23350701 0.28310901 0.218257 0.41345501 0.186234 0.37464899
		 0.18506999 0.41346401 0.241979 0.36598 0.24033099 0.13131499 0.174979 0.102265 0.127873
		 0.085909002 0.209452 0.053295001 0.157152 0.025141999 0.099808998 0.077188998 0.075957999
		 0.20844799 0.252078 0.16699401 0.216434 0.176818 0.29885399 0.127471 0.25689399 0.30452999
		 0.30031499 0.25510001 0.28051499 0.289814 0.35634899 0.232126 0.33272201 0.41347399
		 0.31186801 0.35663 0.30952901 0.41348401 0.37041301 0.34924799 0.367513 0.41348699
		 0.392333 0.34649199 0.389231 0.81527901 0.98969299 0.88078099 0.98363298 0.93688798
		 0.95757699 0.28433099 0.37736401 0.22353201 0.352332 0.974208 0.90542603 0.98848301
		 0.83997202 0.164931 0.316425 0.112592 0.27205899 0.97570902 0.77420801 0.93960398
		 0.72017199 0.068827003 0.22233801 0.034933001 0.168097 0.88413697 0.68973398 0.81880999
		 0.680116 0.0057629999 0.108685 0.018340001 0.72225899 0.048765 0.72754699 0.077832997
		 0.72675598 0.077311002 0.74879599 0.045825001 0.75086498 0.010131 0.74790001 0.26465401
		 0.70342201 0.22994 0.70479399 0.26501599 0.72395998 0.229837 0.72542101 0.196419
		 0.72868198 0.196492 0.70798701 0.1355 0.71732098 0.10645 0.72235298 0.135922 0.737589
		 0.106517 0.74343699 0.16511001 0.712219 0.165574 0.73262101 0.26863199 0.92883098
		 0.268978 0.948488 0.235011 0.93648797 0.24113899 0.92501301 0.211602 0.92661798 0.21740399
		 0.91940898 0.216583 0.90272999 0.24056 0.90469402 0.26822901 0.90599799 0.178074
		 0.95577002 0.17073201 0.93374801 0.193794 0.93085498 0.211252 0.947631 0.22917899
		 0.97180802 0.18623599 0.98658103 0.14811499 0.93993199 0.14505599 0.96164697 0.143328
		 0.99083102 0.108398 0.98559302 0.117989 0.96168399 0.12888999 0.94363999 0.077186003
		 0.79270101 0.086318001 0.84885597 0.052046001 0.85491699 0.044365 0.79812199 0.015587
		 0.86571598 0.0057629999 0.80137402 0.691145 0.63514298 0.69506299 0.67010999 0.67630899
		 0.66961801 0.67381501 0.63465601 0.66105503 0.66915298 0.66023397 0.63419801 0.65929699
		 0.58706802 0.67136699 0.58796799 0.68718499 0.58916402 0.087514997 0.93713498 0.065245003
		 0.955733 0.038380001 0.91874403 0.068406999 0.90259999 0.093970999 0.89349198 0.107339
		 0.92406797 0.087082997 0.97545701 0.102671 0.95486802 0.117694 0.93936902 0.114397
		 0.873124 0.114246 0.89682102 0.13706499 0.88111901 0.135416 0.89721298 0.13768201
		 0.910896 0.121781 0.91960901 0.65886098 0.53391898 0.65949398 0.490583 0.67290097
		 0.49489701 0.67071801 0.53618997 0.68829 0.500772 0.685673 0.53948599 0.652587 0.489914
		 0.65227097 0.53381997 0.65262598 0.58683401 0.64592701 0.58696401 0.64541 0.53482503
		 0.64630198 0.49158701 0.129787 0.930462 0.142298 0.91573101 0.14852799 0.916044 0.13927899
		 0.93308198 0.64646697 0.63374901 0.65306699 0.63391501 0.65337402 0.66886699 0.64685398
		 0.668688 0.26579699 0.76817799 0.26673999 0.82168198 0.23486 0.82220203 0.23153199
		 0.76928502 0.202618 0.827416 0.19931 0.77170801 0.138455 0.78001899 0.169514 0.77505201
		 0.17285401 0.82157201 0.143481 0.82598001 0.26756901 0.86861497 0.23772299 0.86851197
		 0.212338 0.87069303 0.142974 0.86982399 0.12758701 0.859128 0.113826 0.83240199 0.14783099
		 0.85214502 0.150561 0.86509299 0.106996 0.78623402 0.18648 0.85926402 0.169594 0.87440699
		 0.15945099 0.86638802 0.169089 0.85113299 0.57605898 0.636971 0.57326198 0.67204499
		 0.55569601 0.672831 0.55874801 0.63781798 0.54338098 0.672719 0.546803 0.63781297
		 0.55122799 0.59306401 0.56228501 0.59253597 0.57854998 0.59088898 0.020268001 0.541237
		 0.011913 0.53907299 0.018518001 0.52266598 0.028603001 0.52115798 0.027014 0.50960201
		 0.037555002 0.50243002 0.049403001 0.50099099 0.040321998 0.52242202 0.03077 0.54496199
		 0.70073998 0.50660002 0.69960898 0.54277301 0.71117401 0.50989503 0.70880401 0.54446602;
	setAttr ".uvst[0].uvsp[1000:1249]" 0.71449101 0.59047502 0.70345402 0.59029597
		 0.708435 0.63544101 0.72034103 0.635059 0.72487599 0.669837 0.71260601 0.67033899
		 0.578475 0.541188 0.56460798 0.54491502 0.55543703 0.54689598 0.55196601 0.51241702
		 0.56233001 0.50880098 0.57464701 0.50258201 0.201288 0.90440297 0.20055699 0.91656101
		 0.194796 0.92161101 0.176566 0.91061503 0.181628 0.90777302 0.182988 0.90172398 0.640333
		 0.66852999 0.639862 0.63360101 0.63410699 0.66850299 0.63355201 0.633578 0.63281298
		 0.58721399 0.63922399 0.58709902 0.18246099 0.924088 0.167548 0.92587203 0.162761
		 0.91375202 0.16982999 0.91259497 0.15569399 0.91511798 0.15282699 0.93024802 0.63903499
		 0.494057 0.63856101 0.53576899 0.63200599 0.536282 0.63135701 0.49555501 0.196475
		 0.88104302 0.178249 0.888529 0.041758001 0.55973399 0.037338 0.561634 0.031978 0.56014901
		 0.035115998 0.557724 0.025125001 0.55830902 0.026582001 0.55486 0.040732 0.54996502
		 0.048083 0.55449998 0.620673 0.63415802 0.62139601 0.66909099 0.61489898 0.669478
		 0.61409497 0.63453501 0.60724699 0.67000699 0.60695499 0.63504398 0.60639799 0.58790898
		 0.61303997 0.58746302 0.61972201 0.58737999 0.60514599 0.53477198 0.603149 0.491478
		 0.610008 0.49058899 0.61171299 0.534464 0.61632001 0.492062 0.61858797 0.53525102
		 0.056600001 0.510553 0.049364001 0.53001201 0.058770999 0.52314502 0.055284999 0.53834701
		 0.58987701 0.49621901 0.59335101 0.53741902 0.59435302 0.58919197 0.019045001 0.55463398
		 0.018603999 0.55068099 0.014251 0.55179 0.012547 0.54727602 0.59201199 0.67095703
		 0.59338897 0.635934 0.62364399 0.494302 0.62544799 0.53597701 0.62641001 0.58730298
		 0.62725401 0.63380098 0.62789297 0.66872698 0.67916 0.76858699 0.70572197 0.72762799
		 0.74945998 0.70663899 0.73851103 0.76312298 0.73084402 0.83789599 0.66647798 0.836568
		 0.74651098 0.96483397 0.73681998 0.91097403 0.703242 0.94456202 0.67760402 0.90436101
		 0.55124903 0.0057629999 0.56039202 0.0090680001 0.55116498 0.032218002 0.542391 0.027745999
		 0.53860098 0.053383 0.53067797 0.047513999 0.33136299 0.68419898 0.331608 0.69260103
		 0.298419 0.69126898 0.29815599 0.68299699 0.52316803 0.071494997 0.51630402 0.064366996
		 0.50537503 0.086505003 0.49972001 0.078318 0.394122 0.68967098 0.393837 0.69836497
		 0.36339399 0.69496 0.36336201 0.68642998 0.48501 0.098165996 0.48073801 0.089167997
		 0.46292001 0.106133 0.460134 0.096550003 0.45156699 0.69647503 0.45159701 0.70542002
		 0.42340699 0.70209599 0.42381901 0.693308 0.439206 0.109852 0.437884 0.100049 0.501118
		 0.69157898 0.50433803 0.69985002 0.47851399 0.70550102 0.47709799 0.69667703 0.57223898
		 0.012353 0.59426802 0.01808 0.58255798 0.048108999 0.56232101 0.037916999 0.56677598
		 0.075111002 0.54863799 0.061122999 0.54778498 0.09798 0.53193098 0.080935001 0.52617198
		 0.116798 0.512775 0.097322002 0.50140798 0.13124099 0.490834 0.10997 0.47453499 0.141045
		 0.46705401 0.118601 0.445416 0.145217 0.44141999 0.122433 0.633241 0.029507 0.68614101
		 0.048525002 0.665793 0.093258999 0.61784601 0.066524997 0.64157802 0.13349301 0.59834099
		 0.099705003 0.61298603 0.16770101 0.57536298 0.12762199 0.58059102 0.19606701 0.54943001
		 0.150472 0.543809 0.21821401 0.51976103 0.16802301 0.50402099 0.233477 0.48745301
		 0.17995401 0.46094701 0.24031501 0.45225999 0.185057 0.74967903 0.075842999 0.80173302
		 0.099676996 0.773601 0.157029 0.72462201 0.127767 0.741005 0.209341 0.69558698 0.17488401
		 0.69945902 0.25679901 0.659922 0.216351 0.65012598 0.298776 0.61848003 0.25200999
		 0.59482902 0.33266199 0.57183802 0.28046399 0.53714901 0.356309 0.52241403 0.30027899
		 0.477718 0.36749199 0.47031799 0.30951101 0.69442999 0.95481902 0.74993199 0.982144
		 0.48048201 0.389209 0.54263902 0.37732199 0.64553303 0.83606201 0.658306 0.90182799
		 0.60343099 0.35226899 0.66201901 0.31634301 0.69713998 0.71741301 0.65981001 0.77060598
		 0.71434301 0.27195799 0.75809199 0.222221 0.75328499 0.68825102 0.79196501 0.167968
		 0.82111502 0.108546 0.48501799 0.743119 0.45347899 0.74216098 0.45218 0.72015297
		 0.48125699 0.71991903 0.51147598 0.71355999 0.52058601 0.73889601 0.33293399 0.70558202
		 0.333736 0.72626197 0.300226 0.72417998 0.299393 0.70356899 0.42410201 0.737836 0.42342499
		 0.71676201 0.39450899 0.73302799 0.39421499 0.712758 0.36469901 0.72910899 0.364443
		 0.70870399 0.29583499 0.90372002 0.31972799 0.90091199 0.31949699 0.91760999 0.29597399
		 0.92404699 0.32554901 0.92461097 0.30250299 0.93529898 0.309576 0.97039002 0.32664001
		 0.94559902 0.34349701 0.92821598 0.366644 0.93029499 0.360084 0.95256197 0.35301301
		 0.98364103 0.42034 0.95635301 0.430767 0.979909 0.39604399 0.98637599 0.393289 0.95727098
		 0.389465 0.93567801 0.40880999 0.93870401 0.52683902 0.79218298 0.48814699 0.790295
		 0.51929098 0.85683203 0.482474 0.84732699 0.44801101 0.84247702 0.45515299 0.786035
		 0.15694401 0.38182801 0.15285701 0.39324099 0.109549 0.37294701 0.114662 0.360315
		 0.077551998 0.35783899 0.083295003 0.34369001 0.090406001 0.32636499 0.121244 0.34423599
		 0.16226999 0.366862 0.441937 0.887357 0.467805 0.89555597 0.498384 0.910631 0.47284201
		 0.94854498 0.44992799 0.93074501 0.429658 0.91838503 0.41984999 0.93404102 0.43540901
		 0.94900101 0.451713 0.96902698 0.41506699 0.914437 0.39886799 0.90628999 0.40064999
		 0.89253598 0.42179301 0.89139801 0.398435 0.87651002 0.42080501 0.86772001 0.20724399
		 0.38856801 0.204184 0.40358701 0.243681 0.40190899 0.242798 0.41837499;
	setAttr ".uvst[0].uvsp[1250:1499]" 0.24128599 0.43239799 0.201437 0.41535801
		 0.19512101 0.42729899 0.14748999 0.40549999 0.150346 0.399436 0.19884001 0.42143101
		 0.239081 0.43899801 0.234984 0.44407201 0.39805499 0.92851901 0.38821101 0.91181803
		 0.394425 0.91128498 0.40744999 0.92556697 0.074639 0.364934 0.106868 0.37960601 0.104314
		 0.38567501 0.072097003 0.370893 0.33236501 0.76936102 0.30008 0.76807702 0.33102599
		 0.82515299 0.298619 0.821078 0.36056501 0.81826198 0.36226201 0.77165401 0.393475
		 0.77552199 0.390075 0.82163 0.32284001 0.86874598 0.29739401 0.86746001 0.38665199
		 0.84793401 0.41993901 0.827003 0.40713099 0.85419899 0.39212999 0.86543 0.38438001
		 0.86097097 0.425134 0.78062201 0.365372 0.84767097 0.37554201 0.86257899 0.365688
		 0.87094998 0.34827799 0.85641199 0.108272 0.47964001 0.10326 0.48951799 0.059879001
		 0.47500101 0.064813003 0.46409699 0.026068 0.46346301 0.031257998 0.45224899 0.039188001
		 0.436566 0.072718002 0.448645 0.116445 0.46546799 0.90498197 0.063925996 0.90505999
		 0.040346 0.91580898 0.046425998 0.91640502 0.067405 0.92244101 0.057346001 0.92516798
		 0.07271 0.924981 0.090634003 0.91625899 0.089367002 0.90489799 0.088623002 0.167827
		 0.351515 0.17214 0.341346 0.212081 0.365392 0.209895 0.374486 0.244636 0.37727901
		 0.24340899 0.388154 0.097274996 0.31022999 0.102492 0.29906601 0.13309801 0.3175
		 0.127951 0.328269 0.18497699 0.51385802 0.177442 0.52184403 0.147374 0.50460303 0.152933
		 0.49704 0.16201 0.48590201 0.195695 0.50515002 0.35484001 0.90472001 0.36000001 0.907381
		 0.34216899 0.91901302 0.33623299 0.91417003 0.33507299 0.90204602 0.353266 0.89872199
		 0.144629 0.41156399 0.141913 0.417373 0.099212997 0.39749199 0.101754 0.39173999
		 0.067084 0.38254899 0.069581002 0.37686899 0.366799 0.90912199 0.373907 0.91002899
		 0.36954901 0.92230999 0.354583 0.92105299 0.38441601 0.926166 0.381017 0.91114599
		 0.18832199 0.43896699 0.19146299 0.43317899 0.229761 0.44971901 0.225263 0.45613399
		 0.357537 0.88536698 0.339059 0.87853003 0.89353198 0.089344002 0.90485698 0.099491
		 0.89563501 0.098862998 0.90484101 0.103189 0.89769202 0.102253 0.89224899 0.101415
		 0.88848299 0.098324001 0.88480097 0.090594001 0.133669 0.43534699 0.13056301 0.441241
		 0.087150998 0.42117301 0.090503998 0.41484401 0.054981999 0.40643501 0.058518998
		 0.39964899 0.061425999 0.39387801 0.093487002 0.40899101 0.13645101 0.42927 0.183806
		 0.45166501 0.18172801 0.45827499 0.22233 0.47132301 0.221102 0.47770801 0.217489
		 0.48362899 0.17877001 0.464158 0.88472599 0.072669998 0.89353198 0.067382 0.88755202
		 0.05731 0.89426202 0.046404999 0.124468 0.45172799 0.171536 0.47385499 0.207735 0.49383
		 0.92124498 0.098356999 0.91408598 0.098880999 0.917458 0.101442 0.91200101 0.102268
		 0.080784 0.433229 0.047862999 0.419947 0.13923199 0.423188 0.185931 0.44509199 0.223267
		 0.46370399 0.06436 0.38811401 0.096461996 0.40313599 0.64702499 0.685314 0.65349501
		 0.68549597 0.661403 0.685781 0.0057629999 0.53821403 0.0067659998 0.54764599 0.010884
		 0.554331 0.64055097 0.68515301 0.016411001 0.55975199 0.022857999 0.56364101 0.63437098
		 0.68512398 0.62175399 0.68571401 0.62820297 0.68534797 0.030119 0.56559402 0.037841
		 0.56582099 0.61530799 0.68610102 0.045499001 0.56415498 0.052967001 0.55833501 0.60742801
		 0.68663698 0.063496001 0.52232403 0.060398001 0.54254901 0.57198101 0.68873698 0.591407
		 0.687617 0.55429298 0.68950701 0.061250001 0.50379199 0.052877001 0.49279499 0.54178703
		 0.689358 0.69687802 0.68675101 0.71454197 0.68696201 0.72700202 0.68641698 0.039181001
		 0.494389 0.024324 0.505629 0.67744499 0.68624902 0.011949 0.5219 0.063065998 0.35095999
		 0.068939 0.336169 0.057810001 0.34892499 0.064668 0.33181599 0.071896002 0.31748599
		 0.076255001 0.31823999 0.075770997 0.30461201 0.078985997 0.292299 0.087931 0.29106599
		 0.083309002 0.30238 0.010572 0.45748299 0.016256999 0.44662499 0.0057629999 0.44984201
		 0.013112 0.43941599 0.020473 0.42813599 0.02386 0.43097699 0.040385999 0.399795 0.032862999
		 0.41381499 0.026776999 0.41336501 0.035436001 0.397098 0.043868002 0.392914 0.039912
		 0.389229 0.043336999 0.38536301 0.046652999 0.38711599 0.044647999 0.37999699 0.047754999
		 0.37356699 0.052439999 0.37574601 0.049663 0.38132399 0.054912001 0.37001699 0.050664
		 0.36703601 0.053924002 0.36256 0.057402 0.36396199 0.054673001 0.357436 0.060044002
		 0.35807699 0.72500598 0.45413601 0.77806097 0.45942801 0.77806598 0.46716699 0.72310501
		 0.461328 0.77807099 0.475447 0.72078699 0.46928501 0.67010498 0.45434201 0.67433
		 0.44595301 0.67787403 0.438043 0.63519698 0.41356999 0.630907 0.41995201 0.62555599
		 0.42592499 0.58019 0.38975999 0.58854401 0.38766599 0.59432298 0.384179 0.92087001
		 0.41339001 0.961707 0.383948 0.96749002 0.387427 0.92516899 0.41976801 0.97584802
		 0.38951099 0.93053001 0.42573699 0.886006 0.45419499 0.88177103 0.445813 0.87821698
		 0.43790799 0.83111 0.45407301 0.83301902 0.461263 0.83534902 0.46921799 0.77807498
		 0.481105 0.719266 0.47476101 0.77807897 0.48753899 0.71783102 0.48107001 0.66451198
		 0.46546 0.667211 0.45974499 0.62165397 0.429618 0.61783499 0.43463299 0.56918001
		 0.39594799 0.57416898 0.39139599 0.98187 0.39114001 0.93443799 0.42942601 0.98686397
		 0.39568499 0.938263 0.43443701 0.89161199 0.46530399 0.888906 0.45959201 0.83687598
		 0.474693 0.838319 0.48100001 0.77808201 0.49346 0.71669102 0.486994 0.77808499 0.49796599;
	setAttr ".uvst[0].uvsp[1500:1749]" 0.71615303 0.49170101 0.660622 0.474787 0.66235799
		 0.470658 0.61474299 0.43997699 0.61198002 0.44401801 0.56484199 0.40730801 0.56612003
		 0.40195 0.98993099 0.401683 0.94136101 0.439778 0.991216 0.407038 0.94412899 0.44381499
		 0.89551401 0.474628 0.89377302 0.47050199 0.83946598 0.486922 0.84000897 0.49162799
		 0.72674 0.441194 0.77805299 0.44594401 0.77805698 0.452645 0.725941 0.447815 0.68062598
		 0.430998 0.68354899 0.424153 0.64236099 0.400617 0.63854897 0.406977 0.59541601 0.377141
		 0.59601402 0.36910599 0.91369098 0.40044701 0.959997 0.36887601 0.96060503 0.376912
		 0.91750997 0.406802 0.87545598 0.43086299 0.87252301 0.42401901 0.82936001 0.44113401
		 0.83016801 0.44775301 0.96976602 0.72195899 0.96569502 0.72459698 0.96518099 0.72215497
		 0.96874499 0.71944898 0.96598101 0.71986699 0.96828002 0.71736503 0.97253698 0.71422201
		 0.97369802 0.716012 0.97524899 0.71840501 0.98072898 0.714849 0.97885501 0.71289098
		 0.97714198 0.71161699 0.98036599 0.71054 0.98278099 0.71074402 0.98479998 0.71221
		 0.98531401 0.71465302 0.98175198 0.71735901 0.98451602 0.71694201 0.982216 0.71944302
		 0.977961 0.72258699 0.97680002 0.72079599 0.971641 0.72391701 0.97335398 0.72519201
		 0.97013098 0.72626901 0.96771502 0.72606403 0.96001297 0.72420299 0.96146798 0.71978599
		 0.96198601 0.719688 0.96060401 0.72395301 0.96263498 0.71955198 0.961357 0.72362798
		 0.96209699 0.72693098 0.96135598 0.72741199 0.96078497 0.72778302 0.96504599 0.71506399
		 0.97029901 0.71077597 0.97045797 0.71102101 0.96541297 0.71513897 0.97065002 0.71131498
		 0.96586698 0.71521598 0.97635698 0.70772499 0.98212802 0.706388 0.98182899 0.70682102
		 0.97627503 0.70809102 0.98144001 0.70735902 0.976161 0.70853698 0.98675501 0.706864
		 0.98970997 0.70902699 0.98913902 0.70939797 0.98628598 0.70730102 0.98839903 0.70987701
		 0.98568302 0.70785701 0.99048001 0.71260703 0.989025 0.71702498 0.98850799 0.71712297
		 0.98988903 0.71285701 0.98785901 0.71725798 0.98913699 0.71318102 0.98544902 0.72174799
		 0.980196 0.72603798 0.98003697 0.72579199 0.98508197 0.72167403 0.979846 0.72549802
		 0.98462701 0.72159702 0.97413701 0.729087 0.96836603 0.730425 0.96866602 0.72999197
		 0.97421902 0.72872198 0.969055 0.72945303 0.97433501 0.728275 0.96373999 0.72994602
		 0.96420902 0.72950798 0.96481198 0.728953 0.96247399 0.72310501 0.96359903 0.71930498
		 0.963853 0.71931899 0.96270299 0.723059 0.96427798 0.71947497 0.96311599 0.72301197
		 0.96377099 0.72584498 0.96342301 0.726071 0.963229 0.72619599 0.96655899 0.71528703
		 0.97093201 0.71174997 0.97107798 0.71197498 0.96677601 0.71540898 0.971389 0.71245402
		 0.96716303 0.71575898 0.97594702 0.70920098 0.98082298 0.70814002 0.98073202 0.708377
		 0.97596902 0.70944899 0.98070198 0.70882797 0.97613102 0.70994401 0.98475099 0.70866197
		 0.987266 0.71061099 0.98707199 0.71073598 0.98461699 0.70885199 0.98672402 0.71096301
		 0.98440498 0.70920998 0.98802 0.71370202 0.98689598 0.71750301 0.98664302 0.71749002
		 0.98779202 0.71374899 0.98621899 0.71733397 0.98737901 0.71379501 0.98393601 0.72152102
		 0.97956502 0.72506201 0.97941798 0.72483701 0.98371899 0.72140002 0.97910798 0.72435701
		 0.98333299 0.72105002 0.97455001 0.72761202 0.96967202 0.72867101 0.96976298 0.72843301
		 0.974527 0.72736299 0.96979398 0.72798198 0.974365 0.72686702 0.96574497 0.72814602
		 0.96587902 0.727956 0.96609002 0.72759902 0.982216 0.70605201 0.98697001 0.70656699
		 0.94536901 0.0058510001 0.94531399 0.006972 0.94082898 0.0068970001 0.94087303 0.0057629999
		 0.94035602 0.083524004 0.93890399 0.086746 0.93787003 0.086318001 0.93931597 0.083071999
		 0.990022 0.70882499 0.97012901 0.71051198 0.97631198 0.70740402 0.95755702 0.006817
		 0.95747602 0.0079420004 0.95100302 0.0073680002 0.951078 0.006244 0.96112502 0.71972799
		 0.96477097 0.71489203 0.96964002 0.0076390002 0.96958601 0.0087679997 0.96393698
		 0.0084560001 0.96400702 0.0073299999 0.98547101 0.084053002 0.98435402 0.084206 0.98386401
		 0.080728002 0.98497701 0.080558002 0.97404301 0.0088839997 0.97408903 0.007762 0.95965397
		 0.72427702 0.960473 0.72798502 0.968279 0.730762 0.963525 0.73024398 0.98419797 0.17050999
		 0.98409897 0.169385 0.98855102 0.16910601 0.98863798 0.170224 0.98481703 0.087687001
		 0.98593301 0.087553002 0.96433097 0.72548199 0.96641201 0.72695202 0.96976697 0.727135
		 0.97405398 0.72600502 0.97857398 0.72353297 0.98267198 0.720415 0.985457 0.71696502
		 0.98665601 0.713826 0.98616499 0.711326 0.984083 0.70985597 0.98072898 0.70967299
		 0.97644198 0.71080601 0.97192401 0.71327698 0.96782398 0.71639401 0.96504003 0.719845
		 0.96383899 0.72298199 0.96278298 0.72648501 0.96537203 0.72845 0.96942502 0.72896999
		 0.97445297 0.72787398 0.97967702 0.725236 0.98421401 0.72153997 0.98726898 0.71739799
		 0.98844898 0.71348703 0.98771203 0.71032202 0.98512298 0.70835799 0.981071 0.70784098
		 0.976044 0.70893902 0.97082001 0.71157598 0.966281 0.71526998 0.96322501 0.71941102
		 0.962044 0.72332102 0.95728898 0.010497 0.96377498 0.011011 0.969468 0.011336 0.96966499
		 0.014732 0.96429598 0.014411 0.95816702 0.013938 0.94519699 0.0094910003 0.95082998
		 0.0099170003 0.95220298 0.013389 0.94705802 0.012899 0.94123799 0.087713003 0.94269198
		 0.084628001 0.94074601 0.0095030004 0.94294 0.013114 0.94657397 0.085285999 0.94483399
		 0.089207999 0.97050399 0.018391 0.96626902 0.018122001 0.97128803 0.023898 0.96812701
		 0.023728 0.96424598 0.023502 0.96128899 0.017749 0.95298398 0.017077999 0.949781
		 0.017501 0.95876199 0.023282001;
	setAttr ".uvst[0].uvsp[1750:1999]" 0.95651102 0.023557 0.95990902 0.077852003
		 0.961146 0.078588001 0.95477098 0.085259996 0.95388597 0.083608001 0.95678198 0.017371999
		 0.96117401 0.023343001 0.97166699 0.16831701 0.97201699 0.170837 0.96558398 0.171764
		 0.96516401 0.16924299 0.95999801 0.172731 0.95950401 0.170202 0.95901501 0.167154
		 0.964724 0.16624001 0.97125298 0.165334 0.98388702 0.16681799 0.97846103 0.169974
		 0.97817898 0.167436 0.97778302 0.164427 0.98352599 0.163753 0.97418201 0.72940803
		 0.98036599 0.72630101 0.97217298 0.171951 0.97858697 0.171094 0.98572499 0.72192103
		 0.98936802 0.71708298 0.96020901 0.173841 0.96576703 0.172876 0.990839 0.71253401
		 0.93765301 0.090049997 0.93659902 0.089635998 0.955639 0.173676 0.95586002 0.174776
		 0.97394902 0.011403 0.98179102 0.084556997 0.98138702 0.081175998 0.97796899 0.085105002
		 0.97842699 0.081491999 0.97374898 0.014676 0.95512301 0.171206 0.94008702 0.090906002
		 0.94261903 0.092106998 0.95463198 0.16825099 0.97967398 0.088441998 0.98230898 0.087922998
		 0.98837501 0.16659001 0.988042 0.163614 0.96425903 0.050905 0.96436101 0.049518 0.96730298
		 0.049465999 0.96715897 0.050751001 0.96916097 0.048983 0.96958703 0.049961999 0.96957701
		 0.051217999 0.968804 0.051890999 0.967035 0.051594999 0.96425402 0.051872 0.96199
		 0.049562 0.96157998 0.051029 0.961559 0.052147001 0.96032298 0.052967001 0.95990002
		 0.052418999 0.95969701 0.049885001 0.96065098 0.048999999 0.97334099 0.018199001
		 0.97490197 0.081795 0.97617698 0.080417998 0.97335702 0.076394998 0.97530597 0.075701997
		 0.97307599 0.023799 0.97049803 0.086309999 0.97080201 0.083094001 0.96199799 0.087554999
		 0.96484399 0.084146999 0.96704203 0.077772997 0.97057599 0.077122003 0.95348102 0.088587999
		 0.95887798 0.08512 0.963512 0.078351997 0.974114 0.122384 0.978333 0.120957 0.97961998
		 0.122162 0.973894 0.125054 0.98016798 0.12617999 0.97742099 0.128371 0.97299099 0.128336
		 0.96622902 0.129279 0.96578598 0.12605099 0.96544999 0.123515 0.95139599 0.12579399
		 0.95234197 0.124306 0.95678598 0.12465 0.95768797 0.127216 0.95946199 0.13016701
		 0.95521402 0.131421 0.95196903 0.13007 0.97681701 0.088474996 0.970429 0.088561997
		 0.97711098 0.093318 0.97091001 0.093630001 0.96175897 0.094697997 0.96115202 0.089755997
		 0.98290801 0.16100501 0.98735398 0.16088299 0.982117 0.15630101 0.98653698 0.155982
		 0.97951102 0.093516 0.97866601 0.089722 0.97087401 0.162605 0.97729701 0.161698 0.970222
		 0.157924 0.976565 0.15702599 0.94556499 0.092357002 0.94367099 0.093916997 0.946468
		 0.097138003 0.944179 0.097942002 0.95399803 0.16049699 0.958336 0.15959799 0.95885998
		 0.16434 0.95454901 0.165436 0.95187098 0.090966001 0.952609 0.095955998 0.96444702
		 0.16348 0.963875 0.158785 0.97826701 0.10257 0.97205001 0.103129 0.97957402 0.112355
		 0.973297 0.112979 0.96422398 0.114046 0.96295601 0.104166 0.98092502 0.147009 0.98534501
		 0.146377 0.97947198 0.137234 0.98380601 0.13624001 0.98205501 0.111964 0.98071301
		 0.102344 0.968943 0.14874201 0.975326 0.147834 0.96759599 0.139092 0.97393203 0.138181
		 0.94771898 0.106434 0.94529402 0.106829 0.94894898 0.116259 0.94645101 0.116505 0.951226
		 0.140754 0.95566797 0.140526 0.95694399 0.150333 0.952519 0.150931 0.95388597 0.10543
		 0.95517802 0.115292 0.96255398 0.14960299 0.96125299 0.13993099 0.97995299 0.118425
		 0.97398102 0.119188 0.96503001 0.12029 0.97850198 0.13108 0.98266602 0.12967101 0.98216498
		 0.118549 0.966748 0.133009 0.97305 0.132103 0.95012999 0.122247 0.94802099 0.122921
		 0.95491701 0.134332 0.95052701 0.134119 0.95609701 0.121494 0.960437 0.13383 0.96652597
		 0.068227999 0.96369702 0.068640999 0.96174502 0.068829 0.96042699 0.059707999 0.96237898
		 0.059480999 0.96521199 0.059101999 0.97175199 0.067286998 0.96934801 0.067754 0.96804702
		 0.058674 0.97044498 0.058295 0.97123098 0.033131 0.97261101 0.033218 0.97369701 0.066896997
		 0.97221899 0.05807 0.97177303 0.042451002 0.97068202 0.042211998 0.96843898 0.032981999
		 0.968009 0.042048 0.96464503 0.041949999 0.96494299 0.03283 0.96239901 0.032756001
		 0.96219403 0.041926 0.96041203 0.041993 0.960522 0.032777 0.95873302 0.032869998
		 0.958848 0.04214 0.95939398 0.059863999 0.960473 0.068774998 0.96451902 0.054101001
		 0.96174598 0.054435998 0.95987898 0.054699 0.96960902 0.053389002 0.967314 0.05373
		 0.97006601 0.047182001 0.9709 0.047610998 0.97111499 0.053231001 0.96759701 0.047042001
		 0.96448499 0.047012001 0.96215701 0.047017999 0.96045202 0.047111999 0.95905203 0.047385
		 0.95904398 0.054792002 0.029909 0.052625999 0.029399 0.049966 0.033861998 0.050294999
		 0.034761 0.052622002 0.028653 0.047967002 0.031945001 0.048811998 0.023372 0.049780998
		 0.023372 0.047648001 0.023375001 0.05263 0.01684 0.052632999 0.017346 0.049972001
		 0.018089 0.047972001 0.012883 0.050303999 0.014799 0.04882 0.011987 0.052632999 0.017349999
		 0.055293001 0.012887 0.054960001 0.018095 0.057291999 0.014805 0.056442998 0.023375999
		 0.055479001 0.023378 0.057611998 0.029402999 0.055287998 0.028659999 0.057289001
		 0.033865001 0.054949999 0.031948999 0.056435999 0.039312001 0.049194999 0.038679
		 0.049307998 0.035197001 0.046484001 0.035684999 0.046282999 0.037870999 0.049446002
		 0.034577999 0.046725001 0.039935 0.052618001 0.039051998 0.052618999 0.040617 0.052616999
		 0.030111 0.044273999 0.029843999 0.044535998 0.023368999 0.043834001 0.023367999
		 0.043542001 0.029503999 0.044849001 0.023368999 0.044183999 0.016626 0.044280998;
	setAttr ".uvst[0].uvsp[2000:2249]" 0.016894 0.044542 0.011543 0.046496999 0.011056
		 0.046298001 0.017233999 0.044854999 0.012162 0.046737 0.0074319998 0.049215 0.0080650002
		 0.049325999 0.0068160002 0.052639 0.0061340001 0.052639 0.0088740001 0.049463 0.0076979999
		 0.052638002 0.0074410001 0.056060001 0.0080730002 0.055948 0.011556 0.058770999 0.01107
		 0.058972001 0.0088820001 0.055808999 0.012175 0.058531001 0.016641 0.060986001 0.016907999
		 0.060722999 0.023383001 0.061427001 0.023383999 0.061719999 0.017247001 0.06041 0.023383001
		 0.061076999 0.030126 0.06098 0.029858001 0.060717002 0.035207 0.058759999 0.035696
		 0.058961 0.029517001 0.060405001 0.034587 0.058520999 0.039317001 0.056040999 0.038683999
		 0.05593 0.037875 0.055792999 0.036649 0.049614999 0.036432002 0.049701001 0.033429001
		 0.047192998 0.033635002 0.047044002 0.036061998 0.049885999 0.033158999 0.047556002
		 0.037470002 0.052620001 0.037055999 0.052620001 0.037702002 0.052618999 0.028964
		 0.045288 0.028847 0.045508999 0.023368999 0.044971 0.023368999 0.044702001 0.028713999
		 0.046013001 0.02337 0.045541 0.017774999 0.045294002 0.017891999 0.045515001 0.013312
		 0.047203001 0.013105 0.047054 0.018026 0.046018001 0.013582 0.047564998 0.010095
		 0.049630001 0.010312 0.049716 0.0092780003 0.052634999 0.0090469997 0.052634999 0.010683
		 0.049899999 0.0096930005 0.052634001 0.010102 0.055638 0.010319 0.055551 0.013321
		 0.058060002 0.013117 0.05821 0.01069 0.055365 0.013592 0.057698 0.017786 0.059971001
		 0.017902 0.059749998 0.023381 0.060288999 0.023381 0.060557999 0.018035 0.059246998
		 0.023381 0.059719 0.028976001 0.059967 0.028860001 0.059746999 0.033438001 0.058052
		 0.033643 0.058201998 0.028725 0.059243001 0.033167001 0.057691 0.036653001 0.055624001
		 0.036435999 0.055537999 0.036065001 0.055353001 0.0070910002 0.049081001 0.010799
		 0.046062998 0.62536103 0.82205701 0.629798 0.82132602 0.63000399 0.822442 0.62557602
		 0.82315898 0.641123 0.898287 0.64208698 0.89769202 0.64398199 0.90069801 0.64301902
		 0.90126902 0.0057629999 0.052639 0.016488999 0.043986 0.023367999 0.043226998 0.61343801
		 0.82476199 0.61976802 0.82326299 0.62000299 0.82436597 0.61368001 0.82586199 0.030247999
		 0.043979999 0.035941001 0.046046998 0.60159397 0.82729602 0.60712701 0.82618999 0.60735703
		 0.82729298 0.60180801 0.82840401 0.59654701 0.90514702 0.596542 0.90161699 0.59766698
		 0.90162802 0.59767401 0.90513998 0.597413 0.82915401 0.59720701 0.82805097 0.039652999
		 0.049061 0.040989 0.052616999 0.039659001 0.056175001 0.035952002 0.059195999 0.60963899
		 0.99048603 0.60520101 0.990794 0.605138 0.98967397 0.60958701 0.98935699 0.597709
		 0.90865201 0.59658402 0.90867603 0.035441998 0.054986 0.036387999 0.052620001 0.032726999
		 0.056965999 0.028516 0.058350999 0.02338 0.058736999 0.018243 0.058354001 0.014029
		 0.056972999 0.011311 0.054997999 0.010361 0.052634001 0.011306 0.050268002 0.014021
		 0.048289001 0.018235 0.046911001 0.023371 0.046523001 0.028507 0.046905 0.032721002
		 0.048280999 0.035439 0.050255999 0.037131999 0.055675998 0.038233001 0.052618999
		 0.034015 0.058316 0.029201001 0.060132001 0.023382001 0.060764998 0.017563 0.060137
		 0.012747 0.058325 0.0096249999 0.055691 0.0085159997 0.052636001 0.0096169999 0.049578998
		 0.012735 0.046939999 0.017550999 0.045127999 0.023368999 0.044496 0.029188 0.045120999
		 0.034006 0.046929002 0.037126999 0.049564 0.61422998 0.82836199 0.60788202 0.82979703
		 0.60229099 0.83092803 0.60785198 0.833233 0.60258102 0.83431399 0.61385399 0.831891
		 0.62605298 0.82563502 0.62053901 0.82686299 0.61967897 0.83049601 0.62470102 0.829274
		 0.64085102 0.90255898 0.63897198 0.89971203 0.63045901 0.82500899 0.62880599 0.828897
		 0.63522798 0.90091503 0.63750702 0.904549 0.60642999 0.83718401 0.602274 0.83805102
		 0.605389 0.84299397 0.60228097 0.84360898 0.61130899 0.83610803 0.60920101 0.84222102
		 0.622666 0.83421999 0.61943501 0.83425897 0.61687201 0.84118098 0.614604 0.84122801
		 0.62099302 0.895437 0.62775999 0.90028697 0.62711698 0.90204602 0.61987197 0.89633501
		 0.61571699 0.83509201 0.61222303 0.84162998 0.62176502 0.986642 0.62833399 0.98669201
		 0.62825203 0.98924702 0.62175298 0.989187 0.63407099 0.98689198 0.63391602 0.989465
		 0.628371 0.98365498 0.634152 0.983805 0.62177902 0.98362797 0.609456 0.98678398 0.61519498
		 0.986637 0.61525297 0.98919302 0.61518699 0.98359901 0.60940599 0.98369598 0.030262999
		 0.061274 0.023383999 0.062033001 0.62174797 0.99031299 0.61527801 0.99032098 0.016504001
		 0.061280001 0.010813 0.059207998 0.63385397 0.99059302 0.628218 0.99037403 0.0071
		 0.056194 0.64571297 0.90380102 0.64472902 0.90436101 0.63836199 0.989824 0.63828897
		 0.99094403 0.59786397 0.83163202 0.60018402 0.90171999 0.60026199 0.90512502 0.60316098
		 0.90161198 0.60412699 0.90512699 0.59852999 0.83484101 0.638547 0.98730701 0.64244401
		 0.90555501 0.64011103 0.907103 0.638641 0.98431098 0.60290802 0.90867299 0.60022599
		 0.90853101 0.60497701 0.98715597 0.60491103 0.98415899 0.61302102 0.86935103 0.610129
		 0.86959898 0.60980803 0.86835098 0.61272901 0.86799198 0.60761702 0.86916399 0.60790098
		 0.86813498 0.607795 0.870386 0.61036599 0.87041402 0.60865301 0.87094599 0.613159
		 0.870305 0.61508298 0.867706 0.61569101 0.86910301 0.61586499 0.87020802 0.61754602
		 0.87024897 0.61720198 0.87085003 0.61739898 0.86770803 0.61633199 0.86696303 0.599437
		 0.838265 0.60523999 0.90022701 0.60669798 0.90141499 0.60545802 0.89543003 0.607485
		 0.89584798 0.60049599 0.84376401 0.61094302 0.90213197 0.61169499 0.90527499 0.61698997
		 0.90234601;
	setAttr ".uvst[0].uvsp[2250:2499]" 0.62028599 0.905321 0.61033899 0.89618403
		 0.61392802 0.89634103 0.62303299 0.90248102 0.62886298 0.90515298 0.61750102 0.89642799
		 0.61320698 0.94148201 0.61379898 0.94408202 0.60772502 0.94204497 0.60882902 0.940669
		 0.60773301 0.94601798 0.615134 0.94720298 0.61074698 0.94782501 0.62196302 0.94396299
		 0.621961 0.94722199 0.62194401 0.941405 0.63617897 0.94169801 0.63014799 0.94401199
		 0.63068098 0.94133198 0.635032 0.940359 0.62878501 0.94718701 0.636199 0.94609702
		 0.63316101 0.94786698 0.612082 0.90749502 0.605744 0.908306 0.61232197 0.91258198
		 0.606139 0.91314501 0.62143499 0.90737998 0.62153298 0.91235697 0.60522997 0.98136002
		 0.609653 0.98088902 0.605389 0.97639298 0.60981202 0.97612101 0.60378999 0.91368097
		 0.604087 0.90980202 0.61530602 0.98082799 0.62179297 0.98087102 0.61540997 0.97610098
		 0.62181598 0.97614402 0.63932598 0.90904498 0.63723201 0.90776801 0.63939399 0.9131
		 0.63701397 0.91262603 0.63823998 0.97653699 0.63835001 0.98150802 0.63393199 0.98099399
		 0.63382101 0.97622299 0.63079399 0.90727901 0.630768 0.91232002 0.62827998 0.980883
		 0.62822199 0.97615302 0.61253703 0.92214602 0.60630399 0.92246801 0.61269301 0.93207097
		 0.60639 0.93233597 0.62168598 0.92189801 0.62182498 0.931858 0.60529399 0.966717
		 0.60975802 0.96675497 0.60546798 0.95646799 0.60989702 0.95687503 0.60387802 0.932298
		 0.60385001 0.92258799 0.615417 0.96682602 0.62186402 0.96687502 0.61551398 0.957075
		 0.62191403 0.95713103 0.63954699 0.92205602 0.63709098 0.92200601 0.639768 0.93179899
		 0.63726002 0.93190801 0.638358 0.95659399 0.63843298 0.96685499 0.633968 0.96685201
		 0.63392597 0.95696002 0.63084501 0.92187899 0.63095701 0.931826 0.62831002 0.96687502
		 0.62831199 0.95711398 0.61288899 0.938308 0.60686803 0.93839598 0.62190598 0.93815303
		 0.60572302 0.94980901 0.61003703 0.95064998 0.60469502 0.93883097 0.615578 0.95093697
		 0.62194401 0.95098799 0.63911599 0.938375 0.63693303 0.93800598 0.63816702 0.94992101
		 0.63384402 0.95071697 0.63091999 0.93810201 0.62830698 0.95095402 0.613132 0.88681698
		 0.61598903 0.88683498 0.617948 0.88675302 0.61605102 0.87758398 0.61801702 0.87754101
		 0.61319202 0.87759697 0.60782599 0.886603 0.61027199 0.88673502 0.61032599 0.877563
		 0.60789901 0.877514 0.60364097 0.852736 0.60228503 0.85301697 0.60584599 0.88648403
		 0.60610998 0.87753397 0.60440397 0.862037 0.60545099 0.86164802 0.60638601 0.85219502
		 0.60807598 0.86110997 0.60982901 0.85155201 0.611395 0.860542 0.61233997 0.85112202
		 0.613819 0.860174 0.61420202 0.850878 0.61559302 0.85999101 0.61598599 0.85071898
		 0.61716199 0.859918 0.61905903 0.877554 0.61919999 0.886527 0.613199 0.872549 0.615991
		 0.8725 0.61787802 0.872504 0.60805899 0.87254 0.61037898 0.87256402 0.60675299 0.86648101
		 0.60598701 0.86702198 0.60654598 0.87258899 0.60917997 0.86599499 0.612257 0.865529
		 0.61456501 0.86521101 0.61626703 0.86506599 0.61769098 0.86514097 0.61871701 0.87248403
		 0.026289999 0.965684 0.029336 0.96647602 0.030127 0.969522 0.026314 0.96949703 0.030487999
		 0.97341001 0.026527001 0.973409 0.022402 0.973409 0.022402 0.969284 0.022402 0.96532297
		 0.030127 0.97729802 0.029336 0.980344 0.026291 0.981134 0.026314 0.97732103 0.022403
		 0.98149598 0.022402 0.977534 0.018513 0.981134 0.015468 0.980344 0.014676 0.97729802
		 0.018488999 0.97732103 0.014314 0.97341001 0.018276 0.973409 0.014676 0.969522 0.015468
		 0.96647602 0.018513 0.965684 0.018488999 0.96949703 0.031456999 0.98246503 0.027309
		 0.98485702 0.034246001 0.98525399 0.028922999 0.98876703 0.022403 0.99004698 0.022403
		 0.98553598 0.84619999 0.066040002 0.84832197 0.063918002 0.851367 0.064709 0.85034901
		 0.068430997 0.85525602 0.065068997 0.85525602 0.069109999 0.85525602 0.073620997
		 0.84873497 0.072342001 0.84341103 0.068828002 0.85914499 0.064708002 0.86219001 0.063917004
		 0.86431199 0.066039003 0.86016297 0.068430997 0.8671 0.068827003 0.86177701 0.072342001
		 0.013346 0.98246503 0.017495001 0.98485702 0.015881 0.98876798 0.010557 0.98525399
		 0.84753001 0.060873002 0.85134298 0.060895 0.84716803 0.056984 0.851129 0.056984
		 0.85525501 0.056984 0.85525501 0.061108001 0.84753001 0.053096 0.84832197 0.050051
		 0.851367 0.049258001 0.85134298 0.053072002 0.85525602 0.048896998 0.85525501 0.052857999
		 0.85914499 0.049258001 0.86219001 0.050050002 0.86298001 0.053095002 0.85916698 0.053072002
		 0.86334199 0.056984 0.85938102 0.056984 0.86298001 0.060872 0.85916698 0.060895 0.84619999
		 0.047929 0.85034901 0.045536999 0.84341103 0.045139998 0.84873402 0.041625999 0.85525501
		 0.040346 0.85525602 0.044856999 0.031458002 0.96435499 0.027309 0.96196198 0.022402
		 0.96128201 0.022402 0.95677298 0.028922999 0.95805198 0.034246001 0.96156502 0.013346
		 0.96435499 0.017494 0.96196198 0.010557 0.96156502 0.01588 0.95805198 0.86431199
		 0.047928002 0.86016297 0.045536999 0.86177701 0.041625999 0.8671 0.045139 0.843808
		 0.052076999 0.84312701 0.056984 0.83861703 0.056984 0.83989698 0.050462 0.843808
		 0.061891001 0.83989698 0.063505001 0.033849999 0.97831798 0.034527998 0.97341102
		 0.039039001 0.97341102 0.037760001 0.97993201 0.033849999 0.968503 0.037760999 0.96688902
		 0.010954 0.96850199 0.010273 0.97341001 0.0057629999 0.973409 0.0070429998 0.96688801
		 0.010954 0.97831702 0.0070429998 0.979931 0.86670297 0.061889999 0.867383 0.056984
		 0.871894 0.056984 0.87061399 0.063504003 0.86670297 0.052076001 0.87061399 0.050462
		 0.010735 0.021229001;
	setAttr ".uvst[0].uvsp[2500:2676]" 0.0091819996 0.017748 0.010362 0.016726 0.011973
		 0.020932 0.012908 0.015531 0.014563 0.020313 0.015336 0.025291 0.012505 0.025397001
		 0.010976 0.025032001 0.055024002 0.017127 0.05604 0.01348 0.058912002 0.01163 0.059032001
		 0.017091 0.061911002 0.013387 0.063116997 0.017072 0.061939001 0.020764001 0.058997001
		 0.022559 0.056138001 0.020746 0.103485 0.019611999 0.105041 0.014779 0.107598 0.01595
		 0.106063 0.020199001 0.108791 0.016965 0.10729 0.020479999 0.107075 0.024294 0.105546
		 0.024681 0.102714 0.024611 0.112185 0.015351 0.11185 0.021539999 0.0057629999 0.016183
		 0.006178 0.022343 0.0086589996 0.027992001 0.109414 0.027233999 0.110984 0.012595
		 0.107273 0.010183 0.01056 0.010995 0.0069149998 0.013435 0.010939 0.029894 0.015279
		 0.030385001 0.102757 0.029707 0.10713 0.029173 0.053948998 0.026931999 0.052604999
		 0.022055 0.055103999 0.021555001 0.058867998 0.024991 0.062937997 0.021594999 0.065560997
		 0.021992 0.063605003 0.026735 0.062936999 0.012551 0.064061001 0.017086999 0.065926
		 0.01171 0.066574998 0.016891999 0.063940004 0.0069269999 0.058754001 0.0091890004
		 0.054953001 0.012698 0.052062001 0.011862 0.053323001 0.0069090002 0.051669002 0.017003
		 0.053985 0.017154999 0.023455 0.023700999 0.024150001 0.028926 0.034124002 0.022043001
		 0.034603 0.027321 0.083057001 0.026644001 0.083996996 0.021458 0.094612002 0.023058999
		 0.093760997 0.028247001 0.085318998 0.016291 0.086441003 0.011149 0.097185999 0.012702
		 0.095775999 0.017843001 0.098407999 0.0076239998 0.086864002 0.0059750001 0.030515
		 0.0064289998 0.031516001 0.011571 0.020746 0.013316 0.019252 0.0082839997 0.022315999
		 0.018448001 0.032883 0.016752999 0.044684999 0.021919001 0.044939999 0.02722 0.073492996
		 0.02156 0.072529003 0.026765 0.074978001 0.010986 0.074597001 0.016295999 0.07446
		 0.0057629999 0.043021001 0.01123 0.042714 0.0059830002 0.043692999 0.016559999 0.80638301
		 0.015305 0.80765897 0.015674001 0.805709 0.019748 0.80462098 0.018697999 0.81155598
		 0.016094999 0.80956399 0.020852 0.80832601 0.011141 0.81245297 0.011024 0.80672199
		 0.011506 0.86081702 0.017565001 0.86481398 0.017563 0.86474502 0.023023 0.86185402
		 0.021206001 0.86890101 0.017539 0.86772799 0.021235 0.86471498 0.012093 0.86766702
		 0.013864 0.86188501 0.013931 0.91852099 0.015811 0.92225999 0.015385 0.92389798 0.019405
		 0.92018998 0.020664001 0.92335701 0.015044 0.92497498 0.018477 0.92123401 0.011089
		 0.92277801 0.011302 0.91720599 0.01086 0.92778701 0.01378 0.92825198 0.020186 0.801153
		 0.020152999 0.801826 0.014062 0.80435902 0.0085500004 0.92479599 0.0081639998 0.92668402
		 0.023065999 0.92146701 0.025605001 0.80712497 0.025382999 0.80219799 0.022941001
		 0.806472 0.006662 0.811791 0.0058929999 0.91655302 0.0057629999 0.922158 0.0064590001
		 0.85962701 0.0077379998 0.86455297 0.0096629998 0.86084402 0.013127 0.85836399 0.012637
		 0.86864603 0.013029 0.86919999 0.0078910002 0.87124902 0.012601 0.86985099 0.017514
		 0.86876702 0.022054 0.87237799 0.017673001 0.87178499 0.022845 0.86983502 0.027641
		 0.86460298 0.025469 0.86076701 0.022002 0.85911602 0.027806001 0.85786903 0.022857999
		 0.85748702 0.017712999 0.85978597 0.017546 0.82423198 0.0068379999 0.82430702 0.012081
		 0.83865398 0.0078159999 0.838723 0.013068 0.88971001 0.0079779997 0.90402901 0.0069260001
		 0.90538901 0.011984 0.89096999 0.013081 0.89262199 0.01815 0.90703398 0.017059 0.90850502
		 0.022222999 0.89380503 0.023313001 0.908849 0.027454 0.89377302 0.028542001 0.83474702
		 0.028357999 0.81960702 0.027224001 0.821284 0.022256 0.83595198 0.023289001 0.823129
		 0.017258 0.83751202 0.018254001 0.85053003 0.007555 0.85050201 0.012844 0.878021
		 0.007708 0.87916601 0.012889 0.880458 0.018130001 0.88089901 0.023445999 0.88028902
		 0.028674999 0.84839702 0.028701 0.84881198 0.023481 0.849567 0.018192001;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2260 ".vt";
	setAttr ".vt[0:165]"  -0.51544309 10.1939249 0.25711024 0.51544309 10.1939249 0.25711024
		 -0.5020839 11.22047424 0.24375105 0.5020839 11.22047424 0.24375105 -0.5020839 11.22047424 -0.76041675
		 0.5020839 11.22047424 -0.76041675 -0.51544309 10.1939249 -0.77377594 0.51544309 10.1939249 -0.77377594
		 0 10.11188412 0.44907236 0 11.35255623 0.37583351 0 11.35255623 -0.89249909 0 10.11188412 -0.96573818
		 -0.63416624 10.71838951 0.37583351 0.63416624 10.71838951 0.37583351 -0.63416624 11.35255623 -0.25833285
		 0.63416624 11.35255623 -0.25833285 -0.63416624 10.71838951 -0.89249909 0.63416624 10.71838951 -0.89249909
		 -0.70740533 10.11188412 -0.25833285 0.70740533 10.11188412 -0.25833285 0 10.71838951 0.60776186
		 0 11.5844841 -0.25833285 0 10.71838951 -1.12442756 0.86609483 10.71838951 -0.25833285
		 -0.86609483 10.71838951 -0.25833285 0 9.92865276 -0.85744429 0.435987 10.0017442703 -0.69431984
		 0.59911144 9.92865276 -0.25833285 0.435987 10.0017442703 0.17765415 0 9.92865276 0.34077859
		 -0.435987 10.0017442703 0.17765415 -0.59911144 9.92865276 -0.25833285 -0.435987 10.0017442703 -0.69431984
		 0 9.40118217 -0.77100611 0.39254189 9.48644733 -0.65087473 0.51267326 9.40118217 -0.25833285
		 0 9.32788563 -0.25833285 0.39254189 9.48644733 0.13420904 0 9.40118217 0.25434041
		 -0.39254189 9.48644733 0.13420904 -0.51267326 9.40118217 -0.25833285 -0.39254189 9.48644733 -0.65087473
		 -0.28061807 10.15290451 0.40034938 0.28061807 10.15290451 0.40034938 -0.29782695 11.30890846 0.33218622
		 0.29782695 11.30890846 0.33218622 -0.29782695 11.30890846 -0.84885192 0.29782695 11.30890846 -0.84885192
		 -0.28061807 10.15290451 -0.91701508 0.28061807 10.15290451 -0.91701508 -0.59051907 10.42056274 0.33218622
		 -0.59051907 11.016216278 0.33218622 0.59051907 10.42056274 0.33218622 0.59051907 11.016216278 0.33218622
		 -0.59051907 11.30890846 0.039494157 -0.59051907 11.30890846 -0.55615979 0.59051907 11.30890846 0.039494157
		 0.59051907 11.30890846 -0.55615979 -0.59051907 11.016216278 -0.84885192 -0.59051907 10.42056274 -0.84885192
		 0.59051907 11.016216278 -0.84885192 0.59051907 10.42056274 -0.84885192 -0.65868223 10.15290451 -0.53895092
		 -0.65868223 10.15290451 0.022285223 0.65868223 10.15290451 -0.53895092 0.65868223 10.15290451 0.022285223
		 0 10.3726368 0.54956579 0.34575331 10.71838951 0.54956579 0 11.064143181 0.54956579
		 -0.34575331 10.71838951 0.54956579 0 11.52628899 0.087420464 0.34575331 11.52628899 -0.25833285
		 0 11.52628899 -0.60408616 -0.34575331 11.52628899 -0.25833285 0 11.064143181 -1.066231489
		 0.34575331 10.71838951 -1.066231489 0 10.3726368 -1.066231489 -0.34575331 10.71838951 -1.066231489
		 0.80789876 10.3726368 -0.25833285 0.80789876 10.71838951 -0.60408616 0.80789876 11.064143181 -0.25833285
		 0.80789876 10.71838951 0.087420464 -0.80789876 10.3726368 -0.25833285 -0.80789876 10.71838951 0.087420464
		 -0.80789876 11.064143181 -0.25833285 -0.80789876 10.71838951 -0.60408616 0 10.021133423 -0.89704216
		 0.46480322 10.10966682 -0.72313607 0.23727196 9.96519852 -0.81592178 0.63870931 10.021133423 -0.25833285
		 0.55758893 9.96519852 -0.49560481 0.46480322 10.10966682 0.20647037 0.55758893 9.96519852 -0.021060884
		 0 10.021133423 0.38037658 0.23727196 9.96519852 0.29925609 -0.46480322 10.10966682 0.20647037
		 -0.23727196 9.96519852 0.29925609 -0.63870931 10.021133423 -0.25833285 -0.55758893 9.96519852 -0.021060884
		 -0.46480322 10.10966682 -0.72313607 -0.55758893 9.96519852 -0.49560481 -0.23727196 9.96519852 -0.81592178
		 0 9.66271114 -0.84424484 0.42638159 9.70411301 -0.68471444 0.24219644 9.40582848 -0.73547423
		 0.58591199 9.66271114 -0.25833285 0.47714138 9.40582848 -0.50052929 0.29295599 9.32193661 -0.25833285
		 0 9.32193661 -0.55128884 0.42638159 9.70411301 0.16804874 0.47714138 9.40582848 -0.016136408
		 0 9.66271114 0.32757902 0.24219644 9.40582848 0.21880853 0 9.32193661 0.034623146
		 -0.42638159 9.70411301 0.16804874 -0.24219644 9.40582848 0.21880853 -0.58591199 9.66271114 -0.25833285
		 -0.47714138 9.40582848 -0.016136408 -0.29295599 9.32193661 -0.25833285 -0.42638159 9.70411301 -0.68471444
		 -0.47714138 9.40582848 -0.50052929 -0.24219644 9.40582848 -0.73547423 0.32179028 10.39659977 0.49479318
		 0.32179028 11.040180206 0.49479318 -0.32179028 11.040180206 0.49479318 -0.32179028 10.39659977 0.49479318
		 0.32179028 11.47151566 0.063457489 0.32179028 11.47151566 -0.58012313 -0.32179028 11.47151566 -0.58012313
		 -0.32179028 11.47151566 0.063457489 0.32179028 11.040180206 -1.011458874 0.32179028 10.39659977 -1.011458874
		 -0.32179028 10.39659977 -1.011458874 -0.32179028 11.040180206 -1.011458874 0.27265203 9.3182354 -0.53098488
		 0.27265203 9.3182354 0.014319181 -0.27265203 9.3182354 0.014319181 -0.27265203 9.3182354 -0.53098488
		 0.75312614 10.39659977 -0.58012313 0.75312614 11.040180206 -0.58012313 0.75312614 11.040180206 0.063457489
		 0.75312614 10.39659977 0.063457489 -0.75312614 10.39659977 0.063457489 -0.75312614 11.040180206 0.063457489
		 -0.75312614 11.040180206 -0.58012313 -0.75312614 10.39659977 -0.58012313 0.25295424 10.065400124 -0.8527751
		 0.59444225 10.065400124 -0.51128709 0.59444225 10.065400124 -0.0053786039 0.25295424 10.065400124 0.3361094
		 -0.25295424 10.065400124 0.3361094 -0.59444225 10.065400124 -0.0053786039 -0.59444225 10.065400124 -0.51128709
		 -0.25295424 10.065400124 -0.8527751 0.23204434 9.68341255 -0.80363715 0.5453043 9.68341255 -0.49037719
		 0.5453043 9.68341255 -0.026288509 0.23204434 9.68341255 0.28697145 -0.23204434 9.68341255 0.28697145
		 -0.5453043 9.68341255 -0.026288509 -0.5453043 9.68341255 -0.49037719 -0.23204434 9.68341255 -0.80363715
		 0.68470478 11.093387604 -0.94303751 0 11.10235405 -1.093819857 -0.68470478 11.093388557 -0.94303751
		 -0.83548689 11.10289955 -0.25833285;
	setAttr ".vt[166:331]" -0.68470478 11.10176754 0.42637181 0 11.11230659 0.57715416
		 0.68470502 11.1040554 0.42637181 0.83548713 11.10311508 -0.25833285 0.099639922 12.53326035 -0.93748176
		 0 12.49774647 -0.96192682 -0.099639922 12.53326035 -0.93748176 -0.11801192 12.53403568 -1.036872387
		 -0.099639922 12.6232214 -1.028494358 0 12.57032681 -1.11181784 0.099639922 12.6232214 -1.028494358
		 0.11801219 12.53403568 -1.036872387 0.58708119 11.24544334 0.32874823 0 11.24544334 0.57192516
		 -0.58708119 11.24544334 0.32874823 -0.83025813 11.24544334 -0.25833285 -0.58708119 11.24544334 -0.84541404
		 0 11.24544334 -1.08859086 0.58708119 11.24544334 -0.84541404 0.83025837 11.24544334 -0.25833285
		 1.15884948 11.048699379 -1.41718245 0 11.048699379 -1.89719343 0 11.14179325 -1.90842009
		 1.16678786 11.14179325 -1.42512083 -1.15884948 11.048699379 -1.41718245 -1.16678786 11.14179325 -1.42512083
		 -1.6388607 11.052516937 -0.25833285 -1.65008688 11.14561081 -0.25833285 -1.15884948 11.081693649 0.90051675
		 -1.16678786 11.17478752 0.90845513 0 11.11836052 1.38052797 0 11.21145439 1.39175463
		 1.15884948 11.090703011 0.90051675 1.16678786 11.18379688 0.90845513 1.6388607 11.054018974 -0.25833285
		 1.65008736 11.14711189 -0.25833285 0 12.47720146 -1.10615206 0.45329022 11.81006622 0.19495738
		 0.64104891 11.81006622 -0.25833285 0.45329022 11.81006622 -0.71162283 0 11.81006622 -0.89938176
		 -0.45328999 11.81006622 -0.71162283 -0.64104891 11.81006622 -0.25833285 -0.45328999 11.81006622 0.19495738
		 0 11.81006622 0.38271594 0.32909459 12.39215469 0.0137012 0.46541011 12.36725426 -0.29503039
		 0.32909459 12.34235477 -0.60376197 0 12.33204079 -0.73164296 -0.32909459 12.34235477 -0.60376197
		 -0.46541011 12.36725426 -0.29503039 -0.32909459 12.39215469 0.0137012 0 12.40246773 0.14158201
		 0.20324177 12.95399094 -0.52277803 0.28742754 12.80446529 -0.58449596 0.20324177 12.65493965 -0.64621359
		 0 12.59300423 -0.67177814 -0.20324177 12.65493965 -0.64621359 -0.28742731 12.80446529 -0.58449596
		 -0.20324177 12.95399094 -0.52277803 0 13.015927315 -0.49721354 -1.0055177212 11.22702503 -0.25833285
		 -0.71100831 11.23119354 0.45267558 0 11.23643112 0.74718499 0.71100855 11.23248005 0.45267582
		 1.0055177212 11.22723961 -0.25833285 0.71100831 11.22647953 -0.96934116 0 11.22647953 -1.26385069
		 -0.71100831 11.22647953 -0.96934116 -0.78506541 11.35319519 -0.25833285 -0.55512524 11.35319519 0.29679239
		 0 11.35319519 0.52673292 0.55512524 11.35319519 0.29679239 0.78506565 11.35319519 -0.25833285
		 0.55512524 11.35319519 -0.81345809 0 11.35319519 -1.043398142 -0.55512524 11.35319519 -0.81345809
		 0 11.11388397 -0.25833285 -0.76466036 11.24544334 0.058399916 0.61255324 11.2380085 -0.87088609
		 0.62948716 11.14179325 -1.77804899 -1.24142814 11.083717346 -1.49976087 -0.62948716 11.14179325 -1.77804899
		 -0.86628103 11.2380085 -0.25833285 -1.51971626 11.14179325 -0.88782001 -0.61255324 11.2380085 0.35422039
		 -1.51971626 11.15706444 0.37115431 -0.62948716 11.1974144 1.26138306 1.24142814 11.1317215 0.98309493
		 0.62948716 11.20342064 1.26138306 1.51971626 11.16307068 0.37115431 1.51971626 11.14179325 -0.88782001
		 0.31673282 11.24544334 -1.022993326 -0.31673282 11.24544334 -1.022993326 0 11.2380085 -1.124614
		 -0.76466036 11.24544334 -0.57506567 -0.61255324 11.2380085 -0.87088609 -0.31673282 11.24544334 0.50632787
		 0 11.2380085 0.60794854 0.7646606 11.24544334 -0.57506567 0.86628127 11.2380085 -0.25833285
		 0 12.84611225 -0.85522687 -0.14031544 12.80464649 -0.8475548 -0.19843614 12.70454025 -0.82903278
		 -0.14031544 12.60443306 -0.81051075 0 12.5629673 -0.80283868 0.14031544 12.60443306 -0.81051075
		 0.10868821 12.52009487 -1.00090134144 0.19843614 12.70454025 -0.82903278 0.14031544 12.80464649 -0.8475548
		 0.7646606 11.24544334 0.058399916 0.31673282 11.24544334 0.50632787 0.61255348 11.2380085 0.35422063
		 0 12.47791958 -1.1608696 -0.052400708 12.50061321 -0.96784806 -0.10868794 12.52009487 -1.00090134144
		 -0.10868794 12.54797745 -1.072843075 -0.052400708 12.56745911 -1.10589623 0 12.4764843 -1.051434517
		 0.052400708 12.56746006 -1.10589623 0.10868821 12.54797745 -1.072843075 0.052400708 12.50061321 -0.96784806
		 0.054722473 12.47720146 -1.10615206 -0.054722473 12.47720146 -1.10615206 0.51124501 11.55005836 0.25291216
		 0.72300935 11.55005836 -0.25833285 0.5904007 11.81006622 -0.013780951 0.51124465 11.55005836 -0.7695775
		 0.5904007 11.81006622 -0.50288475 0 11.55005836 -0.98134208 0.2445519 11.81006622 -0.84873354
		 -0.51124465 11.55005836 -0.7695775 -0.2445519 11.81006622 -0.84873354 -0.72300911 11.55005836 -0.25833285
		 -0.5904007 11.81006622 -0.50288475 -0.51124465 11.55005836 0.25291181 -0.5904007 11.81006622 -0.013780951
		 0 11.55005836 0.46467638 -0.2445519 11.81006622 0.33206797 0.2445519 11.81006622 0.33206797
		 0.39202094 12.084942818 0.13368809 0.5544014 12.084942818 -0.25833285 0.4286387 12.38068771 -0.12846833
		 0.39202094 12.084942818 -0.65035373 0.4286387 12.35382175 -0.46159247 0 12.084942818 -0.81273425
		 0.17754781 12.33482265 -0.69714689 -0.39202094 12.084942818 -0.65035373 -0.17754781 12.33482265 -0.69714689
		 -0.5544014 12.084942818 -0.25833285 -0.42863846 12.35382175 -0.46159247 -0.39202094 12.084942818 0.13368809
		 -0.42863846 12.38068771 -0.12846833 0 12.084942818 0.29606855 -0.17754781 12.39968681 0.10708582
		 0.17754805 12.39968681 0.10708582 0.2661683 12.74916553 -0.22040682 0.37641871 12.64956665 -0.40512323
		 0.26471841 12.88513565 -0.55119884 0.2661683 12.54996681 -0.5898397 0.26471812 12.72379589 -0.61779279
		 0 12.50871181 -0.66635168 0.10964978 12.60971165 -0.66488194 -0.26616806 12.54996681 -0.5898397
		 -0.10964978 12.60971165 -0.66488194 -0.37641871 12.64956665 -0.40512323;
	setAttr ".vt[332:497]" -0.26471812 12.72379589 -0.61779279 -0.26616806 12.74916553 -0.22040682
		 -0.26471812 12.88513565 -0.55119884 0 12.79042149 -0.14389452 -0.10964978 12.99921989 -0.50410974
		 0.10964978 12.99921989 -0.50410974 -1.33341575 11.19407558 -0.25833285 -0.94286752 11.21074772 0.68453455
		 -0.92607331 11.22866058 0.12525928 0 11.2317009 1.075082779 -0.38359213 11.23442554 0.66774035
		 0.94286752 11.21589661 0.68453455 0.38359213 11.2352829 0.66774035 1.33341575 11.19493294 -0.25833285
		 0.92607331 11.22951889 0.12525928 0.94286752 11.19189453 -1.20120025 0.92607331 11.22647953 -0.64192498
		 0 11.19189453 -1.59174848 0.38359213 11.22647953 -1.18440604 -0.94286752 11.19189453 -1.20120025
		 -0.38359213 11.22647953 -1.18440604 -0.92607331 11.22647953 -0.64192498 -0.81200075 11.26774788 -0.25833285
		 -0.5741713 11.26774788 0.3158381 -0.72303867 11.35319519 0.04115963 0 11.26774788 0.55366778
		 -0.2994923 11.35319519 0.46470571 0.5741713 11.26774788 0.31583846 0.29949254 11.35319519 0.46470571
		 0.81200075 11.26774788 -0.25833285 0.72303891 11.35319519 0.04115963 0.5741713 11.26774788 -0.8325038
		 0.72303867 11.35319519 -0.55782509 0 11.26774788 -1.070333481 0.29949254 11.35319519 -0.98137152
		 -0.57417095 11.26774788 -0.83250415 -0.2994923 11.35319519 -0.98137152 -0.72303867 11.35319519 -0.55782509
		 0 11.083717346 -2.013977051 -0.62520421 11.048699379 -1.76770973 0 11.16333103 1.49731159
		 0.62520421 11.11032677 1.2510438 -1.50937653 11.048699379 -0.88353705 -1.75564432 11.08808136 -0.25833285
		 1.50937653 11.069976807 0.36687136 1.75564432 11.08979702 -0.25833285 0.62520421 11.048699379 -1.76770973
		 -1.50937653 11.063970566 0.36687136 -1.24142814 11.12142563 0.98309517 -0.62520444 11.10432053 1.2510438
		 1.2414279 11.083717346 -1.49976087 0.37411964 11.10235405 -1.027809143 0 11.067769051 -1.54684258
		 -0.37411964 11.10235405 -1.027809143 -0.91111398 11.067769051 -1.16944695 -0.76947641 11.10235405 -0.63245249
		 -1.28850961 11.069951057 -0.25833285 -0.76947641 11.10453606 0.11578679 -0.91111398 11.086623192 0.65278125
		 -0.37411964 11.11030006 0.51114345 0 11.10757637 1.03017664 0.37411964 11.11115837 0.51114345
		 0.91111398 11.091772079 0.65278125 0.76947641 11.10539436 0.11578679 1.28850961 11.070808411 -0.25833285
		 0.91111398 11.067769051 -1.16944695 1.50937653 11.048699379 -0.88353705 0 11.11388397 0.15235484
		 0.76947641 11.10235405 -0.63245219 0 11.11388397 -0.66902053 0.41068769 11.11388397 -0.25833285
		 -0.41068769 11.11388397 -0.25833285 -0.79783773 11.2380085 0.072142243 0.33047509 11.2380085 -1.056170464
		 -0.33047509 11.2380085 -1.056170464 -0.79783773 11.2380085 -0.58880794 -0.33047509 11.2380085 0.53950477
		 0.79783773 11.2380085 -0.58880794 0.075700745 12.83492756 -0.8531574 -0.075700745 12.83492661 -0.8531574
		 -0.18275785 12.75854778 -0.8390255 -0.18275785 12.65053177 -0.81903994 -0.075700745 12.57415295 -0.80490839
		 0.075700745 12.57415295 -0.80490839 0.18275809 12.65053177 -0.8190403 0.18275809 12.75854778 -0.8390255
		 0.33047509 11.2380085 0.53950477 0.79783773 11.2380085 0.072142243 -0.050398886 12.47786236 -1.15654659
		 0.050398886 12.47786236 -1.15654659 0.050398886 12.47654057 -1.055757523 -0.050398886 12.47654057 -1.055757523
		 0.66588557 11.55005836 0.017485857 0.66588557 11.55005836 -0.53415161 0.27581877 11.55005836 -0.92421842
		 -0.27581877 11.55005836 -0.92421842 -0.66588533 11.55005836 -0.53415161 -0.66588557 11.55005836 0.017485857
		 -0.27581877 11.55005836 0.40755272 0.27581877 11.55005836 0.40755272 0.51059878 12.084942818 -0.046836019
		 0.51059878 12.084942818 -0.46982971 0.21149713 12.084942818 -0.76893163 -0.21149683 12.084942818 -0.76893163
		 -0.51059878 12.084942818 -0.46982971 -0.51059878 12.084942818 -0.046836019 -0.21149683 12.084942818 0.25226593
		 0.21149713 12.084942818 0.25226593 0.34667838 12.70330048 -0.30546799 0.34667838 12.59583187 -0.5047785
		 0.14359879 12.51984024 -0.64571249 -0.14359879 12.51984024 -0.64571249 -0.34667838 12.59583187 -0.5047785
		 -0.34667838 12.70330048 -0.30546799 -0.14359879 12.77929211 -0.16453403 0.14359879 12.77929211 -0.16453403
		 -1.22806454 11.20062065 0.25034809 -0.50868094 11.22367668 0.96973181 0.50868094 11.22710991 0.96973181
		 1.22806454 11.20405197 0.25034809 1.22806454 11.19189453 -0.76701379 0.50868094 11.19189453 -1.4863975
		 -0.50868094 11.19189453 -1.4863975 -1.22806454 11.19189453 -0.76701379 -0.74784565 11.26774788 0.051434875
		 -0.30976778 11.26774788 0.48951268 0.30976778 11.26774788 0.48951268 0.74784589 11.26774788 0.051434875
		 0.74784565 11.26774788 -0.56810063 0.30976778 11.26774788 -1.0061784983 -0.30976778 11.26774788 -1.0061784983
		 -0.74784565 11.26774788 -0.56810063 -0.6697557 11.083717346 -1.87526608 0.6697557 11.15414906 1.35860062
		 -1.61693335 11.083717346 -0.92808855 1.61693335 11.10803509 0.41142297 0.6697557 11.083717346 -1.87526608
		 -1.61693335 11.10117054 0.41142297 -0.6697557 11.14728451 1.35860062 1.61693335 11.083717346 -0.92808855
		 0.49154997 11.067769051 -1.44503927 -0.49154997 11.067769051 -1.44503927 -1.1867063 11.067769051 -0.74988282
		 -1.1867063 11.076496124 0.23321712 -0.49154997 11.099552155 0.92837358 0.49154997 11.10298538 0.92837358
		 1.18670654 11.079927444 0.23321712 1.1867063 11.067769051 -0.74988282 0.37823987 11.11388397 -0.63657272
		 -0.37823987 11.11388397 -0.63657272 0.37823987 11.11388397 0.11990702 -0.37823987 11.11388397 0.11990702
		 -0.32448906 10.68206692 0.63117361 0.33741784 10.68206692 0.63117361 -0.32448906 10.72495461 0.63117361
		 0.33741784 10.72495461 0.63117361 -0.32448906 10.72495461 0.59175014 0.33741784 10.72495461 0.59175014
		 -0.32448906 10.68206692 0.59175014 0.33741784 10.68206692 0.59175014 -0.001295089 10.76341248 0.65387607
		 -0.001295089 10.76341248 0.56904793 -0.001295089 10.68371201 0.56904793 -0.001295089 10.68371201 0.65387607
		 -0.10507134 10.78554344 0.66805887 -0.10507134 10.78554344 0.55486512;
	setAttr ".vt[498:663]" -0.10507134 10.66216087 0.55486512 -0.10507134 10.66216087 0.66805887
		 0.1049751 10.78554344 0.66805887 0.1049751 10.78554344 0.55486512 0.1049751 10.66216087 0.55486512
		 0.1049751 10.66216087 0.66805887 -0.23497987 10.67009354 0.6500082 -0.23497987 10.75498104 0.6500082
		 -0.23497987 10.75498104 0.57291579 -0.23497987 10.67009354 0.57291579 -0.3558172 10.70035172 0.63011575
		 0.3706857 10.70035172 0.63011575 -0.3558172 10.72030449 0.61146212 0.3706857 10.72030449 0.61146212
		 -0.3558172 10.70035172 0.59280825 0.3706857 10.70035172 0.59280825 -0.3558172 10.68039894 0.61146212
		 0.37068594 10.68039894 0.61146212 0.034988768 10.77726364 0.66253304 0.034988768 10.77726364 0.56039071
		 -0.001295089 10.77669716 0.61146212 0.034988768 10.67334175 0.56039071 -0.001295089 10.72356224 0.55490994
		 0.034988768 10.67334175 0.66253304 -0.001295089 10.67042923 0.61146212 -0.001295089 10.72356224 0.66801381
		 -0.037579194 10.77726364 0.66253304 -0.037579194 10.77726364 0.56039071 -0.10507134 10.80610752 0.61146212
		 -0.037579194 10.67334175 0.56039071 -0.10507134 10.72385216 0.5359993 -0.037579194 10.67334175 0.66253304
		 -0.10507134 10.64159584 0.61146212 -0.10507134 10.72385216 0.68692446 0.24236608 10.75498104 0.6500082
		 0.24236608 10.75498104 0.57291579 0.1049751 10.80610752 0.61146212 0.24236608 10.67009354 0.57291579
		 0.1049751 10.72385216 0.5359993 0.24236608 10.67009354 0.6500082 0.1049751 10.64159584 0.61146212
		 0.1049751 10.72385216 0.68692446 -0.23497987 10.71253777 0.66285706 -0.23497987 10.7691288 0.61146212
		 -0.23497987 10.71253777 0.56006694 -0.23497987 10.65594482 0.61146212 0.41345906 10.69629002 0.61146212
		 -0.39609647 10.69629002 0.61146212 0.034988768 10.79458427 0.61146212 0.034988768 10.72530365 0.54336715
		 0.034988768 10.65602207 0.61146212 0.034988768 10.72530365 0.67955685 -0.037579194 10.79458427 0.61146212
		 -0.037579194 10.72530365 0.54336715 -0.037579194 10.65602207 0.61146212 -0.037579194 10.72530365 0.67955685
		 0.24236608 10.7691288 0.61146212 0.24236608 10.71253777 0.56006694 0.24236608 10.65594482 0.61146212
		 0.24236608 10.71253777 0.66285706 -0.76886773 10.2756691 0.027300477 0.77766562 10.2756691 0.014683366
		 -0.84866095 10.68428802 -0.027165115 0.85007906 10.68428802 -0.039782286 -0.81620574 10.68294525 -0.10960597
		 0.81331396 10.68294525 -0.096988559 -0.73641229 10.26895905 -0.040176809 0.74090052 10.26895905 -0.027559221
		 0.4805963 10.55135536 0.62316966 0.47173333 10.54915237 0.46787572 0.34093606 9.36922073 0.76618385
		 0.34979916 9.38023853 0.86454058 0 10.42801476 0.79357719 0 10.44046211 0.6189425
		 0 8.91931248 1.080955505 0 8.8570776 1.20014763 -0.48049188 10.55135536 0.62621117
		 -0.47266769 10.54915237 0.46483397 -0.34009135 9.36922073 0.76314187 -0.34791553 9.38023853 0.86758232
		 -0.60487461 9.90715981 0.37584972 -0.72831702 10.65498352 0.25330198 -0.69702077 10.65196228 0.12475944
		 -0.5735786 9.89206028 0.28097522 -0.86817312 10.54997444 -0.09396565 0.87495852 10.54997444 -0.11424369
		 -0.89071298 10.75989151 -0.16428873 0.88810468 10.75989151 -0.16428873 -0.81601262 10.54695415 -0.18815124
		 0.81587172 10.54695415 -0.16787338 -0.79347253 10.33703613 -0.11782813 0.80272532 10.33703613 -0.11782813
		 0.72873497 10.65498352 0.24113512 0.69328308 10.65196228 0.13692617 0.49796438 10.74617481 0.50054932
		 0.57695723 9.89206028 0.29314208 0.40485764 9.95808506 0.59589243 0.61240947 9.90715981 0.36368299
		 0.32356817 9.17880917 0.86033559 0.41667485 9.96689892 0.76499248 0.23873013 10.46607208 0.77512169
		 0.23873013 10.4723568 0.6014781 0 10.69191265 0.63387823 0.15735745 9.037439346 1.01363039
		 0 9.68611145 0.82546329 0.15735745 9.0060157776 1.12786722 0 8.63052177 1.21293306
		 0 9.63632298 1.021348 -0.23873013 10.46607208 0.77512169 -0.23873013 10.4723568 0.6014781
		 -0.49867582 10.74617481 0.50054932 -0.15735745 9.037439346 1.01363039 -0.40507555 9.95808506 0.59183669
		 -0.15735745 9.0060157776 1.12786722 -0.32190764 9.17880917 0.86033559 -0.41550767 9.96689892 0.76904821
		 -0.67181182 10.28258133 0.33319402 -0.73324251 10.77911663 0.16580033 -0.63008356 10.27050209 0.18424928
		 -0.56865287 9.77396584 0.35164285 0.90960455 10.63910484 -0.27431852 -0.90580773 10.63910484 -0.27431852
		 0.73039651 10.77911663 0.16580033 0.62921143 10.27050209 0.2004714 0.57529569 9.77396584 0.35164285
		 0.67648077 10.28258133 0.31697166 0.25229204 10.71046257 0.62455845 0.19804382 9.75804043 0.78356433
		 0.14379534 8.78047943 1.13449001 0.19804382 9.73290062 0.97548413 -0.25229228 10.71046257 0.62455845
		 -0.19804382 9.75804043 0.78356433 -0.14379534 8.78047943 1.13449001 -0.19804382 9.73290062 0.97548413
		 0.89719629 5.29672432 -1.075060844 -2.5132715e-07 5.29672432 -1.40097284 -2.5132715e-07 5.29672432 0.69405055
		 0.89719629 5.29672432 0.47683954 1.2688272 5.29672432 -0.28824052 0.94217324 9.42087841 -0.98843932
		 -2.5132715e-07 9.44760513 -1.24910784 -2.5132715e-07 9.44760513 0.72850704 0.94217324 9.42087841 0.47164035
		 1.12632561 9.46631432 -0.25823209 -2.5132715e-07 9.49036789 -0.25833285 -2.5132715e-07 7.54374599 1.3171463
		 -2.5132715e-07 7.54374599 -1.98763466 1.43442345 7.63909435 -1.46077251 1.75152636 7.44943285 -0.25080159
		 1.43442392 7.63909435 0.94034243 -2.5132715e-07 5.6956749 1.17701864 -2.5132715e-07 5.6956749 -1.86726332
		 1.29282093 5.6956749 -1.3950088 1.79512501 5.6956749 -0.25488564 1.29282093 5.6956749 0.85515857
		 1.22011399 4.9704895 -0.29089004 -2.5132715e-07 4.9704895 -1.36090159 0.86275101 4.9704895 -1.047502279
		 0.86275101 4.9704895 0.44464469 -2.5132715e-07 4.9704895 0.65265465 1.59360647 4.38361263 -0.26508123
		 -2.5132715e-07 4.38361263 -1.662637 1.12685037 4.38361263 -1.25330234 1.12685037 4.38361263 0.69561005
		 -2.5132715e-07 4.38361263 0.96729493 2.44586468 2.74316978 -0.20221776;
	setAttr ".vt[664:829]" -2.5132715e-07 2.74316978 -2.36468649 1.72948742 2.74316978 -1.74909449
		 1.7294879 2.74316978 1.30689669 0 2.74316788 1.73367596 2.88496304 0.021442413 -0.11062837
		 -2.5132715e-07 0.0050315857 -0.092154562 -2.5132715e-07 0.021442413 -2.7657876 2.15675545 0.034208298 -2.20689201
		 2.15675592 0.034204483 1.9507823 -2.5132715e-07 0.021440506 2.44669247 -2.5132715e-07 0.46275902 2.63571548
		 2.2408514 0.46275902 2.026387215 3.16904163 0.46276093 -0.12822706 2.24085045 0.46276283 -2.30593824
		 -2.5132715e-07 0.46276093 -3.030754089 1.012275934 9.012091637 0.70784831 -2.5132715e-07 9.00096225739 0.99625921
		 -2.5132715e-07 9.00096225739 -1.55796027 1.012275696 9.012091637 -1.22871804 1.55035305 9.078865051 -0.74585783
		 1.91039324 8.26735592 -0.25366497 1.55035353 9.078865051 0.23248279 1.63076115 9.32199001 -0.25762737
		 3.47183418 8.15638733 -0.25411284 3.39285374 9.086461067 0.1694361 3.38824368 9.29793644 -0.25752658
		 3.39285374 9.086461067 -0.68292701 5.42447472 8.090616226 -0.25446114 5.39251328 9.05060482 0.12487018
		 5.43860245 9.015330315 -0.25662962 5.37659645 9.27922726 -0.25744843 5.39251328 9.05060482 -0.63832247
		 1.75288773 8.52487564 0.10682118 1.75288773 8.52487564 -0.61644012 1.56168461 9.26051903 -0.55968922
		 1.56168461 9.26051903 0.044949651 5.35260391 9.2314167 -0.026347578 3.38756371 9.2337923 -0.019760549
		 5.38300323 8.41774368 0.070216894 3.43532944 8.5721426 0.045554161 5.38300133 8.41774368 -0.58091986
		 3.43532944 8.5721426 -0.55616862 5.35260391 9.2314167 -0.48814842 3.38756371 9.2337923 -0.49475557
		 -2.88496304 0.021442413 -0.11062837 -2.15675545 0.034208298 -2.20689201 -2.1567564 0.034204483 1.9507823
		 -0.89719653 5.29672432 -1.075060844 -0.86275125 4.9704895 -1.047502279 -1.26882744 5.29672432 -0.28824052
		 -1.22011423 4.9704895 -0.29089004 -0.89719677 5.29672432 0.47683954 -0.86275125 4.9704895 0.44464469
		 -1.12685037 4.38361263 -1.25330234 -1.59360695 4.38361263 -0.26508123 -1.12685061 4.38361263 0.69561005
		 -1.7294879 2.74316978 -1.74909449 -2.44586515 2.74316978 -0.20221776 -1.72948837 2.74316978 1.30689669
		 -2.24085093 0.46276283 -2.30593824 -3.16904163 0.46276093 -0.12822706 -2.2408514 0.46275902 2.026387215
		 -1.29282117 5.6956749 0.85515857 -1.29282117 5.6956749 -1.3950088 -1.79512548 5.6956749 -0.25488564
		 -0.94217348 9.42087841 -0.98843932 -1.012275934 9.012091637 -1.22871804 -1.12632608 9.46631432 -0.25823209
		 -0.94217348 9.42087841 0.47164035 -1.43442392 7.63909435 0.94034243 -3.43987465 8.57163048 0.045689583
		 -5.39108086 8.41683197 0.070457935 -5.39705849 9.05052948 0.12489283 -3.39739895 9.086384773 0.16945863
		 -1.012276173 9.012091637 0.70784831 -1.7528882 8.52487564 0.10682118 -1.55035353 9.078865051 0.23248279
		 -1.56168509 9.26051903 0.044949651 -3.39210796 9.2337923 -0.019760549 -5.36068439 9.2314167 -0.026347578
		 -1.43442392 7.63909435 -1.46077251 -1.75152636 7.44943285 -0.25080159 -1.91039324 8.26735592 -0.25366497
		 -1.7528882 8.52487564 -0.61644012 -3.4398737 8.57163048 -0.5563041 -5.39108086 8.41683197 -0.5811606
		 -5.42901802 8.09012413 -0.25446114 -3.47637939 8.15589714 -0.25411284 -5.43860245 9.015330315 -0.25662962
		 -1.55035353 9.078865051 -0.74585783 -1.56168509 9.26051903 -0.55968922 -5.38114166 9.27922726 -0.25744843
		 -3.39278889 9.29793644 -0.25752658 -1.63076162 9.32199001 -0.25762737 -5.39705849 9.05052948 -0.63834506
		 -5.36068344 9.2314167 -0.48814842 -3.39210796 9.2337923 -0.49475557 -3.39739895 9.086384773 -0.68294954
		 4.96446609 9.064664841 0.17727292 4.95364571 9.2337923 -0.019760251 4.94673252 9.29793644 -0.25752658
		 4.95364475 9.2337923 -0.49475557 4.96446514 9.064664841 -0.69076359 4.98491096 8.42006111 -0.60318863
		 5.001449585 7.98497772 -0.25411284 4.98491192 8.42006111 0.092574477 -4.9917345 9.064208984 0.17740834
		 -5.012180328 8.41698551 0.093387485 -5.028717995 7.98203325 -0.25411284 -5.012180328 8.41698551 -0.604002
		 -4.99173355 9.064208984 -0.69089913 -4.98091412 9.2337923 -0.49475557 -4.97400093 9.29793644 -0.25752658
		 -4.98091412 9.2337923 -0.019760251 -2.5132715e-07 9.49036789 0.29854357 -2.5132715e-07 8.38799858 1.17265558
		 -2.5132715e-07 6.53152514 -2.00075912476 -2.5132715e-07 6.53152514 1.31304502 -2.5132715e-07 5.38053465 -1.62427449
		 0.48404109 5.29672432 -1.31305718 -2.5132715e-07 5.38053465 0.92573476 0.48404133 5.29672432 0.64961553
		 1.1685791 5.29672432 0.13625228 1.16857886 5.29672432 -0.71273339 1.1775794 5.15486336 -0.29382902
		 -2.5132715e-07 5.15486336 -1.3265388 0.83267426 5.15486336 -1.024064779 0.46545768 4.9704895 -1.27636147
		 1.12371445 4.9704895 -0.69908559 0.83267426 5.15486336 0.41606402 1.12371469 4.9704895 0.11730576
		 -2.5132715e-07 5.15486336 0.61682248 0.46545792 4.9704895 0.6102705 1.34771776 4.75913858 -0.28207219
		 -2.5132715e-07 4.75913858 -1.46398973 0.95298076 4.75913858 -1.11781406 0.60794055 4.38361263 -1.5522182
		 1.4676981 4.38361263 -0.79823065 0.952981 4.75913858 0.5303874 1.4676981 4.38361263 0.26806843
		 -2.5132715e-07 4.75913858 0.76015186 0.60794055 4.38361263 0.91193581 1.99099445 3.70671082 -0.23762125
		 -2.5132715e-07 3.70671082 -1.98367691 1.40784597 3.70671082 -1.47226906 0.93306589 2.74316978 -2.20220041
		 2.25262022 2.74316978 -1.039722681 1.40784597 3.70671082 0.9626317 2.2526207 2.74316978 0.63528705
		 -2.5132715e-07 3.70671082 1.3020649 0.93306613 2.74316978 1.64671469 2.86420155 1.50772285 -0.16139597
		 1.65128565 0.0050315857 -0.09933573 0 1.50772285 -2.74324226 0 0.0050315857 -1.61112928
		 2.025296211 1.50772285 -2.058158398 1.32329512 0.021442413 -2.61879539 2.65702581 0.021444321 -1.41633368
		 2.025296688 1.50772095 1.70384884 2.65702629 0.021440506 1.18621397 0 1.50772095 2.23134279
		 1.32329559 0.021440506 2.32991934 -2.5132715e-07 0.0050296783 1.37363958 0 0.070672989 2.78744125
		 2.32173061 0.070674896 2.14740753 1.20894861 0.46275902 2.51155758;
	setAttr ".vt[830:995]" 3.28342152 0.070676804 -0.11578161 2.91866016 0.46275902 1.067507267
		 2.32172918 0.070678711 -2.39890862 2.91865921 0.46276093 -1.32396126 0 0.070676804 -3.13863277
		 1.20894814 0.46276093 -2.86040115 -2.5132715e-07 9.31931686 0.90464449 1.090762615 5.38053465 0.65916681
		 0.70138836 5.6956749 1.1097846 0.70138812 5.6956749 -1.73987198 1.54257107 5.38053465 -0.27147499
		 1.6643424 5.6956749 0.36021543 1.090762377 5.38053465 -1.22804856 1.66434193 5.6956749 -0.86998701
		 -2.5132715e-07 9.31931686 -1.43705034 0.51671338 9.44760513 -1.1708138 -2.5132715e-07 9.49036789 -0.81520927
		 -2.5132715e-07 8.38799858 -1.79076147 0.5662334 9.00096225739 -1.45511699 0.93220282 9.30328178 -1.13606572
		 0.63499594 9.49036789 -0.25833285 1.04882741 9.46364117 -0.66683549 1.04882741 9.46364212 0.15039396
		 0.51671338 9.44760513 0.65164423 1.1851263 9.3406992 0.23907936 2.38796091 9.2337923 -0.019760549
		 5.27226543 9.2337923 -0.019760251 1.38282013 6.53152514 0.95692778 5.40249252 8.70951271 0.12742424
		 3.41099358 8.85828209 0.14238548 0.5662334 9.00096225739 0.90979409 0.75537515 7.54374599 1.23601198
		 1.17575097 8.44855118 0.85892773 1.52614403 8.22476101 0.44810557 1.63650227 8.81186199 0.20793641
		 2.37666178 9.089994431 0.16431022 3.38710976 9.19102955 0.099167466 0.93220305 9.30328178 0.61810541
		 1.21793699 9.045477867 0.44787002 1.53443336 9.20706558 0.16705453 4.29492188 9.075861931 0.17394483
		 5.38490486 9.19637489 0.054614544 2.48522615 8.61129093 0.014798284 5.30353165 8.38412952 0.10207665
		 0.75537491 7.54374599 -1.85043716 1.38281965 6.53152514 -1.48792243 1.82280493 6.53152514 -0.24982543
		 1.68863583 7.45788288 -0.8687669 1.17575073 8.44855022 -1.3854003 5.41903114 8.20672607 -0.46011439
		 1.85951757 8.33672333 -0.057456851 1.70720673 8.010746002 -0.25232109 1.68863583 7.45788288 0.36706448
		 1.52614355 8.22476101 -0.95473862 3.45966625 8.29369164 -0.41550231 1.85951757 8.33672333 -0.45056948
		 5.45137024 8.54168606 -0.25550616 5.43448257 9.029200554 -0.038511515 2.58163261 8.27115631 -0.25411284
		 3.45966625 8.29369164 -0.093519747 4.33190536 8.057290077 -0.25411284 5.41903114 8.20672607 -0.049504578
		 2.48522615 8.61129093 -0.52541268 5.30353165 8.38412952 -0.61269087 1.1851263 9.3406992 -0.75449085
		 3.3880167 9.27655506 -0.1386883 1.33032227 9.39415264 -0.25792974 1.60505009 9.30328178 -0.41456461
		 1.60505009 9.30328178 -0.10053355 1.21793675 9.045477867 -0.96089756 1.63650227 8.81186199 -0.71954608
		 5.38490486 9.19637489 -0.5688169 5.43448257 9.029200554 -0.47482508 3.38710976 9.19102955 -0.61332548
		 4.27718925 9.29793644 -0.25752658 5.37799263 9.26051903 -0.39096248 1.53443289 9.20706558 -0.68134642
		 2.41141796 9.29793644 -0.25752658 3.3880167 9.27655506 -0.37618595 2.37666178 9.089994431 -0.67780089
		 3.41099262 8.85828209 -0.65459239 5.42401123 9.22310162 -0.25721318 5.37799263 9.26051903 -0.12377757
		 4.29492188 9.075861931 -0.68743575 5.40249252 8.70951271 -0.63943207 2.38796091 9.2337923 -0.49475557
		 5.27226543 9.2337923 -0.49475557 -1.65128613 0.0050315857 -0.09933573 -1.32329535 0.021444321 -2.61879539
		 -2.65702629 0.021442413 -1.41633368 -2.65702677 0.021440506 1.18621397 -1.32329583 0.021440506 2.32991934
		 -0.48404157 5.29672432 -1.31305718 -0.8326745 5.15486336 -1.024064779 -0.46545792 4.9704895 -1.27636147
		 -1.1685791 5.29672432 -0.71273339 -1.17757964 5.15486336 -0.29382902 -1.12371469 4.9704895 -0.69908559
		 -1.16857934 5.29672432 0.13625228 -0.8326745 5.15486336 0.41606402 -1.12371492 4.9704895 0.11730576
		 -0.48404157 5.29672432 0.64961553 -0.46545815 4.9704895 0.6102705 -0.952981 4.75913858 -1.11781406
		 -0.60794055 4.38361263 -1.5522182 -1.34771824 4.75913858 -0.28207245 -1.46769857 4.38361263 -0.79823065
		 -0.95298123 4.75913858 0.5303874 -1.46769857 4.38361263 0.26806843 -0.60794079 4.38361263 0.91193581
		 -1.40784597 3.70671082 -1.47226906 -0.93306613 2.74316978 -2.20220041 -1.99099445 3.70671082 -0.23762125
		 -2.2526207 2.74316978 -1.039722681 -1.40784597 3.70671082 0.9626317 -2.2526207 2.74316978 0.63528705
		 -0.93306637 2.74316978 1.64671469 -2.025296211 1.50772285 -2.058158875 -1.20894837 0.46276093 -2.86040115
		 -2.86420155 1.50772095 -0.16139624 -2.91866016 0.46276093 -1.32396126 -2.025296688 1.50772095 1.70384884
		 -2.91866016 0.46275902 1.067507267 -1.20894909 0.46275711 2.51155758 -2.32173061 0.070674896 2.14740753
		 -3.28342247 0.070676804 -0.11578161 -2.32172966 0.070678711 -2.39890862 -1.090762854 5.38053465 0.65916657
		 -0.7013886 5.6956749 1.1097846 -0.7013886 5.6956749 -1.7398715 -1.090762854 5.38053465 -1.22804856
		 -1.54257154 5.38053465 -0.27147526 -1.6643424 5.6956749 0.36021543 -1.6643424 5.6956749 -0.86998701
		 -0.51671362 9.44760513 -1.17081404 -0.93220329 9.30328083 -1.13606572 -0.56623363 9.00096225739 -1.45511675
		 -0.63499618 9.49036789 -0.25833285 -1.048827887 9.46364212 -0.66683549 -1.048828125 9.46364117 0.15039396
		 -0.51671362 9.44760513 0.65164423 -1.38282013 6.53152514 0.95692778 -0.75537539 7.54374599 1.23601198
		 -5.32171059 8.38208008 0.10261869 -5.40703773 8.70920849 0.12751472 -4.31310081 9.075557709 0.17403531
		 -3.41553783 8.85797882 0.14247596 -0.56623363 9.00096225739 0.90979433 -1.17575121 8.44855022 0.85892773
		 -0.93220329 9.30328178 0.61810541 -1.52614403 8.22476101 0.44810557 -1.63650274 8.81186199 0.20793641
		 -1.21793723 9.045477867 0.44787002 -2.37666225 9.089994431 0.16431022 -2.48522663 8.61129093 0.014798284
		 -1.53443336 9.20706558 0.16705453 -3.39165497 9.19102955 0.099167466 -2.38796091 9.2337923 -0.019760549
		 -1.18512654 9.3406992 0.23907936 -5.29044437 9.2337923 -0.019760251 -5.38945007 9.19637489 0.054614544
		 -0.75537515 7.54374599 -1.85043716 -1.38282013 6.53152514 -1.48792243 -1.82280493 6.53152514 -0.24982543
		 -1.68863583 7.45788288 -0.8687669 -1.17575097 8.44855022 -1.3854003;
	setAttr ".vt[996:1161]" -1.85951757 8.33672333 -0.057456851 -1.68863583 7.45788288 0.36706448
		 -1.70720673 8.010746002 -0.25232109 -1.52614403 8.22476101 -0.95473862 -1.85951757 8.33672333 -0.45056948
		 -5.32171059 8.38208008 -0.61323297 -5.42357445 8.20619583 -0.46020463 -4.3500843 8.055326462 -0.25411284
		 -3.46421051 8.29316139 -0.41559255 -5.43448257 9.029200554 -0.038511515 -5.42357445 8.20619583 -0.049414396
		 -5.45137024 8.54168606 -0.25550616 -2.58163261 8.27115631 -0.25411284 -3.46421051 8.29316139 -0.093429267
		 -2.48522615 8.61129093 -0.52541268 -1.21793699 9.045477867 -0.96089756 -1.18512654 9.3406992 -0.75449085
		 -1.53443336 9.20706558 -0.68134642 -5.38253784 9.26051903 -0.12377757 -4.29536819 9.29793644 -0.25752658
		 -3.39256191 9.27655506 -0.1386883 -1.3303225 9.39415264 -0.25792974 -1.60505009 9.30328178 -0.41456461
		 -1.60505009 9.30328178 -0.10053355 -2.41141796 9.29793644 -0.25752658 -1.63650227 8.81186199 -0.71954608
		 -5.38945007 9.19637489 -0.5688169 -5.38253784 9.26051903 -0.39096248 -5.42401314 9.22310162 -0.25721318
		 -5.43448257 9.029200554 -0.47482508 -5.29044437 9.2337923 -0.49475557 -4.31310081 9.075557709 -0.68752599
		 -3.39165497 9.19102955 -0.61332548 -2.37666178 9.089994431 -0.67780089 -2.38796091 9.2337923 -0.49475557
		 -5.40703773 8.70920849 -0.63952225 -3.41553783 8.85797882 -0.65468287 -3.39256191 9.27655506 -0.37618595
		 5.28308487 9.059335709 0.17885661 4.28410149 9.2337923 -0.019760549 4.95825291 9.19102955 0.099167705
		 5.2653532 9.29793644 -0.25752658 4.94903755 9.27655506 -0.1386883 4.28410149 9.2337923 -0.49475557
		 4.9490366 9.27655506 -0.37618595 5.28308487 9.059335709 -0.69234717 4.95825291 9.19102955 -0.61332548
		 4.3153677 8.49556541 -0.58322096 4.97388649 8.77109623 -0.68593919 5.32006931 7.95056629 -0.25411284
		 4.99593735 8.12129402 -0.44684884 4.3153677 8.49556541 0.072606444 4.99593735 8.12129402 -0.062172711
		 4.97388649 8.77109623 0.17373228 -5.30126476 9.05903244 0.17894697 -4.33354664 8.49351501 0.073148608
		 -5.0011548996 8.76927185 0.17427433 -5.33824825 7.94860315 -0.25411284 -5.023204803 8.11811447 -0.061630607
		 -4.33354664 8.49351501 -0.58376306 -5.023204803 8.11811447 -0.44739121 -5.30126476 9.05903244 -0.69243765
		 -5.0011548996 8.76927185 -0.68648124 -4.30228138 9.2337923 -0.49475557 -4.98552227 9.19102955 -0.61332548
		 -5.28353214 9.29793644 -0.25752658 -4.97630596 9.27655506 -0.37618595 -4.30228138 9.2337923 -0.019760251
		 -4.97630596 9.27655506 -0.1386883 -4.98552227 9.19102955 0.099167705 1.52082014 0.0050315857 -1.5340116
		 1.52082062 0.0050296783 1.2998867 0.44923127 5.15486336 -1.24494576 1.084540367 5.15486336 -0.68779445
		 1.084540606 5.15486336 0.1001364 0.44923127 5.15486336 0.57591558 0.51413727 4.75913858 -1.37060809
		 1.24123669 4.75913858 -0.73295844 1.24123669 4.75913858 0.16881406 0.51413727 4.75913858 0.7133348
		 0.75953889 3.70671082 -1.84572363 1.83368874 3.70671082 -0.90371895 1.83368874 3.70671082 0.42847681
		 0.75953913 3.70671082 1.2329011 1.092655659 1.50772285 -2.57272339 2.63790512 1.50772285 -1.19654083
		 2.6379056 1.50772095 0.87374878 1.092656136 1.50772095 2.12386036 1.2525835 0.070672989 2.65702629
		 3.024003983 0.070674896 1.14020824 3.024003029 0.070678711 -1.3717711 1.25258303 0.070676804 -2.96834183
		 0.58847106 5.38053465 0.87071514 0.58847106 5.38053465 -1.51739168 1.42069483 5.38053465 0.24460018
		 1.42069483 5.38053465 -0.78755045 0.51305676 9.31931686 -1.34386492 0.58482575 9.49036789 -0.77121139
		 0.58482575 9.49036789 0.25454569 0.76166058 6.53152514 1.23739386 4.30434322 8.8158865 0.16042042
		 0.66861951 8.38799858 1.088107586 0.51305676 9.31931686 0.81718326 1.35990667 8.63020706 0.50957799
		 2.42095566 8.87241459 0.12188148 2.37232256 9.19102955 0.099167466 1.16352773 9.25517368 0.37071466
		 4.28870964 9.19102955 0.099167466 0.76166034 6.53152514 -1.86242056 1.72297668 6.53152514 -0.91778398
		 0.66861928 8.38799858 -1.66932178 1.65335655 8.044546127 0.15572011 1.72297716 6.53152514 0.41813326
		 1.65335655 8.044546127 -0.6607607 4.32639313 8.1993618 -0.43353704 5.44613457 8.60785866 -0.036196828
		 2.54949713 8.37962532 -0.11402357 4.32639313 8.1993618 -0.075484812 2.54949713 8.37962532 -0.39499828
		 1.16352773 9.25517368 -0.88540971 4.27949333 9.27655506 -0.1386883 1.27118087 9.383461 -0.52970058
		 1.27118087 9.383461 0.013930678 2.40359879 9.27655506 -0.1386883 1.3599062 8.63020706 -1.019395828
		 5.42268562 9.21241093 -0.43529156 4.28870964 9.19102955 -0.61332548 2.37232256 9.19102955 -0.61332524
		 5.44613457 8.60785866 -0.47521362 2.42095518 8.87241459 -0.63408834 5.42268562 9.21241093 -0.079045057
		 4.30434227 8.8158865 -0.67262733 2.40359879 9.27655506 -0.37618595 4.27949333 9.27655506 -0.37618595
		 -1.52082062 0.0050315857 -1.5340116 -1.52082062 0.0050296783 1.2998867 -0.44923151 5.15486336 -1.24494576
		 -1.084540606 5.15486336 -0.68779445 -1.084540844 5.15486336 0.1001364 -0.44923151 5.15486336 0.57591558
		 -0.51413751 4.75913858 -1.37060809 -1.24123693 4.75913858 -0.73295867 -1.24123693 4.75913858 0.16881406
		 -0.51413751 4.75913858 0.7133348 -0.75953913 3.70671082 -1.84572363 -1.83368874 3.70671082 -0.90371895
		 -1.83368921 3.70671082 0.42847681 -0.75953937 3.70671082 1.2329011 -1.092655897 1.50772285 -2.57272339
		 -2.63790512 1.50772285 -1.19654083 -2.6379056 1.50772095 0.87374878 -1.092656374 1.50772095 2.12386036
		 -1.2525835 0.070672989 2.65702724 -3.024003983 0.070674896 1.14020824 -3.024003029 0.070678711 -1.3717711
		 -1.25258327 0.070676804 -2.96834183 -0.58847129 5.38053465 0.87071514 -0.58847129 5.38053465 -1.51739192
		 -1.42069483 5.38053465 0.24460018 -1.42069483 5.38053465 -0.78755045 -0.51305699 9.31931686 -1.34386492
		 -0.58482599 9.49036789 -0.77121139 -0.58482599 9.49036789 0.25454569 -0.76166081 6.53152514 1.23739386
		 -4.32252216 8.81467056 0.16078186 -0.66861975 8.38799858 1.088107586;
	setAttr ".vt[1162:1327]" -0.51305735 9.31931686 0.81718349 -1.35990667 8.63020611 0.50957799
		 -2.42095566 8.87241459 0.12188148 -2.37232304 9.19102955 0.099167466 -1.16352797 9.25517273 0.37071466
		 -4.30688953 9.19102955 0.099167466 -0.76166058 6.53152514 -1.86242056 -1.72297716 6.53152514 -0.91778398
		 -0.66861951 8.38799858 -1.66932178 -1.65335703 8.044546127 0.15572011 -1.72297716 6.53152514 0.41813326
		 -1.65335655 8.044546127 -0.6607607 -4.34457207 8.19724274 -0.43389845 -5.44613457 8.60785866 -0.036196828
		 -2.5494976 8.37962532 -0.11402357 -4.34457207 8.19724274 -0.07512337 -2.5494976 8.37962532 -0.39499828
		 -1.16352797 9.25517273 -0.88540971 -4.29767323 9.27655506 -0.1386883 -1.27118134 9.383461 -0.52970058
		 -1.27118134 9.383461 0.013930678 -2.40359926 9.27655506 -0.1386883 -1.35990667 8.63020611 -1.019396067
		 -5.42268562 9.21241093 -0.43529156 -4.30688858 9.19102955 -0.61332548 -2.37232256 9.19102955 -0.61332524
		 -5.44613457 8.60785866 -0.47521362 -2.42095566 8.87241459 -0.63408834 -5.42268562 9.21241093 -0.079045057
		 -4.32252216 8.81467056 -0.67298877 -2.40359926 9.27655506 -0.37618595 -4.29767323 9.27655506 -0.37618595
		 5.27687359 9.19102955 0.099167705 5.26765728 9.27655506 -0.1386883 5.26765633 9.27655506 -0.37618595
		 5.27687359 9.19102955 -0.61332548 5.29250622 8.74978161 -0.69227386 5.31455708 8.084143639 -0.4531838
		 5.31455708 8.084143639 -0.055838048 5.29250622 8.74978161 0.18006718 -5.31068516 8.74856567 0.18042862
		 -5.33273506 8.082023621 -0.055476606 -5.33273602 8.082023621 -0.45354521 -5.31068516 8.74856567 -0.69263554
		 -5.29505253 9.19102955 -0.61332548 -5.28583622 9.27655506 -0.37618595 -5.28583622 9.27655506 -0.1386883
		 -5.29505348 9.19102955 0.099167705 2.5132715e-07 5.11673212 -1.45508313 -1.28330827 5.11673212 -0.3367497
		 0 5.11673212 0.78158331 1.28330827 5.11673212 -0.3367497 2.5132715e-07 5.2938714 -1.67742491
		 -1.53844976 5.2938714 -0.33674994 0 5.2938714 1.0039253235 1.53844976 5.2938714 -0.3367497
		 2.5132715e-07 5.11673212 -1.7822876 -1.65878153 5.11673212 -0.33674994 0 5.11673212 1.10878801
		 1.65878153 5.11673212 -0.3367497 2.5132715e-07 4.93959379 -1.67742491 -1.53844976 4.93959379 -0.33674994
		 0 4.93959379 1.0039253235 1.53844976 4.93959379 -0.3367497 -0.85553885 5.11673212 -1.082305193
		 -0.85553885 5.11673212 0.40880585 0.85553885 5.11673212 0.40880585 0.85553908 5.11673212 -1.082305193
		 -1.025632858 5.2938714 -1.23053336 -1.025633335 5.2938714 0.55703354 1.025633335 5.2938714 0.55703378
		 1.025633335 5.2938714 -1.23053336 -1.10585403 5.11673212 -1.3004415 -1.10585451 5.11673212 0.62694192
		 1.10585451 5.11673212 0.62694192 1.10585451 5.11673212 -1.30044127 -1.025632858 4.93959379 -1.23053336
		 -1.025633335 4.93959379 0.55703354 1.025632858 4.93959379 0.55703354 1.025633335 4.93959379 -1.23053288
		 2.5132715e-07 5.23482513 -1.53898692 -1.37958956 5.23482513 -0.3367497 0 5.23482513 0.86548734
		 1.37958956 5.23482513 -0.3367497 2.5132715e-07 5.23482513 -1.75712299 -1.62990475 5.23482513 -0.33674994
		 0 5.23482513 1.083623409 1.62990522 5.23482513 -0.3367497 2.5132715e-07 4.99863958 -1.75712299
		 -1.62990475 4.99863958 -0.33674994 0 4.99863958 1.083623409 1.62990522 4.99863958 -0.3367497
		 2.5132715e-07 4.99863958 -1.53898692 -1.37958956 4.99863958 -0.33674994 0 4.99863958 0.86548734
		 1.37958956 4.99863958 -0.3367497 -0.91972637 5.23482513 -1.13824105 -0.91972661 5.23482513 0.46474171
		 0.91972637 5.23482513 0.46474171 0.91972661 5.23482513 -1.13824105 -1.086603165 5.23482513 -1.28366518
		 -1.086603403 5.23482513 0.61016583 1.086603403 5.23482513 0.61016583 1.086603403 5.23482513 -1.28366518
		 -1.086603165 4.99863958 -1.28366518 -1.086603403 4.99863958 0.61016583 1.086603403 4.99863958 0.61016583
		 1.086603403 4.99863958 -1.28366518 -0.91972637 4.99863958 -1.13824105 -0.91972661 4.99863958 0.46474171
		 0.91972637 4.99863958 0.46474171 0.91972661 4.99863958 -1.13824105 -5.58931971 9.10095215 -0.027908057
		 -5.59956408 8.98055935 0.061047435 -5.59550524 8.86438656 -0.019559711 -5.59550524 8.8360548 -0.23616177
		 -5.59550524 8.86438656 -0.44818664 -5.59956408 8.98055935 -0.55851424 -5.58931971 9.10095215 -0.48875761
		 -5.58931971 9.13057613 -0.25833285 -5.45540094 9.09138298 -0.031089365 -5.45540094 8.98055935 0.063037932
		 -5.45540094 8.86973476 -0.031089365 -5.45540094 8.84051991 -0.25833285 -5.45540094 8.86973476 -0.48557633
		 -5.45540094 8.98055935 -0.57970369 -5.45540094 9.09138298 -0.48557633 -5.45540094 9.12059784 -0.25833285
		 -5.45086145 8.98055935 0.0018958449 -5.45086145 8.89081955 -0.074323416 -5.45086145 8.8671627 -0.25833285
		 -5.45086145 8.98055935 -0.51856148 -5.45086145 8.89081955 -0.44234228 -5.45086145 9.070298195 -0.44234228
		 -5.45086145 9.09395504 -0.25833285 -5.45086145 9.070298195 -0.074323416 -5.38822794 8.98055935 -0.034338772
		 -5.38184214 8.98055935 -0.25833285 -5.39319468 8.90062618 -0.090689331 -5.38822794 8.88295269 -0.25833285
		 -5.38822794 8.98055935 -0.48232692 -5.39319468 8.90062618 -0.4259764 -5.39319468 9.060491562 -0.4259764
		 -5.38822794 9.078166008 -0.25833285 -5.39319468 9.060491562 -0.090689331 -5.48289537 9.092934608 -0.027908057
		 -5.48289537 9.12255859 -0.25833285 -5.48289537 9.092934608 -0.48875761 -5.48289537 8.98055935 -0.58420289
		 -5.48289537 8.86818314 -0.48875761 -5.48289537 8.83855915 -0.25833285 -5.48289537 8.86818314 -0.027908057
		 -5.48289537 8.98055935 0.067537189 -5.42910242 8.98055935 -0.0023397803 -5.42910242 9.068837166 -0.07731849
		 -5.42910242 9.092108727 -0.25833285 -5.42910242 9.068837166 -0.43934721 -5.42910242 8.98055935 -0.51432598
		 -5.42910242 8.89228058 -0.43934721 -5.42910242 8.86900902 -0.25833285 -5.42910242 8.89228058 -0.07731849
		 -5.45342302 8.98055935 0.03180778 -5.45342302 9.080613136 -0.053172469 -5.45342302 9.10698891 -0.25833285
		 -5.45342302 9.080613136 -0.46349323 -5.45342302 8.98055935 -0.54847348;
	setAttr ".vt[1328:1493]" -5.45342302 8.88050461 -0.46349323 -5.45342302 8.85412884 -0.25833285
		 -5.45342302 8.88050461 -0.053172469 -5.62181139 8.97987556 -0.2373063 -6.25512171 8.88488865 -0.084755003
		 -6.25512171 8.91937542 -0.089416623 -6.24441004 8.8874979 0.0066192746 -6.24441004 8.91215897 -0.0024453402
		 -6.24441004 8.91215897 -0.16825667 -6.24441004 8.8874979 -0.15589339 -5.72515917 9.052336693 0.036458313
		 -5.72515917 9.036682129 -0.25833285 -5.72515917 9.14905739 -0.027908057 -5.72515917 9.17868137 -0.25833285
		 -5.72515917 9.052336693 -0.55312407 -5.72515917 9.14905739 -0.48875761 -6.093067646 9.052565575 0.036458313
		 -6.093067646 9.036911011 -0.25833285 -6.093067646 9.14928627 -0.027908057 -6.093067646 9.17891026 -0.25833285
		 -6.093067646 9.052565575 -0.55312407 -6.093067646 9.14928627 -0.48875761 -6.25565004 9.024687767 -0.0048784018
		 -6.27150393 9.0079298019 -0.25833285 -6.25565004 9.097989082 -0.04492867 -6.27150393 9.11442947 -0.25833285
		 -6.25565004 9.024688721 -0.5117873 -6.25565004 9.097989082 -0.47173703 -5.76601839 8.90696335 -0.23053733
		 -5.76601839 8.9173584 -0.11114687 -5.76601839 8.90696335 0.016254425 -5.76601839 8.84273624 0.030239284
		 -5.76601839 8.8230648 -0.10313606 -5.76601839 8.84273624 -0.20446891 -6.11879969 8.89283276 -0.18212166
		 -6.11879969 8.89948463 -0.090120077 -6.11879969 8.89283276 0.010595262 -6.11879969 8.85173702 0.034077764
		 -6.11879969 8.83914948 -0.081406057 -6.11879969 8.85173702 -0.16203356 -5.59550524 8.84139919 -0.11895785
		 -5.59550524 8.84139919 -0.35107711 -5.45540094 9.11508751 -0.13573396 -5.45540094 9.042312622 0.03764689
		 -5.45540094 8.91880512 0.03764689 -5.45540094 8.84603024 -0.13573396 -5.45540094 8.84603024 -0.38093176
		 -5.45540094 8.91880512 -0.55431259 -5.45540094 9.042312622 -0.55431259 -5.45540094 9.11508751 -0.38093176
		 -5.45342302 8.98055935 0.049540699 -5.45342302 8.87438965 -0.0406335 -5.45086145 8.93055439 -0.018664479
		 -5.45342302 8.84640121 -0.25833285 -5.45086145 8.87162495 -0.15905887 -5.45342302 8.98055935 -0.56620634
		 -5.45342302 8.87438965 -0.4760322 -5.45086145 8.87162495 -0.35760686 -5.45086145 8.93055439 -0.49800122
		 -5.45342302 9.086728096 -0.47603244 -5.45086145 9.030563354 -0.49800122 -5.45342302 9.11471653 -0.25833285
		 -5.45086145 9.089492798 -0.35760686 -5.45342302 9.086728096 -0.0406335 -5.45086145 9.089492798 -0.15905887
		 -5.45086145 9.030563354 -0.018664479 -5.44317484 8.98055935 -0.0023397803 -5.38184214 8.98055935 -0.1303362
		 -5.44317484 8.89228058 -0.07731849 -5.38822794 8.93027115 -0.052036464 -5.44317484 8.86900902 -0.25833285
		 -5.38822794 8.88679314 -0.15561846 -5.38184214 8.92478371 -0.25833285 -5.44317484 8.98055935 -0.51432598
		 -5.38184214 8.98055935 -0.38632953 -5.44317484 8.89228058 -0.43934721 -5.38822794 8.88679314 -0.36104727
		 -5.38822794 8.93027115 -0.46462923 -5.44317484 9.068837166 -0.43934721 -5.38822794 9.030846596 -0.46462947
		 -5.44317484 9.092108727 -0.25833285 -5.38822794 9.074324608 -0.36104727 -5.38184214 9.036334038 -0.25833285
		 -5.44317484 9.068837166 -0.07731849 -5.38822794 9.074324608 -0.15561846 -5.38822794 9.030846596 -0.052036464
		 -5.4613328 9.092934608 -0.027908057 -5.4613328 8.98055935 -0.58420289 -5.4613328 8.86818314 -0.48875761
		 -5.48289537 8.91794109 -0.55845618 -5.4613328 8.83855915 -0.25833285 -5.48289537 8.84414673 -0.38264805
		 -5.4613328 8.86818314 -0.027908057 -5.48289537 8.84414673 -0.13401765 -5.4613328 8.98055935 0.067537189
		 -5.48289537 8.91794109 0.041790485 -5.48289537 9.043176651 0.041790485 -5.40738535 8.98055935 -0.0023397803
		 -5.40738535 9.068837166 -0.07731849 -5.42910242 9.02974987 -0.022565335 -5.40738535 9.092108727 -0.25833285
		 -5.42910242 9.087719917 -0.16067463 -5.40738535 9.068837166 -0.43934721 -5.42910242 9.087719917 -0.35599107
		 -5.40738535 8.98055935 -0.51432598 -5.42910242 9.02974987 -0.49410039 -5.40738535 8.89228058 -0.43934721
		 -5.42910242 8.93136787 -0.49410039 -5.40738535 8.86900902 -0.25833285 -5.42910242 8.87339783 -0.35599107
		 -5.40738535 8.89228058 -0.07731849 -5.42910242 8.87339783 -0.16067463 -5.42910242 8.93136787 -0.022565335
		 -5.45342302 8.98055935 0.014602184 -5.45342302 9.074679375 -0.065338463 -5.45342302 9.03631115 0.0088842511
		 -5.45342302 9.099492073 -0.25833285 -5.45342302 9.10201359 -0.14764786 -5.45342302 9.074679375 -0.45132726
		 -5.45342302 9.10201359 -0.36901787 -5.45342302 8.98055935 -0.53126788 -5.45342302 9.03631115 -0.52554989
		 -5.45342302 8.88643837 -0.45132726 -5.45342302 8.92480659 -0.52554989 -5.45342302 8.86162567 -0.25833285
		 -5.45342302 8.8591032 -0.36901787 -5.45342302 8.88643837 -0.065338463 -5.45342302 8.8591032 -0.14764786
		 -5.45342302 8.92480659 0.0088842511 -5.53175592 8.86818314 -0.027908057 -5.53175592 8.83855915 -0.25833285
		 -5.53175592 8.86818314 -0.48875761 -5.59550524 8.911973 -0.5119735 -5.53175592 8.98055935 -0.58420289
		 -5.66173029 8.82854176 -0.16964903 -5.66173029 8.85299587 0.0054852962 -6.25512171 8.8864603 -0.028425276
		 -5.66173029 8.93283463 -0.36900151 -5.66173029 8.85299587 -0.32647318 -6.25512171 8.90438271 -0.17095342
		 -6.25512171 8.8864603 -0.13035381 -5.48289537 9.11697102 -0.38264805 -5.58931971 9.12498856 -0.38264805
		 -5.53175592 9.12255859 -0.25833285 -5.48289537 9.11697102 -0.13401765 -5.53175592 9.092934608 -0.027908057
		 -5.4613328 9.12255859 -0.25833285 -5.4613328 9.092934608 -0.48875761 -5.59550524 8.911973 0.041938245
		 -5.48289537 9.043176651 -0.55845618 -5.58931971 9.051194191 -0.55845618 -5.53175592 9.092934608 -0.48875761
		 -5.53175592 8.98055935 0.067537189 -5.58931971 9.051194191 0.041790485 -6.25512171 8.90438271 0.012174129
		 -6.26889467 8.90762329 -0.087306201 -6.25512171 8.91959095 -0.14224508 -5.66173029 8.93283463 0.02512604
		 -5.62181139 8.97987556 -0.076409936 -5.66173029 8.94575691 -0.17422646 -6.25512171 8.91959095 -0.035181195
		 -5.58931971 9.12498856 -0.13401765 -5.62181139 8.97987556 -0.39820248 -5.63698244 9.028284073 0.036458313
		 -5.63698244 9.012629509 -0.25833285 -5.72515917 9.036682129 -0.082524717;
	setAttr ".vt[1494:1659]" -5.63698244 9.12500477 -0.027908057 -5.72515917 9.099300385 0.041790485
		 -5.63698244 9.15462875 -0.25833285 -5.72515917 9.1730938 -0.13401765 -5.63698244 9.028284073 -0.55312407
		 -5.63698244 9.12500477 -0.48875761 -5.72515917 9.099300385 -0.55845618 -5.72515917 9.036682129 -0.43414098
		 -5.72515917 9.1730938 -0.38264805 -5.90426111 9.060354233 0.036458313 -5.90426111 9.044699669 -0.25833285
		 -6.093067646 9.036911011 -0.082524717 -5.90426111 9.15707493 -0.027908057 -6.093067646 9.099529266 0.041790485
		 -5.90426111 9.18669987 -0.25833285 -6.093067646 9.17332268 -0.13401765 -5.90426111 9.060354233 -0.55312407
		 -5.90426111 9.15707493 -0.48875761 -6.093067646 9.099529266 -0.55845618 -6.093067646 9.036911011 -0.43414098
		 -6.093067646 9.17332268 -0.38264805 -6.21035624 9.0291996 0.036458313 -6.21035624 9.013545036 -0.25833285
		 -6.27150393 9.0072307587 -0.088961184 -6.21035624 9.12592125 -0.027908057 -6.27150393 9.05419445 0.0042751431
		 -6.21035624 9.15554523 -0.25833285 -6.27150393 9.10953999 -0.12758088 -6.29188585 9.053390503 -0.25833285
		 -6.21035624 9.0291996 -0.55312407 -6.21035624 9.12592125 -0.48875761 -6.27150393 9.05419445 -0.5209409
		 -6.27150393 9.0072307587 -0.42770451 -6.27150393 9.10953999 -0.38908482 -5.94395494 8.89348888 -0.18339011
		 -5.94395494 8.90176773 -0.090120077 -5.76601839 8.9173584 -0.18234912 -5.94395494 8.89348888 0.012304783
		 -5.76601839 8.9173584 -0.039944351 -5.94395494 8.84233475 0.03771472 -5.76601839 8.87577724 0.042445958
		 -5.94395494 8.82666779 -0.080965221 -5.76601839 8.82677555 -0.028756529 -5.94395494 8.84233475 -0.16302505
		 -5.76601839 8.82677555 -0.16149449 -5.76601839 8.87577724 -0.23269698 -6.21380663 8.90541935 -0.18129352
		 -6.21380663 8.91100788 -0.090120077 -6.11879969 8.89948463 -0.14498806 -6.21380663 8.90541935 0.0084446669
		 -6.11879969 8.89948463 -0.035252094 -6.21380663 8.8708868 0.025494695 -6.11879969 8.87287807 0.038401842
		 -6.21380663 8.86031055 -0.082728803 -6.11879969 8.84152412 -0.016466141 -6.21380663 8.8708868 -0.1613864
		 -6.11879969 8.84152412 -0.12891769 -6.11879969 8.87287807 -0.18378568 -5.38184214 8.92697811 -0.14044911
		 -5.38184214 8.92697811 -0.37621662 -5.38184214 9.034139633 -0.37621662 -5.38184214 9.034139633 -0.14044911
		 -5.45342302 8.92139912 0.025215983 -5.45342302 8.85168076 -0.14088315 -5.45342302 8.85168076 -0.37578255
		 -5.45342302 8.92139912 -0.54188168 -5.45342302 9.039718628 -0.54188168 -5.45342302 9.10943699 -0.37578255
		 -5.45342302 9.10943699 -0.14088315 -5.45342302 9.039718628 0.025215983 -5.44317484 8.93136787 -0.022565335
		 -5.44317484 8.87339783 -0.16067463 -5.44317484 8.87339783 -0.35599107 -5.44317484 8.93136787 -0.49410039
		 -5.44317484 9.02974987 -0.49410039 -5.44317484 9.087719917 -0.35599107 -5.44317484 9.087719917 -0.16067463
		 -5.44317484 9.02974987 -0.022565335 -5.4613328 8.91794109 -0.55845618 -5.4613328 8.84414673 -0.38264805
		 -5.4613328 8.84414673 -0.13401765 -5.4613328 8.91794109 0.041790485 -5.4613328 9.043176651 0.041790485
		 -5.40738535 9.02974987 -0.022565335 -5.40738535 9.087719917 -0.16067463 -5.40738535 9.087719917 -0.35599107
		 -5.40738535 9.02974987 -0.49410039 -5.40738535 8.93136787 -0.49410039 -5.40738535 8.87339783 -0.35599107
		 -5.40738535 8.87339783 -0.16067463 -5.40738535 8.93136787 -0.022565335 -5.45342302 9.033004761 -0.0069619417
		 -5.45342302 9.09481144 -0.15421152 -5.45342302 9.09481144 -0.36245421 -5.45342302 9.033004761 -0.50970376
		 -5.45342302 8.92811298 -0.50970376 -5.45342302 8.8663063 -0.36245421 -5.45342302 8.8663063 -0.15421152
		 -5.45342302 8.92811298 -0.0069619417 -5.53175592 8.84414673 -0.13401765 -5.53175592 8.84414673 -0.38264805
		 -5.53175592 8.91794109 -0.55845618 -5.66173029 8.83315468 -0.073778778 -5.66173029 8.89406872 -0.37252462
		 -5.66173029 8.83315468 -0.2563642 -5.53175592 9.11697102 -0.38264805 -5.53175592 9.11697102 -0.13401765
		 -5.4613328 9.11697102 -0.13401765 -5.4613328 9.11697102 -0.38264805 -5.4613328 9.043176651 -0.55845642
		 -5.53175592 8.91794109 0.041790485 -5.53175592 9.043176651 -0.55845618 -5.53175592 9.043176651 0.041790485
		 -6.26889467 8.9084816 -0.032762438 -6.26889467 8.9084816 -0.13622227 -5.66173029 8.89406872 0.042381644
		 -5.66173029 8.94575691 -0.05806607 -5.66173029 8.94575691 -0.29038686 -6.29188585 9.050597191 -0.10827118
		 -6.29188585 9.050597191 -0.40839452 -5.63698244 9.012629509 -0.082524717 -5.63698244 9.075247765 0.041790485
		 -5.63698244 9.14904118 -0.13401765 -5.63698244 9.075247765 -0.55845642 -5.63698244 9.012629509 -0.43414098
		 -5.63698244 9.14904118 -0.38264805 -5.90426111 9.044699669 -0.082524717 -5.90426111 9.10731792 0.041790485
		 -5.90426111 9.18111134 -0.13401765 -5.90426111 9.10731792 -0.55845642 -5.90426111 9.044699669 -0.43414098
		 -5.90426111 9.18111134 -0.38264805 -6.21035624 9.013545036 -0.082524717 -6.21035624 9.076163292 0.041790485
		 -6.21035624 9.1499567 -0.13401765 -6.21035624 9.076163292 -0.55845642 -6.21035624 9.013545036 -0.43414098
		 -6.21035624 9.1499567 -0.38264805 -5.94395494 8.90176773 -0.1457448 -5.94395494 8.90176773 -0.034495592
		 -5.94395494 8.86865044 0.041456759 -5.94395494 8.82962227 -0.014167726 -5.94395494 8.82962227 -0.12945276
		 -5.94395494 8.86865044 -0.18507725 -6.21380663 8.91100788 -0.14449447 -6.21380663 8.91100788 -0.035745949
		 -6.21380663 8.88865185 0.032268226 -6.21380663 8.86230564 -0.022105932 -6.21380663 8.86230564 -0.12856859
		 -6.21380663 8.88865185 -0.18294275 5.56569576 9.041184425 -0.0086714029 5.57408428 8.92079163 0.080478251
		 5.57170582 8.80461884 -0.00019598007 5.57621813 8.77628708 -0.21675104 5.58063507 8.80461884 -0.42872989
		 5.58699131 8.92079163 -0.53894901 5.5752964 9.041184425 -0.46942091 5.57049608 9.070808411 -0.23904616
		 5.43187237 9.031615257 -0.014641881 5.42991114 8.92079163 0.079464972 5.43187237 8.80996704 -0.014641881
		 5.43660641 8.78075218 -0.24183604 5.44134045 8.80996704 -0.4690302 5.4433012 8.92079163 -0.56313717
		 5.44134045 9.031615257 -0.4690302 5.43660641 9.060830116 -0.24183604;
	setAttr ".vt[1660:1825]" 5.42664623 8.92079163 0.018241584 5.4282341 8.83105183 -0.057961106
		 5.43206787 8.80739498 -0.2419306 5.43748903 8.92079163 -0.50210279 5.43590117 8.83105183 -0.4259001
		 5.43590117 9.010530472 -0.4259001 5.43206787 9.034187317 -0.2419306 5.4282341 9.010530472 -0.057961106
		 5.36478186 8.92079163 -0.01928997 5.36306334 8.92079163 -0.24336848 5.37092066 8.84085846 -0.075524807
		 5.36944818 8.82318497 -0.24323544 5.37411451 8.92079163 -0.46718091 5.37790585 8.84085846 -0.41073912
		 5.37790585 9.00072383881 -0.41073912 5.36944818 9.018398285 -0.24323544 5.37092066 9.00072383881 -0.075524807
		 5.45929432 9.033166885 -0.010888457 5.46409464 9.062790871 -0.24126327 5.46889496 9.033166885 -0.47163802
		 5.47088337 8.92079163 -0.56706262 5.46889496 8.80841541 -0.47163802 5.46409464 8.77879143 -0.24126327
		 5.45929432 8.80841541 -0.010888457 5.45730591 8.92079163 0.084536076 5.40498066 8.92079163 0.01355356
		 5.4065423 9.0090694427 -0.061408818 5.41031361 9.032341003 -0.2423839 5.41408443 9.0090694427 -0.42335898
		 5.41564655 8.92079163 -0.49832147 5.41408443 8.83251286 -0.42335898 5.41031361 8.80924129 -0.2423839
		 5.4065423 8.83251286 -0.061408818 5.42858458 8.92079163 0.048200428 5.4303546 9.020845413 -0.036761403
		 5.43462896 9.047221184 -0.24187726 5.43890285 9.020845413 -0.44699311 5.44067335 8.92079163 -0.53195488
		 5.43890285 8.82073689 -0.44699311 5.43462896 8.79436111 -0.24187726 5.4303546 8.82073689 -0.036761403
		 5.6025424 8.92010784 -0.21734729 6.23253727 8.82512093 -0.051635563 6.23263454 8.8596077 -0.05629617
		 6.21992445 8.82773018 0.039495707 6.22011328 8.85239124 0.030433059 6.22356749 8.85239124 -0.13534227
		 6.22330999 8.82773018 -0.12298167 5.70016432 8.99256897 0.0585109 5.70630598 8.97691441 -0.23621628
		 5.70150566 9.089289665 -0.0058414936 5.70630598 9.11891365 -0.23621628 5.71244717 8.99256897 -0.53094351
		 5.7111063 9.089289665 -0.46659103 6.067993164 8.99279785 0.066175401 6.074134827 8.97714329 -0.22855175
		 6.069334507 9.089518547 0.0018230081 6.074134827 9.11914253 -0.22855175 6.080276012 8.99279785 -0.52327901
		 6.078935146 9.089518547 -0.45892653 6.23140144 8.96492004 0.02823472 6.25253248 8.94816208 -0.22483444
		 6.23223591 9.038221359 -0.011806846 6.25253248 9.054661751 -0.22483444 6.24196196 8.964921 -0.4785642
		 6.24112749 9.038221359 -0.43852261 5.74657726 8.84719563 -0.20757559 5.74409008 8.85759068 -0.08821106
		 5.741436 8.84719563 0.039162636 5.74114466 8.78296852 0.053144455 5.74392319 8.76329708 -0.080201983
		 5.74603415 8.78296852 -0.1815128 6.098273277 8.83306503 -0.15182105 6.096356869 8.83971691 -0.059839427
		 6.094258308 8.83306503 0.040854037 6.093769073 8.7919693 0.064331472 6.096175194 8.77938175 -0.051127315
		 6.097854614 8.7919693 -0.13173729 5.57377625 8.78163147 -0.099572539 5.57861233 8.78163147 -0.33164144
		 5.43405199 9.055319786 -0.11926377 5.43044043 8.9825449 0.054079473 5.43044043 8.8590374 0.054079473
		 5.43405199 8.78626251 -0.11926377 5.43916035 8.78626251 -0.36440837 5.44277239 8.8590374 -0.53775156
		 5.44277239 8.9825449 -0.53775156 5.43916035 9.055319786 -0.36440837 5.42821503 8.92079163 0.065929472
		 5.43009377 8.81462193 -0.024225116 5.42707491 8.87078667 -0.0023142695 5.43462896 8.78663349 -0.24187726
		 5.42999935 8.81185722 -0.14267817 5.44104242 8.92079163 -0.54968393 5.43916416 8.81462193 -0.45952934
		 5.43413591 8.81185722 -0.3411831 5.43706083 8.87078667 -0.48154697 5.43916416 9.026960373 -0.45952958
		 5.43706083 8.97079563 -0.48154697 5.43462896 9.054948807 -0.24187726 5.43413591 9.029725075 -0.3411831
		 5.43009377 9.026960373 -0.024225116 5.42999935 9.029725075 -0.14267817 5.42707491 8.97079563 -0.0023142695
		 5.41905022 8.92079163 0.013846755 5.36039686 8.92079163 -0.1153996 5.42061186 8.83251286 -0.061115682
		 5.36515045 8.87050343 -0.036983848 5.42438316 8.80924129 -0.24209076 5.36730814 8.82702541 -0.14054334
		 5.36306334 8.86501598 -0.24336848 5.42971611 8.92079163 -0.49802834 5.36572981 8.92079163 -0.37133738
		 5.42815399 8.83251286 -0.42306584 5.37158775 8.82702541 -0.34592757 5.37374592 8.87050343 -0.44948706
		 5.42815399 9.0090694427 -0.42306584 5.37374592 8.97107887 -0.4494873 5.42438316 9.032341003 -0.24209076
		 5.37158775 9.014556885 -0.34592757 5.36306334 8.97656631 -0.24336848 5.42061186 9.0090694427 -0.061115682
		 5.36730814 9.014556885 -0.14054334 5.36515045 8.97107887 -0.036983848 5.43773651 9.033166885 -0.011337698
		 5.44932556 8.92079163 -0.5675118 5.44733715 8.80841541 -0.47208723 5.47034693 8.85817337 -0.54132146
		 5.44253683 8.77879143 -0.24171245 5.46668434 8.78437901 -0.3655515 5.43773651 8.80841541 -0.011337698
		 5.46150446 8.78437901 -0.11697504 5.4357481 8.92079163 0.084086835 5.45784187 8.85817337 0.058794975
		 5.45784187 8.98340893 0.058794975 5.38326836 8.92079163 0.013101161 5.38483 9.0090694427 -0.061861277
		 5.40540171 8.96998215 -0.0066675544 5.3886013 9.032341003 -0.24283633 5.40827894 9.027952194 -0.1447469
		 5.39237213 9.0090694427 -0.42381141 5.41234779 9.027952194 -0.34002095 5.39393425 8.92079163 -0.4987739
		 5.41522503 8.96998215 -0.4781003 5.39237213 8.83251286 -0.42381141 5.41522503 8.87160015 -0.4781003
		 5.3886013 8.80924129 -0.24283633 5.41234779 8.8136301 -0.34002095 5.38483 8.83251286 -0.061861277
		 5.40827894 8.8136301 -0.1447469 5.40540171 8.87160015 -0.0066675544 5.42894268 8.92079163 0.030998588
		 5.43060827 9.014911652 -0.048924744 5.42906189 8.97654343 0.025281847 5.43462896 9.03972435 -0.24187726
		 5.43232298 9.042245865 -0.13121629 5.43864918 9.014911652 -0.43482977 5.43693447 9.042245865 -0.35253826
		 5.44031477 8.92079163 -0.51475304 5.44019556 8.97654343 -0.5090363 5.43864918 8.82667065 -0.43482977
		 5.44019556 8.86503887 -0.5090363 5.43462896 8.80185795 -0.24187726 5.43693447 8.79933548 -0.35253826
		 5.43060827 8.82667065 -0.048924744 5.43232298 8.79933548 -0.13121629;
	setAttr ".vt[1826:1991]" 5.42906189 8.86503887 0.025281847 5.50814438 8.80841541 -0.0098705888
		 5.5129447 8.77879143 -0.24024537 5.51774502 8.80841541 -0.47062013 5.58196402 8.85220528 -0.49250293
		 5.51973343 8.92079163 -0.56604469 5.64104319 8.76877403 -0.14887309 5.63739491 8.79322815 0.026223242
		 6.23136377 8.82669258 0.0046819448 5.64519644 8.8730669 -0.34818232 5.64431047 8.79322815 -0.3056632
		 6.23433304 8.84461498 -0.1378153 6.23348713 8.82669258 -0.097224474 5.46668434 9.057203293 -0.3655515
		 5.57308578 9.065220833 -0.36333439 5.5129447 9.062790871 -0.24024537 5.46150446 9.057203293 -0.11697504
		 5.50814438 9.033166885 -0.0098705888 5.44253683 9.062790871 -0.24171245 5.44733715 9.033166885 -0.47208723
		 5.57042456 8.85220528 0.061288595 5.47034693 8.98340893 -0.54132146 5.57674837 8.99142647 -0.53910434
		 5.51774502 9.033166885 -0.47062013 5.50615597 8.92079163 0.085553944 5.56424332 8.99142647 0.06101203
		 6.23051786 8.84461498 0.04527247 6.24636078 8.84785557 -0.053899288 6.23373508 8.85982323 -0.10911322
		 5.63698578 8.8730669 0.045859694 5.59919071 8.92010784 -0.056485832 5.64113855 8.88598919 -0.15344954
		 6.23150444 8.85982323 -0.0020725131 5.5679059 9.065220833 -0.11475793 5.60589409 8.92010784 -0.37820858
		 5.61200666 8.96851635 0.056673944 5.61814833 8.95286179 -0.23805323 5.70264339 8.97691441 -0.060446262
		 5.61334801 9.065237045 -0.0076784492 5.70005322 9.039532661 0.063841939 5.61814833 9.094861031 -0.23805323
		 5.7037158 9.11332607 -0.11192805 5.62428951 8.96851635 -0.53278047 5.62294865 9.065237045 -0.46842799
		 5.71255827 9.039532661 -0.53627449 5.70996857 8.97691441 -0.41198626 5.70889568 9.11332607 -0.36050451
		 5.87922764 9.0005865097 0.062242091 5.8853693 8.98493195 -0.23248512 6.07047224 8.97714329 -0.052781761
		 5.88056898 9.097307205 -0.0021103024 6.067882061 9.039761543 0.071506441 5.8853693 9.12693214 -0.23248512
		 6.071544647 9.11355495 -0.10426354 5.89151049 9.0005865097 -0.52721238 5.89016962 9.097307205 -0.46285987
		 6.080387115 9.039761543 -0.52860999 6.077797413 8.97714329 -0.40432173 6.076724529 9.11355495 -0.35284001
		 6.185256 8.96943188 0.068618834 6.19139767 8.95377731 -0.22610831 6.24900389 8.94746304 -0.055499554
		 6.18659735 9.066153526 0.0042664409 6.24706173 8.99442673 0.037716508 6.19139767 9.095777512 -0.22610831
		 6.24980831 9.049772263 -0.094110847 6.27290964 8.99362278 -0.22440985 6.19753885 8.96943188 -0.52083558
		 6.19619799 9.066153526 -0.4564831 6.25800323 8.99442673 -0.48738551 6.2560606 8.94746304 -0.39416936
		 6.25525618 9.049772263 -0.35555804 5.92349291 8.83372116 -0.15673172 5.92155027 8.84200001 -0.063481927
		 5.74557352 8.85759068 -0.15939781 5.91941643 8.83372116 0.038920701 5.74260664 8.85759068 -0.017023921
		 5.91888714 8.78256702 0.064325154 5.7408905 8.81600952 0.065348446 5.92135954 8.76690006 -0.054329038
		 5.74237394 8.76700783 -0.005838573 5.923069 8.78256702 -0.13637108 5.74513912 8.76700783 -0.13854772
		 5.74662256 8.81600952 -0.20973477 6.19324255 8.84565163 -0.14901382 6.19134331 8.85124016 -0.057860196
		 6.097499847 8.83971691 -0.11469552 6.18928957 8.84565163 0.04068315 6.095213413 8.83971691 -0.0049833655
		 6.18893433 8.81111908 0.057729483 6.093679428 8.81311035 0.068654597 6.19118929 8.80054283 -0.050470531
		 6.094822407 8.7817564 0.013798535 6.1928277 8.81111908 -0.12911105 6.097165108 8.7817564 -0.09862864
		 6.098308086 8.81311035 -0.1534847 5.36060715 8.86721039 -0.12551033 5.36551905 8.86721039 -0.36122668
		 5.36551905 8.97437191 -0.36122668 5.36060715 8.97437191 -0.12551033 5.4287219 8.86163139 0.041610062
		 5.43218184 8.79191303 -0.12445304 5.43707561 8.79191303 -0.35930148 5.44053602 8.86163139 -0.52536452
		 5.44053602 8.9799509 -0.52536452 5.43707561 9.049669266 -0.35930148 5.43218184 9.049669266 -0.12445304
		 5.4287219 8.9799509 0.041610062 5.41947126 8.87160015 -0.0063744187 5.4223485 8.8136301 -0.1444537
		 5.42641735 8.8136301 -0.33972776 5.42929459 8.87160015 -0.47780713 5.42929459 8.96998215 -0.47780713
		 5.42641735 9.027952194 -0.33972776 5.4223485 9.027952194 -0.1444537 5.41947126 8.96998215 -0.0063744187
		 5.44878912 8.85817337 -0.5417707 5.44512653 8.78437901 -0.36600071 5.43994665 8.78437901 -0.11742425
		 5.43628407 8.85817337 0.058345735 5.43628407 8.98340893 0.058345735 5.3836894 8.96998215 -0.0071200132
		 5.38656664 9.027952194 -0.1451993 5.39063549 9.027952194 -0.34047335 5.39351273 8.96998215 -0.4785527
		 5.39351273 8.87160015 -0.4785527 5.39063549 8.8136301 -0.34047335 5.38656664 8.8136301 -0.1451993
		 5.3836894 8.87160015 -0.0071200132 5.42939186 8.97323704 0.0094391108 5.43245983 9.035043716 -0.13777852
		 5.4367981 9.035043716 -0.34597602 5.43986559 8.97323704 -0.49319363 5.43986559 8.86834526 -0.49319363
		 5.4367981 8.80653858 -0.34597602 5.43245983 8.80653858 -0.13777852 5.42939186 8.86834526 0.0094391108
		 5.51035452 8.78437901 -0.11595714 5.5155344 8.78437901 -0.3645336 5.51919699 8.85817337 -0.54030359
		 5.63904619 8.77338696 -0.053023636 5.64526987 8.83430099 -0.35170466 5.64284992 8.77338696 -0.23556945
		 5.5155344 9.057203293 -0.3645336 5.51035452 9.057203293 -0.11595714 5.43994665 9.057203293 -0.11742425
		 5.44512653 9.057203293 -0.36600071 5.44878912 8.98340893 -0.54177094 5.50669193 8.85817337 0.059812844
		 5.51919699 8.98340893 -0.54030359 5.50669193 8.98340893 0.059812844 6.24522448 8.84871387 0.0006326437
		 6.24737978 8.84871387 -0.10280472 5.63662624 8.83430099 0.063111603 5.63871861 8.88598919 -0.037314355
		 5.6435585 8.88598919 -0.26958475 6.2697835 8.99082947 -0.074380755 6.27603579 8.99082947 -0.37443894
		 5.61448574 8.95286179 -0.062283218 5.61189556 9.015480042 0.062004983 5.61555815 9.089273453 -0.113765
		 5.62440062 9.015480042 -0.53811169 5.62181091 8.95286179 -0.41382322 5.62073803 9.089273453 -0.36234146
		 5.88170671 8.98493195 -0.056715131 5.87911654 9.047550201 0.06757313;
	setAttr ".vt[1992:2157]" 5.88277912 9.12134361 -0.10819685 5.89162159 9.047550201 -0.53254354
		 5.88903189 8.98493195 -0.4082551 5.887959 9.12134361 -0.35677332 6.18773508 8.95377731 -0.050338328
		 6.1851449 9.016395569 0.073949873 6.18880749 9.09018898 -0.10182011 6.19764996 9.016395569 -0.5261668
		 6.19506025 8.95377731 -0.4018783 6.19398737 9.09018898 -0.35039657 5.92270899 8.84200001 -0.11909455
		 5.92039108 8.84200001 -0.007869482 5.91880894 8.80888271 0.068066359 5.91996765 8.76985455 0.012453914
		 5.92236948 8.76985455 -0.10280609 5.92352819 8.80888271 -0.15841848 6.1924758 8.85124016 -0.11222279
		 6.19021034 8.85124016 -0.003497839 6.18879366 8.82888412 0.064501524 6.18992615 8.80253792 0.010139227
		 6.19214392 8.80253792 -0.096300364 6.19327688 8.82888412 -0.15066272 -0.13899398 4.97266531 1.024589539
		 0.12515238 4.97266531 1.024589539 -0.13899398 5.23681164 1.024589539 0.12515265 5.23681164 1.024589539
		 -0.13899398 5.23681164 0.76044273 0.12515265 5.23681164 0.76044273 -0.13899398 4.97266531 0.76044273
		 0.12515265 4.97266531 0.76044273 -0.0069207959 4.93792057 1.059333801 -0.0069207959 5.27155638 1.059333801
		 -0.0069207959 5.27155638 0.72569847 -0.0069207959 4.93792057 0.72569847 -0.17373848 5.10473824 1.059333801
		 0.15989688 5.10473824 1.059333801 -0.17373848 5.27155638 0.89251614 0.15989688 5.27155638 0.89251614
		 -0.17373848 5.10473824 0.72569847 0.15989688 5.10473824 0.72569847 -0.17373848 4.93792057 0.89251614
		 0.15989688 4.93792057 0.89251614 -0.0069207959 5.10473824 1.12034225 -0.0069207959 5.33256531 0.89251614
		 -0.0069207959 5.10473824 0.66468954 -0.0069207959 4.87691259 0.89251614 0.22090578 5.10473824 0.89251614
		 -0.23474711 5.10473824 0.89251614 -0.085264236 4.94940186 1.047852039 0.071422666 4.94940186 1.047852039
		 -0.085264236 5.26007462 1.047852039 0.071422666 5.26007462 1.047852039 -0.085264236 5.26007462 0.73717999
		 0.071422666 5.26007462 0.73717999 -0.085264236 4.94940186 0.73717999 0.071422666 4.94940186 0.73717999
		 -0.16225708 5.026394844 1.047852039 -0.16225708 5.1830821 1.047852039 0.14841548 5.026394844 1.047852039
		 0.14841548 5.1830821 1.047852039 -0.16225708 5.26007462 0.97085977 -0.16225708 5.26007462 0.81417274
		 0.14841548 5.26007462 0.97085977 0.14841548 5.26007462 0.81417274 -0.16225708 5.1830821 0.73717999
		 -0.16225708 5.026394844 0.73717999 0.14841548 5.1830821 0.73717999 0.14841548 5.026394844 0.73717999
		 -0.16225708 4.94940186 0.81417274 -0.16225708 4.94940186 0.97085977 0.14841548 4.94940186 0.81417274
		 0.14841548 4.94940186 0.97085977 -0.0069207959 5.013787746 1.10503387 0.084029734 5.10473824 1.10503387
		 -0.0069207959 5.1956892 1.10503387 -0.097871333 5.10473824 1.10503387 -0.0069207959 5.31725597 0.98346663
		 0.084029734 5.31725597 0.89251614 -0.0069207959 5.31725597 0.80156565 -0.097871333 5.31725597 0.89251614
		 -0.0069207959 5.1956892 0.67999816 0.084029734 5.10473824 0.67999816 -0.0069207959 5.013787746 0.67999816
		 -0.097871333 5.10473824 0.67999816 -0.0069207959 4.8922205 0.80156565 0.084029734 4.8922205 0.89251614
		 -0.0069207959 4.8922205 0.98346663 -0.097871333 4.8922205 0.89251614 0.20559716 5.013787746 0.89251614
		 0.20559716 5.10473824 0.80156565 0.20559716 5.1956892 0.89251614 0.20559716 5.10473824 0.98346663
		 -0.21943879 5.013787746 0.89251614 -0.21943879 5.10473824 0.98346663 -0.21943879 5.1956892 0.89251614
		 -0.21943879 5.10473824 0.80156565 0.0777262 5.02009201 1.09062624 0.0777262 5.18938589 1.09062624
		 -0.091567785 5.18938589 1.09062624 -0.091567785 5.02009201 1.09062624 0.0777262 5.30284834 0.97716308
		 0.0777262 5.30284834 0.8078692 -0.091567785 5.30284834 0.8078692 -0.091567785 5.30284834 0.97716308
		 0.0777262 5.18938589 0.69440627 0.0777262 5.02009201 0.69440627 -0.091567785 5.02009201 0.69440627
		 -0.091567785 5.18938589 0.69440627 0.0777262 4.90662861 0.8078692 0.0777262 4.90662861 0.97716308
		 -0.091567785 4.90662861 0.97716308 -0.091567785 4.90662861 0.8078692 0.19118911 5.02009201 0.8078692
		 0.19118911 5.18938589 0.8078692 0.19118911 5.18938589 0.97716308 0.19118911 5.02009201 0.97716308
		 -0.20503068 5.02009201 0.97716308 -0.20503068 5.18938589 0.97716308 -0.20503068 5.18938589 0.8078692
		 -0.20503068 5.02009201 0.8078692 0.22682327 4.3058815 1.39585018 0.36589921 4.31624508 1.3890686
		 -0.027459756 5.0067725182 0.99077249 0.099672079 5.055079937 0.95916009 -0.027459756 4.93031645 0.87393928
		 0.099672079 4.97862434 0.84232688 0.22682327 4.22942543 1.27901697 0.36589921 4.23978901 1.27223539
		 -0.015760478 4.9569664 1.04586935 -0.015760478 4.85375118 0.88814402 0.15788299 4.91256332 0.8496573
		 0.15788299 5.015778542 1.0073823929 0.19654989 4.37321663 1.38098764 0.19654965 4.27000141 1.22326279
		 0.38430214 4.28399181 1.21410799 0.38430214 4.38720703 1.37183285 0.14303407 4.67623043 1.18939447
		 0.14303407 4.57301521 1.031669617 0.32877088 4.59340858 1.018323898 0.32877088 4.69662333 1.17604876
		 0.29655403 4.32263184 1.41409016 0.034835957 5.046646118 0.99388075 0.034835957 4.9434309 0.83615589
		 0.29655373 4.21941662 1.2563653 0.20193434 4.32262135 1.41409731 0.38968682 4.33661175 1.40494251
		 -0.050977945 4.96243095 0.93635654 0.12065011 5.027646065 0.89367986 -0.046079338 4.90179634 0.86340117
		 0.12554872 4.96701193 0.82072425 0.20267779 4.2640295 1.3398056 0.39042997 4.27801991 1.33065033
		 -0.046079338 5.0050115585 1.021125793 0.062132597 4.73368406 0.94662166 -0.044701055 4.89555645 0.9734211
		 0.24182272 4.77328682 0.92070556 0.071061254 4.86595488 0.84261322 0.12554872 5.070226669 0.97844911
		 0.18682355 4.97397232 0.92210531 0.071061254 5.0035748482 1.052913189 0.18237811 4.50637722 1.29384756
		 0.20193434 4.21940613 1.25637245 0.16525766 4.31927681 1.30365133 0.38968682 4.23339653 1.24721718
		 0.2904259 4.25979424 1.19239807 0.37013054 4.52036762 1.28469229;
	setAttr ".vt[2158:2259]" 0.41559434 4.33793068 1.2914443 0.2904259 4.39741421 1.40269756
		 0.062132597 4.8368988 1.10434675 0.18237811 4.40316296 1.1361227 0.11207786 4.62122345 1.11275578
		 0.37013054 4.41715336 1.12696743 0.23590249 4.56600952 0.9987092 0.24182272 4.87650204 1.078430653
		 0.35972714 4.64841461 1.09496212 0.23590249 4.70362902 1.20900917 0.29581058 4.34681892 1.43580723
		 0.033203088 4.99804688 0.91304946 0.29581058 4.20919895 1.22550726 0.29680157 4.26869678 1.33675146
		 0.42097878 4.2873354 1.32455397 0.17064238 4.26868153 1.336761 -0.074683875 4.9425354 0.94937634
		 0.039734825 4.91720152 0.81577516 0.15415329 5.029488564 0.8924737 0.039734825 5.054822445 1.026075363
		 0.15108609 4.45243835 1.21651077 0.2762543 4.39295483 1.10525751 0.4014225 4.47109222 1.20430374
		 0.2762543 4.53057575 1.31555748 0.032184206 4.77869081 1.029803753 0.15197754 4.7362833 0.90737605
		 0.27177113 4.83149529 0.99524856 0.15197754 4.87390327 1.11767626 -0.54100108 4.15411568 1.38558722
		 -0.67802763 4.17655563 1.37090302 -0.056843415 4.99658632 0.99743819 -0.17726332 5.05572176 0.95874047
		 -0.056843415 4.92013025 0.88060474 -0.17726332 4.97926617 0.84190679 -0.54100108 4.077659607 1.26875401
		 -0.67802763 4.10009956 1.25406981 -0.07596916 4.9481287 1.051652431 -0.07596916 4.84491301 0.89392734
		 -0.24133867 4.91855288 0.8457377 -0.24133867 5.02176857 1.0034623146 -0.48579609 4.23944664 1.37264538
		 -0.48579609 4.13623142 1.21492052 -0.67078221 4.16652489 1.19509697 -0.67078221 4.2697401 1.35282183
		 -0.28495651 4.66174412 1.18517685 -0.28495651 4.5585289 1.027451992 -0.46714032 4.59501457 1.0035758018
		 -0.46714032 4.69822979 1.16130066 -0.60993159 4.17693138 1.39985847 -0.11549866 5.041749954 0.99708462
		 -0.11549866 4.93853569 0.83935952 -0.60993159 4.073716164 1.24213362 -0.51582921 4.16866589 1.40526724
		 -0.7008152 4.19895935 1.38544321 -0.034215182 4.95022631 0.94434309 -0.19678211 5.030059338 0.89210105
		 -0.040211849 4.89006948 0.87107563 -0.20277882 4.96990108 0.81883359 -0.51743865 4.11017704 1.33090782
		 -0.70242476 4.1404705 1.31108427 -0.040211849 4.99328375 1.028800488 -0.16725045 4.73224592 0.94756293
		 -0.048407629 4.88424778 0.98082137 -0.34102714 4.78730869 0.91152978 -0.1586538 4.86453104 0.84354496
		 -0.20277882 5.073116779 0.97655845 -0.26890022 4.9824338 0.91656852 -0.1586538 5.0021510124 1.053844929
		 -0.39998841 4.43343925 1.28678942 -0.51582921 4.065450668 1.24754238 -0.454965 4.18279076 1.29708719
		 -0.7008152 4.095744133 1.22771835 -0.57828903 4.13417625 1.17872143 -0.58497453 4.46373272 1.26696539
		 -0.70161295 4.22318172 1.27065563 -0.57828903 4.27179623 1.3890214 -0.16725045 4.83546066 1.10528755
		 -0.39998841 4.33022404 1.12906408 -0.25459266 4.6040554 1.11029387 -0.58497453 4.3605175 1.10924053
		 -0.37604856 4.55956936 0.9892261 -0.34102714 4.89052391 1.069254875 -0.49750435 4.65270329 1.078458786
		 -0.37604856 4.69718933 1.19952631 -0.60832214 4.20101452 1.42164326 -0.11349985 4.9929924 0.91635704
		 -0.60832214 4.063394547 1.21134281 -0.61046815 4.12302971 1.32249689 -0.73164606 4.15240002 1.30327702
		 -0.48499811 4.11200905 1.32970905 -0.013117269 4.92837095 0.95864511 -0.12149534 4.91278315 0.81866693
		 -0.22987336 5.034814358 0.88898873 -0.12149534 5.050403118 1.028966904 -0.36915743 4.37678242 1.21123075
		 -0.49248159 4.32816792 1.092864513 -0.61580563 4.41717339 1.18479872 -0.49248159 4.46578789 1.30316448
		 -0.13828775 4.7746768 1.032430649 -0.25413877 4.74257469 0.9032588 -0.36998975 4.84809351 0.98438692
		 -0.25413877 4.88019466 1.11355877;
	setAttr -s 4480 ".ed";
	setAttr ".ed[0:165]"  43 1 1 1 52 1 52 122 1 122 43 1 52 13 1 13 67 1 67 122 1
		 67 20 1 20 66 1 66 122 1 66 8 1 8 43 1 53 3 1 3 45 1 45 123 1 123 53 1 45 9 1 9 68 1
		 68 123 1 68 20 1 67 123 1 13 53 1 44 2 1 2 51 1 51 124 1 124 44 1 51 12 1 12 69 1
		 69 124 1 69 20 1 68 124 1 9 44 1 50 0 1 0 42 1 42 125 1 125 50 1 42 8 1 66 125 1
		 69 125 1 12 50 1 3 56 1 56 126 1 126 45 1 56 15 1 15 71 1 71 126 1 71 21 1 21 70 1
		 70 126 1 70 9 1 57 5 1 5 47 1 47 127 1 127 57 1 47 10 1 10 72 1 72 127 1 72 21 1
		 71 127 1 15 57 1 46 4 1 4 55 1 55 128 1 128 46 1 55 14 1 14 73 1 73 128 1 73 21 1
		 72 128 1 10 46 1 54 2 1 44 129 1 129 54 1 70 129 1 73 129 1 14 54 1 5 60 1 60 130 1
		 130 47 1 60 17 1 17 75 1 75 130 1 75 22 1 22 74 1 74 130 1 74 10 1 61 7 1 7 49 1
		 49 131 1 131 61 1 49 11 1 11 76 1 76 131 1 76 22 1 75 131 1 17 61 1 48 6 1 6 59 1
		 59 132 1 132 48 1 59 16 1 16 77 1 77 132 1 77 22 1 76 132 1 11 48 1 58 4 1 46 133 1
		 133 58 1 74 133 1 77 133 1 16 58 1 104 34 1 34 106 1 106 134 1 134 104 1 106 35 1
		 35 107 1 107 134 1 107 36 1 36 108 1 108 134 1 108 33 1 33 104 1 110 37 1 37 112 1
		 112 135 1 135 110 1 112 38 1 38 113 1 113 135 1 113 36 1 107 135 1 35 110 1 115 39 1
		 39 117 1 117 136 1 136 115 1 117 40 1 40 118 1 118 136 1 118 36 1 113 136 1 38 115 1
		 120 41 1 41 121 1 121 137 1 137 120 1 121 33 1 108 137 1 118 137 1 40 120 1 64 7 1
		 61 138 1 138 64 1 17 79 1 79 138 1 79 23 1 23 78 1 78 138 1 78 19 1 19 64 1 57 139 1
		 139 60 1 15 80 1 80 139 1;
	setAttr ".ed[166:331]" 80 23 1 79 139 1 53 140 1 140 56 1 13 81 1 81 140 1
		 81 23 1 80 140 1 1 65 1 65 141 1 141 52 1 65 19 1 78 141 1 81 141 1 63 0 1 50 142 1
		 142 63 1 12 83 1 83 142 1 83 24 1 24 82 1 82 142 1 82 18 1 18 63 1 54 143 1 143 51 1
		 14 84 1 84 143 1 84 24 1 83 143 1 58 144 1 144 55 1 16 85 1 85 144 1 85 24 1 84 144 1
		 6 62 1 62 145 1 145 59 1 62 18 1 82 145 1 85 145 1 7 87 1 87 146 1 146 49 1 87 26 1
		 26 88 1 88 146 1 88 25 1 25 86 1 86 146 1 86 11 1 19 89 1 89 147 1 147 64 1 89 27 1
		 27 90 1 90 147 1 90 26 1 87 147 1 1 91 1 91 148 1 148 65 1 91 28 1 28 92 1 92 148 1
		 92 27 1 89 148 1 8 93 1 93 149 1 149 43 1 93 29 1 29 94 1 94 149 1 94 28 1 91 149 1
		 0 95 1 95 150 1 150 42 1 95 30 1 30 96 1 96 150 1 96 29 1 93 150 1 18 97 1 97 151 1
		 151 63 1 97 31 1 31 98 1 98 151 1 98 30 1 95 151 1 6 99 1 99 152 1 152 62 1 99 32 1
		 32 100 1 100 152 1 100 31 1 97 152 1 86 153 1 153 48 1 25 101 1 101 153 1 101 32 1
		 99 153 1 26 103 1 103 154 1 154 88 1 103 34 1 104 154 1 33 102 1 102 154 1 102 25 1
		 27 105 1 105 155 1 155 90 1 105 35 1 106 155 1 103 155 1 28 109 1 109 156 1 156 92 1
		 109 37 1 110 156 1 105 156 1 29 111 1 111 157 1 157 94 1 111 38 1 112 157 1 109 157 1
		 30 114 1 114 158 1 158 96 1 114 39 1 115 158 1 111 158 1 31 116 1 116 159 1 159 98 1
		 116 40 1 117 159 1 114 159 1 32 119 1 119 160 1 160 100 1 119 41 1 120 160 1 116 160 1
		 102 161 1 161 101 1 121 161 1 119 161 1 244 181 1 181 249 1 249 404 1 404 244 1 249 227 1
		 227 340 1 340 404 1 340 228 1 228 251 1 251 404 1 251 180 1 180 244 1;
	setAttr ".ed[332:497]" 260 183 1 183 258 1 258 405 1 405 260 1 258 184 1 184 245 1
		 245 405 1 245 232 1 232 350 1 350 405 1 350 233 1 233 260 1 262 182 1 182 259 1 259 406 1
		 406 262 1 259 183 1 260 406 1 233 352 1 352 406 1 352 234 1 234 262 1 353 227 1 249 407 1
		 407 353 1 181 261 1 261 407 1 261 182 1 262 407 1 234 353 1 264 179 1 179 263 1 263 408 1
		 408 264 1 263 180 1 251 408 1 228 342 1 342 408 1 342 229 1 229 264 1 184 265 1 265 409 1
		 409 245 1 265 185 1 185 266 1 266 409 1 266 231 1 231 348 1 348 409 1 348 232 1 337 219 1
		 219 275 1 275 410 1 410 337 1 275 176 1 176 285 1 285 410 1 285 175 1 175 267 1 267 410 1
		 267 226 1 226 337 1 175 283 1 283 411 1 411 267 1 283 174 1 174 268 1 268 411 1 268 225 1
		 225 336 1 336 411 1 336 226 1 174 282 1 282 412 1 412 268 1 282 173 1 173 269 1 269 412 1
		 269 224 1 224 334 1 334 412 1 334 225 1 173 281 1 281 413 1 413 269 1 281 172 1 172 270 1
		 270 413 1 270 223 1 223 332 1 332 413 1 332 224 1 172 280 1 280 414 1 414 270 1 280 171 1
		 171 271 1 271 414 1 271 222 1 222 330 1 330 414 1 330 223 1 171 287 1 287 415 1 415 271 1
		 287 170 1 170 272 1 272 415 1 272 221 1 221 328 1 328 415 1 328 222 1 170 273 1 273 416 1
		 416 272 1 273 177 1 177 274 1 274 416 1 274 220 1 220 326 1 326 416 1 326 221 1 177 286 1
		 286 417 1 417 274 1 286 176 1 275 417 1 219 324 1 324 417 1 324 220 1 344 230 1 230 278 1
		 278 418 1 418 344 1 278 178 1 178 277 1 277 418 1 277 179 1 264 418 1 229 344 1 346 231 1
		 266 419 1 419 346 1 185 276 1 276 419 1 276 178 1 278 419 1 230 346 1 289 173 1 282 420 1
		 420 289 1 283 420 1 175 279 1 279 420 1 279 202 1 202 289 1 288 202 1 279 421 1 421 288 1
		 285 421 1 286 421 1 177 288 1 273 422 1 422 288 1 287 422 1 171 284 1;
	setAttr ".ed[498:663]" 284 422 1 284 202 1 284 423 1 423 289 1 280 423 1 281 423 1
		 291 204 1 204 292 1 292 424 1 424 291 1 292 203 1 203 290 1 290 424 1 290 238 1 238 362 1
		 362 424 1 362 239 1 239 291 1 293 205 1 205 294 1 294 425 1 425 293 1 294 204 1 291 425 1
		 239 364 1 364 425 1 364 240 1 240 293 1 295 206 1 206 296 1 296 426 1 426 295 1 296 205 1
		 293 426 1 240 366 1 366 426 1 366 241 1 241 295 1 297 207 1 207 298 1 298 427 1 427 297 1
		 298 206 1 295 427 1 241 368 1 368 427 1 368 242 1 242 297 1 369 235 1 235 299 1 299 428 1
		 428 369 1 299 208 1 208 300 1 300 428 1 300 207 1 297 428 1 242 369 1 301 209 1 209 302 1
		 302 429 1 429 301 1 302 208 1 299 429 1 235 356 1 356 429 1 356 236 1 236 301 1 303 210 1
		 210 304 1 304 430 1 430 303 1 304 209 1 301 430 1 236 358 1 358 430 1 358 237 1 237 303 1
		 203 305 1 305 431 1 431 290 1 305 210 1 303 431 1 237 360 1 360 431 1 360 238 1 307 212 1
		 212 308 1 308 432 1 432 307 1 308 211 1 211 306 1 306 432 1 306 203 1 292 432 1 204 307 1
		 309 213 1 213 310 1 310 433 1 433 309 1 310 212 1 307 433 1 294 433 1 205 309 1 311 214 1
		 214 312 1 312 434 1 434 311 1 312 213 1 309 434 1 296 434 1 206 311 1 313 215 1 215 314 1
		 314 435 1 435 313 1 314 214 1 311 435 1 298 435 1 207 313 1 315 216 1 216 316 1 316 436 1
		 436 315 1 316 215 1 313 436 1 300 436 1 208 315 1 317 217 1 217 318 1 318 437 1 437 317 1
		 318 216 1 315 437 1 302 437 1 209 317 1 319 218 1 218 320 1 320 438 1 438 319 1 320 217 1
		 317 438 1 304 438 1 210 319 1 306 439 1 439 305 1 211 321 1 321 439 1 321 218 1 319 439 1
		 323 220 1 324 440 1 440 323 1 219 322 1 322 440 1 322 211 1 308 440 1 212 323 1 325 221 1
		 326 441 1 441 325 1 323 441 1 310 441 1 213 325 1 327 222 1 328 442 1;
	setAttr ".ed[664:829]" 442 327 1 325 442 1 312 442 1 214 327 1 329 223 1 330 443 1
		 443 329 1 327 443 1 314 443 1 215 329 1 331 224 1 332 444 1 444 331 1 329 444 1 316 444 1
		 216 331 1 333 225 1 334 445 1 445 333 1 331 445 1 318 445 1 217 333 1 335 226 1 336 446 1
		 446 335 1 333 446 1 320 446 1 218 335 1 322 447 1 447 321 1 337 447 1 335 447 1 227 338 1
		 338 448 1 448 340 1 338 193 1 193 252 1 252 448 1 252 195 1 195 339 1 339 448 1 339 228 1
		 341 229 1 342 449 1 449 341 1 339 449 1 195 253 1 253 449 1 253 197 1 197 341 1 255 199 1
		 199 343 1 343 450 1 450 255 1 343 230 1 344 450 1 341 450 1 197 255 1 256 201 1 201 345 1
		 345 451 1 451 256 1 345 231 1 346 451 1 343 451 1 199 256 1 347 232 1 348 452 1 452 347 1
		 345 452 1 201 257 1 257 452 1 257 189 1 189 347 1 349 233 1 350 453 1 453 349 1 347 453 1
		 189 246 1 246 453 1 246 188 1 188 349 1 351 234 1 352 454 1 454 351 1 349 454 1 188 248 1
		 248 454 1 248 191 1 191 351 1 353 455 1 455 338 1 351 455 1 191 250 1 250 455 1 250 193 1
		 355 236 1 356 456 1 456 355 1 235 354 1 354 456 1 354 181 1 244 456 1 180 355 1 357 237 1
		 358 457 1 457 357 1 355 457 1 263 457 1 179 357 1 359 238 1 360 458 1 458 359 1 357 458 1
		 277 458 1 178 359 1 361 239 1 362 459 1 459 361 1 359 459 1 276 459 1 185 361 1 363 240 1
		 364 460 1 460 363 1 361 460 1 265 460 1 184 363 1 365 241 1 366 461 1 461 365 1 363 461 1
		 258 461 1 183 365 1 367 242 1 368 462 1 462 367 1 365 462 1 259 462 1 182 367 1 369 463 1
		 463 354 1 367 463 1 261 463 1 370 187 1 187 371 1 371 464 1 464 370 1 371 190 1 190 247 1
		 247 464 1 247 191 1 248 464 1 188 370 1 373 198 1 198 254 1 254 465 1 465 373 1 254 199 1
		 255 465 1 197 372 1 372 465 1 372 196 1 196 373 1 190 374 1 374 466 1;
	setAttr ".ed[830:995]" 466 247 1 374 192 1 192 375 1 375 466 1 375 193 1 250 466 1
		 376 200 1 200 377 1 377 467 1 467 376 1 377 201 1 256 467 1 254 467 1 198 376 1 189 382 1
		 382 468 1 468 246 1 382 186 1 186 378 1 378 468 1 378 187 1 370 468 1 192 379 1 379 469 1
		 469 375 1 379 194 1 194 380 1 380 469 1 380 195 1 252 469 1 194 381 1 381 470 1 470 380 1
		 381 196 1 372 470 1 253 470 1 377 471 1 471 257 1 200 398 1 398 471 1 398 186 1 382 471 1
		 186 397 1 397 472 1 472 378 1 397 162 1 162 383 1 383 472 1 383 163 1 163 384 1 384 472 1
		 384 187 1 384 473 1 473 371 1 163 385 1 385 473 1 385 164 1 164 386 1 386 473 1 386 190 1
		 386 474 1 474 374 1 164 387 1 387 474 1 387 165 1 165 388 1 388 474 1 388 192 1 389 166 1
		 166 390 1 390 475 1 475 389 1 390 194 1 379 475 1 388 475 1 165 389 1 391 167 1 167 392 1
		 392 476 1 476 391 1 392 196 1 381 476 1 390 476 1 166 391 1 392 477 1 477 373 1 167 393 1
		 393 477 1 393 168 1 168 394 1 394 477 1 394 198 1 168 395 1 395 478 1 478 394 1 395 169 1
		 169 396 1 396 478 1 396 200 1 376 478 1 396 479 1 479 398 1 169 400 1 400 479 1 400 162 1
		 397 479 1 402 243 1 243 401 1 401 480 1 480 402 1 401 163 1 383 480 1 400 480 1 169 402 1
		 403 165 1 387 481 1 481 403 1 385 481 1 401 481 1 243 403 1 395 482 1 482 402 1 393 482 1
		 167 399 1 399 482 1 399 243 1 399 483 1 483 403 1 391 483 1 389 483 1 504 499 1 499 531 1
		 531 540 1 540 504 1 531 496 1 496 505 1 505 540 1 505 486 1 486 508 1 508 540 1 508 484 1
		 484 504 1 496 526 1 526 541 1 541 505 1 526 497 1 497 506 1 506 541 1 506 488 1 488 510 1
		 510 541 1 510 486 1 497 528 1 528 542 1 542 506 1 528 498 1 498 507 1 507 542 1 507 490 1
		 490 512 1 512 542 1 512 488 1 498 530 1 530 543 1 543 507 1 530 499 1;
	setAttr ".ed[996:1161]" 504 543 1 484 514 1 514 543 1 514 490 1 515 491 1 491 513 1
		 513 544 1 544 515 1 513 489 1 489 511 1 511 544 1 511 487 1 487 509 1 509 544 1 509 485 1
		 485 515 1 508 545 1 545 514 1 510 545 1 512 545 1 516 500 1 500 534 1 534 546 1 546 516 1
		 534 501 1 501 517 1 517 546 1 517 493 1 493 518 1 518 546 1 518 492 1 492 516 1 520 493 1
		 517 547 1 547 520 1 501 536 1 536 547 1 536 502 1 502 519 1 519 547 1 519 494 1 494 520 1
		 522 494 1 519 548 1 548 522 1 502 538 1 538 548 1 538 503 1 503 521 1 521 548 1 521 495 1
		 495 522 1 523 495 1 521 549 1 549 523 1 503 539 1 539 549 1 539 500 1 516 549 1 492 523 1
		 524 492 1 518 550 1 550 524 1 493 525 1 525 550 1 525 497 1 526 550 1 496 524 1 525 551 1
		 551 528 1 520 551 1 494 527 1 527 551 1 527 498 1 527 552 1 552 530 1 522 552 1 495 529 1
		 529 552 1 529 499 1 529 553 1 553 531 1 523 553 1 524 553 1 532 487 1 511 554 1 554 532 1
		 489 533 1 533 554 1 533 501 1 534 554 1 500 532 1 533 555 1 555 536 1 513 555 1 491 535 1
		 535 555 1 535 502 1 535 556 1 556 538 1 515 556 1 485 537 1 537 556 1 537 503 1 537 557 1
		 557 539 1 509 557 1 532 557 1 578 577 1 577 613 1 613 614 1 614 578 1 613 574 1 574 579 1
		 579 614 1 579 560 1 560 582 1 582 614 1 582 558 1 558 578 1 574 608 1 608 615 1 615 579 1
		 608 575 1 575 580 1 580 615 1 580 562 1 562 584 1 584 615 1 584 560 1 575 610 1 610 616 1
		 616 580 1 610 576 1 576 581 1 581 616 1 581 564 1 564 586 1 586 616 1 586 562 1 576 612 1
		 612 617 1 617 581 1 612 577 1 578 617 1 558 588 1 588 617 1 588 564 1 589 565 1 565 587 1
		 587 618 1 618 589 1 587 563 1 563 585 1 585 618 1 585 561 1 561 583 1 583 618 1 583 559 1
		 559 589 1 582 619 1 619 588 1 584 619 1 586 619 1 590 561 1 585 620 1;
	setAttr ".ed[1162:1327]" 620 590 1 563 591 1 591 620 1 591 567 1 567 592 1 592 620 1
		 592 566 1 566 590 1 594 567 1 591 621 1 621 594 1 587 621 1 565 593 1 593 621 1 593 568 1
		 568 594 1 596 568 1 593 622 1 622 596 1 589 622 1 559 595 1 595 622 1 595 569 1 569 596 1
		 597 569 1 595 623 1 623 597 1 583 623 1 590 623 1 566 597 1 598 566 1 592 624 1 624 598 1
		 567 599 1 599 624 1 599 571 1 571 600 1 600 624 1 600 570 1 570 598 1 602 571 1 599 625 1
		 625 602 1 594 625 1 568 601 1 601 625 1 601 572 1 572 602 1 604 572 1 601 626 1 626 604 1
		 596 626 1 569 603 1 603 626 1 603 573 1 573 604 1 605 573 1 603 627 1 627 605 1 597 627 1
		 598 627 1 570 605 1 606 570 1 600 628 1 628 606 1 571 607 1 607 628 1 607 575 1 608 628 1
		 574 606 1 607 629 1 629 610 1 602 629 1 572 609 1 609 629 1 609 576 1 609 630 1 630 612 1
		 604 630 1 573 611 1 611 630 1 611 577 1 611 631 1 631 613 1 605 631 1 606 631 1 816 669 1
		 669 818 1 818 1066 1 1066 816 1 818 670 1 670 820 1 820 1066 1 820 671 1 671 821 1
		 821 1066 1 821 668 1 668 816 1 668 823 1 823 1067 1 1067 816 1 823 672 1 672 825 1
		 825 1067 1 825 673 1 673 826 1 826 1067 1 826 669 1 783 632 1 632 790 1 790 1068 1
		 1068 783 1 790 655 1 655 791 1 791 1068 1 791 654 1 654 789 1 789 1068 1 789 633 1
		 633 783 1 787 636 1 636 788 1 788 1069 1 1069 787 1 788 653 1 653 792 1 792 1069 1
		 792 655 1 790 1069 1 632 787 1 786 635 1 635 793 1 793 1070 1 1070 786 1 793 656 1
		 656 794 1 794 1070 1 794 653 1 788 1070 1 636 786 1 785 634 1 634 795 1 795 1071 1
		 1071 785 1 795 657 1 657 796 1 796 1071 1 796 656 1 793 1071 1 635 785 1 655 799 1
		 799 1072 1 1072 791 1 799 660 1 660 800 1 800 1072 1 800 659 1 659 798 1 798 1072 1
		 798 654 1 653 797 1 797 1073 1 1073 792 1 797 658 1 658 801 1 801 1073 1;
	setAttr ".ed[1328:1493]" 801 660 1 799 1073 1 656 802 1 802 1074 1 1074 794 1
		 802 661 1 661 803 1 803 1074 1 803 658 1 797 1074 1 657 804 1 804 1075 1 1075 796 1
		 804 662 1 662 805 1 805 1075 1 805 661 1 802 1075 1 660 808 1 808 1076 1 1076 800 1
		 808 665 1 665 809 1 809 1076 1 809 664 1 664 807 1 807 1076 1 807 659 1 658 806 1
		 806 1077 1 1077 801 1 806 663 1 663 810 1 810 1077 1 810 665 1 808 1077 1 661 811 1
		 811 1078 1 1078 803 1 811 666 1 666 812 1 812 1078 1 812 663 1 806 1078 1 662 813 1
		 813 1079 1 1079 805 1 813 667 1 667 814 1 814 1079 1 814 666 1 811 1079 1 665 819 1
		 819 1080 1 1080 809 1 819 677 1 677 835 1 835 1080 1 835 678 1 678 817 1 817 1080 1
		 817 664 1 663 815 1 815 1081 1 1081 810 1 815 676 1 676 833 1 833 1081 1 833 677 1
		 819 1081 1 666 822 1 822 1082 1 1082 812 1 822 675 1 675 831 1 831 1082 1 831 676 1
		 815 1082 1 667 824 1 824 1083 1 1083 814 1 824 674 1 674 829 1 829 1083 1 829 675 1
		 822 1083 1 674 827 1 827 1084 1 1084 829 1 827 673 1 825 1084 1 672 828 1 828 1084 1
		 828 675 1 828 1085 1 1085 831 1 823 1085 1 668 830 1 830 1085 1 830 676 1 830 1086 1
		 1086 833 1 821 1086 1 671 832 1 832 1086 1 832 677 1 832 1087 1 1087 835 1 820 1087 1
		 670 834 1 834 1087 1 834 678 1 784 634 1 785 1088 1 1088 784 1 635 837 1 837 1088 1
		 837 652 1 652 838 1 838 1088 1 838 648 1 648 784 1 633 782 1 782 1089 1 1089 783 1
		 782 649 1 649 839 1 839 1089 1 839 650 1 650 842 1 842 1089 1 842 632 1 636 840 1
		 840 1090 1 1090 786 1 840 651 1 651 841 1 841 1090 1 841 652 1 837 1090 1 842 1091 1
		 1091 787 1 650 843 1 843 1091 1 843 651 1 840 1091 1 844 638 1 638 845 1 845 1092 1
		 1092 844 1 845 637 1 637 849 1 849 1092 1 849 682 1 682 848 1 848 1092 1 848 681 1
		 681 844 1 850 641 1 641 851 1 851 1093 1 1093 850 1 851 637 1 845 1093 1 638 846 1
		 846 1093 1;
	setAttr ".ed[1494:1659]" 846 642 1 642 850 1 852 641 1 850 1094 1 1094 852 1
		 642 778 1 778 1094 1 778 639 1 639 853 1 853 1094 1 853 640 1 640 852 1 857 647 1
		 647 861 1 861 1095 1 1095 857 1 861 643 1 643 781 1 781 1095 1 781 648 1 838 1095 1
		 652 857 1 1047 769 1 769 1049 1 1049 1096 1 1096 1047 1 1049 762 1 762 870 1 870 1096 1
		 870 688 1 688 859 1 859 1096 1 859 703 1 703 1047 1 860 680 1 680 779 1 779 1097 1
		 1097 860 1 779 643 1 861 1097 1 647 862 1 862 1097 1 862 679 1 679 860 1 639 836 1
		 836 1098 1 1098 853 1 836 680 1 860 1098 1 679 867 1 867 1098 1 867 640 1 647 863 1
		 863 1099 1 1099 862 1 863 696 1 696 864 1 864 1099 1 864 685 1 685 868 1 868 1099 1
		 868 679 1 865 685 1 864 1100 1 1100 865 1 696 872 1 872 1100 1 872 703 1 859 1100 1
		 688 865 1 869 685 1 865 1101 1 1101 869 1 688 866 1 866 1101 1 866 701 1 701 855 1
		 855 1101 1 855 699 1 699 869 1 868 1102 1 1102 867 1 869 1102 1 699 854 1 854 1102 1
		 854 640 1 1035 701 1 866 1103 1 1103 1035 1 870 1103 1 762 1036 1 1036 1103 1 1036 763 1
		 763 1035 1 780 644 1 644 874 1 874 1104 1 1104 780 1 874 645 1 645 875 1 875 1104 1
		 875 650 1 839 1104 1 649 780 1 876 651 1 843 1105 1 1105 876 1 875 1105 1 645 877 1
		 877 1105 1 877 646 1 646 876 1 847 681 1 848 1106 1 1106 847 1 682 878 1 878 1106 1
		 878 645 1 874 1106 1 644 847 1 880 696 1 863 1107 1 1107 880 1 647 882 1 882 1107 1
		 882 646 1 646 881 1 881 1107 1 881 684 1 684 880 1 876 1108 1 1108 841 1 882 1108 1
		 857 1108 1 883 697 1 697 885 1 885 1109 1 1109 883 1 885 684 1 881 1109 1 877 1109 1
		 645 883 1 1043 767 1 767 1046 1 1046 1110 1 1110 1043 1 1046 768 1 768 890 1 890 1110 1
		 890 687 1 687 884 1 884 1110 1 884 705 1 705 1043 1 887 692 1 692 858 1 858 1111 1
		 1111 887 1 858 702 1 702 891 1 891 1111 1 891 691 1 691 886 1 886 1111 1 886 693 1
		 693 887 1;
	setAttr ".ed[1660:1825]" 880 1112 1 1112 872 1 684 888 1 888 1112 1 888 687 1
		 687 889 1 889 1112 1 889 703 1 889 1113 1 1113 1047 1 890 1113 1 768 1048 1 1048 1113 1
		 1048 769 1 892 705 1 884 1114 1 1114 892 1 888 1114 1 885 1114 1 697 892 1 899 682 1
		 849 1115 1 1115 899 1 637 894 1 894 1115 1 894 698 1 698 906 1 906 1115 1 906 683 1
		 683 899 1 763 1038 1 1038 1116 1 1116 1035 1 1038 764 1 764 904 1 904 1116 1 904 689 1
		 689 895 1 895 1116 1 895 701 1 851 1117 1 1117 894 1 641 896 1 896 1117 1 896 686 1
		 686 897 1 897 1117 1 897 698 1 898 686 1 896 1118 1 1118 898 1 852 1118 1 854 1118 1
		 699 898 1 855 1119 1 1119 898 1 895 1119 1 689 907 1 907 1119 1 907 686 1 878 1120 1
		 1120 883 1 899 1120 1 683 900 1 900 1120 1 900 697 1 901 706 1 706 905 1 905 1121 1
		 1121 901 1 905 694 1 694 911 1 911 1121 1 911 693 1 693 902 1 902 1121 1 902 695 1
		 695 901 1 1039 765 1 765 1042 1 1042 1122 1 1122 1039 1 1042 766 1 766 913 1 913 1122 1
		 913 690 1 690 903 1 903 1122 1 903 707 1 707 1039 1 909 683 1 906 1123 1 1123 909 1
		 698 915 1 915 1123 1 915 707 1 903 1123 1 690 909 1 691 879 1 879 1124 1 1124 886 1
		 879 704 1 704 914 1 914 1124 1 914 695 1 902 1124 1 909 1125 1 1125 900 1 690 910 1
		 910 1125 1 910 705 1 892 1125 1 694 912 1 912 1126 1 1126 911 1 912 700 1 700 871 1
		 871 1126 1 871 692 1 887 1126 1 1044 767 1 1043 1127 1 1127 1044 1 910 1127 1 913 1127 1
		 766 1044 1 897 1128 1 1128 915 1 907 1128 1 689 908 1 908 1128 1 908 707 1 908 1129 1
		 1129 1039 1 904 1129 1 764 1040 1 1040 1129 1 1040 765 1 919 709 1 709 918 1 918 1130 1
		 1130 919 1 918 670 1 818 1130 1 669 917 1 917 1130 1 917 708 1 708 919 1 673 921 1
		 921 1131 1 1131 826 1 921 710 1 710 920 1 920 1131 1 920 708 1 917 1131 1 654 924 1
		 924 1132 1 1132 789 1 924 712 1 712 923 1 923 1132 1 923 711 1 711 922 1 922 1132 1
		 922 633 1;
	setAttr ".ed[1826:1991]" 712 927 1 927 1133 1 1133 923 1 927 714 1 714 926 1
		 926 1133 1 926 713 1 713 925 1 925 1133 1 925 711 1 714 930 1 930 1134 1 1134 926 1
		 930 716 1 716 929 1 929 1134 1 929 715 1 715 928 1 928 1134 1 928 713 1 716 932 1
		 932 1135 1 1135 929 1 932 657 1 795 1135 1 634 931 1 931 1135 1 931 715 1 659 934 1
		 934 1136 1 1136 798 1 934 717 1 717 933 1 933 1136 1 933 712 1 924 1136 1 717 936 1
		 936 1137 1 1137 933 1 936 718 1 718 935 1 935 1137 1 935 714 1 927 1137 1 718 938 1
		 938 1138 1 1138 935 1 938 719 1 719 937 1 937 1138 1 937 716 1 930 1138 1 719 939 1
		 939 1139 1 1139 937 1 939 662 1 804 1139 1 932 1139 1 664 941 1 941 1140 1 1140 807 1
		 941 720 1 720 940 1 940 1140 1 940 717 1 934 1140 1 720 943 1 943 1141 1 1141 940 1
		 943 721 1 721 942 1 942 1141 1 942 718 1 936 1141 1 721 945 1 945 1142 1 1142 942 1
		 945 722 1 722 944 1 944 1142 1 944 719 1 938 1142 1 722 946 1 946 1143 1 1143 944 1
		 946 667 1 813 1143 1 939 1143 1 678 948 1 948 1144 1 1144 817 1 948 723 1 723 947 1
		 947 1144 1 947 720 1 941 1144 1 723 950 1 950 1145 1 1145 947 1 950 724 1 724 949 1
		 949 1145 1 949 721 1 943 1145 1 724 952 1 952 1146 1 1146 949 1 952 725 1 725 951 1
		 951 1146 1 951 722 1 945 1146 1 725 953 1 953 1147 1 1147 951 1 953 674 1 824 1147 1
		 946 1147 1 954 710 1 921 1148 1 1148 954 1 827 1148 1 953 1148 1 725 954 1 955 708 1
		 920 1149 1 1149 955 1 954 1149 1 952 1149 1 724 955 1 956 709 1 919 1150 1 1150 956 1
		 955 1150 1 950 1150 1 723 956 1 918 1151 1 1151 834 1 956 1151 1 948 1151 1 958 726 1
		 726 957 1 957 1152 1 1152 958 1 957 715 1 931 1152 1 784 1152 1 648 958 1 960 727 1
		 727 959 1 959 1153 1 1153 960 1 959 649 1 782 1153 1 922 1153 1 711 960 1 726 962 1
		 962 1154 1 1154 957 1 962 728 1 728 961 1 961 1154 1 961 713 1 928 1154 1 728 963 1
		 963 1155 1;
	setAttr ".ed[1992:2157]" 1155 961 1 963 727 1 960 1155 1 925 1155 1 966 730 1
		 730 965 1 965 1156 1 1156 966 1 965 729 1 729 964 1 964 1156 1 964 638 1 844 1156 1
		 681 966 1 964 1157 1 1157 846 1 729 968 1 968 1157 1 968 731 1 731 967 1 967 1157 1
		 967 642 1 970 639 1 778 1158 1 1158 970 1 967 1158 1 731 969 1 969 1158 1 969 732 1
		 732 970 1 781 1159 1 1159 958 1 643 972 1 972 1159 1 972 733 1 733 971 1 971 1159 1
		 971 726 1 976 737 1 737 975 1 975 1160 1 1160 976 1 975 770 1 770 1052 1 1052 1160 1
		 1052 771 1 771 1051 1 1051 1160 1 1051 734 1 734 976 1 978 733 1 972 1161 1 1161 978 1
		 779 1161 1 680 977 1 977 1161 1 977 738 1 738 978 1 979 738 1 977 1162 1 1162 979 1
		 836 1162 1 970 1162 1 732 979 1 982 740 1 740 981 1 981 1163 1 1163 982 1 981 739 1
		 739 980 1 980 1163 1 980 733 1 978 1163 1 738 982 1 734 984 1 984 1164 1 1164 976 1
		 984 739 1 981 1164 1 740 983 1 983 1164 1 983 737 1 987 742 1 742 986 1 986 1165 1
		 1165 987 1 986 737 1 983 1165 1 740 985 1 985 1165 1 985 741 1 741 987 1 988 741 1
		 985 1166 1 1166 988 1 982 1166 1 979 1166 1 732 988 1 1065 770 1 975 1167 1 1167 1065 1
		 986 1167 1 742 1063 1 1063 1167 1 1063 777 1 777 1065 1 727 992 1 992 1168 1 1168 959 1
		 992 744 1 744 991 1 991 1168 1 991 644 1 780 1168 1 994 744 1 992 1169 1 1169 994 1
		 963 1169 1 728 993 1 993 1169 1 993 745 1 745 994 1 744 995 1 995 1170 1 1170 991 1
		 995 730 1 966 1170 1 847 1170 1 998 745 1 745 997 1 997 1171 1 1171 998 1 997 733 1
		 980 1171 1 739 996 1 996 1171 1 996 746 1 746 998 1 997 1172 1 1172 971 1 993 1172 1
		 962 1172 1 998 1173 1 1173 994 1 746 1000 1 1000 1173 1 1000 747 1 747 999 1 999 1173 1
		 999 744 1 1004 751 1 751 1003 1 1003 1174 1 1174 1004 1 1003 772 1 772 1056 1 1056 1174 1
		 1056 773 1 773 1055 1 1055 1174 1 1055 748 1 748 1004 1 1007 750 1 750 1006 1 1006 1175 1
		 1175 1007 1;
	setAttr ".ed[2158:2323]" 1006 735 1 735 974 1 974 1175 1 974 736 1 736 1005 1
		 1005 1175 1 1005 752 1 752 1007 1 1009 751 1 751 1008 1 1008 1176 1 1176 1009 1 1008 746 1
		 996 1176 1 984 1176 1 734 1009 1 1054 772 1 1003 1177 1 1177 1054 1 1009 1177 1 1051 1177 1
		 771 1054 1 1008 1178 1 1178 1000 1 1004 1178 1 748 1010 1 1010 1178 1 1010 747 1
		 1013 754 1 754 1012 1 1012 1179 1 1179 1013 1 1012 729 1 965 1179 1 730 1011 1 1011 1179 1
		 1011 753 1 753 1013 1 1016 756 1 756 1015 1 1015 1180 1 1180 1016 1 1015 776 1 776 1064 1
		 1064 1180 1 1064 777 1 1063 1180 1 742 1016 1 1018 757 1 757 1017 1 1017 1181 1 1181 1018 1
		 1017 731 1 968 1181 1 1012 1181 1 754 1018 1 969 1182 1 1182 988 1 1017 1182 1 757 1019 1
		 1019 1182 1 1019 741 1 1020 756 1 1016 1183 1 1183 1020 1 987 1183 1 1019 1183 1
		 757 1020 1 1021 753 1 1011 1184 1 1184 1021 1 995 1184 1 999 1184 1 747 1021 1 1025 752 1
		 752 1024 1 1024 1185 1 1185 1025 1 1024 755 1 755 1023 1 1023 1185 1 1023 759 1 759 1022 1
		 1022 1185 1 1022 758 1 758 1025 1 1028 761 1 761 1027 1 1027 1186 1 1186 1028 1 1027 774 1
		 774 1060 1 1060 1186 1 1060 775 1 775 1059 1 1059 1186 1 1059 760 1 760 1028 1 760 1030 1
		 1030 1187 1 1187 1028 1 1030 754 1 1013 1187 1 753 1029 1 1029 1187 1 1029 761 1
		 758 1031 1 1031 1188 1 1188 1025 1 1031 749 1 749 1002 1 1002 1188 1 1002 750 1 1007 1188 1
		 748 1032 1 1032 1189 1 1189 1010 1 1032 761 1 1029 1189 1 1021 1189 1 736 990 1 990 1190 1
		 1190 1005 1 990 743 1 743 1014 1 1014 1190 1 1014 755 1 1024 1190 1 1032 1191 1 1191 1027 1
		 1055 1191 1 773 1058 1 1058 1191 1 1058 774 1 1033 756 1 1020 1192 1 1192 1033 1
		 1018 1192 1 1030 1192 1 760 1033 1 1062 776 1 1015 1193 1 1193 1062 1 1033 1193 1
		 1059 1193 1 775 1062 1 856 763 1 1036 1194 1 1194 856 1 762 1034 1 1034 1194 1 1034 692 1
		 871 1194 1 700 856 1 856 1195 1 1195 1038 1 912 1195 1 694 1037 1 1037 1195 1 1037 764 1
		 916 765 1 1040 1196 1 1196 916 1 1037 1196 1 905 1196 1 706 916 1;
	setAttr ".ed[2324:2489]" 916 1197 1 1197 1042 1 901 1197 1 695 1041 1 1041 1197 1
		 1041 766 1 704 893 1 893 1198 1 1198 914 1 893 767 1 1044 1198 1 1041 1198 1 893 1199 1
		 1199 1046 1 879 1199 1 691 1045 1 1045 1199 1 1045 768 1 873 769 1 1048 1200 1 1200 873 1
		 1045 1200 1 891 1200 1 702 873 1 873 1201 1 1201 1049 1 858 1201 1 1034 1201 1 770 1050 1
		 1050 1202 1 1202 1052 1 1050 736 1 974 1202 1 735 973 1 973 1202 1 973 771 1 750 1053 1
		 1053 1203 1 1203 1006 1 1053 772 1 1054 1203 1 973 1203 1 1053 1204 1 1204 1056 1
		 1002 1204 1 749 1001 1 1001 1204 1 1001 773 1 1057 774 1 1058 1205 1 1205 1057 1
		 1001 1205 1 1031 1205 1 758 1057 1 1057 1206 1 1206 1060 1 1022 1206 1 759 1026 1
		 1026 1206 1 1026 775 1 755 1061 1 1061 1207 1 1207 1023 1 1061 776 1 1062 1207 1
		 1026 1207 1 1061 1208 1 1208 1064 1 1014 1208 1 743 989 1 989 1208 1 989 777 1 1050 1209 1
		 1209 990 1 1065 1209 1 989 1209 1 1226 1210 1 1210 1242 1 1242 1258 1 1258 1226 1
		 1242 1214 1 1214 1230 1 1230 1258 1 1230 1215 1 1215 1243 1 1243 1258 1 1243 1211 1
		 1211 1226 1 1227 1211 1 1243 1259 1 1259 1227 1 1215 1231 1 1231 1259 1 1231 1216 1
		 1216 1244 1 1244 1259 1 1244 1212 1 1212 1227 1 1228 1212 1 1244 1260 1 1260 1228 1
		 1216 1232 1 1232 1260 1 1232 1217 1 1217 1245 1 1245 1260 1 1245 1213 1 1213 1228 1
		 1229 1213 1 1245 1261 1 1261 1229 1 1217 1233 1 1233 1261 1 1233 1214 1 1242 1261 1
		 1210 1229 1 1214 1246 1 1246 1262 1 1262 1230 1 1246 1218 1 1218 1234 1 1234 1262 1
		 1234 1219 1 1219 1247 1 1247 1262 1 1247 1215 1 1247 1263 1 1263 1231 1 1219 1235 1
		 1235 1263 1 1235 1220 1 1220 1248 1 1248 1263 1 1248 1216 1 1248 1264 1 1264 1232 1
		 1220 1236 1 1236 1264 1 1236 1221 1 1221 1249 1 1249 1264 1 1249 1217 1 1249 1265 1
		 1265 1233 1 1221 1237 1 1237 1265 1 1237 1218 1 1246 1265 1 1218 1250 1 1250 1266 1
		 1266 1234 1 1250 1222 1 1222 1238 1 1238 1266 1 1238 1223 1 1223 1251 1 1251 1266 1
		 1251 1219 1 1251 1267 1 1267 1235 1 1223 1239 1 1239 1267 1 1239 1224 1 1224 1252 1
		 1252 1267 1 1252 1220 1;
	setAttr ".ed[2490:2655]" 1252 1268 1 1268 1236 1 1224 1240 1 1240 1268 1 1240 1225 1
		 1225 1253 1 1253 1268 1 1253 1221 1 1253 1269 1 1269 1237 1 1225 1241 1 1241 1269 1
		 1241 1222 1 1250 1269 1 1222 1254 1 1254 1270 1 1270 1238 1 1254 1210 1 1226 1270 1
		 1211 1255 1 1255 1270 1 1255 1223 1 1255 1271 1 1271 1239 1 1227 1271 1 1212 1256 1
		 1256 1271 1 1256 1224 1 1256 1272 1 1272 1240 1 1228 1272 1 1213 1257 1 1257 1272 1
		 1257 1225 1 1257 1273 1 1273 1241 1 1229 1273 1 1254 1273 1 1395 1298 1 1298 1397 1
		 1397 1552 1 1552 1395 1 1397 1300 1 1300 1399 1 1399 1552 1 1399 1301 1 1301 1400 1
		 1400 1552 1 1400 1299 1 1299 1395 1 1402 1299 1 1400 1553 1 1553 1402 1 1301 1404 1
		 1404 1553 1 1404 1303 1 1303 1405 1 1405 1553 1 1405 1302 1 1302 1402 1 1302 1407 1
		 1407 1554 1 1554 1402 1 1407 1304 1 1304 1409 1 1409 1554 1 1409 1305 1 1305 1410 1
		 1410 1554 1 1410 1299 1 1410 1555 1 1555 1395 1 1305 1412 1 1412 1555 1 1412 1306 1
		 1306 1413 1 1413 1555 1 1413 1298 1 1372 1284 1 1284 1379 1 1379 1556 1 1556 1372 1
		 1379 1330 1 1330 1456 1 1456 1556 1 1456 1323 1 1323 1378 1 1378 1556 1 1378 1283 1
		 1283 1372 1 1373 1285 1 1285 1381 1 1381 1557 1 1557 1373 1 1381 1329 1 1329 1455 1
		 1455 1557 1 1455 1330 1 1379 1557 1 1284 1373 1 1374 1286 1 1286 1384 1 1384 1558 1
		 1558 1374 1 1384 1328 1 1328 1453 1 1453 1558 1 1453 1329 1 1381 1558 1 1285 1374 1
		 1375 1287 1 1287 1383 1 1383 1559 1 1559 1375 1 1383 1327 1 1327 1451 1 1451 1559 1
		 1451 1328 1 1384 1559 1 1286 1375 1 1376 1288 1 1288 1387 1 1387 1560 1 1560 1376 1
		 1387 1326 1 1326 1449 1 1449 1560 1 1449 1327 1 1383 1560 1 1287 1376 1 1377 1289 1
		 1289 1389 1 1389 1561 1 1561 1377 1 1389 1325 1 1325 1447 1 1447 1561 1 1447 1326 1
		 1387 1561 1 1288 1377 1 1370 1282 1 1282 1391 1 1391 1562 1 1562 1370 1 1391 1324 1
		 1324 1445 1 1445 1562 1 1445 1325 1 1389 1562 1 1289 1370 1 1371 1283 1 1378 1563 1
		 1563 1371 1 1323 1443 1 1443 1563 1 1443 1324 1 1391 1563 1 1282 1371 1 1380 1291 1
		 1291 1396 1 1396 1564 1 1564 1380 1 1396 1322 1 1322 1440 1 1440 1564 1 1440 1315 1;
	setAttr ".ed[2656:2821]" 1315 1394 1 1394 1564 1 1394 1290 1 1290 1380 1 1382 1292 1
		 1292 1398 1 1398 1565 1 1565 1382 1 1398 1321 1 1321 1439 1 1439 1565 1 1439 1322 1
		 1396 1565 1 1291 1382 1 1385 1294 1 1294 1403 1 1403 1566 1 1566 1385 1 1403 1320 1
		 1320 1437 1 1437 1566 1 1437 1321 1 1398 1566 1 1292 1385 1 1386 1293 1 1293 1401 1
		 1401 1567 1 1567 1386 1 1401 1319 1 1319 1435 1 1435 1567 1 1435 1320 1 1403 1567 1
		 1294 1386 1 1388 1295 1 1295 1406 1 1406 1568 1 1568 1388 1 1406 1318 1 1318 1433 1
		 1433 1568 1 1433 1319 1 1401 1568 1 1293 1388 1 1390 1296 1 1296 1408 1 1408 1569 1
		 1569 1390 1 1408 1317 1 1317 1431 1 1431 1569 1 1431 1318 1 1406 1569 1 1295 1390 1
		 1392 1297 1 1297 1411 1 1411 1570 1 1570 1392 1 1411 1316 1 1316 1429 1 1429 1570 1
		 1429 1317 1 1408 1570 1 1296 1392 1 1393 1290 1 1394 1571 1 1571 1393 1 1315 1427 1
		 1427 1571 1 1427 1316 1 1411 1571 1 1297 1393 1 1286 1416 1 1416 1572 1 1572 1375 1
		 1416 1311 1 1311 1417 1 1417 1572 1 1417 1310 1 1310 1415 1 1415 1572 1 1415 1287 1
		 1285 1418 1 1418 1573 1 1573 1374 1 1418 1312 1 1312 1419 1 1419 1573 1 1419 1311 1
		 1416 1573 1 1284 1420 1 1420 1574 1 1574 1373 1 1420 1313 1 1313 1421 1 1421 1574 1
		 1421 1312 1 1418 1574 1 1422 1314 1 1314 1423 1 1423 1575 1 1575 1422 1 1423 1313 1
		 1420 1575 1 1372 1575 1 1283 1422 1 1282 1414 1 1414 1576 1 1576 1371 1 1414 1307 1
		 1307 1424 1 1424 1576 1 1424 1314 1 1422 1576 1 1315 1425 1 1425 1577 1 1577 1427 1
		 1425 1298 1 1413 1577 1 1306 1426 1 1426 1577 1 1426 1316 1 1426 1578 1 1578 1429 1
		 1412 1578 1 1305 1428 1 1428 1578 1 1428 1317 1 1428 1579 1 1579 1431 1 1409 1579 1
		 1304 1430 1 1430 1579 1 1430 1318 1 1430 1580 1 1580 1433 1 1407 1580 1 1302 1432 1
		 1432 1580 1 1432 1319 1 1432 1581 1 1581 1435 1 1405 1581 1 1303 1434 1 1434 1581 1
		 1434 1320 1 1434 1582 1 1582 1437 1 1404 1582 1 1301 1436 1 1436 1582 1 1436 1321 1
		 1436 1583 1 1583 1439 1 1399 1583 1 1300 1438 1 1438 1583 1 1438 1322 1 1438 1584 1
		 1584 1440 1 1397 1584 1 1425 1584 1 1323 1441 1 1441 1585 1 1585 1443 1 1441 1290 1;
	setAttr ".ed[2822:2987]" 1393 1585 1 1297 1442 1 1442 1585 1 1442 1324 1 1442 1586 1
		 1586 1445 1 1392 1586 1 1296 1444 1 1444 1586 1 1444 1325 1 1444 1587 1 1587 1447 1
		 1390 1587 1 1295 1446 1 1446 1587 1 1446 1326 1 1446 1588 1 1588 1449 1 1388 1588 1
		 1293 1448 1 1448 1588 1 1448 1327 1 1448 1589 1 1589 1451 1 1386 1589 1 1294 1450 1
		 1450 1589 1 1450 1328 1 1450 1590 1 1590 1453 1 1385 1590 1 1292 1452 1 1452 1590 1
		 1452 1329 1 1452 1591 1 1591 1455 1 1382 1591 1 1291 1454 1 1454 1591 1 1454 1330 1
		 1454 1592 1 1592 1456 1 1380 1592 1 1441 1592 1 1458 1312 1 1421 1593 1 1593 1458 1
		 1313 1457 1 1457 1593 1 1457 1276 1 1276 1368 1 1368 1593 1 1368 1277 1 1277 1458 1
		 1459 1311 1 1419 1594 1 1594 1459 1 1458 1594 1 1277 1369 1 1369 1594 1 1369 1278 1
		 1278 1459 1 1461 1310 1 1417 1595 1 1595 1461 1 1459 1595 1 1278 1460 1 1460 1595 1
		 1460 1279 1 1279 1461 1 1276 1463 1 1463 1596 1 1596 1368 1 1463 1359 1 1359 1536 1
		 1536 1596 1 1536 1360 1 1360 1462 1 1462 1596 1 1462 1277 1 1278 1466 1 1466 1597 1
		 1597 1460 1 1466 1361 1 1361 1539 1 1539 1597 1 1539 1356 1 1356 1465 1 1465 1597 1
		 1465 1279 1 1462 1598 1 1598 1369 1 1360 1538 1 1538 1598 1 1538 1361 1 1466 1598 1
		 1471 1308 1 1308 1469 1 1469 1599 1 1599 1471 1 1469 1309 1 1309 1479 1 1479 1599 1
		 1479 1280 1 1280 1470 1 1470 1599 1 1470 1281 1 1281 1471 1 1473 1307 1 1307 1472 1
		 1472 1600 1 1600 1473 1 1472 1308 1 1471 1600 1 1281 1489 1 1489 1600 1 1489 1274 1
		 1274 1473 1 1370 1601 1 1601 1414 1 1289 1474 1 1474 1601 1 1474 1308 1 1472 1601 1
		 1377 1602 1 1602 1474 1 1288 1475 1 1475 1602 1 1475 1309 1 1469 1602 1 1415 1603 1
		 1603 1376 1 1310 1477 1 1477 1603 1 1477 1309 1 1475 1603 1 1423 1604 1 1604 1457 1
		 1314 1480 1 1480 1604 1 1480 1275 1 1275 1476 1 1476 1604 1 1476 1276 1 1477 1605 1
		 1605 1479 1 1461 1605 1 1279 1478 1 1478 1605 1 1478 1280 1 1481 1275 1 1480 1606 1
		 1606 1481 1 1424 1606 1 1473 1606 1 1274 1481 1 1483 1332 1 1332 1464 1 1464 1607 1
		 1607 1483 1 1464 1334 1 1334 1482 1 1482 1607 1 1482 1335 1 1335 1488 1 1488 1607 1;
	setAttr ".ed[2988:3153]" 1488 1333 1 1333 1483 1 1468 1332 1 1483 1608 1 1608 1468 1
		 1333 1484 1 1484 1608 1 1484 1336 1 1336 1467 1 1467 1608 1 1467 1337 1 1337 1468 1
		 1476 1609 1 1609 1463 1 1275 1485 1 1485 1609 1 1485 1358 1 1358 1534 1 1534 1609 1
		 1534 1359 1 1275 1486 1 1486 1610 1 1610 1485 1 1486 1331 1 1331 1487 1 1487 1610 1
		 1487 1357 1 1357 1532 1 1532 1610 1 1532 1358 1 1331 1490 1 1490 1611 1 1611 1487 1
		 1490 1279 1 1465 1611 1 1356 1530 1 1530 1611 1 1530 1357 1 1517 1350 1 1350 1519 1
		 1519 1612 1 1612 1517 1 1519 1352 1 1352 1521 1 1521 1612 1 1521 1353 1 1353 1522 1
		 1522 1612 1 1522 1351 1 1351 1517 1 1525 1354 1 1354 1526 1 1526 1613 1 1613 1525 1
		 1526 1351 1 1522 1613 1 1353 1527 1 1527 1613 1 1527 1355 1 1355 1525 1 1275 1491 1
		 1491 1614 1 1614 1486 1 1491 1338 1 1338 1493 1 1493 1614 1 1493 1339 1 1339 1492 1
		 1492 1614 1 1492 1331 1 1274 1494 1 1494 1615 1 1615 1481 1 1494 1340 1 1340 1495 1
		 1495 1615 1 1495 1338 1 1491 1615 1 1281 1496 1 1496 1616 1 1616 1489 1 1496 1341 1
		 1341 1497 1 1497 1616 1 1497 1340 1 1494 1616 1 1279 1498 1 1498 1617 1 1617 1478 1
		 1498 1342 1 1342 1500 1 1500 1617 1 1500 1343 1 1343 1499 1 1499 1617 1 1499 1280 1
		 1492 1618 1 1618 1490 1 1339 1501 1 1501 1618 1 1501 1342 1 1498 1618 1 1499 1619 1
		 1619 1470 1 1343 1502 1 1502 1619 1 1502 1341 1 1496 1619 1 1338 1503 1 1503 1620 1
		 1620 1493 1 1503 1344 1 1344 1505 1 1505 1620 1 1505 1345 1 1345 1504 1 1504 1620 1
		 1504 1339 1 1340 1506 1 1506 1621 1 1621 1495 1 1506 1346 1 1346 1507 1 1507 1621 1
		 1507 1344 1 1503 1621 1 1341 1508 1 1508 1622 1 1622 1497 1 1508 1347 1 1347 1509 1
		 1509 1622 1 1509 1346 1 1506 1622 1 1342 1510 1 1510 1623 1 1623 1500 1 1510 1348 1
		 1348 1512 1 1512 1623 1 1512 1349 1 1349 1511 1 1511 1623 1 1511 1343 1 1504 1624 1
		 1624 1501 1 1345 1513 1 1513 1624 1 1513 1348 1 1510 1624 1 1511 1625 1 1625 1502 1
		 1349 1514 1 1514 1625 1 1514 1347 1 1508 1625 1 1344 1515 1 1515 1626 1 1626 1505 1
		 1515 1350 1 1517 1626 1 1351 1516 1 1516 1626 1 1516 1345 1 1346 1518 1 1518 1627 1;
	setAttr ".ed[3154:3319]" 1627 1507 1 1518 1352 1 1519 1627 1 1515 1627 1 1347 1520 1
		 1520 1628 1 1628 1509 1 1520 1353 1 1521 1628 1 1518 1628 1 1348 1523 1 1523 1629 1
		 1629 1512 1 1523 1354 1 1525 1629 1 1355 1524 1 1524 1629 1 1524 1349 1 1516 1630 1
		 1630 1513 1 1526 1630 1 1523 1630 1 1524 1631 1 1631 1514 1 1527 1631 1 1520 1631 1
		 1529 1357 1 1530 1632 1 1632 1529 1 1356 1528 1 1528 1632 1 1528 1362 1 1362 1542 1
		 1542 1632 1 1542 1363 1 1363 1529 1 1531 1358 1 1532 1633 1 1633 1531 1 1529 1633 1
		 1363 1544 1 1544 1633 1 1544 1364 1 1364 1531 1 1533 1359 1 1534 1634 1 1634 1533 1
		 1531 1634 1 1364 1546 1 1546 1634 1 1546 1365 1 1365 1533 1 1533 1635 1 1635 1536 1
		 1365 1548 1 1548 1635 1 1548 1366 1 1366 1535 1 1535 1635 1 1535 1360 1 1535 1636 1
		 1636 1538 1 1366 1550 1 1550 1636 1 1550 1367 1 1367 1537 1 1537 1636 1 1537 1361 1
		 1537 1637 1 1637 1539 1 1367 1551 1 1551 1637 1 1551 1362 1 1528 1637 1 1541 1363 1
		 1542 1638 1 1638 1541 1 1362 1540 1 1540 1638 1 1540 1336 1 1484 1638 1 1333 1541 1
		 1543 1364 1 1544 1639 1 1639 1543 1 1541 1639 1 1488 1639 1 1335 1543 1 1545 1365 1
		 1546 1640 1 1640 1545 1 1543 1640 1 1482 1640 1 1334 1545 1 1545 1641 1 1641 1548 1
		 1464 1641 1 1332 1547 1 1547 1641 1 1547 1366 1 1547 1642 1 1642 1550 1 1468 1642 1
		 1337 1549 1 1549 1642 1 1549 1367 1 1549 1643 1 1643 1551 1 1467 1643 1 1540 1643 1
		 1765 1922 1 1922 1767 1 1767 1668 1 1668 1765 1 1922 1769 1 1769 1670 1 1670 1767 1
		 1922 1770 1 1770 1671 1 1671 1769 1 1765 1669 1 1669 1770 1 1772 1923 1 1923 1770 1
		 1669 1772 1 1923 1774 1 1774 1671 1 1923 1775 1 1775 1673 1 1673 1774 1 1772 1672 1
		 1672 1775 1 1772 1924 1 1924 1777 1 1777 1672 1 1924 1779 1 1779 1674 1 1674 1777 1
		 1924 1780 1 1780 1675 1 1675 1779 1 1669 1780 1 1765 1925 1 1925 1780 1 1925 1782 1
		 1782 1675 1 1925 1783 1 1783 1676 1 1676 1782 1 1668 1783 1 1742 1926 1 1926 1749 1
		 1749 1654 1 1654 1742 1 1926 1826 1 1826 1700 1 1700 1749 1 1926 1748 1 1748 1693 1
		 1693 1826 1 1742 1653 1 1653 1748 1 1743 1927 1 1927 1751 1 1751 1655 1 1655 1743 1;
	setAttr ".ed[3320:3485]" 1927 1825 1 1825 1699 1 1699 1751 1 1927 1749 1 1700 1825 1
		 1743 1654 1 1744 1928 1 1928 1754 1 1754 1656 1 1656 1744 1 1928 1823 1 1823 1698 1
		 1698 1754 1 1928 1751 1 1699 1823 1 1744 1655 1 1745 1929 1 1929 1753 1 1753 1657 1
		 1657 1745 1 1929 1821 1 1821 1697 1 1697 1753 1 1929 1754 1 1698 1821 1 1745 1656 1
		 1746 1930 1 1930 1757 1 1757 1658 1 1658 1746 1 1930 1819 1 1819 1696 1 1696 1757 1
		 1930 1753 1 1697 1819 1 1746 1657 1 1747 1931 1 1931 1759 1 1759 1659 1 1659 1747 1
		 1931 1817 1 1817 1695 1 1695 1759 1 1931 1757 1 1696 1817 1 1747 1658 1 1740 1932 1
		 1932 1761 1 1761 1652 1 1652 1740 1 1932 1815 1 1815 1694 1 1694 1761 1 1932 1759 1
		 1695 1815 1 1740 1659 1 1741 1933 1 1933 1748 1 1653 1741 1 1933 1813 1 1813 1693 1
		 1933 1761 1 1694 1813 1 1741 1652 1 1750 1934 1 1934 1766 1 1766 1661 1 1661 1750 1
		 1934 1810 1 1810 1692 1 1692 1766 1 1934 1764 1 1764 1685 1 1685 1810 1 1750 1660 1
		 1660 1764 1 1752 1935 1 1935 1768 1 1768 1662 1 1662 1752 1 1935 1809 1 1809 1691 1
		 1691 1768 1 1935 1766 1 1692 1809 1 1752 1661 1 1755 1936 1 1936 1773 1 1773 1664 1
		 1664 1755 1 1936 1807 1 1807 1690 1 1690 1773 1 1936 1768 1 1691 1807 1 1755 1662 1
		 1756 1937 1 1937 1771 1 1771 1663 1 1663 1756 1 1937 1805 1 1805 1689 1 1689 1771 1
		 1937 1773 1 1690 1805 1 1756 1664 1 1758 1938 1 1938 1776 1 1776 1665 1 1665 1758 1
		 1938 1803 1 1803 1688 1 1688 1776 1 1938 1771 1 1689 1803 1 1758 1663 1 1760 1939 1
		 1939 1778 1 1778 1666 1 1666 1760 1 1939 1801 1 1801 1687 1 1687 1778 1 1939 1776 1
		 1688 1801 1 1760 1665 1 1762 1940 1 1940 1781 1 1781 1667 1 1667 1762 1 1940 1799 1
		 1799 1686 1 1686 1781 1 1940 1778 1 1687 1799 1 1762 1666 1 1763 1941 1 1941 1764 1
		 1660 1763 1 1941 1797 1 1797 1685 1 1941 1781 1 1686 1797 1 1763 1667 1 1745 1942 1
		 1942 1786 1 1786 1656 1 1942 1787 1 1787 1681 1 1681 1786 1 1942 1785 1 1785 1680 1
		 1680 1787 1 1657 1785 1 1744 1943 1 1943 1788 1 1788 1655 1 1943 1789 1 1789 1682 1
		 1682 1788 1 1943 1786 1 1681 1789 1 1743 1944 1 1944 1790 1 1790 1654 1 1944 1791 1;
	setAttr ".ed[3486:3651]" 1791 1683 1 1683 1790 1 1944 1788 1 1682 1791 1 1792 1945 1
		 1945 1793 1 1793 1684 1 1684 1792 1 1945 1790 1 1683 1793 1 1945 1742 1 1792 1653 1
		 1741 1946 1 1946 1784 1 1784 1652 1 1946 1794 1 1794 1677 1 1677 1784 1 1946 1792 1
		 1684 1794 1 1797 1947 1 1947 1795 1 1795 1685 1 1947 1783 1 1668 1795 1 1947 1796 1
		 1796 1676 1 1686 1796 1 1799 1948 1 1948 1796 1 1948 1782 1 1948 1798 1 1798 1675 1
		 1687 1798 1 1801 1949 1 1949 1798 1 1949 1779 1 1949 1800 1 1800 1674 1 1688 1800 1
		 1803 1950 1 1950 1800 1 1950 1777 1 1950 1802 1 1802 1672 1 1689 1802 1 1805 1951 1
		 1951 1802 1 1951 1775 1 1951 1804 1 1804 1673 1 1690 1804 1 1807 1952 1 1952 1804 1
		 1952 1774 1 1952 1806 1 1806 1671 1 1691 1806 1 1809 1953 1 1953 1806 1 1953 1769 1
		 1953 1808 1 1808 1670 1 1692 1808 1 1810 1954 1 1954 1808 1 1954 1767 1 1954 1795 1
		 1813 1955 1 1955 1811 1 1811 1693 1 1955 1763 1 1660 1811 1 1955 1812 1 1812 1667 1
		 1694 1812 1 1815 1956 1 1956 1812 1 1956 1762 1 1956 1814 1 1814 1666 1 1695 1814 1
		 1817 1957 1 1957 1814 1 1957 1760 1 1957 1816 1 1816 1665 1 1696 1816 1 1819 1958 1
		 1958 1816 1 1958 1758 1 1958 1818 1 1818 1663 1 1697 1818 1 1821 1959 1 1959 1818 1
		 1959 1756 1 1959 1820 1 1820 1664 1 1698 1820 1 1823 1960 1 1960 1820 1 1960 1755 1
		 1960 1822 1 1822 1662 1 1699 1822 1 1825 1961 1 1961 1822 1 1961 1752 1 1961 1824 1
		 1824 1661 1 1700 1824 1 1826 1962 1 1962 1824 1 1962 1750 1 1962 1811 1 1828 1963 1
		 1963 1791 1 1682 1828 1 1963 1827 1 1827 1683 1 1963 1738 1 1738 1646 1 1646 1827 1
		 1828 1647 1 1647 1738 1 1829 1964 1 1964 1789 1 1681 1829 1 1964 1828 1 1964 1739 1
		 1739 1647 1 1829 1648 1 1648 1739 1 1831 1965 1 1965 1787 1 1680 1831 1 1965 1829 1
		 1965 1830 1 1830 1648 1 1831 1649 1 1649 1830 1 1738 1966 1 1966 1833 1 1833 1646 1
		 1966 1906 1 1906 1729 1 1729 1833 1 1966 1832 1 1832 1730 1 1730 1906 1 1647 1832 1
		 1830 1967 1 1967 1836 1 1836 1648 1 1967 1909 1 1909 1731 1 1731 1836 1 1967 1835 1
		 1835 1726 1 1726 1909 1 1649 1835 1 1739 1968 1 1968 1832 1 1968 1908 1 1908 1730 1;
	setAttr ".ed[3652:3817]" 1968 1836 1 1731 1908 1 1841 1969 1 1969 1839 1 1839 1678 1
		 1678 1841 1 1969 1849 1 1849 1679 1 1679 1839 1 1969 1840 1 1840 1650 1 1650 1849 1
		 1841 1651 1 1651 1840 1 1843 1970 1 1970 1842 1 1842 1677 1 1677 1843 1 1970 1841 1
		 1678 1842 1 1970 1859 1 1859 1651 1 1843 1644 1 1644 1859 1 1784 1971 1 1971 1740 1
		 1971 1844 1 1844 1659 1 1971 1842 1 1678 1844 1 1844 1972 1 1972 1747 1 1972 1845 1
		 1845 1658 1 1972 1839 1 1679 1845 1 1746 1973 1 1973 1785 1 1973 1847 1 1847 1680 1
		 1973 1845 1 1679 1847 1 1827 1974 1 1974 1793 1 1974 1850 1 1850 1684 1 1974 1846 1
		 1846 1645 1 1645 1850 1 1646 1846 1 1849 1975 1 1975 1847 1 1975 1831 1 1975 1848 1
		 1848 1649 1 1650 1848 1 1851 1976 1 1976 1850 1 1645 1851 1 1976 1794 1 1976 1843 1
		 1851 1644 1 1853 1977 1 1977 1834 1 1834 1702 1 1702 1853 1 1977 1852 1 1852 1704 1
		 1704 1834 1 1977 1858 1 1858 1705 1 1705 1852 1 1853 1703 1 1703 1858 1 1838 1978 1
		 1978 1853 1 1702 1838 1 1978 1854 1 1854 1703 1 1978 1837 1 1837 1706 1 1706 1854 1
		 1838 1707 1 1707 1837 1 1833 1979 1 1979 1846 1 1979 1855 1 1855 1645 1 1979 1904 1
		 1904 1728 1 1728 1855 1 1729 1904 1 1855 1980 1 1980 1856 1 1856 1645 1 1980 1857 1
		 1857 1701 1 1701 1856 1 1980 1902 1 1902 1727 1 1727 1857 1 1728 1902 1 1857 1981 1
		 1981 1860 1 1860 1701 1 1981 1835 1 1649 1860 1 1981 1900 1 1900 1726 1 1727 1900 1
		 1887 1982 1 1982 1889 1 1889 1720 1 1720 1887 1 1982 1891 1 1891 1722 1 1722 1889 1
		 1982 1892 1 1892 1723 1 1723 1891 1 1887 1721 1 1721 1892 1 1895 1983 1 1983 1896 1
		 1896 1724 1 1724 1895 1 1983 1892 1 1721 1896 1 1983 1897 1 1897 1723 1 1895 1725 1
		 1725 1897 1 1856 1984 1 1984 1861 1 1861 1645 1 1984 1863 1 1863 1708 1 1708 1861 1
		 1984 1862 1 1862 1709 1 1709 1863 1 1701 1862 1 1851 1985 1 1985 1864 1 1864 1644 1
		 1985 1865 1 1865 1710 1 1710 1864 1 1985 1861 1 1708 1865 1 1859 1986 1 1986 1866 1
		 1866 1651 1 1986 1867 1 1867 1711 1 1711 1866 1 1986 1864 1 1710 1867 1 1848 1987 1
		 1987 1868 1 1868 1649 1 1987 1870 1 1870 1712 1 1712 1868 1 1987 1869 1 1869 1713 1;
	setAttr ".ed[3818:3983]" 1713 1870 1 1650 1869 1 1860 1988 1 1988 1862 1 1988 1871 1
		 1871 1709 1 1988 1868 1 1712 1871 1 1840 1989 1 1989 1869 1 1989 1872 1 1872 1713 1
		 1989 1866 1 1711 1872 1 1863 1990 1 1990 1873 1 1873 1708 1 1990 1875 1 1875 1714 1
		 1714 1873 1 1990 1874 1 1874 1715 1 1715 1875 1 1709 1874 1 1865 1991 1 1991 1876 1
		 1876 1710 1 1991 1877 1 1877 1716 1 1716 1876 1 1991 1873 1 1714 1877 1 1867 1992 1
		 1992 1878 1 1878 1711 1 1992 1879 1 1879 1717 1 1717 1878 1 1992 1876 1 1716 1879 1
		 1870 1993 1 1993 1880 1 1880 1712 1 1993 1882 1 1882 1718 1 1718 1880 1 1993 1881 1
		 1881 1719 1 1719 1882 1 1713 1881 1 1871 1994 1 1994 1874 1 1994 1883 1 1883 1715 1
		 1994 1880 1 1718 1883 1 1872 1995 1 1995 1881 1 1995 1884 1 1884 1719 1 1995 1878 1
		 1717 1884 1 1875 1996 1 1996 1885 1 1885 1714 1 1996 1887 1 1720 1885 1 1996 1886 1
		 1886 1721 1 1715 1886 1 1877 1997 1 1997 1888 1 1888 1716 1 1997 1889 1 1722 1888 1
		 1997 1885 1 1879 1998 1 1998 1890 1 1890 1717 1 1998 1891 1 1723 1890 1 1998 1888 1
		 1882 1999 1 1999 1893 1 1893 1718 1 1999 1895 1 1724 1893 1 1999 1894 1 1894 1725 1
		 1719 1894 1 1883 2000 1 2000 1886 1 2000 1896 1 2000 1893 1 1884 2001 1 2001 1894 1
		 2001 1897 1 2001 1890 1 1899 2002 1 2002 1900 1 1727 1899 1 2002 1898 1 1898 1726 1
		 2002 1912 1 1912 1732 1 1732 1898 1 1899 1733 1 1733 1912 1 1901 2003 1 2003 1902 1
		 1728 1901 1 2003 1899 1 2003 1914 1 1914 1733 1 1901 1734 1 1734 1914 1 1903 2004 1
		 2004 1904 1 1729 1903 1 2004 1901 1 2004 1916 1 1916 1734 1 1903 1735 1 1735 1916 1
		 1906 2005 1 2005 1903 1 2005 1918 1 1918 1735 1 2005 1905 1 1905 1736 1 1736 1918 1
		 1730 1905 1 1908 2006 1 2006 1905 1 2006 1920 1 1920 1736 1 2006 1907 1 1907 1737 1
		 1737 1920 1 1731 1907 1 1909 2007 1 2007 1907 1 2007 1921 1 1921 1737 1 2007 1898 1
		 1732 1921 1 1911 2008 1 2008 1912 1 1733 1911 1 2008 1910 1 1910 1732 1 2008 1854 1
		 1706 1910 1 1911 1703 1 1913 2009 1 2009 1914 1 1734 1913 1 2009 1911 1 2009 1858 1
		 1913 1705 1 1915 2010 1 2010 1916 1 1735 1915 1 2010 1913 1 2010 1852 1 1915 1704 1;
	setAttr ".ed[3984:4149]" 1918 2011 1 2011 1915 1 2011 1834 1 2011 1917 1 1917 1702 1
		 1736 1917 1 1920 2012 1 2012 1917 1 2012 1838 1 2012 1919 1 1919 1707 1 1737 1919 1
		 1921 2013 1 2013 1919 1 2013 1837 1 2013 1910 1 2041 2015 1 2015 2050 1 2050 2088 1
		 2088 2041 1 2050 2027 1 2027 2065 1 2065 2088 1 2065 2034 1 2034 2064 1 2064 2088 1
		 2064 2022 1 2022 2041 1 2051 2017 1 2017 2043 1 2043 2089 1 2089 2051 1 2043 2023 1
		 2023 2066 1 2066 2089 1 2066 2034 1 2065 2089 1 2027 2051 1 2042 2016 1 2016 2049 1
		 2049 2090 1 2090 2042 1 2049 2026 1 2026 2067 1 2067 2090 1 2067 2034 1 2066 2090 1
		 2023 2042 1 2048 2014 1 2014 2040 1 2040 2091 1 2091 2048 1 2040 2022 1 2064 2091 1
		 2067 2091 1 2026 2048 1 2017 2054 1 2054 2092 1 2092 2043 1 2054 2029 1 2029 2069 1
		 2069 2092 1 2069 2035 1 2035 2068 1 2068 2092 1 2068 2023 1 2055 2019 1 2019 2045 1
		 2045 2093 1 2093 2055 1 2045 2024 1 2024 2070 1 2070 2093 1 2070 2035 1 2069 2093 1
		 2029 2055 1 2044 2018 1 2018 2053 1 2053 2094 1 2094 2044 1 2053 2028 1 2028 2071 1
		 2071 2094 1 2071 2035 1 2070 2094 1 2024 2044 1 2052 2016 1 2042 2095 1 2095 2052 1
		 2068 2095 1 2071 2095 1 2028 2052 1 2019 2058 1 2058 2096 1 2096 2045 1 2058 2031 1
		 2031 2073 1 2073 2096 1 2073 2036 1 2036 2072 1 2072 2096 1 2072 2024 1 2059 2021 1
		 2021 2047 1 2047 2097 1 2097 2059 1 2047 2025 1 2025 2074 1 2074 2097 1 2074 2036 1
		 2073 2097 1 2031 2059 1 2046 2020 1 2020 2057 1 2057 2098 1 2098 2046 1 2057 2030 1
		 2030 2075 1 2075 2098 1 2075 2036 1 2074 2098 1 2025 2046 1 2056 2018 1 2044 2099 1
		 2099 2056 1 2072 2099 1 2075 2099 1 2030 2056 1 2021 2062 1 2062 2100 1 2100 2047 1
		 2062 2033 1 2033 2077 1 2077 2100 1 2077 2037 1 2037 2076 1 2076 2100 1 2076 2025 1
		 2063 2015 1 2041 2101 1 2101 2063 1 2022 2078 1 2078 2101 1 2078 2037 1 2077 2101 1
		 2033 2063 1 2014 2061 1 2061 2102 1 2102 2040 1 2061 2032 1 2032 2079 1 2079 2102 1
		 2079 2037 1 2078 2102 1 2060 2020 1 2046 2103 1 2103 2060 1 2076 2103 1 2079 2103 1
		 2032 2060 1 2059 2104 1 2104 2062 1 2031 2081 1 2081 2104 1 2081 2038 1 2038 2080 1;
	setAttr ".ed[4150:4315]" 2080 2104 1 2080 2033 1 2055 2105 1 2105 2058 1 2029 2082 1
		 2082 2105 1 2082 2038 1 2081 2105 1 2051 2106 1 2106 2054 1 2027 2083 1 2083 2106 1
		 2083 2038 1 2082 2106 1 2063 2107 1 2107 2050 1 2080 2107 1 2083 2107 1 2048 2108 1
		 2108 2061 1 2026 2085 1 2085 2108 1 2085 2039 1 2039 2084 1 2084 2108 1 2084 2032 1
		 2052 2109 1 2109 2049 1 2028 2086 1 2086 2109 1 2086 2039 1 2085 2109 1 2056 2110 1
		 2110 2053 1 2030 2087 1 2087 2110 1 2087 2039 1 2086 2110 1 2060 2111 1 2111 2057 1
		 2084 2111 1 2087 2111 1 2132 2113 1 2113 2137 1 2137 2168 1 2168 2132 1 2137 2127 1
		 2127 2159 1 2159 2168 1 2159 2124 1 2124 2136 1 2136 2168 1 2136 2112 1 2112 2132 1
		 2133 2115 1 2115 2139 1 2139 2169 1 2169 2133 1 2139 2117 1 2117 2134 1 2134 2169 1
		 2134 2116 1 2116 2138 1 2138 2169 1 2138 2114 1 2114 2133 1 2156 2126 1 2126 2155 1
		 2155 2170 1 2170 2156 1 2155 2119 1 2119 2135 1 2135 2170 1 2135 2118 1 2118 2153 1
		 2153 2170 1 2153 2125 1 2125 2156 1 2119 2143 1 2143 2171 1 2171 2135 1 2143 2113 1
		 2132 2171 1 2112 2142 1 2142 2171 1 2142 2118 1 2155 2172 1 2172 2143 1 2126 2158 1
		 2158 2172 1 2158 2127 1 2137 2172 1 2136 2173 1 2173 2142 1 2124 2154 1 2154 2173 1
		 2154 2125 1 2153 2173 1 2146 2120 1 2120 2144 1 2144 2174 1 2174 2146 1 2144 2114 1
		 2138 2174 1 2116 2140 1 2140 2174 1 2140 2121 1 2121 2146 1 2117 2141 1 2141 2175 1
		 2175 2134 1 2141 2122 1 2122 2148 1 2148 2175 1 2148 2121 1 2140 2175 1 2150 2122 1
		 2141 2176 1 2176 2150 1 2139 2176 1 2115 2149 1 2149 2176 1 2149 2123 1 2123 2150 1
		 2151 2123 1 2149 2177 1 2177 2151 1 2133 2177 1 2144 2177 1 2120 2151 1 2124 2152 1
		 2152 2178 1 2178 2154 1 2152 2128 1 2128 2162 1 2162 2178 1 2162 2129 1 2129 2161 1
		 2161 2178 1 2161 2125 1 2164 2130 1 2130 2163 1 2163 2179 1 2179 2164 1 2163 2126 1
		 2156 2179 1 2161 2179 1 2129 2164 1 2163 2180 1 2180 2158 1 2130 2166 1 2166 2180 1
		 2166 2131 1 2131 2157 1 2157 2180 1 2157 2127 1 2157 2181 1 2181 2159 1 2131 2167 1
		 2167 2181 1 2167 2128 1 2152 2181 1 2128 2160 1 2160 2182 1 2182 2162 1 2160 2120 1;
	setAttr ".ed[4316:4479]" 2146 2182 1 2121 2145 1 2145 2182 1 2145 2129 1 2122 2147 1
		 2147 2183 1 2183 2148 1 2147 2130 1 2164 2183 1 2145 2183 1 2147 2184 1 2184 2166 1
		 2150 2184 1 2123 2165 1 2165 2184 1 2165 2131 1 2165 2185 1 2185 2167 1 2151 2185 1
		 2160 2185 1 2206 2242 1 2242 2211 1 2211 2187 1 2187 2206 1 2242 2233 1 2233 2201 1
		 2201 2211 1 2242 2210 1 2210 2198 1 2198 2233 1 2206 2186 1 2186 2210 1 2207 2243 1
		 2243 2213 1 2213 2189 1 2189 2207 1 2243 2208 1 2208 2191 1 2191 2213 1 2243 2212 1
		 2212 2190 1 2190 2208 1 2207 2188 1 2188 2212 1 2230 2244 1 2244 2229 1 2229 2200 1
		 2200 2230 1 2244 2209 1 2209 2193 1 2193 2229 1 2244 2227 1 2227 2192 1 2192 2209 1
		 2230 2199 1 2199 2227 1 2209 2245 1 2245 2217 1 2217 2193 1 2245 2206 1 2187 2217 1
		 2245 2216 1 2216 2186 1 2192 2216 1 2217 2246 1 2246 2229 1 2246 2232 1 2232 2200 1
		 2246 2211 1 2201 2232 1 2216 2247 1 2247 2210 1 2247 2228 1 2228 2198 1 2247 2227 1
		 2199 2228 1 2220 2248 1 2248 2218 1 2218 2194 1 2194 2220 1 2248 2212 1 2188 2218 1
		 2248 2214 1 2214 2190 1 2220 2195 1 2195 2214 1 2208 2249 1 2249 2215 1 2215 2191 1
		 2249 2222 1 2222 2196 1 2196 2215 1 2249 2214 1 2195 2222 1 2224 2250 1 2250 2215 1
		 2196 2224 1 2250 2213 1 2250 2223 1 2223 2189 1 2224 2197 1 2197 2223 1 2225 2251 1
		 2251 2223 1 2197 2225 1 2251 2207 1 2251 2218 1 2225 2194 1 2228 2252 1 2252 2226 1
		 2226 2198 1 2252 2236 1 2236 2202 1 2202 2226 1 2252 2235 1 2235 2203 1 2203 2236 1
		 2199 2235 1 2238 2253 1 2253 2237 1 2237 2204 1 2204 2238 1 2253 2230 1 2200 2237 1
		 2253 2235 1 2238 2203 1 2232 2254 1 2254 2237 1 2254 2240 1 2240 2204 1 2254 2231 1
		 2231 2205 1 2205 2240 1 2201 2231 1 2233 2255 1 2255 2231 1 2255 2241 1 2241 2205 1
		 2255 2226 1 2202 2241 1 2236 2256 1 2256 2234 1 2234 2202 1 2256 2220 1 2194 2234 1
		 2256 2219 1 2219 2195 1 2203 2219 1 2222 2257 1 2257 2221 1 2221 2196 1 2257 2238 1
		 2204 2221 1 2257 2219 1 2240 2258 1 2258 2221 1 2258 2224 1 2258 2239 1 2239 2197 1
		 2205 2239 1 2241 2259 1 2259 2239 1 2259 2225 1 2259 2234 1;
	setAttr -s 2240 -ch 8960 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 -3
		mu 0 4 2 4 5 3
		f 4 7 8 9 -7
		mu 0 4 5 6 7 3
		f 4 10 11 -4 -10
		mu 0 4 7 8 0 3
		f 4 12 13 14 15
		mu 0 4 9 10 11 12
		f 4 16 17 18 -15
		mu 0 4 11 13 14 12
		f 4 19 -8 20 -19
		mu 0 4 14 6 5 12
		f 4 -6 21 -16 -21
		mu 0 4 5 4 9 12
		f 4 22 23 24 25
		mu 0 4 15 16 17 18
		f 4 26 27 28 -25
		mu 0 4 17 19 20 18
		f 4 29 -20 30 -29
		mu 0 4 20 6 14 18
		f 4 -18 31 -26 -31
		mu 0 4 14 13 15 18
		f 4 32 33 34 35
		mu 0 4 21 22 23 24
		f 4 36 -11 37 -35
		mu 0 4 23 8 7 24
		f 4 -9 -30 38 -38
		mu 0 4 7 6 20 24
		f 4 -28 39 -36 -39
		mu 0 4 20 19 21 24
		f 4 -14 40 41 42
		mu 0 4 11 10 25 26
		f 4 43 44 45 -42
		mu 0 4 25 27 28 26
		f 4 46 47 48 -46
		mu 0 4 28 29 30 26
		f 4 49 -17 -43 -49
		mu 0 4 30 13 11 26
		f 4 50 51 52 53
		mu 0 4 31 32 33 34
		f 4 54 55 56 -53
		mu 0 4 33 35 36 34
		f 4 57 -47 58 -57
		mu 0 4 36 29 28 34
		f 4 -45 59 -54 -59
		mu 0 4 28 27 31 34
		f 4 60 61 62 63
		mu 0 4 37 38 39 40
		f 4 64 65 66 -63
		mu 0 4 39 41 42 40
		f 4 67 -58 68 -67
		mu 0 4 42 29 36 40
		f 4 -56 69 -64 -69
		mu 0 4 36 43 37 40
		f 4 70 -23 71 72
		mu 0 4 44 16 15 45
		f 4 -32 -50 73 -72
		mu 0 4 15 13 30 45
		f 4 -48 -68 74 -74
		mu 0 4 30 29 42 45
		f 4 -66 75 -73 -75
		mu 0 4 42 41 44 45
		f 4 -52 76 77 78
		mu 0 4 33 32 46 47
		f 4 79 80 81 -78
		mu 0 4 46 48 49 47
		f 4 82 83 84 -82
		mu 0 4 49 50 51 47
		f 4 85 -55 -79 -85
		mu 0 4 51 35 33 47
		f 4 86 87 88 89
		mu 0 4 52 53 54 55
		f 4 90 91 92 -89
		mu 0 4 54 56 57 55
		f 4 93 -83 94 -93
		mu 0 4 57 50 49 55
		f 4 -81 95 -90 -95
		mu 0 4 49 48 52 55
		f 4 96 97 98 99
		mu 0 4 58 59 60 61
		f 4 100 101 102 -99
		mu 0 4 60 62 63 61
		f 4 103 -94 104 -103
		mu 0 4 63 64 65 61
		f 4 -92 105 -100 -105
		mu 0 4 65 66 58 61
		f 4 106 -61 107 108
		mu 0 4 67 38 37 68
		f 4 -70 -86 109 -108
		mu 0 4 37 43 69 68
		f 4 -84 -104 110 -110
		mu 0 4 69 64 63 68
		f 4 -102 111 -109 -111
		mu 0 4 63 62 67 68
		f 4 112 113 114 115
		mu 0 4 70 71 72 73
		f 4 116 117 118 -115
		mu 0 4 72 74 75 73
		f 4 119 120 121 -119
		mu 0 4 75 76 77 73
		f 4 122 123 -116 -122
		mu 0 4 77 78 70 73
		f 4 124 125 126 127
		mu 0 4 79 80 81 82
		f 4 128 129 130 -127
		mu 0 4 81 83 84 82
		f 4 131 -120 132 -131
		mu 0 4 84 76 75 82
		f 4 -118 133 -128 -133
		mu 0 4 75 74 79 82
		f 4 134 135 136 137
		mu 0 4 85 86 87 88
		f 4 138 139 140 -137
		mu 0 4 87 89 90 88
		f 4 141 -132 142 -141
		mu 0 4 90 76 84 88
		f 4 -130 143 -138 -143
		mu 0 4 84 83 85 88
		f 4 144 145 146 147
		mu 0 4 91 92 93 94
		f 4 148 -123 149 -147
		mu 0 4 93 95 96 94
		f 4 -121 -142 150 -150
		mu 0 4 96 76 90 94
		f 4 -140 151 -148 -151
		mu 0 4 90 89 91 94
		f 4 152 -87 153 154
		mu 0 4 97 53 52 98
		f 4 -96 155 156 -154
		mu 0 4 52 48 99 98
		f 4 157 158 159 -157
		mu 0 4 99 100 101 98
		f 4 160 161 -155 -160
		mu 0 4 101 102 97 98
		f 4 -77 -51 162 163
		mu 0 4 46 32 31 103
		f 4 -60 164 165 -163
		mu 0 4 31 27 104 103
		f 4 166 -158 167 -166
		mu 0 4 104 100 99 103
		f 4 -156 -80 -164 -168
		mu 0 4 99 48 46 103
		f 4 -41 -13 168 169
		mu 0 4 25 10 9 105
		f 4 -22 170 171 -169
		mu 0 4 9 4 106 105
		f 4 172 -167 173 -172
		mu 0 4 106 100 104 105
		f 4 -165 -44 -170 -174
		mu 0 4 104 27 25 105
		f 4 -2 174 175 176
		mu 0 4 2 1 107 108
		f 4 177 -161 178 -176
		mu 0 4 107 102 101 108
		f 4 -159 -173 179 -179
		mu 0 4 101 100 106 108
		f 4 -171 -5 -177 -180
		mu 0 4 106 4 2 108
		f 4 180 -33 181 182
		mu 0 4 109 22 21 110
		f 4 -40 183 184 -182
		mu 0 4 21 19 111 110
		f 4 185 186 187 -185
		mu 0 4 111 112 113 110
		f 4 188 189 -183 -188
		mu 0 4 113 114 109 110
		f 4 -24 -71 190 191
		mu 0 4 17 16 44 115
		f 4 -76 192 193 -191
		mu 0 4 44 41 116 115
		f 4 194 -186 195 -194
		mu 0 4 116 112 111 115
		f 4 -184 -27 -192 -196
		mu 0 4 111 19 17 115
		f 4 -62 -107 196 197
		mu 0 4 39 38 67 117
		f 4 -112 198 199 -197
		mu 0 4 67 62 118 117
		f 4 200 -195 201 -200
		mu 0 4 118 112 116 117
		f 4 -193 -65 -198 -202
		mu 0 4 116 41 39 117
		f 4 -98 202 203 204
		mu 0 4 60 59 119 120
		f 4 205 -189 206 -204
		mu 0 4 119 114 113 120
		f 4 -187 -201 207 -207
		mu 0 4 113 112 118 120
		f 4 -199 -101 -205 -208
		mu 0 4 118 62 60 120
		f 4 -88 208 209 210
		mu 0 4 54 53 121 122
		f 4 211 212 213 -210
		mu 0 4 123 124 125 126
		f 4 214 215 216 -214
		mu 0 4 125 127 128 126
		f 4 217 -91 -211 -217
		mu 0 4 129 56 54 122
		f 4 -162 218 219 220
		mu 0 4 97 102 130 131
		f 4 221 222 223 -220
		mu 0 4 132 133 134 135
		f 4 224 -212 225 -224
		mu 0 4 134 124 123 135
		f 4 -209 -153 -221 -226
		mu 0 4 121 53 97 131
		f 4 -175 226 227 228
		mu 0 4 107 1 136 137
		f 4 229 230 231 -228
		mu 0 4 138 139 140 141
		f 4 232 -222 233 -232
		mu 0 4 140 133 132 141
		f 4 -219 -178 -229 -234
		mu 0 4 130 102 107 137
		f 4 -12 234 235 236
		mu 0 4 0 8 142 143
		f 4 237 238 239 -236
		mu 0 4 144 145 146 147
		f 4 240 -230 241 -240
		mu 0 4 146 139 138 147
		f 4 -227 -1 -237 -242
		mu 0 4 136 1 0 143
		f 4 -34 242 243 244
		mu 0 4 23 22 148 149
		f 4 245 246 247 -244
		mu 0 4 150 151 152 153
		f 4 248 -238 249 -248
		mu 0 4 152 145 144 153
		f 4 -235 -37 -245 -250
		mu 0 4 142 8 23 149
		f 4 -190 250 251 252
		mu 0 4 109 114 154 155
		f 4 253 254 255 -252
		mu 0 4 156 157 158 159
		f 4 256 -246 257 -256
		mu 0 4 158 151 150 159
		f 4 -243 -181 -253 -258
		mu 0 4 148 22 109 155
		f 4 -203 258 259 260
		mu 0 4 119 59 160 161
		f 4 261 262 263 -260
		mu 0 4 162 163 164 165
		f 4 264 -254 265 -264
		mu 0 4 164 157 156 165
		f 4 -251 -206 -261 -266
		mu 0 4 154 114 119 161
		f 4 -106 -218 266 267
		mu 0 4 58 66 166 167
		f 4 -216 268 269 -267
		mu 0 4 168 169 170 171
		f 4 270 -262 271 -270
		mu 0 4 170 163 162 171
		f 4 -259 -97 -268 -272
		mu 0 4 160 59 58 167
		f 4 -213 272 273 274
		mu 0 4 125 124 172 173
		f 4 275 -113 276 -274
		mu 0 4 172 71 70 173
		f 4 -124 277 278 -277
		mu 0 4 70 78 174 173
		f 4 279 -215 -275 -279
		mu 0 4 174 127 125 173
		f 4 -223 280 281 282
		mu 0 4 134 133 175 176
		f 4 283 -117 284 -282
		mu 0 4 175 74 72 176
		f 4 -114 -276 285 -285
		mu 0 4 72 71 172 176
		f 4 -273 -225 -283 -286
		mu 0 4 172 124 134 176
		f 4 -231 286 287 288
		mu 0 4 140 139 177 178
		f 4 289 -125 290 -288
		mu 0 4 177 80 79 178
		f 4 -134 -284 291 -291
		mu 0 4 79 74 175 178
		f 4 -281 -233 -289 -292
		mu 0 4 175 133 140 178
		f 4 -239 292 293 294
		mu 0 4 146 145 179 180
		f 4 295 -129 296 -294
		mu 0 4 179 83 81 180
		f 4 -126 -290 297 -297
		mu 0 4 81 80 177 180
		f 4 -287 -241 -295 -298
		mu 0 4 177 139 146 180
		f 4 -247 298 299 300
		mu 0 4 152 151 181 182
		f 4 301 -135 302 -300
		mu 0 4 181 86 85 182
		f 4 -144 -296 303 -303
		mu 0 4 85 83 179 182
		f 4 -293 -249 -301 -304
		mu 0 4 179 145 152 182
		f 4 -255 304 305 306
		mu 0 4 158 157 183 184
		f 4 307 -139 308 -306
		mu 0 4 183 89 87 184
		f 4 -136 -302 309 -309
		mu 0 4 87 86 181 184
		f 4 -299 -257 -307 -310
		mu 0 4 181 151 158 184
		f 4 -263 310 311 312
		mu 0 4 164 163 185 186
		f 4 313 -145 314 -312
		mu 0 4 185 92 91 186
		f 4 -152 -308 315 -315
		mu 0 4 91 89 183 186
		f 4 -305 -265 -313 -316
		mu 0 4 183 157 164 186
		f 4 -269 -280 316 317
		mu 0 4 170 169 187 188
		f 4 -278 -149 318 -317
		mu 0 4 187 95 93 188
		f 4 -146 -314 319 -319
		mu 0 4 93 92 185 188
		f 4 -311 -271 -318 -320
		mu 0 4 185 163 170 188
		f 4 320 321 322 323
		mu 0 4 189 190 191 192
		f 4 324 325 326 -323
		mu 0 4 191 193 194 192
		f 4 327 328 329 -327
		mu 0 4 194 195 196 192
		f 4 330 331 -324 -330
		mu 0 4 196 197 189 192
		f 4 332 333 334 335
		mu 0 4 198 199 200 201
		f 4 336 337 338 -335
		mu 0 4 200 202 203 201
		f 4 339 340 341 -339
		mu 0 4 203 204 205 201
		f 4 342 343 -336 -342
		mu 0 4 205 206 198 201
		f 4 344 345 346 347
		mu 0 4 207 208 209 210
		f 4 348 -333 349 -347
		mu 0 4 209 211 212 210
		f 4 -344 350 351 -350
		mu 0 4 212 213 214 210
		f 4 352 353 -348 -352
		mu 0 4 214 215 207 210
		f 4 354 -325 355 356
		mu 0 4 216 193 191 217
		f 4 -322 357 358 -356
		mu 0 4 191 190 218 217
		f 4 359 -345 360 -359
		mu 0 4 218 208 207 217
		f 4 -354 361 -357 -361
		mu 0 4 207 215 216 217
		f 4 362 363 364 365
		mu 0 4 219 220 221 222
		f 4 366 -331 367 -365
		mu 0 4 221 197 196 222
		f 4 -329 368 369 -368
		mu 0 4 196 195 223 222
		f 4 370 371 -366 -370
		mu 0 4 223 224 219 222
		f 4 -338 372 373 374
		mu 0 4 203 202 225 226
		f 4 375 376 377 -374
		mu 0 4 225 227 228 226
		f 4 378 379 380 -378
		mu 0 4 228 229 230 226
		f 4 381 -340 -375 -381
		mu 0 4 230 204 203 226
		f 4 382 383 384 385
		mu 0 4 231 232 233 234
		f 4 386 387 388 -385
		mu 0 4 233 235 236 234
		f 4 389 390 391 -389
		mu 0 4 236 237 238 234
		f 4 392 393 -386 -392
		mu 0 4 238 239 231 234
		f 4 -391 394 395 396
		mu 0 4 238 240 241 242
		f 4 397 398 399 -396
		mu 0 4 241 243 244 242
		f 4 400 401 402 -400
		mu 0 4 244 245 246 242
		f 4 403 -393 -397 -403
		mu 0 4 246 239 238 242
		f 4 -399 404 405 406
		mu 0 4 244 243 247 248
		f 4 407 408 409 -406
		mu 0 4 247 249 250 248
		f 4 410 411 412 -410
		mu 0 4 250 251 252 248
		f 4 413 -401 -407 -413
		mu 0 4 252 245 244 248
		f 4 -409 414 415 416
		mu 0 4 250 249 253 254
		f 4 417 418 419 -416
		mu 0 4 253 255 256 254
		f 4 420 421 422 -420
		mu 0 4 256 257 258 254
		f 4 423 -411 -417 -423
		mu 0 4 258 251 250 254
		f 4 -419 424 425 426
		mu 0 4 256 255 259 260
		f 4 427 428 429 -426
		mu 0 4 259 261 262 260
		f 4 430 431 432 -430
		mu 0 4 262 263 264 260
		f 4 433 -421 -427 -433
		mu 0 4 264 257 256 260
		f 4 -429 434 435 436
		mu 0 4 265 266 267 268
		f 4 437 438 439 -436
		mu 0 4 267 269 270 268
		f 4 440 441 442 -440
		mu 0 4 270 271 272 268
		f 4 443 -431 -437 -443
		mu 0 4 272 273 265 268
		f 4 -439 444 445 446
		mu 0 4 270 269 274 275
		f 4 447 448 449 -446
		mu 0 4 274 276 277 275
		f 4 450 451 452 -450
		mu 0 4 277 278 279 275
		f 4 453 -441 -447 -453
		mu 0 4 279 271 270 275
		f 4 -449 454 455 456
		mu 0 4 277 276 280 281
		f 4 457 -387 458 -456
		mu 0 4 280 235 233 281
		f 4 -384 459 460 -459
		mu 0 4 233 232 282 281
		f 4 461 -451 -457 -461
		mu 0 4 282 278 277 281
		f 4 462 463 464 465
		mu 0 4 283 284 285 286
		f 4 466 467 468 -465
		mu 0 4 285 287 288 286
		f 4 469 -363 470 -469
		mu 0 4 288 220 219 286
		f 4 -372 471 -466 -471
		mu 0 4 219 224 283 286
		f 4 472 -379 473 474
		mu 0 4 289 229 228 290
		f 4 -377 475 476 -474
		mu 0 4 228 227 291 290
		f 4 477 -467 478 -477
		mu 0 4 291 287 285 290
		f 4 -464 479 -475 -479
		mu 0 4 285 284 289 290
		f 4 480 -408 481 482
		mu 0 4 292 249 247 293
		f 4 -405 -398 483 -482
		mu 0 4 247 243 241 293
		f 4 -395 484 485 -484
		mu 0 4 241 240 294 293
		f 4 486 487 -483 -486
		mu 0 4 294 295 292 293
		f 4 488 -487 489 490
		mu 0 4 296 297 298 299
		f 4 -485 -390 491 -490
		mu 0 4 298 237 236 299
		f 4 -388 -458 492 -492
		mu 0 4 236 235 280 299
		f 4 -455 493 -491 -493
		mu 0 4 280 276 296 299
		f 4 -494 -448 494 495
		mu 0 4 296 276 274 300
		f 4 -445 -438 496 -495
		mu 0 4 274 269 267 300
		f 4 -435 497 498 -497
		mu 0 4 267 266 301 300
		f 4 499 -489 -496 -499
		mu 0 4 301 297 296 300
		f 4 -488 -500 500 501
		mu 0 4 292 295 302 303
		f 4 -498 -428 502 -501
		mu 0 4 302 261 259 303
		f 4 -425 -418 503 -503
		mu 0 4 259 255 253 303
		f 4 -415 -481 -502 -504
		mu 0 4 253 249 292 303
		f 4 504 505 506 507
		mu 0 4 304 305 306 307
		f 4 508 509 510 -507
		mu 0 4 306 308 309 307
		f 4 511 512 513 -511
		mu 0 4 309 310 311 307
		f 4 514 515 -508 -514
		mu 0 4 311 312 304 307
		f 4 516 517 518 519
		mu 0 4 313 314 315 316
		f 4 520 -505 521 -519
		mu 0 4 315 305 304 316
		f 4 -516 522 523 -522
		mu 0 4 304 312 317 316
		f 4 524 525 -520 -524
		mu 0 4 317 318 313 316
		f 4 526 527 528 529
		mu 0 4 319 320 321 322
		f 4 530 -517 531 -529
		mu 0 4 321 314 313 322
		f 4 -526 532 533 -532
		mu 0 4 313 318 323 322
		f 4 534 535 -530 -534
		mu 0 4 323 324 319 322
		f 4 536 537 538 539
		mu 0 4 325 326 327 328
		f 4 540 -527 541 -539
		mu 0 4 327 329 330 328
		f 4 -536 542 543 -542
		mu 0 4 330 331 332 328
		f 4 544 545 -540 -544
		mu 0 4 332 333 325 328
		f 4 546 547 548 549
		mu 0 4 334 335 336 337
		f 4 550 551 552 -549
		mu 0 4 336 338 339 337
		f 4 553 -537 554 -553
		mu 0 4 339 326 325 337
		f 4 -546 555 -550 -555
		mu 0 4 325 333 334 337
		f 4 556 557 558 559
		mu 0 4 340 341 342 343
		f 4 560 -551 561 -559
		mu 0 4 342 338 336 343
		f 4 -548 562 563 -562
		mu 0 4 336 335 344 343
		f 4 564 565 -560 -564
		mu 0 4 344 345 340 343
		f 4 566 567 568 569
		mu 0 4 346 347 348 349
		f 4 570 -557 571 -569
		mu 0 4 348 341 340 349
		f 4 -566 572 573 -572
		mu 0 4 340 345 350 349
		f 4 574 575 -570 -574
		mu 0 4 350 351 346 349
		f 4 -510 576 577 578
		mu 0 4 309 308 352 353
		f 4 579 -567 580 -578
		mu 0 4 352 347 346 353
		f 4 -576 581 582 -581
		mu 0 4 346 351 354 353
		f 4 583 -512 -579 -583
		mu 0 4 354 310 309 353
		f 4 584 585 586 587
		mu 0 4 355 356 357 358
		f 4 588 589 590 -587
		mu 0 4 357 359 360 358
		f 4 591 -509 592 -591
		mu 0 4 360 308 306 358
		f 4 -506 593 -588 -593
		mu 0 4 306 305 355 358
		f 4 594 595 596 597
		mu 0 4 361 362 363 364
		f 4 598 -585 599 -597
		mu 0 4 363 356 355 364
		f 4 -594 -521 600 -600
		mu 0 4 355 305 315 364
		f 4 -518 601 -598 -601
		mu 0 4 315 314 361 364
		f 4 602 603 604 605
		mu 0 4 365 366 367 368
		f 4 606 -595 607 -605
		mu 0 4 367 362 361 368
		f 4 -602 -531 608 -608
		mu 0 4 361 314 321 368
		f 4 -528 609 -606 -609
		mu 0 4 321 320 365 368
		f 4 610 611 612 613
		mu 0 4 369 370 371 372
		f 4 614 -603 615 -613
		mu 0 4 371 373 374 372
		f 4 -610 -541 616 -616
		mu 0 4 374 329 327 372
		f 4 -538 617 -614 -617
		mu 0 4 327 326 369 372
		f 4 618 619 620 621
		mu 0 4 375 376 377 378
		f 4 622 -611 623 -621
		mu 0 4 377 370 369 378
		f 4 -618 -554 624 -624
		mu 0 4 369 326 339 378
		f 4 -552 625 -622 -625
		mu 0 4 339 338 375 378
		f 4 626 627 628 629
		mu 0 4 379 380 381 382
		f 4 630 -619 631 -629
		mu 0 4 381 376 375 382
		f 4 -626 -561 632 -632
		mu 0 4 375 338 342 382
		f 4 -558 633 -630 -633
		mu 0 4 342 341 379 382
		f 4 634 635 636 637
		mu 0 4 383 384 385 386
		f 4 638 -627 639 -637
		mu 0 4 385 380 379 386
		f 4 -634 -571 640 -640
		mu 0 4 379 341 348 386
		f 4 -568 641 -638 -641
		mu 0 4 348 347 383 386
		f 4 -577 -592 642 643
		mu 0 4 352 308 360 387
		f 4 -590 644 645 -643
		mu 0 4 360 359 388 387
		f 4 646 -635 647 -646
		mu 0 4 388 384 383 387
		f 4 -642 -580 -644 -648
		mu 0 4 383 347 352 387
		f 4 648 -462 649 650
		mu 0 4 389 278 282 390
		f 4 -460 651 652 -650
		mu 0 4 282 232 391 390
		f 4 653 -589 654 -653
		mu 0 4 391 359 357 390
		f 4 -586 655 -651 -655
		mu 0 4 357 356 389 390
		f 4 656 -454 657 658
		mu 0 4 392 271 279 393
		f 4 -452 -649 659 -658
		mu 0 4 279 278 389 393
		f 4 -656 -599 660 -660
		mu 0 4 389 356 363 393
		f 4 -596 661 -659 -661
		mu 0 4 363 362 392 393
		f 4 662 -444 663 664
		mu 0 4 394 273 272 395
		f 4 -442 -657 665 -664
		mu 0 4 272 271 392 395
		f 4 -662 -607 666 -666
		mu 0 4 392 362 367 395
		f 4 -604 667 -665 -667
		mu 0 4 367 366 394 395
		f 4 668 -434 669 670
		mu 0 4 396 257 264 397
		f 4 -432 -663 671 -670
		mu 0 4 264 263 398 397
		f 4 -668 -615 672 -672
		mu 0 4 398 373 371 397
		f 4 -612 673 -671 -673
		mu 0 4 371 370 396 397
		f 4 674 -424 675 676
		mu 0 4 399 251 258 400
		f 4 -422 -669 677 -676
		mu 0 4 258 257 396 400
		f 4 -674 -623 678 -678
		mu 0 4 396 370 377 400
		f 4 -620 679 -677 -679
		mu 0 4 377 376 399 400
		f 4 680 -414 681 682
		mu 0 4 401 245 252 402
		f 4 -412 -675 683 -682
		mu 0 4 252 251 399 402
		f 4 -680 -631 684 -684
		mu 0 4 399 376 381 402
		f 4 -628 685 -683 -685
		mu 0 4 381 380 401 402
		f 4 686 -404 687 688
		mu 0 4 403 239 246 404
		f 4 -402 -681 689 -688
		mu 0 4 246 245 401 404
		f 4 -686 -639 690 -690
		mu 0 4 401 380 385 404
		f 4 -636 691 -689 -691
		mu 0 4 385 384 403 404
		f 4 -645 -654 692 693
		mu 0 4 388 359 391 405
		f 4 -652 -383 694 -693
		mu 0 4 391 232 231 405
		f 4 -394 -687 695 -695
		mu 0 4 231 239 403 405
		f 4 -692 -647 -694 -696
		mu 0 4 403 384 388 405
		f 4 -326 696 697 698
		mu 0 4 194 193 406 407
		f 4 699 700 701 -698
		mu 0 4 406 408 409 407
		f 4 702 703 704 -702
		mu 0 4 409 410 411 407
		f 4 705 -328 -699 -705
		mu 0 4 411 195 194 407
		f 4 706 -371 707 708
		mu 0 4 412 224 223 413
		f 4 -369 -706 709 -708
		mu 0 4 223 195 411 413
		f 4 -704 710 711 -710
		mu 0 4 411 410 414 413
		f 4 712 713 -709 -712
		mu 0 4 414 415 412 413
		f 4 714 715 716 717
		mu 0 4 416 417 418 419
		f 4 718 -463 719 -717
		mu 0 4 418 284 283 419
		f 4 -472 -707 720 -720
		mu 0 4 283 224 412 419
		f 4 -714 721 -718 -721
		mu 0 4 412 415 416 419
		f 4 722 723 724 725
		mu 0 4 420 421 422 423
		f 4 726 -473 727 -725
		mu 0 4 422 229 289 423
		f 4 -480 -719 728 -728
		mu 0 4 289 284 418 423
		f 4 -716 729 -726 -729
		mu 0 4 418 417 420 423
		f 4 730 -382 731 732
		mu 0 4 424 204 230 425
		f 4 -380 -727 733 -732
		mu 0 4 230 229 422 425
		f 4 -724 734 735 -734
		mu 0 4 422 421 426 425
		f 4 736 737 -733 -736
		mu 0 4 426 427 424 425
		f 4 738 -343 739 740
		mu 0 4 428 206 205 429
		f 4 -341 -731 741 -740
		mu 0 4 205 204 424 429
		f 4 -738 742 743 -742
		mu 0 4 424 427 430 429
		f 4 744 745 -741 -744
		mu 0 4 430 431 428 429
		f 4 746 -353 747 748
		mu 0 4 432 215 214 433
		f 4 -351 -739 749 -748
		mu 0 4 214 213 434 433
		f 4 -746 750 751 -750
		mu 0 4 434 435 436 433
		f 4 752 753 -749 -752
		mu 0 4 436 437 432 433
		f 4 -697 -355 754 755
		mu 0 4 406 193 216 438
		f 4 -362 -747 756 -755
		mu 0 4 216 215 432 438
		f 4 -754 757 758 -757
		mu 0 4 432 437 439 438
		f 4 759 -700 -756 -759
		mu 0 4 439 408 406 438
		f 4 760 -565 761 762
		mu 0 4 440 345 344 441
		f 4 -563 763 764 -762
		mu 0 4 344 335 442 441
		f 4 765 -321 766 -765
		mu 0 4 442 443 444 441
		f 4 -332 767 -763 -767
		mu 0 4 444 445 440 441
		f 4 768 -575 769 770
		mu 0 4 446 351 350 447
		f 4 -573 -761 771 -770
		mu 0 4 350 345 440 447
		f 4 -768 -367 772 -772
		mu 0 4 440 445 448 447
		f 4 -364 773 -771 -773
		mu 0 4 448 449 446 447
		f 4 774 -584 775 776
		mu 0 4 450 310 354 451
		f 4 -582 -769 777 -776
		mu 0 4 354 351 446 451
		f 4 -774 -470 778 -778
		mu 0 4 446 449 452 451
		f 4 -468 779 -777 -779
		mu 0 4 452 453 450 451
		f 4 780 -515 781 782
		mu 0 4 454 312 311 455
		f 4 -513 -775 783 -782
		mu 0 4 311 310 450 455
		f 4 -780 -478 784 -784
		mu 0 4 450 453 456 455
		f 4 -476 785 -783 -785
		mu 0 4 456 457 454 455
		f 4 786 -525 787 788
		mu 0 4 458 318 317 459
		f 4 -523 -781 789 -788
		mu 0 4 317 312 454 459
		f 4 -786 -376 790 -790
		mu 0 4 454 457 460 459
		f 4 -373 791 -789 -791
		mu 0 4 460 461 458 459
		f 4 792 -535 793 794
		mu 0 4 462 324 323 463
		f 4 -533 -787 795 -794
		mu 0 4 323 318 458 463
		f 4 -792 -337 796 -796
		mu 0 4 458 461 464 463
		f 4 -334 797 -795 -797
		mu 0 4 464 465 462 463
		f 4 798 -545 799 800
		mu 0 4 466 333 332 467
		f 4 -543 -793 801 -800
		mu 0 4 332 331 468 467
		f 4 -798 -349 802 -802
		mu 0 4 468 469 470 467
		f 4 -346 803 -801 -803
		mu 0 4 470 471 466 467
		f 4 -764 -547 804 805
		mu 0 4 442 335 334 472
		f 4 -556 -799 806 -805
		mu 0 4 334 333 466 472
		f 4 -804 -360 807 -807
		mu 0 4 466 471 473 472
		f 4 -358 -766 -806 -808
		mu 0 4 473 443 442 472
		f 4 808 809 810 811
		mu 0 4 474 475 476 477
		f 4 812 813 814 -811
		mu 0 4 476 478 479 477
		f 4 815 -753 816 -815
		mu 0 4 480 437 436 481
		f 4 -751 817 -812 -817
		mu 0 4 436 435 482 481
		f 4 818 819 820 821
		mu 0 4 483 484 485 486
		f 4 822 -715 823 -821
		mu 0 4 487 417 416 488
		f 4 -722 824 825 -824
		mu 0 4 416 415 489 488
		f 4 826 827 -822 -826
		mu 0 4 490 491 483 486
		f 4 -814 828 829 830
		mu 0 4 479 478 492 493
		f 4 831 832 833 -830
		mu 0 4 492 494 495 493
		f 4 834 -760 835 -834
		mu 0 4 496 408 439 497
		f 4 -758 -816 -831 -836
		mu 0 4 439 437 480 497
		f 4 836 837 838 839
		mu 0 4 498 499 500 501
		f 4 840 -723 841 -839
		mu 0 4 502 421 420 503
		f 4 -730 -823 842 -842
		mu 0 4 420 417 487 503
		f 4 -820 843 -840 -843
		mu 0 4 485 484 498 501
		f 4 -743 844 845 846
		mu 0 4 430 427 504 505
		f 4 847 848 849 -846
		mu 0 4 506 507 508 509
		f 4 850 -809 851 -850
		mu 0 4 508 475 474 509
		f 4 -818 -745 -847 -852
		mu 0 4 510 431 430 505
		f 4 -833 852 853 854
		mu 0 4 495 494 511 512
		f 4 855 856 857 -854
		mu 0 4 511 513 514 512
		f 4 858 -703 859 -858
		mu 0 4 515 410 409 516
		f 4 -701 -835 -855 -860
		mu 0 4 409 408 496 516
		f 4 -857 860 861 862
		mu 0 4 514 513 517 518
		f 4 863 -827 864 -862
		mu 0 4 517 491 490 518
		f 4 -825 -713 865 -865
		mu 0 4 489 415 414 519
		f 4 -711 -859 -863 -866
		mu 0 4 414 410 515 519
		f 4 -735 -841 866 867
		mu 0 4 426 421 502 520
		f 4 -838 868 869 -867
		mu 0 4 500 499 521 522
		f 4 870 -848 871 -870
		mu 0 4 521 507 506 522
		f 4 -845 -737 -868 -872
		mu 0 4 504 427 426 520
		f 4 -849 872 873 874
		mu 0 4 508 507 523 524
		f 4 875 876 877 -874
		mu 0 4 523 525 526 524
		f 4 878 879 880 -878
		mu 0 4 526 527 528 524
		f 4 881 -851 -875 -881
		mu 0 4 528 475 508 524
		f 4 -810 -882 882 883
		mu 0 4 476 475 528 529
		f 4 -880 884 885 -883
		mu 0 4 528 527 530 529
		f 4 886 887 888 -886
		mu 0 4 530 531 532 529
		f 4 889 -813 -884 -889
		mu 0 4 532 478 476 529
		f 4 -829 -890 890 891
		mu 0 4 492 478 532 533
		f 4 -888 892 893 -891
		mu 0 4 532 531 534 533
		f 4 894 895 896 -894
		mu 0 4 534 535 536 533
		f 4 897 -832 -892 -897
		mu 0 4 536 494 492 533
		f 4 898 899 900 901
		mu 0 4 537 538 539 540
		f 4 902 -856 903 -901
		mu 0 4 539 513 511 540
		f 4 -853 -898 904 -904
		mu 0 4 511 494 536 540
		f 4 -896 905 -902 -905
		mu 0 4 536 535 537 540
		f 4 906 907 908 909
		mu 0 4 541 542 543 544
		f 4 910 -864 911 -909
		mu 0 4 543 491 517 544
		f 4 -861 -903 912 -912
		mu 0 4 517 513 539 544
		f 4 -900 913 -910 -913
		mu 0 4 539 538 541 544
		f 4 -828 -911 914 915
		mu 0 4 483 491 543 545
		f 4 -908 916 917 -915
		mu 0 4 543 542 546 545
		f 4 918 919 920 -918
		mu 0 4 546 547 548 545
		f 4 921 -819 -916 -921
		mu 0 4 548 484 483 545
		f 4 -920 922 923 924
		mu 0 4 548 547 549 550
		f 4 925 926 927 -924
		mu 0 4 549 551 552 550
		f 4 928 -837 929 -928
		mu 0 4 552 499 498 550
		f 4 -844 -922 -925 -930
		mu 0 4 498 484 548 550
		f 4 -869 -929 930 931
		mu 0 4 521 499 552 553
		f 4 -927 932 933 -931
		mu 0 4 552 551 554 553
		f 4 934 -876 935 -934
		mu 0 4 554 525 523 553
		f 4 -873 -871 -932 -936
		mu 0 4 523 507 521 553
		f 4 936 937 938 939
		mu 0 4 555 556 557 558
		f 4 940 -879 941 -939
		mu 0 4 557 527 526 558
		f 4 -877 -935 942 -942
		mu 0 4 526 525 554 558
		f 4 -933 943 -940 -943
		mu 0 4 554 551 555 558
		f 4 944 -895 945 946
		mu 0 4 559 535 534 560
		f 4 -893 -887 947 -946
		mu 0 4 534 531 530 560
		f 4 -885 -941 948 -948
		mu 0 4 530 527 557 560
		f 4 -938 949 -947 -949
		mu 0 4 557 556 559 560
		f 4 -944 -926 950 951
		mu 0 4 555 551 549 561
		f 4 -923 -919 952 -951
		mu 0 4 549 547 546 561
		f 4 -917 953 954 -953
		mu 0 4 546 542 562 561
		f 4 955 -937 -952 -955
		mu 0 4 562 556 555 561
		f 4 -950 -956 956 957
		mu 0 4 559 556 562 563
		f 4 -954 -907 958 -957
		mu 0 4 562 542 541 563
		f 4 -914 -899 959 -959
		mu 0 4 541 538 537 563
		f 4 -906 -945 -958 -960
		mu 0 4 537 535 559 563
		f 4 960 961 962 963
		mu 0 4 564 565 566 567
		f 4 964 965 966 -963
		mu 0 4 566 568 569 567
		f 4 967 968 969 -967
		mu 0 4 569 570 571 567
		f 4 970 971 -964 -970
		mu 0 4 571 572 564 567
		f 4 -966 972 973 974
		mu 0 4 569 568 573 574
		f 4 975 976 977 -974
		mu 0 4 573 575 576 574
		f 4 978 979 980 -978
		mu 0 4 576 577 578 574
		f 4 981 -968 -975 -981
		mu 0 4 578 570 569 574
		f 4 -977 982 983 984
		mu 0 4 576 575 579 580
		f 4 985 986 987 -984
		mu 0 4 579 581 582 580
		f 4 988 989 990 -988
		mu 0 4 582 583 584 580
		f 4 991 -979 -985 -991
		mu 0 4 584 577 576 580
		f 4 -987 992 993 994
		mu 0 4 582 581 585 586
		f 4 995 -961 996 -994
		mu 0 4 587 565 564 588
		f 4 -972 997 998 -997
		mu 0 4 564 572 589 588
		f 4 999 -989 -995 -999
		mu 0 4 590 583 582 586
		f 4 1000 1001 1002 1003
		mu 0 4 591 592 593 594
		f 4 1004 1005 1006 -1003
		mu 0 4 593 595 596 594
		f 4 1007 1008 1009 -1007
		mu 0 4 596 597 598 599
		f 4 1010 1011 -1004 -1010
		mu 0 4 598 600 601 599;
	setAttr ".fc[500:999]"
		f 4 -998 -971 1012 1013
		mu 0 4 589 572 571 602
		f 4 -969 -982 1014 -1013
		mu 0 4 571 570 578 602
		f 4 -980 -992 1015 -1015
		mu 0 4 578 577 584 603
		f 4 -990 -1000 -1014 -1016
		mu 0 4 584 583 590 603
		f 4 1016 1017 1018 1019
		mu 0 4 604 605 606 607
		f 4 1020 1021 1022 -1019
		mu 0 4 606 608 609 607
		f 4 1023 1024 1025 -1023
		mu 0 4 609 610 611 607
		f 4 1026 1027 -1020 -1026
		mu 0 4 611 612 604 607
		f 4 1028 -1024 1029 1030
		mu 0 4 613 610 609 614
		f 4 -1022 1031 1032 -1030
		mu 0 4 609 608 615 614
		f 4 1033 1034 1035 -1033
		mu 0 4 615 616 617 614
		f 4 1036 1037 -1031 -1036
		mu 0 4 617 618 613 614
		f 4 1038 -1037 1039 1040
		mu 0 4 619 618 617 620
		f 4 -1035 1041 1042 -1040
		mu 0 4 617 616 621 620
		f 4 1043 1044 1045 -1043
		mu 0 4 622 623 624 625
		f 4 1046 1047 -1041 -1046
		mu 0 4 624 626 627 625
		f 4 1048 -1047 1049 1050
		mu 0 4 628 626 624 629
		f 4 -1045 1051 1052 -1050
		mu 0 4 624 623 630 629
		f 4 1053 -1017 1054 -1053
		mu 0 4 630 605 604 629
		f 4 -1028 1055 -1051 -1055
		mu 0 4 604 612 628 629
		f 4 1056 -1027 1057 1058
		mu 0 4 631 612 611 632
		f 4 -1025 1059 1060 -1058
		mu 0 4 611 610 633 632
		f 4 1061 -976 1062 -1061
		mu 0 4 633 575 573 632
		f 4 -973 1063 -1059 -1063
		mu 0 4 573 568 631 632
		f 4 -983 -1062 1064 1065
		mu 0 4 579 575 633 634
		f 4 -1060 -1029 1066 -1065
		mu 0 4 633 610 613 634
		f 4 -1038 1067 1068 -1067
		mu 0 4 613 618 635 634
		f 4 1069 -986 -1066 -1069
		mu 0 4 635 581 579 634
		f 4 -993 -1070 1070 1071
		mu 0 4 585 581 635 636
		f 4 -1068 -1039 1072 -1071
		mu 0 4 635 618 619 636
		f 4 -1048 1073 1074 -1073
		mu 0 4 627 626 637 638
		f 4 1075 -996 -1072 -1075
		mu 0 4 637 565 587 638
		f 4 -962 -1076 1076 1077
		mu 0 4 566 565 637 639
		f 4 -1074 -1049 1078 -1077
		mu 0 4 637 626 628 639
		f 4 -1056 -1057 1079 -1079
		mu 0 4 628 612 631 639
		f 4 -1064 -965 -1078 -1080
		mu 0 4 631 568 566 639
		f 4 1080 -1008 1081 1082
		mu 0 4 640 597 596 641
		f 4 -1006 1083 1084 -1082
		mu 0 4 596 595 642 641
		f 4 1085 -1021 1086 -1085
		mu 0 4 642 608 606 641
		f 4 -1018 1087 -1083 -1087
		mu 0 4 606 605 640 641
		f 4 -1032 -1086 1088 1089
		mu 0 4 615 608 642 643
		f 4 -1084 -1005 1090 -1089
		mu 0 4 642 595 593 643
		f 4 -1002 1091 1092 -1091
		mu 0 4 593 592 644 643
		f 4 1093 -1034 -1090 -1093
		mu 0 4 644 616 615 643
		f 4 -1042 -1094 1094 1095
		mu 0 4 621 616 644 645
		f 4 -1092 -1001 1096 -1095
		mu 0 4 644 592 591 645
		f 4 -1012 1097 1098 -1097
		mu 0 4 601 600 646 647
		f 4 1099 -1044 -1096 -1099
		mu 0 4 646 623 622 647
		f 4 -1052 -1100 1100 1101
		mu 0 4 630 623 646 648
		f 4 -1098 -1011 1102 -1101
		mu 0 4 646 600 598 648
		f 4 -1009 -1081 1103 -1103
		mu 0 4 598 597 640 648
		f 4 -1088 -1054 -1102 -1104
		mu 0 4 640 605 630 648
		f 4 1104 1105 1106 1107
		mu 0 4 649 650 651 652
		f 4 1108 1109 1110 -1107
		mu 0 4 651 653 654 652
		f 4 1111 1112 1113 -1111
		mu 0 4 654 655 656 652
		f 4 1114 1115 -1108 -1114
		mu 0 4 656 657 649 652
		f 4 -1110 1116 1117 1118
		mu 0 4 654 653 658 659
		f 4 1119 1120 1121 -1118
		mu 0 4 658 660 661 659
		f 4 1122 1123 1124 -1122
		mu 0 4 661 662 663 659
		f 4 1125 -1112 -1119 -1125
		mu 0 4 663 655 654 659
		f 4 -1121 1126 1127 1128
		mu 0 4 664 665 666 667
		f 4 1129 1130 1131 -1128
		mu 0 4 666 668 669 667
		f 4 1132 1133 1134 -1132
		mu 0 4 669 670 671 667
		f 4 1135 -1123 -1129 -1135
		mu 0 4 671 672 664 667
		f 4 -1131 1136 1137 1138
		mu 0 4 673 674 675 676
		f 4 1139 -1105 1140 -1138
		mu 0 4 675 650 649 676
		f 4 -1116 1141 1142 -1141
		mu 0 4 649 657 677 676
		f 4 1143 -1133 -1139 -1143
		mu 0 4 677 678 673 676
		f 4 1144 1145 1146 1147
		mu 0 4 679 680 681 682
		f 4 1148 1149 1150 -1147
		mu 0 4 681 683 684 682
		f 4 1151 1152 1153 -1151
		mu 0 4 684 685 686 682
		f 4 1154 1155 -1148 -1154
		mu 0 4 686 687 679 682
		f 4 -1142 -1115 1156 1157
		mu 0 4 677 657 656 688
		f 4 -1113 -1126 1158 -1157
		mu 0 4 656 655 663 688
		f 4 -1124 -1136 1159 -1159
		mu 0 4 663 662 689 688
		f 4 -1134 -1144 -1158 -1160
		mu 0 4 689 678 677 688
		f 4 1160 -1152 1161 1162
		mu 0 4 690 685 684 691
		f 4 -1150 1163 1164 -1162
		mu 0 4 684 683 692 691
		f 4 1165 1166 1167 -1165
		mu 0 4 692 693 694 691
		f 4 1168 1169 -1163 -1168
		mu 0 4 694 695 690 691
		f 4 1170 -1166 1171 1172
		mu 0 4 696 697 698 699
		f 4 -1164 -1149 1173 -1172
		mu 0 4 698 700 701 699
		f 4 -1146 1174 1175 -1174
		mu 0 4 701 702 703 699
		f 4 1176 1177 -1173 -1176
		mu 0 4 703 704 696 699
		f 4 1178 -1177 1179 1180
		mu 0 4 705 706 707 708
		f 4 -1175 -1145 1181 -1180
		mu 0 4 707 680 679 708
		f 4 -1156 1182 1183 -1182
		mu 0 4 679 687 709 708
		f 4 1184 1185 -1181 -1184
		mu 0 4 709 710 705 708
		f 4 1186 -1185 1187 1188
		mu 0 4 711 710 709 712
		f 4 -1183 -1155 1189 -1188
		mu 0 4 709 687 686 712
		f 4 -1153 -1161 1190 -1190
		mu 0 4 686 685 690 712
		f 4 -1170 1191 -1189 -1191
		mu 0 4 690 695 711 712
		f 4 1192 -1169 1193 1194
		mu 0 4 713 695 694 714
		f 4 -1167 1195 1196 -1194
		mu 0 4 694 693 715 714
		f 4 1197 1198 1199 -1197
		mu 0 4 715 716 717 714
		f 4 1200 1201 -1195 -1200
		mu 0 4 717 718 713 714
		f 4 1202 -1198 1203 1204
		mu 0 4 719 720 721 722
		f 4 -1196 -1171 1205 -1204
		mu 0 4 721 697 696 722
		f 4 -1178 1206 1207 -1206
		mu 0 4 696 704 723 722
		f 4 1208 1209 -1205 -1208
		mu 0 4 723 724 719 722
		f 4 1210 -1209 1211 1212
		mu 0 4 725 726 727 728
		f 4 -1207 -1179 1213 -1212
		mu 0 4 727 706 705 728
		f 4 -1186 1214 1215 -1214
		mu 0 4 705 710 729 728
		f 4 1216 1217 -1213 -1216
		mu 0 4 729 730 725 728
		f 4 1218 -1217 1219 1220
		mu 0 4 731 730 729 732
		f 4 -1215 -1187 1221 -1220
		mu 0 4 729 710 711 732
		f 4 -1192 -1193 1222 -1222
		mu 0 4 711 695 713 732
		f 4 -1202 1223 -1221 -1223
		mu 0 4 713 718 731 732
		f 4 1224 -1201 1225 1226
		mu 0 4 733 718 717 734
		f 4 -1199 1227 1228 -1226
		mu 0 4 717 716 735 734
		f 4 1229 -1120 1230 -1229
		mu 0 4 735 660 658 734
		f 4 -1117 1231 -1227 -1231
		mu 0 4 658 653 733 734
		f 4 -1127 -1230 1232 1233
		mu 0 4 666 665 736 737
		f 4 -1228 -1203 1234 -1233
		mu 0 4 736 720 719 737
		f 4 -1210 1235 1236 -1235
		mu 0 4 719 724 738 737
		f 4 1237 -1130 -1234 -1237
		mu 0 4 738 668 666 737
		f 4 -1137 -1238 1238 1239
		mu 0 4 675 674 739 740
		f 4 -1236 -1211 1240 -1239
		mu 0 4 739 726 725 740
		f 4 -1218 1241 1242 -1241
		mu 0 4 725 730 741 740
		f 4 1243 -1140 -1240 -1243
		mu 0 4 741 650 675 740
		f 4 -1106 -1244 1244 1245
		mu 0 4 651 650 741 742
		f 4 -1242 -1219 1246 -1245
		mu 0 4 741 730 731 742
		f 4 -1224 -1225 1247 -1247
		mu 0 4 731 718 733 742
		f 4 -1232 -1109 -1246 -1248
		mu 0 4 733 653 651 742
		f 4 1248 1249 1250 1251
		mu 0 4 743 744 745 746
		f 4 1252 1253 1254 -1251
		mu 0 4 745 747 748 746
		f 4 1255 1256 1257 -1255
		mu 0 4 748 749 750 746
		f 4 1258 1259 -1252 -1258
		mu 0 4 750 751 743 746
		f 4 -1260 1260 1261 1262
		mu 0 4 743 751 752 753
		f 4 1263 1264 1265 -1262
		mu 0 4 752 754 755 753
		f 4 1266 1267 1268 -1266
		mu 0 4 755 756 757 753
		f 4 1269 -1249 -1263 -1269
		mu 0 4 757 744 743 753
		f 4 1270 1271 1272 1273
		mu 0 4 758 759 760 761
		f 4 1274 1275 1276 -1273
		mu 0 4 762 763 764 765
		f 4 1277 1278 1279 -1277
		mu 0 4 764 766 767 765
		f 4 1280 1281 -1274 -1280
		mu 0 4 768 769 758 761
		f 4 1282 1283 1284 1285
		mu 0 4 770 771 772 773
		f 4 1286 1287 1288 -1285
		mu 0 4 774 775 776 777
		f 4 1289 -1275 1290 -1289
		mu 0 4 776 763 762 777
		f 4 -1272 1291 -1286 -1291
		mu 0 4 760 759 770 773
		f 4 1292 1293 1294 1295
		mu 0 4 778 779 780 781
		f 4 1296 1297 1298 -1295
		mu 0 4 782 783 784 785
		f 4 1299 -1287 1300 -1299
		mu 0 4 784 775 774 785
		f 4 -1284 1301 -1296 -1301
		mu 0 4 772 771 778 781
		f 4 1302 1303 1304 1305
		mu 0 4 786 787 788 789
		f 4 1306 1307 1308 -1305
		mu 0 4 790 791 792 793
		f 4 1309 -1297 1310 -1309
		mu 0 4 792 783 782 793
		f 4 -1294 1311 -1306 -1311
		mu 0 4 780 779 786 789
		f 4 -1276 1312 1313 1314
		mu 0 4 764 763 794 795
		f 4 1315 1316 1317 -1314
		mu 0 4 794 796 797 795
		f 4 1318 1319 1320 -1318
		mu 0 4 797 798 799 795
		f 4 1321 -1278 -1315 -1321
		mu 0 4 799 766 764 795
		f 4 -1288 1322 1323 1324
		mu 0 4 776 775 800 801
		f 4 1325 1326 1327 -1324
		mu 0 4 800 802 803 801
		f 4 1328 -1316 1329 -1328
		mu 0 4 803 796 794 801
		f 4 -1313 -1290 -1325 -1330
		mu 0 4 794 763 776 801
		f 4 -1298 1330 1331 1332
		mu 0 4 784 783 804 805
		f 4 1333 1334 1335 -1332
		mu 0 4 804 806 807 805
		f 4 1336 -1326 1337 -1336
		mu 0 4 807 802 800 805
		f 4 -1323 -1300 -1333 -1338
		mu 0 4 800 775 784 805
		f 4 -1308 1338 1339 1340
		mu 0 4 792 791 808 809
		f 4 1341 1342 1343 -1340
		mu 0 4 808 810 811 809
		f 4 1344 -1334 1345 -1344
		mu 0 4 811 806 804 809
		f 4 -1331 -1310 -1341 -1346
		mu 0 4 804 783 792 809
		f 4 -1317 1346 1347 1348
		mu 0 4 797 796 812 813
		f 4 1349 1350 1351 -1348
		mu 0 4 812 814 815 813
		f 4 1352 1353 1354 -1352
		mu 0 4 815 816 817 813
		f 4 1355 -1319 -1349 -1355
		mu 0 4 817 798 797 813
		f 4 -1327 1356 1357 1358
		mu 0 4 803 802 818 819
		f 4 1359 1360 1361 -1358
		mu 0 4 818 820 821 819
		f 4 1362 -1350 1363 -1362
		mu 0 4 821 814 812 819
		f 4 -1347 -1329 -1359 -1364
		mu 0 4 812 796 803 819
		f 4 -1335 1364 1365 1366
		mu 0 4 807 806 822 823
		f 4 1367 1368 1369 -1366
		mu 0 4 822 824 825 823
		f 4 1370 -1360 1371 -1370
		mu 0 4 825 820 818 823
		f 4 -1357 -1337 -1367 -1372
		mu 0 4 818 802 807 823
		f 4 -1343 1372 1373 1374
		mu 0 4 811 810 826 827
		f 4 1375 1376 1377 -1374
		mu 0 4 826 828 829 827
		f 4 1378 -1368 1379 -1378
		mu 0 4 829 824 822 827
		f 4 -1365 -1345 -1375 -1380
		mu 0 4 822 806 811 827
		f 4 -1351 1380 1381 1382
		mu 0 4 815 814 830 831
		f 4 1383 1384 1385 -1382
		mu 0 4 830 832 833 831
		f 4 1386 1387 1388 -1386
		mu 0 4 833 834 835 831
		f 4 1389 -1353 -1383 -1389
		mu 0 4 835 816 815 831
		f 4 -1361 1390 1391 1392
		mu 0 4 821 820 836 837
		f 4 1393 1394 1395 -1392
		mu 0 4 836 838 839 837
		f 4 1396 -1384 1397 -1396
		mu 0 4 839 832 830 837
		f 4 -1381 -1363 -1393 -1398
		mu 0 4 830 814 821 837
		f 4 -1369 1398 1399 1400
		mu 0 4 825 824 840 841
		f 4 1401 1402 1403 -1400
		mu 0 4 840 842 843 841
		f 4 1404 -1394 1405 -1404
		mu 0 4 843 838 836 841
		f 4 -1391 -1371 -1401 -1406
		mu 0 4 836 820 825 841
		f 4 -1377 1406 1407 1408
		mu 0 4 829 828 844 845
		f 4 1409 1410 1411 -1408
		mu 0 4 844 846 847 845
		f 4 1412 -1402 1413 -1412
		mu 0 4 847 842 840 845
		f 4 -1399 -1379 -1409 -1414
		mu 0 4 840 824 829 845
		f 4 -1411 1414 1415 1416
		mu 0 4 847 846 848 849
		f 4 1417 -1267 1418 -1416
		mu 0 4 850 756 755 851
		f 4 -1265 1419 1420 -1419
		mu 0 4 755 754 852 851
		f 4 1421 -1413 -1417 -1421
		mu 0 4 853 842 847 849
		f 4 -1403 -1422 1422 1423
		mu 0 4 843 842 853 854
		f 4 -1420 -1264 1424 -1423
		mu 0 4 852 754 752 855
		f 4 -1261 1425 1426 -1425
		mu 0 4 752 751 856 855
		f 4 1427 -1405 -1424 -1427
		mu 0 4 857 838 843 854
		f 4 -1395 -1428 1428 1429
		mu 0 4 839 838 857 858
		f 4 -1426 -1259 1430 -1429
		mu 0 4 856 751 750 859
		f 4 -1257 1431 1432 -1431
		mu 0 4 750 749 860 859
		f 4 1433 -1397 -1430 -1433
		mu 0 4 861 832 839 858
		f 4 -1385 -1434 1434 1435
		mu 0 4 833 832 861 862
		f 4 -1432 -1256 1436 -1435
		mu 0 4 860 749 748 863
		f 4 -1254 1437 1438 -1437
		mu 0 4 748 747 864 863
		f 4 1439 -1387 -1436 -1439
		mu 0 4 865 834 833 862
		f 4 1440 -1303 1441 1442
		mu 0 4 866 787 786 867
		f 4 -1312 1443 1444 -1442
		mu 0 4 786 779 868 867
		f 4 1445 1446 1447 -1445
		mu 0 4 868 869 870 867
		f 4 1448 1449 -1443 -1448
		mu 0 4 870 871 866 867
		f 4 -1282 1450 1451 1452
		mu 0 4 758 769 872 873
		f 4 1453 1454 1455 -1452
		mu 0 4 872 874 875 873
		f 4 1456 1457 1458 -1456
		mu 0 4 875 876 877 873
		f 4 1459 -1271 -1453 -1459
		mu 0 4 877 759 758 873
		f 4 -1302 1460 1461 1462
		mu 0 4 778 771 878 879
		f 4 1463 1464 1465 -1462
		mu 0 4 878 880 881 879
		f 4 1466 -1446 1467 -1466
		mu 0 4 881 869 868 879
		f 4 -1444 -1293 -1463 -1468
		mu 0 4 868 779 778 879
		f 4 -1292 -1460 1468 1469
		mu 0 4 770 759 877 882
		f 4 -1458 1470 1471 -1469
		mu 0 4 877 876 883 882
		f 4 1472 -1464 1473 -1472
		mu 0 4 883 880 878 882
		f 4 -1461 -1283 -1470 -1474
		mu 0 4 878 771 770 882
		f 4 1474 1475 1476 1477
		mu 0 4 884 885 886 887
		f 4 1478 1479 1480 -1477
		mu 0 4 886 888 889 887
		f 4 1481 1482 1483 -1481
		mu 0 4 889 890 891 887
		f 4 1484 1485 -1478 -1484
		mu 0 4 891 892 884 887
		f 4 1486 1487 1488 1489
		mu 0 4 893 894 895 896
		f 4 1490 -1479 1491 -1489
		mu 0 4 895 888 886 896
		f 4 -1476 1492 1493 -1492
		mu 0 4 886 885 897 896
		f 4 1494 1495 -1490 -1494
		mu 0 4 897 898 893 896
		f 4 1496 -1487 1497 1498
		mu 0 4 899 894 893 900
		f 4 -1496 1499 1500 -1498
		mu 0 4 893 898 901 900
		f 4 1501 1502 1503 -1501
		mu 0 4 901 902 903 900
		f 4 1504 1505 -1499 -1504
		mu 0 4 903 904 899 900
		f 4 1506 1507 1508 1509
		mu 0 4 905 906 907 908
		f 4 1510 1511 1512 -1509
		mu 0 4 907 909 910 908
		f 4 1513 -1449 1514 -1513
		mu 0 4 910 871 870 908
		f 4 -1447 1515 -1510 -1515
		mu 0 4 870 869 905 908
		f 4 1516 1517 1518 1519
		mu 0 4 911 912 913 914
		f 4 1520 1521 1522 -1519
		mu 0 4 913 915 916 914
		f 4 1523 1524 1525 -1523
		mu 0 4 916 917 918 914
		f 4 1526 1527 -1520 -1526
		mu 0 4 918 919 911 914
		f 4 1528 1529 1530 1531
		mu 0 4 920 921 922 923
		f 4 1532 -1511 1533 -1531
		mu 0 4 922 909 907 923
		f 4 -1508 1534 1535 -1534
		mu 0 4 907 906 924 923
		f 4 1536 1537 -1532 -1536
		mu 0 4 924 925 920 923
		f 4 -1503 1538 1539 1540
		mu 0 4 903 902 926 927
		f 4 1541 -1529 1542 -1540
		mu 0 4 926 921 920 927
		f 4 -1538 1543 1544 -1543
		mu 0 4 920 925 928 927
		f 4 1545 -1505 -1541 -1545
		mu 0 4 928 904 903 927
		f 4 -1535 1546 1547 1548
		mu 0 4 924 906 929 930
		f 4 1549 1550 1551 -1548
		mu 0 4 929 931 932 930
		f 4 1552 1553 1554 -1552
		mu 0 4 932 933 934 930
		f 4 1555 -1537 -1549 -1555
		mu 0 4 934 925 924 930
		f 4 1556 -1553 1557 1558
		mu 0 4 935 936 937 938
		f 4 -1551 1559 1560 -1558
		mu 0 4 937 939 940 938
		f 4 1561 -1527 1562 -1561
		mu 0 4 940 919 918 938
		f 4 -1525 1563 -1559 -1563
		mu 0 4 918 917 935 938
		f 4 1564 -1557 1565 1566
		mu 0 4 941 936 935 942
		f 4 -1564 1567 1568 -1566
		mu 0 4 935 917 943 942
		f 4 1569 1570 1571 -1569
		mu 0 4 943 944 945 942
		f 4 1572 1573 -1567 -1572
		mu 0 4 945 946 941 942
		f 4 -1544 -1556 1574 1575
		mu 0 4 928 925 934 947
		f 4 -1554 -1565 1576 -1575
		mu 0 4 934 933 948 947
		f 4 -1574 1577 1578 -1577
		mu 0 4 948 949 950 947
		f 4 1579 -1546 -1576 -1579
		mu 0 4 950 904 928 947
		f 4 1580 -1570 1581 1582
		mu 0 4 951 944 943 952
		f 4 -1568 -1524 1583 -1582
		mu 0 4 943 917 916 952
		f 4 -1522 1584 1585 -1584
		mu 0 4 916 915 953 952
		f 4 1586 1587 -1583 -1586
		mu 0 4 953 954 951 952
		f 4 1588 1589 1590 1591
		mu 0 4 955 956 957 958
		f 4 1592 1593 1594 -1591
		mu 0 4 957 959 960 958
		f 4 1595 -1457 1596 -1595
		mu 0 4 960 876 875 958
		f 4 -1455 1597 -1592 -1597
		mu 0 4 875 874 955 958
		f 4 1598 -1473 1599 1600
		mu 0 4 961 880 883 962
		f 4 -1471 -1596 1601 -1600
		mu 0 4 883 876 960 962
		f 4 -1594 1602 1603 -1602
		mu 0 4 960 959 963 962
		f 4 1604 1605 -1601 -1604
		mu 0 4 963 964 961 962
		f 4 1606 -1485 1607 1608
		mu 0 4 965 892 891 966
		f 4 -1483 1609 1610 -1608
		mu 0 4 891 890 967 966
		f 4 1611 -1593 1612 -1611
		mu 0 4 967 959 957 966
		f 4 -1590 1613 -1609 -1613
		mu 0 4 957 956 965 966
		f 4 1614 -1550 1615 1616
		mu 0 4 968 931 929 969
		f 4 -1547 1617 1618 -1616
		mu 0 4 929 906 970 969
		f 4 1619 1620 1621 -1619
		mu 0 4 970 964 971 969
		f 4 1622 1623 -1617 -1622
		mu 0 4 971 972 968 969
		f 4 -1465 -1599 1624 1625
		mu 0 4 881 880 961 973
		f 4 -1606 -1620 1626 -1625
		mu 0 4 961 964 970 973
		f 4 -1618 -1507 1627 -1627
		mu 0 4 970 906 905 973
		f 4 -1516 -1467 -1626 -1628
		mu 0 4 905 869 881 973
		f 4 1628 1629 1630 1631
		mu 0 4 974 975 976 977
		f 4 1632 -1623 1633 -1631
		mu 0 4 976 972 971 977
		f 4 -1621 -1605 1634 -1634
		mu 0 4 971 964 963 977
		f 4 -1603 1635 -1632 -1635
		mu 0 4 963 959 974 977
		f 4 1636 1637 1638 1639
		mu 0 4 978 979 980 981
		f 4 1640 1641 1642 -1639
		mu 0 4 980 982 983 981
		f 4 1643 1644 1645 -1643
		mu 0 4 983 984 985 981
		f 4 1646 1647 -1640 -1646
		mu 0 4 985 986 978 981
		f 4 1648 1649 1650 1651
		mu 0 4 987 988 989 990
		f 4 1652 1653 1654 -1651
		mu 0 4 989 991 992 990
		f 4 1655 1656 1657 -1655
		mu 0 4 992 993 994 990
		f 4 1658 1659 -1652 -1658
		mu 0 4 994 995 987 990
		f 4 -1560 -1615 1660 1661
		mu 0 4 940 939 996 997
		f 4 -1624 1662 1663 -1661
		mu 0 4 996 998 999 997
		f 4 1664 1665 1666 -1664
		mu 0 4 999 1000 1001 997
		f 4 1667 -1562 -1662 -1667
		mu 0 4 1001 919 940 997
		f 4 -1528 -1668 1668 1669
		mu 0 4 911 919 1001 1002
		f 4 -1666 -1644 1670 -1669
		mu 0 4 1001 1000 1003 1002
		f 4 -1642 1671 1672 -1671
		mu 0 4 1003 1004 1005 1002
		f 4 1673 -1517 -1670 -1673
		mu 0 4 1005 912 911 1002
		f 4 1674 -1647 1675 1676
		mu 0 4 1006 986 985 1007
		f 4 -1645 -1665 1677 -1676
		mu 0 4 985 984 1008 1007
		f 4 -1663 -1633 1678 -1678
		mu 0 4 1008 1009 1010 1007
		f 4 -1630 1679 -1677 -1679
		mu 0 4 1010 1011 1006 1007
		f 4 1680 -1482 1681 1682
		mu 0 4 1012 890 889 1013
		f 4 -1480 1683 1684 -1682
		mu 0 4 889 888 1014 1013
		f 4 1685 1686 1687 -1685
		mu 0 4 1014 1015 1016 1013
		f 4 1688 1689 -1683 -1688
		mu 0 4 1016 1017 1012 1013
		f 4 -1588 1690 1691 1692
		mu 0 4 951 954 1018 1019
		f 4 1693 1694 1695 -1692
		mu 0 4 1018 1020 1021 1019
		f 4 1696 1697 1698 -1696
		mu 0 4 1021 1022 1023 1019
		f 4 1699 -1581 -1693 -1699
		mu 0 4 1023 944 951 1019
		f 4 -1684 -1491 1700 1701
		mu 0 4 1014 888 895 1024
		f 4 -1488 1702 1703 -1701
		mu 0 4 895 894 1025 1024
		f 4 1704 1705 1706 -1704
		mu 0 4 1025 1026 1027 1024
		f 4 1707 -1686 -1702 -1707
		mu 0 4 1027 1015 1014 1024
		f 4 1708 -1705 1709 1710
		mu 0 4 1028 1026 1025 1029
		f 4 -1703 -1497 1711 -1710
		mu 0 4 1025 894 899 1029
		f 4 -1506 -1580 1712 -1712
		mu 0 4 899 904 950 1029
		f 4 -1578 1713 -1711 -1713
		mu 0 4 950 949 1028 1029
		f 4 -1714 -1573 1714 1715
		mu 0 4 1030 946 945 1031
		f 4 -1571 -1700 1716 -1715
		mu 0 4 945 944 1023 1031
		f 4 -1698 1717 1718 -1717
		mu 0 4 1023 1022 1032 1031
		f 4 1719 -1709 -1716 -1719
		mu 0 4 1032 1033 1030 1031
		f 4 -1636 -1612 1720 1721
		mu 0 4 974 959 967 1034
		f 4 -1610 -1681 1722 -1721
		mu 0 4 967 890 1012 1034
		f 4 -1690 1723 1724 -1723
		mu 0 4 1012 1017 1035 1034
		f 4 1725 -1629 -1722 -1725
		mu 0 4 1035 975 974 1034
		f 4 1726 1727 1728 1729
		mu 0 4 1036 1037 1038 1039
		f 4 1730 1731 1732 -1729
		mu 0 4 1038 1040 1041 1039
		f 4 1733 1734 1735 -1733
		mu 0 4 1041 995 1042 1039
		f 4 1736 1737 -1730 -1736
		mu 0 4 1042 1043 1036 1039
		f 4 1738 1739 1740 1741
		mu 0 4 1044 1045 1046 1047
		f 4 1742 1743 1744 -1741
		mu 0 4 1046 1048 1049 1047
		f 4 1745 1746 1747 -1745
		mu 0 4 1049 1050 1051 1047
		f 4 1748 1749 -1742 -1748
		mu 0 4 1051 1052 1044 1047
		f 4 1750 -1689 1751 1752
		mu 0 4 1053 1054 1055 1056
		f 4 -1687 1753 1754 -1752
		mu 0 4 1055 1057 1058 1056
		f 4 1755 -1749 1756 -1755
		mu 0 4 1058 1052 1051 1056
		f 4 -1747 1757 -1753 -1757
		mu 0 4 1051 1050 1053 1056
		f 4 -1657 1758 1759 1760
		mu 0 4 994 993 1059 1060
		f 4 1761 1762 1763 -1760
		mu 0 4 1059 1061 1062 1060
		f 4 1764 -1737 1765 -1764
		mu 0 4 1062 1043 1042 1060
		f 4 -1735 -1659 -1761 -1766
		mu 0 4 1042 995 994 1060
		f 4 -1724 -1751 1766 1767
		mu 0 4 1063 1054 1053 1064
		f 4 -1758 1768 1769 -1767
		mu 0 4 1053 1050 1065 1064
		f 4 1770 -1675 1771 -1770
		mu 0 4 1065 986 1006 1064
		f 4 -1680 -1726 -1768 -1772
		mu 0 4 1006 1011 1063 1064
		f 4 -1732 1772 1773 1774
		mu 0 4 1041 1040 1066 1067
		f 4 1775 1776 1777 -1774
		mu 0 4 1066 1068 1069 1067
		f 4 1778 -1649 1779 -1778
		mu 0 4 1069 988 987 1067
		f 4 -1660 -1734 -1775 -1780
		mu 0 4 987 995 1041 1067
		f 4 1780 -1637 1781 1782
		mu 0 4 1070 979 978 1071
		f 4 -1648 -1771 1783 -1782
		mu 0 4 978 986 1065 1071
		f 4 -1769 -1746 1784 -1784
		mu 0 4 1065 1050 1049 1071
		f 4 -1744 1785 -1783 -1785
		mu 0 4 1049 1048 1070 1071
		f 4 -1754 -1708 1786 1787
		mu 0 4 1058 1057 1072 1073
		f 4 -1706 -1720 1788 -1787
		mu 0 4 1072 1033 1032 1073
		f 4 -1718 1789 1790 -1789
		mu 0 4 1032 1022 1074 1073
		f 4 1791 -1756 -1788 -1791
		mu 0 4 1074 1052 1058 1073
		f 4 -1750 -1792 1792 1793
		mu 0 4 1044 1052 1074 1075
		f 4 -1790 -1697 1794 -1793
		mu 0 4 1074 1022 1021 1075
		f 4 -1695 1795 1796 -1795
		mu 0 4 1021 1020 1076 1075
		f 4 1797 -1739 -1794 -1797
		mu 0 4 1076 1045 1044 1075
		f 4 1798 1799 1800 1801
		mu 0 4 1077 1078 1079 1080
		f 4 1802 -1253 1803 -1801
		mu 0 4 1079 747 745 1080
		f 4 -1250 1804 1805 -1804
		mu 0 4 745 744 1081 1080
		f 4 1806 1807 -1802 -1806
		mu 0 4 1081 1082 1077 1080
		f 4 -1268 1808 1809 1810
		mu 0 4 757 756 1083 1084
		f 4 1811 1812 1813 -1810
		mu 0 4 1083 1085 1086 1084
		f 4 1814 -1807 1815 -1814
		mu 0 4 1086 1082 1081 1084
		f 4 -1805 -1270 -1811 -1816
		mu 0 4 1081 744 757 1084
		f 4 -1279 1816 1817 1818
		mu 0 4 1087 1088 1089 1090
		f 4 1819 1820 1821 -1818
		mu 0 4 1089 1091 1092 1090
		f 4 1822 1823 1824 -1822
		mu 0 4 1093 1094 1095 1096
		f 4 1825 -1281 -1819 -1825
		mu 0 4 1095 769 768 1096
		f 4 -1821 1826 1827 1828
		mu 0 4 1092 1091 1097 1098
		f 4 1829 1830 1831 -1828
		mu 0 4 1097 1099 1100 1098
		f 4 1832 1833 1834 -1832
		mu 0 4 1101 1102 1103 1104
		f 4 1835 -1823 -1829 -1835
		mu 0 4 1103 1094 1093 1104
		f 4 -1831 1836 1837 1838
		mu 0 4 1100 1099 1105 1106
		f 4 1839 1840 1841 -1838
		mu 0 4 1105 1107 1108 1106
		f 4 1842 1843 1844 -1842
		mu 0 4 1109 1110 1111 1112
		f 4 1845 -1833 -1839 -1845
		mu 0 4 1111 1102 1101 1112
		f 4 -1841 1846 1847 1848
		mu 0 4 1108 1107 1113 1114
		f 4 1849 -1307 1850 -1848
		mu 0 4 1113 791 790 1114
		f 4 -1304 1851 1852 -1851
		mu 0 4 1115 1116 1117 1118
		f 4 1853 -1843 -1849 -1853
		mu 0 4 1117 1110 1109 1118
		f 4 -1320 1854 1855 1856
		mu 0 4 1119 1120 1121 1122
		f 4 1857 1858 1859 -1856
		mu 0 4 1121 1123 1124 1122
		f 4 1860 -1820 1861 -1860
		mu 0 4 1124 1091 1089 1122
		f 4 -1817 -1322 -1857 -1862
		mu 0 4 1089 1088 1119 1122
		f 4 -1859 1862 1863 1864
		mu 0 4 1124 1123 1125 1126
		f 4 1865 1866 1867 -1864
		mu 0 4 1125 1127 1128 1126
		f 4 1868 -1830 1869 -1868
		mu 0 4 1128 1099 1097 1126
		f 4 -1827 -1861 -1865 -1870
		mu 0 4 1097 1091 1124 1126
		f 4 -1867 1870 1871 1872
		mu 0 4 1128 1127 1129 1130
		f 4 1873 1874 1875 -1872
		mu 0 4 1129 1131 1132 1130
		f 4 1876 -1840 1877 -1876
		mu 0 4 1132 1107 1105 1130
		f 4 -1837 -1869 -1873 -1878
		mu 0 4 1105 1099 1128 1130
		f 4 -1875 1878 1879 1880
		mu 0 4 1132 1131 1133 1134
		f 4 1881 -1342 1882 -1880
		mu 0 4 1133 810 808 1134
		f 4 -1339 -1850 1883 -1883
		mu 0 4 808 791 1113 1134
		f 4 -1847 -1877 -1881 -1884
		mu 0 4 1113 1107 1132 1134
		f 4 -1354 1884 1885 1886
		mu 0 4 1135 1136 1137 1138
		f 4 1887 1888 1889 -1886
		mu 0 4 1137 1139 1140 1138
		f 4 1890 -1858 1891 -1890
		mu 0 4 1140 1123 1121 1138
		f 4 -1855 -1356 -1887 -1892
		mu 0 4 1121 1120 1135 1138
		f 4 -1889 1892 1893 1894
		mu 0 4 1140 1139 1141 1142
		f 4 1895 1896 1897 -1894
		mu 0 4 1141 1143 1144 1142
		f 4 1898 -1866 1899 -1898
		mu 0 4 1144 1127 1125 1142
		f 4 -1863 -1891 -1895 -1900
		mu 0 4 1125 1123 1140 1142
		f 4 -1897 1900 1901 1902
		mu 0 4 1144 1143 1145 1146
		f 4 1903 1904 1905 -1902
		mu 0 4 1145 1147 1148 1146
		f 4 1906 -1874 1907 -1906
		mu 0 4 1148 1131 1129 1146
		f 4 -1871 -1899 -1903 -1908
		mu 0 4 1129 1127 1144 1146
		f 4 -1905 1908 1909 1910
		mu 0 4 1148 1147 1149 1150
		f 4 1911 -1376 1912 -1910
		mu 0 4 1149 828 826 1150
		f 4 -1373 -1882 1913 -1913
		mu 0 4 826 810 1133 1150
		f 4 -1879 -1907 -1911 -1914
		mu 0 4 1133 1131 1148 1150
		f 4 -1388 1914 1915 1916
		mu 0 4 1151 1152 1153 1154
		f 4 1917 1918 1919 -1916
		mu 0 4 1153 1155 1156 1154
		f 4 1920 -1888 1921 -1920
		mu 0 4 1156 1139 1137 1154
		f 4 -1885 -1390 -1917 -1922
		mu 0 4 1137 1136 1151 1154
		f 4 -1919 1922 1923 1924
		mu 0 4 1156 1155 1157 1158
		f 4 1925 1926 1927 -1924
		mu 0 4 1157 1159 1160 1158
		f 4 1928 -1896 1929 -1928
		mu 0 4 1160 1143 1141 1158
		f 4 -1893 -1921 -1925 -1930
		mu 0 4 1141 1139 1156 1158
		f 4 -1927 1930 1931 1932
		mu 0 4 1160 1159 1161 1162
		f 4 1933 1934 1935 -1932
		mu 0 4 1161 1163 1164 1162
		f 4 1936 -1904 1937 -1936
		mu 0 4 1164 1147 1145 1162
		f 4 -1901 -1929 -1933 -1938
		mu 0 4 1145 1143 1160 1162
		f 4 -1935 1938 1939 1940
		mu 0 4 1164 1163 1165 1166
		f 4 1941 -1410 1942 -1940
		mu 0 4 1165 846 844 1166
		f 4 -1407 -1912 1943 -1943
		mu 0 4 844 828 1149 1166
		f 4 -1909 -1937 -1941 -1944
		mu 0 4 1149 1147 1164 1166
		f 4 1944 -1812 1945 1946
		mu 0 4 1167 1085 1083 1168
		f 4 -1809 -1418 1947 -1946
		mu 0 4 1083 756 850 1168
		f 4 -1415 -1942 1948 -1948
		mu 0 4 848 846 1165 1169
		f 4 -1939 1949 -1947 -1949
		mu 0 4 1165 1163 1170 1169
		f 4 1950 -1815 1951 1952
		mu 0 4 1171 1082 1086 1172
		f 4 -1813 -1945 1953 -1952
		mu 0 4 1086 1085 1167 1172
		f 4 -1950 -1934 1954 -1954
		mu 0 4 1170 1163 1161 1173
		f 4 -1931 1955 -1953 -1955
		mu 0 4 1161 1159 1174 1173
		f 4 1956 -1799 1957 1958
		mu 0 4 1175 1078 1077 1176
		f 4 -1808 -1951 1959 -1958
		mu 0 4 1077 1082 1171 1176
		f 4 -1956 -1926 1960 -1960
		mu 0 4 1174 1159 1157 1177
		f 4 -1923 1961 -1959 -1961
		mu 0 4 1157 1155 1178 1177
		f 4 -1438 -1803 1962 1963
		mu 0 4 864 747 1079 1179
		f 4 -1800 -1957 1964 -1963
		mu 0 4 1079 1078 1175 1179
		f 4 -1962 -1918 1965 -1965
		mu 0 4 1178 1155 1153 1180
		f 4 -1915 -1440 -1964 -1966
		mu 0 4 1153 1152 1181 1180
		f 4 1966 1967 1968 1969
		mu 0 4 1182 1183 1184 1185
		f 4 1970 -1854 1971 -1969
		mu 0 4 1184 1110 1117 1185
		f 4 -1852 -1441 1972 -1972
		mu 0 4 1117 1116 1186 1185
		f 4 -1450 1973 -1970 -1973
		mu 0 4 1186 1187 1182 1185
		f 4 1974 1975 1976 1977
		mu 0 4 1188 1189 1190 1191
		f 4 1978 -1454 1979 -1977
		mu 0 4 1190 874 872 1191
		f 4 -1451 -1826 1980 -1980
		mu 0 4 872 769 1095 1191
		f 4 -1824 1981 -1978 -1981
		mu 0 4 1095 1094 1188 1191
		f 4 -1968 1982 1983 1984
		mu 0 4 1184 1183 1192 1193
		f 4 1985 1986 1987 -1984
		mu 0 4 1192 1194 1195 1193
		f 4 1988 -1846 1989 -1988
		mu 0 4 1195 1102 1111 1193
		f 4 -1844 -1971 -1985 -1990
		mu 0 4 1111 1110 1184 1193
		f 4 -1987 1990 1991 1992
		mu 0 4 1195 1194 1196 1197
		f 4 1993 -1975 1994 -1992
		mu 0 4 1196 1189 1188 1197
		f 4 -1982 -1836 1995 -1995
		mu 0 4 1188 1094 1103 1197
		f 4 -1834 -1989 -1993 -1996
		mu 0 4 1103 1102 1195 1197
		f 4 1996 1997 1998 1999
		mu 0 4 1198 1199 1200 1201
		f 4 2000 2001 2002 -1999
		mu 0 4 1200 1202 1203 1201
		f 4 2003 -1475 2004 -2003
		mu 0 4 1203 885 884 1201
		f 4 -1486 2005 -2000 -2005
		mu 0 4 884 892 1198 1201
		f 4 -1493 -2004 2006 2007
		mu 0 4 1204 885 1203 1205
		f 4 -2002 2008 2009 -2007
		mu 0 4 1203 1202 1206 1205
		f 4 2010 2011 2012 -2010
		mu 0 4 1206 1207 1208 1205
		f 4 2013 -1495 -2008 -2013
		mu 0 4 1208 1209 1204 1205
		f 4 2014 -1502 2015 2016
		mu 0 4 1210 1211 1212 1213
		f 4 -1500 -2014 2017 -2016
		mu 0 4 1212 1209 1208 1213
		f 4 -2012 2018 2019 -2018
		mu 0 4 1208 1207 1214 1213
		f 4 2020 2021 -2017 -2020
		mu 0 4 1214 1215 1210 1213
		f 4 -1974 -1514 2022 2023
		mu 0 4 1182 1187 1216 1217
		f 4 -1512 2024 2025 -2023
		mu 0 4 1216 1218 1219 1217
		f 4 2026 2027 2028 -2026
		mu 0 4 1219 1220 1221 1217
		f 4 2029 -1967 -2024 -2029
		mu 0 4 1221 1183 1182 1217;
	setAttr ".fc[1000:1499]"
		f 4 2030 2031 2032 2033
		mu 0 4 1222 1223 1224 1225
		f 4 2034 2035 2036 -2033
		mu 0 4 1224 1226 1227 1225
		f 4 2037 2038 2039 -2037
		mu 0 4 1227 1228 1229 1225
		f 4 2040 2041 -2034 -2040
		mu 0 4 1229 1230 1222 1225
		f 4 2042 -2027 2043 2044
		mu 0 4 1231 1220 1219 1232
		f 4 -2025 -1533 2045 -2044
		mu 0 4 1219 1218 1233 1232
		f 4 -1530 2046 2047 -2046
		mu 0 4 1233 1234 1235 1232
		f 4 2048 2049 -2045 -2048
		mu 0 4 1235 1236 1231 1232
		f 4 2050 -2049 2051 2052
		mu 0 4 1237 1236 1235 1238
		f 4 -2047 -1542 2053 -2052
		mu 0 4 1235 1234 1239 1238
		f 4 -1539 -2015 2054 -2054
		mu 0 4 1239 1211 1210 1238
		f 4 -2022 2055 -2053 -2055
		mu 0 4 1210 1215 1237 1238
		f 4 2056 2057 2058 2059
		mu 0 4 1240 1241 1242 1243
		f 4 2060 2061 2062 -2059
		mu 0 4 1242 1244 1245 1243
		f 4 2063 -2043 2064 -2063
		mu 0 4 1245 1220 1231 1243
		f 4 -2050 2065 -2060 -2065
		mu 0 4 1231 1236 1240 1243
		f 4 -2042 2066 2067 2068
		mu 0 4 1222 1230 1246 1247
		f 4 2069 -2061 2070 -2068
		mu 0 4 1246 1248 1249 1247
		f 4 -2058 2071 2072 -2071
		mu 0 4 1249 1250 1251 1247
		f 4 2073 -2031 -2069 -2073
		mu 0 4 1251 1223 1222 1247
		f 4 2074 2075 2076 2077
		mu 0 4 1252 1253 1254 1255
		f 4 2078 -2074 2079 -2077
		mu 0 4 1254 1223 1251 1255
		f 4 -2072 2080 2081 -2080
		mu 0 4 1251 1250 1256 1255
		f 4 2082 2083 -2078 -2082
		mu 0 4 1256 1257 1252 1255
		f 4 2084 -2083 2085 2086
		mu 0 4 1258 1259 1260 1261
		f 4 -2081 -2057 2087 -2086
		mu 0 4 1260 1241 1240 1261
		f 4 -2066 -2051 2088 -2088
		mu 0 4 1240 1236 1237 1261
		f 4 -2056 2089 -2087 -2089
		mu 0 4 1237 1215 1258 1261
		f 4 2090 -2035 2091 2092
		mu 0 4 1262 1226 1224 1263
		f 4 -2032 -2079 2093 -2092
		mu 0 4 1224 1223 1254 1263
		f 4 -2076 2094 2095 -2094
		mu 0 4 1254 1253 1264 1263
		f 4 2096 2097 -2093 -2096
		mu 0 4 1264 1265 1262 1263
		f 4 -1976 2098 2099 2100
		mu 0 4 1190 1189 1266 1267
		f 4 2101 2102 2103 -2100
		mu 0 4 1266 1268 1269 1267
		f 4 2104 -1589 2105 -2104
		mu 0 4 1269 956 955 1267
		f 4 -1598 -1979 -2101 -2106
		mu 0 4 955 874 1190 1267
		f 4 2106 -2102 2107 2108
		mu 0 4 1270 1268 1266 1271
		f 4 -2099 -1994 2109 -2108
		mu 0 4 1266 1189 1196 1271
		f 4 -1991 2110 2111 -2110
		mu 0 4 1196 1194 1272 1271
		f 4 2112 2113 -2109 -2112
		mu 0 4 1272 1273 1270 1271
		f 4 -2103 2114 2115 2116
		mu 0 4 1269 1268 1274 1275
		f 4 2117 -1997 2118 -2116
		mu 0 4 1274 1199 1198 1275
		f 4 -2006 -1607 2119 -2119
		mu 0 4 1198 892 965 1275
		f 4 -1614 -2105 -2117 -2120
		mu 0 4 965 956 1269 1275
		f 4 2120 2121 2122 2123
		mu 0 4 1276 1273 1277 1278
		f 4 2124 -2064 2125 -2123
		mu 0 4 1277 1220 1245 1278
		f 4 -2062 2126 2127 -2126
		mu 0 4 1245 1244 1279 1278
		f 4 2128 2129 -2124 -2128
		mu 0 4 1279 1280 1276 1278
		f 4 -2028 -2125 2130 2131
		mu 0 4 1221 1220 1277 1281
		f 4 -2122 -2113 2132 -2131
		mu 0 4 1277 1273 1272 1281
		f 4 -2111 -1986 2133 -2133
		mu 0 4 1272 1194 1192 1281
		f 4 -1983 -2030 -2132 -2134
		mu 0 4 1192 1183 1221 1281
		f 4 -2114 -2121 2134 2135
		mu 0 4 1270 1273 1276 1282
		f 4 -2130 2136 2137 -2135
		mu 0 4 1276 1280 1283 1282
		f 4 2138 2139 2140 -2138
		mu 0 4 1283 1284 1285 1282
		f 4 2141 -2107 -2136 -2141
		mu 0 4 1285 1268 1270 1282
		f 4 2142 2143 2144 2145
		mu 0 4 1286 1287 1288 1289
		f 4 2146 2147 2148 -2145
		mu 0 4 1288 1290 1291 1289
		f 4 2149 2150 2151 -2149
		mu 0 4 1291 1292 1293 1289
		f 4 2152 2153 -2146 -2152
		mu 0 4 1293 1294 1286 1289
		f 4 2154 2155 2156 2157
		mu 0 4 1295 1296 1297 1298
		f 4 2158 2159 2160 -2157
		mu 0 4 1297 1299 1300 1298
		f 4 2161 2162 2163 -2161
		mu 0 4 1300 1301 1302 1298
		f 4 2164 2165 -2158 -2164
		mu 0 4 1302 1303 1295 1298
		f 4 2166 2167 2168 2169
		mu 0 4 1304 1305 1306 1307
		f 4 2170 -2129 2171 -2169
		mu 0 4 1306 1308 1309 1307
		f 4 -2127 -2070 2172 -2172
		mu 0 4 1309 1248 1246 1307
		f 4 -2067 2173 -2170 -2173
		mu 0 4 1246 1230 1304 1307
		f 4 2174 -2147 2175 2176
		mu 0 4 1310 1311 1312 1313
		f 4 -2144 -2167 2177 -2176
		mu 0 4 1312 1305 1304 1313
		f 4 -2174 -2041 2178 -2178
		mu 0 4 1304 1230 1229 1313
		f 4 -2039 2179 -2177 -2179
		mu 0 4 1229 1228 1310 1313
		f 4 -2137 -2171 2180 2181
		mu 0 4 1314 1315 1316 1317
		f 4 -2168 -2143 2182 -2181
		mu 0 4 1316 1287 1286 1317
		f 4 -2154 2183 2184 -2183
		mu 0 4 1286 1294 1318 1317
		f 4 2185 -2139 -2182 -2185
		mu 0 4 1318 1319 1314 1317
		f 4 2186 2187 2188 2189
		mu 0 4 1320 1321 1322 1323
		f 4 2190 -2001 2191 -2189
		mu 0 4 1322 1202 1200 1323
		f 4 -1998 2192 2193 -2192
		mu 0 4 1200 1199 1324 1323
		f 4 2194 2195 -2190 -2194
		mu 0 4 1324 1325 1320 1323
		f 4 2196 2197 2198 2199
		mu 0 4 1326 1327 1328 1329
		f 4 2200 2201 2202 -2199
		mu 0 4 1328 1330 1331 1329
		f 4 2203 -2097 2204 -2203
		mu 0 4 1331 1265 1264 1329
		f 4 -2095 2205 -2200 -2205
		mu 0 4 1264 1253 1326 1329
		f 4 2206 2207 2208 2209
		mu 0 4 1332 1333 1334 1335
		f 4 2210 -2011 2211 -2209
		mu 0 4 1334 1207 1206 1335
		f 4 -2009 -2191 2212 -2212
		mu 0 4 1206 1202 1322 1335
		f 4 -2188 2213 -2210 -2213
		mu 0 4 1322 1321 1332 1335
		f 4 -2090 -2021 2214 2215
		mu 0 4 1258 1215 1214 1336
		f 4 -2019 -2211 2216 -2215
		mu 0 4 1214 1207 1334 1336
		f 4 -2208 2217 2218 -2217
		mu 0 4 1334 1333 1337 1336
		f 4 2219 -2085 -2216 -2219
		mu 0 4 1337 1259 1258 1336
		f 4 2220 -2197 2221 2222
		mu 0 4 1338 1327 1326 1339
		f 4 -2206 -2075 2223 -2222
		mu 0 4 1326 1253 1252 1339
		f 4 -2084 -2220 2224 -2224
		mu 0 4 1252 1257 1340 1339
		f 4 -2218 2225 -2223 -2225
		mu 0 4 1340 1341 1338 1339
		f 4 2226 -2195 2227 2228
		mu 0 4 1342 1325 1324 1343
		f 4 -2193 -2118 2229 -2228
		mu 0 4 1324 1199 1274 1343
		f 4 -2115 -2142 2230 -2230
		mu 0 4 1274 1268 1285 1343
		f 4 -2140 2231 -2229 -2231
		mu 0 4 1285 1284 1342 1343
		f 4 2232 2233 2234 2235
		mu 0 4 1344 1303 1345 1346
		f 4 2236 2237 2238 -2235
		mu 0 4 1345 1347 1348 1346
		f 4 2239 2240 2241 -2239
		mu 0 4 1348 1349 1350 1346
		f 4 2242 2243 -2236 -2242
		mu 0 4 1350 1351 1344 1346
		f 4 2244 2245 2246 2247
		mu 0 4 1352 1353 1354 1355
		f 4 2248 2249 2250 -2247
		mu 0 4 1354 1356 1357 1355
		f 4 2251 2252 2253 -2251
		mu 0 4 1357 1358 1359 1355
		f 4 2254 2255 -2248 -2254
		mu 0 4 1359 1360 1352 1355
		f 4 -2256 2256 2257 2258
		mu 0 4 1352 1360 1361 1362
		f 4 2259 -2187 2260 -2258
		mu 0 4 1361 1363 1364 1362
		f 4 -2196 2261 2262 -2261
		mu 0 4 1364 1365 1366 1362
		f 4 2263 -2245 -2259 -2263
		mu 0 4 1366 1353 1352 1362
		f 4 -2244 2264 2265 2266
		mu 0 4 1344 1351 1367 1368
		f 4 2267 2268 2269 -2266
		mu 0 4 1367 1369 1370 1368
		f 4 2270 -2155 2271 -2270
		mu 0 4 1370 1296 1295 1368
		f 4 -2166 -2233 -2267 -2272
		mu 0 4 1295 1303 1344 1368
		f 4 -2184 2272 2273 2274
		mu 0 4 1318 1294 1371 1372
		f 4 2275 -2264 2276 -2274
		mu 0 4 1371 1353 1366 1372
		f 4 -2262 -2227 2277 -2277
		mu 0 4 1366 1365 1373 1372
		f 4 -2232 -2186 -2275 -2278
		mu 0 4 1373 1319 1318 1372
		f 4 -2163 2278 2279 2280
		mu 0 4 1302 1301 1374 1375
		f 4 2281 2282 2283 -2280
		mu 0 4 1374 1376 1377 1375
		f 4 2284 -2237 2285 -2284
		mu 0 4 1377 1347 1345 1375
		f 4 -2234 -2165 -2281 -2286
		mu 0 4 1345 1303 1302 1375
		f 4 -2246 -2276 2286 2287
		mu 0 4 1354 1353 1371 1378
		f 4 -2273 -2153 2288 -2287
		mu 0 4 1371 1294 1293 1378
		f 4 -2151 2289 2290 -2289
		mu 0 4 1293 1292 1379 1378
		f 4 2291 -2249 -2288 -2291
		mu 0 4 1379 1356 1354 1378
		f 4 2292 -2221 2293 2294
		mu 0 4 1380 1327 1338 1381
		f 4 -2226 -2207 2295 -2294
		mu 0 4 1338 1341 1382 1381
		f 4 -2214 -2260 2296 -2296
		mu 0 4 1382 1363 1361 1381
		f 4 -2257 2297 -2295 -2297
		mu 0 4 1361 1360 1380 1381
		f 4 2298 -2201 2299 2300
		mu 0 4 1383 1330 1328 1384
		f 4 -2198 -2293 2301 -2300
		mu 0 4 1328 1327 1380 1384
		f 4 -2298 -2255 2302 -2302
		mu 0 4 1380 1360 1359 1384
		f 4 -2253 2303 -2301 -2303
		mu 0 4 1359 1358 1383 1384
		f 4 2304 -1587 2305 2306
		mu 0 4 1385 954 953 1386
		f 4 -1585 2307 2308 -2306
		mu 0 4 953 915 1387 1386
		f 4 2309 -1779 2310 -2309
		mu 0 4 1388 988 1069 1389
		f 4 -1777 2311 -2307 -2311
		mu 0 4 1069 1068 1390 1389
		f 4 -1691 -2305 2312 2313
		mu 0 4 1018 954 1385 1391
		f 4 -2312 -1776 2314 -2313
		mu 0 4 1390 1068 1066 1392
		f 4 -1773 2315 2316 -2315
		mu 0 4 1066 1040 1393 1392
		f 4 2317 -1694 -2314 -2317
		mu 0 4 1394 1020 1018 1391
		f 4 2318 -1798 2319 2320
		mu 0 4 1395 1045 1076 1396
		f 4 -1796 -2318 2321 -2320
		mu 0 4 1076 1020 1394 1396
		f 4 -2316 -1731 2322 -2322
		mu 0 4 1393 1040 1038 1397
		f 4 -1728 2323 -2321 -2323
		mu 0 4 1038 1037 1398 1397
		f 4 -1740 -2319 2324 2325
		mu 0 4 1046 1045 1395 1399
		f 4 -2324 -1727 2326 -2325
		mu 0 4 1398 1037 1036 1400
		f 4 -1738 2327 2328 -2327
		mu 0 4 1036 1043 1401 1400
		f 4 2329 -1743 -2326 -2329
		mu 0 4 1402 1048 1046 1399
		f 4 -1763 2330 2331 2332
		mu 0 4 1062 1061 1403 1404
		f 4 2333 -1781 2334 -2332
		mu 0 4 1405 979 1070 1406
		f 4 -1786 -2330 2335 -2335
		mu 0 4 1070 1048 1402 1406
		f 4 -2328 -1765 -2333 -2336
		mu 0 4 1401 1043 1062 1404
		f 4 -1638 -2334 2336 2337
		mu 0 4 980 979 1405 1407
		f 4 -2331 -1762 2338 -2337
		mu 0 4 1403 1061 1059 1408
		f 4 -1759 2339 2340 -2339
		mu 0 4 1059 993 1409 1408
		f 4 2341 -1641 -2338 -2341
		mu 0 4 1410 982 980 1407
		f 4 2342 -1674 2343 2344
		mu 0 4 1411 912 1005 1412
		f 4 -1672 -2342 2345 -2344
		mu 0 4 1005 1004 1413 1412
		f 4 -2340 -1656 2346 -2346
		mu 0 4 1409 993 992 1414
		f 4 -1654 2347 -2345 -2347
		mu 0 4 992 991 1415 1414
		f 4 -1518 -2343 2348 2349
		mu 0 4 913 912 1411 1416
		f 4 -2348 -1653 2350 -2349
		mu 0 4 1415 991 989 1417
		f 4 -1650 -2310 2351 -2351
		mu 0 4 989 988 1388 1417
		f 4 -2308 -1521 -2350 -2352
		mu 0 4 1387 915 913 1416
		f 4 -2036 2352 2353 2354
		mu 0 4 1227 1226 1418 1419
		f 4 2355 -2162 2356 -2354
		mu 0 4 1418 1420 1421 1419
		f 4 -2160 2357 2358 -2357
		mu 0 4 1421 1422 1423 1419
		f 4 2359 -2038 -2355 -2359
		mu 0 4 1423 1228 1227 1419
		f 4 -2156 2360 2361 2362
		mu 0 4 1424 1425 1426 1427
		f 4 2363 -2175 2364 -2362
		mu 0 4 1426 1311 1310 1427
		f 4 -2180 -2360 2365 -2365
		mu 0 4 1310 1228 1423 1427
		f 4 -2358 -2159 -2363 -2366
		mu 0 4 1423 1422 1424 1427
		f 4 -2148 -2364 2366 2367
		mu 0 4 1291 1290 1428 1429
		f 4 -2361 -2271 2368 -2367
		mu 0 4 1428 1430 1431 1429
		f 4 -2269 2369 2370 -2369
		mu 0 4 1431 1432 1433 1429
		f 4 2371 -2150 -2368 -2371
		mu 0 4 1433 1292 1291 1429
		f 4 2372 -2292 2373 2374
		mu 0 4 1434 1356 1379 1435
		f 4 -2290 -2372 2375 -2374
		mu 0 4 1379 1292 1433 1435
		f 4 -2370 -2268 2376 -2376
		mu 0 4 1433 1432 1436 1435
		f 4 -2265 2377 -2375 -2377
		mu 0 4 1436 1437 1434 1435
		f 4 -2250 -2373 2378 2379
		mu 0 4 1357 1356 1434 1438
		f 4 -2378 -2243 2380 -2379
		mu 0 4 1434 1437 1439 1438
		f 4 -2241 2381 2382 -2381
		mu 0 4 1439 1440 1441 1438
		f 4 2383 -2252 -2380 -2383
		mu 0 4 1441 1358 1357 1438
		f 4 -2238 2384 2385 2386
		mu 0 4 1442 1443 1444 1445
		f 4 2387 -2299 2388 -2386
		mu 0 4 1444 1330 1383 1445
		f 4 -2304 -2384 2389 -2389
		mu 0 4 1383 1358 1441 1445
		f 4 -2382 -2240 -2387 -2390
		mu 0 4 1441 1440 1442 1445
		f 4 -2202 -2388 2390 2391
		mu 0 4 1331 1330 1444 1446
		f 4 -2385 -2285 2392 -2391
		mu 0 4 1444 1443 1447 1446
		f 4 -2283 2393 2394 -2393
		mu 0 4 1447 1448 1449 1446
		f 4 2395 -2204 -2392 -2395
		mu 0 4 1449 1265 1331 1446
		f 4 -2279 -2356 2396 2397
		mu 0 4 1450 1420 1418 1451
		f 4 -2353 -2091 2398 -2397
		mu 0 4 1418 1226 1262 1451
		f 4 -2098 -2396 2399 -2399
		mu 0 4 1262 1265 1449 1451
		f 4 -2394 -2282 -2398 -2400
		mu 0 4 1449 1448 1450 1451
		f 4 2400 2401 2402 2403
		mu 0 4 1452 1453 1454 1455
		f 4 2404 2405 2406 -2403
		mu 0 4 1454 1456 1457 1455
		f 4 2407 2408 2409 -2407
		mu 0 4 1457 1458 1459 1455
		f 4 2410 2411 -2404 -2410
		mu 0 4 1459 1460 1452 1455
		f 4 2412 -2411 2413 2414
		mu 0 4 1461 1460 1459 1462
		f 4 -2409 2415 2416 -2414
		mu 0 4 1459 1458 1463 1462
		f 4 2417 2418 2419 -2417
		mu 0 4 1463 1464 1465 1462
		f 4 2420 2421 -2415 -2420
		mu 0 4 1465 1466 1461 1462
		f 4 2422 -2421 2423 2424
		mu 0 4 1467 1468 1469 1470
		f 4 -2419 2425 2426 -2424
		mu 0 4 1469 1471 1472 1470
		f 4 2427 2428 2429 -2427
		mu 0 4 1472 1473 1474 1470
		f 4 2430 2431 -2425 -2430
		mu 0 4 1474 1475 1467 1470
		f 4 2432 -2431 2433 2434
		mu 0 4 1476 1475 1474 1477
		f 4 -2429 2435 2436 -2434
		mu 0 4 1474 1473 1478 1477
		f 4 2437 -2405 2438 -2437
		mu 0 4 1478 1456 1454 1477
		f 4 -2402 2439 -2435 -2439
		mu 0 4 1454 1453 1476 1477
		f 4 -2406 2440 2441 2442
		mu 0 4 1457 1456 1479 1480
		f 4 2443 2444 2445 -2442
		mu 0 4 1479 1481 1482 1480
		f 4 2446 2447 2448 -2446
		mu 0 4 1482 1483 1484 1480
		f 4 2449 -2408 -2443 -2449
		mu 0 4 1484 1458 1457 1480
		f 4 -2416 -2450 2450 2451
		mu 0 4 1463 1458 1484 1485
		f 4 -2448 2452 2453 -2451
		mu 0 4 1484 1483 1486 1485
		f 4 2454 2455 2456 -2454
		mu 0 4 1486 1487 1488 1485
		f 4 2457 -2418 -2452 -2457
		mu 0 4 1488 1464 1463 1485
		f 4 -2426 -2458 2458 2459
		mu 0 4 1472 1471 1489 1490
		f 4 -2456 2460 2461 -2459
		mu 0 4 1489 1491 1492 1490
		f 4 2462 2463 2464 -2462
		mu 0 4 1492 1493 1494 1490
		f 4 2465 -2428 -2460 -2465
		mu 0 4 1494 1473 1472 1490
		f 4 -2436 -2466 2466 2467
		mu 0 4 1478 1473 1494 1495
		f 4 -2464 2468 2469 -2467
		mu 0 4 1494 1493 1496 1495
		f 4 2470 -2444 2471 -2470
		mu 0 4 1496 1481 1479 1495
		f 4 -2441 -2438 -2468 -2472
		mu 0 4 1479 1456 1478 1495
		f 4 -2445 2472 2473 2474
		mu 0 4 1482 1481 1497 1498
		f 4 2475 2476 2477 -2474
		mu 0 4 1497 1499 1500 1498
		f 4 2478 2479 2480 -2478
		mu 0 4 1500 1501 1502 1498
		f 4 2481 -2447 -2475 -2481
		mu 0 4 1502 1483 1482 1498
		f 4 -2453 -2482 2482 2483
		mu 0 4 1486 1483 1502 1503
		f 4 -2480 2484 2485 -2483
		mu 0 4 1502 1501 1504 1503
		f 4 2486 2487 2488 -2486
		mu 0 4 1504 1505 1506 1503
		f 4 2489 -2455 -2484 -2489
		mu 0 4 1506 1487 1486 1503
		f 4 -2461 -2490 2490 2491
		mu 0 4 1492 1491 1507 1508
		f 4 -2488 2492 2493 -2491
		mu 0 4 1507 1509 1510 1508
		f 4 2494 2495 2496 -2494
		mu 0 4 1510 1511 1512 1508
		f 4 2497 -2463 -2492 -2497
		mu 0 4 1512 1493 1492 1508
		f 4 -2469 -2498 2498 2499
		mu 0 4 1496 1493 1512 1513
		f 4 -2496 2500 2501 -2499
		mu 0 4 1512 1511 1514 1513
		f 4 2502 -2476 2503 -2502
		mu 0 4 1514 1499 1497 1513
		f 4 -2473 -2471 -2500 -2504
		mu 0 4 1497 1481 1496 1513
		f 4 -2477 2504 2505 2506
		mu 0 4 1515 1516 1517 1518
		f 4 2507 -2401 2508 -2506
		mu 0 4 1517 1453 1452 1518
		f 4 -2412 2509 2510 -2509
		mu 0 4 1452 1460 1519 1518
		f 4 2511 -2479 -2507 -2511
		mu 0 4 1519 1520 1515 1518
		f 4 -2485 -2512 2512 2513
		mu 0 4 1521 1520 1519 1522
		f 4 -2510 -2413 2514 -2513
		mu 0 4 1519 1460 1461 1522
		f 4 -2422 2515 2516 -2515
		mu 0 4 1461 1466 1523 1522
		f 4 2517 -2487 -2514 -2517
		mu 0 4 1523 1524 1521 1522
		f 4 -2493 -2518 2518 2519
		mu 0 4 1525 1526 1527 1528
		f 4 -2516 -2423 2520 -2519
		mu 0 4 1527 1468 1467 1528
		f 4 -2432 2521 2522 -2521
		mu 0 4 1467 1475 1529 1528
		f 4 2523 -2495 -2520 -2523
		mu 0 4 1529 1530 1525 1528
		f 4 -2501 -2524 2524 2525
		mu 0 4 1531 1530 1529 1532
		f 4 -2522 -2433 2526 -2525
		mu 0 4 1529 1475 1476 1532
		f 4 -2440 -2508 2527 -2527
		mu 0 4 1476 1453 1517 1532
		f 4 -2505 -2503 -2526 -2528
		mu 0 4 1517 1516 1531 1532
		f 4 2528 2529 2530 2531
		mu 0 4 1533 1534 1535 1536
		f 4 2532 2533 2534 -2531
		mu 0 4 1535 1537 1538 1536
		f 4 2535 2536 2537 -2535
		mu 0 4 1538 1539 1540 1536
		f 4 2538 2539 -2532 -2538
		mu 0 4 1540 1541 1533 1536
		f 4 2540 -2539 2541 2542
		mu 0 4 1542 1541 1540 1543
		f 4 -2537 2543 2544 -2542
		mu 0 4 1540 1539 1544 1543
		f 4 2545 2546 2547 -2545
		mu 0 4 1544 1545 1546 1543
		f 4 2548 2549 -2543 -2548
		mu 0 4 1546 1547 1542 1543
		f 4 -2550 2550 2551 2552
		mu 0 4 1542 1547 1548 1549
		f 4 2553 2554 2555 -2552
		mu 0 4 1548 1550 1551 1549
		f 4 2556 2557 2558 -2556
		mu 0 4 1551 1552 1553 1549
		f 4 2559 -2541 -2553 -2559
		mu 0 4 1553 1541 1542 1549
		f 4 -2540 -2560 2560 2561
		mu 0 4 1533 1541 1553 1554
		f 4 -2558 2562 2563 -2561
		mu 0 4 1553 1552 1555 1554
		f 4 2564 2565 2566 -2564
		mu 0 4 1555 1556 1557 1554
		f 4 2567 -2529 -2562 -2567
		mu 0 4 1557 1534 1533 1554
		f 4 2568 2569 2570 2571
		mu 0 4 1558 1559 1560 1561
		f 4 2572 2573 2574 -2571
		mu 0 4 1560 1562 1563 1561
		f 4 2575 2576 2577 -2575
		mu 0 4 1563 1564 1565 1561
		f 4 2578 2579 -2572 -2578
		mu 0 4 1565 1566 1558 1561
		f 4 2580 2581 2582 2583
		mu 0 4 1567 1568 1569 1570
		f 4 2584 2585 2586 -2583
		mu 0 4 1569 1571 1572 1570
		f 4 2587 -2573 2588 -2587
		mu 0 4 1572 1562 1560 1570
		f 4 -2570 2589 -2584 -2589
		mu 0 4 1560 1559 1567 1570
		f 4 2590 2591 2592 2593
		mu 0 4 1573 1574 1575 1576
		f 4 2594 2595 2596 -2593
		mu 0 4 1575 1577 1578 1576
		f 4 2597 -2585 2598 -2597
		mu 0 4 1578 1571 1569 1576
		f 4 -2582 2599 -2594 -2599
		mu 0 4 1569 1568 1573 1576
		f 4 2600 2601 2602 2603
		mu 0 4 1579 1580 1581 1582
		f 4 2604 2605 2606 -2603
		mu 0 4 1581 1583 1584 1582
		f 4 2607 -2595 2608 -2607
		mu 0 4 1584 1577 1575 1582
		f 4 -2592 2609 -2604 -2609
		mu 0 4 1575 1574 1579 1582
		f 4 2610 2611 2612 2613
		mu 0 4 1585 1586 1587 1588
		f 4 2614 2615 2616 -2613
		mu 0 4 1587 1589 1590 1588
		f 4 2617 -2605 2618 -2617
		mu 0 4 1590 1583 1581 1588
		f 4 -2602 2619 -2614 -2619
		mu 0 4 1581 1580 1585 1588
		f 4 2620 2621 2622 2623
		mu 0 4 1591 1592 1593 1594
		f 4 2624 2625 2626 -2623
		mu 0 4 1593 1595 1596 1594
		f 4 2627 -2615 2628 -2627
		mu 0 4 1596 1589 1587 1594
		f 4 -2612 2629 -2624 -2629
		mu 0 4 1587 1586 1591 1594
		f 4 2630 2631 2632 2633
		mu 0 4 1597 1598 1599 1600
		f 4 2634 2635 2636 -2633
		mu 0 4 1599 1601 1602 1600
		f 4 2637 -2625 2638 -2637
		mu 0 4 1602 1595 1593 1600
		f 4 -2622 2639 -2634 -2639
		mu 0 4 1593 1592 1597 1600
		f 4 2640 -2579 2641 2642
		mu 0 4 1603 1566 1565 1604
		f 4 -2577 2643 2644 -2642
		mu 0 4 1565 1564 1605 1604
		f 4 2645 -2635 2646 -2645
		mu 0 4 1605 1601 1599 1604
		f 4 -2632 2647 -2643 -2647
		mu 0 4 1599 1598 1603 1604
		f 4 2648 2649 2650 2651
		mu 0 4 1606 1607 1608 1609
		f 4 2652 2653 2654 -2651
		mu 0 4 1608 1610 1611 1609
		f 4 2655 2656 2657 -2655
		mu 0 4 1611 1612 1613 1609
		f 4 2658 2659 -2652 -2658
		mu 0 4 1613 1614 1606 1609
		f 4 2660 2661 2662 2663
		mu 0 4 1615 1616 1617 1618
		f 4 2664 2665 2666 -2663
		mu 0 4 1617 1619 1620 1618
		f 4 2667 -2653 2668 -2667
		mu 0 4 1620 1610 1608 1618
		f 4 -2650 2669 -2664 -2669
		mu 0 4 1608 1607 1615 1618
		f 4 2670 2671 2672 2673
		mu 0 4 1621 1622 1623 1624
		f 4 2674 2675 2676 -2673
		mu 0 4 1623 1625 1626 1624
		f 4 2677 -2665 2678 -2677
		mu 0 4 1626 1619 1617 1624
		f 4 -2662 2679 -2674 -2679
		mu 0 4 1617 1616 1621 1624
		f 4 2680 2681 2682 2683
		mu 0 4 1627 1628 1629 1630
		f 4 2684 2685 2686 -2683
		mu 0 4 1629 1631 1632 1630
		f 4 2687 -2675 2688 -2687
		mu 0 4 1632 1625 1623 1630
		f 4 -2672 2689 -2684 -2689
		mu 0 4 1623 1622 1627 1630
		f 4 2690 2691 2692 2693
		mu 0 4 1633 1634 1635 1636
		f 4 2694 2695 2696 -2693
		mu 0 4 1635 1637 1638 1636
		f 4 2697 -2685 2698 -2697
		mu 0 4 1638 1631 1629 1636
		f 4 -2682 2699 -2694 -2699
		mu 0 4 1629 1628 1633 1636
		f 4 2700 2701 2702 2703
		mu 0 4 1639 1640 1641 1642
		f 4 2704 2705 2706 -2703
		mu 0 4 1641 1643 1644 1642
		f 4 2707 -2695 2708 -2707
		mu 0 4 1644 1637 1635 1642
		f 4 -2692 2709 -2704 -2709
		mu 0 4 1635 1634 1639 1642
		f 4 2710 2711 2712 2713
		mu 0 4 1645 1646 1647 1648
		f 4 2714 2715 2716 -2713
		mu 0 4 1647 1649 1650 1648
		f 4 2717 -2705 2718 -2717
		mu 0 4 1650 1643 1641 1648
		f 4 -2702 2719 -2714 -2719
		mu 0 4 1641 1640 1645 1648
		f 4 2720 -2659 2721 2722
		mu 0 4 1651 1614 1613 1652
		f 4 -2657 2723 2724 -2722
		mu 0 4 1613 1612 1653 1652
		f 4 2725 -2715 2726 -2725
		mu 0 4 1653 1649 1647 1652
		f 4 -2712 2727 -2723 -2727
		mu 0 4 1647 1646 1651 1652
		f 4 -2610 2728 2729 2730
		mu 0 4 1579 1574 1654 1655
		f 4 2731 2732 2733 -2730
		mu 0 4 1656 1657 1658 1659
		f 4 2734 2735 2736 -2734
		mu 0 4 1660 1661 1662 1663
		f 4 2737 -2601 -2731 -2737
		mu 0 4 1664 1580 1579 1655
		f 4 -2600 2738 2739 2740
		mu 0 4 1573 1568 1665 1666
		f 4 2741 2742 2743 -2740
		mu 0 4 1667 1668 1669 1670
		f 4 2744 -2732 2745 -2744
		mu 0 4 1669 1657 1656 1670
		f 4 -2729 -2591 -2741 -2746
		mu 0 4 1654 1574 1573 1666
		f 4 -2590 2746 2747 2748
		mu 0 4 1567 1559 1671 1672
		f 4 2749 2750 2751 -2748
		mu 0 4 1673 1674 1675 1676
		f 4 2752 -2742 2753 -2752
		mu 0 4 1675 1668 1667 1676
		f 4 -2739 -2581 -2749 -2754
		mu 0 4 1665 1568 1567 1672
		f 4 2754 2755 2756 2757
		mu 0 4 1677 1678 1679 1680
		f 4 2758 -2750 2759 -2757
		mu 0 4 1681 1674 1673 1682
		f 4 -2747 -2569 2760 -2760
		mu 0 4 1671 1559 1558 1683
		f 4 -2580 2761 -2758 -2761
		mu 0 4 1558 1566 1684 1683
		f 4 -2648 2762 2763 2764
		mu 0 4 1603 1598 1685 1686
		f 4 2765 2766 2767 -2764
		mu 0 4 1687 1688 1689 1690
		f 4 2768 -2755 2769 -2768
		mu 0 4 1691 1678 1677 1692
		f 4 -2762 -2641 -2765 -2770
		mu 0 4 1684 1566 1603 1686
		f 4 -2724 2770 2771 2772
		mu 0 4 1653 1612 1693 1694
		f 4 2773 -2568 2774 -2772
		mu 0 4 1693 1534 1557 1694
		f 4 -2566 2775 2776 -2775
		mu 0 4 1557 1556 1695 1694
		f 4 2777 -2726 -2773 -2777
		mu 0 4 1695 1649 1653 1694
		f 4 -2716 -2778 2778 2779
		mu 0 4 1650 1649 1695 1696
		f 4 -2776 -2565 2780 -2779
		mu 0 4 1695 1556 1555 1696
		f 4 -2563 2781 2782 -2781
		mu 0 4 1555 1552 1697 1696
		f 4 2783 -2718 -2780 -2783
		mu 0 4 1697 1643 1650 1696
		f 4 -2706 -2784 2784 2785
		mu 0 4 1644 1643 1697 1698
		f 4 -2782 -2557 2786 -2785
		mu 0 4 1697 1552 1551 1698
		f 4 -2555 2787 2788 -2787
		mu 0 4 1551 1550 1699 1698
		f 4 2789 -2708 -2786 -2789
		mu 0 4 1699 1637 1644 1698
		f 4 -2696 -2790 2790 2791
		mu 0 4 1638 1637 1699 1700
		f 4 -2788 -2554 2792 -2791
		mu 0 4 1699 1550 1548 1700
		f 4 -2551 2793 2794 -2793
		mu 0 4 1548 1547 1701 1700
		f 4 2795 -2698 -2792 -2795
		mu 0 4 1701 1631 1638 1700
		f 4 -2686 -2796 2796 2797
		mu 0 4 1632 1631 1701 1702
		f 4 -2794 -2549 2798 -2797
		mu 0 4 1701 1547 1546 1702
		f 4 -2547 2799 2800 -2799
		mu 0 4 1546 1545 1703 1702
		f 4 2801 -2688 -2798 -2801
		mu 0 4 1703 1625 1632 1702
		f 4 -2676 -2802 2802 2803
		mu 0 4 1626 1625 1703 1704
		f 4 -2800 -2546 2804 -2803
		mu 0 4 1703 1545 1544 1704
		f 4 -2544 2805 2806 -2805
		mu 0 4 1544 1539 1705 1704
		f 4 2807 -2678 -2804 -2807
		mu 0 4 1705 1619 1626 1704
		f 4 -2666 -2808 2808 2809
		mu 0 4 1620 1619 1705 1706
		f 4 -2806 -2536 2810 -2809
		mu 0 4 1705 1539 1538 1706
		f 4 -2534 2811 2812 -2811
		mu 0 4 1538 1537 1707 1706
		f 4 2813 -2668 -2810 -2813
		mu 0 4 1707 1610 1620 1706
		f 4 -2654 -2814 2814 2815
		mu 0 4 1611 1610 1707 1708
		f 4 -2812 -2533 2816 -2815
		mu 0 4 1707 1537 1535 1708
		f 4 -2530 -2774 2817 -2817
		mu 0 4 1535 1534 1693 1708
		f 4 -2771 -2656 -2816 -2818
		mu 0 4 1693 1612 1611 1708
		f 4 -2644 2818 2819 2820
		mu 0 4 1605 1564 1709 1710
		f 4 2821 -2721 2822 -2820
		mu 0 4 1709 1614 1651 1710
		f 4 -2728 2823 2824 -2823
		mu 0 4 1651 1646 1711 1710
		f 4 2825 -2646 -2821 -2825
		mu 0 4 1711 1601 1605 1710
		f 4 -2636 -2826 2826 2827
		mu 0 4 1602 1601 1711 1712
		f 4 -2824 -2711 2828 -2827
		mu 0 4 1711 1646 1645 1712
		f 4 -2720 2829 2830 -2829
		mu 0 4 1645 1640 1713 1712
		f 4 2831 -2638 -2828 -2831
		mu 0 4 1713 1595 1602 1712
		f 4 -2626 -2832 2832 2833
		mu 0 4 1596 1595 1713 1714
		f 4 -2830 -2701 2834 -2833
		mu 0 4 1713 1640 1639 1714
		f 4 -2710 2835 2836 -2835
		mu 0 4 1639 1634 1715 1714
		f 4 2837 -2628 -2834 -2837
		mu 0 4 1715 1589 1596 1714
		f 4 -2616 -2838 2838 2839
		mu 0 4 1590 1589 1715 1716
		f 4 -2836 -2691 2840 -2839
		mu 0 4 1715 1634 1633 1716
		f 4 -2700 2841 2842 -2841
		mu 0 4 1633 1628 1717 1716
		f 4 2843 -2618 -2840 -2843
		mu 0 4 1717 1583 1590 1716
		f 4 -2606 -2844 2844 2845
		mu 0 4 1584 1583 1717 1718
		f 4 -2842 -2681 2846 -2845
		mu 0 4 1717 1628 1627 1718
		f 4 -2690 2847 2848 -2847
		mu 0 4 1627 1622 1719 1718
		f 4 2849 -2608 -2846 -2849
		mu 0 4 1719 1577 1584 1718
		f 4 -2596 -2850 2850 2851
		mu 0 4 1578 1577 1719 1720
		f 4 -2848 -2671 2852 -2851
		mu 0 4 1719 1622 1621 1720
		f 4 -2680 2853 2854 -2853
		mu 0 4 1621 1616 1721 1720
		f 4 2855 -2598 -2852 -2855
		mu 0 4 1721 1571 1578 1720
		f 4 -2586 -2856 2856 2857
		mu 0 4 1572 1571 1721 1722
		f 4 -2854 -2661 2858 -2857
		mu 0 4 1721 1616 1615 1722
		f 4 -2670 2859 2860 -2859
		mu 0 4 1615 1607 1723 1722
		f 4 2861 -2588 -2858 -2861
		mu 0 4 1723 1562 1572 1722
		f 4 -2574 -2862 2862 2863
		mu 0 4 1563 1562 1723 1724
		f 4 -2860 -2649 2864 -2863
		mu 0 4 1723 1607 1606 1724
		f 4 -2660 -2822 2865 -2865
		mu 0 4 1606 1614 1709 1724
		f 4 -2819 -2576 -2864 -2866
		mu 0 4 1709 1564 1563 1724
		f 4 2866 -2753 2867 2868
		mu 0 4 1725 1668 1675 1726
		f 4 -2751 2869 2870 -2868
		mu 0 4 1675 1674 1727 1726
		f 4 2871 2872 2873 -2871
		mu 0 4 1727 1728 1729 1726
		f 4 2874 2875 -2869 -2874
		mu 0 4 1729 1730 1725 1726
		f 4 2876 -2745 2877 2878
		mu 0 4 1731 1657 1669 1732
		f 4 -2743 -2867 2879 -2878
		mu 0 4 1669 1668 1725 1732
		f 4 -2876 2880 2881 -2880
		mu 0 4 1725 1730 1733 1732
		f 4 2882 2883 -2879 -2882
		mu 0 4 1733 1734 1731 1732
		f 4 2884 -2735 2885 2886
		mu 0 4 1735 1661 1660 1736
		f 4 -2733 -2877 2887 -2886
		mu 0 4 1658 1657 1731 1737
		f 4 -2884 2888 2889 -2888
		mu 0 4 1731 1734 1738 1737
		f 4 2890 2891 -2887 -2890
		mu 0 4 1739 1740 1735 1736
		f 4 -2873 2892 2893 2894
		mu 0 4 1729 1728 1741 1742
		f 4 2895 2896 2897 -2894
		mu 0 4 1741 1743 1744 1742
		f 4 2898 2899 2900 -2898
		mu 0 4 1744 1745 1746 1742
		f 4 2901 -2875 -2895 -2901
		mu 0 4 1746 1730 1729 1742
		f 4 -2889 2902 2903 2904
		mu 0 4 1738 1734 1747 1748
		f 4 2905 2906 2907 -2904
		mu 0 4 1747 1749 1750 1748
		f 4 2908 2909 2910 -2908
		mu 0 4 1751 1752 1753 1754
		f 4 2911 -2891 -2905 -2911
		mu 0 4 1753 1740 1739 1754
		f 4 -2881 -2902 2912 2913
		mu 0 4 1733 1730 1746 1755
		f 4 -2900 2914 2915 -2913
		mu 0 4 1746 1745 1756 1755
		f 4 2916 -2906 2917 -2916
		mu 0 4 1756 1749 1747 1755
		f 4 -2903 -2883 -2914 -2918
		mu 0 4 1747 1734 1733 1755
		f 4 2918 2919 2920 2921
		mu 0 4 1757 1758 1759 1760
		f 4 2922 2923 2924 -2921
		mu 0 4 1759 1761 1762 1760
		f 4 2925 2926 2927 -2925
		mu 0 4 1762 1763 1764 1760
		f 4 2928 2929 -2922 -2928
		mu 0 4 1764 1765 1757 1760
		f 4 2930 2931 2932 2933
		mu 0 4 1766 1688 1767 1768
		f 4 2934 -2919 2935 -2933
		mu 0 4 1767 1758 1757 1768
		f 4 -2930 2936 2937 -2936
		mu 0 4 1757 1765 1769 1768
		f 4 2938 2939 -2934 -2938
		mu 0 4 1769 1770 1766 1768
		f 4 -2763 -2631 2940 2941
		mu 0 4 1685 1598 1597 1771
		f 4 -2640 2942 2943 -2941
		mu 0 4 1597 1592 1772 1771
		f 4 2944 -2935 2945 -2944
		mu 0 4 1773 1758 1767 1774
		f 4 -2932 -2766 -2942 -2946
		mu 0 4 1767 1688 1687 1774
		f 4 -2943 -2621 2946 2947
		mu 0 4 1772 1592 1591 1775
		f 4 -2630 2948 2949 -2947
		mu 0 4 1591 1586 1776 1775
		f 4 2950 -2923 2951 -2950
		mu 0 4 1777 1761 1759 1778
		f 4 -2920 -2945 -2948 -2952
		mu 0 4 1759 1758 1773 1778
		f 4 -2620 -2738 2952 2953
		mu 0 4 1585 1580 1664 1779
		f 4 -2736 2954 2955 -2953
		mu 0 4 1662 1661 1780 1781
		f 4 2956 -2951 2957 -2956
		mu 0 4 1782 1761 1777 1783
		f 4 -2949 -2611 -2954 -2958
		mu 0 4 1776 1586 1585 1779
		f 4 -2870 -2759 2958 2959
		mu 0 4 1727 1674 1681 1784
		f 4 -2756 2960 2961 -2959
		mu 0 4 1679 1678 1785 1786
		f 4 2962 2963 2964 -2962
		mu 0 4 1785 1787 1788 1786
		f 4 2965 -2872 -2960 -2965
		mu 0 4 1789 1728 1727 1784
		f 4 -2924 -2957 2966 2967
		mu 0 4 1762 1761 1782 1790
		f 4 -2955 -2885 2968 -2967
		mu 0 4 1780 1661 1735 1791
		f 4 -2892 2969 2970 -2969
		mu 0 4 1735 1740 1792 1791
		f 4 2971 -2926 -2968 -2971
		mu 0 4 1793 1763 1762 1790
		f 4 2972 -2963 2973 2974
		mu 0 4 1794 1787 1785 1795
		f 4 -2961 -2769 2975 -2974
		mu 0 4 1785 1678 1691 1795
		f 4 -2767 -2931 2976 -2976
		mu 0 4 1689 1688 1766 1796
		f 4 -2940 2977 -2975 -2977
		mu 0 4 1766 1770 1797 1796
		f 4 2978 2979 2980 2981
		mu 0 4 1798 1799 1800 1801
		f 4 2982 2983 2984 -2981
		mu 0 4 1800 1802 1803 1801
		f 4 2985 2986 2987 -2985
		mu 0 4 1804 1805 1806 1801
		f 4 2988 2989 -2982 -2988
		mu 0 4 1806 1807 1798 1801
		f 4 2990 -2979 2991 2992
		mu 0 4 1808 1799 1798 1809
		f 4 -2990 2993 2994 -2992
		mu 0 4 1798 1807 1810 1809
		f 4 2995 2996 2997 -2995
		mu 0 4 1810 1811 1812 1809
		f 4 2998 2999 -2993 -2998
		mu 0 4 1813 1814 1808 1809
		f 4 -2893 -2966 3000 3001
		mu 0 4 1741 1728 1789 1815
		f 4 -2964 3002 3003 -3001
		mu 0 4 1788 1787 1816 1817
		f 4 3004 3005 3006 -3004
		mu 0 4 1816 1818 1819 1817
		f 4 3007 -2896 -3002 -3007
		mu 0 4 1820 1743 1741 1815
		f 4 -3003 3008 3009 3010
		mu 0 4 1816 1787 1821 1822
		f 4 3011 3012 3013 -3010
		mu 0 4 1821 1823 1824 1822
		f 4 3014 3015 3016 -3014
		mu 0 4 1824 1825 1826 1822
		f 4 3017 -3005 -3011 -3017
		mu 0 4 1826 1818 1816 1822;
	setAttr ".fc[1500:1999]"
		f 4 -3013 3018 3019 3020
		mu 0 4 1824 1823 1827 1828
		f 4 3021 -2912 3022 -3020
		mu 0 4 1827 1740 1753 1828
		f 4 -2910 3023 3024 -3023
		mu 0 4 1753 1752 1829 1828
		f 4 3025 -3015 -3021 -3025
		mu 0 4 1829 1825 1824 1828
		f 4 3026 3027 3028 3029
		mu 0 4 1830 1831 1832 1833
		f 4 3030 3031 3032 -3029
		mu 0 4 1834 1835 1836 1833
		f 4 3033 3034 3035 -3033
		mu 0 4 1836 1837 1838 1833
		f 4 3036 3037 -3030 -3036
		mu 0 4 1838 1839 1830 1833
		f 4 3038 3039 3040 3041
		mu 0 4 1840 1841 1842 1843
		f 4 3042 -3037 3043 -3041
		mu 0 4 1842 1839 1838 1843
		f 4 -3035 3044 3045 -3044
		mu 0 4 1838 1837 1844 1843
		f 4 3046 3047 -3042 -3046
		mu 0 4 1844 1845 1846 1843
		f 4 -3009 3048 3049 3050
		mu 0 4 1821 1787 1847 1848
		f 4 3051 3052 3053 -3050
		mu 0 4 1847 1849 1850 1848
		f 4 3054 3055 3056 -3054
		mu 0 4 1850 1851 1852 1848
		f 4 3057 -3012 -3051 -3057
		mu 0 4 1852 1823 1821 1848
		f 4 -2978 3058 3059 3060
		mu 0 4 1797 1770 1853 1854
		f 4 3061 3062 3063 -3060
		mu 0 4 1853 1855 1856 1854
		f 4 3064 -3052 3065 -3064
		mu 0 4 1857 1849 1847 1858
		f 4 -3049 -2973 -3061 -3066
		mu 0 4 1847 1787 1794 1858
		f 4 -2937 3066 3067 3068
		mu 0 4 1769 1765 1859 1860
		f 4 3069 3070 3071 -3068
		mu 0 4 1859 1861 1862 1860
		f 4 3072 -3062 3073 -3072
		mu 0 4 1862 1855 1853 1860
		f 4 -3059 -2939 -3069 -3074
		mu 0 4 1853 1770 1769 1860
		f 4 -2970 3074 3075 3076
		mu 0 4 1792 1740 1863 1864
		f 4 3077 3078 3079 -3076
		mu 0 4 1863 1865 1866 1864
		f 4 3080 3081 3082 -3080
		mu 0 4 1867 1868 1869 1870
		f 4 3083 -2972 -3077 -3083
		mu 0 4 1869 1763 1793 1870
		f 4 -3019 -3058 3084 3085
		mu 0 4 1827 1823 1852 1871
		f 4 -3056 3086 3087 -3085
		mu 0 4 1852 1851 1872 1871
		f 4 3088 -3078 3089 -3088
		mu 0 4 1872 1865 1863 1871
		f 4 -3075 -3022 -3086 -3090
		mu 0 4 1863 1740 1827 1871
		f 4 -2927 -3084 3090 3091
		mu 0 4 1764 1763 1869 1873
		f 4 -3082 3092 3093 -3091
		mu 0 4 1869 1868 1874 1873
		f 4 3094 -3070 3095 -3094
		mu 0 4 1874 1861 1859 1873
		f 4 -3067 -2929 -3092 -3096
		mu 0 4 1859 1765 1764 1873
		f 4 -3053 3096 3097 3098
		mu 0 4 1850 1849 1875 1876
		f 4 3099 3100 3101 -3098
		mu 0 4 1875 1877 1878 1876
		f 4 3102 3103 3104 -3102
		mu 0 4 1878 1879 1880 1876
		f 4 3105 -3055 -3099 -3105
		mu 0 4 1880 1851 1850 1876
		f 4 -3063 3106 3107 3108
		mu 0 4 1856 1855 1881 1882
		f 4 3109 3110 3111 -3108
		mu 0 4 1881 1883 1884 1882
		f 4 3112 -3100 3113 -3112
		mu 0 4 1885 1877 1875 1886
		f 4 -3097 -3065 -3109 -3114
		mu 0 4 1875 1849 1857 1886
		f 4 -3071 3114 3115 3116
		mu 0 4 1862 1861 1887 1888
		f 4 3117 3118 3119 -3116
		mu 0 4 1887 1889 1890 1888
		f 4 3120 -3110 3121 -3120
		mu 0 4 1890 1883 1881 1888
		f 4 -3107 -3073 -3117 -3122
		mu 0 4 1881 1855 1862 1888
		f 4 -3079 3122 3123 3124
		mu 0 4 1866 1865 1891 1892
		f 4 3125 3126 3127 -3124
		mu 0 4 1891 1893 1894 1892
		f 4 3128 3129 3130 -3128
		mu 0 4 1895 1896 1897 1898
		f 4 3131 -3081 -3125 -3131
		mu 0 4 1897 1868 1867 1898
		f 4 -3087 -3106 3132 3133
		mu 0 4 1872 1851 1880 1899
		f 4 -3104 3134 3135 -3133
		mu 0 4 1880 1879 1900 1899
		f 4 3136 -3126 3137 -3136
		mu 0 4 1900 1893 1891 1899
		f 4 -3123 -3089 -3134 -3138
		mu 0 4 1891 1865 1872 1899
		f 4 -3093 -3132 3138 3139
		mu 0 4 1874 1868 1897 1901
		f 4 -3130 3140 3141 -3139
		mu 0 4 1897 1896 1902 1901
		f 4 3142 -3118 3143 -3142
		mu 0 4 1902 1889 1887 1901
		f 4 -3115 -3095 -3140 -3144
		mu 0 4 1887 1861 1874 1901
		f 4 -3101 3144 3145 3146
		mu 0 4 1878 1877 1903 1904
		f 4 3147 -3027 3148 -3146
		mu 0 4 1903 1831 1830 1904
		f 4 -3038 3149 3150 -3149
		mu 0 4 1830 1839 1905 1904
		f 4 3151 -3103 -3147 -3151
		mu 0 4 1905 1879 1878 1904
		f 4 -3111 3152 3153 3154
		mu 0 4 1884 1883 1906 1907
		f 4 3155 -3031 3156 -3154
		mu 0 4 1906 1835 1834 1907
		f 4 -3028 -3148 3157 -3157
		mu 0 4 1832 1831 1903 1908
		f 4 -3145 -3113 -3155 -3158
		mu 0 4 1903 1877 1885 1908
		f 4 -3119 3158 3159 3160
		mu 0 4 1890 1889 1909 1910
		f 4 3161 -3034 3162 -3160
		mu 0 4 1909 1837 1836 1910
		f 4 -3032 -3156 3163 -3163
		mu 0 4 1836 1835 1906 1910
		f 4 -3153 -3121 -3161 -3164
		mu 0 4 1906 1883 1890 1910
		f 4 -3127 3164 3165 3166
		mu 0 4 1894 1893 1911 1912
		f 4 3167 -3039 3168 -3166
		mu 0 4 1911 1841 1840 1912
		f 4 -3048 3169 3170 -3169
		mu 0 4 1846 1845 1913 1914
		f 4 3171 -3129 -3167 -3171
		mu 0 4 1913 1896 1895 1914
		f 4 -3135 -3152 3172 3173
		mu 0 4 1900 1879 1905 1915
		f 4 -3150 -3043 3174 -3173
		mu 0 4 1905 1839 1842 1915
		f 4 -3040 -3168 3175 -3175
		mu 0 4 1842 1841 1911 1915
		f 4 -3165 -3137 -3174 -3176
		mu 0 4 1911 1893 1900 1915
		f 4 -3141 -3172 3176 3177
		mu 0 4 1902 1896 1913 1916
		f 4 -3170 -3047 3178 -3177
		mu 0 4 1913 1845 1844 1916
		f 4 -3045 -3162 3179 -3179
		mu 0 4 1844 1837 1909 1916
		f 4 -3159 -3143 -3178 -3180
		mu 0 4 1909 1889 1902 1916
		f 4 3180 -3026 3181 3182
		mu 0 4 1917 1825 1829 1918
		f 4 -3024 3183 3184 -3182
		mu 0 4 1829 1752 1919 1918
		f 4 3185 3186 3187 -3185
		mu 0 4 1919 1920 1921 1918
		f 4 3188 3189 -3183 -3188
		mu 0 4 1921 1922 1917 1918
		f 4 3190 -3018 3191 3192
		mu 0 4 1923 1818 1826 1924
		f 4 -3016 -3181 3193 -3192
		mu 0 4 1826 1825 1917 1924
		f 4 -3190 3194 3195 -3194
		mu 0 4 1917 1922 1925 1924
		f 4 3196 3197 -3193 -3196
		mu 0 4 1925 1926 1923 1924
		f 4 3198 -3008 3199 3200
		mu 0 4 1927 1743 1820 1928
		f 4 -3006 -3191 3201 -3200
		mu 0 4 1819 1818 1923 1929
		f 4 -3198 3202 3203 -3202
		mu 0 4 1923 1926 1930 1929
		f 4 3204 3205 -3201 -3204
		mu 0 4 1931 1932 1927 1928
		f 4 -2897 -3199 3206 3207
		mu 0 4 1744 1743 1927 1933
		f 4 -3206 3208 3209 -3207
		mu 0 4 1927 1932 1934 1933
		f 4 3210 3211 3212 -3210
		mu 0 4 1934 1935 1936 1933
		f 4 3213 -2899 -3208 -3213
		mu 0 4 1936 1745 1744 1933
		f 4 -2915 -3214 3214 3215
		mu 0 4 1756 1745 1936 1937
		f 4 -3212 3216 3217 -3215
		mu 0 4 1936 1935 1938 1937
		f 4 3218 3219 3220 -3218
		mu 0 4 1938 1939 1940 1937
		f 4 3221 -2917 -3216 -3221
		mu 0 4 1940 1749 1756 1937
		f 4 -2907 -3222 3222 3223
		mu 0 4 1750 1749 1940 1941
		f 4 -3220 3224 3225 -3223
		mu 0 4 1940 1939 1942 1941
		f 4 3226 -3186 3227 -3226
		mu 0 4 1943 1920 1919 1944
		f 4 -3184 -2909 -3224 -3228
		mu 0 4 1919 1752 1751 1944
		f 4 3228 -3189 3229 3230
		mu 0 4 1945 1922 1921 1946
		f 4 -3187 3231 3232 -3230
		mu 0 4 1921 1920 1947 1946
		f 4 3233 -2996 3234 -3233
		mu 0 4 1947 1811 1810 1946
		f 4 -2994 3235 -3231 -3235
		mu 0 4 1810 1807 1945 1946
		f 4 3236 -3197 3237 3238
		mu 0 4 1948 1926 1925 1949
		f 4 -3195 -3229 3239 -3238
		mu 0 4 1925 1922 1945 1949
		f 4 -3236 -2989 3240 -3240
		mu 0 4 1945 1807 1806 1949
		f 4 -2987 3241 -3239 -3241
		mu 0 4 1806 1805 1948 1949
		f 4 3242 -3205 3243 3244
		mu 0 4 1950 1932 1931 1951
		f 4 -3203 -3237 3245 -3244
		mu 0 4 1930 1926 1948 1952
		f 4 -3242 -2986 3246 -3246
		mu 0 4 1948 1805 1804 1952
		f 4 -2984 3247 -3245 -3247
		mu 0 4 1803 1802 1950 1951
		f 4 -3209 -3243 3248 3249
		mu 0 4 1934 1932 1950 1953
		f 4 -3248 -2983 3250 -3249
		mu 0 4 1950 1802 1800 1953
		f 4 -2980 3251 3252 -3251
		mu 0 4 1800 1799 1954 1953
		f 4 3253 -3211 -3250 -3253
		mu 0 4 1954 1935 1934 1953
		f 4 -3217 -3254 3254 3255
		mu 0 4 1938 1935 1954 1955
		f 4 -3252 -2991 3256 -3255
		mu 0 4 1954 1799 1808 1955
		f 4 -3000 3257 3258 -3257
		mu 0 4 1808 1814 1956 1955
		f 4 3259 -3219 -3256 -3259
		mu 0 4 1956 1939 1938 1955
		f 4 -3225 -3260 3260 3261
		mu 0 4 1942 1939 1956 1957
		f 4 -3258 -2999 3262 -3261
		mu 0 4 1956 1814 1813 1957
		f 4 -2997 -3234 3263 -3263
		mu 0 4 1812 1811 1947 1958
		f 4 -3232 -3227 -3262 -3264
		mu 0 4 1947 1920 1943 1958
		f 4 3264 3265 3266 3267
		mu 0 4 1959 1960 1961 1962
		f 4 -3266 3268 3269 3270
		mu 0 4 1961 1960 1963 1964
		f 4 -3269 3271 3272 3273
		mu 0 4 1963 1960 1965 1966
		f 4 -3272 -3265 3274 3275
		mu 0 4 1965 1960 1959 1967
		f 4 3276 3277 -3276 3278
		mu 0 4 1968 1969 1965 1967
		f 4 -3278 3279 3280 -3273
		mu 0 4 1965 1969 1970 1966
		f 4 -3280 3281 3282 3283
		mu 0 4 1970 1969 1971 1972
		f 4 -3282 -3277 3284 3285
		mu 0 4 1971 1969 1968 1973
		f 4 3286 3287 3288 -3285
		mu 0 4 1968 1974 1975 1973
		f 4 -3288 3289 3290 3291
		mu 0 4 1975 1974 1976 1977
		f 4 -3290 3292 3293 3294
		mu 0 4 1976 1974 1978 1979
		f 4 -3293 -3287 -3279 3295
		mu 0 4 1978 1974 1968 1967
		f 4 3296 3297 -3296 -3275
		mu 0 4 1959 1980 1978 1967
		f 4 -3298 3298 3299 -3294
		mu 0 4 1978 1980 1981 1979
		f 4 -3299 3300 3301 3302
		mu 0 4 1981 1980 1982 1983
		f 4 -3301 -3297 -3268 3303
		mu 0 4 1982 1980 1959 1962
		f 4 3304 3305 3306 3307
		mu 0 4 1984 1985 1986 1987
		f 4 -3306 3308 3309 3310
		mu 0 4 1986 1985 1988 1989
		f 4 -3309 3311 3312 3313
		mu 0 4 1988 1985 1990 1991
		f 4 -3312 -3305 3314 3315
		mu 0 4 1990 1985 1984 1992
		f 4 3316 3317 3318 3319
		mu 0 4 1993 1994 1995 1996
		f 4 -3318 3320 3321 3322
		mu 0 4 1995 1994 1997 1998
		f 4 -3321 3323 -3311 3324
		mu 0 4 1997 1994 1986 1989
		f 4 -3324 -3317 3325 -3307
		mu 0 4 1986 1994 1993 1987
		f 4 3326 3327 3328 3329
		mu 0 4 1999 2000 2001 2002
		f 4 -3328 3330 3331 3332
		mu 0 4 2001 2000 2003 2004
		f 4 -3331 3333 -3323 3334
		mu 0 4 2003 2000 1995 1998
		f 4 -3334 -3327 3335 -3319
		mu 0 4 1995 2000 1999 1996
		f 4 3336 3337 3338 3339
		mu 0 4 2005 2006 2007 2008
		f 4 -3338 3340 3341 3342
		mu 0 4 2007 2006 2009 2010
		f 4 -3341 3343 -3333 3344
		mu 0 4 2009 2006 2001 2004
		f 4 -3344 -3337 3345 -3329
		mu 0 4 2001 2006 2005 2002
		f 4 3346 3347 3348 3349
		mu 0 4 2011 2012 2013 2014
		f 4 -3348 3350 3351 3352
		mu 0 4 2013 2012 2015 2016
		f 4 -3351 3353 -3343 3354
		mu 0 4 2015 2012 2007 2010
		f 4 -3354 -3347 3355 -3339
		mu 0 4 2007 2012 2011 2008
		f 4 3356 3357 3358 3359
		mu 0 4 2017 2018 2019 2020
		f 4 -3358 3360 3361 3362
		mu 0 4 2019 2018 2021 2022
		f 4 -3361 3363 -3353 3364
		mu 0 4 2021 2018 2013 2016
		f 4 -3364 -3357 3365 -3349
		mu 0 4 2013 2018 2017 2014
		f 4 3366 3367 3368 3369
		mu 0 4 2023 2024 2025 2026
		f 4 -3368 3370 3371 3372
		mu 0 4 2025 2024 2027 2028
		f 4 -3371 3373 -3363 3374
		mu 0 4 2027 2024 2019 2022
		f 4 -3374 -3367 3375 -3359
		mu 0 4 2019 2024 2023 2020
		f 4 3376 3377 -3316 3378
		mu 0 4 2029 2030 1990 1992
		f 4 -3378 3379 3380 -3313
		mu 0 4 1990 2030 2031 1991
		f 4 -3380 3381 -3373 3382
		mu 0 4 2031 2030 2025 2028
		f 4 -3382 -3377 3383 -3369
		mu 0 4 2025 2030 2029 2026
		f 4 3384 3385 3386 3387
		mu 0 4 2032 2033 2034 2035
		f 4 -3386 3388 3389 3390
		mu 0 4 2034 2033 2036 2037
		f 4 -3389 3391 3392 3393
		mu 0 4 2036 2033 2038 2039
		f 4 -3392 -3385 3394 3395
		mu 0 4 2038 2033 2032 2040
		f 4 3396 3397 3398 3399
		mu 0 4 2041 2042 2043 2044
		f 4 -3398 3400 3401 3402
		mu 0 4 2043 2042 2045 2046
		f 4 -3401 3403 -3391 3404
		mu 0 4 2045 2042 2034 2037
		f 4 -3404 -3397 3405 -3387
		mu 0 4 2034 2042 2041 2035
		f 4 3406 3407 3408 3409
		mu 0 4 2047 2048 2049 2050
		f 4 -3408 3410 3411 3412
		mu 0 4 2049 2048 2051 2052
		f 4 -3411 3413 -3403 3414
		mu 0 4 2051 2048 2043 2046
		f 4 -3414 -3407 3415 -3399
		mu 0 4 2043 2048 2047 2044
		f 4 3416 3417 3418 3419
		mu 0 4 2053 2054 2055 2056
		f 4 -3418 3420 3421 3422
		mu 0 4 2055 2054 2057 2058
		f 4 -3421 3423 -3413 3424
		mu 0 4 2057 2054 2049 2052
		f 4 -3424 -3417 3425 -3409
		mu 0 4 2049 2054 2053 2050
		f 4 3426 3427 3428 3429
		mu 0 4 2059 2060 2061 2062
		f 4 -3428 3430 3431 3432
		mu 0 4 2061 2060 2063 2064
		f 4 -3431 3433 -3423 3434
		mu 0 4 2063 2060 2055 2058
		f 4 -3434 -3427 3435 -3419
		mu 0 4 2055 2060 2059 2056
		f 4 3436 3437 3438 3439
		mu 0 4 2065 2066 2067 2068
		f 4 -3438 3440 3441 3442
		mu 0 4 2067 2066 2069 2070
		f 4 -3441 3443 -3433 3444
		mu 0 4 2069 2066 2061 2064
		f 4 -3444 -3437 3445 -3429
		mu 0 4 2061 2066 2065 2062
		f 4 3446 3447 3448 3449
		mu 0 4 2071 2072 2073 2074
		f 4 -3448 3450 3451 3452
		mu 0 4 2073 2072 2075 2076
		f 4 -3451 3453 -3443 3454
		mu 0 4 2075 2072 2067 2070
		f 4 -3454 -3447 3455 -3439
		mu 0 4 2067 2072 2071 2068
		f 4 3456 3457 -3396 3458
		mu 0 4 2077 2078 2038 2040
		f 4 -3458 3459 3460 -3393
		mu 0 4 2038 2078 2079 2039
		f 4 -3460 3461 -3453 3462
		mu 0 4 2079 2078 2073 2076
		f 4 -3462 -3457 3463 -3449
		mu 0 4 2073 2078 2077 2074
		f 4 3464 3465 3466 -3346
		mu 0 4 2005 2080 2081 2002
		f 4 -3466 3467 3468 3469
		mu 0 4 2082 2083 2084 2085
		f 4 -3468 3470 3471 3472
		mu 0 4 2086 2087 2088 2089
		f 4 -3471 -3465 -3340 3473
		mu 0 4 2090 2080 2005 2008
		f 4 3474 3475 3476 -3336
		mu 0 4 1999 2091 2092 1996
		f 4 -3476 3477 3478 3479
		mu 0 4 2093 2094 2095 2096
		f 4 -3478 3480 -3470 3481
		mu 0 4 2095 2094 2082 2085
		f 4 -3481 -3475 -3330 -3467
		mu 0 4 2081 2091 1999 2002
		f 4 3482 3483 3484 -3326
		mu 0 4 1993 2097 2098 1987
		f 4 -3484 3485 3486 3487
		mu 0 4 2099 2100 2101 2102
		f 4 -3486 3488 -3480 3489
		mu 0 4 2101 2100 2093 2096
		f 4 -3489 -3483 -3320 -3477
		mu 0 4 2092 2097 1993 1996
		f 4 3490 3491 3492 3493
		mu 0 4 2103 2104 2105 2106
		f 4 -3492 3494 -3488 3495
		mu 0 4 2107 2108 2099 2102
		f 4 -3495 3496 -3308 -3485
		mu 0 4 2098 2109 1984 1987
		f 4 -3497 -3491 3497 -3315
		mu 0 4 1984 2109 2110 1992
		f 4 3498 3499 3500 -3384
		mu 0 4 2029 2111 2112 2026
		f 4 -3500 3501 3502 3503
		mu 0 4 2113 2114 2115 2116
		f 4 -3502 3504 -3494 3505
		mu 0 4 2117 2118 2103 2106
		f 4 -3505 -3499 -3379 -3498
		mu 0 4 2110 2111 2029 1992
		f 4 3506 3507 3508 -3461
		mu 0 4 2079 2119 2120 2039
		f 4 -3508 3509 -3304 3510
		mu 0 4 2120 2119 1982 1962
		f 4 -3510 3511 3512 -3302
		mu 0 4 1982 2119 2121 1983
		f 4 -3512 -3507 -3463 3513
		mu 0 4 2121 2119 2079 2076
		f 4 3514 3515 -3514 -3452
		mu 0 4 2075 2122 2121 2076
		f 4 -3516 3516 -3303 -3513
		mu 0 4 2121 2122 1981 1983
		f 4 -3517 3517 3518 -3300
		mu 0 4 1981 2122 2123 1979
		f 4 -3518 -3515 -3455 3519
		mu 0 4 2123 2122 2075 2070
		f 4 3520 3521 -3520 -3442
		mu 0 4 2069 2124 2123 2070
		f 4 -3522 3522 -3295 -3519
		mu 0 4 2123 2124 1976 1979
		f 4 -3523 3523 3524 -3291
		mu 0 4 1976 2124 2125 1977
		f 4 -3524 -3521 -3445 3525
		mu 0 4 2125 2124 2069 2064
		f 4 3526 3527 -3526 -3432
		mu 0 4 2063 2126 2125 2064
		f 4 -3528 3528 -3292 -3525
		mu 0 4 2125 2126 1975 1977
		f 4 -3529 3529 3530 -3289
		mu 0 4 1975 2126 2127 1973
		f 4 -3530 -3527 -3435 3531
		mu 0 4 2127 2126 2063 2058
		f 4 3532 3533 -3532 -3422
		mu 0 4 2057 2128 2127 2058
		f 4 -3534 3534 -3286 -3531
		mu 0 4 2127 2128 1971 1973
		f 4 -3535 3535 3536 -3283
		mu 0 4 1971 2128 2129 1972
		f 4 -3536 -3533 -3425 3537
		mu 0 4 2129 2128 2057 2052
		f 4 3538 3539 -3538 -3412
		mu 0 4 2051 2130 2129 2052
		f 4 -3540 3540 -3284 -3537
		mu 0 4 2129 2130 1970 1972
		f 4 -3541 3541 3542 -3281
		mu 0 4 1970 2130 2131 1966
		f 4 -3542 -3539 -3415 3543
		mu 0 4 2131 2130 2051 2046
		f 4 3544 3545 -3544 -3402
		mu 0 4 2045 2132 2131 2046
		f 4 -3546 3546 -3274 -3543
		mu 0 4 2131 2132 1963 1966
		f 4 -3547 3547 3548 -3270
		mu 0 4 1963 2132 2133 1964
		f 4 -3548 -3545 -3405 3549
		mu 0 4 2133 2132 2045 2037
		f 4 3550 3551 -3550 -3390
		mu 0 4 2036 2134 2133 2037
		f 4 -3552 3552 -3271 -3549
		mu 0 4 2133 2134 1961 1964
		f 4 -3553 3553 -3511 -3267
		mu 0 4 1961 2134 2120 1962
		f 4 -3554 -3551 -3394 -3509
		mu 0 4 2120 2134 2036 2039
		f 4 3554 3555 3556 -3381
		mu 0 4 2031 2135 2136 1991
		f 4 -3556 3557 -3459 3558
		mu 0 4 2136 2135 2077 2040
		f 4 -3558 3559 3560 -3464
		mu 0 4 2077 2135 2137 2074
		f 4 -3560 -3555 -3383 3561
		mu 0 4 2137 2135 2031 2028
		f 4 3562 3563 -3562 -3372
		mu 0 4 2027 2138 2137 2028
		f 4 -3564 3564 -3450 -3561
		mu 0 4 2137 2138 2071 2074
		f 4 -3565 3565 3566 -3456
		mu 0 4 2071 2138 2139 2068
		f 4 -3566 -3563 -3375 3567
		mu 0 4 2139 2138 2027 2022
		f 4 3568 3569 -3568 -3362
		mu 0 4 2021 2140 2139 2022
		f 4 -3570 3570 -3440 -3567
		mu 0 4 2139 2140 2065 2068
		f 4 -3571 3571 3572 -3446
		mu 0 4 2065 2140 2141 2062
		f 4 -3572 -3569 -3365 3573
		mu 0 4 2141 2140 2021 2016
		f 4 3574 3575 -3574 -3352
		mu 0 4 2015 2142 2141 2016
		f 4 -3576 3576 -3430 -3573
		mu 0 4 2141 2142 2059 2062
		f 4 -3577 3577 3578 -3436
		mu 0 4 2059 2142 2143 2056
		f 4 -3578 -3575 -3355 3579
		mu 0 4 2143 2142 2015 2010
		f 4 3580 3581 -3580 -3342
		mu 0 4 2009 2144 2143 2010
		f 4 -3582 3582 -3420 -3579
		mu 0 4 2143 2144 2053 2056
		f 4 -3583 3583 3584 -3426
		mu 0 4 2053 2144 2145 2050
		f 4 -3584 -3581 -3345 3585
		mu 0 4 2145 2144 2009 2004
		f 4 3586 3587 -3586 -3332
		mu 0 4 2003 2146 2145 2004
		f 4 -3588 3588 -3410 -3585
		mu 0 4 2145 2146 2047 2050
		f 4 -3589 3589 3590 -3416
		mu 0 4 2047 2146 2147 2044
		f 4 -3590 -3587 -3335 3591
		mu 0 4 2147 2146 2003 1998
		f 4 3592 3593 -3592 -3322
		mu 0 4 1997 2148 2147 1998
		f 4 -3594 3594 -3400 -3591
		mu 0 4 2147 2148 2041 2044
		f 4 -3595 3595 3596 -3406
		mu 0 4 2041 2148 2149 2035
		f 4 -3596 -3593 -3325 3597
		mu 0 4 2149 2148 1997 1989
		f 4 3598 3599 -3598 -3310
		mu 0 4 1988 2150 2149 1989
		f 4 -3600 3600 -3388 -3597
		mu 0 4 2149 2150 2032 2035
		f 4 -3601 3601 -3559 -3395
		mu 0 4 2032 2150 2136 2040
		f 4 -3602 -3599 -3314 -3557
		mu 0 4 2136 2150 1988 1991
		f 4 3602 3603 -3490 3604
		mu 0 4 2151 2152 2101 2096
		f 4 -3604 3605 3606 -3487
		mu 0 4 2101 2152 2153 2102
		f 4 -3606 3607 3608 3609
		mu 0 4 2153 2152 2154 2155
		f 4 -3608 -3603 3610 3611
		mu 0 4 2154 2152 2151 2156
		f 4 3612 3613 -3482 3614
		mu 0 4 2157 2158 2095 2085
		f 4 -3614 3615 -3605 -3479
		mu 0 4 2095 2158 2151 2096
		f 4 -3616 3616 3617 -3611
		mu 0 4 2151 2158 2159 2156
		f 4 -3617 -3613 3618 3619
		mu 0 4 2159 2158 2157 2160
		f 4 3620 3621 -3473 3622
		mu 0 4 2161 2162 2086 2089
		f 4 -3622 3623 -3615 -3469
		mu 0 4 2084 2163 2157 2085
		f 4 -3624 3624 3625 -3619
		mu 0 4 2157 2163 2164 2160
		f 4 -3625 -3621 3626 3627
		mu 0 4 2165 2162 2161 2166
		f 4 3628 3629 3630 -3609
		mu 0 4 2154 2167 2168 2155
		f 4 -3630 3631 3632 3633
		mu 0 4 2168 2167 2169 2170
		f 4 -3632 3634 3635 3636
		mu 0 4 2169 2167 2171 2172
		f 4 -3635 -3629 -3612 3637
		mu 0 4 2171 2167 2154 2156
		f 4 3638 3639 3640 -3626
		mu 0 4 2164 2173 2174 2160
		f 4 -3640 3641 3642 3643
		mu 0 4 2174 2173 2175 2176
		f 4 -3642 3644 3645 3646
		mu 0 4 2177 2178 2179 2180
		f 4 -3645 -3639 -3628 3647
		mu 0 4 2179 2178 2165 2166
		f 4 3648 3649 -3638 -3618
		mu 0 4 2159 2181 2171 2156
		f 4 -3650 3650 3651 -3636
		mu 0 4 2171 2181 2182 2172
		f 4 -3651 3652 -3644 3653
		mu 0 4 2182 2181 2174 2176
		f 4 -3653 -3649 -3620 -3641
		mu 0 4 2174 2181 2159 2160
		f 4 3654 3655 3656 3657
		mu 0 4 2183 2184 2185 2186
		f 4 -3656 3658 3659 3660
		mu 0 4 2185 2184 2187 2188
		f 4 -3659 3661 3662 3663
		mu 0 4 2187 2184 2189 2190
		f 4 -3662 -3655 3664 3665
		mu 0 4 2189 2184 2183 2191
		f 4 3666 3667 3668 3669
		mu 0 4 2192 2193 2194 2116
		f 4 -3668 3670 -3658 3671
		mu 0 4 2194 2193 2183 2186
		f 4 -3671 3672 3673 -3665
		mu 0 4 2183 2193 2195 2191
		f 4 -3673 -3667 3674 3675
		mu 0 4 2195 2193 2192 2196
		f 4 3676 3677 -3370 -3501
		mu 0 4 2112 2197 2023 2026
		f 4 -3678 3678 3679 -3376
		mu 0 4 2023 2197 2198 2020
		f 4 -3679 3680 -3672 3681
		mu 0 4 2199 2200 2194 2186
		f 4 -3681 -3677 -3504 -3669
		mu 0 4 2194 2200 2113 2116
		f 4 3682 3683 -3360 -3680
		mu 0 4 2198 2201 2017 2020
		f 4 -3684 3684 3685 -3366
		mu 0 4 2017 2201 2202 2014
		f 4 -3685 3686 -3661 3687
		mu 0 4 2203 2204 2185 2188
		f 4 -3687 -3683 -3682 -3657
		mu 0 4 2185 2204 2199 2186
		f 4 3688 3689 -3474 -3356
		mu 0 4 2011 2205 2090 2008
		f 4 -3690 3690 3691 -3472
		mu 0 4 2088 2206 2207 2089
		f 4 -3691 3692 -3688 3693
		mu 0 4 2208 2209 2203 2188
		f 4 -3693 -3689 -3350 -3686
		mu 0 4 2202 2205 2011 2014
		f 4 3694 3695 -3496 -3607
		mu 0 4 2153 2210 2107 2102
		f 4 -3696 3696 3697 -3493
		mu 0 4 2105 2211 2212 2106
		f 4 -3697 3698 3699 3700
		mu 0 4 2212 2211 2213 2214
		f 4 -3699 -3695 -3610 3701
		mu 0 4 2215 2210 2153 2155
		f 4 3702 3703 -3694 -3660
		mu 0 4 2187 2216 2208 2188
		f 4 -3704 3704 -3623 -3692
		mu 0 4 2207 2217 2161 2089
		f 4 -3705 3705 3706 -3627
		mu 0 4 2161 2217 2218 2166
		f 4 -3706 -3703 -3664 3707
		mu 0 4 2219 2216 2187 2190
		f 4 3708 3709 -3701 3710
		mu 0 4 2220 2221 2212 2214
		f 4 -3710 3711 -3506 -3698
		mu 0 4 2212 2221 2117 2106
		f 4 -3712 3712 -3670 -3503
		mu 0 4 2115 2222 2192 2116
		f 4 -3713 -3709 3713 -3675
		mu 0 4 2192 2222 2223 2196
		f 4 3714 3715 3716 3717
		mu 0 4 2224 2225 2226 2227
		f 4 -3716 3718 3719 3720
		mu 0 4 2226 2225 2228 2229
		f 4 -3719 3721 3722 3723
		mu 0 4 2230 2225 2231 2232
		f 4 -3722 -3715 3724 3725
		mu 0 4 2231 2225 2224 2233
		f 4 3726 3727 -3718 3728
		mu 0 4 2234 2235 2224 2227
		f 4 -3728 3729 3730 -3725
		mu 0 4 2224 2235 2236 2233
		f 4 -3730 3731 3732 3733
		mu 0 4 2236 2235 2237 2238
		f 4 -3732 -3727 3734 3735
		mu 0 4 2239 2235 2234 2240
		f 4 3736 3737 -3702 -3631
		mu 0 4 2168 2241 2215 2155
		f 4 -3738 3738 3739 -3700
		mu 0 4 2213 2242 2243 2214
		f 4 -3739 3740 3741 3742
		mu 0 4 2243 2242 2244 2245
		f 4 -3741 -3737 -3634 3743
		mu 0 4 2246 2241 2168 2170
		f 4 3744 3745 3746 -3740
		mu 0 4 2243 2247 2248 2214
		f 4 -3746 3747 3748 3749
		mu 0 4 2248 2247 2249 2250
		f 4 -3748 3750 3751 3752
		mu 0 4 2249 2247 2251 2252
		f 4 -3751 -3745 -3743 3753
		mu 0 4 2251 2247 2243 2245
		f 4 3754 3755 3756 -3749
		mu 0 4 2249 2253 2254 2250
		f 4 -3756 3757 -3648 3758
		mu 0 4 2254 2253 2179 2166
		f 4 -3758 3759 3760 -3646
		mu 0 4 2179 2253 2255 2180
		f 4 -3760 -3755 -3753 3761
		mu 0 4 2255 2253 2249 2252
		f 4 3762 3763 3764 3765
		mu 0 4 2256 2257 2258 2259
		f 4 -3764 3766 3767 3768
		mu 0 4 2260 2257 2261 2262
		f 4 -3767 3769 3770 3771
		mu 0 4 2261 2257 2263 2264
		f 4 -3770 -3763 3772 3773
		mu 0 4 2263 2257 2256 2265
		f 4 3774 3775 3776 3777
		mu 0 4 2266 2267 2268 2269
		f 4 -3776 3778 -3774 3779
		mu 0 4 2268 2267 2263 2265
		f 4 -3779 3780 3781 -3771
		mu 0 4 2263 2267 2270 2264
		f 4 -3781 -3775 3782 3783
		mu 0 4 2270 2267 2271 2272
		f 4 3784 3785 3786 -3747
		mu 0 4 2248 2273 2274 2214
		f 4 -3786 3787 3788 3789
		mu 0 4 2274 2273 2275 2276
		f 4 -3788 3790 3791 3792
		mu 0 4 2275 2273 2277 2278
		f 4 -3791 -3785 -3750 3793
		mu 0 4 2277 2273 2248 2250
		f 4 3794 3795 3796 -3714
		mu 0 4 2223 2279 2280 2196
		f 4 -3796 3797 3798 3799
		mu 0 4 2280 2279 2281 2282
		f 4 -3798 3800 -3790 3801
		mu 0 4 2283 2284 2274 2276
		f 4 -3801 -3795 -3711 -3787
		mu 0 4 2274 2284 2220 2214
		f 4 3802 3803 3804 -3674
		mu 0 4 2195 2285 2286 2191
		f 4 -3804 3805 3806 3807
		mu 0 4 2286 2285 2287 2288
		f 4 -3806 3808 -3800 3809
		mu 0 4 2287 2285 2280 2282
		f 4 -3809 -3803 -3676 -3797
		mu 0 4 2280 2285 2195 2196
		f 4 3810 3811 3812 -3707
		mu 0 4 2218 2289 2290 2166
		f 4 -3812 3813 3814 3815
		mu 0 4 2290 2289 2291 2292
		f 4 -3814 3816 3817 3818
		mu 0 4 2293 2294 2295 2296
		f 4 -3817 -3811 -3708 3819
		mu 0 4 2295 2294 2219 2190
		f 4 3820 3821 -3794 -3757
		mu 0 4 2254 2297 2277 2250
		f 4 -3822 3822 3823 -3792
		mu 0 4 2277 2297 2298 2278
		f 4 -3823 3824 -3816 3825
		mu 0 4 2298 2297 2290 2292
		f 4 -3825 -3821 -3759 -3813
		mu 0 4 2290 2297 2254 2166
		f 4 3826 3827 -3820 -3663
		mu 0 4 2189 2299 2295 2190
		f 4 -3828 3828 3829 -3818
		mu 0 4 2295 2299 2300 2296
		f 4 -3829 3830 -3808 3831
		mu 0 4 2300 2299 2286 2288
		f 4 -3831 -3827 -3666 -3805
		mu 0 4 2286 2299 2189 2191
		f 4 3832 3833 3834 -3789
		mu 0 4 2275 2301 2302 2276
		f 4 -3834 3835 3836 3837
		mu 0 4 2302 2301 2303 2304
		f 4 -3836 3838 3839 3840
		mu 0 4 2303 2301 2305 2306
		f 4 -3839 -3833 -3793 3841
		mu 0 4 2305 2301 2275 2278
		f 4 3842 3843 3844 -3799
		mu 0 4 2281 2307 2308 2282
		f 4 -3844 3845 3846 3847
		mu 0 4 2308 2307 2309 2310
		f 4 -3846 3848 -3838 3849
		mu 0 4 2311 2312 2302 2304
		f 4 -3849 -3843 -3802 -3835
		mu 0 4 2302 2312 2283 2276
		f 4 3850 3851 3852 -3807
		mu 0 4 2287 2313 2314 2288
		f 4 -3852 3853 3854 3855
		mu 0 4 2314 2313 2315 2316
		f 4 -3854 3856 -3848 3857
		mu 0 4 2315 2313 2308 2310
		f 4 -3857 -3851 -3810 -3845
		mu 0 4 2308 2313 2287 2282
		f 4 3858 3859 3860 -3815
		mu 0 4 2291 2317 2318 2292
		f 4 -3860 3861 3862 3863
		mu 0 4 2318 2317 2319 2320
		f 4 -3862 3864 3865 3866
		mu 0 4 2321 2322 2323 2324
		f 4 -3865 -3859 -3819 3867
		mu 0 4 2323 2322 2293 2296
		f 4 3868 3869 -3842 -3824
		mu 0 4 2298 2325 2305 2278
		f 4 -3870 3870 3871 -3840
		mu 0 4 2305 2325 2326 2306
		f 4 -3871 3872 -3864 3873
		mu 0 4 2326 2325 2318 2320
		f 4 -3873 -3869 -3826 -3861
		mu 0 4 2318 2325 2298 2292
		f 4 3874 3875 -3868 -3830
		mu 0 4 2300 2327 2323 2296
		f 4 -3876 3876 3877 -3866
		mu 0 4 2323 2327 2328 2324
		f 4 -3877 3878 -3856 3879
		mu 0 4 2328 2327 2314 2316
		f 4 -3879 -3875 -3832 -3853
		mu 0 4 2314 2327 2300 2288
		f 4 3880 3881 3882 -3837
		mu 0 4 2303 2329 2330 2304
		f 4 -3882 3883 -3766 3884
		mu 0 4 2330 2329 2256 2259
		f 4 -3884 3885 3886 -3773
		mu 0 4 2256 2329 2331 2265
		f 4 -3886 -3881 -3841 3887
		mu 0 4 2331 2329 2303 2306
		f 4 3888 3889 3890 -3847
		mu 0 4 2309 2332 2333 2310
		f 4 -3890 3891 -3769 3892
		mu 0 4 2333 2332 2260 2262
		f 4 -3892 3893 -3885 -3765
		mu 0 4 2258 2334 2330 2259
		f 4 -3894 -3889 -3850 -3883
		mu 0 4 2330 2334 2311 2304
		f 4 3894 3895 3896 -3855
		mu 0 4 2315 2335 2336 2316
		f 4 -3896 3897 -3772 3898
		mu 0 4 2336 2335 2261 2264
		f 4 -3898 3899 -3893 -3768
		mu 0 4 2261 2335 2333 2262
		f 4 -3900 -3895 -3858 -3891
		mu 0 4 2333 2335 2315 2310
		f 4 3900 3901 3902 -3863
		mu 0 4 2319 2337 2338 2320
		f 4 -3902 3903 -3778 3904
		mu 0 4 2338 2337 2266 2269
		f 4 -3904 3905 3906 -3783
		mu 0 4 2271 2339 2340 2272
		f 4 -3906 -3901 -3867 3907
		mu 0 4 2340 2339 2321 2324
		f 4 3908 3909 -3888 -3872
		mu 0 4 2326 2341 2331 2306
		f 4 -3910 3910 -3780 -3887
		mu 0 4 2331 2341 2268 2265
		f 4 -3911 3911 -3905 -3777
		mu 0 4 2268 2341 2338 2269
		f 4 -3912 -3909 -3874 -3903
		mu 0 4 2338 2341 2326 2320
		f 4 3912 3913 -3908 -3878
		mu 0 4 2328 2342 2340 2324
		f 4 -3914 3914 -3784 -3907
		mu 0 4 2340 2342 2270 2272
		f 4 -3915 3915 -3899 -3782
		mu 0 4 2270 2342 2336 2264
		f 4 -3916 -3913 -3880 -3897
		mu 0 4 2336 2342 2328 2316
		f 4 3916 3917 -3762 3918
		mu 0 4 2343 2344 2255 2252
		f 4 -3918 3919 3920 -3761
		mu 0 4 2255 2344 2345 2180
		f 4 -3920 3921 3922 3923
		mu 0 4 2345 2344 2346 2347
		f 4 -3922 -3917 3924 3925
		mu 0 4 2346 2344 2343 2348
		f 4 3926 3927 -3754 3928
		mu 0 4 2349 2350 2251 2245
		f 4 -3928 3929 -3919 -3752
		mu 0 4 2251 2350 2343 2252
		f 4 -3930 3930 3931 -3925
		mu 0 4 2343 2350 2351 2348
		f 4 -3931 -3927 3932 3933
		mu 0 4 2351 2350 2349 2352
		f 4 3934 3935 -3744 3936
		mu 0 4 2353 2354 2246 2170
		f 4 -3936 3937 -3929 -3742
		mu 0 4 2244 2355 2349 2245
		f 4 -3938 3938 3939 -3933
		mu 0 4 2349 2355 2356 2352
		f 4 -3939 -3935 3940 3941
		mu 0 4 2357 2354 2353 2358
		f 4 3942 3943 -3937 -3633
		mu 0 4 2169 2359 2353 2170
		f 4 -3944 3944 3945 -3941
		mu 0 4 2353 2359 2360 2358
		f 4 -3945 3946 3947 3948
		mu 0 4 2360 2359 2361 2362
		f 4 -3947 -3943 -3637 3949
		mu 0 4 2361 2359 2169 2172
		f 4 3950 3951 -3950 -3652
		mu 0 4 2182 2363 2361 2172
		f 4 -3952 3952 3953 -3948
		mu 0 4 2361 2363 2364 2362
		f 4 -3953 3954 3955 3956
		mu 0 4 2364 2363 2365 2366
		f 4 -3955 -3951 -3654 3957
		mu 0 4 2365 2363 2182 2176
		f 4 3958 3959 -3958 -3643
		mu 0 4 2175 2367 2365 2176
		f 4 -3960 3960 3961 -3956
		mu 0 4 2365 2367 2368 2366
		f 4 -3961 3962 -3924 3963
		mu 0 4 2369 2370 2345 2347
		f 4 -3963 -3959 -3647 -3921
		mu 0 4 2345 2370 2177 2180
		f 4 3964 3965 -3926 3966
		mu 0 4 2371 2372 2346 2348
		f 4 -3966 3967 3968 -3923
		mu 0 4 2346 2372 2373 2347
		f 4 -3968 3969 -3734 3970
		mu 0 4 2373 2372 2236 2238
		f 4 -3970 -3965 3971 -3731
		mu 0 4 2236 2372 2371 2233
		f 4 3972 3973 -3934 3974
		mu 0 4 2374 2375 2351 2352
		f 4 -3974 3975 -3967 -3932
		mu 0 4 2351 2375 2371 2348
		f 4 -3976 3976 -3726 -3972
		mu 0 4 2371 2375 2231 2233
		f 4 -3977 -3973 3977 -3723
		mu 0 4 2231 2375 2374 2232
		f 4 3978 3979 -3942 3980
		mu 0 4 2376 2377 2357 2358
		f 4 -3980 3981 -3975 -3940
		mu 0 4 2356 2378 2374 2352
		f 4 -3982 3982 -3724 -3978
		mu 0 4 2374 2378 2230 2232
		f 4 -3983 -3979 3983 -3720
		mu 0 4 2228 2377 2376 2229
		f 4 3984 3985 -3981 -3946
		mu 0 4 2360 2379 2376 2358
		f 4 -3986 3986 -3721 -3984
		mu 0 4 2376 2379 2226 2229
		f 4 -3987 3987 3988 -3717
		mu 0 4 2226 2379 2380 2227
		f 4 -3988 -3985 -3949 3989
		mu 0 4 2380 2379 2360 2362
		f 4 3990 3991 -3990 -3954
		mu 0 4 2364 2381 2380 2362
		f 4 -3992 3992 -3729 -3989
		mu 0 4 2380 2381 2234 2227
		f 4 -3993 3993 3994 -3735
		mu 0 4 2234 2381 2382 2240
		f 4 -3994 -3991 -3957 3995
		mu 0 4 2382 2381 2364 2366
		f 4 3996 3997 -3996 -3962
		mu 0 4 2368 2383 2382 2366
		f 4 -3998 3998 -3736 -3995
		mu 0 4 2382 2383 2239 2240
		f 4 -3999 3999 -3971 -3733
		mu 0 4 2237 2384 2373 2238
		f 4 -4000 -3997 -3964 -3969
		mu 0 4 2373 2384 2369 2347;
	setAttr ".fc[2000:2239]"
		f 4 4000 4001 4002 4003
		mu 0 4 2385 2386 2387 2388
		f 4 4004 4005 4006 -4003
		mu 0 4 2387 2389 2390 2388
		f 4 4007 4008 4009 -4007
		mu 0 4 2390 2391 2392 2388
		f 4 4010 4011 -4004 -4010
		mu 0 4 2392 2393 2385 2388
		f 4 4012 4013 4014 4015
		mu 0 4 2394 2395 2396 2397
		f 4 4016 4017 4018 -4015
		mu 0 4 2396 2398 2399 2397
		f 4 4019 -4008 4020 -4019
		mu 0 4 2399 2391 2390 2397
		f 4 -4006 4021 -4016 -4021
		mu 0 4 2390 2389 2394 2397
		f 4 4022 4023 4024 4025
		mu 0 4 2400 2401 2402 2403
		f 4 4026 4027 4028 -4025
		mu 0 4 2402 2404 2405 2403
		f 4 4029 -4020 4030 -4029
		mu 0 4 2405 2391 2399 2403
		f 4 -4018 4031 -4026 -4031
		mu 0 4 2399 2398 2400 2403
		f 4 4032 4033 4034 4035
		mu 0 4 2406 2407 2408 2409
		f 4 4036 -4011 4037 -4035
		mu 0 4 2408 2393 2392 2409
		f 4 -4009 -4030 4038 -4038
		mu 0 4 2392 2391 2405 2409
		f 4 -4028 4039 -4036 -4039
		mu 0 4 2405 2404 2406 2409
		f 4 -4014 4040 4041 4042
		mu 0 4 2396 2395 2410 2411
		f 4 4043 4044 4045 -4042
		mu 0 4 2410 2412 2413 2411
		f 4 4046 4047 4048 -4046
		mu 0 4 2413 2414 2415 2411
		f 4 4049 -4017 -4043 -4049
		mu 0 4 2415 2398 2396 2411
		f 4 4050 4051 4052 4053
		mu 0 4 2416 2417 2418 2419
		f 4 4054 4055 4056 -4053
		mu 0 4 2418 2420 2421 2419
		f 4 4057 -4047 4058 -4057
		mu 0 4 2421 2422 2423 2419
		f 4 -4045 4059 -4054 -4059
		mu 0 4 2423 2424 2416 2419
		f 4 4060 4061 4062 4063
		mu 0 4 2425 2426 2427 2428
		f 4 4064 4065 4066 -4063
		mu 0 4 2427 2429 2430 2428
		f 4 4067 -4058 4068 -4067
		mu 0 4 2430 2422 2421 2428
		f 4 -4056 4069 -4064 -4069
		mu 0 4 2421 2420 2425 2428
		f 4 4070 -4023 4071 4072
		mu 0 4 2431 2401 2400 2432
		f 4 -4032 -4050 4073 -4072
		mu 0 4 2400 2398 2415 2432
		f 4 -4048 -4068 4074 -4074
		mu 0 4 2415 2414 2433 2432
		f 4 -4066 4075 -4073 -4075
		mu 0 4 2433 2434 2431 2432
		f 4 -4052 4076 4077 4078
		mu 0 4 2418 2417 2435 2436
		f 4 4079 4080 4081 -4078
		mu 0 4 2435 2437 2438 2436
		f 4 4082 4083 4084 -4082
		mu 0 4 2438 2439 2440 2436
		f 4 4085 -4055 -4079 -4085
		mu 0 4 2440 2420 2418 2436
		f 4 4086 4087 4088 4089
		mu 0 4 2441 2442 2443 2444
		f 4 4090 4091 4092 -4089
		mu 0 4 2443 2445 2446 2444
		f 4 4093 -4083 4094 -4093
		mu 0 4 2446 2439 2438 2444
		f 4 -4081 4095 -4090 -4095
		mu 0 4 2438 2437 2441 2444
		f 4 4096 4097 4098 4099
		mu 0 4 2447 2448 2449 2450
		f 4 4100 4101 4102 -4099
		mu 0 4 2449 2451 2452 2450
		f 4 4103 -4094 4104 -4103
		mu 0 4 2452 2439 2446 2450
		f 4 -4092 4105 -4100 -4105
		mu 0 4 2446 2445 2447 2450
		f 4 4106 -4061 4107 4108
		mu 0 4 2453 2426 2425 2454
		f 4 -4070 -4086 4109 -4108
		mu 0 4 2425 2420 2440 2454
		f 4 -4084 -4104 4110 -4110
		mu 0 4 2440 2439 2452 2454
		f 4 -4102 4111 -4109 -4111
		mu 0 4 2452 2451 2453 2454
		f 4 -4088 4112 4113 4114
		mu 0 4 2443 2442 2455 2456
		f 4 4115 4116 4117 -4114
		mu 0 4 2455 2457 2458 2456
		f 4 4118 4119 4120 -4118
		mu 0 4 2458 2459 2460 2456
		f 4 4121 -4091 -4115 -4121
		mu 0 4 2460 2445 2443 2456
		f 4 4122 -4001 4123 4124
		mu 0 4 2461 2386 2385 2462
		f 4 -4012 4125 4126 -4124
		mu 0 4 2385 2393 2463 2462
		f 4 4127 -4119 4128 -4127
		mu 0 4 2463 2464 2465 2462
		f 4 -4117 4129 -4125 -4129
		mu 0 4 2465 2466 2461 2462
		f 4 -4034 4130 4131 4132
		mu 0 4 2408 2407 2467 2468
		f 4 4133 4134 4135 -4132
		mu 0 4 2467 2469 2470 2468
		f 4 4136 -4128 4137 -4136
		mu 0 4 2470 2464 2463 2468
		f 4 -4126 -4037 -4133 -4138
		mu 0 4 2463 2393 2408 2468
		f 4 4138 -4097 4139 4140
		mu 0 4 2471 2448 2447 2472
		f 4 -4106 -4122 4141 -4140
		mu 0 4 2447 2445 2460 2472
		f 4 -4120 -4137 4142 -4142
		mu 0 4 2460 2459 2473 2472
		f 4 -4135 4143 -4141 -4143
		mu 0 4 2473 2474 2471 2472
		f 4 -4113 -4087 4144 4145
		mu 0 4 2455 2442 2441 2475
		f 4 -4096 4146 4147 -4145
		mu 0 4 2441 2437 2476 2475
		f 4 4148 4149 4150 -4148
		mu 0 4 2476 2477 2478 2475
		f 4 4151 -4116 -4146 -4151
		mu 0 4 2478 2457 2455 2475
		f 4 -4077 -4051 4152 4153
		mu 0 4 2435 2417 2416 2479
		f 4 -4060 4154 4155 -4153
		mu 0 4 2416 2424 2480 2479
		f 4 4156 -4149 4157 -4156
		mu 0 4 2480 2477 2476 2479
		f 4 -4147 -4080 -4154 -4158
		mu 0 4 2476 2437 2435 2479
		f 4 -4041 -4013 4158 4159
		mu 0 4 2410 2395 2394 2481
		f 4 -4022 4160 4161 -4159
		mu 0 4 2394 2389 2482 2481
		f 4 4162 -4157 4163 -4162
		mu 0 4 2482 2483 2484 2481
		f 4 -4155 -4044 -4160 -4164
		mu 0 4 2484 2412 2410 2481
		f 4 -4002 -4123 4164 4165
		mu 0 4 2387 2386 2461 2485
		f 4 -4130 -4152 4166 -4165
		mu 0 4 2461 2466 2486 2485
		f 4 -4150 -4163 4167 -4167
		mu 0 4 2486 2483 2482 2485
		f 4 -4161 -4005 -4166 -4168
		mu 0 4 2482 2389 2387 2485
		f 4 -4131 -4033 4168 4169
		mu 0 4 2467 2407 2406 2487
		f 4 -4040 4170 4171 -4169
		mu 0 4 2406 2404 2488 2487
		f 4 4172 4173 4174 -4172
		mu 0 4 2488 2489 2490 2487
		f 4 4175 -4134 -4170 -4175
		mu 0 4 2490 2469 2467 2487
		f 4 -4024 -4071 4176 4177
		mu 0 4 2402 2401 2431 2491
		f 4 -4076 4178 4179 -4177
		mu 0 4 2431 2434 2492 2491
		f 4 4180 -4173 4181 -4180
		mu 0 4 2492 2489 2488 2491
		f 4 -4171 -4027 -4178 -4182
		mu 0 4 2488 2404 2402 2491
		f 4 -4062 -4107 4182 4183
		mu 0 4 2427 2426 2453 2493
		f 4 -4112 4184 4185 -4183
		mu 0 4 2453 2451 2494 2493
		f 4 4186 -4181 4187 -4186
		mu 0 4 2494 2495 2496 2493
		f 4 -4179 -4065 -4184 -4188
		mu 0 4 2496 2429 2427 2493
		f 4 -4098 -4139 4188 4189
		mu 0 4 2449 2448 2471 2497
		f 4 -4144 -4176 4190 -4189
		mu 0 4 2471 2474 2498 2497
		f 4 -4174 -4187 4191 -4191
		mu 0 4 2498 2495 2494 2497
		f 4 -4185 -4101 -4190 -4192
		mu 0 4 2494 2451 2449 2497
		f 4 4192 4193 4194 4195
		mu 0 4 2499 2500 2501 2502
		f 4 4196 4197 4198 -4195
		mu 0 4 2501 2503 2504 2502
		f 4 4199 4200 4201 -4199
		mu 0 4 2504 2505 2506 2502
		f 4 4202 4203 -4196 -4202
		mu 0 4 2506 2507 2499 2502
		f 4 4204 4205 4206 4207
		mu 0 4 2508 2509 2510 2511
		f 4 4208 4209 4210 -4207
		mu 0 4 2510 2512 2513 2511
		f 4 4211 4212 4213 -4211
		mu 0 4 2513 2514 2515 2511
		f 4 4214 4215 -4208 -4214
		mu 0 4 2515 2516 2508 2511
		f 4 4216 4217 4218 4219
		mu 0 4 2517 2518 2519 2520
		f 4 4220 4221 4222 -4219
		mu 0 4 2519 2521 2522 2520
		f 4 4223 4224 4225 -4223
		mu 0 4 2522 2523 2524 2520
		f 4 4226 4227 -4220 -4226
		mu 0 4 2524 2525 2517 2520
		f 4 -4222 4228 4229 4230
		mu 0 4 2522 2521 2526 2527
		f 4 4231 -4193 4232 -4230
		mu 0 4 2528 2500 2499 2529
		f 4 -4204 4233 4234 -4233
		mu 0 4 2499 2507 2530 2529
		f 4 4235 -4224 -4231 -4235
		mu 0 4 2531 2523 2522 2527
		f 4 -4229 -4221 4236 4237
		mu 0 4 2526 2521 2519 2532
		f 4 -4218 4238 4239 -4237
		mu 0 4 2519 2518 2533 2532
		f 4 4240 -4197 4241 -4240
		mu 0 4 2534 2503 2501 2535
		f 4 -4194 -4232 -4238 -4242
		mu 0 4 2501 2500 2528 2535
		f 4 -4234 -4203 4242 4243
		mu 0 4 2530 2507 2506 2536
		f 4 -4201 4244 4245 -4243
		mu 0 4 2506 2505 2537 2536
		f 4 4246 -4227 4247 -4246
		mu 0 4 2538 2525 2524 2539
		f 4 -4225 -4236 -4244 -4248
		mu 0 4 2524 2523 2531 2539
		f 4 4248 4249 4250 4251
		mu 0 4 2540 2541 2542 2543
		f 4 4252 -4215 4253 -4251
		mu 0 4 2542 2516 2515 2543
		f 4 -4213 4254 4255 -4254
		mu 0 4 2515 2514 2544 2543
		f 4 4256 4257 -4252 -4256
		mu 0 4 2544 2545 2546 2543
		f 4 -4210 4258 4259 4260
		mu 0 4 2513 2512 2547 2548
		f 4 4261 4262 4263 -4260
		mu 0 4 2547 2549 2550 2548
		f 4 4264 -4257 4265 -4264
		mu 0 4 2550 2545 2544 2548
		f 4 -4255 -4212 -4261 -4266
		mu 0 4 2544 2514 2513 2548
		f 4 4266 -4262 4267 4268
		mu 0 4 2551 2549 2547 2552
		f 4 -4259 -4209 4269 -4268
		mu 0 4 2547 2512 2510 2552
		f 4 -4206 4270 4271 -4270
		mu 0 4 2510 2509 2553 2552
		f 4 4272 4273 -4269 -4272
		mu 0 4 2553 2554 2555 2552
		f 4 4274 -4273 4275 4276
		mu 0 4 2556 2554 2553 2557
		f 4 -4271 -4205 4277 -4276
		mu 0 4 2553 2509 2508 2557
		f 4 -4216 -4253 4278 -4278
		mu 0 4 2508 2516 2542 2557
		f 4 -4250 4279 -4277 -4279
		mu 0 4 2542 2541 2556 2557
		f 4 -4245 4280 4281 4282
		mu 0 4 2537 2505 2558 2559
		f 4 4283 4284 4285 -4282
		mu 0 4 2558 2560 2561 2559
		f 4 4286 4287 4288 -4286
		mu 0 4 2562 2563 2564 2565
		f 4 4289 -4247 -4283 -4289
		mu 0 4 2564 2525 2538 2565
		f 4 4290 4291 4292 4293
		mu 0 4 2566 2567 2568 2569
		f 4 4294 -4217 4295 -4293
		mu 0 4 2568 2518 2517 2569
		f 4 -4228 -4290 4296 -4296
		mu 0 4 2517 2525 2564 2569
		f 4 -4288 4297 -4294 -4297
		mu 0 4 2564 2563 2566 2569
		f 4 -4239 -4295 4298 4299
		mu 0 4 2533 2518 2568 2570
		f 4 -4292 4300 4301 -4299
		mu 0 4 2568 2567 2571 2570
		f 4 4302 4303 4304 -4302
		mu 0 4 2572 2573 2574 2575
		f 4 4305 -4241 -4300 -4305
		mu 0 4 2574 2503 2534 2575
		f 4 -4198 -4306 4306 4307
		mu 0 4 2504 2503 2574 2576
		f 4 -4304 4308 4309 -4307
		mu 0 4 2574 2573 2577 2576
		f 4 4310 -4284 4311 -4310
		mu 0 4 2577 2560 2558 2576
		f 4 -4281 -4200 -4308 -4312
		mu 0 4 2558 2505 2504 2576
		f 4 -4285 4312 4313 4314
		mu 0 4 2561 2560 2578 2579
		f 4 4315 -4249 4316 -4314
		mu 0 4 2578 2541 2540 2579
		f 4 -4258 4317 4318 -4317
		mu 0 4 2546 2545 2580 2581
		f 4 4319 -4287 -4315 -4319
		mu 0 4 2580 2563 2562 2581
		f 4 -4263 4320 4321 4322
		mu 0 4 2550 2549 2582 2583
		f 4 4323 -4291 4324 -4322
		mu 0 4 2582 2567 2566 2583
		f 4 -4298 -4320 4325 -4325
		mu 0 4 2566 2563 2580 2583
		f 4 -4318 -4265 -4323 -4326
		mu 0 4 2580 2545 2550 2583
		f 4 -4301 -4324 4326 4327
		mu 0 4 2571 2567 2582 2584
		f 4 -4321 -4267 4328 -4327
		mu 0 4 2582 2549 2551 2584
		f 4 -4274 4329 4330 -4329
		mu 0 4 2555 2554 2585 2586
		f 4 4331 -4303 -4328 -4331
		mu 0 4 2585 2573 2572 2586
		f 4 -4309 -4332 4332 4333
		mu 0 4 2577 2573 2585 2587
		f 4 -4330 -4275 4334 -4333
		mu 0 4 2585 2554 2556 2587
		f 4 -4280 -4316 4335 -4335
		mu 0 4 2556 2541 2578 2587
		f 4 -4313 -4311 -4334 -4336
		mu 0 4 2578 2560 2577 2587
		f 4 4336 4337 4338 4339
		mu 0 4 2588 2589 2590 2591
		f 4 -4338 4340 4341 4342
		mu 0 4 2590 2589 2592 2593
		f 4 -4341 4343 4344 4345
		mu 0 4 2592 2589 2594 2595
		f 4 -4344 -4337 4346 4347
		mu 0 4 2594 2589 2588 2596
		f 4 4348 4349 4350 4351
		mu 0 4 2597 2598 2599 2600
		f 4 -4350 4352 4353 4354
		mu 0 4 2599 2598 2601 2602
		f 4 -4353 4355 4356 4357
		mu 0 4 2601 2598 2603 2604
		f 4 -4356 -4349 4358 4359
		mu 0 4 2603 2598 2597 2605
		f 4 4360 4361 4362 4363
		mu 0 4 2606 2607 2608 2609
		f 4 -4362 4364 4365 4366
		mu 0 4 2608 2607 2610 2611
		f 4 -4365 4367 4368 4369
		mu 0 4 2610 2607 2612 2613
		f 4 -4368 -4361 4370 4371
		mu 0 4 2612 2607 2606 2614
		f 4 4372 4373 4374 -4366
		mu 0 4 2610 2615 2616 2611
		f 4 -4374 4375 -4340 4376
		mu 0 4 2617 2618 2588 2591
		f 4 -4376 4377 4378 -4347
		mu 0 4 2588 2618 2619 2596
		f 4 -4378 -4373 -4370 4379
		mu 0 4 2620 2615 2610 2613
		f 4 4380 4381 -4367 -4375
		mu 0 4 2616 2621 2608 2611
		f 4 -4382 4382 4383 -4363
		mu 0 4 2608 2621 2622 2609
		f 4 -4383 4384 -4343 4385
		mu 0 4 2623 2624 2590 2593
		f 4 -4385 -4381 -4377 -4339
		mu 0 4 2590 2624 2617 2591
		f 4 4386 4387 -4348 -4379
		mu 0 4 2619 2625 2594 2596
		f 4 -4388 4388 4389 -4345
		mu 0 4 2594 2625 2626 2595
		f 4 -4389 4390 -4372 4391
		mu 0 4 2627 2628 2612 2614
		f 4 -4391 -4387 -4380 -4369
		mu 0 4 2612 2628 2620 2613
		f 4 4392 4393 4394 4395
		mu 0 4 2629 2630 2631 2632
		f 4 -4394 4396 -4360 4397
		mu 0 4 2631 2630 2603 2605
		f 4 -4397 4398 4399 -4357
		mu 0 4 2603 2630 2633 2604
		f 4 -4399 -4393 4400 4401
		mu 0 4 2633 2630 2634 2635
		f 4 4402 4403 4404 -4354
		mu 0 4 2601 2636 2637 2602
		f 4 -4404 4405 4406 4407
		mu 0 4 2637 2636 2638 2639
		f 4 -4406 4408 -4402 4409
		mu 0 4 2638 2636 2633 2635
		f 4 -4409 -4403 -4358 -4400
		mu 0 4 2633 2636 2601 2604
		f 4 4410 4411 -4408 4412
		mu 0 4 2640 2641 2637 2639
		f 4 -4412 4413 -4355 -4405
		mu 0 4 2637 2641 2599 2602
		f 4 -4414 4414 4415 -4351
		mu 0 4 2599 2641 2642 2600
		f 4 -4415 -4411 4416 4417
		mu 0 4 2642 2641 2643 2644
		f 4 4418 4419 -4418 4420
		mu 0 4 2645 2646 2642 2644
		f 4 -4420 4421 -4352 -4416
		mu 0 4 2642 2646 2597 2600
		f 4 -4422 4422 -4398 -4359
		mu 0 4 2597 2646 2631 2605
		f 4 -4423 -4419 4423 -4395
		mu 0 4 2631 2646 2645 2632
		f 4 4424 4425 4426 -4390
		mu 0 4 2626 2647 2648 2595
		f 4 -4426 4427 4428 4429
		mu 0 4 2648 2647 2649 2650
		f 4 -4428 4430 4431 4432
		mu 0 4 2651 2652 2653 2654
		f 4 -4431 -4425 -4392 4433
		mu 0 4 2653 2652 2627 2614
		f 4 4434 4435 4436 4437
		mu 0 4 2655 2656 2657 2658
		f 4 -4436 4438 -4364 4439
		mu 0 4 2657 2656 2606 2609
		f 4 -4439 4440 -4434 -4371
		mu 0 4 2606 2656 2653 2614
		f 4 -4441 -4435 4441 -4432
		mu 0 4 2653 2656 2655 2654
		f 4 4442 4443 -4440 -4384
		mu 0 4 2622 2659 2657 2609
		f 4 -4444 4444 4445 -4437
		mu 0 4 2657 2659 2660 2658
		f 4 -4445 4446 4447 4448
		mu 0 4 2661 2662 2663 2664
		f 4 -4447 -4443 -4386 4449
		mu 0 4 2663 2662 2623 2593
		f 4 4450 4451 -4450 -4342
		mu 0 4 2592 2665 2663 2593
		f 4 -4452 4452 4453 -4448
		mu 0 4 2663 2665 2666 2664
		f 4 -4453 4454 -4430 4455
		mu 0 4 2666 2665 2648 2650
		f 4 -4455 -4451 -4346 -4427
		mu 0 4 2648 2665 2592 2595
		f 4 4456 4457 4458 -4429
		mu 0 4 2649 2667 2668 2650
		f 4 -4458 4459 -4396 4460
		mu 0 4 2668 2667 2629 2632
		f 4 -4460 4461 4462 -4401
		mu 0 4 2634 2669 2670 2635
		f 4 -4462 -4457 -4433 4463
		mu 0 4 2670 2669 2651 2654
		f 4 4464 4465 4466 -4407
		mu 0 4 2638 2671 2672 2639
		f 4 -4466 4467 -4438 4468
		mu 0 4 2672 2671 2655 2658
		f 4 -4468 4469 -4464 -4442
		mu 0 4 2655 2671 2670 2654
		f 4 -4470 -4465 -4410 -4463
		mu 0 4 2670 2671 2638 2635
		f 4 4470 4471 -4469 -4446
		mu 0 4 2660 2673 2672 2658
		f 4 -4472 4472 -4413 -4467
		mu 0 4 2672 2673 2640 2639
		f 4 -4473 4473 4474 -4417
		mu 0 4 2643 2674 2675 2644
		f 4 -4474 -4471 -4449 4475
		mu 0 4 2675 2674 2661 2664
		f 4 4476 4477 -4476 -4454
		mu 0 4 2666 2676 2675 2664
		f 4 -4478 4478 -4421 -4475
		mu 0 4 2675 2676 2645 2644
		f 4 -4479 4479 -4461 -4424
		mu 0 4 2645 2676 2668 2632
		f 4 -4480 -4477 -4456 -4459
		mu 0 4 2668 2676 2666 2650;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "miSamplesQualityR" -ln "miSamplesQualityR" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityG" -ln "miSamplesQualityG" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityB" -ln "miSamplesQualityB" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesQualityA" -ln "miSamplesQualityA" -dv 0.25 -min 0.01 
		-max 9999999.9000000004 -smn 0.1 -smx 2 -at "double";
	addAttr -ci true -sn "miSamplesMin" -ln "miSamplesMin" -dv 1 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesMax" -ln "miSamplesMax" -dv 100 -min 0.1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffR" -ln "miSamplesErrorCutoffR" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffG" -ln "miSamplesErrorCutoffG" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffB" -ln "miSamplesErrorCutoffB" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesErrorCutoffA" -ln "miSamplesErrorCutoffA" -min 0 
		-max 1 -at "double";
	addAttr -ci true -sn "miSamplesPerObject" -ln "miSamplesPerObject" -min 0 -max 1 
		-at "bool";
	addAttr -ci true -sn "miRastShadingSamples" -ln "miRastShadingSamples" -dv 1 -min 
		0.25 -at "double";
	addAttr -ci true -sn "miRastSamples" -ln "miRastSamples" -dv 3 -min 1 -at "long";
	addAttr -ci true -sn "miContrastAsColor" -ln "miContrastAsColor" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miProgMaxTime" -ln "miProgMaxTime" -min 0 -at "long";
	addAttr -ci true -sn "miProgSubsampleSize" -ln "miProgSubsampleSize" -dv 4 -min 
		1 -at "long";
	addAttr -ci true -sn "miTraceCameraMotionVectors" -ln "miTraceCameraMotionVectors" 
		-min 0 -max 1 -at "bool";
	addAttr -ci true -sn "miTraceCameraClip" -ln "miTraceCameraClip" -min 0 -max 1 -at "bool";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.5 0.5 0.5 0.5";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode phong -n "blinn1";
	setAttr ".cp" 6.311790943145752;
createNode shadingEngine -n "MordakTheFantastic:polySurface1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode shadingEngine -n "MordakTheFantastic:polySurface1SG1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
createNode file -n "PSD_blinn1_color";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "D:/Documents/3D Projects/Bosscraft/CharacterModels/MordakTheFantastic/textures/Mordak_Diffuse.png";
createNode place2dTexture -n "place2dTexture1";
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 0\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 0\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 0\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 0\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n"
		+ "                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n"
		+ "                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n"
		+ "                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n"
		+ "                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n"
		+ "                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n"
		+ "                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 0\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 0\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 48 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode groupId -n "groupId1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:2239]";
createNode skinCluster -n "skinCluster1";
	setAttr -s 2260 ".wl";
	setAttr -s 4 ".wl[0].w";
	setAttr ".wl[0].w[1]" 0.16133700863678196;
	setAttr ".wl[0].w[2]" 0.79370296351706915;
	setAttr ".wl[0].w[3]" 0.032716370164253883;
	setAttr ".wl[0].w[7]" 0.012243657681894915;
	setAttr -s 4 ".wl[1].w";
	setAttr ".wl[1].w[1]" 0.19260694561666161;
	setAttr ".wl[1].w[2]" 0.73172836184365309;
	setAttr ".wl[1].w[3]" 0.039055384096208429;
	setAttr ".wl[1].w[11]" 0.036609308443476818;
	setAttr -s 4 ".wl[2].w[1:4]"  0.0031936842685741929 0.50248881420451108 
		0.48861661960226394 0.005700881924650898;
	setAttr -s 4 ".wl[3].w[1:4]"  0.0061602922049229444 0.49991883883389265 
		0.48401816990439883 0.0099026990567854817;
	setAttr -s 4 ".wl[4].w[2:5]"  0.41190378599078276 0.42750751715144569 
		0.082860293023880399 0.077728403833891097;
	setAttr -s 4 ".wl[5].w[2:5]"  0.41217176596300154 0.41607682654314743 
		0.086987503089899054 0.084763904403952137;
	setAttr -s 4 ".wl[6].w";
	setAttr ".wl[6].w[1]" 0.31398916497244372;
	setAttr ".wl[6].w[2]" 0.53686905592289602;
	setAttr ".wl[6].w[3]" 0.07834575385977717;
	setAttr ".wl[6].w[7]" 0.07079602524488314;
	setAttr -s 4 ".wl[7].w";
	setAttr ".wl[7].w[1]" 0.3095434534784906;
	setAttr ".wl[7].w[2]" 0.49850245382028652;
	setAttr ".wl[7].w[3]" 0.076584367467550624;
	setAttr ".wl[7].w[11]" 0.11536972523367223;
	setAttr -s 4 ".wl[8].w";
	setAttr ".wl[8].w[1]" 0.14534546307902682;
	setAttr ".wl[8].w[2]" 0.83319031159787094;
	setAttr ".wl[8].w[3]" 0.014290487594322106;
	setAttr ".wl[8].w[11]" 0.0071737377287801694;
	setAttr -s 4 ".wl[9].w[2:5]"  0.49936902361209362 0.49936902361209384 
		0.0009481440498045144 0.00031380872600796322;
	setAttr -s 3 ".wl[10].w";
	setAttr ".wl[10].w[2]" 0.36995014333186876;
	setAttr ".wl[10].w[3]" 0.47986272517196804;
	setAttr ".wl[10].w[5]" 0.15018713149616308;
	setAttr -s 4 ".wl[11].w";
	setAttr ".wl[11].w[1]" 0.36376524565928403;
	setAttr ".wl[11].w[2]" 0.50187486316606655;
	setAttr ".wl[11].w[3]" 0.066378982317841242;
	setAttr ".wl[11].w[11]" 0.067980908856808228;
	setAttr -s 4 ".wl[12].w";
	setAttr ".wl[12].w[1]" 0.0470926881082879;
	setAttr ".wl[12].w[2]" 0.68943981366119089;
	setAttr ".wl[12].w[3]" 0.2541824414769735;
	setAttr ".wl[12].w[7]" 0.0092850567535476443;
	setAttr -s 4 ".wl[13].w";
	setAttr ".wl[13].w[1]" 0.0622403988384964;
	setAttr ".wl[13].w[2]" 0.6530391783267856;
	setAttr ".wl[13].w[3]" 0.26470956740672208;
	setAttr ".wl[13].w[11]" 0.020010855427995806;
	setAttr -s 4 ".wl[14].w[2:5]"  0.44246961182704053 0.50446496660959406 
		0.036419184820518709 0.016646236742846772;
	setAttr -s 4 ".wl[15].w[2:5]"  0.447393578392763 0.48205418020174068 
		0.046720028602270279 0.023832212803226036;
	setAttr -s 4 ".wl[16].w";
	setAttr ".wl[16].w[1]" 0.15128261362489417;
	setAttr ".wl[16].w[2]" 0.51192197741130552;
	setAttr ".wl[16].w[3]" 0.28354137337664548;
	setAttr ".wl[16].w[7]" 0.053254035587154892;
	setAttr -s 4 ".wl[17].w";
	setAttr ".wl[17].w[1]" 0.16080001695966722;
	setAttr ".wl[17].w[2]" 0.49246476475018219;
	setAttr ".wl[17].w[3]" 0.27464907814464851;
	setAttr ".wl[17].w[11]" 0.072086140145502159;
	setAttr -s 4 ".wl[18].w";
	setAttr ".wl[18].w[1]" 0.28981168073878055;
	setAttr ".wl[18].w[2]" 0.59266652947098475;
	setAttr ".wl[18].w[7]" 0.05968286801750864;
	setAttr ".wl[18].w[8]" 0.057838921772726114;
	setAttr -s 4 ".wl[19].w";
	setAttr ".wl[19].w[1]" 0.29547398143665921;
	setAttr ".wl[19].w[2]" 0.52932329289590407;
	setAttr ".wl[19].w[3]" 0.044217422980135632;
	setAttr ".wl[19].w[11]" 0.13098530268730105;
	setAttr -s 4 ".wl[20].w[1:4]"  0.027088322947765819 0.7638373784465311 
		0.20470971439942043 0.0043645842062826328;
	setAttr -s 4 ".wl[21].w[2:5]"  0.096745715991431239 0.89543660817358262 
		0.0059777851826954128 0.0018398906522906974;
	setAttr -s 4 ".wl[22].w";
	setAttr ".wl[22].w[1]" 0.16620101400110943;
	setAttr ".wl[22].w[2]" 0.48781415231570663;
	setAttr ".wl[22].w[3]" 0.27185598002996642;
	setAttr ".wl[22].w[5]" 0.074128853653217544;
	setAttr -s 4 ".wl[23].w";
	setAttr ".wl[23].w[1]" 0.11325124469985136;
	setAttr ".wl[23].w[2]" 0.55675416663879829;
	setAttr ".wl[23].w[3]" 0.27487213321137038;
	setAttr ".wl[23].w[11]" 0.055122455449980019;
	setAttr -s 4 ".wl[24].w";
	setAttr ".wl[24].w[1]" 0.094583026901975537;
	setAttr ".wl[24].w[2]" 0.59027314313755697;
	setAttr ".wl[24].w[3]" 0.2796922913012963;
	setAttr ".wl[24].w[7]" 0.035451538659171188;
	setAttr -s 4 ".wl[25].w";
	setAttr ".wl[25].w[1]" 0.41562870184945555;
	setAttr ".wl[25].w[2]" 0.48020265048635297;
	setAttr ".wl[25].w[7]" 0.043617060322277613;
	setAttr ".wl[25].w[11]" 0.060551587341913883;
	setAttr -s 4 ".wl[26].w";
	setAttr ".wl[26].w[1]" 0.37092974176703353;
	setAttr ".wl[26].w[2]" 0.48311473485656831;
	setAttr ".wl[26].w[3]" 0.034414701033462142;
	setAttr ".wl[26].w[11]" 0.11154082234293607;
	setAttr -s 4 ".wl[27].w";
	setAttr ".wl[27].w[1]" 0.36269959271559543;
	setAttr ".wl[27].w[2]" 0.49312086435627622;
	setAttr ".wl[27].w[3]" 0.01698067604171373;
	setAttr ".wl[27].w[11]" 0.1271988668864146;
	setAttr -s 4 ".wl[28].w";
	setAttr ".wl[28].w[1]" 0.26887836445959284;
	setAttr ".wl[28].w[2]" 0.68905739480604178;
	setAttr ".wl[28].w[3]" 0.011658462608049432;
	setAttr ".wl[28].w[11]" 0.030405778126315976;
	setAttr -s 4 ".wl[29].w";
	setAttr ".wl[29].w[1]" 0.21652634717971878;
	setAttr ".wl[29].w[2]" 0.77574508557767297;
	setAttr ".wl[29].w[3]" 0.003310298260371213;
	setAttr ".wl[29].w[11]" 0.0044182689822370429;
	setAttr -s 4 ".wl[30].w";
	setAttr ".wl[30].w[1]" 0.2319229214046408;
	setAttr ".wl[30].w[2]" 0.7508287107877053;
	setAttr ".wl[30].w[3]" 0.0090191209425970233;
	setAttr ".wl[30].w[7]" 0.0082292468650568395;
	setAttr -s 4 ".wl[31].w";
	setAttr ".wl[31].w[1]" 0.36898449024700947;
	setAttr ".wl[31].w[2]" 0.54934424032958451;
	setAttr ".wl[31].w[7]" 0.044103923224904043;
	setAttr ".wl[31].w[8]" 0.037567346198502098;
	setAttr -s 4 ".wl[32].w";
	setAttr ".wl[32].w[1]" 0.37854008055595656;
	setAttr ".wl[32].w[2]" 0.51395192500090259;
	setAttr ".wl[32].w[7]" 0.059526543545058583;
	setAttr ".wl[32].w[8]" 0.047981450898082298;
	setAttr -s 4 ".wl[33].w";
	setAttr ".wl[33].w[1]" 0.49541430766466299;
	setAttr ".wl[33].w[2]" 0.3344628842695011;
	setAttr ".wl[33].w[7]" 0.088964505842222444;
	setAttr ".wl[33].w[11]" 0.081158302223613463;
	setAttr -s 4 ".wl[34].w";
	setAttr ".wl[34].w[1]" 0.39521177326536289;
	setAttr ".wl[34].w[2]" 0.32700661414076337;
	setAttr ".wl[34].w[7]" 0.03483807812165482;
	setAttr ".wl[34].w[11]" 0.24294353447221878;
	setAttr -s 4 ".wl[35].w";
	setAttr ".wl[35].w[1]" 0.41375561401933253;
	setAttr ".wl[35].w[2]" 0.16399624131660859;
	setAttr ".wl[35].w[7]" 0.0084925306447262913;
	setAttr ".wl[35].w[11]" 0.41375561401933253;
	setAttr -s 4 ".wl[36].w";
	setAttr ".wl[36].w[1]" 0.94882108301081614;
	setAttr ".wl[36].w[2]" 0.048111715349137239;
	setAttr ".wl[36].w[7]" 0.0014142415184304142;
	setAttr ".wl[36].w[11]" 0.0016529601216161074;
	setAttr -s 4 ".wl[37].w";
	setAttr ".wl[37].w[1]" 0.47360670737165494;
	setAttr ".wl[37].w[2]" 0.4163460848733907;
	setAttr ".wl[37].w[7]" 0.0064351797999913935;
	setAttr ".wl[37].w[11]" 0.10361202795496292;
	setAttr -s 4 ".wl[38].w";
	setAttr ".wl[38].w[1]" 0.62490326450021849;
	setAttr ".wl[38].w[2]" 0.36205571093297123;
	setAttr ".wl[38].w[7]" 0.0047306725358155074;
	setAttr ".wl[38].w[11]" 0.0083103520309946478;
	setAttr -s 4 ".wl[39].w";
	setAttr ".wl[39].w[1]" 0.51866688081051526;
	setAttr ".wl[39].w[2]" 0.45419101059689893;
	setAttr ".wl[39].w[7]" 0.019137906212905193;
	setAttr ".wl[39].w[8]" 0.0080042023796806569;
	setAttr -s 4 ".wl[40].w";
	setAttr ".wl[40].w[1]" 0.49198301986152948;
	setAttr ".wl[40].w[2]" 0.33065674435516079;
	setAttr ".wl[40].w[7]" 0.1344906807987033;
	setAttr ".wl[40].w[8]" 0.042869554984606484;
	setAttr -s 4 ".wl[41].w";
	setAttr ".wl[41].w[1]" 0.44191254491319615;
	setAttr ".wl[41].w[2]" 0.36624325536373964;
	setAttr ".wl[41].w[7]" 0.13789066841149364;
	setAttr ".wl[41].w[8]" 0.053953531311570525;
	setAttr -s 4 ".wl[42].w";
	setAttr ".wl[42].w[1]" 0.13590601737928878;
	setAttr ".wl[42].w[2]" 0.83976398264290375;
	setAttr ".wl[42].w[3]" 0.018917541004367495;
	setAttr ".wl[42].w[11]" 0.0054124589734400709;
	setAttr -s 4 ".wl[43].w";
	setAttr ".wl[43].w[1]" 0.1593532107614766;
	setAttr ".wl[43].w[2]" 0.80288144580172671;
	setAttr ".wl[43].w[3]" 0.022510351347219272;
	setAttr ".wl[43].w[11]" 0.015254992089577464;
	setAttr -s 4 ".wl[44].w[1:4]"  0.00069156875819949217 0.49874705304639316 
		0.49874705304639338 0.0018143251490138203;
	setAttr -s 4 ".wl[45].w[1:4]"  0.0015309193061119049 0.49737154199118255 
		0.49737154199118255 0.0037259967115230363;
	setAttr -s 3 ".wl[46].w";
	setAttr ".wl[46].w[2]" 0.39794482428041533;
	setAttr ".wl[46].w[3]" 0.48011133727006572;
	setAttr ".wl[46].w[5]" 0.12194383844951895;
	setAttr -s 3 ".wl[47].w";
	setAttr ".wl[47].w[2]" 0.40152683083099988;
	setAttr ".wl[47].w[3]" 0.47061343423564334;
	setAttr ".wl[47].w[5]" 0.12785973493335687;
	setAttr -s 4 ".wl[48].w";
	setAttr ".wl[48].w[1]" 0.34613188555171498;
	setAttr ".wl[48].w[2]" 0.51982691010446958;
	setAttr ".wl[48].w[3]" 0.073731162559681818;
	setAttr ".wl[48].w[7]" 0.060310041784133725;
	setAttr -s 4 ".wl[49].w";
	setAttr ".wl[49].w[1]" 0.33915256336419186;
	setAttr ".wl[49].w[2]" 0.49604791472973103;
	setAttr ".wl[49].w[3]" 0.072166575712371145;
	setAttr ".wl[49].w[11]" 0.092632946193706031;
	setAttr -s 4 ".wl[50].w";
	setAttr ".wl[50].w[1]" 0.10423968490869936;
	setAttr ".wl[50].w[2]" 0.78082548328645229;
	setAttr ".wl[50].w[3]" 0.10101117951464898;
	setAttr ".wl[50].w[7]" 0.01392365229019942;
	setAttr -s 4 ".wl[51].w[1:4]"  0.012861641732615305 0.56710313441127891 
		0.41120225952561634 0.0088329643304894765;
	setAttr -s 4 ".wl[52].w";
	setAttr ".wl[52].w[1]" 0.1281462600070602;
	setAttr ".wl[52].w[2]" 0.72563825526558157;
	setAttr ".wl[52].w[3]" 0.11204958092960622;
	setAttr ".wl[52].w[11]" 0.034165903797751945;
	setAttr -s 4 ".wl[53].w[1:4]"  0.019923693710160392 0.55344411085791478 
		0.41394648714689736 0.012685708285027537;
	setAttr -s 4 ".wl[54].w[2:5]"  0.48800686524962206 0.49406178301398873 
		0.01304788991981962 0.0048834618165697202;
	setAttr -s 4 ".wl[55].w[2:5]"  0.41300857030963806 0.4722380813564363 
		0.067797827600868291 0.046955520733057268;
	setAttr -s 4 ".wl[56].w[2:5]"  0.48504690689881735 0.48582666347252101 
		0.020513954360066509 0.0086124752685950885;
	setAttr -s 4 ".wl[57].w[2:5]"  0.41572939038975215 0.45312398976791884 
		0.075272417111643161 0.055874202730685878;
	setAttr -s 3 ".wl[58].w";
	setAttr ".wl[58].w[2]" 0.50412235775106362;
	setAttr ".wl[58].w[3]" 0.41072005850710608;
	setAttr ".wl[58].w[5]" 0.085157583741830412;
	setAttr -s 4 ".wl[59].w";
	setAttr ".wl[59].w[1]" 0.23815344807551664;
	setAttr ".wl[59].w[2]" 0.53247245413830468;
	setAttr ".wl[59].w[3]" 0.15796300309087277;
	setAttr ".wl[59].w[7]" 0.07141109469530596;
	setAttr -s 3 ".wl[60].w";
	setAttr ".wl[60].w[2]" 0.50184906762340098;
	setAttr ".wl[60].w[3]" 0.40677072189793667;
	setAttr ".wl[60].w[5]" 0.091380210478662405;
	setAttr -s 4 ".wl[61].w";
	setAttr ".wl[61].w[1]" 0.24211237678857231;
	setAttr ".wl[61].w[2]" 0.50117311076258109;
	setAttr ".wl[61].w[3]" 0.15289680409873715;
	setAttr ".wl[61].w[11]" 0.10381770835010944;
	setAttr -s 4 ".wl[62].w";
	setAttr ".wl[62].w[1]" 0.30249473961195483;
	setAttr ".wl[62].w[2]" 0.5514384662041687;
	setAttr ".wl[62].w[7]" 0.074465275721208188;
	setAttr ".wl[62].w[8]" 0.071601518462668309;
	setAttr -s 4 ".wl[63].w";
	setAttr ".wl[63].w[1]" 0.23127107650543804;
	setAttr ".wl[63].w[2]" 0.70019611468687915;
	setAttr ".wl[63].w[3]" 0.03843525675286652;
	setAttr ".wl[63].w[7]" 0.030097552054816321;
	setAttr -s 4 ".wl[64].w";
	setAttr ".wl[64].w[1]" 0.30244954133560714;
	setAttr ".wl[64].w[2]" 0.50267191315561077;
	setAttr ".wl[64].w[3]" 0.059768546695404483;
	setAttr ".wl[64].w[11]" 0.13510999881337771;
	setAttr -s 4 ".wl[65].w";
	setAttr ".wl[65].w[1]" 0.25317556904977978;
	setAttr ".wl[65].w[2]" 0.6247257904249931;
	setAttr ".wl[65].w[3]" 0.042687560484210803;
	setAttr ".wl[65].w[11]" 0.07941108004101631;
	setAttr -s 4 ".wl[66].w";
	setAttr ".wl[66].w[1]" 0.078211302016553796;
	setAttr ".wl[66].w[2]" 0.85598098595815786;
	setAttr ".wl[66].w[3]" 0.058218786658021215;
	setAttr ".wl[66].w[11]" 0.0075889253672671714;
	setAttr -s 4 ".wl[67].w";
	setAttr ".wl[67].w[1]" 0.038949928513356095;
	setAttr ".wl[67].w[2]" 0.71888193934084754;
	setAttr ".wl[67].w[3]" 0.23344778445229553;
	setAttr ".wl[67].w[11]" 0.0087203476935009076;
	setAttr -s 4 ".wl[68].w[1:4]"  0.004552726090884229 0.57472351667427357 
		0.41732984612012031 0.0033939111147219389;
	setAttr -s 4 ".wl[69].w[1:4]"  0.030946998559496935 0.74450895090028935 
		0.21927366618512203 0.0052703843550917029;
	setAttr -s 4 ".wl[70].w[2:5]"  0.0091289425563407609 0.99084285065660016 
		2.2243338997601248e-05 5.9634480615386936e-06;
	setAttr -s 4 ".wl[71].w[2:5]"  0.31603669277009316 0.64620295050745857 
		0.027235960785695918 0.010524395936752416;
	setAttr -s 4 ".wl[72].w[2:5]"  0.27223495611399073 0.60868593281755556 
		0.070943214515816255 0.048135896552637579;
	setAttr -s 4 ".wl[73].w[2:5]"  0.25407082662680702 0.7229141486477475 
		0.017101377006597224 0.0059136477188483595;
	setAttr -s 3 ".wl[74].w";
	setAttr ".wl[74].w[2]" 0.45840649001101519;
	setAttr ".wl[74].w[3]" 0.39816947524004248;
	setAttr ".wl[74].w[5]" 0.14342403474894236;
	setAttr -s 4 ".wl[75].w";
	setAttr ".wl[75].w[1]" 0.1678946199533945;
	setAttr ".wl[75].w[2]" 0.48890256379604552;
	setAttr ".wl[75].w[3]" 0.27357132238531495;
	setAttr ".wl[75].w[5]" 0.069631493865245128;
	setAttr -s 4 ".wl[76].w";
	setAttr ".wl[76].w[1]" 0.27734974798883028;
	setAttr ".wl[76].w[2]" 0.51412826831758862;
	setAttr ".wl[76].w[3]" 0.14427303364861674;
	setAttr ".wl[76].w[11]" 0.064248950044964365;
	setAttr -s 4 ".wl[77].w";
	setAttr ".wl[77].w[1]" 0.1615269328737087;
	setAttr ".wl[77].w[2]" 0.49448287753736397;
	setAttr ".wl[77].w[3]" 0.2759001619535526;
	setAttr ".wl[77].w[5]" 0.068090027635374806;
	setAttr -s 4 ".wl[78].w";
	setAttr ".wl[78].w[1]" 0.21018977377699488;
	setAttr ".wl[78].w[2]" 0.5687379221375255;
	setAttr ".wl[78].w[3]" 0.11897536831181572;
	setAttr ".wl[78].w[11]" 0.1020969357736638;
	setAttr -s 4 ".wl[79].w";
	setAttr ".wl[79].w[1]" 0.14019005445756472;
	setAttr ".wl[79].w[2]" 0.51583744869955539;
	setAttr ".wl[79].w[3]" 0.27454842233617399;
	setAttr ".wl[79].w[11]" 0.069424074506705874;
	setAttr -s 4 ".wl[80].w[1:4]"  0.044787273041482875 0.49627397748314961 
		0.41682798100829288 0.042110768467074587;
	setAttr -s 4 ".wl[81].w";
	setAttr ".wl[81].w[1]" 0.086308074870944662;
	setAttr ".wl[81].w[2]" 0.60425458330112236;
	setAttr ".wl[81].w[3]" 0.2733854834803367;
	setAttr ".wl[81].w[11]" 0.036051858347596263;
	setAttr -s 4 ".wl[82].w";
	setAttr ".wl[82].w[1]" 0.19374238723112586;
	setAttr ".wl[82].w[2]" 0.62772543153929772;
	setAttr ".wl[82].w[3]" 0.11855697029854281;
	setAttr ".wl[82].w[7]" 0.059975210931033501;
	setAttr -s 4 ".wl[83].w";
	setAttr ".wl[83].w[1]" 0.067328750489796474;
	setAttr ".wl[83].w[2]" 0.64235374737547979;
	setAttr ".wl[83].w[3]" 0.27076345982541156;
	setAttr ".wl[83].w[7]" 0.019554042309312285;
	setAttr -s 4 ".wl[84].w[1:4]"  0.033252075746823914 0.5058970469402102 
		0.42549814877929149 0.035352728533674466;
	setAttr -s 4 ".wl[85].w";
	setAttr ".wl[85].w[1]" 0.12524175103204815;
	setAttr ".wl[85].w[2]" 0.54152285597519989;
	setAttr ".wl[85].w[3]" 0.2829795132149327;
	setAttr ".wl[85].w[7]" 0.050255879777819097;
	setAttr -s 4 ".wl[86].w";
	setAttr ".wl[86].w[1]" 0.39246480151777385;
	setAttr ".wl[86].w[2]" 0.50002262930582819;
	setAttr ".wl[86].w[7]" 0.04475606913059451;
	setAttr ".wl[86].w[11]" 0.062756500045803479;
	setAttr -s 4 ".wl[87].w";
	setAttr ".wl[87].w[1]" 0.33585624759099497;
	setAttr ".wl[87].w[2]" 0.50406676175799003;
	setAttr ".wl[87].w[3]" 0.052822120315335119;
	setAttr ".wl[87].w[11]" 0.1072548703356799;
	setAttr -s 4 ".wl[88].w";
	setAttr ".wl[88].w[1]" 0.39837157715458626;
	setAttr ".wl[88].w[2]" 0.48166018912180475;
	setAttr ".wl[88].w[7]" 0.033448879202758151;
	setAttr ".wl[88].w[11]" 0.086519354520850772;
	setAttr -s 4 ".wl[89].w";
	setAttr ".wl[89].w[1]" 0.32811231779640765;
	setAttr ".wl[89].w[2]" 0.52299961171216414;
	setAttr ".wl[89].w[3]" 0.02696243268558279;
	setAttr ".wl[89].w[11]" 0.12192563780584556;
	setAttr -s 4 ".wl[90].w";
	setAttr ".wl[90].w[1]" 0.36416602590520408;
	setAttr ".wl[90].w[2]" 0.47827084984760043;
	setAttr ".wl[90].w[3]" 0.025132409749806319;
	setAttr ".wl[90].w[11]" 0.13243071449738916;
	setAttr -s 4 ".wl[91].w";
	setAttr ".wl[91].w[1]" 0.21227554405254631;
	setAttr ".wl[91].w[2]" 0.73615372215537866;
	setAttr ".wl[91].w[3]" 0.02128119346538878;
	setAttr ".wl[91].w[11]" 0.030289540326686161;
	setAttr -s 4 ".wl[92].w";
	setAttr ".wl[92].w[1]" 0.33244315406721026;
	setAttr ".wl[92].w[2]" 0.5801317808863975;
	setAttr ".wl[92].w[3]" 0.014571746181644286;
	setAttr ".wl[92].w[11]" 0.072853318864748023;
	setAttr -s 4 ".wl[93].w";
	setAttr ".wl[93].w[1]" 0.16367418216703822;
	setAttr ".wl[93].w[2]" 0.82483739464982087;
	setAttr ".wl[93].w[3]" 0.0063218079051126008;
	setAttr ".wl[93].w[11]" 0.0051666152780283098;
	setAttr -s 4 ".wl[94].w";
	setAttr ".wl[94].w[1]" 0.23167814833177008;
	setAttr ".wl[94].w[2]" 0.75176323163093983;
	setAttr ".wl[94].w[3]" 0.0057707961170345521;
	setAttr ".wl[94].w[11]" 0.010787823920255367;
	setAttr -s 4 ".wl[95].w";
	setAttr ".wl[95].w[1]" 0.17591021887659028;
	setAttr ".wl[95].w[2]" 0.79826341873550166;
	setAttr ".wl[95].w[3]" 0.016813670042064931;
	setAttr ".wl[95].w[7]" 0.0090126923458431537;
	setAttr -s 4 ".wl[96].w";
	setAttr ".wl[96].w[1]" 0.2001048024803079;
	setAttr ".wl[96].w[2]" 0.79194631002871418;
	setAttr ".wl[96].w[3]" 0.0045399529181539841;
	setAttr ".wl[96].w[11]" 0.003408934572823897;
	setAttr -s 4 ".wl[97].w";
	setAttr ".wl[97].w[1]" 0.32535332320346627;
	setAttr ".wl[97].w[2]" 0.58343459400855513;
	setAttr ".wl[97].w[7]" 0.04762096459148335;
	setAttr ".wl[97].w[8]" 0.043591118196495124;
	setAttr -s 4 ".wl[98].w";
	setAttr ".wl[98].w[1]" 0.31508106666377228;
	setAttr ".wl[98].w[2]" 0.64578523721624592;
	setAttr ".wl[98].w[7]" 0.021098735320963676;
	setAttr ".wl[98].w[8]" 0.018034960799018118;
	setAttr -s 4 ".wl[99].w";
	setAttr ".wl[99].w[1]" 0.34258052916418474;
	setAttr ".wl[99].w[2]" 0.54299473480363036;
	setAttr ".wl[99].w[3]" 0.053310506782032689;
	setAttr ".wl[99].w[7]" 0.06111422925015228;
	setAttr -s 4 ".wl[100].w";
	setAttr ".wl[100].w[1]" 0.37150275260537513;
	setAttr ".wl[100].w[2]" 0.51979954893860258;
	setAttr ".wl[100].w[7]" 0.058835354292026951;
	setAttr ".wl[100].w[8]" 0.049862344163995424;
	setAttr -s 4 ".wl[101].w";
	setAttr ".wl[101].w[1]" 0.40672686035934719;
	setAttr ".wl[101].w[2]" 0.50020740457551849;
	setAttr ".wl[101].w[7]" 0.052935802728189006;
	setAttr ".wl[101].w[11]" 0.040129932336945319;
	setAttr -s 4 ".wl[102].w";
	setAttr ".wl[102].w[1]" 0.43547945315077136;
	setAttr ".wl[102].w[2]" 0.42823059869707419;
	setAttr ".wl[102].w[7]" 0.062115816850749808;
	setAttr ".wl[102].w[11]" 0.0741741313014046;
	setAttr -s 4 ".wl[103].w";
	setAttr ".wl[103].w[1]" 0.39437302764396609;
	setAttr ".wl[103].w[2]" 0.3943700901866754;
	setAttr ".wl[103].w[7]" 0.030094090452016314;
	setAttr ".wl[103].w[11]" 0.1811627917173422;
	setAttr -s 4 ".wl[104].w";
	setAttr ".wl[104].w[1]" 0.45582072918092603;
	setAttr ".wl[104].w[2]" 0.31711070163137317;
	setAttr ".wl[104].w[7]" 0.057634052047490406;
	setAttr ".wl[104].w[11]" 0.16943451714021046;
	setAttr -s 4 ".wl[105].w";
	setAttr ".wl[105].w[1]" 0.35686904821991144;
	setAttr ".wl[105].w[2]" 0.35699402895467625;
	setAttr ".wl[105].w[7]" 0.011482298067279559;
	setAttr ".wl[105].w[11]" 0.27465462475813274;
	setAttr -s 4 ".wl[106].w";
	setAttr ".wl[106].w[1]" 0.3788937949256711;
	setAttr ".wl[106].w[2]" 0.22146849287384679;
	setAttr ".wl[106].w[7]" 0.020743917274811084;
	setAttr ".wl[106].w[11]" 0.3788937949256711;
	setAttr -s 4 ".wl[107].w";
	setAttr ".wl[107].w[1]" 0.70243722124935382;
	setAttr ".wl[107].w[2]" 0.19949878781368124;
	setAttr ".wl[107].w[7]" 0.0083553977458770048;
	setAttr ".wl[107].w[11]" 0.089708593191087996;
	setAttr -s 4 ".wl[108].w";
	setAttr ".wl[108].w[1]" 0.66919044351671808;
	setAttr ".wl[108].w[2]" 0.24903616219752525;
	setAttr ".wl[108].w[7]" 0.043020443451556728;
	setAttr ".wl[108].w[11]" 0.038752950834199933;
	setAttr -s 4 ".wl[109].w";
	setAttr ".wl[109].w[1]" 0.45215882300936378;
	setAttr ".wl[109].w[2]" 0.48274083488674108;
	setAttr ".wl[109].w[7]" 0.0053198419526744208;
	setAttr ".wl[109].w[11]" 0.059780500151220668;
	setAttr -s 4 ".wl[110].w";
	setAttr ".wl[110].w[1]" 0.37753394678074742;
	setAttr ".wl[110].w[2]" 0.26569588380926862;
	setAttr ".wl[110].w[7]" 0.0081784652711633845;
	setAttr ".wl[110].w[11]" 0.34859170413882062;
	setAttr -s 4 ".wl[111].w";
	setAttr ".wl[111].w[1]" 0.47805254504173816;
	setAttr ".wl[111].w[2]" 0.51320365661560763;
	setAttr ".wl[111].w[7]" 0.0028511674158716825;
	setAttr ".wl[111].w[11]" 0.0058926309267825277;
	setAttr -s 4 ".wl[112].w";
	setAttr ".wl[112].w[1]" 0.57177838919836255;
	setAttr ".wl[112].w[2]" 0.38145266615682072;
	setAttr ".wl[112].w[7]" 0.0059972750549393489;
	setAttr ".wl[112].w[11]" 0.040771669589877366;
	setAttr -s 4 ".wl[113].w";
	setAttr ".wl[113].w[1]" 0.97947628999740388;
	setAttr ".wl[113].w[2]" 0.019928898405422942;
	setAttr ".wl[113].w[7]" 0.00024159422768931782;
	setAttr ".wl[113].w[11]" 0.00035321736948392644;
	setAttr -s 4 ".wl[114].w";
	setAttr ".wl[114].w[1]" 0.4646088121908723;
	setAttr ".wl[114].w[2]" 0.51459487966083095;
	setAttr ".wl[114].w[7]" 0.012790676024855568;
	setAttr ".wl[114].w[8]" 0.0080056321234411917;
	setAttr -s 4 ".wl[115].w";
	setAttr ".wl[115].w[1]" 0.5970963968533014;
	setAttr ".wl[115].w[2]" 0.38611677876319833;
	setAttr ".wl[115].w[7]" 0.010872266756433597;
	setAttr ".wl[115].w[11]" 0.0059145576270668012;
	setAttr -s 4 ".wl[116].w";
	setAttr ".wl[116].w[1]" 0.4357473989058096;
	setAttr ".wl[116].w[2]" 0.43747662195224224;
	setAttr ".wl[116].w[7]" 0.077072922139731395;
	setAttr ".wl[116].w[8]" 0.049703057002216743;
	setAttr -s 4 ".wl[117].w";
	setAttr ".wl[117].w[1]" 0.54401833326174898;
	setAttr ".wl[117].w[2]" 0.37375409997883591;
	setAttr ".wl[117].w[7]" 0.061102517467140911;
	setAttr ".wl[117].w[8]" 0.021125049292274191;
	setAttr -s 4 ".wl[118].w";
	setAttr ".wl[118].w[1]" 0.77670786573993356;
	setAttr ".wl[118].w[2]" 0.19071653464804678;
	setAttr ".wl[118].w[7]" 0.027280703049059211;
	setAttr ".wl[118].w[11]" 0.005294896562960439;
	setAttr -s 4 ".wl[119].w";
	setAttr ".wl[119].w[1]" 0.42346865879043655;
	setAttr ".wl[119].w[2]" 0.42455945776617687;
	setAttr ".wl[119].w[7]" 0.094782665993183926;
	setAttr ".wl[119].w[8]" 0.057189217450202633;
	setAttr -s 4 ".wl[120].w";
	setAttr ".wl[120].w[1]" 0.4428743184743324;
	setAttr ".wl[120].w[2]" 0.31007344813494292;
	setAttr ".wl[120].w[7]" 0.18643653800678345;
	setAttr ".wl[120].w[8]" 0.060615695383941272;
	setAttr -s 4 ".wl[121].w";
	setAttr ".wl[121].w[1]" 0.48371986776241616;
	setAttr ".wl[121].w[2]" 0.33588214047129311;
	setAttr ".wl[121].w[7]" 0.13549052927729119;
	setAttr ".wl[121].w[11]" 0.044907462488999607;
	setAttr -s 4 ".wl[122].w";
	setAttr ".wl[122].w[1]" 0.095806998783871347;
	setAttr ".wl[122].w[2]" 0.8093964723267999;
	setAttr ".wl[122].w[3]" 0.079546586499861724;
	setAttr ".wl[122].w[11]" 0.01524994238946711;
	setAttr -s 4 ".wl[123].w[1:4]"  0.0089500322336297702 0.56956870881508948 
		0.41546825994921016 0.0060129990020704937;
	setAttr -s 4 ".wl[124].w[1:4]"  0.0061380951393626565 0.58098030757090369 
		0.40857665172433988 0.0043049455653937545;
	setAttr -s 4 ".wl[125].w";
	setAttr ".wl[125].w[1]" 0.08017823709510101;
	setAttr ".wl[125].w[2]" 0.84306176417802026;
	setAttr ".wl[125].w[3]" 0.07064052684095587;
	setAttr ".wl[125].w[11]" 0.0061194718859228399;
	setAttr -s 4 ".wl[126].w[2:5]"  0.40165795270679694 0.59022420476205262 
		0.0061353887276692576 0.0019824538034811934;
	setAttr -s 4 ".wl[127].w[2:5]"  0.33586343064011204 0.5356393533493089 
		0.075759591093764747 0.052737624916814357;
	setAttr -s 4 ".wl[128].w[2:5]"  0.32059162845221262 0.56606395153178457 
		0.068403679581645463 0.04494074043435739;
	setAttr -s 4 ".wl[129].w[2:5]"  0.31552821903807754 0.68156597555513854 
		0.0022488303809366618 0.00065697502584723697;
	setAttr -s 3 ".wl[130].w";
	setAttr ".wl[130].w[2]" 0.47562650490274588;
	setAttr ".wl[130].w[3]" 0.39833656756695124;
	setAttr ".wl[130].w[5]" 0.12603692753030291;
	setAttr -s 4 ".wl[131].w";
	setAttr ".wl[131].w[1]" 0.26275069322443623;
	setAttr ".wl[131].w[2]" 0.50320364713409171;
	setAttr ".wl[131].w[3]" 0.14895907032173006;
	setAttr ".wl[131].w[11]" 0.085086589319741987;
	setAttr -s 4 ".wl[132].w";
	setAttr ".wl[132].w[1]" 0.26387713361532816;
	setAttr ".wl[132].w[2]" 0.5243633462038223;
	setAttr ".wl[132].w[3]" 0.15309330402412555;
	setAttr ".wl[132].w[7]" 0.058666216156724051;
	setAttr -s 3 ".wl[133].w";
	setAttr ".wl[133].w[2]" 0.47607866749391325;
	setAttr ".wl[133].w[3]" 0.40129667409036923;
	setAttr ".wl[133].w[5]" 0.1226246584157175;
	setAttr -s 4 ".wl[134].w";
	setAttr ".wl[134].w[1]" 0.55797627654521897;
	setAttr ".wl[134].w[2]" 0.24808166317205846;
	setAttr ".wl[134].w[7]" 0.031537563380471062;
	setAttr ".wl[134].w[11]" 0.16240449690225148;
	setAttr -s 4 ".wl[135].w";
	setAttr ".wl[135].w[1]" 0.75085881785956654;
	setAttr ".wl[135].w[2]" 0.19725456316371354;
	setAttr ".wl[135].w[7]" 0.004477419393581007;
	setAttr ".wl[135].w[11]" 0.047409199583138967;
	setAttr -s 4 ".wl[136].w";
	setAttr ".wl[136].w[1]" 0.81391993616622671;
	setAttr ".wl[136].w[2]" 0.17306834812880212;
	setAttr ".wl[136].w[7]" 0.010082618384328758;
	setAttr ".wl[136].w[11]" 0.0029290973206423671;
	setAttr -s 4 ".wl[137].w";
	setAttr ".wl[137].w[1]" 0.60896416727268066;
	setAttr ".wl[137].w[2]" 0.26300700922417986;
	setAttr ".wl[137].w[7]" 0.10618957229650158;
	setAttr ".wl[137].w[11]" 0.021839251206637822;
	setAttr -s 4 ".wl[138].w";
	setAttr ".wl[138].w[1]" 0.22870618149981317;
	setAttr ".wl[138].w[2]" 0.52188449098598866;
	setAttr ".wl[138].w[3]" 0.13641136418472127;
	setAttr ".wl[138].w[11]" 0.1129979633294769;
	setAttr -s 4 ".wl[139].w[1:4]"  0.065500154997914437 0.47774352694173772 
		0.39428224167178422 0.062474076388563671;
	setAttr -s 4 ".wl[140].w[1:4]"  0.031011651670742216 0.52439072957391464 
		0.42083543824147951 0.023762180513863636;
	setAttr -s 4 ".wl[141].w";
	setAttr ".wl[141].w[1]" 0.17087362035985287;
	setAttr ".wl[141].w[2]" 0.64565344196877106;
	setAttr ".wl[141].w[3]" 0.11760460123150618;
	setAttr ".wl[141].w[11]" 0.065868336439869904;
	setAttr -s 4 ".wl[142].w";
	setAttr ".wl[142].w[1]" 0.14673829264040728;
	setAttr ".wl[142].w[2]" 0.71006075861345841;
	setAttr ".wl[142].w[3]" 0.11143351807593647;
	setAttr ".wl[142].w[7]" 0.031767430670197841;
	setAttr -s 4 ".wl[143].w[1:4]"  0.020916913712767105 0.53586716620665098 
		0.42540623667672717 0.017809683403854817;
	setAttr -s 4 ".wl[144].w[1:4]"  0.053903547320849732 0.48600304489931256 
		0.40260659555389766 0.057486812225940119;
	setAttr -s 4 ".wl[145].w";
	setAttr ".wl[145].w[1]" 0.21921455214776994;
	setAttr ".wl[145].w[2]" 0.56562237187238351;
	setAttr ".wl[145].w[3]" 0.13974976652096366;
	setAttr ".wl[145].w[7]" 0.075413309458882929;
	setAttr -s 4 ".wl[146].w";
	setAttr ".wl[146].w[1]" 0.36672647980210832;
	setAttr ".wl[146].w[2]" 0.4978033245777862;
	setAttr ".wl[146].w[3]" 0.049408936917957752;
	setAttr ".wl[146].w[11]" 0.086061258702147619;
	setAttr -s 4 ".wl[147].w";
	setAttr ".wl[147].w[1]" 0.33125276651153074;
	setAttr ".wl[147].w[2]" 0.5034513421011082;
	setAttr ".wl[147].w[3]" 0.039123054128352604;
	setAttr ".wl[147].w[11]" 0.12617283725900846;
	setAttr -s 4 ".wl[148].w";
	setAttr ".wl[148].w[1]" 0.2834492931372834;
	setAttr ".wl[148].w[2]" 0.62130034591086758;
	setAttr ".wl[148].w[3]" 0.0248188042199253;
	setAttr ".wl[148].w[11]" 0.070431556731923681;
	setAttr -s 4 ".wl[149].w";
	setAttr ".wl[149].w[1]" 0.17655363298387586;
	setAttr ".wl[149].w[2]" 0.80090138127632959;
	setAttr ".wl[149].w[3]" 0.010856928050360443;
	setAttr ".wl[149].w[11]" 0.011688057689434123;
	setAttr -s 4 ".wl[150].w";
	setAttr ".wl[150].w[1]" 0.14842003056536732;
	setAttr ".wl[150].w[2]" 0.83896823363518525;
	setAttr ".wl[150].w[3]" 0.0086846168488879643;
	setAttr ".wl[150].w[11]" 0.0039271189505593902;
	setAttr -s 4 ".wl[151].w";
	setAttr ".wl[151].w[1]" 0.26046028527125237;
	setAttr ".wl[151].w[2]" 0.69505237353332738;
	setAttr ".wl[151].w[3]" 0.021347992309506469;
	setAttr ".wl[151].w[7]" 0.023139348885913937;
	setAttr -s 4 ".wl[152].w";
	setAttr ".wl[152].w[1]" 0.33280823483535366;
	setAttr ".wl[152].w[2]" 0.54958112336565501;
	setAttr ".wl[152].w[7]" 0.06155706193700592;
	setAttr ".wl[152].w[8]" 0.056053579861985496;
	setAttr -s 4 ".wl[153].w";
	setAttr ".wl[153].w[1]" 0.37503238615390955;
	setAttr ".wl[153].w[2]" 0.52095606640424019;
	setAttr ".wl[153].w[3]" 0.050081609179249399;
	setAttr ".wl[153].w[7]" 0.053929938262600799;
	setAttr -s 4 ".wl[154].w";
	setAttr ".wl[154].w[1]" 0.41972870369403559;
	setAttr ".wl[154].w[2]" 0.41653515810095509;
	setAttr ".wl[154].w[7]" 0.04448836933781887;
	setAttr ".wl[154].w[11]" 0.11924776886719049;
	setAttr -s 4 ".wl[155].w";
	setAttr ".wl[155].w[1]" 0.36563062428543164;
	setAttr ".wl[155].w[2]" 0.36561895252660326;
	setAttr ".wl[155].w[7]" 0.018779229957998481;
	setAttr ".wl[155].w[11]" 0.24997119322996672;
	setAttr -s 4 ".wl[156].w";
	setAttr ".wl[156].w[1]" 0.41062714831995584;
	setAttr ".wl[156].w[2]" 0.41708174527384861;
	setAttr ".wl[156].w[7]" 0.0078019870603547948;
	setAttr ".wl[156].w[11]" 0.16448911934584071;
	setAttr -s 4 ".wl[157].w";
	setAttr ".wl[157].w[1]" 0.47032971200139578;
	setAttr ".wl[157].w[2]" 0.50929687629778075;
	setAttr ".wl[157].w[7]" 0.003386236179632364;
	setAttr ".wl[157].w[11]" 0.016987175521191087;
	setAttr -s 4 ".wl[158].w";
	setAttr ".wl[158].w[1]" 0.46906383287424819;
	setAttr ".wl[158].w[2]" 0.5220834067039275;
	setAttr ".wl[158].w[7]" 0.0047074121186653781;
	setAttr ".wl[158].w[11]" 0.0041453483031589867;
	setAttr -s 4 ".wl[159].w";
	setAttr ".wl[159].w[1]" 0.46317815194952805;
	setAttr ".wl[159].w[2]" 0.47868175641818916;
	setAttr ".wl[159].w[7]" 0.035122883303816826;
	setAttr ".wl[159].w[8]" 0.023017208328465915;
	setAttr -s 4 ".wl[160].w";
	setAttr ".wl[160].w[1]" 0.41752241225608938;
	setAttr ".wl[160].w[2]" 0.41874762002896199;
	setAttr ".wl[160].w[7]" 0.099759154003677603;
	setAttr ".wl[160].w[8]" 0.063970813711271057;
	setAttr -s 4 ".wl[161].w";
	setAttr ".wl[161].w[1]" 0.43899749396167553;
	setAttr ".wl[161].w[2]" 0.43615888893711374;
	setAttr ".wl[161].w[7]" 0.079167440664092217;
	setAttr ".wl[161].w[11]" 0.045676176437118569;
	setAttr -s 2 ".wl[162].w[2:3]"  0.52544614653175525 0.47455385346824475;
	setAttr -s 2 ".wl[163].w[2:3]"  0.5211629580542313 0.47883704194576865;
	setAttr -s 2 ".wl[164].w[2:3]"  0.52115212180939041 0.47884787819060959;
	setAttr -s 4 ".wl[165].w[1:4]"  0.03259202540980622 0.49053546642626161 
		0.43599760875238569 0.040874899411546435;
	setAttr -s 4 ".wl[166].w[1:4]"  0.017056885413282253 0.50515174547219455 
		0.46114743029935923 0.016643938815163996;
	setAttr -s 4 ".wl[167].w[1:4]"  0.0044952388775715757 0.53650468270329221 
		0.4548848809004839 0.0041151975186523183;
	setAttr -s 4 ".wl[168].w[1:4]"  0.024641698483284756 0.49877240038294718 
		0.4545380418955986 0.022047859238169532;
	setAttr -s 4 ".wl[169].w[1:4]"  0.043806461166717742 0.48209302175896823 
		0.42602226294219686 0.048078254132117207;
	setAttr -s 4 ".wl[170].w[3:6]"  0.0027274487173496093 0.0075004122667244374 
		0.5360610715444204 0.45371106747150569;
	setAttr -s 4 ".wl[171].w[3:6]"  0.00066368403611443967 0.0015794982256049684 
		0.50320523008731277 0.49455158765096779;
	setAttr -s 4 ".wl[172].w[3:6]"  0.00044679882495786779 0.001202896399943702 
		0.58298009678115303 0.41537020799394547;
	setAttr -s 4 ".wl[173].w[3:6]"  9.9573111147077399e-05 0.0002616565595279095 
		0.49981938516466251 0.49981938516466251;
	setAttr -s 4 ".wl[174].w[3:6]"  1.002697880922633e-05 3.7843192591968967e-05 
		0.51782508931775251 0.48212704051084632;
	setAttr -s 4 ".wl[175].w[3:6]"  0.00011859107880675651 0.00035093014290080339 
		0.49976523938914624 0.49976523938914624;
	setAttr -s 4 ".wl[176].w[3:6]"  0.00080405209734952111 0.0030458923620484608 
		0.50339896410102969 0.4927510914395723;
	setAttr -s 4 ".wl[177].w[3:6]"  0.0015268857009622843 0.0041040855475409817 
		0.49718451437574834 0.49718451437574834;
	setAttr -s 4 ".wl[178].w[1:4]"  0.010236369486242076 0.48817044854349123 
		0.48450149265637887 0.017091689313887871;
	setAttr -s 4 ".wl[179].w[1:4]"  0.0027052653188691452 0.49669278048458959 
		0.49617302824865983 0.0044289259478815061;
	setAttr -s 4 ".wl[180].w[1:4]"  0.0060692496576966104 0.4920278714542376 
		0.49051435654936754 0.011388522338698272;
	setAttr -s 4 ".wl[181].w[2:5]"  0.45926751065884258 0.46027157071289759 
		0.052734109182416285 0.027726809445843548;
	setAttr -s 2 ".wl[182].w[2:3]"  0.48061179575969609 0.51938820424030396;
	setAttr -s 2 ".wl[183].w[2:3]"  0.47453228698694333 0.52546771301305673;
	setAttr -s 2 ".wl[184].w[2:3]"  0.48876801746939863 0.51123198253060143;
	setAttr -s 4 ".wl[185].w[2:5]"  0.4551006596355755 0.4474757034943318 
		0.061642488792031311 0.035781148078061382;
	setAttr -s 2 ".wl[186].w[2:3]"  0.5163420113751056 0.48365798862489434;
	setAttr -s 2 ".wl[187].w[2:3]"  0.50624292458529296 0.49375707541470698;
	setAttr -s 2 ".wl[188].w[2:3]"  0.4878286406756891 0.5121713593243109;
	setAttr -s 2 ".wl[189].w[2:3]"  0.50179842743090974 0.49820157256909031;
	setAttr -s 3 ".wl[190].w";
	setAttr ".wl[190].w[2]" 0.42358003420068946;
	setAttr ".wl[190].w[3]" 0.40622377532289305;
	setAttr ".wl[190].w[5]" 0.17019619047641754;
	setAttr -s 2 ".wl[191].w[2:3]"  0.49469454370574006 0.50530545629425994;
	setAttr -s 4 ".wl[192].w";
	setAttr ".wl[192].w[2]" 0.36202893372727157;
	setAttr ".wl[192].w[3]" 0.34556459756550395;
	setAttr ".wl[192].w[7]" 0.14434367477238444;
	setAttr ".wl[192].w[8]" 0.14806279393484006;
	setAttr -s 4 ".wl[193].w";
	setAttr ".wl[193].w[2]" 0.3683052318718737;
	setAttr ".wl[193].w[3]" 0.36163200071579338;
	setAttr ".wl[193].w[4]" 0.14054429023525211;
	setAttr ".wl[193].w[8]" 0.12951847717708073;
	setAttr -s 4 ".wl[194].w[1:4]"  0.082754691539639907 0.42641239761359712 
		0.41927473405161825 0.071558176795144729;
	setAttr -s 4 ".wl[195].w[1:4]"  0.072578550872567774 0.42409710407673784 
		0.42360807810458828 0.079716266946106187;
	setAttr -s 4 ".wl[196].w[1:4]"  0.066634902264098364 0.4407168938359653 
		0.43839411679172019 0.054254087108216074;
	setAttr -s 4 ".wl[197].w[1:4]"  0.058272245644813897 0.4407718263395215 
		0.4407718263395215 0.06018410167614309;
	setAttr -s 4 ".wl[198].w[1:4]"  0.094800604161180871 0.41923649639830762 
		0.40993234316297633 0.076030556277535216;
	setAttr -s 4 ".wl[199].w[1:4]"  0.083835961213474919 0.41657385406357494 
		0.41512791387001335 0.084462270852936894;
	setAttr -s 4 ".wl[200].w";
	setAttr ".wl[200].w[2]" 0.33937173414743244;
	setAttr ".wl[200].w[3]" 0.31952103174503166;
	setAttr ".wl[200].w[11]" 0.17463286999707969;
	setAttr ".wl[200].w[12]" 0.16647436411045621;
	setAttr -s 4 ".wl[201].w";
	setAttr ".wl[201].w[2]" 0.35166148951288617;
	setAttr ".wl[201].w[3]" 0.34174467471473036;
	setAttr ".wl[201].w[11]" 0.15638175837648341;
	setAttr ".wl[201].w[12]" 0.15021207739590015;
	setAttr -s 4 ".wl[202].w[3:6]"  0.00058538826612183979 0.001301712010772638 
		0.49905644986155279 0.49905644986155279;
	setAttr -s 4 ".wl[203].w[2:5]"  0.24632344411374901 0.66770670699132628 
		0.066979843195008895 0.018990005699915751;
	setAttr -s 4 ".wl[204].w[2:5]"  0.21060307552903818 0.57047149412957077 
		0.15564425457140529 0.063281175769985668;
	setAttr -s 2 ".wl[205].w[3:4]"  0.68341472110971979 0.31658527889028015;
	setAttr -s 2 ".wl[206].w[3:4]"  0.65187262987146355 0.3481273701285364;
	setAttr -s 2 ".wl[207].w[3:4]"  0.70071252921262728 0.29928747078737267;
	setAttr -s 4 ".wl[208].w[2:5]"  0.16480103829722903 0.65647777509282013 
		0.13626588185164432 0.0424553047583065;
	setAttr -s 4 ".wl[209].w[2:5]"  0.18238836255293256 0.76109295351621586 
		0.046011940631701102 0.010506743299150393;
	setAttr -s 4 ".wl[210].w[2:5]"  0.2045164215555241 0.76221356875341129 
		0.027004146624809992 0.0062658630662546294;
	setAttr -s 4 ".wl[211].w[3:6]"  0.55108532489118278 0.37366608188326017 
		0.059345137043848382 0.015903456181708787;
	setAttr -s 4 ".wl[212].w[3:6]"  0.44347787188559568 0.38254856642600449 
		0.12294424132678587 0.051029320361613971;
	setAttr -s 4 ".wl[213].w[3:6]"  0.35156788382114218 0.35926034250770345 
		0.091295875608921051 0.19787589806223341;
	setAttr -s 3 ".wl[214].w";
	setAttr ".wl[214].w[3]" 0.29294578466510013;
	setAttr ".wl[214].w[4]" 0.33868376755052765;
	setAttr ".wl[214].w[6]" 0.36837044778437222;
	setAttr -s 3 ".wl[215].w";
	setAttr ".wl[215].w[3]" 0.44804629075003788;
	setAttr ".wl[215].w[4]" 0.43640849857235625;
	setAttr ".wl[215].w[6]" 0.1155452106776059;
	setAttr -s 3 ".wl[216].w";
	setAttr ".wl[216].w[3]" 0.53763629708803362;
	setAttr ".wl[216].w[4]" 0.43871344629245246;
	setAttr ".wl[216].w[6]" 0.023650256619513872;
	setAttr -s 4 ".wl[217].w[3:6]"  0.6175265465188744 0.35029528220806339 
		0.025566992652960363 0.0066111786201018636;
	setAttr -s 4 ".wl[218].w[2:5]"  0.0080822215753759939 0.64178703544633042 
		0.32352551441418614 0.026605228564107481;
	setAttr -s 4 ".wl[219].w[3:6]"  0.020266817931318827 0.48445511375064887 
		0.48445511375064887 0.010822954567383435;
	setAttr -s 4 ".wl[220].w[3:6]"  0.056146348716193478 0.43921530073956089 
		0.46276905436861643 0.041869296175629331;
	setAttr -s 4 ".wl[221].w[3:6]"  0.062182608144231925 0.3069717200240617 
		0.55849741583136914 0.072348256000337327;
	setAttr -s 4 ".wl[222].w[3:6]"  0.028659663011766753 0.14967694675933957 
		0.77400386333465576 0.04765952689423792;
	setAttr -s 4 ".wl[223].w[3:6]"  0.040545139769990191 0.25844302449855627 
		0.65990597658546279 0.041105859145990707;
	setAttr -s 4 ".wl[224].w[3:6]"  0.039261335244595269 0.44182310714543371 
		0.49635088693644985 0.022564670673521227;
	setAttr -s 4 ".wl[225].w[3:6]"  0.0095069647505704111 0.4931636645502297 
		0.4931636645502297 0.0041657061489701512;
	setAttr -s 4 ".wl[226].w[3:6]"  0.0042541243873425571 0.49695250674558183 
		0.49695250674558172 0.0018408621214939308;
	setAttr -s 4 ".wl[227].w[2:5]"  0.44243184579817679 0.4394230925937011 
		0.075547181688507242 0.042597879919614862;
	setAttr -s 4 ".wl[228].w[1:4]"  0.014063330099973436 0.48210488789064043 
		0.48109547671488334 0.02273630529450283;
	setAttr -s 4 ".wl[229].w[1:4]"  0.0082097482247964603 0.49007281979975326 
		0.49002908575025833 0.011688346225191945;
	setAttr -s 4 ".wl[230].w[1:4]"  0.020508525846575681 0.4763640032523152 
		0.473569589522429 0.029557881378680036;
	setAttr -s 4 ".wl[231].w[2:5]"  0.43740414600392025 0.42803603054468509 
		0.08295069009988526 0.05160913335150933;
	setAttr -s 2 ".wl[232].w[2:3]"  0.49230545743934834 0.50769454256065161;
	setAttr -s 2 ".wl[233].w[2:3]"  0.4777918111571311 0.5222081888428689;
	setAttr -s 2 ".wl[234].w[2:3]"  0.48445832978691866 0.51554167021308139;
	setAttr -s 4 ".wl[235].w[2:5]"  0.43510034537612036 0.47880883300676802 
		0.057913083098024523 0.028177738519087;
	setAttr -s 4 ".wl[236].w[2:5]"  0.49195325520177435 0.49213600949537928 
		0.011780500279106149 0.0041302350237402002;
	setAttr -s 4 ".wl[237].w[1:4]"  0.0015069784647199545 0.49720139071205499 
		0.49720139071205521 0.0040902401111696943;
	setAttr -s 4 ".wl[238].w[2:5]"  0.48727882456075228 0.48727882456075228 
		0.018259133470904573 0.007183217407590866;
	setAttr -s 4 ".wl[239].w[2:5]"  0.43554417627312386 0.4587749456860804 
		0.068304524101594655 0.0373763539392011;
	setAttr -s 2 ".wl[240].w[2:3]"  0.45743010595512079 0.54256989404487921;
	setAttr -s 2 ".wl[241].w[2:3]"  0.43794637749450249 0.56205362250549751;
	setAttr -s 2 ".wl[242].w[2:3]"  0.44533358183489913 0.55466641816510087;
	setAttr -s 4 ".wl[243].w[1:4]"  0.0019113776438129111 0.65178155552086947 
		0.34380487712822128 0.0025021897070963963;
	setAttr -s 4 ".wl[244].w[1:4]"  0.012679726823830482 0.48226060054527686 
		0.47866870462797223 0.026390968002920404;
	setAttr -s 2 ".wl[245].w[2:3]"  0.49054932518699784 0.5094506748130021;
	setAttr -s 2 ".wl[246].w[2:3]"  0.49330505519434287 0.50669494480565713;
	setAttr -s 2 ".wl[247].w[2:3]"  0.50265198401247702 0.49734801598752293;
	setAttr -s 2 ".wl[248].w[2:3]"  0.48855371033119283 0.51144628966880723;
	setAttr -s 4 ".wl[249].w[2:5]"  0.45695093110848206 0.45565730802733073 
		0.056950559358369575 0.030441201505817736;
	setAttr -s 2 ".wl[250].w[2:3]"  0.50245914216041099 0.49754085783958901;
	setAttr -s 4 ".wl[251].w[1:4]"  0.0074543825178042906 0.49061185499547172 
		0.48864708440850219 0.013286678078221797;
	setAttr -s 4 ".wl[252].w[1:4]"  0.089821280651893237 0.40311813272375963 
		0.40005801582371758 0.10700257080062962;
	setAttr -s 4 ".wl[253].w[1:4]"  0.060194177707910836 0.43815394148598624 
		0.43815394148598613 0.063497939320116784;
	setAttr -s 4 ".wl[254].w[1:4]"  0.10092135919272764 0.40707624360411515 
		0.40327100191414678 0.088731395289010459;
	setAttr -s 4 ".wl[255].w[1:4]"  0.066165770230836043 0.43352141904821312 
		0.43351988205820335 0.066792928662747456;
	setAttr -s 4 ".wl[256].w";
	setAttr ".wl[256].w[2]" 0.39270758939892919;
	setAttr ".wl[256].w[3]" 0.38715783166191364;
	setAttr ".wl[256].w[4]" 0.1090105745586193;
	setAttr ".wl[256].w[11]" 0.11112400438053789;
	setAttr -s 4 ".wl[257].w";
	setAttr ".wl[257].w[2]" 0.32789966197958931;
	setAttr ".wl[257].w[3]" 0.31713831572949458;
	setAttr ".wl[257].w[5]" 0.17748101114545806;
	setAttr ".wl[257].w[6]" 0.17748101114545806;
	setAttr -s 2 ".wl[258].w[2:3]"  0.47983422070587212 0.52016577929412788;
	setAttr -s 2 ".wl[259].w[2:3]"  0.47480605924756053 0.52519394075243941;
	setAttr -s 2 ".wl[260].w[2:3]"  0.47641533934228691 0.52358466065771303;
	setAttr -s 3 ".wl[261].w[2:4]"  0.4469649556863719 0.46485393108438749 
		0.088181113229240646;
	setAttr -s 2 ".wl[262].w[2:3]"  0.48257913162882299 0.51742086837117707;
	setAttr -s 4 ".wl[263].w[1:4]"  0.003012721419413843 0.4961753605753258 
		0.49560937154881302 0.0052025464564474395;
	setAttr -s 4 ".wl[264].w[1:4]"  0.0035856264503719062 0.49585997736446524 
		0.49499277509277928 0.0055616210923835853;
	setAttr -s 4 ".wl[265].w[2:5]"  0.41874608739838032 0.42162344716245709 
		0.088938924247372694 0.070691541191789831;
	setAttr -s 4 ".wl[266].w[2:5]"  0.45239230581557371 0.44328674941443769 
		0.065614857936079185 0.038706086833909581;
	setAttr -s 4 ".wl[267].w[3:6]"  0.0019895412997880352 0.039785747534976663 
		0.91327528727862795 0.04494942388660738;
	setAttr -s 4 ".wl[268].w[3:6]"  0.0018016768061065208 0.02649410527197443 
		0.92654352506693394 0.045160692854985106;
	setAttr -s 4 ".wl[269].w[3:6]"  0.0036162816342528779 0.025869283089975686 
		0.88090911811557693 0.089605317160194473;
	setAttr -s 4 ".wl[270].w[3:6]"  0.0032115930322527378 0.013266833403677993 
		0.88823379513500444 0.095287778429064815;
	setAttr -s 4 ".wl[271].w[3:6]"  0.0047425708494674979 0.016486491937307321 
		0.85100756311370385 0.12776337409952127;
	setAttr -s 4 ".wl[272].w[3:6]"  0.012940076483731352 0.052371443528330355 
		0.73087148073936814 0.20381699924857002;
	setAttr -s 4 ".wl[273].w[3:6]"  0.0017976324295458043 0.0046405791354765412 
		0.49678089421748883 0.49678089421748883;
	setAttr -s 4 ".wl[274].w[3:6]"  0.014807021542902518 0.096781186995821999 
		0.6960720034208614 0.19233978804041404;
	setAttr -s 4 ".wl[275].w[3:6]"  0.0074080883698503849 0.092962848524794944 
		0.78037257478219568 0.11925648832315898;
	setAttr -s 4 ".wl[276].w[1:4]"  0.019630225381921135 0.47598247670215471 
		0.46938861156175715 0.034998686354167037;
	setAttr -s 4 ".wl[277].w[1:4]"  0.0046623300861010561 0.49466124105256154 
		0.49311227301110305 0.0075641558502344344;
	setAttr -s 4 ".wl[278].w[1:4]"  0.012128730491448726 0.48645511012768816 
		0.48218206533400892 0.019234094046854219;
	setAttr -s 4 ".wl[279].w[3:6]"  0.00099337215517870947 0.0021970587021931769 
		0.4984047845713141 0.4984047845713141;
	setAttr -s 4 ".wl[280].w[3:6]"  0.00037320219473667112 0.00088840183221689287 
		0.50107525381946338 0.49766314215358315;
	setAttr -s 4 ".wl[281].w[3:6]"  0.00017050090782212598 0.00042930288141485994 
		0.49970009810538146 0.49970009810538146;
	setAttr -s 4 ".wl[282].w[3:6]"  6.8774038195297662e-05 0.00018814753108002579 
		0.49987153921536237 0.49987153921536237;
	setAttr -s 4 ".wl[283].w[3:6]"  5.2441388217975428e-05 0.00015314690926780477 
		0.49989720585125719 0.49989720585125708;
	setAttr -s 4 ".wl[284].w[3:6]"  0.00045742951054941235 0.0010144626303522752 
		0.4992640539295492 0.4992640539295492;
	setAttr -s 4 ".wl[285].w[3:6]"  0.00034549686912039895 0.0010158485358673841 
		0.49931932729750611 0.49931932729750611;
	setAttr -s 4 ".wl[286].w[3:6]"  0.0010934102448269894 0.0030442776694937822 
		0.49793115604283961 0.49793115604283961;
	setAttr -s 4 ".wl[287].w[3:6]"  0.0012296087707123707 0.0029649427730916757 
		0.49997972184524686 0.49582572661094915;
	setAttr -s 4 ".wl[288].w[3:6]"  0.0010750793264623568 0.0023950649407845298 
		0.49826492786637661 0.49826492786637661;
	setAttr -s 4 ".wl[289].w[3:6]"  0.00045357458369088842 0.00099816981827282681 
		0.4992741277990182 0.4992741277990182;
	setAttr -s 4 ".wl[290].w[2:5]"  0.43704079209508107 0.52457173198120999 
		0.028469732370870561 0.0099177435528383547;
	setAttr -s 4 ".wl[291].w[2:5]"  0.36572362614850451 0.49652689150361146 
		0.092571590221306371 0.045177892126577551;
	setAttr -s 4 ".wl[292].w[2:5]"  0.23597108527059948 0.62052135342369852 
		0.10817971444367398 0.035327846862027994;
	setAttr -s 2 ".wl[293].w[2:3]"  0.38319606186594829 0.61680393813405165;
	setAttr -s 4 ".wl[294].w[2:5]"  0.17610271225538893 0.51734698247794053 
		0.19344196526030105 0.11310834000636948;
	setAttr -s 2 ".wl[295].w[2:3]"  0.35600888478298992 0.64399111521701002;
	setAttr -s 2 ".wl[296].w[3:4]"  0.65899287652710514 0.34100712347289486;
	setAttr -s 3 ".wl[297].w[2:4]"  0.29684749618730832 0.53035039910805271 
		0.17280210470463894;
	setAttr -s 2 ".wl[298].w[3:4]"  0.66644388153830081 0.33355611846169914;
	setAttr -s 4 ".wl[299].w[2:5]"  0.3443623328671121 0.54312255703356394 
		0.079560792342409217 0.032954317756914833;
	setAttr -s 3 ".wl[300].w[2:4]"  0.15809630994671794 0.63847291786809979 
		0.20343077218518227;
	setAttr -s 4 ".wl[301].w[2:5]"  0.41295130209211073 0.56263276639798476 
		0.018771002152007901 0.0056449293578966367;
	setAttr -s 4 ".wl[302].w[2:5]"  0.1772231610895672 0.71940473041286446 
		0.082863253127096165 0.020508855370472239;
	setAttr -s 4 ".wl[303].w[2:5]"  0.44764233254669855 0.5424410792731772 
		0.0076300926145970891 0.002286495565526998;
	setAttr -s 4 ".wl[304].w[2:5]"  0.18406752118886052 0.78133374398490629 
		0.028278476668419938 0.0063202581578132005;
	setAttr -s 4 ".wl[305].w[2:5]"  0.23130186828506932 0.7198406566504314 
		0.039041929305285217 0.0098155457592140043;
	setAttr -s 4 ".wl[306].w[2:5]"  0.081921445058922751 0.71684913973625053 
		0.16569197759171458 0.035537437613112136;
	setAttr -s 4 ".wl[307].w[2:5]"  0.072491033362655402 0.57446865203254016 
		0.26593541722779085 0.087104897377013615;
	setAttr -s 4 ".wl[308].w[3:6]"  0.49691132645664293 0.38687910905041278 
		0.088085953124180646 0.028123611368763599;
	setAttr -s 2 ".wl[309].w[3:4]"  0.59660048070088167 0.40339951929911833;
	setAttr -s 4 ".wl[310].w[3:6]"  0.38346741882929897 0.35518706279014273 
		0.16878147282725994 0.092564045553298349;
	setAttr -s 2 ".wl[311].w[3:4]"  0.57187761293213601 0.42812238706786399;
	setAttr -s 3 ".wl[312].w";
	setAttr ".wl[312].w[3]" 0.29215123117029451;
	setAttr ".wl[312].w[4]" 0.32767827032688401;
	setAttr ".wl[312].w[6]" 0.38017049850282147;
	setAttr -s 2 ".wl[313].w[3:4]"  0.61164957610051252 0.38835042389948748;
	setAttr -s 3 ".wl[314].w";
	setAttr ".wl[314].w[3]" 0.39718526324115688;
	setAttr ".wl[314].w[4]" 0.43004574328742118;
	setAttr ".wl[314].w[6]" 0.17276899347142191;
	setAttr -s 3 ".wl[315].w[2:4]"  0.043465140513986762 0.70303190627285028 
		0.25350295321316302;
	setAttr -s 3 ".wl[316].w";
	setAttr ".wl[316].w[3]" 0.49397939891038012;
	setAttr ".wl[316].w[4]" 0.44813911069004664;
	setAttr ".wl[316].w[6]" 0.057881490399573327;
	setAttr -s 4 ".wl[317].w[2:5]"  0.046310903773875971 0.81391507806064045 
		0.1215574467602402 0.018216571405243461;
	setAttr -s 4 ".wl[318].w[3:6]"  0.56480209802127768 0.38902542567031789 
		0.035295939218724419 0.010876537089679991;
	setAttr -s 4 ".wl[319].w[2:5]"  0.060052160138345363 0.82834959424414967 
		0.09617074076471345 0.01542750485279158;
	setAttr -s 4 ".wl[320].w[2:5]"  0.0062519455655097433 0.64967239682817934 
		0.32163180425400639 0.022443853352304432;
	setAttr -s 4 ".wl[321].w[2:5]"  0.010910046338296616 0.60374514763487774 
		0.34711695485331123 0.038227851173514495;
	setAttr -s 4 ".wl[322].w[3:6]"  0.30895983776795777 0.46104197361791543 
		0.21481148115455961 0.015186707459567282;
	setAttr -s 4 ".wl[323].w[3:6]"  0.26401764152764212 0.41871630134557752 
		0.27265016929023372 0.044615887836546735;
	setAttr -s 4 ".wl[324].w[3:6]"  0.037397215991935503 0.46964401335427886 
		0.46964401335427874 0.023314757299506984;
	setAttr -s 4 ".wl[325].w[3:6]"  0.18802868055914893 0.36374858045048436 
		0.3497476818045156 0.098475057185851173;
	setAttr -s 4 ".wl[326].w[3:6]"  0.066131851243739687 0.38135417607527844 
		0.49197590666970831 0.060538066011273546;
	setAttr -s 4 ".wl[327].w[3:6]"  0.13359349722695479 0.32292524582975324 
		0.37814736366271973 0.16533389328057224;
	setAttr -s 4 ".wl[328].w[3:6]"  0.044505019124497641 0.21773264171528214 
		0.67208160554505736 0.065680733615162801;
	setAttr -s 4 ".wl[329].w[3:6]"  0.23417771254701647 0.46906253548082988 
		0.2207912802696228 0.075968471702530807;
	setAttr -s 4 ".wl[330].w[3:6]"  0.028057668996372589 0.1624690238473811 
		0.76973432302474976 0.039738984131496548;
	setAttr -s 4 ".wl[331].w[3:6]"  0.32279858875047879 0.49664650158388146 
		0.15983310314837182 0.020721806517268009;
	setAttr -s 4 ".wl[332].w[3:6]"  0.046975961905414372 0.36622406305656696 
		0.5524080263057326 0.034391948732286043;
	setAttr -s 4 ".wl[333].w[3:6]"  0.35390226246390588 0.53039874850402946 
		0.10973881731244314 0.0059601717196214087;
	setAttr -s 4 ".wl[334].w[3:6]"  0.022174603244402881 0.48338728035467693 
		0.48366872665065858 0.010769389750261593;
	setAttr -s 4 ".wl[335].w[3:6]"  0.34141773599804431 0.53366764402214728 
		0.12012708678715714 0.0047875331926512579;
	setAttr -s 4 ".wl[336].w[3:6]"  0.0042720912842398491 0.49696345643850709 
		0.49696345643850731 0.0018009958387456395;
	setAttr -s 4 ".wl[337].w[3:6]"  0.0088713340468113688 0.49347716741653835 
		0.49347716741653835 0.0041743311201118474;
	setAttr -s 4 ".wl[338].w[2:5]"  0.40772131142178114 0.40246743140828062 
		0.11625672289066534 0.073554534279272915;
	setAttr -s 4 ".wl[339].w[1:4]"  0.039216258028180025 0.45472120813462885 
		0.45438322481243726 0.051679309024753713;
	setAttr -s 4 ".wl[340].w[1:4]"  0.023990921089942843 0.46812051400070048 
		0.46482891216568756 0.043059652743669054;
	setAttr -s 4 ".wl[341].w[1:4]"  0.028554145679550454 0.46853268961673283 
		0.46853268961673272 0.034380475086983962;
	setAttr -s 4 ".wl[342].w[1:4]"  0.0087706890497336598 0.48911773536823178 
		0.48899751595939567 0.013114059622638913;
	setAttr -s 4 ".wl[343].w[1:4]"  0.049025190438346883 0.44700063313524013 
		0.4456277126611789 0.058346463765234023;
	setAttr -s 4 ".wl[344].w[1:4]"  0.011663620819345986 0.4862921807537024 
		0.48563526444721988 0.01640893397973173;
	setAttr -s 4 ".wl[345].w[1:4]"  0.087400921348725596 0.40173824139384329 
		0.39236845154277317 0.11849238571465802;
	setAttr -s 4 ".wl[346].w[1:4]"  0.033720837954942622 0.46035459551477598 
		0.45411793944074047 0.051806627089541014;
	setAttr -s 2 ".wl[347].w[2:3]"  0.49656856031963148 0.50343143968036852;
	setAttr -s 4 ".wl[348].w[2:5]"  0.39594832348933595 0.39378227662112919 
		0.11201746541972002 0.098251934469814797;
	setAttr -s 2 ".wl[349].w[2:3]"  0.48187520992267724 0.51812479007732282;
	setAttr -s 2 ".wl[350].w[2:3]"  0.48323616098488775 0.51676383901511236;
	setAttr -s 2 ".wl[351].w[2:3]"  0.48905961258811897 0.51094038741188108;
	setAttr -s 2 ".wl[352].w[2:3]"  0.47827086410620712 0.52172913589379288;
	setAttr -s 3 ".wl[353].w[2:4]"  0.43525965276565598 0.44581451411521827 
		0.1189258331191258;
	setAttr -s 4 ".wl[354].w[2:5]"  0.45566603234026437 0.46480358220547324 
		0.052455029052455471 0.027075356401806912;
	setAttr -s 4 ".wl[355].w[1:4]"  0.0051939980314742017 0.49197874998824387 
		0.49196533918206897 0.01086191279821299;
	setAttr -s 4 ".wl[356].w[2:5]"  0.47215721043806108 0.48810392467956282 
		0.028692855875979473 0.011046009006396724;
	setAttr -s 4 ".wl[357].w[1:4]"  0.0022017800712963169 0.49688914429623188 
		0.49688914429623188 0.0040199313362399049;
	setAttr -s 4 ".wl[358].w[1:4]"  0.0017122708296723669 0.49668343723603536 
		0.49668343723603536 0.0049208546982569649;
	setAttr -s 4 ".wl[359].w[1:4]"  0.0089695665356865299 0.48765642340043813 
		0.48674222348528673 0.01663178657858852;
	setAttr -s 4 ".wl[360].w[1:4]"  0.0028146764093543658 0.49483427191377932 
		0.49483427191377921 0.007516779763087224;
	setAttr -s 4 ".wl[361].w[2:5]"  0.45227522498515627 0.45075440598432615 
		0.06169715437794928 0.035273214652568302;
	setAttr -s 4 ".wl[362].w[2:5]"  0.46945655498555472 0.47467414508199041 
		0.038775147536476311 0.017094152395978716;
	setAttr -s 2 ".wl[363].w[2:3]"  0.48251394323807478 0.51748605676192516;
	setAttr -s 4 ".wl[364].w[2:5]"  0.39074388470088756 0.43642844752943843 
		0.09816450904567732 0.074663158723996786;
	setAttr -s 2 ".wl[365].w[2:3]"  0.46728798059781063 0.53271201940218949;
	setAttr -s 2 ".wl[366].w[2:3]"  0.44498360671336445 0.55501639328663555;
	setAttr -s 2 ".wl[367].w[2:3]"  0.47360986025916274 0.52639013974083726;
	setAttr -s 2 ".wl[368].w[2:3]"  0.43787094992034475 0.5621290500796553;
	setAttr -s 3 ".wl[369].w[2:4]"  0.41431371734895989 0.48835137379520949 
		0.097334908855830668;
	setAttr -s 2 ".wl[370].w[2:3]"  0.49709389648805485 0.5029061035119452;
	setAttr -s 3 ".wl[371].w";
	setAttr ".wl[371].w[2]" 0.49535917990968853;
	setAttr ".wl[371].w[3]" 0.48252253665460909;
	setAttr ".wl[371].w[5]" 0.022118283435702324;
	setAttr -s 4 ".wl[372].w[1:4]"  0.073372462373918368 0.43030845747723873 
		0.43025462795233382 0.06606445219650911;
	setAttr -s 4 ".wl[373].w[1:4]"  0.075399561551005481 0.43439166570805449 
		0.43000953268709974 0.060199240053840232;
	setAttr -s 4 ".wl[374].w";
	setAttr ".wl[374].w[2]" 0.3475454587665498;
	setAttr ".wl[374].w[3]" 0.32834136936724739;
	setAttr ".wl[374].w[7]" 0.16178108768989963;
	setAttr ".wl[374].w[8]" 0.16233208417630315;
	setAttr -s 4 ".wl[375].w";
	setAttr ".wl[375].w[2]" 0.3396409101952142;
	setAttr ".wl[375].w[3]" 0.32974444316337154;
	setAttr ".wl[375].w[7]" 0.16074184690475074;
	setAttr ".wl[375].w[8]" 0.16987279973666367;
	setAttr -s 4 ".wl[376].w";
	setAttr ".wl[376].w[2]" 0.38624061241132951;
	setAttr ".wl[376].w[3]" 0.37071603143237647;
	setAttr ".wl[376].w[11]" 0.12621209171539452;
	setAttr ".wl[376].w[12]" 0.11683126444089953;
	setAttr -s 4 ".wl[377].w";
	setAttr ".wl[377].w[2]" 0.31265357310649644;
	setAttr ".wl[377].w[3]" 0.29983780466575521;
	setAttr ".wl[377].w[11]" 0.19553502462937303;
	setAttr ".wl[377].w[12]" 0.19197359759837535;
	setAttr -s 2 ".wl[378].w[2:3]"  0.51058096086460347 0.48941903913539658;
	setAttr -s 4 ".wl[379].w[1:4]"  0.10172797282870606 0.40748982038415726 
		0.39511402699150766 0.095668179795629082;
	setAttr -s 4 ".wl[380].w[1:4]"  0.089286193654783505 0.41410329771427906 
		0.41180604811607924 0.084804460514858321;
	setAttr -s 4 ".wl[381].w[1:4]"  0.068944657851191249 0.43868361274714124 
		0.43520508897019106 0.057166640431476477;
	setAttr -s 2 ".wl[382].w[2:3]"  0.50917235812637485 0.4908276418736251;
	setAttr -s 2 ".wl[383].w[2:3]"  0.52380608640095339 0.47619391359904667;
	setAttr -s 2 ".wl[384].w[2:3]"  0.51200592006251133 0.48799407993748867;
	setAttr -s 2 ".wl[385].w[2:3]"  0.52081006620714088 0.47918993379285918;
	setAttr -s 2 ".wl[386].w[2:3]"  0.51570244434002555 0.4842975556599744;
	setAttr -s 3 ".wl[387].w[2:4]"  0.48646030592219863 0.43609942438551136 
		0.077440269692289995;
	setAttr -s 4 ".wl[388].w[1:4]"  0.084497513440309036 0.42526329715629152 
		0.39766810280181103 0.092571086601588429;
	setAttr -s 4 ".wl[389].w[1:4]"  0.018303641112830057 0.50920560005623416 
		0.45224795339486518 0.020242805436070582;
	setAttr -s 4 ".wl[390].w[1:4]"  0.045596899595901415 0.46615399637464378 
		0.44768589003653292 0.040563213992921815;
	setAttr -s 4 ".wl[391].w[1:4]"  0.0061471162447360971 0.52913953657207702 
		0.4588293054679376 0.0058840417152491538;
	setAttr -s 4 ".wl[392].w[1:4]"  0.032932478520776402 0.47625499963171747 
		0.46393450908434541 0.026878012763160694;
	setAttr -s 4 ".wl[393].w[1:4]"  0.0091143294381106389 0.52399114497397647 
		0.45861972608097468 0.0082747995069381349;
	setAttr -s 4 ".wl[394].w[1:4]"  0.056703444820332398 0.45895321189780092 
		0.43790022716224003 0.046443116119626551;
	setAttr -s 4 ".wl[395].w[1:4]"  0.027358573643669129 0.5011545093244687 
		0.44453043373141882 0.026956483300443375;
	setAttr -s 4 ".wl[396].w[1:4]"  0.10082935026076865 0.41775830226397415 
		0.3856135628944054 0.095798784580851726;
	setAttr -s 2 ".wl[397].w[2:3]"  0.52090611552793376 0.47909388447206624;
	setAttr -s 4 ".wl[398].w";
	setAttr ".wl[398].w[2]" 0.34449781415325598;
	setAttr ".wl[398].w[3]" 0.31955431101037252;
	setAttr ".wl[398].w[5]" 0.16194403183588596;
	setAttr ".wl[398].w[11]" 0.17400384300048552;
	setAttr -s 4 ".wl[399].w[1:4]"  6.6017401197089837e-06 0.9765339111790422 
		0.023452312138268174 7.1749425699373955e-06;
	setAttr -s 3 ".wl[400].w[2:4]"  0.48591869666934717 0.43064988805654569 
		0.083431415274107204;
	setAttr -s 3 ".wl[401].w[2:4]"  0.52837555064376385 0.43331748235361078 
		0.038306967002625376;
	setAttr -s 4 ".wl[402].w[1:4]"  0.009964029565984165 0.55656554663850599 
		0.42120349285091646 0.01226693094459325;
	setAttr -s 4 ".wl[403].w[1:4]"  0.0063351960288356409 0.56958380035508727 
		0.4155935097317543 0.0084874938843229417;
	setAttr -s 4 ".wl[404].w[1:4]"  0.014791508064294629 0.48002675138781581 
		0.47585956628940407 0.029322174258485528;
	setAttr -s 2 ".wl[405].w[2:3]"  0.48169435900261259 0.51830564099738752;
	setAttr -s 2 ".wl[406].w[2:3]"  0.47674283010249024 0.52325716989750981;
	setAttr -s 3 ".wl[407].w[2:4]"  0.44579968328664338 0.46024084549164829 
		0.093959471221708349;
	setAttr -s 4 ".wl[408].w[1:4]"  0.0039409584928286889 0.49526216110748222 
		0.4943447639937063 0.006452116405982789;
	setAttr -s 4 ".wl[409].w[2:5]"  0.41517711071616065 0.41549666812193981 
		0.093410580898447906 0.075915640263451614;
	setAttr -s 4 ".wl[410].w[3:6]"  0.003903602284756 0.066173195478332725 
		0.8544923800270775 0.075430822209833853;
	setAttr -s 4 ".wl[411].w[3:6]"  0.0014125446965028029 0.026756000001922266 
		0.93683571280989508 0.0349957424916797;
	setAttr -s 4 ".wl[412].w[3:6]"  0.0027129624299509253 0.027996731142366095 
		0.90231791055051558 0.06697239587716744;
	setAttr -s 4 ".wl[413].w[3:6]"  0.0036647649726681978 0.019043386002523758 
		0.87948283304656838 0.097809015978239613;
	setAttr -s 4 ".wl[414].w[3:6]"  0.0031145714651257964 0.01135886127780918 
		0.88875945669142442 0.096767110565640607;
	setAttr -s 4 ".wl[415].w[3:6]"  0.0084119712668135598 0.030324897864090164 
		0.78856338538038562 0.17269974548871081;
	setAttr -s 4 ".wl[416].w[3:6]"  0.015360064521679413 0.076898884628078423 
		0.70003897257795045 0.20770207827229176;
	setAttr -s 4 ".wl[417].w[3:6]"  0.011498679020561862 0.10309088210245632 
		0.72482736452492713 0.16058307435205457;
	setAttr -s 4 ".wl[418].w[1:4]"  0.0058548024605614511 0.49358415564533725 
		0.49155577386997484 0.0090052680241263418;
	setAttr -s 4 ".wl[419].w[1:4]"  0.022387887257434912 0.47346022369954233 
		0.46614641806410045 0.038005470978922415;
	setAttr -s 4 ".wl[420].w[3:6]"  0.00080086120333725897 0.0017578643315580118 
		0.49872063723255239 0.49872063723255239;
	setAttr -s 4 ".wl[421].w[3:6]"  0.0014136014152835742 0.003134315770005172 
		0.4977260414073556 0.4977260414073556;
	setAttr -s 4 ".wl[422].w[3:6]"  0.00090995373285438213 0.0020238921853005217 
		0.4985330770409227 0.49853307704092248;
	setAttr -s 4 ".wl[423].w[3:6]"  0.00032779761658348875 0.00071979879813220194 
		0.49947620179264218 0.49947620179264218;
	setAttr -s 4 ".wl[424].w[2:5]"  0.4073427905254996 0.5149769154181375 
		0.055781752040533178 0.021898542015829695;
	setAttr -s 4 ".wl[425].w[2:5]"  0.3162404665000218 0.46818193055221985 
		0.1272164037331156 0.088361199214642694;
	setAttr -s 2 ".wl[426].w[2:3]"  0.3662332823459265 0.63376671765407344;
	setAttr -s 2 ".wl[427].w[2:3]"  0.35312091511185822 0.64687908488814183;
	setAttr -s 3 ".wl[428].w[2:4]"  0.32286985045464439 0.54789522672366975 
		0.12923492282168592;
	setAttr -s 4 ".wl[429].w[2:5]"  0.38304586109768496 0.56141196185479125 
		0.041829828771566893 0.0137123482759568;
	setAttr -s 4 ".wl[430].w[2:5]"  0.43244167027875452 0.55611803127287252 
		0.0088461561066069169 0.0025941423417661933;
	setAttr -s 4 ".wl[431].w[2:5]"  0.45004845826399853 0.53282431384121998 
		0.012997888532185921 0.0041293393625955006;
	setAttr -s 4 ".wl[432].w[2:5]"  0.080709102493842499 0.64507282440715497 
		0.21767183743296223 0.056546235666040355;
	setAttr -s 3 ".wl[433].w";
	setAttr ".wl[433].w[3]" 0.62271188792509946;
	setAttr ".wl[433].w[4]" 0.36131236122227139;
	setAttr ".wl[433].w[6]" 0.015975750852629197;
	setAttr -s 2 ".wl[434].w[3:4]"  0.57742900442875156 0.42257099557124844;
	setAttr -s 2 ".wl[435].w[3:4]"  0.5830685094417517 0.4169314905582483;
	setAttr -s 2 ".wl[436].w[3:4]"  0.66430275041280706 0.33569724958719294;
	setAttr -s 4 ".wl[437].w[2:5]"  0.043818354474885669 0.75375741674355734 
		0.17407971172905556 0.028344517052501409;
	setAttr -s 4 ".wl[438].w[2:5]"  0.049808030860335729 0.84174356730631272 
		0.094371472280325872 0.014076929553025776;
	setAttr -s 4 ".wl[439].w[2:5]"  0.072416653401861458 0.78462530529260321 
		0.12103487202604309 0.021923169279492321;
	setAttr -s 4 ".wl[440].w[3:6]"  0.28691571849375785 0.43497451641079815 
		0.25125677769150251 0.026852987403941653;
	setAttr -s 4 ".wl[441].w[3:6]"  0.23486882692796562 0.40506729511725198 
		0.29125808526017199 0.068805792694610401;
	setAttr -s 4 ".wl[442].w[3:6]"  0.14182719421870862 0.31449352058488883 
		0.41307994723320007 0.1305993379632025;
	setAttr -s 4 ".wl[443].w[3:6]"  0.16958185176107152 0.39596121123027345 
		0.30253580212593079 0.13192113488272428;
	setAttr -s 4 ".wl[444].w[3:6]"  0.28017426458664013 0.48058489119333903 
		0.20110780335855116 0.038133040861469791;
	setAttr -s 4 ".wl[445].w[3:6]"  0.34492787260652746 0.51102479916950017 
		0.13325107615680162 0.01079625206717078;
	setAttr -s 4 ".wl[446].w[3:6]"  0.3492929143873818 0.54630825528368887 
		0.10025957850032503 0.0041392518286042235;
	setAttr -s 4 ".wl[447].w[3:6]"  0.3279667162666467 0.49904097944119546 
		0.16492289926839543 0.0080694050237623861;
	setAttr -s 4 ".wl[448].w[1:4]"  0.054119635490959064 0.43508679542292183 
		0.43254954062609974 0.078244028460019391;
	setAttr -s 4 ".wl[449].w[1:4]"  0.029741071185766053 0.4666127752044088 
		0.4666127752044088 0.037033378405416331;
	setAttr -s 4 ".wl[450].w[1:4]"  0.034685700588361269 0.46217774949626472 
		0.46217432713662421 0.0409622227787498;
	setAttr -s 4 ".wl[451].w[1:4]"  0.067574214831719645 0.42649887695483119 
		0.42140937580005072 0.08451753241339835;
	setAttr -s 4 ".wl[452].w";
	setAttr ".wl[452].w[2]" 0.38341360056939489;
	setAttr ".wl[452].w[3]" 0.37628241860351402;
	setAttr ".wl[452].w[5]" 0.1568946416375617;
	setAttr ".wl[452].w[6]" 0.083409339189529419;
	setAttr -s 2 ".wl[453].w[2:3]"  0.48747834878658747 0.51252165121341253;
	setAttr -s 2 ".wl[454].w[2:3]"  0.48257046046512769 0.51742953953487225;
	setAttr -s 3 ".wl[455].w[2:4]"  0.41316857000898977 0.41626560078227737 
		0.17056582920873292;
	setAttr -s 4 ".wl[456].w[1:4]"  0.011214271570919768 0.48190115753182633 
		0.48092096617596897 0.025963604721285009;
	setAttr -s 4 ".wl[457].w[1:4]"  0.0024733832416141883 0.4963806187330716 
		0.4963806187330716 0.0047653792922426582;
	setAttr -s 4 ".wl[458].w[1:4]"  0.0039338573411821509 0.49448043920918539 
		0.49447880590428367 0.0071068975453487905;
	setAttr -s 4 ".wl[459].w[1:4]"  0.017620424321872264 0.47538178250134944 
		0.47221117371490856 0.034786619461869804;
	setAttr -s 4 ".wl[460].w[2:5]"  0.41455535813357819 0.42614383324919369 
		0.089304372413457497 0.069996436203770621;
	setAttr -s 2 ".wl[461].w[2:3]"  0.47291172051977226 0.52708827948022774;
	setAttr -s 2 ".wl[462].w[2:3]"  0.46749242674446978 0.53250757325553022;
	setAttr -s 3 ".wl[463].w[2:4]"  0.44131809576194853 0.47050166503148605 
		0.088180239206565397;
	setAttr -s 2 ".wl[464].w[2:3]"  0.49762547352246889 0.50237452647753111;
	setAttr -s 4 ".wl[465].w[1:4]"  0.081983276076458611 0.42328097544108895 
		0.42253945359691875 0.072196294885533779;
	setAttr -s 3 ".wl[466].w";
	setAttr ".wl[466].w[2]" 0.39695182305052451;
	setAttr ".wl[466].w[3]" 0.38403267359388665;
	setAttr ".wl[466].w[8]" 0.21901550335558881;
	setAttr -s 4 ".wl[467].w";
	setAttr ".wl[467].w[2]" 0.36295343366869337;
	setAttr ".wl[467].w[3]" 0.35424061325942802;
	setAttr ".wl[467].w[11]" 0.14424890299991946;
	setAttr ".wl[467].w[12]" 0.1385570500719592;
	setAttr -s 2 ".wl[468].w[2:3]"  0.50204490886674835 0.49795509113325176;
	setAttr -s 4 ".wl[469].w";
	setAttr ".wl[469].w[2]" 0.39257473279985322;
	setAttr ".wl[469].w[3]" 0.38638886468597528;
	setAttr ".wl[469].w[4]" 0.1099368872203859;
	setAttr ".wl[469].w[8]" 0.11109951529378556;
	setAttr -s 4 ".wl[470].w[1:4]"  0.075722070559653701 0.42765534179881298 
		0.42729802023308161 0.069324567408451762;
	setAttr -s 4 ".wl[471].w";
	setAttr ".wl[471].w[2]" 0.32257797762818341;
	setAttr ".wl[471].w[3]" 0.30564491411442885;
	setAttr ".wl[471].w[11]" 0.19096462834615027;
	setAttr ".wl[471].w[12]" 0.18081247991123753;
	setAttr -s 2 ".wl[472].w[2:3]"  0.51584709526068839 0.48415290473931161;
	setAttr -s 2 ".wl[473].w[2:3]"  0.51229080050699538 0.48770919949300456;
	setAttr -s 3 ".wl[474].w[2:4]"  0.44611643140492724 0.41336335688447007 
		0.14052021171060267;
	setAttr -s 4 ".wl[475].w[1:4]"  0.063028525599881632 0.4494154933026423 
		0.42583913172258836 0.061716849374887806;
	setAttr -s 4 ".wl[476].w[1:4]"  0.034447622984589157 0.4753974057642415 
		0.46122957006672666 0.02892540118444261;
	setAttr -s 4 ".wl[477].w[1:4]"  0.040084547050706229 0.47157317982593622 
		0.45607666834996691 0.03226560477339073;
	setAttr -s 4 ".wl[478].w[1:4]"  0.07812089811299236 0.44075508447908079 
		0.4133114061927517 0.067812611215175123;
	setAttr -s 4 ".wl[479].w[2:5]"  0.39385015812658336 0.35917352508594969 
		0.12483681213323314 0.1221395046542339;
	setAttr -s 3 ".wl[480].w[2:4]"  0.51685893464137322 0.43520203327477153 
		0.047939032083855311;
	setAttr -s 3 ".wl[481].w[2:4]"  0.51830748058320375 0.43816312848350192 
		0.043529390933294283;
	setAttr -s 4 ".wl[482].w[1:4]"  0.0032831673428262882 0.58432616521290692 
		0.40891544386151474 0.0034752235827519213;
	setAttr -s 4 ".wl[483].w[1:4]"  0.0013769723939859443 0.61765122081761781 
		0.37940656858168453 0.0015652382067116572;
	setAttr -s 4 ".wl[484].w[1:4]"  0.044630093038774486 0.71735428589900163 
		0.23122789468722513 0.0067877263749987465;
	setAttr -s 4 ".wl[485].w";
	setAttr ".wl[485].w[1]" 0.053990270363172749;
	setAttr ".wl[485].w[2]" 0.69144548588440935;
	setAttr ".wl[485].w[3]" 0.24221561920489687;
	setAttr ".wl[485].w[11]" 0.012348624547521024;
	setAttr -s 4 ".wl[486].w[1:4]"  0.0381912289141925 0.70001498491821346 
		0.25481987484785751 0.0069739113197365751;
	setAttr -s 4 ".wl[487].w";
	setAttr ".wl[487].w[1]" 0.046738819249329565;
	setAttr ".wl[487].w[2]" 0.67646460171615341;
	setAttr ".wl[487].w[3]" 0.2658231674848896;
	setAttr ".wl[487].w[11]" 0.010973411549627419;
	setAttr -s 4 ".wl[488].w[1:4]"  0.03346468333814933 0.72317129076173314 
		0.23740508454268935 0.0059589413574281901;
	setAttr -s 4 ".wl[489].w";
	setAttr ".wl[489].w[1]" 0.042037833130343545;
	setAttr ".wl[489].w[2]" 0.69684007127062719;
	setAttr ".wl[489].w[3]" 0.2515117511944166;
	setAttr ".wl[489].w[11]" 0.009610344404612647;
	setAttr -s 4 ".wl[490].w[1:4]"  0.039283471216569076 0.74110915214565898 
		0.21380827772166727 0.0057990989161047892;
	setAttr -s 4 ".wl[491].w";
	setAttr ".wl[491].w[1]" 0.048756930955557061;
	setAttr ".wl[491].w[2]" 0.71266429961280064;
	setAttr ".wl[491].w[3]" 0.22774103154727246;
	setAttr ".wl[491].w[11]" 0.010837737884369808;
	setAttr -s 4 ".wl[492].w[1:4]"  0.027853063886649678 0.71002692055966254 
		0.25647264545809723 0.0056473700955905542;
	setAttr -s 4 ".wl[493].w[1:4]"  0.018634113735472264 0.77081409847602678 
		0.20696072938398635 0.0035910584045145311;
	setAttr -s 4 ".wl[494].w";
	setAttr ".wl[494].w[1]" 0.025699000195836857;
	setAttr ".wl[494].w[2]" 0.80697538080554099;
	setAttr ".wl[494].w[3]" 0.16352885051475491;
	setAttr ".wl[494].w[11]" 0.0037967684838673644;
	setAttr -s 4 ".wl[495].w";
	setAttr ".wl[495].w[1]" 0.037671935492590576;
	setAttr ".wl[495].w[2]" 0.74523361453499615;
	setAttr ".wl[495].w[3]" 0.21106688941934357;
	setAttr ".wl[495].w[11]" 0.0060275605530697145;
	setAttr -s 4 ".wl[496].w[1:4]"  0.027243961017040744 0.68746249472703036 
		0.27910796623313622 0.0061855780227927709;
	setAttr -s 4 ".wl[497].w[1:4]"  0.015890606960609503 0.7667635820060521 
		0.21394788452116439 0.0033979265121740123;
	setAttr -s 4 ".wl[498].w";
	setAttr ".wl[498].w[1]" 0.026302502427952221;
	setAttr ".wl[498].w[2]" 0.82273319393894162;
	setAttr ".wl[498].w[3]" 0.14751280221921056;
	setAttr ".wl[498].w[11]" 0.0034515014138956444;
	setAttr -s 4 ".wl[499].w";
	setAttr ".wl[499].w[1]" 0.043283001358416118;
	setAttr ".wl[499].w[2]" 0.74130469982898362;
	setAttr ".wl[499].w[3]" 0.20905095455639111;
	setAttr ".wl[499].w[11]" 0.0063613442562091976;
	setAttr -s 4 ".wl[500].w[1:4]"  0.029266682927185952 0.68131976960455798 
		0.28280996937129271 0.0066035780969633893;
	setAttr -s 4 ".wl[501].w[1:4]"  0.017737333799923403 0.75671353877366287 
		0.22177995125349256 0.0037691761729211799;
	setAttr -s 4 ".wl[502].w";
	setAttr ".wl[502].w[1]" 0.028977222237439263;
	setAttr ".wl[502].w[2]" 0.81225972076603092;
	setAttr ".wl[502].w[3]" 0.15418085116842281;
	setAttr ".wl[502].w[11]" 0.0045822058281069847;
	setAttr -s 4 ".wl[503].w";
	setAttr ".wl[503].w[1]" 0.045997658018865732;
	setAttr ".wl[503].w[2]" 0.73324261624930209;
	setAttr ".wl[503].w[3]" 0.21268203574646205;
	setAttr ".wl[503].w[11]" 0.0080776899853702063;
	setAttr -s 4 ".wl[504].w[1:4]"  0.043683047431395804 0.73223956431369797 
		0.21791603914964047 0.0061613491052658143;
	setAttr -s 4 ".wl[505].w[1:4]"  0.031885120483547601 0.69636900378381339 
		0.26527165577850526 0.0064742199541336053;
	setAttr -s 4 ".wl[506].w[1:4]"  0.023346109623494638 0.74611806280979387 
		0.22600555396877961 0.0045302735979318816;
	setAttr -s 4 ".wl[507].w[1:4]"  0.032577266086733662 0.78365311046185149 
		0.17945948907978204 0.0043101343716328714;
	setAttr -s 4 ".wl[508].w[1:4]"  0.043922667902969545 0.70181738149021455 
		0.24693091496384006 0.0073290356429759574;
	setAttr -s 4 ".wl[509].w";
	setAttr ".wl[509].w[1]" 0.053963243125601176;
	setAttr ".wl[509].w[2]" 0.67498920957982911;
	setAttr ".wl[509].w[3]" 0.25800658670643528;
	setAttr ".wl[509].w[11]" 0.013040960588134436;
	setAttr -s 4 ".wl[510].w[1:4]"  0.038611881367187066 0.70410150072179734 
		0.25036004215726676 0.00692657575374892;
	setAttr -s 4 ".wl[511].w";
	setAttr ".wl[511].w[1]" 0.048287773785802242;
	setAttr ".wl[511].w[2]" 0.67720724446941571;
	setAttr ".wl[511].w[3]" 0.26284800751676018;
	setAttr ".wl[511].w[11]" 0.011656974228021897;
	setAttr -s 4 ".wl[512].w[1:4]"  0.039172917889119897 0.72310215008481338 
		0.23135467186006123 0.0063702601660054741;
	setAttr -s 4 ".wl[513].w";
	setAttr ".wl[513].w[1]" 0.049320416414604863;
	setAttr ".wl[513].w[2]" 0.6936844017951288;
	setAttr ".wl[513].w[3]" 0.24537236276916555;
	setAttr ".wl[513].w[11]" 0.011622819021100876;
	setAttr -s 4 ".wl[514].w[1:4]"  0.044630457364996545 0.72012136072506472 
		0.22849942472132304 0.0067487571886156788;
	setAttr -s 4 ".wl[515].w";
	setAttr ".wl[515].w[1]" 0.055159991259306984;
	setAttr ".wl[515].w[2]" 0.69085835650147309;
	setAttr ".wl[515].w[3]" 0.24097106621425396;
	setAttr ".wl[515].w[11]" 0.013010586024965992;
	setAttr -s 4 ".wl[516].w[1:4]"  0.027861235537559613 0.69563895849170543 
		0.27047289517210921 0.0060269107986258563;
	setAttr -s 4 ".wl[517].w[1:4]"  0.017240697576217111 0.76738676628697544 
		0.21184991845611337 0.0035226176806940528;
	setAttr -s 4 ".wl[518].w[1:4]"  0.02187091804224724 0.73233313831493063 
		0.24121158008007615 0.004584363562745916;
	setAttr -s 4 ".wl[519].w";
	setAttr ".wl[519].w[1]" 0.026223750706248144;
	setAttr ".wl[519].w[2]" 0.81458994529218043;
	setAttr ".wl[519].w[3]" 0.15525972089924259;
	setAttr ".wl[519].w[11]" 0.0039265831023289023;
	setAttr -s 4 ".wl[520].w[1:4]"  0.020310554062573871 0.80121523986892773 
		0.17524563709102259 0.0032285689774757651;
	setAttr -s 4 ".wl[521].w";
	setAttr ".wl[521].w[1]" 0.041112812879820278;
	setAttr ".wl[521].w[2]" 0.74096333580398122;
	setAttr ".wl[521].w[3]" 0.21114544655847822;
	setAttr ".wl[521].w[11]" 0.0067784047577204012;
	setAttr -s 4 ".wl[522].w";
	setAttr ".wl[522].w[1]" 0.03315629201837731;
	setAttr ".wl[522].w[2]" 0.78047525082547708;
	setAttr ".wl[522].w[3]" 0.18134335908214505;
	setAttr ".wl[522].w[11]" 0.0050250980740004867;
	setAttr -s 4 ".wl[523].w[1:4]"  0.034321757916604682 0.71937237897625783 
		0.24039179697313234 0.0059140661340052114;
	setAttr -s 4 ".wl[524].w[1:4]"  0.027171758805681807 0.6978293470052982 
		0.26910831521244932 0.0058905789765707666;
	setAttr -s 4 ".wl[525].w[1:4]"  0.016608627446796496 0.77079428274593265 
		0.20919619142512466 0.0034008983821462355;
	setAttr -s 4 ".wl[526].w[1:4]"  0.01955702936721529 0.71344897119453332 
		0.26227795894059452 0.0047160404976568987;
	setAttr -s 4 ".wl[527].w";
	setAttr ".wl[527].w[1]" 0.025359302283932496;
	setAttr ".wl[527].w[2]" 0.81811766613736503;
	setAttr ".wl[527].w[3]" 0.15295993755919401;
	setAttr ".wl[527].w[11]" 0.0035630940195084853;
	setAttr -s 4 ".wl[528].w[1:4]"  0.018454642075800137 0.81268470920007119 
		0.16592875867942877 0.0029318900446999138;
	setAttr -s 4 ".wl[529].w";
	setAttr ".wl[529].w[1]" 0.040223876847241338;
	setAttr ".wl[529].w[2]" 0.7437078986711918;
	setAttr ".wl[529].w[3]" 0.20982904633767732;
	setAttr ".wl[529].w[11]" 0.0062391781438895623;
	setAttr -s 4 ".wl[530].w";
	setAttr ".wl[530].w[1]" 0.037300107391104009;
	setAttr ".wl[530].w[2]" 0.78804321284143353;
	setAttr ".wl[530].w[3]" 0.16959933859743681;
	setAttr ".wl[530].w[11]" 0.0050573411700256202;
	setAttr -s 4 ".wl[531].w[1:4]"  0.036980324521064804 0.70468374579297877 
		0.25180252300000122 0.0065334066859552113;
	setAttr -s 4 ".wl[532].w";
	setAttr ".wl[532].w[1]" 0.037312885216549775;
	setAttr ".wl[532].w[2]" 0.68076566333938704;
	setAttr ".wl[532].w[3]" 0.27385237568045817;
	setAttr ".wl[532].w[11]" 0.0080690757636050162;
	setAttr -s 4 ".wl[533].w";
	setAttr ".wl[533].w[1]" 0.028636552481483033;
	setAttr ".wl[533].w[2]" 0.72559340088883784;
	setAttr ".wl[533].w[3]" 0.23990704110730457;
	setAttr ".wl[533].w[11]" 0.0058630055223747226;
	setAttr -s 4 ".wl[534].w[1:4]"  0.02139230071967306 0.70575671453848421 
		0.26772743257614739 0.0051235521656953654;
	setAttr -s 4 ".wl[535].w";
	setAttr ".wl[535].w[1]" 0.039214700591102616;
	setAttr ".wl[535].w[2]" 0.76084475986864175;
	setAttr ".wl[535].w[3]" 0.19244955617727025;
	setAttr ".wl[535].w[11]" 0.0074909833629854261;
	setAttr -s 4 ".wl[536].w";
	setAttr ".wl[536].w[1]" 0.020651002101480118;
	setAttr ".wl[536].w[2]" 0.80189503815044993;
	setAttr ".wl[536].w[3]" 0.17402560836605854;
	setAttr ".wl[536].w[11]" 0.0034283513820114278;
	setAttr -s 4 ".wl[537].w";
	setAttr ".wl[537].w[1]" 0.050248199746240195;
	setAttr ".wl[537].w[2]" 0.71320620894630127;
	setAttr ".wl[537].w[3]" 0.22632087909013698;
	setAttr ".wl[537].w[11]" 0.010224712217321543;
	setAttr -s 4 ".wl[538].w";
	setAttr ".wl[538].w[1]" 0.040173913206763549;
	setAttr ".wl[538].w[2]" 0.77870140462020954;
	setAttr ".wl[538].w[3]" 0.17456624278081384;
	setAttr ".wl[538].w[11]" 0.0065584393922130694;
	setAttr -s 4 ".wl[539].w";
	setAttr ".wl[539].w[1]" 0.039348476799121283;
	setAttr ".wl[539].w[2]" 0.69811546644680922;
	setAttr ".wl[539].w[3]" 0.25513237425140006;
	setAttr ".wl[539].w[11]" 0.0074036825026694434;
	setAttr -s 4 ".wl[540].w[1:4]"  0.039083967862500894 0.70743102569837346 
		0.24680629114828226 0.0066787152908433954;
	setAttr -s 4 ".wl[541].w[1:4]"  0.026010392521445949 0.71340055263594238 
		0.25509833000227167 0.0054907248403399146;
	setAttr -s 4 ".wl[542].w[1:4]"  0.026095742424300624 0.77517950350964682 
		0.19457767699908837 0.0041470770669643012;
	setAttr -s 4 ".wl[543].w";
	setAttr ".wl[543].w[1]" 0.040078795350235966;
	setAttr ".wl[543].w[2]" 0.76265915472661205;
	setAttr ".wl[543].w[3]" 0.19210631058668884;
	setAttr ".wl[543].w[11]" 0.0051557393364632153;
	setAttr -s 4 ".wl[544].w";
	setAttr ".wl[544].w[1]" 0.057024725287331934;
	setAttr ".wl[544].w[2]" 0.67047244627636582;
	setAttr ".wl[544].w[3]" 0.25809152914313749;
	setAttr ".wl[544].w[11]" 0.014411299293164714;
	setAttr -s 4 ".wl[545].w[1:4]"  0.045483730381783084 0.7010120539984146 
		0.24597773471979498 0.0075264809000073719;
	setAttr -s 4 ".wl[546].w[1:4]"  0.020802210131838954 0.72039296757844473 
		0.25408564820488938 0.0047191740848269957;
	setAttr -s 4 ".wl[547].w[1:4]"  0.019428303943571416 0.80655422554099165 
		0.17092739973013232 0.0030900707853045804;
	setAttr -s 4 ".wl[548].w";
	setAttr ".wl[548].w[1]" 0.035694622825443834;
	setAttr ".wl[548].w[2]" 0.78311935388729836;
	setAttr ".wl[548].w[3]" 0.17567983724823447;
	setAttr ".wl[548].w[11]" 0.0055061860390233624;
	setAttr -s 4 ".wl[549].w";
	setAttr ".wl[549].w[1]" 0.036206845045295366;
	setAttr ".wl[549].w[2]" 0.70922606175094716;
	setAttr ".wl[549].w[3]" 0.24820865001660908;
	setAttr ".wl[549].w[11]" 0.0063584431871483776;
	setAttr -s 4 ".wl[550].w[1:4]"  0.020170253785282463 0.72308948921233407 
		0.25215370252596803 0.0045865544764155212;
	setAttr -s 4 ".wl[551].w[1:4]"  0.018697377336037331 0.81013640345516436 
		0.16818748477502329 0.0029787344337748973;
	setAttr -s 4 ".wl[552].w";
	setAttr ".wl[552].w[1]" 0.034767841870374197;
	setAttr ".wl[552].w[2]" 0.78627155234383228;
	setAttr ".wl[552].w[3]" 0.17392637126265864;
	setAttr ".wl[552].w[11]" 0.005034234523134948;
	setAttr -s 4 ".wl[553].w[1:4]"  0.035411075483637713 0.71142507372984654 
		0.2469525499616407 0.0062113008248748727;
	setAttr -s 4 ".wl[554].w";
	setAttr ".wl[554].w[1]" 0.031192906136752425;
	setAttr ".wl[554].w[2]" 0.69603705854984399;
	setAttr ".wl[554].w[3]" 0.26613887504989236;
	setAttr ".wl[554].w[11]" 0.0066311602635113079;
	setAttr -s 4 ".wl[555].w";
	setAttr ".wl[555].w[1]" 0.032009677250216882;
	setAttr ".wl[555].w[2]" 0.75263686545415365;
	setAttr ".wl[555].w[3]" 0.20908045462329619;
	setAttr ".wl[555].w[11]" 0.0062730026723332938;
	setAttr -s 4 ".wl[556].w";
	setAttr ".wl[556].w[1]" 0.046927053029986697;
	setAttr ".wl[556].w[2]" 0.74135470951099869;
	setAttr ".wl[556].w[3]" 0.2025711008649986;
	setAttr ".wl[556].w[11]" 0.0091471365940159997;
	setAttr -s 4 ".wl[557].w";
	setAttr ".wl[557].w[1]" 0.045061129266196799;
	setAttr ".wl[557].w[2]" 0.69071782513195212;
	setAttr ".wl[557].w[3]" 0.25467087115074377;
	setAttr ".wl[557].w[11]" 0.0095501744511073267;
	setAttr -s 4 ".wl[558].w";
	setAttr ".wl[558].w[1]" 0.20352147674738583;
	setAttr ".wl[558].w[2]" 0.67062160939121085;
	setAttr ".wl[558].w[3]" 0.080439710373843121;
	setAttr ".wl[558].w[7]" 0.045417203487560141;
	setAttr -s 4 ".wl[559].w";
	setAttr ".wl[559].w[1]" 0.22423117515120417;
	setAttr ".wl[559].w[2]" 0.59241532129635699;
	setAttr ".wl[559].w[3]" 0.084709391658272182;
	setAttr ".wl[559].w[11]" 0.098644111894166706;
	setAttr -s 4 ".wl[560].w";
	setAttr ".wl[560].w[1]" 0.084810010622038898;
	setAttr ".wl[560].w[2]" 0.6289442899086467;
	setAttr ".wl[560].w[3]" 0.25858411943834697;
	setAttr ".wl[560].w[7]" 0.027661580030967449;
	setAttr -s 4 ".wl[561].w";
	setAttr ".wl[561].w[1]" 0.10557855462132507;
	setAttr ".wl[561].w[2]" 0.58794739163504062;
	setAttr ".wl[561].w[3]" 0.25824789477207399;
	setAttr ".wl[561].w[11]" 0.048226158971560386;
	setAttr -s 4 ".wl[562].w";
	setAttr ".wl[562].w[1]" 0.083146220600635312;
	setAttr ".wl[562].w[2]" 0.64205329907980269;
	setAttr ".wl[562].w[3]" 0.24914664356048855;
	setAttr ".wl[562].w[7]" 0.025653836759073512;
	setAttr -s 4 ".wl[563].w";
	setAttr ".wl[563].w[1]" 0.10182845228149696;
	setAttr ".wl[563].w[2]" 0.60405512155087848;
	setAttr ".wl[563].w[3]" 0.25039760956527862;
	setAttr ".wl[563].w[11]" 0.043718816602345868;
	setAttr -s 4 ".wl[564].w";
	setAttr ".wl[564].w[1]" 0.20066437533536405;
	setAttr ".wl[564].w[2]" 0.68798627648227217;
	setAttr ".wl[564].w[3]" 0.071341353488881459;
	setAttr ".wl[564].w[7]" 0.040007994693482297;
	setAttr -s 4 ".wl[565].w";
	setAttr ".wl[565].w[1]" 0.22177815725096486;
	setAttr ".wl[565].w[2]" 0.61383650921858601;
	setAttr ".wl[565].w[3]" 0.076418197828802645;
	setAttr ".wl[565].w[11]" 0.087967135701646604;
	setAttr -s 4 ".wl[566].w";
	setAttr ".wl[566].w[1]" 0.10173806947541401;
	setAttr ".wl[566].w[2]" 0.66961317739919501;
	setAttr ".wl[566].w[3]" 0.20165220008225943;
	setAttr ".wl[566].w[11]" 0.026996553043131578;
	setAttr -s 4 ".wl[567].w";
	setAttr ".wl[567].w[1]" 0.078510364022600795;
	setAttr ".wl[567].w[2]" 0.74626489305986499;
	setAttr ".wl[567].w[3]" 0.15683133719339817;
	setAttr ".wl[567].w[11]" 0.018393405724136034;
	setAttr -s 4 ".wl[568].w";
	setAttr ".wl[568].w[1]" 0.38281450235721248;
	setAttr ".wl[568].w[2]" 0.36373705673373741;
	setAttr ".wl[568].w[7]" 0.04403003280058685;
	setAttr ".wl[568].w[11]" 0.20941840810846332;
	setAttr -s 4 ".wl[569].w";
	setAttr ".wl[569].w[1]" 0.37005713692578579;
	setAttr ".wl[569].w[2]" 0.35931310453085313;
	setAttr ".wl[569].w[7]" 0.051465861028923122;
	setAttr ".wl[569].w[11]" 0.21916389751443791;
	setAttr -s 4 ".wl[570].w";
	setAttr ".wl[570].w[1]" 0.13006760028507058;
	setAttr ".wl[570].w[2]" 0.69530584750838265;
	setAttr ".wl[570].w[3]" 0.15503738539158324;
	setAttr ".wl[570].w[11]" 0.019589166814963508;
	setAttr -s 4 ".wl[571].w";
	setAttr ".wl[571].w[1]" 0.079732401911525627;
	setAttr ".wl[571].w[2]" 0.81384395203893578;
	setAttr ".wl[571].w[3]" 0.097001140018436752;
	setAttr ".wl[571].w[11]" 0.0094225060311018266;
	setAttr -s 4 ".wl[572].w";
	setAttr ".wl[572].w[1]" 0.38437834733161153;
	setAttr ".wl[572].w[2]" 0.2653446583968932;
	setAttr ".wl[572].w[7]" 0.16148642827376283;
	setAttr ".wl[572].w[11]" 0.1887905659977325;
	setAttr -s 4 ".wl[573].w";
	setAttr ".wl[573].w[1]" 0.3655514250573822;
	setAttr ".wl[573].w[2]" 0.25570579378938596;
	setAttr ".wl[573].w[7]" 0.17738837395662532;
	setAttr ".wl[573].w[11]" 0.20135440719660658;
	setAttr -s 4 ".wl[574].w";
	setAttr ".wl[574].w[1]" 0.088330227452664709;
	setAttr ".wl[574].w[2]" 0.70176519793772274;
	setAttr ".wl[574].w[3]" 0.1973127448586488;
	setAttr ".wl[574].w[7]" 0.012591829750963644;
	setAttr -s 4 ".wl[575].w";
	setAttr ".wl[575].w[1]" 0.062154889122544132;
	setAttr ".wl[575].w[2]" 0.7878510400358445;
	setAttr ".wl[575].w[3]" 0.14235735173867242;
	setAttr ".wl[575].w[7]" 0.0076367191029388486;
	setAttr -s 4 ".wl[576].w";
	setAttr ".wl[576].w[1]" 0.43479459697087597;
	setAttr ".wl[576].w[2]" 0.41410187016668737;
	setAttr ".wl[576].w[7]" 0.093588135102478126;
	setAttr ".wl[576].w[11]" 0.057515397759958557;
	setAttr -s 4 ".wl[577].w";
	setAttr ".wl[577].w[1]" 0.41832065688953352;
	setAttr ".wl[577].w[2]" 0.40730495923051985;
	setAttr ".wl[577].w[7]" 0.10509330875640463;
	setAttr ".wl[577].w[11]" 0.069281075123542091;
	setAttr -s 4 ".wl[578].w";
	setAttr ".wl[578].w[1]" 0.37015493462730825;
	setAttr ".wl[578].w[2]" 0.54998443094538818;
	setAttr ".wl[578].w[7]" 0.042370208617589659;
	setAttr ".wl[578].w[8]" 0.037490425809713915;
	setAttr -s 4 ".wl[579].w";
	setAttr ".wl[579].w[1]" 0.067898137864794289;
	setAttr ".wl[579].w[2]" 0.68737675540392351;
	setAttr ".wl[579].w[3]" 0.2291146053630434;
	setAttr ".wl[579].w[7]" 0.015610501368238874;
	setAttr -s 4 ".wl[580].w";
	setAttr ".wl[580].w[1]" 0.05953824568033738;
	setAttr ".wl[580].w[2]" 0.72708239404983177;
	setAttr ".wl[580].w[3]" 0.20080256606602198;
	setAttr ".wl[580].w[7]" 0.012576794203808811;
	setAttr -s 4 ".wl[581].w";
	setAttr ".wl[581].w[1]" 0.37219795450818305;
	setAttr ".wl[581].w[2]" 0.56863766687613071;
	setAttr ".wl[581].w[7]" 0.031937682034354584;
	setAttr ".wl[581].w[8]" 0.027226696581331703;
	setAttr -s 4 ".wl[582].w";
	setAttr ".wl[582].w[1]" 0.12831724349164592;
	setAttr ".wl[582].w[2]" 0.63036307205283959;
	setAttr ".wl[582].w[3]" 0.1970367638006319;
	setAttr ".wl[582].w[7]" 0.044282920654882575;
	setAttr -s 4 ".wl[583].w";
	setAttr ".wl[583].w[1]" 0.15130535348574603;
	setAttr ".wl[583].w[2]" 0.57545386571141977;
	setAttr ".wl[583].w[3]" 0.19709673295172245;
	setAttr ".wl[583].w[11]" 0.076144047851111812;
	setAttr -s 4 ".wl[584].w";
	setAttr ".wl[584].w[1]" 0.082449657312663183;
	setAttr ".wl[584].w[2]" 0.58202991446068053;
	setAttr ".wl[584].w[3]" 0.30392732541215345;
	setAttr ".wl[584].w[7]" 0.031593102814502845;
	setAttr -s 4 ".wl[585].w";
	setAttr ".wl[585].w[1]" 0.10056344355966478;
	setAttr ".wl[585].w[2]" 0.55201975873483589;
	setAttr ".wl[585].w[3]" 0.29784803372345986;
	setAttr ".wl[585].w[11]" 0.049568763982039514;
	setAttr -s 4 ".wl[586].w";
	setAttr ".wl[586].w[1]" 0.12574019579317047;
	setAttr ".wl[586].w[2]" 0.65057473844405012;
	setAttr ".wl[586].w[3]" 0.18484409951457018;
	setAttr ".wl[586].w[7]" 0.038840966248209201;
	setAttr -s 4 ".wl[587].w";
	setAttr ".wl[587].w[1]" 0.14514104264126823;
	setAttr ".wl[587].w[2]" 0.60379171023004219;
	setAttr ".wl[587].w[3]" 0.18613177057078512;
	setAttr ".wl[587].w[11]" 0.064935476557904465;
	setAttr -s 4 ".wl[588].w";
	setAttr ".wl[588].w[1]" 0.19135554928827936;
	setAttr ".wl[588].w[2]" 0.65693318545261536;
	setAttr ".wl[588].w[3]" 0.10077385106459877;
	setAttr ".wl[588].w[7]" 0.050937414194506411;
	setAttr -s 4 ".wl[589].w";
	setAttr ".wl[589].w[1]" 0.21159561198733801;
	setAttr ".wl[589].w[2]" 0.58482830485661608;
	setAttr ".wl[589].w[3]" 0.10463748529093971;
	setAttr ".wl[589].w[11]" 0.098938597865106143;
	setAttr -s 4 ".wl[590].w";
	setAttr ".wl[590].w[1]" 0.086780370195881112;
	setAttr ".wl[590].w[2]" 0.64586644415549732;
	setAttr ".wl[590].w[3]" 0.23578809053614511;
	setAttr ".wl[590].w[11]" 0.031565095112476467;
	setAttr -s 4 ".wl[591].w";
	setAttr ".wl[591].w[1]" 0.078457302560414235;
	setAttr ".wl[591].w[2]" 0.6807780051634964;
	setAttr ".wl[591].w[3]" 0.2143804057226934;
	setAttr ".wl[591].w[11]" 0.02638428655339585;
	setAttr -s 4 ".wl[592].w";
	setAttr ".wl[592].w[1]" 0.04713058186797036;
	setAttr ".wl[592].w[2]" 0.66705898451906354;
	setAttr ".wl[592].w[3]" 0.27283658604905081;
	setAttr ".wl[592].w[11]" 0.012973847563915234;
	setAttr -s 4 ".wl[593].w";
	setAttr ".wl[593].w[1]" 0.36515789141928418;
	setAttr ".wl[593].w[2]" 0.50635643889213211;
	setAttr ".wl[593].w[3]" 0.018598570860957203;
	setAttr ".wl[593].w[11]" 0.10988709882762661;
	setAttr -s 4 ".wl[594].w";
	setAttr ".wl[594].w[1]" 0.34575129555680295;
	setAttr ".wl[594].w[2]" 0.55061558755244433;
	setAttr ".wl[594].w[3]" 0.033741031801057232;
	setAttr ".wl[594].w[11]" 0.069892085089695508;
	setAttr -s 4 ".wl[595].w";
	setAttr ".wl[595].w[1]" 0.35614686847598975;
	setAttr ".wl[595].w[2]" 0.48769856286424962;
	setAttr ".wl[595].w[3]" 0.025061677482620833;
	setAttr ".wl[595].w[11]" 0.13109289117713985;
	setAttr -s 4 ".wl[596].w";
	setAttr ".wl[596].w[1]" 0.36597312594052794;
	setAttr ".wl[596].w[2]" 0.3020249707984502;
	setAttr ".wl[596].w[7]" 0.069395246083202577;
	setAttr ".wl[596].w[11]" 0.26260665717781928;
	setAttr -s 4 ".wl[597].w";
	setAttr ".wl[597].w[1]" 0.34676855387991379;
	setAttr ".wl[597].w[2]" 0.50392663077334821;
	setAttr ".wl[597].w[3]" 0.054541850703306055;
	setAttr ".wl[597].w[11]" 0.094762964643431841;
	setAttr -s 4 ".wl[598].w";
	setAttr ".wl[598].w[1]" 0.12548513377888118;
	setAttr ".wl[598].w[2]" 0.67394840003555923;
	setAttr ".wl[598].w[3]" 0.17533788055420582;
	setAttr ".wl[598].w[11]" 0.025228585631353828;
	setAttr -s 4 ".wl[599].w";
	setAttr ".wl[599].w[1]" 0.084959282757868876;
	setAttr ".wl[599].w[2]" 0.78062957596538141;
	setAttr ".wl[599].w[3]" 0.12038798235936318;
	setAttr ".wl[599].w[11]" 0.014023158917386548;
	setAttr -s 4 ".wl[600].w";
	setAttr ".wl[600].w[1]" 0.03367726437212766;
	setAttr ".wl[600].w[2]" 0.75610355676861229;
	setAttr ".wl[600].w[3]" 0.20488288363677418;
	setAttr ".wl[600].w[11]" 0.0053362952224857308;
	setAttr -s 4 ".wl[601].w";
	setAttr ".wl[601].w[1]" 0.37736444990884782;
	setAttr ".wl[601].w[2]" 0.28602055181098784;
	setAttr ".wl[601].w[7]" 0.11540876615161461;
	setAttr ".wl[601].w[11]" 0.22120623212854981;
	setAttr -s 4 ".wl[602].w";
	setAttr ".wl[602].w[1]" 0.43939829961081489;
	setAttr ".wl[602].w[2]" 0.4618991484931918;
	setAttr ".wl[602].w[7]" 0.034259030139527996;
	setAttr ".wl[602].w[11]" 0.064443521756465325;
	setAttr -s 4 ".wl[603].w";
	setAttr ".wl[603].w[1]" 0.35953720262771494;
	setAttr ".wl[603].w[2]" 0.28099942930659599;
	setAttr ".wl[603].w[7]" 0.12890764499973337;
	setAttr ".wl[603].w[11]" 0.23055572306595568;
	setAttr -s 4 ".wl[604].w";
	setAttr ".wl[604].w[1]" 0.37482232332726562;
	setAttr ".wl[604].w[2]" 0.20634275848664949;
	setAttr ".wl[604].w[7]" 0.21196601253110736;
	setAttr ".wl[604].w[11]" 0.20686890565497751;
	setAttr -s 4 ".wl[605].w";
	setAttr ".wl[605].w[1]" 0.41814101339697435;
	setAttr ".wl[605].w[2]" 0.42628816026004407;
	setAttr ".wl[605].w[7]" 0.056355678591363097;
	setAttr ".wl[605].w[11]" 0.099215147751618479;
	setAttr -s 4 ".wl[606].w";
	setAttr ".wl[606].w[1]" 0.11851709144497222;
	setAttr ".wl[606].w[2]" 0.69258142108040821;
	setAttr ".wl[606].w[3]" 0.17369268568060434;
	setAttr ".wl[606].w[11]" 0.015208801794015223;
	setAttr -s 4 ".wl[607].w";
	setAttr ".wl[607].w[1]" 0.075573666864770878;
	setAttr ".wl[607].w[2]" 0.803250957264661;
	setAttr ".wl[607].w[3]" 0.11352237252046518;
	setAttr ".wl[607].w[11]" 0.0076530033501029633;
	setAttr -s 4 ".wl[608].w[1:4]"  0.036122107248534721 0.69570231177451436 
		0.26076501020291137 0.0074105707740396128;
	setAttr -s 4 ".wl[609].w";
	setAttr ".wl[609].w[1]" 0.39914982405286137;
	setAttr ".wl[609].w[2]" 0.30299231521256248;
	setAttr ".wl[609].w[7]" 0.16006796323577371;
	setAttr ".wl[609].w[11]" 0.13778989749880236;
	setAttr -s 4 ".wl[610].w";
	setAttr ".wl[610].w[1]" 0.34776937884332249;
	setAttr ".wl[610].w[2]" 0.59373689035788313;
	setAttr ".wl[610].w[3]" 0.032493249249022402;
	setAttr ".wl[610].w[7]" 0.02600048154977198;
	setAttr -s 4 ".wl[611].w";
	setAttr ".wl[611].w[1]" 0.37839775359542938;
	setAttr ".wl[611].w[2]" 0.29621513131161181;
	setAttr ".wl[611].w[7]" 0.17261104714188993;
	setAttr ".wl[611].w[11]" 0.15277606795106893;
	setAttr -s 4 ".wl[612].w";
	setAttr ".wl[612].w[1]" 0.42070064387995876;
	setAttr ".wl[612].w[2]" 0.34818496516489217;
	setAttr ".wl[612].w[7]" 0.14585711294281972;
	setAttr ".wl[612].w[11]" 0.085257278012329393;
	setAttr -s 4 ".wl[613].w";
	setAttr ".wl[613].w[1]" 0.35886989998030921;
	setAttr ".wl[613].w[2]" 0.54415601559990456;
	setAttr ".wl[613].w[3]" 0.056393852617538293;
	setAttr ".wl[613].w[7]" 0.04058023180224786;
	setAttr -s 4 ".wl[614].w";
	setAttr ".wl[614].w[1]" 0.18346924124879427;
	setAttr ".wl[614].w[2]" 0.70348140698657713;
	setAttr ".wl[614].w[3]" 0.084173137250480229;
	setAttr ".wl[614].w[7]" 0.028876214514148429;
	setAttr -s 4 ".wl[615].w[1:4]"  0.044765957920862291 0.65511247632190894 
		0.2882682842097265 0.011853281547502302;
	setAttr -s 4 ".wl[616].w";
	setAttr ".wl[616].w[1]" 0.16144774960193181;
	setAttr ".wl[616].w[2]" 0.76041623949801829;
	setAttr ".wl[616].w[3]" 0.057926829084364639;
	setAttr ".wl[616].w[7]" 0.020209181815685394;
	setAttr -s 4 ".wl[617].w";
	setAttr ".wl[617].w[1]" 0.42451584822682703;
	setAttr ".wl[617].w[2]" 0.49811404912891177;
	setAttr ".wl[617].w[7]" 0.043028534578775944;
	setAttr ".wl[617].w[8]" 0.034341568065485294;
	setAttr -s 4 ".wl[618].w";
	setAttr ".wl[618].w[1]" 0.14190090695782059;
	setAttr ".wl[618].w[2]" 0.53980460893936899;
	setAttr ".wl[618].w[3]" 0.24144283043369655;
	setAttr ".wl[618].w[11]" 0.076851653669113743;
	setAttr -s 4 ".wl[619].w";
	setAttr ".wl[619].w[1]" 0.12266615574290321;
	setAttr ".wl[619].w[2]" 0.57919090538835627;
	setAttr ".wl[619].w[3]" 0.2470458613290778;
	setAttr ".wl[619].w[7]" 0.051097077539662847;
	setAttr -s 4 ".wl[620].w";
	setAttr ".wl[620].w[1]" 0.060375734302786761;
	setAttr ".wl[620].w[2]" 0.62219234277831459;
	setAttr ".wl[620].w[3]" 0.29519668627493439;
	setAttr ".wl[620].w[11]" 0.022235236643964332;
	setAttr -s 4 ".wl[621].w";
	setAttr ".wl[621].w[1]" 0.18997071920622133;
	setAttr ".wl[621].w[2]" 0.69097350388883749;
	setAttr ".wl[621].w[3]" 0.06630418205330689;
	setAttr ".wl[621].w[11]" 0.052751594851634304;
	setAttr -s 4 ".wl[622].w";
	setAttr ".wl[622].w[1]" 0.3910179170053934;
	setAttr ".wl[622].w[2]" 0.43909686188012625;
	setAttr ".wl[622].w[11]" 0.15475777370285324;
	setAttr ".wl[622].w[12]" 0.015127447411627071;
	setAttr -s 4 ".wl[623].w";
	setAttr ".wl[623].w[1]" 0.20413611426167808;
	setAttr ".wl[623].w[2]" 0.63982561809112659;
	setAttr ".wl[623].w[3]" 0.087793976437515298;
	setAttr ".wl[623].w[11]" 0.068244291209679911;
	setAttr -s 4 ".wl[624].w";
	setAttr ".wl[624].w[1]" 0.041042365312104458;
	setAttr ".wl[624].w[2]" 0.71104728898034331;
	setAttr ".wl[624].w[3]" 0.23937712133367939;
	setAttr ".wl[624].w[11]" 0.0085332243738728795;
	setAttr -s 4 ".wl[625].w";
	setAttr ".wl[625].w[1]" 0.42150597280572477;
	setAttr ".wl[625].w[2]" 0.47131704226143134;
	setAttr ".wl[625].w[3]" 0.028180440829695331;
	setAttr ".wl[625].w[11]" 0.078996544103148575;
	setAttr -s 4 ".wl[626].w";
	setAttr ".wl[626].w[1]" 0.36831019801941894;
	setAttr ".wl[626].w[2]" 0.22650526826960055;
	setAttr ".wl[626].w[7]" 0.16383855587746704;
	setAttr ".wl[626].w[11]" 0.2413459778335135;
	setAttr -s 4 ".wl[627].w";
	setAttr ".wl[627].w[1]" 0.40605106800283408;
	setAttr ".wl[627].w[2]" 0.43525288967906506;
	setAttr ".wl[627].w[3]" 0.04567569546436398;
	setAttr ".wl[627].w[11]" 0.11302034685373681;
	setAttr -s 4 ".wl[628].w[1:4]"  0.03513967897746216 0.72868692110836331 
		0.23034871913166152 0.0058246807825129915;
	setAttr -s 4 ".wl[629].w";
	setAttr ".wl[629].w[1]" 0.43345624663685112;
	setAttr ".wl[629].w[2]" 0.48959444040707678;
	setAttr ".wl[629].w[7]" 0.035114693113213837;
	setAttr ".wl[629].w[11]" 0.041834619842858266;
	setAttr -s 4 ".wl[630].w";
	setAttr ".wl[630].w[1]" 0.38257511176738962;
	setAttr ".wl[630].w[2]" 0.2356130630862131;
	setAttr ".wl[630].w[7]" 0.21510285557345785;
	setAttr ".wl[630].w[11]" 0.16670896957293951;
	setAttr -s 4 ".wl[631].w";
	setAttr ".wl[631].w[1]" 0.42194345639221537;
	setAttr ".wl[631].w[2]" 0.45493028604426705;
	setAttr ".wl[631].w[7]" 0.05585854144311081;
	setAttr ".wl[631].w[11]" 0.067267716120406801;
	setAttr -s 4 ".wl[632].w";
	setAttr ".wl[632].w[0]" 0.48549009912096108;
	setAttr ".wl[632].w[1]" 0.0062945517708598731;
	setAttr ".wl[632].w[15]" 0.022725249987218053;
	setAttr ".wl[632].w[18]" 0.48549009912096108;
	setAttr -s 4 ".wl[633].w";
	setAttr ".wl[633].w[0]" 0.52637912588551561;
	setAttr ".wl[633].w[1]" 0.0202078167802998;
	setAttr ".wl[633].w[15]" 0.22873562064974018;
	setAttr ".wl[633].w[18]" 0.22467743668444443;
	setAttr -s 4 ".wl[634].w";
	setAttr ".wl[634].w[0]" 0.49766652698140668;
	setAttr ".wl[634].w[1]" 0.017163634668207743;
	setAttr ".wl[634].w[15]" 0.24562845672840078;
	setAttr ".wl[634].w[18]" 0.23954138162198493;
	setAttr -s 4 ".wl[635].w";
	setAttr ".wl[635].w[0]" 0.48500459812647667;
	setAttr ".wl[635].w[1]" 0.0063437051227314637;
	setAttr ".wl[635].w[15]" 0.023647098624315136;
	setAttr ".wl[635].w[18]" 0.48500459812647667;
	setAttr -s 4 ".wl[636].w";
	setAttr ".wl[636].w[0]" 0.49665982759572064;
	setAttr ".wl[636].w[15]" 0.0046720640100454151;
	setAttr ".wl[636].w[18]" 0.49665982759572064;
	setAttr ".wl[636].w[19]" 0.0020082807985133473;
	setAttr -s 4 ".wl[637].w";
	setAttr ".wl[637].w[1]" 0.402946661503506;
	setAttr ".wl[637].w[2]" 0.10035854268168701;
	setAttr ".wl[637].w[11]" 0.4161792420533908;
	setAttr ".wl[637].w[12]" 0.080515553761416295;
	setAttr -s 4 ".wl[638].w";
	setAttr ".wl[638].w[1]" 0.35477836149914843;
	setAttr ".wl[638].w[2]" 0.30123834634722119;
	setAttr ".wl[638].w[7]" 0.18732973967620276;
	setAttr ".wl[638].w[11]" 0.15665355247742743;
	setAttr -s 4 ".wl[639].w";
	setAttr ".wl[639].w[1]" 0.4464164658822059;
	setAttr ".wl[639].w[2]" 0.43933376246533867;
	setAttr ".wl[639].w[7]" 0.041484825466466473;
	setAttr ".wl[639].w[11]" 0.072764946185988902;
	setAttr -s 4 ".wl[640].w";
	setAttr ".wl[640].w[1]" 0.40441158761790608;
	setAttr ".wl[640].w[2]" 0.078835378589683272;
	setAttr ".wl[640].w[11]" 0.46543771983274035;
	setAttr ".wl[640].w[12]" 0.051315313959670247;
	setAttr -s 4 ".wl[641].w";
	setAttr ".wl[641].w[1]" 0.28245826542508157;
	setAttr ".wl[641].w[2]" 0.01643670366640149;
	setAttr ".wl[641].w[11]" 0.66659549387248773;
	setAttr ".wl[641].w[12]" 0.034509537036029214;
	setAttr -s 4 ".wl[642].w";
	setAttr ".wl[642].w[1]" 0.73836557281782533;
	setAttr ".wl[642].w[2]" 0.25912543764490742;
	setAttr ".wl[642].w[7]" 0.00099911279538474019;
	setAttr ".wl[642].w[11]" 0.0015098767418824605;
	setAttr -s 4 ".wl[643].w";
	setAttr ".wl[643].w[0]" 0.34821950586110972;
	setAttr ".wl[643].w[1]" 0.37184092482063841;
	setAttr ".wl[643].w[7]" 0.16881806479180225;
	setAttr ".wl[643].w[11]" 0.1111215045264495;
	setAttr -s 4 ".wl[644].w";
	setAttr ".wl[644].w[0]" 0.38356040255538726;
	setAttr ".wl[644].w[1]" 0.38357194330429839;
	setAttr ".wl[644].w[7]" 0.16030741867035192;
	setAttr ".wl[644].w[11]" 0.072560235469962514;
	setAttr -s 4 ".wl[645].w";
	setAttr ".wl[645].w[0]" 0.26857160079139952;
	setAttr ".wl[645].w[1]" 0.35392079921391256;
	setAttr ".wl[645].w[11]" 0.20859403417809264;
	setAttr ".wl[645].w[12]" 0.16891356581659528;
	setAttr -s 4 ".wl[646].w";
	setAttr ".wl[646].w[0]" 0.18571691790654996;
	setAttr ".wl[646].w[1]" 0.28843419702983419;
	setAttr ".wl[646].w[11]" 0.27082823686127072;
	setAttr ".wl[646].w[12]" 0.2550206482023451;
	setAttr -s 4 ".wl[647].w";
	setAttr ".wl[647].w[0]" 0.17985499716435907;
	setAttr ".wl[647].w[1]" 0.31361887200174215;
	setAttr ".wl[647].w[11]" 0.27618607418030328;
	setAttr ".wl[647].w[12]" 0.23034005665359544;
	setAttr -s 4 ".wl[648].w";
	setAttr ".wl[648].w[0]" 0.43628757115176509;
	setAttr ".wl[648].w[1]" 0.077521827585439546;
	setAttr ".wl[648].w[15]" 0.24711420683884044;
	setAttr ".wl[648].w[18]" 0.23907639442395498;
	setAttr -s 4 ".wl[649].w";
	setAttr ".wl[649].w[0]" 0.46264623277975947;
	setAttr ".wl[649].w[1]" 0.083522576827075645;
	setAttr ".wl[649].w[15]" 0.23002019711126365;
	setAttr ".wl[649].w[18]" 0.22381099328190127;
	setAttr -s 4 ".wl[650].w";
	setAttr ".wl[650].w[0]" 0.44794851157042753;
	setAttr ".wl[650].w[1]" 0.048451988790989345;
	setAttr ".wl[650].w[15]" 0.055650988068155653;
	setAttr ".wl[650].w[18]" 0.44794851157042753;
	setAttr -s 4 ".wl[651].w";
	setAttr ".wl[651].w[0]" 0.47098543486344674;
	setAttr ".wl[651].w[1]" 0.030026250219949088;
	setAttr ".wl[651].w[15]" 0.028002880053157393;
	setAttr ".wl[651].w[18]" 0.47098543486344674;
	setAttr -s 4 ".wl[652].w";
	setAttr ".wl[652].w[0]" 0.44422278062899134;
	setAttr ".wl[652].w[1]" 0.050565718563450372;
	setAttr ".wl[652].w[15]" 0.060988720178567095;
	setAttr ".wl[652].w[18]" 0.44422278062899123;
	setAttr -s 4 ".wl[653].w";
	setAttr ".wl[653].w[0]" 0.40253542648860113;
	setAttr ".wl[653].w[15]" 0.0021837077047390254;
	setAttr ".wl[653].w[18]" 0.59380559349791551;
	setAttr ".wl[653].w[19]" 0.0014752723087442382;
	setAttr -s 4 ".wl[654].w";
	setAttr ".wl[654].w[0]" 0.45675469823164544;
	setAttr ".wl[654].w[15]" 0.26348337636668318;
	setAttr ".wl[654].w[16]" 0.0132019103845327;
	setAttr ".wl[654].w[18]" 0.26656001501713872;
	setAttr -s 4 ".wl[655].w";
	setAttr ".wl[655].w[0]" 0.47601833954911354;
	setAttr ".wl[655].w[15]" 0.017767376763605031;
	setAttr ".wl[655].w[18]" 0.5004161144886301;
	setAttr ".wl[655].w[19]" 0.0057981691986512445;
	setAttr -s 4 ".wl[656].w";
	setAttr ".wl[656].w[0]" 0.47588768000115045;
	setAttr ".wl[656].w[15]" 0.018283784045381585;
	setAttr ".wl[656].w[18]" 0.49985547443507544;
	setAttr ".wl[656].w[19]" 0.0059730615183924514;
	setAttr -s 4 ".wl[657].w";
	setAttr ".wl[657].w[0]" 0.43372496036452507;
	setAttr ".wl[657].w[15]" 0.27637965209299931;
	setAttr ".wl[657].w[16]" 0.01123197696074193;
	setAttr ".wl[657].w[18]" 0.27866341058173366;
	setAttr -s 4 ".wl[658].w";
	setAttr ".wl[658].w[0]" 0.11245091005775361;
	setAttr ".wl[658].w[15]" 0.0051663779328315141;
	setAttr ".wl[658].w[18]" 0.85300602903494949;
	setAttr ".wl[658].w[19]" 0.029376682974465343;
	setAttr -s 4 ".wl[659].w";
	setAttr ".wl[659].w[0]" 0.30186521189443549;
	setAttr ".wl[659].w[15]" 0.31558935017162942;
	setAttr ".wl[659].w[16]" 0.061794567247166275;
	setAttr ".wl[659].w[18]" 0.32075087068676889;
	setAttr -s 4 ".wl[660].w";
	setAttr ".wl[660].w[0]" 0.25699909898634354;
	setAttr ".wl[660].w[15]" 0.026730980767868069;
	setAttr ".wl[660].w[18]" 0.66169686741487244;
	setAttr ".wl[660].w[19]" 0.054573052830916;
	setAttr -s 4 ".wl[661].w";
	setAttr ".wl[661].w[0]" 0.2679331394950652;
	setAttr ".wl[661].w[15]" 0.030418774779010449;
	setAttr ".wl[661].w[18]" 0.64073444814942115;
	setAttr ".wl[661].w[19]" 0.06091363757650315;
	setAttr -s 4 ".wl[662].w";
	setAttr ".wl[662].w[0]" 0.2913334046007019;
	setAttr ".wl[662].w[15]" 0.3235643148391899;
	setAttr ".wl[662].w[16]" 0.057024410060893191;
	setAttr ".wl[662].w[18]" 0.328077870499215;
	setAttr -s 4 ".wl[663].w";
	setAttr ".wl[663].w[0]" 0.010484739415871653;
	setAttr ".wl[663].w[18]" 0.47930776140580211;
	setAttr ".wl[663].w[19]" 0.49941022225845416;
	setAttr ".wl[663].w[20]" 0.01079727691987214;
	setAttr -s 4 ".wl[664].w";
	setAttr ".wl[664].w[15]" 0.25113628443130792;
	setAttr ".wl[664].w[16]" 0.24702466075921678;
	setAttr ".wl[664].w[18]" 0.25384322370111284;
	setAttr ".wl[664].w[19]" 0.24799583110836249;
	setAttr -s 4 ".wl[665].w";
	setAttr ".wl[665].w[0]" 0.032847954146236556;
	setAttr ".wl[665].w[18]" 0.46551594339789781;
	setAttr ".wl[665].w[19]" 0.47458074998456745;
	setAttr ".wl[665].w[20]" 0.027055352471298099;
	setAttr -s 4 ".wl[666].w";
	setAttr ".wl[666].w[0]" 0.043447309361307591;
	setAttr ".wl[666].w[18]" 0.45946726641143076;
	setAttr ".wl[666].w[19]" 0.46279433414655907;
	setAttr ".wl[666].w[20]" 0.034291090080702552;
	setAttr -s 4 ".wl[667].w";
	setAttr ".wl[667].w[15]" 0.25156458951932287;
	setAttr ".wl[667].w[16]" 0.24738053310854555;
	setAttr ".wl[667].w[18]" 0.25369492507477914;
	setAttr ".wl[667].w[19]" 0.2473599522973525;
	setAttr -s 4 ".wl[668].w";
	setAttr ".wl[668].w[16]" 0.0054797349306448893;
	setAttr ".wl[668].w[18]" 0.018416957944944627;
	setAttr ".wl[668].w[19]" 0.4880516535622052;
	setAttr ".wl[668].w[20]" 0.4880516535622052;
	setAttr -s 4 ".wl[669].w";
	setAttr ".wl[669].w[16]" 0.24283296300962265;
	setAttr ".wl[669].w[17]" 0.24283296300962265;
	setAttr ".wl[669].w[19]" 0.25735318020253511;
	setAttr ".wl[669].w[20]" 0.25698089377821959;
	setAttr -s 4 ".wl[670].w";
	setAttr ".wl[670].w[16]" 0.2411331669391073;
	setAttr ".wl[670].w[17]" 0.2411331669391073;
	setAttr ".wl[670].w[19]" 0.25887468346286668;
	setAttr ".wl[670].w[20]" 0.25885898265891866;
	setAttr -s 4 ".wl[671].w";
	setAttr ".wl[671].w[16]" 0.024949012019641507;
	setAttr ".wl[671].w[18]" 0.047594713248802697;
	setAttr ".wl[671].w[19]" 0.46372813736577789;
	setAttr ".wl[671].w[20]" 0.46372813736577789;
	setAttr -s 4 ".wl[672].w";
	setAttr ".wl[672].w[16]" 0.043173417387032821;
	setAttr ".wl[672].w[18]" 0.077484660102798725;
	setAttr ".wl[672].w[19]" 0.4396709612550842;
	setAttr ".wl[672].w[20]" 0.4396709612550842;
	setAttr -s 4 ".wl[673].w";
	setAttr ".wl[673].w[16]" 0.25418955562676093;
	setAttr ".wl[673].w[17]" 0.25418955562676082;
	setAttr ".wl[673].w[19]" 0.24603718212650025;
	setAttr ".wl[673].w[20]" 0.24558370661997794;
	setAttr -s 4 ".wl[674].w";
	setAttr ".wl[674].w[16]" 0.2585827580086773;
	setAttr ".wl[674].w[17]" 0.25755950054535293;
	setAttr ".wl[674].w[19]" 0.24825689762369743;
	setAttr ".wl[674].w[20]" 0.23560084382227237;
	setAttr -s 4 ".wl[675].w";
	setAttr ".wl[675].w[16]" 0.045593026726887385;
	setAttr ".wl[675].w[18]" 0.11674196132701728;
	setAttr ".wl[675].w[19]" 0.43048697453587204;
	setAttr ".wl[675].w[20]" 0.40717803741022324;
	setAttr -s 4 ".wl[676].w";
	setAttr ".wl[676].w[16]" 0.0092677325893527687;
	setAttr ".wl[676].w[18]" 0.051782251743852606;
	setAttr ".wl[676].w[19]" 0.48743088699654319;
	setAttr ".wl[676].w[20]" 0.4515191286702514;
	setAttr -s 4 ".wl[677].w";
	setAttr ".wl[677].w[16]" 0.029075083707780232;
	setAttr ".wl[677].w[18]" 0.082314038919402643;
	setAttr ".wl[677].w[19]" 0.45647163875874419;
	setAttr ".wl[677].w[20]" 0.432139238614073;
	setAttr -s 4 ".wl[678].w";
	setAttr ".wl[678].w[16]" 0.24784094506866539;
	setAttr ".wl[678].w[17]" 0.24673695804069701;
	setAttr ".wl[678].w[19]" 0.25816779819560592;
	setAttr ".wl[678].w[20]" 0.24725429869503168;
	setAttr -s 4 ".wl[679].w";
	setAttr ".wl[679].w[1]" 0.38659848841502376;
	setAttr ".wl[679].w[2]" 0.053412743581324924;
	setAttr ".wl[679].w[11]" 0.47111892595950411;
	setAttr ".wl[679].w[12]" 0.088869842044147204;
	setAttr -s 4 ".wl[680].w";
	setAttr ".wl[680].w[1]" 0.39944403291250513;
	setAttr ".wl[680].w[2]" 0.28551842426625623;
	setAttr ".wl[680].w[7]" 0.14089926084415649;
	setAttr ".wl[680].w[11]" 0.17413828197708212;
	setAttr -s 4 ".wl[681].w";
	setAttr ".wl[681].w[1]" 0.32068522294230489;
	setAttr ".wl[681].w[2]" 0.17837286893132645;
	setAttr ".wl[681].w[7]" 0.31991602469995156;
	setAttr ".wl[681].w[11]" 0.18102588342641712;
	setAttr -s 4 ".wl[682].w";
	setAttr ".wl[682].w[1]" 0.39284439762204998;
	setAttr ".wl[682].w[2]" 0.077246615164153501;
	setAttr ".wl[682].w[11]" 0.41095030741169847;
	setAttr ".wl[682].w[12]" 0.11895867980209815;
	setAttr -s 4 ".wl[683].w";
	setAttr ".wl[683].w[1]" 0.11982391212011513;
	setAttr ".wl[683].w[2]" 0.01059741680766486;
	setAttr ".wl[683].w[11]" 0.60409493309178997;
	setAttr ".wl[683].w[12]" 0.26548373798043001;
	setAttr -s 4 ".wl[684].w";
	setAttr ".wl[684].w[1]" 0.056661012123817286;
	setAttr ".wl[684].w[11]" 0.46838171651572175;
	setAttr ".wl[684].w[12]" 0.45316540319809678;
	setAttr ".wl[684].w[13]" 0.021791868162364055;
	setAttr -s 4 ".wl[685].w";
	setAttr ".wl[685].w[1]" 0.01867454300694224;
	setAttr ".wl[685].w[2]" 0.0011801415287626149;
	setAttr ".wl[685].w[11]" 0.86126377884666061;
	setAttr ".wl[685].w[12]" 0.11888153661763452;
	setAttr -s 4 ".wl[686].w";
	setAttr ".wl[686].w[1]" 0.015138392611229507;
	setAttr ".wl[686].w[2]" 0.0012455043092456991;
	setAttr ".wl[686].w[11]" 0.80893120732470503;
	setAttr ".wl[686].w[12]" 0.1746848957548198;
	setAttr -s 4 ".wl[687].w[11:14]"  0.020873822681991849 0.48191159582088544 
		0.48207819421712894 0.01513638727999383;
	setAttr -s 4 ".wl[688].w[11:14]"  0.01028223104897274 0.55701010239782844 
		0.42930975299835006 0.0033979135548487956;
	setAttr -s 4 ".wl[689].w[11:14]"  0.0022117536940537245 0.56004160689929405 
		0.43694151771117962 0.00080512169547267587;
	setAttr -s 4 ".wl[690].w[11:14]"  0.0014825487495129593 0.50219854052277169 
		0.49560629570716863 0.00071261502054672783;
	setAttr -s 4 ".wl[691].w[11:14]"  0.0016409807587618606 0.012985504336459452 
		0.49268675745238938 0.49268675745238938;
	setAttr -s 4 ".wl[692].w[11:14]"  0.00027542538138810083 0.0024171778535217732 
		0.49865369838254509 0.49865369838254509;
	setAttr -s 4 ".wl[693].w[11:14]"  2.0765659014991553e-05 0.00019789983479905667 
		0.49989066725309306 0.49989066725309295;
	setAttr -s 4 ".wl[694].w[11:14]"  0.00011322092673576739 0.0010857923803095101 
		0.4994004933464774 0.4994004933464774;
	setAttr -s 4 ".wl[695].w[11:14]"  7.7174682492368545e-05 0.0007755980969668567 
		0.49957361361027042 0.49957361361027042;
	setAttr -s 4 ".wl[696].w";
	setAttr ".wl[696].w[1]" 0.032900628713398057;
	setAttr ".wl[696].w[11]" 0.56434508728390975;
	setAttr ".wl[696].w[12]" 0.39837239454978396;
	setAttr ".wl[696].w[13]" 0.0043818894529082403;
	setAttr -s 4 ".wl[697].w";
	setAttr ".wl[697].w[1]" 0.077305609039242926;
	setAttr ".wl[697].w[11]" 0.51101294634651506;
	setAttr ".wl[697].w[12]" 0.39669302676820845;
	setAttr ".wl[697].w[13]" 0.014988417846033523;
	setAttr -s 4 ".wl[698].w";
	setAttr ".wl[698].w[1]" 0.071764676284249526;
	setAttr ".wl[698].w[2]" 0.00610367695354554;
	setAttr ".wl[698].w[11]" 0.68987800317741155;
	setAttr ".wl[698].w[12]" 0.23225364358479345;
	setAttr -s 4 ".wl[699].w";
	setAttr ".wl[699].w[1]" 0.0093346110156418468;
	setAttr ".wl[699].w[2]" 0.00063246200107363747;
	setAttr ".wl[699].w[11]" 0.91470307472749723;
	setAttr ".wl[699].w[12]" 0.075329852255787158;
	setAttr -s 4 ".wl[700].w[11:14]"  0.00019462551777600171 0.0018139416837289165 
		0.4989957163992475 0.4989957163992475;
	setAttr -s 4 ".wl[701].w[11:14]"  0.0047976417623911763 0.57215350807996868 
		0.42145856219108385 0.0015902879665563548;
	setAttr -s 4 ".wl[702].w[11:14]"  0.00069905914178453401 0.0059225715091126424 
		0.49668918467455148 0.49668918467455137;
	setAttr -s 4 ".wl[703].w[11:14]"  0.0083288951388178628 0.50892103293629998 
		0.47874766534446805 0.0040024065804141664;
	setAttr -s 4 ".wl[704].w[11:14]"  0.00038618924868829684 0.003658941409732937 
		0.49797743467078953 0.49797743467078931;
	setAttr -s 4 ".wl[705].w[11:14]"  0.0023190336645294053 0.49812821556019138 
		0.4981282155601916 0.0014245352150875552;
	setAttr -s 4 ".wl[706].w[11:14]"  8.3442328900818071e-05 0.00084678660783124224 
		0.49953488553163394 0.49953488553163394;
	setAttr -s 4 ".wl[707].w[11:14]"  0.0011989814704856077 0.52965718617815205 
		0.46864986657565549 0.00049396577570691262;
	setAttr -s 4 ".wl[708].w";
	setAttr ".wl[708].w[15]" 0.020691201794940228;
	setAttr ".wl[708].w[16]" 0.48645164298441179;
	setAttr ".wl[708].w[17]" 0.48645164298441179;
	setAttr ".wl[708].w[19]" 0.0064055122362361569;
	setAttr -s 4 ".wl[709].w";
	setAttr ".wl[709].w[15]" 0.053100594659753614;
	setAttr ".wl[709].w[16]" 0.45873053257363039;
	setAttr ".wl[709].w[17]" 0.45873053257363028;
	setAttr ".wl[709].w[19]" 0.029438340192985798;
	setAttr -s 4 ".wl[710].w";
	setAttr ".wl[710].w[15]" 0.074189812162700985;
	setAttr ".wl[710].w[16]" 0.4419913063573841;
	setAttr ".wl[710].w[17]" 0.44199130635738398;
	setAttr ".wl[710].w[19]" 0.041827575122530922;
	setAttr -s 4 ".wl[711].w";
	setAttr ".wl[711].w[0]" 0.48625037310431646;
	setAttr ".wl[711].w[1]" 0.0060121595523707346;
	setAttr ".wl[711].w[15]" 0.48625037310431646;
	setAttr ".wl[711].w[18]" 0.021487094238996418;
	setAttr -s 4 ".wl[712].w";
	setAttr ".wl[712].w[0]" 0.46711838509325693;
	setAttr ".wl[712].w[15]" 0.50856978914992224;
	setAttr ".wl[712].w[16]" 0.006237389127450239;
	setAttr ".wl[712].w[18]" 0.018074436629370742;
	setAttr -s 4 ".wl[713].w";
	setAttr ".wl[713].w[0]" 0.49691895432442162;
	setAttr ".wl[713].w[15]" 0.49691895432442162;
	setAttr ".wl[713].w[16]" 0.0019475400341475899;
	setAttr ".wl[713].w[18]" 0.0042145513170091546;
	setAttr -s 4 ".wl[714].w";
	setAttr ".wl[714].w[0]" 0.3680185146563123;
	setAttr ".wl[714].w[15]" 0.62833339655192089;
	setAttr ".wl[714].w[16]" 0.0015318380904473683;
	setAttr ".wl[714].w[18]" 0.0021162507013194401;
	setAttr -s 4 ".wl[715].w";
	setAttr ".wl[715].w[0]" 0.48590108781103358;
	setAttr ".wl[715].w[1]" 0.0060200273718985786;
	setAttr ".wl[715].w[15]" 0.48590108781103358;
	setAttr ".wl[715].w[18]" 0.022177797006034294;
	setAttr -s 4 ".wl[716].w";
	setAttr ".wl[716].w[0]" 0.46715304123800971;
	setAttr ".wl[716].w[15]" 0.50807122981267616;
	setAttr ".wl[716].w[16]" 0.0063741088005552912;
	setAttr ".wl[716].w[18]" 0.018401620148758801;
	setAttr -s 4 ".wl[717].w";
	setAttr ".wl[717].w[0]" 0.24167632596258629;
	setAttr ".wl[717].w[15]" 0.67174518344127632;
	setAttr ".wl[717].w[16]" 0.059105015294989127;
	setAttr ".wl[717].w[18]" 0.02747347530114834;
	setAttr -s 4 ".wl[718].w";
	setAttr ".wl[718].w[0]" 0.098392215041994732;
	setAttr ".wl[718].w[15]" 0.86575753269590605;
	setAttr ".wl[718].w[16]" 0.030847542029359062;
	setAttr ".wl[718].w[18]" 0.0050027102327401974;
	setAttr -s 4 ".wl[719].w";
	setAttr ".wl[719].w[0]" 0.25266272802087186;
	setAttr ".wl[719].w[15]" 0.65070669157386407;
	setAttr ".wl[719].w[16]" 0.065599005335026675;
	setAttr ".wl[719].w[18]" 0.031031575070237384;
	setAttr -s 4 ".wl[720].w";
	setAttr ".wl[720].w[0]" 0.031388165953076419;
	setAttr ".wl[720].w[15]" 0.45916689639033809;
	setAttr ".wl[720].w[16]" 0.47007189407289135;
	setAttr ".wl[720].w[17]" 0.039373043583694214;
	setAttr -s 4 ".wl[721].w";
	setAttr ".wl[721].w[0]" 0.01002838527287642;
	setAttr ".wl[721].w[15]" 0.47107193005208847;
	setAttr ".wl[721].w[16]" 0.50172737801937617;
	setAttr ".wl[721].w[17]" 0.017172306655658914;
	setAttr -s 4 ".wl[722].w";
	setAttr ".wl[722].w[0]" 0.041097053013613459;
	setAttr ".wl[722].w[15]" 0.44966606675302456;
	setAttr ".wl[722].w[16]" 0.45836281095097181;
	setAttr ".wl[722].w[17]" 0.050874069282390084;
	setAttr -s 4 ".wl[723].w";
	setAttr ".wl[723].w[15]" 0.082697017545170365;
	setAttr ".wl[723].w[16]" 0.44344703174305289;
	setAttr ".wl[723].w[17]" 0.44315885021567875;
	setAttr ".wl[723].w[19]" 0.030697100496098091;
	setAttr -s 4 ".wl[724].w";
	setAttr ".wl[724].w[15]" 0.047916514866010444;
	setAttr ".wl[724].w[16]" 0.47159045690008555;
	setAttr ".wl[724].w[17]" 0.47159045690008577;
	setAttr ".wl[724].w[19]" 0.0089025713338181395;
	setAttr -s 4 ".wl[725].w";
	setAttr ".wl[725].w[15]" 0.1059285178905157;
	setAttr ".wl[725].w[16]" 0.42621754658276478;
	setAttr ".wl[725].w[17]" 0.42600579470737543;
	setAttr ".wl[725].w[19]" 0.041848140819344029;
	setAttr -s 4 ".wl[726].w";
	setAttr ".wl[726].w[0]" 0.44702896952093224;
	setAttr ".wl[726].w[1]" 0.048484549141677317;
	setAttr ".wl[726].w[15]" 0.44702896952093224;
	setAttr ".wl[726].w[18]" 0.057457511816458251;
	setAttr -s 4 ".wl[727].w";
	setAttr ".wl[727].w[0]" 0.45054505535861628;
	setAttr ".wl[727].w[1]" 0.046445543625875829;
	setAttr ".wl[727].w[15]" 0.45054505535861628;
	setAttr ".wl[727].w[18]" 0.052464345656891634;
	setAttr -s 4 ".wl[728].w";
	setAttr ".wl[728].w[0]" 0.47278897771749301;
	setAttr ".wl[728].w[1]" 0.028358069513426721;
	setAttr ".wl[728].w[15]" 0.47278897771749301;
	setAttr ".wl[728].w[18]" 0.026063975051587304;
	setAttr -s 4 ".wl[729].w";
	setAttr ".wl[729].w[1]" 0.14647372229898301;
	setAttr ".wl[729].w[2]" 0.085253441238482802;
	setAttr ".wl[729].w[7]" 0.41323749037056262;
	setAttr ".wl[729].w[8]" 0.35503534609197146;
	setAttr -s 4 ".wl[730].w";
	setAttr ".wl[730].w[1]" 0.20651698927034029;
	setAttr ".wl[730].w[2]" 0.046205534639922795;
	setAttr ".wl[730].w[7]" 0.42453412585940259;
	setAttr ".wl[730].w[8]" 0.32274335023033429;
	setAttr -s 4 ".wl[731].w";
	setAttr ".wl[731].w[1]" 0.027593363212107258;
	setAttr ".wl[731].w[2]" 0.02220101007914017;
	setAttr ".wl[731].w[7]" 0.48053241586958662;
	setAttr ".wl[731].w[8]" 0.46967321083916591;
	setAttr -s 4 ".wl[732].w";
	setAttr ".wl[732].w[1]" 0.1979050633006007;
	setAttr ".wl[732].w[2]" 0.19274784369029485;
	setAttr ".wl[732].w[7]" 0.32254542858516927;
	setAttr ".wl[732].w[8]" 0.28680166442393512;
	setAttr -s 4 ".wl[733].w";
	setAttr ".wl[733].w[0]" 0.1964719751818389;
	setAttr ".wl[733].w[1]" 0.28121297840820747;
	setAttr ".wl[733].w[7]" 0.28386761101812791;
	setAttr ".wl[733].w[8]" 0.23844743539182572;
	setAttr -s 4 ".wl[734].w[7:10]"  0.0025140048367104048 0.36048925667656506 
		0.63225634989838364 0.0047403885883408178;
	setAttr -s 4 ".wl[735].w[7:10]"  0.00061752563481317529 0.0053368738392792344 
		0.49702280026295381 0.49702280026295381;
	setAttr -s 4 ".wl[736].w[7:10]"  6.6788179832703807e-05 0.00061983597810670707 
		0.49965668792103035 0.49965668792103035;
	setAttr -s 4 ".wl[737].w[7:10]"  0.0011606018549881688 0.32398149975174628 
		0.67289644590514619 0.001961452488119369;
	setAttr -s 4 ".wl[738].w";
	setAttr ".wl[738].w[1]" 0.19094250082190145;
	setAttr ".wl[738].w[2]" 0.10876034838467152;
	setAttr ".wl[738].w[7]" 0.38742598030671499;
	setAttr ".wl[738].w[8]" 0.31287117048671204;
	setAttr -s 4 ".wl[739].w";
	setAttr ".wl[739].w[1]" 0.029727076899496697;
	setAttr ".wl[739].w[7]" 0.33990921586214434;
	setAttr ".wl[739].w[8]" 0.61249211500921896;
	setAttr ".wl[739].w[9]" 0.017871592229140015;
	setAttr -s 4 ".wl[740].w";
	setAttr ".wl[740].w[1]" 0.014380600259549731;
	setAttr ".wl[740].w[2]" 0.0051439820035263928;
	setAttr ".wl[740].w[7]" 0.44511516144564506;
	setAttr ".wl[740].w[8]" 0.53536025629127881;
	setAttr -s 4 ".wl[741].w";
	setAttr ".wl[741].w[1]" 0.0055045235047817657;
	setAttr ".wl[741].w[2]" 0.0024967591932792514;
	setAttr ".wl[741].w[7]" 0.41970715406621129;
	setAttr ".wl[741].w[8]" 0.57229156323572772;
	setAttr -s 4 ".wl[742].w[7:10]"  0.00042727594744505212 0.23912752759666506 
		0.75973066040722448 0.00071453604866539668;
	setAttr -s 4 ".wl[743].w[7:10]"  2.4512472187213305e-05 0.00023574264849657067 
		0.49986987243965808 0.49986987243965808;
	setAttr -s 4 ".wl[744].w";
	setAttr ".wl[744].w[0]" 0.21340107160471095;
	setAttr ".wl[744].w[1]" 0.29208309635597984;
	setAttr ".wl[744].w[7]" 0.27290077931763085;
	setAttr ".wl[744].w[8]" 0.22161505272167845;
	setAttr -s 4 ".wl[745].w";
	setAttr ".wl[745].w[0]" 0.17960061367762681;
	setAttr ".wl[745].w[1]" 0.25285213847791838;
	setAttr ".wl[745].w[7]" 0.28286341564025264;
	setAttr ".wl[745].w[8]" 0.28468383220420218;
	setAttr -s 4 ".wl[746].w";
	setAttr ".wl[746].w[1]" 0.043531270067086798;
	setAttr ".wl[746].w[7]" 0.28681976738418391;
	setAttr ".wl[746].w[8]" 0.62296596810835425;
	setAttr ".wl[746].w[9]" 0.046682994440375006;
	setAttr -s 4 ".wl[747].w";
	setAttr ".wl[747].w[1]" 0.02743034795612246;
	setAttr ".wl[747].w[7]" 0.34317617184301669;
	setAttr ".wl[747].w[8]" 0.61423346155257474;
	setAttr ".wl[747].w[9]" 0.015160018648286071;
	setAttr -s 4 ".wl[748].w[7:10]"  0.0028097829408872217 0.36991401735574131 
		0.62215890756997516 0.0051172921333962774;
	setAttr -s 4 ".wl[749].w[7:10]"  0.00069851796433564471 0.0059797896497787439 
		0.49666084619294282 0.49666084619294282;
	setAttr -s 4 ".wl[750].w[7:10]"  0.0018419208706797444 0.014222446904780804 
		0.49196781611226975 0.49196781611226975;
	setAttr -s 4 ".wl[751].w[7:10]"  0.011865220406596551 0.41981519120358751 
		0.54553708535386114 0.02278250303595479;
	setAttr -s 4 ".wl[752].w[7:10]"  1.4560364555421926e-05 0.00013524719599849319 
		0.4999250962197232 0.49992509621972298;
	setAttr -s 4 ".wl[753].w";
	setAttr ".wl[753].w[1]" 0.011062408265956326;
	setAttr ".wl[753].w[7]" 0.45189266102784409;
	setAttr ".wl[753].w[8]" 0.53353165598901797;
	setAttr ".wl[753].w[9]" 0.0035132747171815119;
	setAttr -s 4 ".wl[754].w";
	setAttr ".wl[754].w[1]" 0.0041679867447298588;
	setAttr ".wl[754].w[7]" 0.42048891934192473;
	setAttr ".wl[754].w[8]" 0.57368799833523576;
	setAttr ".wl[754].w[9]" 0.001655095578109703;
	setAttr -s 4 ".wl[755].w[7:10]"  1.8695671594027759e-05 0.00017775886925105116 
		0.49990177272957748 0.49990177272957748;
	setAttr -s 4 ".wl[756].w[7:10]"  0.0003068893665979887 0.20696739155225355 
		0.79221431533898257 0.00051140374216594308;
	setAttr -s 4 ".wl[757].w";
	setAttr ".wl[757].w[1]" 0.0019679823393028658;
	setAttr ".wl[757].w[7]" 0.2940188646638815;
	setAttr ".wl[757].w[8]" 0.70282414062937337;
	setAttr ".wl[757].w[9]" 0.0011890123674422183;
	setAttr -s 4 ".wl[758].w[7:10]"  0.00010234395636675567 0.00093843146530349418 
		0.49947961228916488 0.49947961228916488;
	setAttr -s 4 ".wl[759].w[7:10]"  3.8060490210414299e-05 0.00036324288640469146 
		0.49979934831169243 0.49979934831169243;
	setAttr -s 4 ".wl[760].w[7:10]"  0.00055166800522438869 0.26547486767260475 
		0.73307743808385495 0.00089602623831600036;
	setAttr -s 4 ".wl[761].w[7:10]"  0.001496897556358473 0.34732000489496095 
		0.64877895299345334 0.0024041445552272513;
	setAttr -s 4 ".wl[762].w[11:14]"  0.00061499910971803454 0.0076742503243899043 
		0.57400903225793387 0.41770171830795816;
	setAttr -s 4 ".wl[763].w[11:14]"  0.00033283267592419631 0.0045540954821385272 
		0.61925704905017931 0.37585602279175806;
	setAttr -s 4 ".wl[764].w[11:14]"  0.00019768110691050411 0.0029265740165668542 
		0.66594216186607613 0.33093358301044645;
	setAttr -s 4 ".wl[765].w[11:14]"  0.0001301569970749161 0.0020250454642624762 
		0.69046365323975079 0.30738114429891195;
	setAttr -s 4 ".wl[766].w[11:14]"  0.00015827480826062298 0.0024804129473304688 
		0.65874370513451119 0.33861760710989774;
	setAttr -s 4 ".wl[767].w[11:14]"  0.0005909237674021603 0.0081199357502153534 
		0.55770722828568198 0.43358191219670045;
	setAttr -s 4 ".wl[768].w[11:14]"  0.0036252669627208503 0.035857560313654349 
		0.49698959793581982 0.46352757478780493;
	setAttr -s 4 ".wl[769].w[11:14]"  0.0011431122857697219 0.013260848968406659 
		0.5337860395881483 0.45180999915767533;
	setAttr -s 4 ".wl[770].w[7:10]"  9.0133329190786473e-05 0.0011543369237500974 
		0.56611851614762965 0.43263701359942963;
	setAttr -s 4 ".wl[771].w[7:10]"  0.00078721387812960824 0.0089240549444712901 
		0.51729343908509084 0.47299529209230828;
	setAttr -s 4 ".wl[772].w[7:10]"  0.0033936014725049857 0.031456811929381011 
		0.49239554032558341 0.4727540462725307;
	setAttr -s 4 ".wl[773].w[7:10]"  0.00090323582769181825 0.010104316106191424 
		0.51669893907083531 0.4722935089952815;
	setAttr -s 4 ".wl[774].w[7:10]"  0.00014805230461384408 0.0018610521054266466 
		0.5561541074108689 0.44183678817909056;
	setAttr -s 4 ".wl[775].w[7:10]"  3.5794991754024095e-05 0.00047190050784212803 
		0.6196897878677472 0.37980251663265668;
	setAttr -s 4 ".wl[776].w[7:10]"  1.2325184122571341e-05 0.00016604205978074864 
		0.69843319324645847 0.30138843950963828;
	setAttr -s 4 ".wl[777].w[7:10]"  1.9889994688035032e-05 0.00026503421393705512 
		0.6499052404669986 0.34980983532437637;
	setAttr -s 4 ".wl[778].w";
	setAttr ".wl[778].w[1]" 0.52270357208111429;
	setAttr ".wl[778].w[2]" 0.46510295608476504;
	setAttr ".wl[778].w[7]" 0.0042043330921114584;
	setAttr ".wl[778].w[11]" 0.0079891387420091345;
	setAttr -s 4 ".wl[779].w";
	setAttr ".wl[779].w[1]" 0.40463369321489878;
	setAttr ".wl[779].w[2]" 0.15897591707207548;
	setAttr ".wl[779].w[7]" 0.23939907824372969;
	setAttr ".wl[779].w[11]" 0.19699131146929619;
	setAttr -s 4 ".wl[780].w";
	setAttr ".wl[780].w[0]" 0.48222996539861845;
	setAttr ".wl[780].w[1]" 0.27630411767810986;
	setAttr ".wl[780].w[15]" 0.12313602473500196;
	setAttr ".wl[780].w[18]" 0.11832989218826964;
	setAttr -s 4 ".wl[781].w";
	setAttr ".wl[781].w[0]" 0.47353410436086363;
	setAttr ".wl[781].w[1]" 0.27005324842124123;
	setAttr ".wl[781].w[15]" 0.13102862584017483;
	setAttr ".wl[781].w[18]" 0.12538402137772028;
	setAttr -s 4 ".wl[782].w";
	setAttr ".wl[782].w[0]" 0.48085332669667458;
	setAttr ".wl[782].w[1]" 0.036234980743537488;
	setAttr ".wl[782].w[15]" 0.24370256581187677;
	setAttr ".wl[782].w[18]" 0.23920912674791123;
	setAttr -s 4 ".wl[783].w";
	setAttr ".wl[783].w[0]" 0.50532708070087118;
	setAttr ".wl[783].w[1]" 0.014409471618125698;
	setAttr ".wl[783].w[15]" 0.082538507214067275;
	setAttr ".wl[783].w[18]" 0.39772494046693591;
	setAttr -s 4 ".wl[784].w";
	setAttr ".wl[784].w[0]" 0.44952444146438425;
	setAttr ".wl[784].w[1]" 0.032136334119771563;
	setAttr ".wl[784].w[15]" 0.26239940502077114;
	setAttr ".wl[784].w[18]" 0.25593981939507304;
	setAttr -s 4 ".wl[785].w";
	setAttr ".wl[785].w[0]" 0.48858114972762201;
	setAttr ".wl[785].w[1]" 0.012374067551257667;
	setAttr ".wl[785].w[15]" 0.078433397651460859;
	setAttr ".wl[785].w[18]" 0.42061138506965939;
	setAttr -s 4 ".wl[786].w";
	setAttr ".wl[786].w[0]" 0.49463081048482083;
	setAttr ".wl[786].w[15]" 0.007884322966056654;
	setAttr ".wl[786].w[18]" 0.49463081048482083;
	setAttr ".wl[786].w[19]" 0.002854056064301622;
	setAttr -s 4 ".wl[787].w";
	setAttr ".wl[787].w[0]" 0.49518251646854777;
	setAttr ".wl[787].w[15]" 0.0070839388051802061;
	setAttr ".wl[787].w[18]" 0.49518251646854766;
	setAttr ".wl[787].w[19]" 0.0025510282577244749;
	setAttr -s 4 ".wl[788].w";
	setAttr ".wl[788].w[0]" 0.49639376665672225;
	setAttr ".wl[788].w[15]" 0.0022088330964881008;
	setAttr ".wl[788].w[18]" 0.50042779451528574;
	setAttr ".wl[788].w[19]" 0.00096960573150397232;
	setAttr -s 4 ".wl[789].w";
	setAttr ".wl[789].w[0]" 0.51347356146791245;
	setAttr ".wl[789].w[1]" 0.014047244233435235;
	setAttr ".wl[789].w[15]" 0.23698169410256933;
	setAttr ".wl[789].w[18]" 0.23549750019608301;
	setAttr -s 4 ".wl[790].w";
	setAttr ".wl[790].w[0]" 0.48928487306199869;
	setAttr ".wl[790].w[15]" 0.017515699110346644;
	setAttr ".wl[790].w[18]" 0.48928487306199858;
	setAttr ".wl[790].w[19]" 0.003914554765656165;
	setAttr -s 4 ".wl[791].w";
	setAttr ".wl[791].w[0]" 0.47393953544700218;
	setAttr ".wl[791].w[15]" 0.081233930485491032;
	setAttr ".wl[791].w[18]" 0.43346217641320933;
	setAttr ".wl[791].w[19]" 0.011364357654297517;
	setAttr -s 4 ".wl[792].w";
	setAttr ".wl[792].w[0]" 0.43922660754590565;
	setAttr ".wl[792].w[15]" 0.0040889102523760821;
	setAttr ".wl[792].w[18]" 0.55441277723747584;
	setAttr ".wl[792].w[19]" 0.0022717049642424982;
	setAttr -s 4 ".wl[793].w";
	setAttr ".wl[793].w[0]" 0.48920031695507871;
	setAttr ".wl[793].w[15]" 0.017657060855672705;
	setAttr ".wl[793].w[18]" 0.48920031695507871;
	setAttr ".wl[793].w[19]" 0.0039423052341699214;
	setAttr -s 4 ".wl[794].w";
	setAttr ".wl[794].w[0]" 0.44262926661615642;
	setAttr ".wl[794].w[15]" 0.0046340892244232214;
	setAttr ".wl[794].w[18]" 0.55015573489211689;
	setAttr ".wl[794].w[19]" 0.0025809092673034894;
	setAttr -s 4 ".wl[795].w";
	setAttr ".wl[795].w[0]" 0.48819932175030673;
	setAttr ".wl[795].w[1]" 0.011546819418574139;
	setAttr ".wl[795].w[15]" 0.25166389554781665;
	setAttr ".wl[795].w[18]" 0.24858996328330252;
	setAttr -s 4 ".wl[796].w";
	setAttr ".wl[796].w[0]" 0.4663955348810388;
	setAttr ".wl[796].w[15]" 0.074202633238234894;
	setAttr ".wl[796].w[18]" 0.44964870545422664;
	setAttr ".wl[796].w[19]" 0.0097531264264995206;
	setAttr -s 4 ".wl[797].w";
	setAttr ".wl[797].w[0]" 0.24948165353651428;
	setAttr ".wl[797].w[15]" 0.0033484013175224215;
	setAttr ".wl[797].w[18]" 0.74249207856379418;
	setAttr ".wl[797].w[19]" 0.0046778665821691351;
	setAttr -s 4 ".wl[798].w";
	setAttr ".wl[798].w[0]" 0.39382536670348189;
	setAttr ".wl[798].w[15]" 0.28812995969735178;
	setAttr ".wl[798].w[16]" 0.024339078838220344;
	setAttr ".wl[798].w[18]" 0.29370559476094599;
	setAttr -s 4 ".wl[799].w";
	setAttr ".wl[799].w[0]" 0.40487212864386701;
	setAttr ".wl[799].w[15]" 0.021383880093636207;
	setAttr ".wl[799].w[18]" 0.56049404238810219;
	setAttr ".wl[799].w[19]" 0.013249948874394634;
	setAttr -s 4 ".wl[800].w";
	setAttr ".wl[800].w[0]" 0.33185028330992777;
	setAttr ".wl[800].w[15]" 0.098955121193484291;
	setAttr ".wl[800].w[18]" 0.50171927050001019;
	setAttr ".wl[800].w[19]" 0.06747532499657774;
	setAttr -s 4 ".wl[801].w";
	setAttr ".wl[801].w[0]" 0.148207157828706;
	setAttr ".wl[801].w[15]" 0.0080618982592129181;
	setAttr ".wl[801].w[18]" 0.8087404974761;
	setAttr ".wl[801].w[19]" 0.034990446435981114;
	setAttr -s 4 ".wl[802].w";
	setAttr ".wl[802].w[0]" 0.40728345713476355;
	setAttr ".wl[802].w[15]" 0.02305271804819924;
	setAttr ".wl[802].w[18]" 0.55534084252620897;
	setAttr ".wl[802].w[19]" 0.014322982290828288;
	setAttr -s 4 ".wl[803].w";
	setAttr ".wl[803].w[0]" 0.16775510633249119;
	setAttr ".wl[803].w[15]" 0.010072686189994552;
	setAttr ".wl[803].w[18]" 0.77989657401244028;
	setAttr ".wl[803].w[19]" 0.042275633465074021;
	setAttr -s 4 ".wl[804].w";
	setAttr ".wl[804].w[0]" 0.37553735120936116;
	setAttr ".wl[804].w[15]" 0.29901100036050021;
	setAttr ".wl[804].w[16]" 0.021332626670864115;
	setAttr ".wl[804].w[18]" 0.30411902175927452;
	setAttr -s 4 ".wl[805].w";
	setAttr ".wl[805].w[0]" 0.33080934269828166;
	setAttr ".wl[805].w[15]" 0.095452927188451511;
	setAttr ".wl[805].w[18]" 0.50928182568112013;
	setAttr ".wl[805].w[19]" 0.064455904432146646;
	setAttr -s 4 ".wl[806].w";
	setAttr ".wl[806].w[0]" 0.031646502987077156;
	setAttr ".wl[806].w[15]" 0.0048594254787559209;
	setAttr ".wl[806].w[18]" 0.68128767387775446;
	setAttr ".wl[806].w[19]" 0.28220639765641242;
	setAttr -s 4 ".wl[807].w";
	setAttr ".wl[807].w[0]" 0.18825634762905169;
	setAttr ".wl[807].w[15]" 0.3160663125256214;
	setAttr ".wl[807].w[16]" 0.17545572228851053;
	setAttr ".wl[807].w[18]" 0.32022161755681638;
	setAttr -s 4 ".wl[808].w";
	setAttr ".wl[808].w[0]" 0.094073767810738423;
	setAttr ".wl[808].w[15]" 0.024417382573761835;
	setAttr ".wl[808].w[18]" 0.59650178914115204;
	setAttr ".wl[808].w[19]" 0.28500706047434782;
	setAttr -s 4 ".wl[809].w";
	setAttr ".wl[809].w[15]" 0.081725977343202263;
	setAttr ".wl[809].w[16]" 0.077215535184404868;
	setAttr ".wl[809].w[18]" 0.41909620675819698;
	setAttr ".wl[809].w[19]" 0.42196228071419595;
	setAttr -s 4 ".wl[810].w";
	setAttr ".wl[810].w[0]" 0.013297320290398023;
	setAttr ".wl[810].w[18]" 0.4775214303809554;
	setAttr ".wl[810].w[19]" 0.49622150124370906;
	setAttr ".wl[810].w[20]" 0.012959748084937455;
	setAttr -s 4 ".wl[811].w";
	setAttr ".wl[811].w[0]" 0.10709477531666257;
	setAttr ".wl[811].w[15]" 0.029610956628253589;
	setAttr ".wl[811].w[18]" 0.56681777063097349;
	setAttr ".wl[811].w[19]" 0.29647649742411031;
	setAttr -s 4 ".wl[812].w";
	setAttr ".wl[812].w[0]" 0.019077656315795947;
	setAttr ".wl[812].w[18]" 0.47625321753817185;
	setAttr ".wl[812].w[19]" 0.48666272696815477;
	setAttr ".wl[812].w[20]" 0.018006399177877455;
	setAttr -s 4 ".wl[813].w";
	setAttr ".wl[813].w[0]" 0.18457703007182377;
	setAttr ".wl[813].w[15]" 0.32004328911929336;
	setAttr ".wl[813].w[16]" 0.17187386877596858;
	setAttr ".wl[813].w[18]" 0.32350581203291423;
	setAttr -s 4 ".wl[814].w";
	setAttr ".wl[814].w[15]" 0.086166759491005787;
	setAttr ".wl[814].w[16]" 0.081515948068716704;
	setAttr ".wl[814].w[18]" 0.41586413000022676;
	setAttr ".wl[814].w[19]" 0.41645316244005071;
	setAttr -s 4 ".wl[815].w";
	setAttr ".wl[815].w[0]" 0.0098792864386499282;
	setAttr ".wl[815].w[18]" 0.17185247336530246;
	setAttr ".wl[815].w[19]" 0.64284778109749929;
	setAttr ".wl[815].w[20]" 0.17542045909854825;
	setAttr -s 4 ".wl[816].w";
	setAttr ".wl[816].w[16]" 4.2828975824290686e-05;
	setAttr ".wl[816].w[18]" 6.1939272261965635e-05;
	setAttr ".wl[816].w[19]" 0.49994761587595687;
	setAttr ".wl[816].w[20]" 0.49994761587595687;
	setAttr -s 4 ".wl[817].w[16:19]"  0.29105020674101073 0.20956384095253547 
		0.20087606395129404 0.29850988835515979;
	setAttr -s 4 ".wl[818].w";
	setAttr ".wl[818].w[16]" 0.23719434963472519;
	setAttr ".wl[818].w[17]" 0.23719434963472519;
	setAttr ".wl[818].w[19]" 0.26282578384992539;
	setAttr ".wl[818].w[20]" 0.26278551688062435;
	setAttr -s 4 ".wl[819].w";
	setAttr ".wl[819].w[16]" 0.027986738527147816;
	setAttr ".wl[819].w[18]" 0.21660983016210952;
	setAttr ".wl[819].w[19]" 0.5410389634403252;
	setAttr ".wl[819].w[20]" 0.21436446787041746;
	setAttr -s 4 ".wl[820].w";
	setAttr ".wl[820].w[16]" 0.066260872860594885;
	setAttr ".wl[820].w[17]" 0.066260872860594885;
	setAttr ".wl[820].w[19]" 0.43373912713940527;
	setAttr ".wl[820].w[20]" 0.43373912713940505;
	setAttr -s 4 ".wl[821].w";
	setAttr ".wl[821].w[16]" 0.00909876872840212;
	setAttr ".wl[821].w[18]" 0.025529746128480545;
	setAttr ".wl[821].w[19]" 0.48268574257155872;
	setAttr ".wl[821].w[20]" 0.48268574257155861;
	setAttr -s 4 ".wl[822].w";
	setAttr ".wl[822].w[16]" 0.03953934665145567;
	setAttr ".wl[822].w[18]" 0.24896392698203448;
	setAttr ".wl[822].w[19]" 0.48834287836571227;
	setAttr ".wl[822].w[20]" 0.22315384800079757;
	setAttr -s 4 ".wl[823].w";
	setAttr ".wl[823].w[16]" 0.017459900684648203;
	setAttr ".wl[823].w[18]" 0.045757332591745428;
	setAttr ".wl[823].w[19]" 0.46839138336180314;
	setAttr ".wl[823].w[20]" 0.46839138336180314;
	setAttr -s 4 ".wl[824].w[16:19]"  0.2932055317636158 0.21478856276006195 
		0.20594111758774367 0.28606478788857853;
	setAttr -s 4 ".wl[825].w";
	setAttr ".wl[825].w[16]" 0.09526063427962092;
	setAttr ".wl[825].w[17]" 0.09526063427962092;
	setAttr ".wl[825].w[19]" 0.4047847589703209;
	setAttr ".wl[825].w[20]" 0.40469397247043726;
	setAttr -s 4 ".wl[826].w";
	setAttr ".wl[826].w[16]" 0.2530516353187815;
	setAttr ".wl[826].w[17]" 0.25305163531878139;
	setAttr ".wl[826].w[19]" 0.24713891508002586;
	setAttr ".wl[826].w[20]" 0.24675781428241128;
	setAttr -s 4 ".wl[827].w";
	setAttr ".wl[827].w[16]" 0.25469451313785801;
	setAttr ".wl[827].w[17]" 0.2546945131378579;
	setAttr ".wl[827].w[19]" 0.24576768723056289;
	setAttr ".wl[827].w[20]" 0.24484328649372125;
	setAttr -s 4 ".wl[828].w";
	setAttr ".wl[828].w[16]" 0.049207998923857424;
	setAttr ".wl[828].w[18]" 0.0952673081961872;
	setAttr ".wl[828].w[19]" 0.42780113435930744;
	setAttr ".wl[828].w[20]" 0.42772355852064792;
	setAttr -s 4 ".wl[829].w";
	setAttr ".wl[829].w[16]" 0.11424132865313087;
	setAttr ".wl[829].w[18]" 0.13871208947644631;
	setAttr ".wl[829].w[19]" 0.38376827007626785;
	setAttr ".wl[829].w[20]" 0.36327831179415504;
	setAttr -s 4 ".wl[830].w";
	setAttr ".wl[830].w[16]" 0.010049901413229806;
	setAttr ".wl[830].w[18]" 0.038935063019777776;
	setAttr ".wl[830].w[19]" 0.47550751778349637;
	setAttr ".wl[830].w[20]" 0.47550751778349615;
	setAttr -s 4 ".wl[831].w";
	setAttr ".wl[831].w[16]" 0.017826260532690354;
	setAttr ".wl[831].w[18]" 0.077779575716219143;
	setAttr ".wl[831].w[19]" 0.46704873506071587;
	setAttr ".wl[831].w[20]" 0.43734542869037457;
	setAttr -s 4 ".wl[832].w";
	setAttr ".wl[832].w[16]" 0.030813195818712943;
	setAttr ".wl[832].w[18]" 0.063965718576688688;
	setAttr ".wl[832].w[19]" 0.45261054280229918;
	setAttr ".wl[832].w[20]" 0.45261054280229918;
	setAttr -s 4 ".wl[833].w";
	setAttr ".wl[833].w[16]" 0.011274510248159295;
	setAttr ".wl[833].w[18]" 0.054028167129762911;
	setAttr ".wl[833].w[19]" 0.48391328099614045;
	setAttr ".wl[833].w[20]" 0.45078404162593733;
	setAttr -s 4 ".wl[834].w";
	setAttr ".wl[834].w[16]" 0.24281318799064422;
	setAttr ".wl[834].w[17]" 0.24281318799064422;
	setAttr ".wl[834].w[19]" 0.25727656302675467;
	setAttr ".wl[834].w[20]" 0.25709706099195695;
	setAttr -s 4 ".wl[835].w";
	setAttr ".wl[835].w[16]" 0.090140571906015057;
	setAttr ".wl[835].w[18]" 0.11193289484658774;
	setAttr ".wl[835].w[19]" 0.4085456651055791;
	setAttr ".wl[835].w[20]" 0.38938086814181816;
	setAttr -s 4 ".wl[836].w";
	setAttr ".wl[836].w[1]" 0.41339783152670051;
	setAttr ".wl[836].w[2]" 0.38864122816697944;
	setAttr ".wl[836].w[7]" 0.077004239916533893;
	setAttr ".wl[836].w[11]" 0.12095670038978613;
	setAttr -s 4 ".wl[837].w";
	setAttr ".wl[837].w[0]" 0.47353431248219086;
	setAttr ".wl[837].w[1]" 0.015537940595803186;
	setAttr ".wl[837].w[15]" 0.037393434439815146;
	setAttr ".wl[837].w[18]" 0.47353431248219086;
	setAttr -s 4 ".wl[838].w";
	setAttr ".wl[838].w[0]" 0.41569435015080597;
	setAttr ".wl[838].w[1]" 0.069499745506040364;
	setAttr ".wl[838].w[15]" 0.12197368464336479;
	setAttr ".wl[838].w[18]" 0.39283221969978893;
	setAttr -s 4 ".wl[839].w";
	setAttr ".wl[839].w[0]" 0.42780498115797383;
	setAttr ".wl[839].w[1]" 0.073853332638785282;
	setAttr ".wl[839].w[15]" 0.11977302608861155;
	setAttr ".wl[839].w[18]" 0.37856866011462947;
	setAttr -s 4 ".wl[840].w";
	setAttr ".wl[840].w[0]" 0.48989953606392839;
	setAttr ".wl[840].w[15]" 0.012374416970520016;
	setAttr ".wl[840].w[18]" 0.48989953606392839;
	setAttr ".wl[840].w[19]" 0.0078265109016231616;
	setAttr -s 4 ".wl[841].w";
	setAttr ".wl[841].w[0]" 0.46412954581512605;
	setAttr ".wl[841].w[1]" 0.035862013080011079;
	setAttr ".wl[841].w[15]" 0.035878895289736895;
	setAttr ".wl[841].w[18]" 0.46412954581512605;
	setAttr -s 4 ".wl[842].w";
	setAttr ".wl[842].w[0]" 0.47568959960229146;
	setAttr ".wl[842].w[1]" 0.014575129677800789;
	setAttr ".wl[842].w[15]" 0.034045671117616277;
	setAttr ".wl[842].w[18]" 0.47568959960229146;
	setAttr -s 4 ".wl[843].w";
	setAttr ".wl[843].w[0]" 0.4670233398108008;
	setAttr ".wl[843].w[1]" 0.033435963423208585;
	setAttr ".wl[843].w[15]" 0.032517356955190035;
	setAttr ".wl[843].w[18]" 0.46702333981080069;
	setAttr -s 4 ".wl[844].w";
	setAttr ".wl[844].w[1]" 0.32605299683832334;
	setAttr ".wl[844].w[2]" 0.25248830463303557;
	setAttr ".wl[844].w[7]" 0.24322813116742117;
	setAttr ".wl[844].w[11]" 0.17823056736122;
	setAttr -s 4 ".wl[845].w";
	setAttr ".wl[845].w[1]" 0.3332294419263091;
	setAttr ".wl[845].w[2]" 0.23854242151370902;
	setAttr ".wl[845].w[7]" 0.094998694633672984;
	setAttr ".wl[845].w[11]" 0.33322944192630899;
	setAttr -s 4 ".wl[846].w";
	setAttr ".wl[846].w[1]" 0.45873802244862105;
	setAttr ".wl[846].w[2]" 0.3770113374976663;
	setAttr ".wl[846].w[7]" 0.081655659415727444;
	setAttr ".wl[846].w[11]" 0.082594980637985219;
	setAttr -s 4 ".wl[847].w";
	setAttr ".wl[847].w[0]" 0.21926826610676348;
	setAttr ".wl[847].w[1]" 0.35021371939844287;
	setAttr ".wl[847].w[7]" 0.29804369038535261;
	setAttr ".wl[847].w[11]" 0.13247432410944104;
	setAttr -s 4 ".wl[848].w";
	setAttr ".wl[848].w[1]" 0.36296898292223079;
	setAttr ".wl[848].w[2]" 0.14699127820159438;
	setAttr ".wl[848].w[7]" 0.15688707486525785;
	setAttr ".wl[848].w[11]" 0.33315266401091687;
	setAttr -s 4 ".wl[849].w";
	setAttr ".wl[849].w[1]" 0.39819598626384362;
	setAttr ".wl[849].w[2]" 0.10287005335363417;
	setAttr ".wl[849].w[11]" 0.4057723072310212;
	setAttr ".wl[849].w[12]" 0.093161653151501031;
	setAttr -s 4 ".wl[850].w";
	setAttr ".wl[850].w[1]" 0.43431862806743465;
	setAttr ".wl[850].w[2]" 0.1242111843975077;
	setAttr ".wl[850].w[11]" 0.43431862806743465;
	setAttr ".wl[850].w[12]" 0.0071515594676230341;
	setAttr -s 4 ".wl[851].w";
	setAttr ".wl[851].w[1]" 0.39377968588223211;
	setAttr ".wl[851].w[2]" 0.050814566097095143;
	setAttr ".wl[851].w[11]" 0.49374369738105001;
	setAttr ".wl[851].w[12]" 0.061662050639622844;
	setAttr -s 4 ".wl[852].w";
	setAttr ".wl[852].w[1]" 0.34678567393099458;
	setAttr ".wl[852].w[2]" 0.028259015697897689;
	setAttr ".wl[852].w[11]" 0.59133899697097447;
	setAttr ".wl[852].w[12]" 0.033616313400133269;
	setAttr -s 4 ".wl[853].w";
	setAttr ".wl[853].w[1]" 0.34290546995993076;
	setAttr ".wl[853].w[2]" 0.3365742445924027;
	setAttr ".wl[853].w[7]" 0.03045492836851213;
	setAttr ".wl[853].w[11]" 0.29006535707915437;
	setAttr -s 4 ".wl[854].w";
	setAttr ".wl[854].w[1]" 0.21834489914585004;
	setAttr ".wl[854].w[2]" 0.013178083411724377;
	setAttr ".wl[854].w[11]" 0.71837387821879595;
	setAttr ".wl[854].w[12]" 0.050103139223629613;
	setAttr -s 4 ".wl[855].w";
	setAttr ".wl[855].w[1]" 0.00052163663825581606;
	setAttr ".wl[855].w[11]" 0.090522067630648545;
	setAttr ".wl[855].w[12]" 0.90709934941908865;
	setAttr ".wl[855].w[13]" 0.0018569463120069202;
	setAttr -s 4 ".wl[856].w[11:14]"  0.0001895883114730034 0.0018870805830795802 
		0.49896166555272375 0.49896166555272375;
	setAttr -s 4 ".wl[857].w";
	setAttr ".wl[857].w[0]" 0.38498643966265833;
	setAttr ".wl[857].w[1]" 0.24779384512619967;
	setAttr ".wl[857].w[11]" 0.092099860477328502;
	setAttr ".wl[857].w[18]" 0.27511985473381351;
	setAttr -s 4 ".wl[858].w[11:14]"  0.00032583324044746536 0.0028281724873903081 
		0.49842299713608113 0.49842299713608113;
	setAttr -s 4 ".wl[859].w[11:14]"  0.007334016374211336 0.54281623189179551 
		0.44707000837043948 0.0027797433635536354;
	setAttr -s 4 ".wl[860].w";
	setAttr ".wl[860].w[1]" 0.37984103619484239;
	setAttr ".wl[860].w[2]" 0.17755965808771371;
	setAttr ".wl[860].w[7]" 0.062758269522601495;
	setAttr ".wl[860].w[11]" 0.37984103619484239;
	setAttr -s 4 ".wl[861].w";
	setAttr ".wl[861].w[0]" 0.30960467821743476;
	setAttr ".wl[861].w[1]" 0.39651198909175778;
	setAttr ".wl[861].w[7]" 0.11492904715723989;
	setAttr ".wl[861].w[11]" 0.17895428553356757;
	setAttr -s 4 ".wl[862].w";
	setAttr ".wl[862].w[1]" 0.34039158445643913;
	setAttr ".wl[862].w[2]" 0.050710227453509615;
	setAttr ".wl[862].w[11]" 0.41817342732009166;
	setAttr ".wl[862].w[12]" 0.19072476076995956;
	setAttr -s 4 ".wl[863].w";
	setAttr ".wl[863].w[0]" 0.032767556341497833;
	setAttr ".wl[863].w[1]" 0.20243046149467339;
	setAttr ".wl[863].w[11]" 0.45713667536998481;
	setAttr ".wl[863].w[12]" 0.30766530679384396;
	setAttr -s 4 ".wl[864].w";
	setAttr ".wl[864].w[1]" 0.018197093721321826;
	setAttr ".wl[864].w[2]" 0.001154754426946313;
	setAttr ".wl[864].w[11]" 0.76251735989550007;
	setAttr ".wl[864].w[12]" 0.21813079195623178;
	setAttr -s 4 ".wl[865].w";
	setAttr ".wl[865].w[1]" 0.0011292857982476763;
	setAttr ".wl[865].w[11]" 0.18008572474628734;
	setAttr ".wl[865].w[12]" 0.81540282745222603;
	setAttr ".wl[865].w[13]" 0.0033821620032389223;
	setAttr -s 4 ".wl[866].w[11:14]"  0.008645619986451197 0.56398045456347645 
		0.42458710810977845 0.0027868173402937958;
	setAttr -s 4 ".wl[867].w";
	setAttr ".wl[867].w[1]" 0.40343031238006916;
	setAttr ".wl[867].w[2]" 0.082981615803476641;
	setAttr ".wl[867].w[11]" 0.44999242765488312;
	setAttr ".wl[867].w[12]" 0.063595644161571138;
	setAttr -s 4 ".wl[868].w";
	setAttr ".wl[868].w[1]" 0.23415769613785709;
	setAttr ".wl[868].w[2]" 0.01396057243001028;
	setAttr ".wl[868].w[11]" 0.667590914918477;
	setAttr ".wl[868].w[12]" 0.084290816513655706;
	setAttr -s 4 ".wl[869].w";
	setAttr ".wl[869].w[1]" 0.017677225400076251;
	setAttr ".wl[869].w[2]" 0.0011764131171095769;
	setAttr ".wl[869].w[11]" 0.88268961887724195;
	setAttr ".wl[869].w[12]" 0.098456742605572273;
	setAttr -s 4 ".wl[870].w[11:14]"  0.0026452109983800373 0.079817394029635322 
		0.8540654829443417 0.063471912027642935;
	setAttr -s 4 ".wl[871].w[11:14]"  0.00027442178580010202 0.0024370373004555376 
		0.49864427045687221 0.49864427045687221;
	setAttr -s 4 ".wl[872].w";
	setAttr ".wl[872].w[1]" 0.0031748008618367224;
	setAttr ".wl[872].w[11]" 0.15494160081147509;
	setAttr ".wl[872].w[12]" 0.82231502061271966;
	setAttr ".wl[872].w[13]" 0.019568577713968433;
	setAttr -s 4 ".wl[873].w[11:14]"  0.00089062505633074641 0.00780764805472281 
		0.49565086344447329 0.49565086344447329;
	setAttr -s 4 ".wl[874].w";
	setAttr ".wl[874].w[0]" 0.37619934441886321;
	setAttr ".wl[874].w[1]" 0.3890418713647712;
	setAttr ".wl[874].w[7]" 0.11866347929697797;
	setAttr ".wl[874].w[11]" 0.1160953049193876;
	setAttr -s 4 ".wl[875].w";
	setAttr ".wl[875].w[0]" 0.41340838086822035;
	setAttr ".wl[875].w[1]" 0.25462385952810757;
	setAttr ".wl[875].w[15]" 0.070201964772815706;
	setAttr ".wl[875].w[18]" 0.26176579483085638;
	setAttr -s 4 ".wl[876].w";
	setAttr ".wl[876].w[0]" 0.35304953924830301;
	setAttr ".wl[876].w[1]" 0.22030064878192626;
	setAttr ".wl[876].w[11]" 0.098371157416281885;
	setAttr ".wl[876].w[18]" 0.32827865455348876;
	setAttr -s 4 ".wl[877].w";
	setAttr ".wl[877].w[0]" 0.23071656396972048;
	setAttr ".wl[877].w[1]" 0.31859067563563492;
	setAttr ".wl[877].w[11]" 0.23513058833077188;
	setAttr ".wl[877].w[12]" 0.21556217206387271;
	setAttr -s 4 ".wl[878].w";
	setAttr ".wl[878].w[0]" 0.10954981932876588;
	setAttr ".wl[878].w[1]" 0.38495749974033899;
	setAttr ".wl[878].w[11]" 0.33494793628978176;
	setAttr ".wl[878].w[12]" 0.17054474464111335;
	setAttr -s 4 ".wl[879].w[11:14]"  0.00098705288203956528 0.0084145362056653565 
		0.49529920545614753 0.49529920545614753;
	setAttr -s 4 ".wl[880].w";
	setAttr ".wl[880].w[1]" 0.046248659845572845;
	setAttr ".wl[880].w[11]" 0.48965947279709232;
	setAttr ".wl[880].w[12]" 0.45148617794529466;
	setAttr ".wl[880].w[13]" 0.012605689412040299;
	setAttr -s 4 ".wl[881].w";
	setAttr ".wl[881].w[0]" 0.047201777405224001;
	setAttr ".wl[881].w[1]" 0.18233808091637738;
	setAttr ".wl[881].w[11]" 0.41828011552155825;
	setAttr ".wl[881].w[12]" 0.35218002615684035;
	setAttr -s 4 ".wl[882].w";
	setAttr ".wl[882].w[0]" 0.17886855013524663;
	setAttr ".wl[882].w[1]" 0.28507453131089966;
	setAttr ".wl[882].w[11]" 0.27754052302765275;
	setAttr ".wl[882].w[12]" 0.25851639552620104;
	setAttr -s 4 ".wl[883].w";
	setAttr ".wl[883].w[0]" 0.069766126854491115;
	setAttr ".wl[883].w[1]" 0.27757261451649767;
	setAttr ".wl[883].w[11]" 0.38378471889495602;
	setAttr ".wl[883].w[12]" 0.26887653973405512;
	setAttr -s 4 ".wl[884].w[11:14]"  0.01079857439595811 0.49081455672672913 
		0.49081455672672902 0.0075723121505837264;
	setAttr -s 4 ".wl[885].w";
	setAttr ".wl[885].w[1]" 0.066487096416479791;
	setAttr ".wl[885].w[11]" 0.47452320839492762;
	setAttr ".wl[885].w[12]" 0.43762324014240928;
	setAttr ".wl[885].w[13]" 0.021366455046183248;
	setAttr -s 4 ".wl[886].w[11:14]"  0.00014679769919520154 0.0013342068838807734 
		0.49925949770846201 0.49925949770846201;
	setAttr -s 4 ".wl[887].w[11:14]"  9.2236021898318895e-05 0.00083426893026474413 
		0.49953674752391847 0.49953674752391847;
	setAttr -s 4 ".wl[888].w";
	setAttr ".wl[888].w[1]" 0.013268524108685131;
	setAttr ".wl[888].w[11]" 0.19850305061468293;
	setAttr ".wl[888].w[12]" 0.66835443832661434;
	setAttr ".wl[888].w[13]" 0.11987398695001769;
	setAttr -s 4 ".wl[889].w[11:14]"  0.015955769354625275 0.48775373789679971 
		0.48634607411553943 0.009944418633035702;
	setAttr -s 4 ".wl[890].w[11:14]"  0.0090644572762987839 0.16430387076116351 
		0.65254703940580339 0.17408463255673429;
	setAttr -s 4 ".wl[891].w[11:14]"  0.0012714457345994405 0.010145317450821787 
		0.49429161840728952 0.4942916184072893;
	setAttr -s 4 ".wl[892].w";
	setAttr ".wl[892].w[1]" 0.0059170119697151899;
	setAttr ".wl[892].w[11]" 0.14797760168293092;
	setAttr ".wl[892].w[12]" 0.79725962385024596;
	setAttr ".wl[892].w[13]" 0.048845762497108015;
	setAttr -s 4 ".wl[893].w[11:14]"  0.00048559098100088452 0.0048454399006247709 
		0.49733448455918716 0.49733448455918716;
	setAttr -s 4 ".wl[894].w";
	setAttr ".wl[894].w[1]" 0.33159104319320448;
	setAttr ".wl[894].w[2]" 0.034423804434715551;
	setAttr ".wl[894].w[11]" 0.53485683876985735;
	setAttr ".wl[894].w[12]" 0.099128313602222629;
	setAttr -s 4 ".wl[895].w[11:14]"  0.0030777862822016291 0.57027769168722542 
		0.42558075260096334 0.0010637694296096994;
	setAttr -s 4 ".wl[896].w";
	setAttr ".wl[896].w[1]" 0.10177650164513044;
	setAttr ".wl[896].w[2]" 0.0056826295559895632;
	setAttr ".wl[896].w[11]" 0.83248831278290059;
	setAttr ".wl[896].w[12]" 0.060052556015979398;
	setAttr -s 4 ".wl[897].w";
	setAttr ".wl[897].w[1]" 0.034787832455500808;
	setAttr ".wl[897].w[2]" 0.0029071286970621083;
	setAttr ".wl[897].w[11]" 0.74648838539855333;
	setAttr ".wl[897].w[12]" 0.21581665344888376;
	setAttr -s 4 ".wl[898].w";
	setAttr ".wl[898].w[1]" 0.0083562870378676209;
	setAttr ".wl[898].w[2]" 0.00062404065527204807;
	setAttr ".wl[898].w[11]" 0.88989998484341815;
	setAttr ".wl[898].w[12]" 0.10111968746344224;
	setAttr -s 4 ".wl[899].w";
	setAttr ".wl[899].w[1]" 0.33649445101689834;
	setAttr ".wl[899].w[2]" 0.035594783593411811;
	setAttr ".wl[899].w[11]" 0.49188797040048315;
	setAttr ".wl[899].w[12]" 0.13602279498920664;
	setAttr -s 4 ".wl[900].w";
	setAttr ".wl[900].w[1]" 0.093670183784262395;
	setAttr ".wl[900].w[11]" 0.57385578913471991;
	setAttr ".wl[900].w[12]" 0.32309185848188215;
	setAttr ".wl[900].w[13]" 0.0093821685991354944;
	setAttr -s 4 ".wl[901].w[11:14]"  0.00010320780196742407 0.0010244050793781967 
		0.49943619355932722 0.49943619355932722;
	setAttr -s 4 ".wl[902].w[11:14]"  2.7792612597738403e-05 0.00027130823689564611 
		0.49985044957525332 0.49985044957525332;
	setAttr -s 4 ".wl[903].w[11:14]"  0.0016297589287161073 0.51052633647709533 
		0.48712134218776093 0.0007225624064276069;
	setAttr -s 4 ".wl[904].w[11:14]"  0.00070738591328815771 0.033130368098330379 
		0.9458313300449197 0.020330915943461689;
	setAttr -s 4 ".wl[905].w[11:14]"  9.1439973008565161e-05 0.00089608911116277157 
		0.49950623545791439 0.49950623545791439;
	setAttr -s 4 ".wl[906].w";
	setAttr ".wl[906].w[1]" 0.11170801840522064;
	setAttr ".wl[906].w[2]" 0.010101506931990835;
	setAttr ".wl[906].w[11]" 0.63243151291462618;
	setAttr ".wl[906].w[12]" 0.24575896174816239;
	setAttr -s 4 ".wl[907].w";
	setAttr ".wl[907].w[1]" 0.0010172908591323069;
	setAttr ".wl[907].w[11]" 0.09665575311400422;
	setAttr ".wl[907].w[12]" 0.89762743484258189;
	setAttr ".wl[907].w[13]" 0.0046995211842814104;
	setAttr -s 4 ".wl[908].w[11:14]"  0.001463750206813347 0.54815509018573449 
		0.4498165549842682 0.00056460462318403911;
	setAttr -s 4 ".wl[909].w";
	setAttr ".wl[909].w[1]" 0.0072278561037110861;
	setAttr ".wl[909].w[11]" 0.21111333968757509;
	setAttr ".wl[909].w[12]" 0.74869489893743657;
	setAttr ".wl[909].w[13]" 0.032963905271277349;
	setAttr -s 4 ".wl[910].w[11:14]"  0.00077484276343231669 0.49942290966217023 
		0.49938747684297768 0.00041477073141967495;
	setAttr -s 4 ".wl[911].w[11:14]"  8.6040182285166273e-05 0.00080366838999562369 
		0.49955514571385973 0.49955514571385951;
	setAttr -s 4 ".wl[912].w[11:14]"  0.00015140746974268208 0.001413744301194625 
		0.49921742411453135 0.49921742411453135;
	setAttr -s 4 ".wl[913].w[11:14]"  0.00052508054776231796 0.027260318709770043 
		0.95355787899484634 0.018656721747621379;
	setAttr -s 4 ".wl[914].w[11:14]"  0.00010455149651121014 0.0010376197557207499 
		0.49942891437388404 0.49942891437388404;
	setAttr -s 4 ".wl[915].w";
	setAttr ".wl[915].w[1]" 0.0029938301087873917;
	setAttr ".wl[915].w[11]" 0.15744502268778579;
	setAttr ".wl[915].w[12]" 0.82591626597090839;
	setAttr ".wl[915].w[13]" 0.013644881232518499;
	setAttr -s 4 ".wl[916].w[11:14]"  7.3640248007607499e-05 0.00080514060972264789 
		0.49956060957113485 0.49956060957113485;
	setAttr -s 4 ".wl[917].w";
	setAttr ".wl[917].w[15]" 0.00034785521195326071;
	setAttr ".wl[917].w[16]" 0.4996988864470146;
	setAttr ".wl[917].w[17]" 0.4996988864470146;
	setAttr ".wl[917].w[19]" 0.00025437189401756504;
	setAttr -s 4 ".wl[918].w";
	setAttr ".wl[918].w[16]" 0.42410436929643441;
	setAttr ".wl[918].w[17]" 0.42410436929643441;
	setAttr ".wl[918].w[19]" 0.075936488362888799;
	setAttr ".wl[918].w[20]" 0.075854773044242388;
	setAttr -s 4 ".wl[919].w";
	setAttr ".wl[919].w[15]" 0.029999582948657878;
	setAttr ".wl[919].w[16]" 0.4793786114891202;
	setAttr ".wl[919].w[17]" 0.47937861148912042;
	setAttr ".wl[919].w[19]" 0.011243194073101374;
	setAttr -s 4 ".wl[920].w";
	setAttr ".wl[920].w[15]" 0.045250526313308066;
	setAttr ".wl[920].w[16]" 0.46853641987461769;
	setAttr ".wl[920].w[17]" 0.46853641987461769;
	setAttr ".wl[920].w[19]" 0.017676633937456627;
	setAttr -s 4 ".wl[921].w";
	setAttr ".wl[921].w[16]" 0.40905499894890124;
	setAttr ".wl[921].w[17]" 0.40905499894890146;
	setAttr ".wl[921].w[19]" 0.091100097978030592;
	setAttr ".wl[921].w[20]" 0.090789904124166557;
	setAttr -s 4 ".wl[922].w";
	setAttr ".wl[922].w[0]" 0.50103781288957205;
	setAttr ".wl[922].w[1]" 0.014236088761020725;
	setAttr ".wl[922].w[15]" 0.40421442109882816;
	setAttr ".wl[922].w[18]" 0.080511677250579117;
	setAttr -s 4 ".wl[923].w";
	setAttr ".wl[923].w[0]" 0.48941013708978204;
	setAttr ".wl[923].w[15]" 0.48941013708978204;
	setAttr ".wl[923].w[16]" 0.0040686136181470652;
	setAttr ".wl[923].w[18]" 0.017111112202288836;
	setAttr -s 4 ".wl[924].w";
	setAttr ".wl[924].w[0]" 0.46985300887524772;
	setAttr ".wl[924].w[15]" 0.434670963095038;
	setAttr ".wl[924].w[16]" 0.012180190373582804;
	setAttr ".wl[924].w[18]" 0.083295837656131438;
	setAttr -s 4 ".wl[925].w";
	setAttr ".wl[925].w[0]" 0.49549388666063532;
	setAttr ".wl[925].w[15]" 0.49549388666063532;
	setAttr ".wl[925].w[16]" 0.0025113718840241044;
	setAttr ".wl[925].w[18]" 0.006500854794705262;
	setAttr -s 4 ".wl[926].w";
	setAttr ".wl[926].w[0]" 0.48838880103065518;
	setAttr ".wl[926].w[15]" 0.50860379484760876;
	setAttr ".wl[926].w[16]" 0.00096319234111994217;
	setAttr ".wl[926].w[18]" 0.0020442117806161856;
	setAttr -s 4 ".wl[927].w";
	setAttr ".wl[927].w[0]" 0.41534142759022685;
	setAttr ".wl[927].w[15]" 0.57816548339390705;
	setAttr ".wl[927].w[16]" 0.0024168822586870618;
	setAttr ".wl[927].w[18]" 0.004076206757179018;
	setAttr -s 4 ".wl[928].w";
	setAttr ".wl[928].w[0]" 0.49498902032980518;
	setAttr ".wl[928].w[15]" 0.49498902032980518;
	setAttr ".wl[928].w[16]" 0.0028051571600862875;
	setAttr ".wl[928].w[18]" 0.0072168021803032988;
	setAttr -s 4 ".wl[929].w";
	setAttr ".wl[929].w[0]" 0.48943694695592166;
	setAttr ".wl[929].w[15]" 0.48943694695592166;
	setAttr ".wl[929].w[16]" 0.0040636640707415643;
	setAttr ".wl[929].w[18]" 0.017062442017415105;
	setAttr -s 4 ".wl[930].w";
	setAttr ".wl[930].w[0]" 0.41998734220319728;
	setAttr ".wl[930].w[15]" 0.57270464317304948;
	setAttr ".wl[930].w[16]" 0.0027263538070462293;
	setAttr ".wl[930].w[18]" 0.0045816608167070063;
	setAttr -s 4 ".wl[931].w";
	setAttr ".wl[931].w[0]" 0.48482595170160869;
	setAttr ".wl[931].w[1]" 0.012107362197232676;
	setAttr ".wl[931].w[15]" 0.42756192025044487;
	setAttr ".wl[931].w[18]" 0.075504765850713779;
	setAttr -s 4 ".wl[932].w";
	setAttr ".wl[932].w[0]" 0.46282656183228216;
	setAttr ".wl[932].w[15]" 0.45123825123613476;
	setAttr ".wl[932].w[16]" 0.010423062132372611;
	setAttr ".wl[932].w[18]" 0.075512124799210392;
	setAttr -s 4 ".wl[933].w";
	setAttr ".wl[933].w[0]" 0.38841719739823272;
	setAttr ".wl[933].w[15]" 0.57500905258257096;
	setAttr ".wl[933].w[16]" 0.014437779287791651;
	setAttr ".wl[933].w[18]" 0.022135970731404744;
	setAttr -s 4 ".wl[934].w";
	setAttr ".wl[934].w[0]" 0.31986121262130229;
	setAttr ".wl[934].w[15]" 0.50528780454802058;
	setAttr ".wl[934].w[16]" 0.072321329753587058;
	setAttr ".wl[934].w[18]" 0.10252965307709008;
	setAttr -s 4 ".wl[935].w";
	setAttr ".wl[935].w[0]" 0.21839297703626045;
	setAttr ".wl[935].w[15]" 0.77347895029871183;
	setAttr ".wl[935].w[16]" 0.0048755557188528472;
	setAttr ".wl[935].w[18]" 0.0032525169461748169;
	setAttr -s 4 ".wl[936].w";
	setAttr ".wl[936].w[0]" 0.13313805343793392;
	setAttr ".wl[936].w[15]" 0.82146601057458535;
	setAttr ".wl[936].w[16]" 0.037383387794170914;
	setAttr ".wl[936].w[18]" 0.0080125481933097913;
	setAttr -s 4 ".wl[937].w";
	setAttr ".wl[937].w[0]" 0.39125143950842606;
	setAttr ".wl[937].w[15]" 0.56962306584025302;
	setAttr ".wl[937].w[16]" 0.015494841500451308;
	setAttr ".wl[937].w[18]" 0.023630653150869579;
	setAttr -s 4 ".wl[938].w";
	setAttr ".wl[938].w[0]" 0.15191993552064534;
	setAttr ".wl[938].w[15]" 0.79295899447812168;
	setAttr ".wl[938].w[16]" 0.04511641900276292;
	setAttr ".wl[938].w[18]" 0.010004650998470109;
	setAttr -s 4 ".wl[939].w";
	setAttr ".wl[939].w[0]" 0.31860475512355058;
	setAttr ".wl[939].w[15]" 0.51392540665320519;
	setAttr ".wl[939].w[16]" 0.069075631779907609;
	setAttr ".wl[939].w[18]" 0.098394206443336699;
	setAttr -s 4 ".wl[940].w";
	setAttr ".wl[940].w[0]" 0.087374635419791374;
	setAttr ".wl[940].w[15]" 0.59037218272399017;
	setAttr ".wl[940].w[16]" 0.29786535457747298;
	setAttr ".wl[940].w[18]" 0.024387827278745437;
	setAttr -s 4 ".wl[941].w";
	setAttr ".wl[941].w[15]" 0.41789361267320929;
	setAttr ".wl[941].w[16]" 0.42131061059555658;
	setAttr ".wl[941].w[18]" 0.083049382343278078;
	setAttr ".wl[941].w[19]" 0.077746394387956053;
	setAttr -s 4 ".wl[942].w";
	setAttr ".wl[942].w[0]" 0.027923666491751844;
	setAttr ".wl[942].w[15]" 0.67098470282223788;
	setAttr ".wl[942].w[16]" 0.2964741482408168;
	setAttr ".wl[942].w[18]" 0.0046174824451935019;
	setAttr -s 4 ".wl[943].w";
	setAttr ".wl[943].w[0]" 0.012764758833021909;
	setAttr ".wl[943].w[15]" 0.47116411871343467;
	setAttr ".wl[943].w[16]" 0.4959288575588815;
	setAttr ".wl[943].w[17]" 0.020142264894662018;
	setAttr -s 4 ".wl[944].w";
	setAttr ".wl[944].w[0]" 0.099836257334099135;
	setAttr ".wl[944].w[15]" 0.56225560685342324;
	setAttr ".wl[944].w[16]" 0.3084408820551271;
	setAttr ".wl[944].w[18]" 0.029467253757350563;
	setAttr -s 4 ".wl[945].w";
	setAttr ".wl[945].w[0]" 0.018129164095646081;
	setAttr ".wl[945].w[15]" 0.46712826708861466;
	setAttr ".wl[945].w[16]" 0.48669566089953692;
	setAttr ".wl[945].w[17]" 0.028046907916202377;
	setAttr -s 4 ".wl[946].w";
	setAttr ".wl[946].w[15]" 0.41414760728138655;
	setAttr ".wl[946].w[16]" 0.41736245988547005;
	setAttr ".wl[946].w[18]" 0.086962392243554829;
	setAttr ".wl[946].w[19]" 0.081527540589588471;
	setAttr -s 4 ".wl[947].w";
	setAttr ".wl[947].w[15]" 0.20040265270025726;
	setAttr ".wl[947].w[16]" 0.49759255617340559;
	setAttr ".wl[947].w[17]" 0.27481763323695224;
	setAttr ".wl[947].w[19]" 0.027187157889384989;
	setAttr -s 4 ".wl[948].w";
	setAttr ".wl[948].w[15]" 0.11183201006746489;
	setAttr ".wl[948].w[16]" 0.39693043173003256;
	setAttr ".wl[948].w[17]" 0.39599588064037206;
	setAttr ".wl[948].w[19]" 0.095241677562130547;
	setAttr -s 4 ".wl[949].w";
	setAttr ".wl[949].w[0]" 0.0087150481223313928;
	setAttr ".wl[949].w[15]" 0.14883414520249227;
	setAttr ".wl[949].w[16]" 0.5879342885047264;
	setAttr ".wl[949].w[17]" 0.2545165181704499;
	setAttr -s 4 ".wl[950].w";
	setAttr ".wl[950].w[15]" 0.052996942150693609;
	setAttr ".wl[950].w[16]" 0.46772176504646401;
	setAttr ".wl[950].w[17]" 0.46771024375151365;
	setAttr ".wl[950].w[19]" 0.011571049051328828;
	setAttr -s 4 ".wl[951].w";
	setAttr ".wl[951].w[15]" 0.2186982927187717;
	setAttr ".wl[951].w[16]" 0.46060576423508898;
	setAttr ".wl[951].w[17]" 0.28518720114377771;
	setAttr ".wl[951].w[19]" 0.035508741902361585;
	setAttr -s 4 ".wl[952].w";
	setAttr ".wl[952].w[15]" 0.069859523951712896;
	setAttr ".wl[952].w[16]" 0.45686448164940657;
	setAttr ".wl[952].w[17]" 0.45685559428413641;
	setAttr ".wl[952].w[19]" 0.016420400114743997;
	setAttr -s 4 ".wl[953].w";
	setAttr ".wl[953].w[15]" 0.12816058490473548;
	setAttr ".wl[953].w[16]" 0.38361334991617196;
	setAttr ".wl[953].w[17]" 0.38286721479733593;
	setAttr ".wl[953].w[19]" 0.10535885038175653;
	setAttr -s 4 ".wl[954].w";
	setAttr ".wl[954].w[15]" 0.090951818036071677;
	setAttr ".wl[954].w[16]" 0.43083149299112161;
	setAttr ".wl[954].w[17]" 0.43083149299112161;
	setAttr ".wl[954].w[19]" 0.047385195981685092;
	setAttr -s 4 ".wl[955].w";
	setAttr ".wl[955].w[15]" 0.040941635494593773;
	setAttr ".wl[955].w[16]" 0.47406323594608812;
	setAttr ".wl[955].w[17]" 0.47406323594608801;
	setAttr ".wl[955].w[19]" 0.010931892613230158;
	setAttr -s 4 ".wl[956].w";
	setAttr ".wl[956].w[15]" 0.069344133722180706;
	setAttr ".wl[956].w[16]" 0.44775053433964851;
	setAttr ".wl[956].w[17]" 0.44775053433964851;
	setAttr ".wl[956].w[19]" 0.035154797598522192;
	setAttr -s 4 ".wl[957].w";
	setAttr ".wl[957].w[0]" 0.47481671043674928;
	setAttr ".wl[957].w[1]" 0.014912178607878674;
	setAttr ".wl[957].w[15]" 0.47481671043674917;
	setAttr ".wl[957].w[18]" 0.035454400518622933;
	setAttr -s 4 ".wl[958].w";
	setAttr ".wl[958].w[0]" 0.4160115187895902;
	setAttr ".wl[958].w[1]" 0.067809113263075593;
	setAttr ".wl[958].w[15]" 0.3999125234238195;
	setAttr ".wl[958].w[18]" 0.11626684452351474;
	setAttr -s 4 ".wl[959].w";
	setAttr ".wl[959].w[0]" 0.42648527586808599;
	setAttr ".wl[959].w[1]" 0.072510231749040782;
	setAttr ".wl[959].w[15]" 0.3858177557303491;
	setAttr ".wl[959].w[18]" 0.11518673665252405;
	setAttr -s 4 ".wl[960].w";
	setAttr ".wl[960].w[0]" 0.47678147293759288;
	setAttr ".wl[960].w[1]" 0.014025587710343068;
	setAttr ".wl[960].w[15]" 0.47678147293759288;
	setAttr ".wl[960].w[18]" 0.032411466414471311;
	setAttr -s 4 ".wl[961].w";
	setAttr ".wl[961].w[0]" 0.49030296006387608;
	setAttr ".wl[961].w[15]" 0.49030296006387597;
	setAttr ".wl[961].w[16]" 0.0078401297699767451;
	setAttr ".wl[961].w[18]" 0.01155395010227123;
	setAttr -s 4 ".wl[962].w";
	setAttr ".wl[962].w[0]" 0.46620193689282186;
	setAttr ".wl[962].w[1]" 0.034049972651672089;
	setAttr ".wl[962].w[15]" 0.46620193689282186;
	setAttr ".wl[962].w[18]" 0.033546153562684214;
	setAttr -s 4 ".wl[963].w";
	setAttr ".wl[963].w[0]" 0.46897523983344802;
	setAttr ".wl[963].w[1]" 0.031689249078130172;
	setAttr ".wl[963].w[15]" 0.46897523983344802;
	setAttr ".wl[963].w[18]" 0.030360271254973804;
	setAttr -s 4 ".wl[964].w";
	setAttr ".wl[964].w[1]" 0.2727623255151298;
	setAttr ".wl[964].w[2]" 0.2366333461907085;
	setAttr ".wl[964].w[7]" 0.30654007868997668;
	setAttr ".wl[964].w[8]" 0.18406424960418502;
	setAttr -s 4 ".wl[965].w";
	setAttr ".wl[965].w[1]" 0.1815988738740682;
	setAttr ".wl[965].w[2]" 0.078232276659337296;
	setAttr ".wl[965].w[7]" 0.4070734494063068;
	setAttr ".wl[965].w[8]" 0.33309540006028776;
	setAttr -s 4 ".wl[966].w";
	setAttr ".wl[966].w[1]" 0.34273164774568698;
	setAttr ".wl[966].w[2]" 0.10907152003015429;
	setAttr ".wl[966].w[7]" 0.36517441676042944;
	setAttr ".wl[966].w[8]" 0.18302241546372922;
	setAttr -s 4 ".wl[967].w";
	setAttr ".wl[967].w[1]" 0.37304189425720724;
	setAttr ".wl[967].w[2]" 0.33234412160766597;
	setAttr ".wl[967].w[7]" 0.19700928117921007;
	setAttr ".wl[967].w[8]" 0.097604702955916514;
	setAttr -s 4 ".wl[968].w";
	setAttr ".wl[968].w[1]" 0.066895199719189821;
	setAttr ".wl[968].w[2]" 0.044390693730102904;
	setAttr ".wl[968].w[7]" 0.46139397072703053;
	setAttr ".wl[968].w[8]" 0.42732013582367673;
	setAttr -s 4 ".wl[969].w";
	setAttr ".wl[969].w[1]" 0.087210182741824668;
	setAttr ".wl[969].w[2]" 0.084841342412903906;
	setAttr ".wl[969].w[7]" 0.42614022653636252;
	setAttr ".wl[969].w[8]" 0.40180824830890888;
	setAttr -s 4 ".wl[970].w";
	setAttr ".wl[970].w[1]" 0.41506870875298535;
	setAttr ".wl[970].w[2]" 0.40876383936459953;
	setAttr ".wl[970].w[7]" 0.10717444283937827;
	setAttr ".wl[970].w[8]" 0.068993009043036957;
	setAttr -s 4 ".wl[971].w";
	setAttr ".wl[971].w[0]" 0.3709733505113057;
	setAttr ".wl[971].w[1]" 0.23877441772519165;
	setAttr ".wl[971].w[7]" 0.11020137709842148;
	setAttr ".wl[971].w[15]" 0.28005085466508112;
	setAttr -s 4 ".wl[972].w";
	setAttr ".wl[972].w[0]" 0.30455403507786716;
	setAttr ".wl[972].w[1]" 0.34471362148051404;
	setAttr ".wl[972].w[7]" 0.23116827064159703;
	setAttr ".wl[972].w[8]" 0.11956407280002167;
	setAttr -s 4 ".wl[973].w[7:10]"  0.00074412094419201301 0.0066299173816290148 
		0.49631298083708947 0.49631298083708947;
	setAttr -s 4 ".wl[974].w[7:10]"  0.00019845516183595184 0.0017896049248634434 
		0.49900596995665036 0.49900596995665036;
	setAttr -s 4 ".wl[975].w[7:10]"  0.00035556397655443181 0.010638194640912793 
		0.95272570306939575 0.036280538313137031;
	setAttr -s 4 ".wl[976].w[7:10]"  0.0011253817768572727 0.31278708149650469 
		0.68407642943020475 0.0020111072964333005;
	setAttr -s 4 ".wl[977].w";
	setAttr ".wl[977].w[1]" 0.3454431887962538;
	setAttr ".wl[977].w[2]" 0.25178932388691089;
	setAttr ".wl[977].w[7]" 0.26065384575949119;
	setAttr ".wl[977].w[8]" 0.14211364155734421;
	setAttr -s 4 ".wl[978].w";
	setAttr ".wl[978].w[1]" 0.25517496529555211;
	setAttr ".wl[978].w[2]" 0.067349987553235818;
	setAttr ".wl[978].w[7]" 0.37775083961395473;
	setAttr ".wl[978].w[8]" 0.29972420753725737;
	setAttr -s 4 ".wl[979].w";
	setAttr ".wl[979].w[1]" 0.20110933025016417;
	setAttr ".wl[979].w[2]" 0.18720378266712015;
	setAttr ".wl[979].w[7]" 0.32961303511908974;
	setAttr ".wl[979].w[8]" 0.28207385196362594;
	setAttr -s 4 ".wl[980].w";
	setAttr ".wl[980].w[0]" 0.041413474157145677;
	setAttr ".wl[980].w[1]" 0.16860177804223733;
	setAttr ".wl[980].w[7]" 0.40322846679572305;
	setAttr ".wl[980].w[8]" 0.3867562810048939;
	setAttr -s 4 ".wl[981].w";
	setAttr ".wl[981].w[1]" 0.017393682557133534;
	setAttr ".wl[981].w[7]" 0.39144161840979369;
	setAttr ".wl[981].w[8]" 0.58385412618882038;
	setAttr ".wl[981].w[9]" 0.0073105728442523681;
	setAttr -s 4 ".wl[982].w";
	setAttr ".wl[982].w[1]" 0.084085562939018851;
	setAttr ".wl[982].w[2]" 0.033992886023935354;
	setAttr ".wl[982].w[7]" 0.45689266788966226;
	setAttr ".wl[982].w[8]" 0.42502888314738363;
	setAttr -s 4 ".wl[983].w";
	setAttr ".wl[983].w[1]" 0.0020141122057927398;
	setAttr ".wl[983].w[7]" 0.023887202911026344;
	setAttr ".wl[983].w[8]" 0.91492645241100201;
	setAttr ".wl[983].w[9]" 0.059172232472178968;
	setAttr -s 4 ".wl[984].w";
	setAttr ".wl[984].w[1]" 0.0028593109585522817;
	setAttr ".wl[984].w[7]" 0.022970883467296869;
	setAttr ".wl[984].w[8]" 0.85819933016338423;
	setAttr ".wl[984].w[9]" 0.1159704754107666;
	setAttr -s 4 ".wl[985].w";
	setAttr ".wl[985].w[1]" 0.010773192055131758;
	setAttr ".wl[985].w[2]" 0.0047489128680624848;
	setAttr ".wl[985].w[7]" 0.45183654725036271;
	setAttr ".wl[985].w[8]" 0.53264134782644312;
	setAttr -s 4 ".wl[986].w[7:10]"  0.00087550825945416008 0.30364857520451322 
		0.69401970194141283 0.0014562145946196792;
	setAttr -s 4 ".wl[987].w";
	setAttr ".wl[987].w[1]" 0.00068306147233183272;
	setAttr ".wl[987].w[7]" 0.0094322218986218846;
	setAttr ".wl[987].w[8]" 0.96298847458066872;
	setAttr ".wl[987].w[9]" 0.026896242048377657;
	setAttr -s 4 ".wl[988].w";
	setAttr ".wl[988].w[1]" 0.047462380637440293;
	setAttr ".wl[988].w[2]" 0.035992471497053202;
	setAttr ".wl[988].w[7]" 0.46091423509647078;
	setAttr ".wl[988].w[8]" 0.45563091276903561;
	setAttr -s 4 ".wl[989].w[7:10]"  1.7132053953077863e-05 0.00017374121479868848 
		0.49990456336562411 0.49990456336562411;
	setAttr -s 4 ".wl[990].w[7:10]"  4.5339917281818245e-05 0.0004241522715742497 
		0.49976525390557197 0.49976525390557197;
	setAttr -s 4 ".wl[991].w";
	setAttr ".wl[991].w[0]" 0.32950447970779667;
	setAttr ".wl[991].w[1]" 0.35430608167553307;
	setAttr ".wl[991].w[7]" 0.21013877090172967;
	setAttr ".wl[991].w[8]" 0.10605066771494068;
	setAttr -s 4 ".wl[992].w";
	setAttr ".wl[992].w[0]" 0.39425198234005232;
	setAttr ".wl[992].w[1]" 0.24282521510183483;
	setAttr ".wl[992].w[7]" 0.099098120537906717;
	setAttr ".wl[992].w[15]" 0.26382468202020609;
	setAttr -s 4 ".wl[993].w";
	setAttr ".wl[993].w[0]" 0.34043015327287063;
	setAttr ".wl[993].w[1]" 0.21242620155886102;
	setAttr ".wl[993].w[7]" 0.10885412351813342;
	setAttr ".wl[993].w[15]" 0.33828952165013493;
	setAttr -s 4 ".wl[994].w";
	setAttr ".wl[994].w[0]" 0.20050369685359601;
	setAttr ".wl[994].w[1]" 0.2694199548165358;
	setAttr ".wl[994].w[7]" 0.27356883667777471;
	setAttr ".wl[994].w[8]" 0.2565075116520934;
	setAttr -s 4 ".wl[995].w";
	setAttr ".wl[995].w[0]" 0.062048651436664423;
	setAttr ".wl[995].w[1]" 0.26346688097710252;
	setAttr ".wl[995].w[7]" 0.38503794559669752;
	setAttr ".wl[995].w[8]" 0.28944652198953558;
	setAttr -s 4 ".wl[996].w";
	setAttr ".wl[996].w[1]" 0.040255918763036434;
	setAttr ".wl[996].w[7]" 0.3028147548516319;
	setAttr ".wl[996].w[8]" 0.62105824054541781;
	setAttr ".wl[996].w[9]" 0.035871085839913892;
	setAttr -s 4 ".wl[997].w";
	setAttr ".wl[997].w[0]" 0.19101365206027762;
	setAttr ".wl[997].w[1]" 0.26193743471375786;
	setAttr ".wl[997].w[7]" 0.2791541524092459;
	setAttr ".wl[997].w[8]" 0.26789476081671859;
	setAttr -s 4 ".wl[998].w";
	setAttr ".wl[998].w[1]" 0.13640031016687404;
	setAttr ".wl[998].w[7]" 0.3783378112955495;
	setAttr ".wl[998].w[8]" 0.43656424981966757;
	setAttr ".wl[998].w[9]" 0.048697628717908878;
	setAttr -s 4 ".wl[999].w";
	setAttr ".wl[999].w[0]" 0.041211801357143946;
	setAttr ".wl[999].w[1]" 0.16787346789849919;
	setAttr ".wl[999].w[7]" 0.41061079191683325;
	setAttr ".wl[999].w[8]" 0.3803039388275235;
	setAttr -s 4 ".wl[1000].w";
	setAttr ".wl[1000].w[1]" 0.039395351776206976;
	setAttr ".wl[1000].w[7]" 0.30491797723150488;
	setAttr ".wl[1000].w[8]" 0.62200920652784908;
	setAttr ".wl[1000].w[9]" 0.033677464464439116;
	setAttr -s 4 ".wl[1001].w[7:10]"  0.00084484340988878715 0.0074463085306492142 
		0.49585442402973101 0.49585442402973101;
	setAttr -s 4 ".wl[1002].w[7:10]"  0.0013465018193571183 0.010751869212552124 
		0.49395081448404538 0.49395081448404538;
	setAttr -s 4 ".wl[1003].w[7:10]"  0.0068639781507584671 0.10971911450749612 
		0.65708527052254717 0.22633163681919818;
	setAttr -s 4 ".wl[1004].w[7:10]"  0.0076284117009689355 0.40994739376411804 
		0.56796848202902039 0.014455712505892561;
	setAttr -s 4 ".wl[1005].w[7:10]"  2.6680581739969065e-05 0.00024643999985262229 
		0.49986343970920372 0.49986343970920372;
	setAttr -s 4 ".wl[1006].w[7:10]"  0.0012772801494203552 0.010256430138156959 
		0.49423314485621139 0.49423314485621139;
	setAttr -s 4 ".wl[1007].w[7:10]"  0.0002527973823034314 0.0022092115224494027 
		0.49876899554762366 0.49876899554762355;
	setAttr -s 4 ".wl[1008].w";
	setAttr ".wl[1008].w[1]" 0.0099363499077903886;
	setAttr ".wl[1008].w[7]" 0.051728807880952556;
	setAttr ".wl[1008].w[8]" 0.65966326319238466;
	setAttr ".wl[1008].w[9]" 0.27867157901887241;
	setAttr -s 4 ".wl[1009].w[7:10]"  0.0073805641625747887 0.40786897372123398 
		0.57052083574902124 0.014229626367169951;
	setAttr -s 4 ".wl[1010].w";
	setAttr ".wl[1010].w[1]" 0.0028641129583654706;
	setAttr ".wl[1010].w[7]" 0.023452852219724124;
	setAttr ".wl[1010].w[8]" 0.86425338546280728;
	setAttr ".wl[1010].w[9]" 0.10942964935910321;
	setAttr -s 4 ".wl[1011].w";
	setAttr ".wl[1011].w[1]" 0.077653197230659082;
	setAttr ".wl[1011].w[2]" 0.015400304896893674;
	setAttr ".wl[1011].w[7]" 0.47814801149700459;
	setAttr ".wl[1011].w[8]" 0.4287984863754426;
	setAttr -s 4 ".wl[1012].w";
	setAttr ".wl[1012].w[1]" 0.042822564016192478;
	setAttr ".wl[1012].w[2]" 0.017466006253536229;
	setAttr ".wl[1012].w[7]" 0.47492880223932193;
	setAttr ".wl[1012].w[8]" 0.46478262749094928;
	setAttr -s 4 ".wl[1013].w";
	setAttr ".wl[1013].w[1]" 0.0082537586499941851;
	setAttr ".wl[1013].w[7]" 0.45792940705013596;
	setAttr ".wl[1013].w[8]" 0.5310574492513882;
	setAttr ".wl[1013].w[9]" 0.0027593850484815981;
	setAttr -s 4 ".wl[1014].w[7:10]"  1.9909244803786572e-05 0.00018912525213754178 
		0.4998954827515294 0.49989548275152929;
	setAttr -s 4 ".wl[1015].w[7:10]"  5.7322088270200622e-05 0.0019442187020584272 
		0.99126300420265101 0.0067354550070202208;
	setAttr -s 4 ".wl[1016].w[7:10]"  0.00029780483456144576 0.20457708507743283 
		0.79462663247671428 0.00049847761129154201;
	setAttr -s 4 ".wl[1017].w";
	setAttr ".wl[1017].w[1]" 0.0062806537113317068;
	setAttr ".wl[1017].w[2]" 0.0034092702044688349;
	setAttr ".wl[1017].w[7]" 0.4951550380420997;
	setAttr ".wl[1017].w[8]" 0.4951550380420997;
	setAttr -s 4 ".wl[1018].w";
	setAttr ".wl[1018].w[1]" 0.0022952002128717877;
	setAttr ".wl[1018].w[7]" 0.34063310320983037;
	setAttr ".wl[1018].w[8]" 0.65589090622200397;
	setAttr ".wl[1018].w[9]" 0.0011807903552938923;
	setAttr -s 4 ".wl[1019].w";
	setAttr ".wl[1019].w[1]" 0.0027866612190025016;
	setAttr ".wl[1019].w[7]" 0.34625710241459162;
	setAttr ".wl[1019].w[8]" 0.64947194222657889;
	setAttr ".wl[1019].w[9]" 0.0014842941398269377;
	setAttr -s 4 ".wl[1020].w";
	setAttr ".wl[1020].w[1]" 0.00038878342412229942;
	setAttr ".wl[1020].w[7]" 0.0055269262722433855;
	setAttr ".wl[1020].w[8]" 0.97512820345723827;
	setAttr ".wl[1020].w[9]" 0.018956086846396023;
	setAttr -s 4 ".wl[1021].w";
	setAttr ".wl[1021].w[1]" 0.014121863083173885;
	setAttr ".wl[1021].w[7]" 0.39518332590394234;
	setAttr ".wl[1021].w[8]" 0.58539960145451919;
	setAttr ".wl[1021].w[9]" 0.0052952095583645832;
	setAttr -s 4 ".wl[1022].w[7:10]"  6.9671920254608916e-05 0.00064526358921680824 
		0.49964253224526428 0.49964253224526428;
	setAttr -s 4 ".wl[1023].w[7:10]"  2.6651357676251945e-05 0.00025206088948350098 
		0.49986064387642015 0.49986064387642015;
	setAttr -s 4 ".wl[1024].w[7:10]"  1.8612647727734971e-05 0.00017266439499919714 
		0.49990436147863654 0.49990436147863654;
	setAttr -s 4 ".wl[1025].w[7:10]"  3.9149894459379049e-05 0.0003591675503521835 
		0.49980084127759422 0.49980084127759422;
	setAttr -s 4 ".wl[1026].w[7:10]"  2.9556742717090412e-05 0.0002972647767243895 
		0.49983658924027929 0.49983658924027929;
	setAttr -s 4 ".wl[1027].w[7:10]"  0.00053103263458804538 0.015307299862000022 
		0.93620389899022116 0.047957768513190779;
	setAttr -s 4 ".wl[1028].w[7:10]"  0.001128455618183196 0.32801888551791475 
		0.66905476888374871 0.0017978899801534122;
	setAttr -s 4 ".wl[1029].w";
	setAttr ".wl[1029].w[1]" 0.001952215990187228;
	setAttr ".wl[1029].w[7]" 0.02413899734719514;
	setAttr ".wl[1029].w[8]" 0.92152720358528684;
	setAttr ".wl[1029].w[9]" 0.052381583077330665;
	setAttr -s 4 ".wl[1030].w";
	setAttr ".wl[1030].w[1]" 0.00065902026003009356;
	setAttr ".wl[1030].w[7]" 0.0093350260886772736;
	setAttr ".wl[1030].w[8]" 0.96548379477741009;
	setAttr ".wl[1030].w[9]" 0.024522158873882583;
	setAttr -s 4 ".wl[1031].w[7:10]"  0.00025548080752857096 0.002277235893451305 
		0.49873364164951006 0.49873364164951006;
	setAttr -s 4 ".wl[1032].w[7:10]"  0.0014192388369984163 0.33501082571861751 
		0.66115140963378172 0.0024185258106024206;
	setAttr -s 4 ".wl[1033].w[7:10]"  0.00035074360855298821 0.22114497218003376 
		0.77792576824385362 0.00057851596755972726;
	setAttr -s 4 ".wl[1034].w[11:14]"  0.00036937773495984483 0.0034446146768640972 
		0.49809300379408805 0.49809300379408805;
	setAttr -s 4 ".wl[1035].w[11:14]"  0.0013537914796917083 0.051875236919875695 
		0.91102531384003615 0.035745657760396503;
	setAttr -s 4 ".wl[1036].w[11:14]"  0.00054479324018145688 0.0070006688981962637 
		0.58658227946106511 0.4058722584005573;
	setAttr -s 4 ".wl[1037].w[11:14]"  0.00010896442501484147 0.0011481468463930605 
		0.49937144436429604 0.49937144436429604;
	setAttr -s 4 ".wl[1038].w[11:14]"  0.00024293729378512083 0.0034772437663531542 
		0.646653369636498 0.34962644930336378;
	setAttr -s 4 ".wl[1039].w[11:14]"  0.00041350300345926783 0.021871921433353023 
		0.96407901659564543 0.013635558967542314;
	setAttr -s 4 ".wl[1040].w[11:14]"  0.00014731698166600476 0.0022498586448897856 
		0.68658919380879246 0.31101363056465164;
	setAttr -s 4 ".wl[1041].w[11:14]"  9.4215921458171896e-05 0.0010397876717924508 
		0.49943299820337472 0.49943299820337472;
	setAttr -s 4 ".wl[1042].w[11:14]"  0.00017496456484476379 0.0027159355804453338 
		0.66140817416822717 0.33570092568648269;
	setAttr -s 4 ".wl[1043].w[11:14]"  0.0014307015467740361 0.05667048464217362 
		0.89138140682438149 0.050517406986670836;
	setAttr -s 4 ".wl[1044].w[11:14]"  0.00014289596593775202 0.002224935312965355 
		0.64710262022291676 0.35052954849818013;
	setAttr -s 4 ".wl[1045].w[11:14]"  0.0028842328325273748 0.022703250694146342 
		0.48720625823666314 0.48720625823666314;
	setAttr -s 4 ".wl[1046].w[11:14]"  0.0020999301825797398 0.023690081652006759 
		0.51286422231144402 0.46134576585396947;
	setAttr -s 4 ".wl[1047].w[11:14]"  0.0033433062479649887 0.091700354697602307 
		0.81665195377151367 0.088304385282919076;
	setAttr -s 4 ".wl[1048].w[11:14]"  0.0026058785029671608 0.026996297067632409 
		0.50704951615030946 0.46334830827909101;
	setAttr -s 4 ".wl[1049].w[11:14]"  0.00057079988674379827 0.0071115413342999123 
		0.5665700402100915 0.42574761856886484;
	setAttr -s 4 ".wl[1050].w[7:10]"  7.4968281321751543e-05 0.00073878051752645731 
		0.49959312560057606 0.49959312560057584;
	setAttr -s 4 ".wl[1051].w[7:10]"  0.001670378983800374 0.040671184503692441 
		0.84417966017662704 0.11347877633588004;
	setAttr -s 4 ".wl[1052].w[7:10]"  0.0001973392557592866 0.0024577652067269652 
		0.54364075201561113 0.45370414352190258;
	setAttr -s 4 ".wl[1053].w[7:10]"  0.0029770748375760434 0.022769073117323662 
		0.48712692602255014 0.48712692602255014;
	setAttr -s 4 ".wl[1054].w[7:10]"  0.0022219031901338157 0.022158142483425926 
		0.500297411271729 0.47532254305471122;
	setAttr -s 4 ".wl[1055].w[7:10]"  0.0018949922407011817 0.045051405494075908 
		0.83332705258035578 0.11972654968486718;
	setAttr -s 4 ".wl[1056].w[7:10]"  0.0023218398001621982 0.023000862149515494 
		0.50004419897088248 0.47463309907943979;
	setAttr -s 4 ".wl[1057].w[7:10]"  0.00012063427441489549 0.0011714878556041712 
		0.49935393893499047 0.49935393893499047;
	setAttr -s 4 ".wl[1058].w[7:10]"  0.00027625302370329157 0.0033800735206377798 
		0.53974498076801036 0.45659869268764852;
	setAttr -s 4 ".wl[1059].w[7:10]"  0.00014364527807614306 0.0046363226529618539 
		0.9795208638213464 0.015699168247615639;
	setAttr -s 4 ".wl[1060].w[7:10]"  9.0083372089578859e-05 0.0011545208088818294 
		0.57407008071814269 0.42468531510088586;
	setAttr -s 4 ".wl[1061].w[7:10]"  1.054522208697519e-05 0.00010778938962847223 
		0.49994083269414236 0.49994083269414225;
	setAttr -s 4 ".wl[1062].w[7:10]"  1.7303146428550583e-05 0.00023174404383664309 
		0.67085182788323194 0.32889912492650292;
	setAttr -s 4 ".wl[1063].w[7:10]"  9.0258471111664872e-05 0.002978742324497912 
		0.98629767854008499 0.010633320664305466;
	setAttr -s 4 ".wl[1064].w[7:10]"  1.139683484859883e-05 0.00015346966969588961 
		0.69949337197009054 0.30034176152536501;
	setAttr -s 4 ".wl[1065].w[7:10]"  5.2671315778534808e-05 0.00068571140924119918 
		0.58982664203609203 0.40943497523888833;
	setAttr -s 4 ".wl[1066].w";
	setAttr ".wl[1066].w[16]" 0.008724354019428112;
	setAttr ".wl[1066].w[18]" 0.010502958768991953;
	setAttr ".wl[1066].w[19]" 0.49038634360578998;
	setAttr ".wl[1066].w[20]" 0.49038634360578998;
	setAttr -s 4 ".wl[1067].w";
	setAttr ".wl[1067].w[16]" 0.025815130713929678;
	setAttr ".wl[1067].w[18]" 0.030506139764195524;
	setAttr ".wl[1067].w[19]" 0.47183936476093746;
	setAttr ".wl[1067].w[20]" 0.47183936476093746;
	setAttr -s 4 ".wl[1068].w";
	setAttr ".wl[1068].w[0]" 0.50648403424986399;
	setAttr ".wl[1068].w[1]" 0.0091930825619325873;
	setAttr ".wl[1068].w[15]" 0.076350507290591216;
	setAttr ".wl[1068].w[18]" 0.4079723758976122;
	setAttr -s 4 ".wl[1069].w";
	setAttr ".wl[1069].w[0]" 0.4971738482745241;
	setAttr ".wl[1069].w[15]" 0.0040793881751723078;
	setAttr ".wl[1069].w[18]" 0.49725268178210297;
	setAttr ".wl[1069].w[19]" 0.0014940817682005407;
	setAttr -s 4 ".wl[1070].w";
	setAttr ".wl[1070].w[0]" 0.49687898313630707;
	setAttr ".wl[1070].w[15]" 0.0045102642915426476;
	setAttr ".wl[1070].w[18]" 0.49695354198488789;
	setAttr ".wl[1070].w[19]" 0.0016572105872622572;
	setAttr -s 4 ".wl[1071].w";
	setAttr ".wl[1071].w[0]" 0.49186976281973788;
	setAttr ".wl[1071].w[1]" 0.0075832453586100572;
	setAttr ".wl[1071].w[15]" 0.070257090741038947;
	setAttr ".wl[1071].w[18]" 0.4302899010806131;
	setAttr -s 4 ".wl[1072].w";
	setAttr ".wl[1072].w[0]" 0.42714951880682267;
	setAttr ".wl[1072].w[15]" 0.089884220860825814;
	setAttr ".wl[1072].w[18]" 0.46064284722017756;
	setAttr ".wl[1072].w[19]" 0.022323413112173854;
	setAttr -s 4 ".wl[1073].w";
	setAttr ".wl[1073].w[0]" 0.3060745818434753;
	setAttr ".wl[1073].w[15]" 0.005720226586327334;
	setAttr ".wl[1073].w[18]" 0.68178238349684028;
	setAttr ".wl[1073].w[19]" 0.0064228080733569632;
	setAttr -s 4 ".wl[1074].w";
	setAttr ".wl[1074].w[0]" 0.31954877614992427;
	setAttr ".wl[1074].w[15]" 0.006750841476338071;
	setAttr ".wl[1074].w[18]" 0.66613522953918602;
	setAttr ".wl[1074].w[19]" 0.0075651528345516026;
	setAttr -s 4 ".wl[1075].w";
	setAttr ".wl[1075].w[0]" 0.42834899707388269;
	setAttr ".wl[1075].w[15]" 0.083140972341944216;
	setAttr ".wl[1075].w[18]" 0.46867989066478488;
	setAttr ".wl[1075].w[19]" 0.019830139919388198;
	setAttr -s 4 ".wl[1076].w";
	setAttr ".wl[1076].w[0]" 0.17028024038358944;
	setAttr ".wl[1076].w[15]" 0.095452494964398268;
	setAttr ".wl[1076].w[18]" 0.48460391637493361;
	setAttr ".wl[1076].w[19]" 0.24966334827707867;
	setAttr -s 4 ".wl[1077].w";
	setAttr ".wl[1077].w[0]" 0.042272946558816193;
	setAttr ".wl[1077].w[15]" 0.0072172214697459307;
	setAttr ".wl[1077].w[18]" 0.67026634883088465;
	setAttr ".wl[1077].w[19]" 0.28024348314055331;
	setAttr -s 4 ".wl[1078].w";
	setAttr ".wl[1078].w[0]" 0.052826489359154814;
	setAttr ".wl[1078].w[15]" 0.0095714154930066925;
	setAttr ".wl[1078].w[18]" 0.63616669953834559;
	setAttr ".wl[1078].w[19]" 0.30143539560949301;
	setAttr -s 4 ".wl[1079].w";
	setAttr ".wl[1079].w[0]" 0.1699469243058627;
	setAttr ".wl[1079].w[15]" 0.095334982477605759;
	setAttr ".wl[1079].w[18]" 0.48528584245036521;
	setAttr ".wl[1079].w[19]" 0.2494322507661664;
	setAttr -s 4 ".wl[1080].w";
	setAttr ".wl[1080].w[16]" 0.090813654016699802;
	setAttr ".wl[1080].w[18]" 0.24031868194624803;
	setAttr ".wl[1080].w[19]" 0.44446311544177475;
	setAttr ".wl[1080].w[20]" 0.22440454859527753;
	setAttr -s 4 ".wl[1081].w";
	setAttr ".wl[1081].w[0]" 0.011034036496808381;
	setAttr ".wl[1081].w[18]" 0.17660378849509428;
	setAttr ".wl[1081].w[19]" 0.62993145073130863;
	setAttr ".wl[1081].w[20]" 0.1824307242767888;
	setAttr -s 4 ".wl[1082].w";
	setAttr ".wl[1082].w[0]" 0.01590287028391724;
	setAttr ".wl[1082].w[18]" 0.2100561344479808;
	setAttr ".wl[1082].w[19]" 0.57324443352489785;
	setAttr ".wl[1082].w[20]" 0.20079656174320415;
	setAttr -s 4 ".wl[1083].w";
	setAttr ".wl[1083].w[16]" 0.1052970303890403;
	setAttr ".wl[1083].w[18]" 0.25780536403808257;
	setAttr ".wl[1083].w[19]" 0.41748750281052666;
	setAttr ".wl[1083].w[20]" 0.21941010276235043;
	setAttr -s 4 ".wl[1084].w";
	setAttr ".wl[1084].w[16]" 0.11906594916958457;
	setAttr ".wl[1084].w[17]" 0.11906594916958457;
	setAttr ".wl[1084].w[19]" 0.38124534661701676;
	setAttr ".wl[1084].w[20]" 0.38062275504381421;
	setAttr -s 4 ".wl[1085].w";
	setAttr ".wl[1085].w[16]" 0.019444597024236152;
	setAttr ".wl[1085].w[18]" 0.060744437471124929;
	setAttr ".wl[1085].w[19]" 0.45990548275231952;
	setAttr ".wl[1085].w[20]" 0.45990548275231952;
	setAttr -s 4 ".wl[1086].w";
	setAttr ".wl[1086].w[16]" 0.012139825624745108;
	setAttr ".wl[1086].w[18]" 0.040665895966413144;
	setAttr ".wl[1086].w[19]" 0.47359713920442087;
	setAttr ".wl[1086].w[20]" 0.47359713920442087;
	setAttr -s 4 ".wl[1087].w";
	setAttr ".wl[1087].w[16]" 0.091957399342618928;
	setAttr ".wl[1087].w[17]" 0.091957399342618928;
	setAttr ".wl[1087].w[19]" 0.40804260065738091;
	setAttr ".wl[1087].w[20]" 0.40804260065738113;
	setAttr -s 4 ".wl[1088].w";
	setAttr ".wl[1088].w[0]" 0.45178556503466044;
	setAttr ".wl[1088].w[1]" 0.025130879615548662;
	setAttr ".wl[1088].w[15]" 0.097997344244558152;
	setAttr ".wl[1088].w[18]" 0.42508621110523265;
	setAttr -s 4 ".wl[1089].w";
	setAttr ".wl[1089].w[0]" 0.46455927872020158;
	setAttr ".wl[1089].w[1]" 0.027764430609245272;
	setAttr ".wl[1089].w[15]" 0.099526826418005254;
	setAttr ".wl[1089].w[18]" 0.40814946425254789;
	setAttr -s 4 ".wl[1090].w";
	setAttr ".wl[1090].w[0]" 0.48666856211663873;
	setAttr ".wl[1090].w[15]" 0.017409880391716995;
	setAttr ".wl[1090].w[18]" 0.48666856211663873;
	setAttr ".wl[1090].w[19]" 0.0092529953750056133;
	setAttr -s 4 ".wl[1091].w";
	setAttr ".wl[1091].w[0]" 0.48817920778491131;
	setAttr ".wl[1091].w[1]" 0.0082545172673767191;
	setAttr ".wl[1091].w[15]" 0.015387067162800675;
	setAttr ".wl[1091].w[18]" 0.48817920778491131;
	setAttr -s 4 ".wl[1092].w";
	setAttr ".wl[1092].w[1]" 0.33120142315508994;
	setAttr ".wl[1092].w[2]" 0.2093668960302095;
	setAttr ".wl[1092].w[7]" 0.12823977442437651;
	setAttr ".wl[1092].w[11]" 0.33119190639032398;
	setAttr -s 4 ".wl[1093].w";
	setAttr ".wl[1093].w[1]" 0.37780198805765786;
	setAttr ".wl[1093].w[2]" 0.20838045621922213;
	setAttr ".wl[1093].w[7]" 0.036015567665462198;
	setAttr ".wl[1093].w[11]" 0.37780198805765786;
	setAttr -s 4 ".wl[1094].w";
	setAttr ".wl[1094].w[1]" 0.35927832816263033;
	setAttr ".wl[1094].w[2]" 0.26975666895758954;
	setAttr ".wl[1094].w[11]" 0.35927832816263033;
	setAttr ".wl[1094].w[12]" 0.011686674717149872;
	setAttr -s 4 ".wl[1095].w";
	setAttr ".wl[1095].w[0]" 0.44118868649737403;
	setAttr ".wl[1095].w[1]" 0.26565183766927986;
	setAttr ".wl[1095].w[15]" 0.095142227948577018;
	setAttr ".wl[1095].w[18]" 0.19801724788476896;
	setAttr -s 4 ".wl[1096].w[11:14]"  0.0022080829497512868 0.068984897621487726 
		0.86899805761554905 0.059808961813211957;
	setAttr -s 4 ".wl[1097].w";
	setAttr ".wl[1097].w[1]" 0.43486760664517993;
	setAttr ".wl[1097].w[2]" 0.11260363599906296;
	setAttr ".wl[1097].w[7]" 0.10991697870111453;
	setAttr ".wl[1097].w[11]" 0.34261177865464265;
	setAttr -s 4 ".wl[1098].w";
	setAttr ".wl[1098].w[1]" 0.32925183356417226;
	setAttr ".wl[1098].w[2]" 0.29450925348626417;
	setAttr ".wl[1098].w[7]" 0.046987079385391636;
	setAttr ".wl[1098].w[11]" 0.32925183356417204;
	setAttr -s 4 ".wl[1099].w";
	setAttr ".wl[1099].w[1]" 0.20716420869408692;
	setAttr ".wl[1099].w[2]" 0.01567845292834643;
	setAttr ".wl[1099].w[11]" 0.57767102759516176;
	setAttr ".wl[1099].w[12]" 0.19948631078240486;
	setAttr -s 4 ".wl[1100].w";
	setAttr ".wl[1100].w[1]" 0.0010198920435504784;
	setAttr ".wl[1100].w[11]" 0.12162577168863428;
	setAttr ".wl[1100].w[12]" 0.87315389156370815;
	setAttr ".wl[1100].w[13]" 0.0042004447041071756;
	setAttr -s 4 ".wl[1101].w";
	setAttr ".wl[1101].w[1]" 0.00093407034278362143;
	setAttr ".wl[1101].w[11]" 0.16071520450631036;
	setAttr ".wl[1101].w[12]" 0.83554523134810521;
	setAttr ".wl[1101].w[13]" 0.002805493802800861;
	setAttr -s 4 ".wl[1102].w";
	setAttr ".wl[1102].w[1]" 0.26259431455018351;
	setAttr ".wl[1102].w[2]" 0.017789637243630411;
	setAttr ".wl[1102].w[11]" 0.65782028957395988;
	setAttr ".wl[1102].w[12]" 0.061795758632226229;
	setAttr -s 4 ".wl[1103].w[11:14]"  0.0022811724151860654 0.073915314196173157 
		0.86894456590638025 0.054858947482260507;
	setAttr -s 4 ".wl[1104].w";
	setAttr ".wl[1104].w[0]" 0.46119318300487949;
	setAttr ".wl[1104].w[1]" 0.27055443219410208;
	setAttr ".wl[1104].w[15]" 0.087337880852949246;
	setAttr ".wl[1104].w[18]" 0.18091450394806913;
	setAttr -s 4 ".wl[1105].w";
	setAttr ".wl[1105].w[0]" 0.37285032424158582;
	setAttr ".wl[1105].w[1]" 0.231826612298389;
	setAttr ".wl[1105].w[11]" 0.083838940363910069;
	setAttr ".wl[1105].w[18]" 0.31148412309611523;
	setAttr -s 4 ".wl[1106].w";
	setAttr ".wl[1106].w[0]" 0.19749367357134445;
	setAttr ".wl[1106].w[1]" 0.39647519967806316;
	setAttr ".wl[1106].w[7]" 0.16986680984574237;
	setAttr ".wl[1106].w[11]" 0.23616431690485004;
	setAttr -s 4 ".wl[1107].w";
	setAttr ".wl[1107].w[0]" 0.042244953967448519;
	setAttr ".wl[1107].w[1]" 0.18365462829414356;
	setAttr ".wl[1107].w[11]" 0.42886219367999528;
	setAttr ".wl[1107].w[12]" 0.34523822405841265;
	setAttr -s 4 ".wl[1108].w";
	setAttr ".wl[1108].w[0]" 0.35415249404039667;
	setAttr ".wl[1108].w[1]" 0.22680811196114106;
	setAttr ".wl[1108].w[11]" 0.10456074492542139;
	setAttr ".wl[1108].w[18]" 0.31447864907304079;
	setAttr -s 4 ".wl[1109].w";
	setAttr ".wl[1109].w[0]" 0.066582190384150905;
	setAttr ".wl[1109].w[1]" 0.23174498563448109;
	setAttr ".wl[1109].w[11]" 0.38809672317552513;
	setAttr ".wl[1109].w[12]" 0.31357610080584297;
	setAttr -s 4 ".wl[1110].w[11:14]"  0.005301093058861223 0.12929859893271289 
		0.73472826703021699 0.13067204097820906;
	setAttr -s 4 ".wl[1111].w[11:14]"  0.00020763237083264589 0.0018222368306557732 
		0.49898506539925563 0.49898506539925586;
	setAttr -s 4 ".wl[1112].w";
	setAttr ".wl[1112].w[1]" 0.0090546362535452417;
	setAttr ".wl[1112].w[11]" 0.1956579681901271;
	setAttr ".wl[1112].w[12]" 0.72236863205815782;
	setAttr ".wl[1112].w[13]" 0.072918763498169795;
	setAttr -s 4 ".wl[1113].w[11:14]"  0.0067953659519189155 0.14072492598302017 
		0.70619823045820873 0.1462814776068522;
	setAttr -s 4 ".wl[1114].w";
	setAttr ".wl[1114].w[1]" 0.01025114834319343;
	setAttr ".wl[1114].w[11]" 0.17795298354584255;
	setAttr ".wl[1114].w[12]" 0.71642029122501039;
	setAttr ".wl[1114].w[13]" 0.095375576885953675;
	setAttr -s 4 ".wl[1115].w";
	setAttr ".wl[1115].w[1]" 0.35425598564134236;
	setAttr ".wl[1115].w[2]" 0.041988933432351487;
	setAttr ".wl[1115].w[11]" 0.49394070080201558;
	setAttr ".wl[1115].w[12]" 0.10981438012429061;
	setAttr -s 4 ".wl[1116].w[11:14]"  0.00092935295528391636 0.040024184200411213 
		0.93332861129637057 0.025717851547934243;
	setAttr -s 4 ".wl[1117].w";
	setAttr ".wl[1117].w[1]" 0.22588838968849514;
	setAttr ".wl[1117].w[2]" 0.016535733421232747;
	setAttr ".wl[1117].w[11]" 0.66765813371753346;
	setAttr ".wl[1117].w[12]" 0.089917743172738734;
	setAttr -s 4 ".wl[1118].w";
	setAttr ".wl[1118].w[1]" 0.11049217188542348;
	setAttr ".wl[1118].w[2]" 0.0055649418350726806;
	setAttr ".wl[1118].w[11]" 0.84284723231453595;
	setAttr ".wl[1118].w[12]" 0.041095653964967803;
	setAttr -s 4 ".wl[1119].w";
	setAttr ".wl[1119].w[1]" 0.00060464336965293422;
	setAttr ".wl[1119].w[11]" 0.080300798599217904;
	setAttr ".wl[1119].w[12]" 0.91657849378441469;
	setAttr ".wl[1119].w[13]" 0.002516064246714475;
	setAttr -s 4 ".wl[1120].w";
	setAttr ".wl[1120].w[0]" 0.03439072880631986;
	setAttr ".wl[1120].w[1]" 0.29872385906754595;
	setAttr ".wl[1120].w[11]" 0.45449541673259702;
	setAttr ".wl[1120].w[12]" 0.21238999539353717;
	setAttr -s 4 ".wl[1121].w[11:14]"  8.0032064351940723e-05 0.000765517195835666 
		0.49957722536990617 0.49957722536990617;
	setAttr -s 4 ".wl[1122].w[11:14]"  0.00056776743431738506 0.029104575667007325 
		0.95144442502158078 0.018883231877094519;
	setAttr -s 4 ".wl[1123].w";
	setAttr ".wl[1123].w[1]" 0.0058478063368871385;
	setAttr ".wl[1123].w[11]" 0.20734798544286973;
	setAttr ".wl[1123].w[12]" 0.76169294394750287;
	setAttr ".wl[1123].w[13]" 0.0251112642727403;
	setAttr -s 4 ".wl[1124].w[11:14]"  0.00010078356925740154 0.00095301796796677348 
		0.49947309923138794 0.49947309923138794;
	setAttr -s 4 ".wl[1125].w";
	setAttr ".wl[1125].w[1]" 0.005269625277388351;
	setAttr ".wl[1125].w[11]" 0.15933307117696338;
	setAttr ".wl[1125].w[12]" 0.80305611979311786;
	setAttr ".wl[1125].w[13]" 0.032341183752530464;
	setAttr -s 4 ".wl[1126].w[11:14]"  0.00015666920842042533 0.0014081219682644164 
		0.49921760441165763 0.49921760441165763;
	setAttr -s 4 ".wl[1127].w[11:14]"  0.00038022655292751898 0.02007173006536694 
		0.96430137847153119 0.015246664910174363;
	setAttr -s 4 ".wl[1128].w";
	setAttr ".wl[1128].w[1]" 0.0016409933072823594;
	setAttr ".wl[1128].w[11]" 0.11870230437171279;
	setAttr ".wl[1128].w[12]" 0.87191197589305836;
	setAttr ".wl[1128].w[13]" 0.0077447264279465987;
	setAttr -s 4 ".wl[1129].w[11:14]"  0.00049053859844516593 0.024912806215597327 
		0.9594193904068925 0.015177264779065105;
	setAttr -s 4 ".wl[1130].w";
	setAttr ".wl[1130].w[15]" 0.013860800049195125;
	setAttr ".wl[1130].w[16]" 0.4868789281538457;
	setAttr ".wl[1130].w[17]" 0.4868789281538457;
	setAttr ".wl[1130].w[19]" 0.012381343643113576;
	setAttr -s 4 ".wl[1131].w";
	setAttr ".wl[1131].w[15]" 0.029393787653044196;
	setAttr ".wl[1131].w[16]" 0.47257329884843047;
	setAttr ".wl[1131].w[17]" 0.47257329884843069;
	setAttr ".wl[1131].w[19]" 0.02545961465009455;
	setAttr -s 4 ".wl[1132].w";
	setAttr ".wl[1132].w[0]" 0.50227583441338008;
	setAttr ".wl[1132].w[1]" 0.0091979460778608326;
	setAttr ".wl[1132].w[15]" 0.41247887188594057;
	setAttr ".wl[1132].w[18]" 0.076047347622818534;
	setAttr -s 4 ".wl[1133].w";
	setAttr ".wl[1133].w[0]" 0.49435124760371357;
	setAttr ".wl[1133].w[15]" 0.50024249546471877;
	setAttr ".wl[1133].w[16]" 0.001522587156839022;
	setAttr ".wl[1133].w[18]" 0.003883669774728643;
	setAttr -s 4 ".wl[1134].w";
	setAttr ".wl[1134].w[0]" 0.49423424158067902;
	setAttr ".wl[1134].w[15]" 0.4998239727223926;
	setAttr ".wl[1134].w[16]" 0.0016786347637908039;
	setAttr ".wl[1134].w[18]" 0.0042631509331375597;
	setAttr -s 4 ".wl[1135].w";
	setAttr ".wl[1135].w[0]" 0.48800785348670245;
	setAttr ".wl[1135].w[1]" 0.0075239899668713937;
	setAttr ".wl[1135].w[15]" 0.43530141807876416;
	setAttr ".wl[1135].w[18]" 0.069166738467661915;
	setAttr -s 4 ".wl[1136].w";
	setAttr ".wl[1136].w[0]" 0.41840642667632666;
	setAttr ".wl[1136].w[15]" 0.46410238470093168;
	setAttr ".wl[1136].w[16]" 0.024023798517219801;
	setAttr ".wl[1136].w[18]" 0.093467390105521839;
	setAttr -s 4 ".wl[1137].w";
	setAttr ".wl[1137].w[0]" 0.27867066282896141;
	setAttr ".wl[1137].w[15]" 0.70869688762795091;
	setAttr ".wl[1137].w[16]" 0.0068851290539193736;
	setAttr ".wl[1137].w[18]" 0.0057473204891683137;
	setAttr -s 4 ".wl[1138].w";
	setAttr ".wl[1138].w[0]" 0.2930424421785533;
	setAttr ".wl[1138].w[15]" 0.69212629712600582;
	setAttr ".wl[1138].w[16]" 0.0080797473760381676;
	setAttr ".wl[1138].w[18]" 0.0067515133194028121;
	setAttr -s 4 ".wl[1139].w";
	setAttr ".wl[1139].w[0]" 0.41947431117360351;
	setAttr ".wl[1139].w[15]" 0.47321847048815613;
	setAttr ".wl[1139].w[16]" 0.021320619486925867;
	setAttr ".wl[1139].w[18]" 0.085986598851314489;
	setAttr -s 4 ".wl[1140].w";
	setAttr ".wl[1140].w[0]" 0.16209698640783848;
	setAttr ".wl[1140].w[15]" 0.48158764003970522;
	setAttr ".wl[1140].w[16]" 0.25928349178322713;
	setAttr ".wl[1140].w[18]" 0.097031881769229042;
	setAttr -s 4 ".wl[1141].w";
	setAttr ".wl[1141].w[0]" 0.037931441659690528;
	setAttr ".wl[1141].w[15]" 0.66049910956083357;
	setAttr ".wl[1141].w[16]" 0.29458488074749595;
	setAttr ".wl[1141].w[18]" 0.0069845680319799864;
	setAttr -s 4 ".wl[1142].w";
	setAttr ".wl[1142].w[0]" 0.047733952161328232;
	setAttr ".wl[1142].w[15]" 0.62794119378277347;
	setAttr ".wl[1142].w[16]" 0.31504435418713161;
	setAttr ".wl[1142].w[18]" 0.0092804998687666534;
	setAttr -s 4 ".wl[1143].w";
	setAttr ".wl[1143].w[0]" 0.16165784952739956;
	setAttr ".wl[1143].w[15]" 0.4828402157825526;
	setAttr ".wl[1143].w[16]" 0.25908350778922751;
	setAttr ".wl[1143].w[18]" 0.096418426900820337;
	setAttr -s 4 ".wl[1144].w";
	setAttr ".wl[1144].w[15]" 0.22647735251662079;
	setAttr ".wl[1144].w[16]" 0.41610528054496398;
	setAttr ".wl[1144].w[17]" 0.26777165109381162;
	setAttr ".wl[1144].w[19]" 0.08964571584460361;
	setAttr -s 4 ".wl[1145].w";
	setAttr ".wl[1145].w[15]" 0.1586771681914593;
	setAttr ".wl[1145].w[16]" 0.57319932964453491;
	setAttr ".wl[1145].w[17]" 0.25774761385068617;
	setAttr ".wl[1145].w[19]" 0.010375888313319571;
	setAttr -s 4 ".wl[1146].w";
	setAttr ".wl[1146].w[15]" 0.18020669294862673;
	setAttr ".wl[1146].w[16]" 0.53121166705480294;
	setAttr ".wl[1146].w[17]" 0.27452258303986216;
	setAttr ".wl[1146].w[19]" 0.014059056956708256;
	setAttr -s 4 ".wl[1147].w";
	setAttr ".wl[1147].w[15]" 0.23284162378915707;
	setAttr ".wl[1147].w[16]" 0.40032321426833273;
	setAttr ".wl[1147].w[17]" 0.2707871585895637;
	setAttr ".wl[1147].w[19]" 0.096048003352946512;
	setAttr -s 4 ".wl[1148].w";
	setAttr ".wl[1148].w[16]" 0.38703243280527183;
	setAttr ".wl[1148].w[17]" 0.38703243280527205;
	setAttr ".wl[1148].w[19]" 0.11327199077221815;
	setAttr ".wl[1148].w[20]" 0.11266314361723789;
	setAttr -s 4 ".wl[1149].w";
	setAttr ".wl[1149].w[15]" 0.059606826728870153;
	setAttr ".wl[1149].w[16]" 0.46044855762720294;
	setAttr ".wl[1149].w[17]" 0.46044855762720294;
	setAttr ".wl[1149].w[19]" 0.019496058016723907;
	setAttr -s 4 ".wl[1150].w";
	setAttr ".wl[1150].w[15]" 0.044913126033550417;
	setAttr ".wl[1150].w[16]" 0.47054233939563361;
	setAttr ".wl[1150].w[17]" 0.4705423393956335;
	setAttr ".wl[1150].w[19]" 0.01400219517518244;
	setAttr -s 4 ".wl[1151].w";
	setAttr ".wl[1151].w[16]" 0.39839189568737643;
	setAttr ".wl[1151].w[17]" 0.39839189568737643;
	setAttr ".wl[1151].w[19]" 0.10171703146943126;
	setAttr ".wl[1151].w[20]" 0.1014991771558159;
	setAttr -s 4 ".wl[1152].w";
	setAttr ".wl[1152].w[0]" 0.45060096078123019;
	setAttr ".wl[1152].w[1]" 0.024577774554546902;
	setAttr ".wl[1152].w[15]" 0.43055982062916093;
	setAttr ".wl[1152].w[18]" 0.094261444035061981;
	setAttr -s 4 ".wl[1153].w";
	setAttr ".wl[1153].w[0]" 0.46214247819150406;
	setAttr ".wl[1153].w[1]" 0.027372271102980371;
	setAttr ".wl[1153].w[15]" 0.41368340089800232;
	setAttr ".wl[1153].w[18]" 0.096801849807513302;
	setAttr -s 4 ".wl[1154].w";
	setAttr ".wl[1154].w[0]" 0.48718415458276437;
	setAttr ".wl[1154].w[15]" 0.48718415458276426;
	setAttr ".wl[1154].w[16]" 0.0092998905375547697;
	setAttr ".wl[1154].w[18]" 0.016331800296916603;
	setAttr -s 4 ".wl[1155].w";
	setAttr ".wl[1155].w[0]" 0.4886977460206573;
	setAttr ".wl[1155].w[15]" 0.48869774602065708;
	setAttr ".wl[1155].w[16]" 0.0081623924531465785;
	setAttr ".wl[1155].w[18]" 0.014442115505539107;
	setAttr -s 4 ".wl[1156].w";
	setAttr ".wl[1156].w[1]" 0.29213739125379817;
	setAttr ".wl[1156].w[2]" 0.18739103082958508;
	setAttr ".wl[1156].w[7]" 0.3288688789739137;
	setAttr ".wl[1156].w[8]" 0.19160269894270304;
	setAttr -s 4 ".wl[1157].w";
	setAttr ".wl[1157].w[1]" 0.29823932204894449;
	setAttr ".wl[1157].w[2]" 0.26348306702951474;
	setAttr ".wl[1157].w[7]" 0.28352551358673811;
	setAttr ".wl[1157].w[8]" 0.15475209733480261;
	setAttr -s 4 ".wl[1158].w";
	setAttr ".wl[1158].w[1]" 0.43478647104608836;
	setAttr ".wl[1158].w[2]" 0.41919755822420851;
	setAttr ".wl[1158].w[7]" 0.092298556100447099;
	setAttr ".wl[1158].w[8]" 0.053717414629256027;
	setAttr -s 4 ".wl[1159].w";
	setAttr ".wl[1159].w[0]" 0.43736772700477672;
	setAttr ".wl[1159].w[1]" 0.26335114687866024;
	setAttr ".wl[1159].w[7]" 0.092689827208240982;
	setAttr ".wl[1159].w[15]" 0.20659129890832204;
	setAttr -s 4 ".wl[1160].w[7:10]"  0.00053445405468228568 0.015416084721563949 
		0.93340641744479858 0.050643043778955267;
	setAttr -s 4 ".wl[1161].w";
	setAttr ".wl[1161].w[1]" 0.34725362302690282;
	setAttr ".wl[1161].w[2]" 0.1311427668059183;
	setAttr ".wl[1161].w[7]" 0.34827506248136481;
	setAttr ".wl[1161].w[8]" 0.17332854768581399;
	setAttr -s 4 ".wl[1162].w";
	setAttr ".wl[1162].w[1]" 0.38736856165004296;
	setAttr ".wl[1162].w[2]" 0.36422890592687163;
	setAttr ".wl[1162].w[7]" 0.15259429063810323;
	setAttr ".wl[1162].w[8]" 0.095808241784982248;
	setAttr -s 4 ".wl[1163].w";
	setAttr ".wl[1163].w[1]" 0.12570574326694664;
	setAttr ".wl[1163].w[2]" 0.025560784938302959;
	setAttr ".wl[1163].w[7]" 0.44427547885997415;
	setAttr ".wl[1163].w[8]" 0.4044579929347763;
	setAttr -s 4 ".wl[1164].w";
	setAttr ".wl[1164].w[1]" 0.0016020883354424299;
	setAttr ".wl[1164].w[7]" 0.016666360621644536;
	setAttr ".wl[1164].w[8]" 0.92254554760757856;
	setAttr ".wl[1164].w[9]" 0.059186003435334457;
	setAttr -s 4 ".wl[1165].w";
	setAttr ".wl[1165].w[1]" 0.0015155252483150801;
	setAttr ".wl[1165].w[7]" 0.01954810853675671;
	setAttr ".wl[1165].w[8]" 0.93155682209371238;
	setAttr ".wl[1165].w[9]" 0.047379544121215894;
	setAttr -s 4 ".wl[1166].w";
	setAttr ".wl[1166].w[1]" 0.071743420112004463;
	setAttr ".wl[1166].w[2]" 0.048634341877938185;
	setAttr ".wl[1166].w[7]" 0.44858635489563753;
	setAttr ".wl[1166].w[8]" 0.43103588311441982;
	setAttr -s 4 ".wl[1167].w[7:10]"  0.00022896002007152601 0.0071151376221295342 
		0.96797114056425149 0.024684761793547557;
	setAttr -s 4 ".wl[1168].w";
	setAttr ".wl[1168].w[0]" 0.4583298635383059;
	setAttr ".wl[1168].w[1]" 0.2688747098427095;
	setAttr ".wl[1168].w[7]" 0.08414145430634698;
	setAttr ".wl[1168].w[15]" 0.18865397231263756;
	setAttr -s 4 ".wl[1169].w";
	setAttr ".wl[1169].w[0]" 0.35626960594766938;
	setAttr ".wl[1169].w[1]" 0.22151728943768867;
	setAttr ".wl[1169].w[7]" 0.10529384601547627;
	setAttr ".wl[1169].w[15]" 0.31691925859916564;
	setAttr -s 4 ".wl[1170].w";
	setAttr ".wl[1170].w[0]" 0.1409175389123199;
	setAttr ".wl[1170].w[1]" 0.35336140129462884;
	setAttr ".wl[1170].w[7]" 0.3443174449849542;
	setAttr ".wl[1170].w[8]" 0.16140361480809706;
	setAttr -s 4 ".wl[1171].w";
	setAttr ".wl[1171].w[1]" 0.15461518694902857;
	setAttr ".wl[1171].w[7]" 0.38445143332582421;
	setAttr ".wl[1171].w[8]" 0.41183358996721919;
	setAttr ".wl[1171].w[9]" 0.049099789757928107;
	setAttr -s 4 ".wl[1172].w";
	setAttr ".wl[1172].w[0]" 0.3434043857720841;
	setAttr ".wl[1172].w[1]" 0.21978526295944656;
	setAttr ".wl[1172].w[7]" 0.11250234454997368;
	setAttr ".wl[1172].w[15]" 0.32430800671849569;
	setAttr -s 4 ".wl[1173].w";
	setAttr ".wl[1173].w[0]" 0.047969548454513254;
	setAttr ".wl[1173].w[1]" 0.15478526507052451;
	setAttr ".wl[1173].w[7]" 0.3892285738090146;
	setAttr ".wl[1173].w[8]" 0.40801661266594763;
	setAttr -s 4 ".wl[1174].w[7:10]"  0.0047197447788290504 0.087093069557030015 
		0.7135120314638399 0.1946751542003011;
	setAttr -s 4 ".wl[1175].w[7:10]"  0.00021126319728316474 0.0018626017282677026 
		0.49896306753722458 0.49896306753722458;
	setAttr -s 4 ".wl[1176].w";
	setAttr ".wl[1176].w[1]" 0.0068718470226344893;
	setAttr ".wl[1176].w[7]" 0.041156783559742866;
	setAttr ".wl[1176].w[8]" 0.72402553784661783;
	setAttr ".wl[1176].w[9]" 0.22794583157100481;
	setAttr -s 4 ".wl[1177].w[7:10]"  0.0045483744695385322 0.084865968154147958 
		0.71716012482668889 0.19342553254962469;
	setAttr -s 4 ".wl[1178].w";
	setAttr ".wl[1178].w[1]" 0.0069355240142974428;
	setAttr ".wl[1178].w[7]" 0.041823345396207176;
	setAttr ".wl[1178].w[8]" 0.72722814990588036;
	setAttr ".wl[1178].w[9]" 0.22401298068361508;
	setAttr -s 4 ".wl[1179].w";
	setAttr ".wl[1179].w[1]" 0.06703958739244105;
	setAttr ".wl[1179].w[2]" 0.021808200589774947;
	setAttr ".wl[1179].w[7]" 0.46979368318658815;
	setAttr ".wl[1179].w[8]" 0.44135852883119586;
	setAttr -s 4 ".wl[1180].w[7:10]"  5.4129503699934196e-05 0.0018342827501598954 
		0.99161935151782932 0.0064922362283108504;
	setAttr -s 4 ".wl[1181].w";
	setAttr ".wl[1181].w[1]" 0.013879324464353275;
	setAttr ".wl[1181].w[2]" 0.0065530997073163424;
	setAttr ".wl[1181].w[7]" 0.48978378791416516;
	setAttr ".wl[1181].w[8]" 0.48978378791416516;
	setAttr -s 4 ".wl[1182].w";
	setAttr ".wl[1182].w[1]" 0.01629702220661507;
	setAttr ".wl[1182].w[2]" 0.010760725837133379;
	setAttr ".wl[1182].w[7]" 0.48647112597812581;
	setAttr ".wl[1182].w[8]" 0.4864711259781257;
	setAttr -s 4 ".wl[1183].w";
	setAttr ".wl[1183].w[1]" 0.00042284219213387781;
	setAttr ".wl[1183].w[7]" 0.0059874798951900492;
	setAttr ".wl[1183].w[8]" 0.9740083068662424;
	setAttr ".wl[1183].w[9]" 0.01958137104643367;
	setAttr -s 4 ".wl[1184].w";
	setAttr ".wl[1184].w[0]" 0.014509107670285213;
	setAttr ".wl[1184].w[1]" 0.12098826634235478;
	setAttr ".wl[1184].w[7]" 0.46077705711238498;
	setAttr ".wl[1184].w[8]" 0.40372556887497496;
	setAttr -s 4 ".wl[1185].w[7:10]"  3.5751377102935084e-05 0.00032878606108691257 
		0.49981773128090506 0.49981773128090506;
	setAttr -s 4 ".wl[1186].w[7:10]"  0.00035066404453848042 0.010554022374066741 
		0.95508342098876176 0.034011892592633035;
	setAttr -s 4 ".wl[1187].w";
	setAttr ".wl[1187].w[1]" 0.0014666078656511189;
	setAttr ".wl[1187].w[7]" 0.019628118584336288;
	setAttr ".wl[1187].w[8]" 0.93652057075932083;
	setAttr ".wl[1187].w[9]" 0.042384702790691745;
	setAttr -s 4 ".wl[1188].w[7:10]"  0.0002430672816078221 0.0021292588717027007 
		0.4988138369233448 0.4988138369233448;
	setAttr -s 4 ".wl[1189].w";
	setAttr ".wl[1189].w[1]" 0.0015562967074829225;
	setAttr ".wl[1189].w[7]" 0.016757259451150971;
	setAttr ".wl[1189].w[8]" 0.92893087832426835;
	setAttr ".wl[1189].w[9]" 0.052755565517097765;
	setAttr -s 4 ".wl[1190].w[7:10]"  2.5692582068180317e-05 0.000237622360362874 
		0.49986834252878448 0.49986834252878448;
	setAttr -s 4 ".wl[1191].w[7:10]"  0.00072677564864042396 0.020258630035872329 
		0.91740657622836708 0.061608018087120194;
	setAttr -s 4 ".wl[1192].w";
	setAttr ".wl[1192].w[1]" 0.00041459727663041075;
	setAttr ".wl[1192].w[7]" 0.0059451426383140002;
	setAttr ".wl[1192].w[8]" 0.9750032843913613;
	setAttr ".wl[1192].w[9]" 0.018636975693694251;
	setAttr -s 4 ".wl[1193].w[7:10]"  7.4364895051684903e-05 0.0024913849048774001 
		0.98885253510739946 0.0085817150926714297;
	setAttr -s 4 ".wl[1194].w[11:14]"  0.00032175543874888598 0.0030645878574838041 
		0.49830682835188361 0.49830682835188361;
	setAttr -s 4 ".wl[1195].w[11:14]"  0.00013504876666467163 0.0013882939789195527 
		0.49923832862720791 0.49923832862720791;
	setAttr -s 4 ".wl[1196].w[11:14]"  8.1468553880321884e-05 0.00087804382122663672 
		0.49952024381244647 0.49952024381244647;
	setAttr -s 4 ".wl[1197].w[11:14]"  0.00010279819602205719 0.0011249707364525531 
		0.49938611553376272 0.49938611553376272;
	setAttr -s 4 ".wl[1198].w[11:14]"  9.9892513506957332e-05 0.0010944174391787774 
		0.49940284502365712 0.49940284502365712;
	setAttr -s 4 ".wl[1199].w[11:14]"  0.0017031689425583043 0.014751477902332754 
		0.49177267657755447 0.49177267657755447;
	setAttr -s 4 ".wl[1200].w[11:14]"  0.0020828831750287465 0.016871264939285636 
		0.49052292594284286 0.49052292594284275;
	setAttr -s 4 ".wl[1201].w[11:14]"  0.0003791034294402705 0.0035162766339602697 
		0.49805230996829974 0.49805230996829974;
	setAttr -s 4 ".wl[1202].w[7:10]"  0.00018298616571612268 0.0017596891719727256 
		0.49902866233115561 0.49902866233115561;
	setAttr -s 4 ".wl[1203].w[7:10]"  0.0020002049051920938 0.016158477646091112 
		0.49092065872435842 0.49092065872435842;
	setAttr -s 4 ".wl[1204].w[7:10]"  0.0020859691947243799 0.016757989117853211 
		0.49057802084371122 0.49057802084371122;
	setAttr -s 4 ".wl[1205].w[7:10]"  0.0002494906181736202 0.0023655080705123747 
		0.49869250065565701 0.49869250065565701;
	setAttr -s 4 ".wl[1206].w[7:10]"  7.2028423786849819e-05 0.00070974377693553542 
		0.4996091138996388 0.4996091138996388;
	setAttr -s 4 ".wl[1207].w[7:10]"  1.4661670554603781e-05 0.00014919126874438424 
		0.49991807353035056 0.49991807353035045;
	setAttr -s 4 ".wl[1208].w[7:10]"  1.0079233914624898e-05 0.00010299442722183497 
		0.49994346316943183 0.49994346316943172;
	setAttr -s 4 ".wl[1209].w[7:10]"  4.3012238582727299e-05 0.000429047735395134 
		0.49976397001301104 0.49976397001301104;
	setAttr -s 4 ".wl[1210].w";
	setAttr ".wl[1210].w[0]" 0.47114767422352;
	setAttr ".wl[1210].w[1]" 0.018348399558293857;
	setAttr ".wl[1210].w[15]" 0.25548923838469101;
	setAttr ".wl[1210].w[18]" 0.25501468783349512;
	setAttr -s 4 ".wl[1211].w";
	setAttr ".wl[1211].w[0]" 0.47009231157579301;
	setAttr ".wl[1211].w[15]" 0.52428737892298893;
	setAttr ".wl[1211].w[16]" 0.0021741076303540981;
	setAttr ".wl[1211].w[18]" 0.0034462018708638657;
	setAttr -s 4 ".wl[1212].w";
	setAttr ".wl[1212].w[0]" 0.4380063365521254;
	setAttr ".wl[1212].w[1]" 0.016537310882000462;
	setAttr ".wl[1212].w[15]" 0.27364790906863923;
	setAttr ".wl[1212].w[18]" 0.27180844349723493;
	setAttr -s 4 ".wl[1213].w";
	setAttr ".wl[1213].w[0]" 0.48324668941803006;
	setAttr ".wl[1213].w[15]" 0.0036267037274447015;
	setAttr ".wl[1213].w[18]" 0.51099396060398972;
	setAttr ".wl[1213].w[19]" 0.0021326462505355031;
	setAttr -s 4 ".wl[1214].w";
	setAttr ".wl[1214].w[0]" 0.45686238954144109;
	setAttr ".wl[1214].w[1]" 0.035142760348753667;
	setAttr ".wl[1214].w[15]" 0.25555933458808933;
	setAttr ".wl[1214].w[18]" 0.25243551552171606;
	setAttr -s 4 ".wl[1215].w";
	setAttr ".wl[1215].w[0]" 0.49001747092643155;
	setAttr ".wl[1215].w[15]" 0.49166157719861991;
	setAttr ".wl[1215].w[16]" 0.0079157930834125225;
	setAttr ".wl[1215].w[18]" 0.010405158791536074;
	setAttr -s 4 ".wl[1216].w";
	setAttr ".wl[1216].w[0]" 0.42286608119810354;
	setAttr ".wl[1216].w[1]" 0.032322238341580461;
	setAttr ".wl[1216].w[15]" 0.27478293449360991;
	setAttr ".wl[1216].w[18]" 0.27002874596670612;
	setAttr -s 4 ".wl[1217].w";
	setAttr ".wl[1217].w[0]" 0.49061104603294403;
	setAttr ".wl[1217].w[15]" 0.010985176046279637;
	setAttr ".wl[1217].w[18]" 0.49061104603294403;
	setAttr ".wl[1217].w[19]" 0.0077927318878322355;
	setAttr -s 4 ".wl[1218].w";
	setAttr ".wl[1218].w[0]" 0.41594057151090891;
	setAttr ".wl[1218].w[1]" 0.033968014721093974;
	setAttr ".wl[1218].w[15]" 0.27515996513162677;
	setAttr ".wl[1218].w[18]" 0.27493144863637026;
	setAttr -s 4 ".wl[1219].w";
	setAttr ".wl[1219].w[0]" 0.45518854935621023;
	setAttr ".wl[1219].w[15]" 0.5177801814367553;
	setAttr ".wl[1219].w[16]" 0.014814649229300862;
	setAttr ".wl[1219].w[18]" 0.012216619977733675;
	setAttr -s 4 ".wl[1220].w";
	setAttr ".wl[1220].w[0]" 0.38816450042619771;
	setAttr ".wl[1220].w[1]" 0.030884266638333237;
	setAttr ".wl[1220].w[15]" 0.29120151581727183;
	setAttr ".wl[1220].w[18]" 0.28974971711819725;
	setAttr -s 4 ".wl[1221].w";
	setAttr ".wl[1221].w[0]" 0.4645084048376773;
	setAttr ".wl[1221].w[15]" 0.012583597775727208;
	setAttr ".wl[1221].w[18]" 0.50865848137591463;
	setAttr ".wl[1221].w[19]" 0.014249516010680773;
	setAttr -s 4 ".wl[1222].w";
	setAttr ".wl[1222].w[0]" 0.40440687376197226;
	setAttr ".wl[1222].w[15]" 0.28323771757480648;
	setAttr ".wl[1222].w[16]" 0.026183590423447353;
	setAttr ".wl[1222].w[18]" 0.28617181823977383;
	setAttr -s 4 ".wl[1223].w";
	setAttr ".wl[1223].w[0]" 0.3854225194153858;
	setAttr ".wl[1223].w[15]" 0.59562609307515435;
	setAttr ".wl[1223].w[16]" 0.011069027296368201;
	setAttr ".wl[1223].w[18]" 0.0078823602130916257;
	setAttr -s 4 ".wl[1224].w";
	setAttr ".wl[1224].w[0]" 0.37956715825864173;
	setAttr ".wl[1224].w[15]" 0.29687149383694406;
	setAttr ".wl[1224].w[16]" 0.024652000221672275;
	setAttr ".wl[1224].w[18]" 0.29890934768274202;
	setAttr -s 4 ".wl[1225].w";
	setAttr ".wl[1225].w[0]" 0.40536481278993397;
	setAttr ".wl[1225].w[15]" 0.0080303463488361551;
	setAttr ".wl[1225].w[18]" 0.57608762388224377;
	setAttr ".wl[1225].w[19]" 0.010517216978986067;
	setAttr -s 4 ".wl[1226].w";
	setAttr ".wl[1226].w[0]" 0.48652706204024609;
	setAttr ".wl[1226].w[15]" 0.48685413908644315;
	setAttr ".wl[1226].w[16]" 0.0057106961761421906;
	setAttr ".wl[1226].w[18]" 0.020908102697168578;
	setAttr -s 4 ".wl[1227].w";
	setAttr ".wl[1227].w[0]" 0.48968954878507703;
	setAttr ".wl[1227].w[15]" 0.49007947248937483;
	setAttr ".wl[1227].w[16]" 0.0042395898446521653;
	setAttr ".wl[1227].w[18]" 0.015991388880895983;
	setAttr -s 4 ".wl[1228].w";
	setAttr ".wl[1228].w[0]" 0.48978998989943934;
	setAttr ".wl[1228].w[15]" 0.016351792334144265;
	setAttr ".wl[1228].w[18]" 0.48978998989943934;
	setAttr ".wl[1228].w[19]" 0.0040682278669769934;
	setAttr -s 4 ".wl[1229].w";
	setAttr ".wl[1229].w[0]" 0.48673189092856001;
	setAttr ".wl[1229].w[15]" 0.02110615767940803;
	setAttr ".wl[1229].w[18]" 0.4867318909285599;
	setAttr ".wl[1229].w[19]" 0.0054300604634721101;
	setAttr -s 4 ".wl[1230].w";
	setAttr ".wl[1230].w[0]" 0.47858993414885992;
	setAttr ".wl[1230].w[1]" 0.011126901641539736;
	setAttr ".wl[1230].w[15]" 0.47858993414886014;
	setAttr ".wl[1230].w[18]" 0.031693230060740159;
	setAttr -s 4 ".wl[1231].w";
	setAttr ".wl[1231].w[0]" 0.48242301594685644;
	setAttr ".wl[1231].w[1]" 0.0087164558438377025;
	setAttr ".wl[1231].w[15]" 0.48242301594685644;
	setAttr ".wl[1231].w[18]" 0.026437512262449461;
	setAttr -s 4 ".wl[1232].w";
	setAttr ".wl[1232].w[0]" 0.48156071584960025;
	setAttr ".wl[1232].w[1]" 0.0090754905114012567;
	setAttr ".wl[1232].w[15]" 0.027803077789398386;
	setAttr ".wl[1232].w[18]" 0.48156071584960025;
	setAttr -s 4 ".wl[1233].w";
	setAttr ".wl[1233].w[0]" 0.47781737891933856;
	setAttr ".wl[1233].w[1]" 0.011459620452137876;
	setAttr ".wl[1233].w[15]" 0.032905621709184672;
	setAttr ".wl[1233].w[18]" 0.47781737891933879;
	setAttr -s 4 ".wl[1234].w";
	setAttr ".wl[1234].w[0]" 0.47110435044234072;
	setAttr ".wl[1234].w[15]" 0.4769736548947156;
	setAttr ".wl[1234].w[16]" 0.017370595717576331;
	setAttr ".wl[1234].w[18]" 0.034551398945367261;
	setAttr -s 4 ".wl[1235].w";
	setAttr ".wl[1235].w[0]" 0.47479723628840775;
	setAttr ".wl[1235].w[15]" 0.48144858319083039;
	setAttr ".wl[1235].w[16]" 0.014481830128795789;
	setAttr ".wl[1235].w[18]" 0.029272350391966129;
	setAttr -s 4 ".wl[1236].w";
	setAttr ".wl[1236].w[0]" 0.47712203343886106;
	setAttr ".wl[1236].w[15]" 0.029801239029181852;
	setAttr ".wl[1236].w[18]" 0.47920703858720787;
	setAttr ".wl[1236].w[19]" 0.013869688944749263;
	setAttr -s 4 ".wl[1237].w";
	setAttr ".wl[1237].w[0]" 0.47336416485939115;
	setAttr ".wl[1237].w[15]" 0.034867504581432868;
	setAttr ".wl[1237].w[18]" 0.47521177800385772;
	setAttr ".wl[1237].w[19]" 0.016556552555318307;
	setAttr -s 4 ".wl[1238].w";
	setAttr ".wl[1238].w[0]" 0.45034416352416701;
	setAttr ".wl[1238].w[15]" 0.50401290467742599;
	setAttr ".wl[1238].w[16]" 0.016317430641781056;
	setAttr ".wl[1238].w[18]" 0.02932550115662599;
	setAttr -s 4 ".wl[1239].w";
	setAttr ".wl[1239].w[0]" 0.45044927190264306;
	setAttr ".wl[1239].w[15]" 0.51214162150568887;
	setAttr ".wl[1239].w[16]" 0.013251947344295736;
	setAttr ".wl[1239].w[18]" 0.024157159247372332;
	setAttr -s 4 ".wl[1240].w";
	setAttr ".wl[1240].w[0]" 0.45988816400948085;
	setAttr ".wl[1240].w[15]" 0.024079905548179448;
	setAttr ".wl[1240].w[18]" 0.50357038598376669;
	setAttr ".wl[1240].w[19]" 0.012461544458573037;
	setAttr -s 4 ".wl[1241].w";
	setAttr ".wl[1241].w[0]" 0.45878504099013007;
	setAttr ".wl[1241].w[15]" 0.02900360495249564;
	setAttr ".wl[1241].w[18]" 0.49692440109507269;
	setAttr ".wl[1241].w[19]" 0.015286952962301573;
	setAttr -s 4 ".wl[1242].w";
	setAttr ".wl[1242].w[0]" 0.4763145752570046;
	setAttr ".wl[1242].w[1]" 0.025141514141471998;
	setAttr ".wl[1242].w[15]" 0.25056822971021853;
	setAttr ".wl[1242].w[18]" 0.24797568089130489;
	setAttr -s 4 ".wl[1243].w";
	setAttr ".wl[1243].w[0]" 0.4928254704549358;
	setAttr ".wl[1243].w[15]" 0.49749741594184738;
	setAttr ".wl[1243].w[16]" 0.0037373524993643231;
	setAttr ".wl[1243].w[18]" 0.0059397611038525533;
	setAttr -s 4 ".wl[1244].w";
	setAttr ".wl[1244].w[0]" 0.44113915554853822;
	setAttr ".wl[1244].w[1]" 0.022955858885344193;
	setAttr ".wl[1244].w[15]" 0.27006406490190793;
	setAttr ".wl[1244].w[18]" 0.26584092066420967;
	setAttr -s 4 ".wl[1245].w";
	setAttr ".wl[1245].w[0]" 0.49487395451692351;
	setAttr ".wl[1245].w[15]" 0.0063228910367055862;
	setAttr ".wl[1245].w[18]" 0.49509709026000281;
	setAttr ".wl[1245].w[19]" 0.0037060641863681802;
	setAttr -s 4 ".wl[1246].w";
	setAttr ".wl[1246].w[0]" 0.43465736885765666;
	setAttr ".wl[1246].w[1]" 0.037195593115191716;
	setAttr ".wl[1246].w[15]" 0.26510232857100868;
	setAttr ".wl[1246].w[18]" 0.26304470945614294;
	setAttr -s 4 ".wl[1247].w";
	setAttr ".wl[1247].w[0]" 0.48013341246625452;
	setAttr ".wl[1247].w[15]" 0.49552471141013554;
	setAttr ".wl[1247].w[16]" 0.011917042888712509;
	setAttr ".wl[1247].w[18]" 0.012424833234897438;
	setAttr -s 4 ".wl[1248].w";
	setAttr ".wl[1248].w[0]" 0.40356474001366638;
	setAttr ".wl[1248].w[1]" 0.034081591171594913;
	setAttr ".wl[1248].w[15]" 0.28293217185682423;
	setAttr ".wl[1248].w[18]" 0.27942149695791446;
	setAttr -s 4 ".wl[1249].w";
	setAttr ".wl[1249].w[0]" 0.48417835721168617;
	setAttr ".wl[1249].w[15]" 0.012955656742830206;
	setAttr ".wl[1249].w[18]" 0.49126608282664325;
	setAttr ".wl[1249].w[19]" 0.011599903218840251;
	setAttr -s 4 ".wl[1250].w";
	setAttr ".wl[1250].w[0]" 0.4041648948820637;
	setAttr ".wl[1250].w[1]" 0.029213438731526355;
	setAttr ".wl[1250].w[15]" 0.2824488860065702;
	setAttr ".wl[1250].w[18]" 0.28417278037983978;
	setAttr -s 4 ".wl[1251].w";
	setAttr ".wl[1251].w[0]" 0.41610122972126051;
	setAttr ".wl[1251].w[15]" 0.5581545138881111;
	setAttr ".wl[1251].w[16]" 0.015187369554310123;
	setAttr ".wl[1251].w[18]" 0.010556886836318259;
	setAttr -s 4 ".wl[1252].w";
	setAttr ".wl[1252].w[0]" 0.37908408129495091;
	setAttr ".wl[1252].w[1]" 0.026362820963494168;
	setAttr ".wl[1252].w[15]" 0.2969197003453628;
	setAttr ".wl[1252].w[18]" 0.29763339739619205;
	setAttr -s 4 ".wl[1253].w";
	setAttr ".wl[1253].w[0]" 0.43111234222871175;
	setAttr ".wl[1253].w[15]" 0.010779840622086426;
	setAttr ".wl[1253].w[18]" 0.54363386554455539;
	setAttr ".wl[1253].w[19]" 0.014473951604646536;
	setAttr -s 4 ".wl[1254].w";
	setAttr ".wl[1254].w[0]" 0.4322614471564904;
	setAttr ".wl[1254].w[1]" 0.019653702981091765;
	setAttr ".wl[1254].w[15]" 0.27301949565539679;
	setAttr ".wl[1254].w[18]" 0.27506535420702105;
	setAttr -s 4 ".wl[1255].w";
	setAttr ".wl[1255].w[0]" 0.4091216506163759;
	setAttr ".wl[1255].w[15]" 0.5816847548302948;
	setAttr ".wl[1255].w[16]" 0.0044726206858984778;
	setAttr ".wl[1255].w[18]" 0.0047209738674307998;
	setAttr -s 4 ".wl[1256].w";
	setAttr ".wl[1256].w[0]" 0.40410293470380376;
	setAttr ".wl[1256].w[1]" 0.017594323863448526;
	setAttr ".wl[1256].w[15]" 0.28864084992660344;
	setAttr ".wl[1256].w[18]" 0.28966189150614424;
	setAttr -s 4 ".wl[1257].w";
	setAttr ".wl[1257].w[0]" 0.43123405188330721;
	setAttr ".wl[1257].w[15]" 0.0048488651301346816;
	setAttr ".wl[1257].w[18]" 0.55963287911199255;
	setAttr ".wl[1257].w[19]" 0.0042842038745656407;
	setAttr -s 4 ".wl[1258].w";
	setAttr ".wl[1258].w[0]" 0.48401999910390575;
	setAttr ".wl[1258].w[15]" 0.48401999910390575;
	setAttr ".wl[1258].w[16]" 0.0069379608302089319;
	setAttr ".wl[1258].w[18]" 0.025022040961979533;
	setAttr -s 4 ".wl[1259].w";
	setAttr ".wl[1259].w[0]" 0.48735389489200659;
	setAttr ".wl[1259].w[15]" 0.48735389489200648;
	setAttr ".wl[1259].w[16]" 0.0053682884394555394;
	setAttr ".wl[1259].w[18]" 0.019923921776531382;
	setAttr -s 4 ".wl[1260].w";
	setAttr ".wl[1260].w[0]" 0.48688211291372169;
	setAttr ".wl[1260].w[15]" 0.020952869834381129;
	setAttr ".wl[1260].w[18]" 0.48688211291372169;
	setAttr ".wl[1260].w[19]" 0.0052829043381754457;
	setAttr -s 4 ".wl[1261].w";
	setAttr ".wl[1261].w[0]" 0.48358282159515364;
	setAttr ".wl[1261].w[1]" 0.0069122241219209289;
	setAttr ".wl[1261].w[15]" 0.025922132687771809;
	setAttr ".wl[1261].w[18]" 0.48358282159515364;
	setAttr -s 4 ".wl[1262].w";
	setAttr ".wl[1262].w[0]" 0.4757060269678694;
	setAttr ".wl[1262].w[15]" 0.4757060269678694;
	setAttr ".wl[1262].w[16]" 0.014178002085949796;
	setAttr ".wl[1262].w[18]" 0.034409943978311403;
	setAttr -s 4 ".wl[1263].w";
	setAttr ".wl[1263].w[0]" 0.47956529116954782;
	setAttr ".wl[1263].w[15]" 0.47956529116954782;
	setAttr ".wl[1263].w[16]" 0.011756348341104141;
	setAttr ".wl[1263].w[18]" 0.029113069319800178;
	setAttr -s 4 ".wl[1264].w";
	setAttr ".wl[1264].w[0]" 0.4792140356695791;
	setAttr ".wl[1264].w[15]" 0.030148563389424371;
	setAttr ".wl[1264].w[18]" 0.4792140356695791;
	setAttr ".wl[1264].w[19]" 0.011423365271417353;
	setAttr -s 4 ".wl[1265].w";
	setAttr ".wl[1265].w[0]" 0.47552558972794057;
	setAttr ".wl[1265].w[15]" 0.035262934465188848;
	setAttr ".wl[1265].w[18]" 0.47552558972794057;
	setAttr ".wl[1265].w[19]" 0.013685886078930135;
	setAttr -s 4 ".wl[1266].w";
	setAttr ".wl[1266].w[0]" 0.45810125329003903;
	setAttr ".wl[1266].w[15]" 0.49013718741686907;
	setAttr ".wl[1266].w[16]" 0.01893023689251273;
	setAttr ".wl[1266].w[18]" 0.032831322400579128;
	setAttr -s 4 ".wl[1267].w";
	setAttr ".wl[1267].w[0]" 0.46011415738984912;
	setAttr ".wl[1267].w[15]" 0.49657491779326951;
	setAttr ".wl[1267].w[16]" 0.015708052718760641;
	setAttr ".wl[1267].w[18]" 0.027602872098120766;
	setAttr -s 4 ".wl[1268].w";
	setAttr ".wl[1268].w[0]" 0.46681470376682266;
	setAttr ".wl[1268].w[15]" 0.027741612124245509;
	setAttr ".wl[1268].w[18]" 0.49056233809377409;
	setAttr ".wl[1268].w[19]" 0.014881346015157707;
	setAttr -s 4 ".wl[1269].w";
	setAttr ".wl[1269].w[0]" 0.46422646312542376;
	setAttr ".wl[1269].w[15]" 0.032735897968748325;
	setAttr ".wl[1269].w[18]" 0.48517206418351849;
	setAttr ".wl[1269].w[19]" 0.017865574722309416;
	setAttr -s 4 ".wl[1270].w";
	setAttr ".wl[1270].w[0]" 0.46952636191170627;
	setAttr ".wl[1270].w[15]" 0.49730967308920987;
	setAttr ".wl[1270].w[16]" 0.0093059389019976291;
	setAttr ".wl[1270].w[18]" 0.023858026097086236;
	setAttr -s 4 ".wl[1271].w";
	setAttr ".wl[1271].w[0]" 0.47068839252558509;
	setAttr ".wl[1271].w[15]" 0.50332270495124765;
	setAttr ".wl[1271].w[16]" 0.0071744617900965282;
	setAttr ".wl[1271].w[18]" 0.018814440733070679;
	setAttr -s 4 ".wl[1272].w";
	setAttr ".wl[1272].w[0]" 0.47811976960854657;
	setAttr ".wl[1272].w[15]" 0.018828110995729432;
	setAttr ".wl[1272].w[18]" 0.49628962030095219;
	setAttr ".wl[1272].w[19]" 0.006762499094771857;
	setAttr -s 4 ".wl[1273].w";
	setAttr ".wl[1273].w[0]" 0.47604365156783063;
	setAttr ".wl[1273].w[15]" 0.023652827231658239;
	setAttr ".wl[1273].w[18]" 0.49157998900620659;
	setAttr ".wl[1273].w[19]" 0.0087235321943046045;
	setAttr -s 4 ".wl[1274].w[7:10]"  8.8678894464347649e-05 0.00073355017509762392 
		0.49958888546521901 0.49958888546521901;
	setAttr -s 4 ".wl[1275].w[7:10]"  0.00013803442092501963 0.0011261297152962927 
		0.49936791793188934 0.49936791793188934;
	setAttr -s 4 ".wl[1276].w[7:10]"  0.00013084886121287318 0.0010749417172289076 
		0.49939710471077914 0.49939710471077914;
	setAttr -s 4 ".wl[1277].w[7:10]"  0.00010494478830010959 0.00086816257415269112 
		0.49951344631877359 0.49951344631877359;
	setAttr -s 4 ".wl[1278].w[7:10]"  0.00013612828267655042 0.0011162670460354248 
		0.49937380233564399 0.49937380233564399;
	setAttr -s 4 ".wl[1279].w[7:10]"  0.000163856457226124 0.0013281283199221827 
		0.49925400761142591 0.4992540076114258;
	setAttr -s 4 ".wl[1280].w[7:10]"  0.00011132850468516519 0.00091470114845544539 
		0.49948698517342965 0.49948698517342965;
	setAttr -s 4 ".wl[1281].w[7:10]"  6.3623460202303657e-05 0.00052975044077048679 
		0.4997033130495136 0.4997033130495136;
	setAttr -s 4 ".wl[1282].w[7:10]"  3.2435077645714657e-05 0.00029437343295355019 
		0.49983659574470041 0.49983659574470041;
	setAttr -s 4 ".wl[1283].w[7:10]"  6.3791240545359645e-05 0.00057359243387913373 
		0.49968130816278772 0.49968130816278772;
	setAttr -s 4 ".wl[1284].w[7:10]"  5.5368815989748545e-05 0.00050123516299943886 
		0.49972169801050553 0.49972169801050531;
	setAttr -s 4 ".wl[1285].w[7:10]"  4.0792877404956854e-05 0.00037180883024694737 
		0.49979369914617411 0.499793699146174;
	setAttr -s 4 ".wl[1286].w[7:10]"  7.4504034524083465e-05 0.00066954351006741537 
		0.49962797622770427 0.49962797622770427;
	setAttr -s 4 ".wl[1287].w[7:10]"  9.348032811899601e-05 0.00083193571712408549 
		0.49953729197737851 0.49953729197737839;
	setAttr -s 4 ".wl[1288].w[7:10]"  4.7402222324644837e-05 0.00042707060674079724 
		0.49976276358546734 0.49976276358546723;
	setAttr -s 4 ".wl[1289].w[7:10]"  1.7401585625409225e-05 0.0001591243741572933 
		0.49991173702010872 0.4999117370201086;
	setAttr -s 4 ".wl[1290].w[7:10]"  4.2767694888891131e-05 0.00038876654889901448 
		0.49978423287810608 0.49978423287810608;
	setAttr -s 4 ".wl[1291].w[7:10]"  4.0337546737165785e-05 0.00036816437761800163 
		0.49979574903782242 0.49979574903782242;
	setAttr -s 4 ".wl[1292].w[7:10]"  3.3447900359505247e-05 0.00030647203814373352 
		0.49983004003074838 0.49983004003074838;
	setAttr -s 4 ".wl[1293].w[7:10]"  6.2495619139090139e-05 0.00056334059175756315 
		0.49968708189455163 0.49968708189455163;
	setAttr -s 4 ".wl[1294].w[7:10]"  5.3549042615081065e-05 0.00048584131079083056 
		0.49973030482329706 0.49973030482329706;
	setAttr -s 4 ".wl[1295].w[7:10]"  3.47784690532289e-05 0.00031619549743031576 
		0.49982451301675823 0.49982451301675823;
	setAttr -s 4 ".wl[1296].w[7:10]"  1.5874368620439041e-05 0.00014583779813044003 
		0.49991914391662456 0.49991914391662456;
	setAttr -s 4 ".wl[1297].w[7:10]"  2.4318327902220506e-05 0.00022241910265323472 
		0.49987663128472226 0.49987663128472226;
	setAttr -s 4 ".wl[1298].w[7:10]"  2.0917343780920841e-05 0.00019987544741874874 
		0.49988960360440016 0.49988960360440016;
	setAttr -s 4 ".wl[1299].w[7:10]"  8.0626262898815433e-06 7.8151962637589451e-05 
		0.49995689270553628 0.49995689270553628;
	setAttr -s 4 ".wl[1300].w[7:10]"  2.2874474028426224e-05 0.00021823019361986038 
		0.49987944766617581 0.49987944766617581;
	setAttr -s 4 ".wl[1301].w[7:10]"  1.7517742177836155e-05 0.00016834092391979181 
		0.49990707066695123 0.49990707066695123;
	setAttr -s 4 ".wl[1302].w[7:10]"  3.3468448763812974e-05 0.00031738738597603502 
		0.49982457208263009 0.49982457208263009;
	setAttr -s 4 ".wl[1303].w[7:10]"  3.2352728095842045e-05 0.00030690976045446666 
		0.4998303687557249 0.4998303687557249;
	setAttr -s 4 ".wl[1304].w[7:10]"  1.9220478848833102e-05 0.0001826935009644788 
		0.49989904301009336 0.49989904301009336;
	setAttr -s 4 ".wl[1305].w[7:10]"  6.687210813078123e-06 6.4420034488004598e-05 
		0.49996444637734944 0.49996444637734944;
	setAttr -s 4 ".wl[1306].w[7:10]"  1.2106484062731133e-05 0.00011572949767966441 
		0.49993608200912887 0.49993608200912876;
	setAttr -s 4 ".wl[1307].w[7:10]"  4.1188204375280662e-05 0.00036643945117000991 
		0.49979618617222743 0.49979618617222743;
	setAttr -s 4 ".wl[1308].w[7:10]"  2.3715224983373127e-05 0.00021258117742071591 
		0.49988185179879796 0.49988185179879796;
	setAttr -s 4 ".wl[1309].w[7:10]"  5.7892934452633051e-05 0.00051131837887413386 
		0.49971539434333667 0.49971539434333667;
	setAttr -s 4 ".wl[1310].w[7:10]"  0.00010845471977329593 0.00094623476094015725 
		0.49947265525964329 0.49947265525964329;
	setAttr -s 4 ".wl[1311].w[7:10]"  8.7516969195406175e-05 0.00077103534349848236 
		0.49957072384365309 0.49957072384365309;
	setAttr -s 4 ".wl[1312].w[7:10]"  5.0197697084485418e-05 0.00044853046181118873 
		0.49975063592055224 0.49975063592055213;
	setAttr -s 4 ".wl[1313].w[7:10]"  6.6640235187964019e-05 0.00059139375600376269 
		0.4996709830044041 0.4996709830044041;
	setAttr -s 4 ".wl[1314].w[7:10]"  7.623028222553772e-05 0.00067192964281726181 
		0.49962592003747863 0.49962592003747863;
	setAttr -s 4 ".wl[1315].w[7:10]"  3.5744856041834629e-05 0.00033024137306129474 
		0.4998170068854485 0.49981700688544839;
	setAttr -s 4 ".wl[1316].w[7:10]"  1.9322836186285963e-05 0.00017961017721341836 
		0.49990053349330021 0.4999005334933001;
	setAttr -s 4 ".wl[1317].w[7:10]"  1.2060232219294956e-05 0.00011259563051801207 
		0.49993767206863138 0.49993767206863138;
	setAttr -s 4 ".wl[1318].w[7:10]"  2.8685287246506082e-05 0.00026504957627750098 
		0.49985313256823805 0.49985313256823805;
	setAttr -s 4 ".wl[1319].w[7:10]"  5.3819261084354305e-05 0.00049306628536358542 
		0.49972655722677606 0.49972655722677606;
	setAttr -s 4 ".wl[1320].w[7:10]"  4.581759070650087e-05 0.00042246464008361859 
		0.49976585888460495 0.49976585888460495;
	setAttr -s 4 ".wl[1321].w[7:10]"  2.7693209850761697e-05 0.00025785667042447385 
		0.49985722505986235 0.49985722505986235;
	setAttr -s 4 ".wl[1322].w[7:10]"  3.3735137012703288e-05 0.00031291699205554622 
		0.49982667393546587 0.49982667393546587;
	setAttr -s 4 ".wl[1323].w[7:10]"  5.208968814644187e-05 0.00047097234161695977 
		0.49973846898511826 0.49973846898511826;
	setAttr -s 4 ".wl[1324].w[7:10]"  2.7972545200264271e-05 0.00025484715505299756 
		0.49985859014987327 0.49985859014987349;
	setAttr -s 4 ".wl[1325].w[7:10]"  1.6617201287716587e-05 0.00015228260842657887 
		0.49991555009514288 0.49991555009514288;
	setAttr -s 4 ".wl[1326].w[7:10]"  4.0504881381144003e-05 0.00036658371771778312 
		0.49979645570045056 0.49979645570045056;
	setAttr -s 4 ".wl[1327].w[7:10]"  7.6345656191395073e-05 0.00068386980441606244 
		0.49961989226969628 0.49961989226969628;
	setAttr -s 4 ".wl[1328].w[7:10]"  6.3115247498842663e-05 0.00056990132438257109 
		0.49968349171405929 0.49968349171405929;
	setAttr -s 4 ".wl[1329].w[7:10]"  3.6956640781889867e-05 0.00033768021703476709 
		0.49981268157109171 0.49981268157109171;
	setAttr -s 4 ".wl[1330].w[7:10]"  4.7174792722102837e-05 0.0004287981815572962 
		0.49976201351286026 0.49976201351286026;
	setAttr -s 4 ".wl[1331].w[7:10]"  8.4888583482474066e-05 0.00069422912764922169 
		0.4996104411444342 0.4996104411444342;
	setAttr -s 4 ".wl[1332].w[7:10]"  0.0014526497504424934 0.0083806824369341854 
		0.49508333390631165 0.49508333390631165;
	setAttr -s 4 ".wl[1333].w[7:10]"  0.0014244776974978949 0.0082261184797878736 
		0.49517470191135715 0.49517470191135715;
	setAttr -s 4 ".wl[1334].w[7:10]"  0.0014826035171477941 0.0085623108145075812 
		0.49497754283417233 0.49497754283417233;
	setAttr -s 4 ".wl[1335].w[7:10]"  0.0014551104528704117 0.0084126078800005593 
		0.49506614083356454 0.49506614083356454;
	setAttr -s 4 ".wl[1336].w[7:10]"  0.0013576286075895232 0.0078902003763094749 
		0.49537608550805051 0.49537608550805051;
	setAttr -s 4 ".wl[1337].w[7:10]"  0.0013789758465048447 0.0080080650615866576 
		0.49530647954595425 0.49530647954595425;
	setAttr -s 4 ".wl[1338].w[7:10]"  0.00022846185757165123 0.001727502432601937 
		0.49902201785491318 0.49902201785491318;
	setAttr -s 4 ".wl[1339].w[7:10]"  0.00015944748882940611 0.0012209167432048632 
		0.49930981788398282 0.49930981788398282;
	setAttr -s 4 ".wl[1340].w[7:10]"  0.00020193039743752588 0.0015298559209088944 
		0.49913410684082665 0.49913410684082687;
	setAttr -s 4 ".wl[1341].w[7:10]"  0.00016851997446981994 0.0012832272738385357 
		0.49927412637584584 0.49927412637584584;
	setAttr -s 4 ".wl[1342].w[7:10]"  0.00027162172379603498 0.0020377329330516533 
		0.49884532267157616 0.49884532267157616;
	setAttr -s 4 ".wl[1343].w[7:10]"  0.00023342043885067387 0.0017575512564175441 
		0.49900451415236591 0.49900451415236591;
	setAttr -s 4 ".wl[1344].w[7:10]"  0.00093860304690388668 0.0058162488295233272 
		0.49662257406178639 0.49662257406178639;
	setAttr -s 4 ".wl[1345].w[7:10]"  0.00081849577656594396 0.0051167328793751939 
		0.49703238567202945 0.49703238567202945;
	setAttr -s 4 ".wl[1346].w[7:10]"  0.0008933191756534775 0.0055429333893072096 
		0.4967818737175197 0.4967818737175197;
	setAttr -s 4 ".wl[1347].w[7:10]"  0.00083470174245874192 0.0051969671751888359 
		0.49698416554117636 0.49698416554117614;
	setAttr -s 4 ".wl[1348].w[7:10]"  0.0010118239651492791 0.0062305664051098596 
		0.49637880481487051 0.49637880481487051;
	setAttr -s 4 ".wl[1349].w[7:10]"  0.00094896571019442797 0.0058592237917643175 
		0.49659590524902064 0.49659590524902064;
	setAttr -s 4 ".wl[1350].w[7:10]"  0.0014436261446318952 0.0083127581753424194 
		0.49512180784001286 0.49512180784001286;
	setAttr -s 4 ".wl[1351].w[7:10]"  0.0014085410342016228 0.0080947810397353822 
		0.49524833896303155 0.49524833896303144;
	setAttr -s 4 ".wl[1352].w[7:10]"  0.0014052638142062353 0.0080983866600073447 
		0.49524817476289323 0.49524817476289323;
	setAttr -s 4 ".wl[1353].w[7:10]"  0.0014018687236706697 0.0080446540496848917 
		0.49527673861332222 0.49527673861332222;
	setAttr -s 4 ".wl[1354].w[7:10]"  0.0015164405813521485 0.0086888135978612484 
		0.49489737291039337 0.49489737291039337;
	setAttr -s 4 ".wl[1355].w[7:10]"  0.0014656595984648456 0.0084111907168416856 
		0.49506157484234675 0.49506157484234675;
	setAttr -s 4 ".wl[1356].w[7:10]"  0.00022873620946720584 0.0017064540237161304 
		0.49903240488340839 0.49903240488340839;
	setAttr -s 4 ".wl[1357].w[7:10]"  0.00023979291604267006 0.001785585072402841 
		0.49898731100577726 0.49898731100577726;
	setAttr -s 4 ".wl[1358].w[7:10]"  0.00029760013042591412 0.002197584722317751 
		0.49875240757362821 0.49875240757362821;
	setAttr -s 4 ".wl[1359].w[7:10]"  0.00033928860045091683 0.0024949584768121603 
		0.49858287646136851 0.49858287646136851;
	setAttr -s 4 ".wl[1360].w[7:10]"  0.00028701366077514274 0.0021268067656823619 
		0.49879308978677123 0.49879308978677123;
	setAttr -s 4 ".wl[1361].w[7:10]"  0.00025815052220471454 0.00192004169952316 
		0.49891090388913606 0.49891090388913606;
	setAttr -s 4 ".wl[1362].w[7:10]"  0.00095081857931949372 0.005860765128356973 
		0.49659420814616184 0.49659420814616184;
	setAttr -s 4 ".wl[1363].w[7:10]"  0.00097702130249203317 0.0060112944407666414 
		0.49650584212837068 0.49650584212837068;
	setAttr -s 4 ".wl[1364].w[7:10]"  0.0010528504700176831 0.006445863999367113 
		0.49625064276530761 0.49625064276530761;
	setAttr -s 4 ".wl[1365].w[7:10]"  0.0011084077756268048 0.0067663316515027064 
		0.49606263028643527 0.49606263028643527;
	setAttr -s 4 ".wl[1366].w[7:10]"  0.0010288678203676403 0.0063148273512600951 
		0.49632815241418615 0.49632815241418615;
	setAttr -s 4 ".wl[1367].w[7:10]"  0.00098605215723527716 0.0060677577709391016 
		0.49647309503591286 0.49647309503591286;
	setAttr -s 4 ".wl[1368].w[7:10]"  0.00011244327466443595 0.00092839878487285625 
		0.49947957897023137 0.49947957897023137;
	setAttr -s 4 ".wl[1369].w[7:10]"  0.00011551295595104781 0.00095269380375032011 
		0.49946589662014934 0.49946589662014934;
	setAttr -s 4 ".wl[1370].w[7:10]"  1.9990835408294313e-05 0.00018254557411914146 
		0.49989873179523636 0.49989873179523625;
	setAttr -s 4 ".wl[1371].w[7:10]"  4.9945545173276054e-05 0.00045052000290088606 
		0.49974976722596298 0.49974976722596287;
	setAttr -s 4 ".wl[1372].w[7:10]"  6.4602196711959102e-05 0.00058190434638651892 
		0.49967674672845075 0.49967674672845075;
	setAttr -s 4 ".wl[1373].w[7:10]"  4.3513619516734531e-05 0.00039610629879051237 
		0.49978019004084639 0.49978019004084639;
	setAttr -s 4 ".wl[1374].w[7:10]"  5.240737692986372e-05 0.00047517959146905624 
		0.49973620651580053 0.49973620651580053;
	setAttr -s 4 ".wl[1375].w[7:10]"  9.1910095003333887e-05 0.00082005563464666192 
		0.49954401713517499 0.49954401713517499;
	setAttr -s 4 ".wl[1376].w[7:10]"  7.4237384935358519e-05 0.00066330287387729296 
		0.49963122987059372 0.49963122987059361;
	setAttr -s 4 ".wl[1377].w[7:10]"  2.6146246989110577e-05 0.00023780717995339619 
		0.4998680232865288 0.49986802328652868;
	setAttr -s 4 ".wl[1378].w[7:10]"  5.8057356994218871e-05 0.00052372971165472572 
		0.49970910646567551 0.49970910646567551;
	setAttr -s 4 ".wl[1379].w[7:10]"  5.1295753930807544e-05 0.00046557073354088787 
		0.49974156675626413 0.49974156675626413;
	setAttr -s 4 ".wl[1380].w[7:10]"  4.4327304749008531e-05 0.00040334367915714872 
		0.49977616450804691 0.49977616450804691;
	setAttr -s 4 ".wl[1381].w[7:10]"  3.8764208681413245e-05 0.0003539891769166618 
		0.49980362330720102 0.49980362330720102;
	setAttr -s 4 ".wl[1382].w[7:10]"  3.4402849934179427e-05 0.00031505783818429572 
		0.49982526965594076 0.49982526965594076;
	setAttr -s 4 ".wl[1383].w[7:10]"  8.523584577941934e-05 0.0007613356520644604 
		0.49957671425107802 0.49957671425107802;
	setAttr -s 4 ".wl[1384].w[7:10]"  6.8953793611453438e-05 0.00062145818944747775 
		0.49965479400847052 0.49965479400847052;
	setAttr -s 4 ".wl[1385].w[7:10]"  4.0795153570905089e-05 0.00037239481287072517 
		0.49979340501677921 0.49979340501677921;
	setAttr -s 4 ".wl[1386].w[7:10]"  6.2664676779824099e-05 0.00056579788589961185 
		0.49968576871866027 0.49968576871866027;
	setAttr -s 4 ".wl[1387].w[7:10]"  4.3931605638705226e-05 0.00039690848452687686 
		0.49977957995491729 0.49977957995491717;
	setAttr -s 4 ".wl[1388].w[7:10]"  5.0778261176783794e-05 0.00045900403353118033 
		0.49974510885264617 0.49974510885264595;
	setAttr -s 4 ".wl[1389].w[7:10]"  1.6820519243875867e-05 0.00015408317337571608 
		0.49991454815369024 0.49991454815369024;
	setAttr -s 4 ".wl[1390].w[7:10]"  2.1714872660063049e-05 0.0001987260670461793 
		0.49988977953014685 0.49988977953014685;
	setAttr -s 4 ".wl[1391].w[7:10]"  3.0108301992120544e-05 0.00027393976351201816 
		0.49984797596724795 0.49984797596724795;
	setAttr -s 4 ".wl[1392].w[7:10]"  1.7128056374872099e-05 0.00015725657897181473 
		0.49991280768232671 0.49991280768232671;
	setAttr -s 4 ".wl[1393].w[7:10]"  3.4434020125626483e-05 0.00031368547986914357 
		0.4998259402500026 0.4998259402500026;
	setAttr -s 4 ".wl[1394].w[7:10]"  3.950356103972474e-05 0.00036125814976632428 
		0.49979961914459697 0.49979961914459697;
	setAttr -s 4 ".wl[1395].w[7:10]"  1.0363932113934534e-05 0.00010024118854328129 
		0.4999446974396714 0.4999446974396714;
	setAttr -s 4 ".wl[1396].w[7:10]"  3.7409069510032015e-05 0.00034344734600160707 
		0.49980957179224417 0.49980957179224417;
	setAttr -s 4 ".wl[1397].w[7:10]"  2.3116527973492356e-05 0.00022097966374793452 
		0.49987795190413925 0.49987795190413925;
	setAttr -s 4 ".wl[1398].w[7:10]"  3.1080756599272363e-05 0.00028642021901350846 
		0.49984124951219361 0.49984124951219361;
	setAttr -s 4 ".wl[1399].w[7:10]"  1.8568672756199923e-05 0.00017829907285397126 
		0.49990156612719494 0.49990156612719494;
	setAttr -s 4 ".wl[1400].w[7:10]"  1.1808290835427935e-05 0.00011428347919311859 
		0.49993695411498557 0.49993695411498579;
	setAttr -s 4 ".wl[1401].w[7:10]"  5.8272235405097312e-05 0.0005284815525675975 
		0.49970662310601366 0.49970662310601366;
	setAttr -s 4 ".wl[1402].w[7:10]"  1.5321591874641415e-05 0.00014754414252574299 
		0.49991856713279981 0.49991856713279981;
	setAttr -s 4 ".wl[1403].w[7:10]"  4.9993124017194477e-05 0.0004562799450727228 
		0.49974686346545505 0.49974686346545505;
	setAttr -s 4 ".wl[1404].w[7:10]"  2.3671078952843609e-05 0.00022649980207657412 
		0.49987491455948513 0.49987491455948535;
	setAttr -s 4 ".wl[1405].w[7:10]"  3.5096114982139252e-05 0.0003331574433265127 
		0.4998158732208457 0.4998158732208457;
	setAttr -s 4 ".wl[1406].w[7:10]"  3.2122490665137618e-05 0.00029378257578575791 
		0.49983704746677454 0.49983704746677454;
	setAttr -s 4 ".wl[1407].w[7:10]"  2.6036554721348459e-05 0.00024746606223633442 
		0.49986324869152116 0.49986324869152116;
	setAttr -s 4 ".wl[1408].w[7:10]"  1.4425959188949782e-05 0.00013329059869306647 
		0.49992614172105898 0.49992614172105898;
	setAttr -s 4 ".wl[1409].w[7:10]"  1.1088406157696698e-05 0.00010634997733846241 
		0.49994128080825179 0.49994128080825201;
	setAttr -s 4 ".wl[1410].w[7:10]"  6.3443957302302542e-06 6.1489425674099004e-05 
		0.49996608308929785 0.49996608308929785;
	setAttr -s 4 ".wl[1411].w[7:10]"  2.2221318890178572e-05 0.00020443362774649298 
		0.49988667252668173 0.49988667252668162;
	setAttr -s 4 ".wl[1412].w[7:10]"  7.6960837100490147e-06 7.4072814441151352e-05 
		0.49995911555092454 0.49995911555092432;
	setAttr -s 4 ".wl[1413].w[7:10]"  1.5894366477432406e-05 0.00015213077932547619 
		0.49991598742709853 0.49991598742709853;
	setAttr -s 4 ".wl[1414].w[7:10]"  3.4705183882714442e-05 0.00031352467313222409 
		0.49982588507149239 0.49982588507149261;
	setAttr -s 4 ".wl[1415].w[7:10]"  9.8622689440932572e-05 0.00087339746547985067 
		0.49951398992253965 0.49951398992253965;
	setAttr -s 4 ".wl[1416].w[7:10]"  7.8492414646086014e-05 0.00070207376229539369 
		0.49960971691152928 0.49960971691152928;
	setAttr -s 4 ".wl[1417].w[7:10]"  0.00010662406063893459 0.00093265123619160051 
		0.49948036235158477 0.49948036235158477;
	setAttr -s 4 ".wl[1418].w[7:10]"  4.308909431670914e-05 0.00039099772024635645 
		0.49978295659271854 0.49978295659271843;
	setAttr -s 4 ".wl[1419].w[7:10]"  6.3127978751542358e-05 0.00056113277467008513 
		0.4996878696232892 0.4996878696232892;
	setAttr -s 4 ".wl[1420].w[7:10]"  5.8611138295855133e-05 0.00052813193611037557 
		0.49970662846279684 0.49970662846279684;
	setAttr -s 4 ".wl[1421].w[7:10]"  5.3317172908030915e-05 0.00047579704452193955 
		0.49973544289128502 0.49973544289128502;
	setAttr -s 4 ".wl[1422].w[7:10]"  6.7743583798195568e-05 0.00060620247940201084 
		0.49966302696839987 0.49966302696839987;
	setAttr -s 4 ".wl[1423].w[7:10]"  7.7000621839062394e-05 0.00067991974696155161 
		0.4996215398155997 0.4996215398155997;
	setAttr -s 4 ".wl[1424].w[7:10]"  6.0916344741505475e-05 0.00053864189958043409 
		0.49970022087783905 0.49970022087783905;
	setAttr -s 4 ".wl[1425].w[7:10]"  3.0592125603076105e-05 0.00028718151375093146 
		0.49984111318032298 0.49984111318032298;
	setAttr -s 4 ".wl[1426].w[7:10]"  1.544388496851577e-05 0.00014588496649200515 
		0.49991933557426982 0.49991933557426971;
	setAttr -s 4 ".wl[1427].w[7:10]"  2.8256209973966713e-05 0.00026161308533291213 
		0.49985506535234658 0.49985506535234658;
	setAttr -s 4 ".wl[1428].w[7:10]"  8.9741737213074292e-06 8.5154452063366563e-05 
		0.49995293568710769 0.49995293568710769;
	setAttr -s 4 ".wl[1429].w[7:10]"  1.3108431117229534e-05 0.00012230738738472911 
		0.49993229209074908 0.49993229209074896;
	setAttr -s 4 ".wl[1430].w[7:10]"  2.4009395440317166e-05 0.00022542461153947973 
		0.49987528299651018 0.49987528299651007;
	setAttr -s 4 ".wl[1431].w[7:10]"  1.7128757615679833e-05 0.00015930347095875365 
		0.49991178388571278 0.49991178388571278;
	setAttr -s 4 ".wl[1432].w[7:10]"  4.7646008488258284e-05 0.00044347031306963126 
		0.49975444183922108 0.49975444183922108;
	setAttr -s 4 ".wl[1433].w[7:10]"  4.3110680319172442e-05 0.00039606238372146137 
		0.49978041346797969 0.49978041346797969;
	setAttr -s 4 ".wl[1434].w[7:10]"  4.0052445743616194e-05 0.00037524684942945205 
		0.49979235035241348 0.49979235035241348;
	setAttr -s 4 ".wl[1435].w[7:10]"  5.4052259129116748e-05 0.00049600758029801116 
		0.49972497008028649 0.49972497008028649;
	setAttr -s 4 ".wl[1436].w[7:10]"  2.3090999235570634e-05 0.00021850742610091039 
		0.49987920078733183 0.49987920078733172;
	setAttr -s 4 ".wl[1437].w[7:10]"  3.4302231192423595e-05 0.000318207508985417 
		0.49982374512991107 0.49982374512991107;
	setAttr -s 4 ".wl[1438].w[7:10]"  2.8708378863586137e-05 0.0002705993978819076 
		0.49985034611162726 0.49985034611162726;
	setAttr -s 4 ".wl[1439].w[7:10]"  2.8497139976861335e-05 0.00026520978978417846 
		0.49985314653511947 0.49985314653511947;
	setAttr -s 4 ".wl[1440].w[7:10]"  3.7227181776786462e-05 0.00034426974205306214 
		0.49980925153808503 0.49980925153808503;
	setAttr -s 4 ".wl[1441].w[7:10]"  4.6949543927618868e-05 0.00042538091757894273 
		0.49976383476924674 0.49976383476924674;
	setAttr -s 4 ".wl[1442].w[7:10]"  2.6135570895851002e-05 0.00023839808685958218 
		0.49986773317112232 0.49986773317112232;
	setAttr -s 4 ".wl[1443].w[7:10]"  4.1325838824537704e-05 0.00037463191189935767 
		0.49979202112463805 0.49979202112463805;
	setAttr -s 4 ".wl[1444].w[7:10]"  1.646069986870599e-05 0.00015090369982108504 
		0.49991631780015511 0.49991631780015511;
	setAttr -s 4 ".wl[1445].w[7:10]"  1.8461483737195901e-05 0.00016901882743512991 
		0.49990625984441389 0.49990625984441389;
	setAttr -s 4 ".wl[1446].w[7:10]"  3.7504684326792375e-05 0.00033997192229761469 
		0.49981126169668783 0.49981126169668783;
	setAttr -s 4 ".wl[1447].w[7:10]"  2.3786463854175914e-05 0.00021698904118898794 
		0.49987961224747846 0.49987961224747834;
	setAttr -s 4 ".wl[1448].w[7:10]"  6.8598566964622409e-05 0.00061608756478427585 
		0.49965765693412556 0.49965765693412556;
	setAttr -s 4 ".wl[1449].w[7:10]"  6.1289563447894382e-05 0.00055084424709755204 
		0.49969393309472726 0.49969393309472726;
	setAttr -s 4 ".wl[1450].w[7:10]"  5.7932716535251997e-05 0.00052400893937979028 
		0.49970902917204246 0.49970902917204246;
	setAttr -s 4 ".wl[1451].w[7:10]"  7.5820738625436505e-05 0.00068057585720566669 
		0.49962180170208442 0.49962180170208442;
	setAttr -s 4 ".wl[1452].w[7:10]"  3.5301516446594507e-05 0.00032273117447265874 
		0.49982098365454036 0.49982098365454036;
	setAttr -s 4 ".wl[1453].w[7:10]"  4.6232758866977586e-05 0.00042056569286841815 
		0.49976660077413232 0.49976660077413232;
	setAttr -s 4 ".wl[1454].w[7:10]"  4.3545847187192426e-05 0.00039634268508413774 
		0.49978005573386441 0.4997800557338643;
	setAttr -s 4 ".wl[1455].w[7:10]"  3.8670113963683814e-05 0.00035303447510172739 
		0.49980414770546733 0.49980414770546733;
	setAttr -s 4 ".wl[1456].w[7:10]"  5.3389363729324112e-05 0.00048337131456171681 
		0.4997316196608545 0.4997316196608545;
	setAttr -s 4 ".wl[1457].w[7:10]"  8.8636142588253226e-05 0.00076047516689345287 
		0.49957544434525919 0.49957544434525919;
	setAttr -s 4 ".wl[1458].w[7:10]"  6.998923273940835e-05 0.00060437760314301201 
		0.4996628165820588 0.4996628165820588;
	setAttr -s 4 ".wl[1459].w[7:10]"  0.00011193059098086488 0.00095361176424344136 
		0.49946722882238792 0.49946722882238781;
	setAttr -s 4 ".wl[1460].w[7:10]"  0.00014985166359805503 0.0012231943935200624 
		0.49931347697144096 0.49931347697144096;
	setAttr -s 4 ".wl[1461].w[7:10]"  0.00013483181706312297 0.001138085188787021 
		0.4993635414970749 0.4993635414970749;
	setAttr -s 4 ".wl[1462].w[7:10]"  0.00016015993770760359 0.0012680726016783249 
		0.49928588373030702 0.49928588373030702;
	setAttr -s 4 ".wl[1463].w[7:10]"  0.0002001817690098592 0.0015719772389180973 
		0.49911392049603603 0.49911392049603603;
	setAttr -s 4 ".wl[1464].w[7:10]"  0.0014919424278236539 0.0085893252799895022 
		0.49495936614609343 0.49495936614609343;
	setAttr -s 4 ".wl[1465].w[7:10]"  0.00013832114915345145 0.0010967593000140595 
		0.49938245977541629 0.49938245977541629;
	setAttr -s 4 ".wl[1466].w[7:10]"  0.00015556082404179057 0.0012317013775731583 
		0.4993063688991925 0.4993063688991925;
	setAttr -s 4 ".wl[1467].w[7:10]"  0.0014023412110396964 0.0081093153808374629 
		0.49524417170406138 0.49524417170406138;
	setAttr -s 4 ".wl[1468].w[7:10]"  0.0014286813038197972 0.0082525486460551783 
		0.49515938502506246 0.49515938502506246;
	setAttr -s 4 ".wl[1469].w[7:10]"  3.3868034388812234e-05 0.0003019686453144391 
		0.49983208166014842 0.49983208166014842;
	setAttr -s 4 ".wl[1470].w[7:10]"  7.8730167429344111e-05 0.00065236294193659629 
		0.49963445344531704 0.49963445344531704;
	setAttr -s 4 ".wl[1471].w[7:10]"  3.8498568522549211e-05 0.00033345657854767812 
		0.49981402242646489 0.49981402242646489;
	setAttr -s 4 ".wl[1472].w[7:10]"  2.6798529275466736e-05 0.00023988175535330608 
		0.49986665985768558 0.49986665985768558;
	setAttr -s 4 ".wl[1473].w[7:10]"  5.9468006854465983e-05 0.00051143538827549333 
		0.499714548302435 0.499714548302435;
	setAttr -s 4 ".wl[1474].w[7:10]"  1.8702767194048489e-05 0.00017026895326078664 
		0.49990551413977258 0.49990551413977258;
	setAttr -s 4 ".wl[1475].w[7:10]"  5.0326330844559369e-05 0.00045129436646886917 
		0.4997491896513433 0.4997491896513433;
	setAttr -s 4 ".wl[1476].w[7:10]"  0.00014149541458190369 0.0011580357748382725 
		0.49935023440528975 0.49935023440528997;
	setAttr -s 4 ".wl[1477].w[7:10]"  8.7521272422880198e-05 0.0007666139484120791 
		0.4995729323895825 0.4995729323895825;
	setAttr -s 4 ".wl[1478].w[7:10]"  0.00014818433236699305 0.0012086366311387681 
		0.49932158951824707 0.49932158951824707;
	setAttr -s 4 ".wl[1479].w[7:10]"  7.8782054246659457e-05 0.000672788194377585 
		0.49962421487568787 0.49962421487568787;
	setAttr -s 4 ".wl[1480].w[7:10]"  9.9324736533819272e-05 0.00084671270818378614 
		0.49952698127764134 0.49952698127764111;
	setAttr -s 4 ".wl[1481].w[7:10]"  0.00011441600206627285 0.00094143636729046012 
		0.4994720738153216 0.4994720738153216;
	setAttr -s 4 ".wl[1482].w[7:10]"  0.0015159365343387092 0.0087139451448284104 
		0.49488505916041642 0.49488505916041642;
	setAttr -s 4 ".wl[1483].w[7:10]"  0.0014864386694446044 0.0085282379277327302 
		0.49499266170141132 0.49499266170141132;
	setAttr -s 4 ".wl[1484].w[7:10]"  0.001400097847095567 0.0080956589877831612 
		0.49525212158256071 0.4952521215825606;
	setAttr -s 4 ".wl[1485].w[7:10]"  0.00017991398343912253 0.0014151506976974792 
		0.49920246765943171 0.49920246765943171;
	setAttr -s 4 ".wl[1486].w[7:10]"  0.000101968978704444 0.00083019113676858765 
		0.49953391994226348 0.49953391994226348;
	setAttr -s 4 ".wl[1487].w[7:10]"  0.0001202363146475115 0.00095728665738181924 
		0.49946123851398538 0.49946123851398538;
	setAttr -s 4 ".wl[1488].w[7:10]"  0.0014617548624707636 0.0084246229899060041 
		0.49505681107381166 0.49505681107381166;
	setAttr -s 4 ".wl[1489].w[7:10]"  6.8238296005151576e-05 0.00056750158239522979 
		0.49968213006079987 0.49968213006079976;
	setAttr -s 4 ".wl[1490].w[7:10]"  0.00010715761193457012 0.00087084069018184701 
		0.49951100084894184 0.49951100084894184;
	setAttr -s 4 ".wl[1491].w[7:10]"  0.00014738818889816914 0.0011772052086507015 
		0.49933770330122557 0.49933770330122557;
	setAttr -s 4 ".wl[1492].w[7:10]"  9.135343639683927e-05 0.00073967004185306951 
		0.49958448826087509 0.49958448826087498;
	setAttr -s 4 ".wl[1493].w[7:10]"  0.00017856954608716871 0.001362616035563189 
		0.49922940720917486 0.49922940720917486;
	setAttr -s 4 ".wl[1494].w[7:10]"  0.00012131051069625955 0.00097191610729901719 
		0.49945338669100237 0.49945338669100237;
	setAttr -s 4 ".wl[1495].w[7:10]"  0.00023178809509334834 0.0017495397849736939 
		0.4990093360599665 0.4990093360599665;
	setAttr -s 4 ".wl[1496].w[7:10]"  9.3453497682517757e-05 0.00075316555495999753 
		0.49957669047367881 0.4995766904736787;
	setAttr -s 4 ".wl[1497].w[7:10]"  0.00017483297355879948 0.0013301423607409658 
		0.49924751233285009 0.49924751233285009;
	setAttr -s 4 ".wl[1498].w[7:10]"  0.00018388366296161361 0.0014564843836490018 
		0.4991798159766947 0.4991798159766947;
	setAttr -s 4 ".wl[1499].w[7:10]"  0.00014699815296466241 0.0011700390854622372 
		0.4993414813807866 0.49934148138078649;
	setAttr -s 4 ".wl[1500].w[7:10]"  0.00027604990508933155 0.002066997083139155 
		0.49882847650588574 0.49882847650588574;
	setAttr -s 4 ".wl[1501].w[7:10]"  0.00020104104737228535 0.0015268518143663836 
		0.49913605356913071 0.4991360535691306;
	setAttr -s 4 ".wl[1502].w[7:10]"  0.00019041158974307586 0.0014438349730008071 
		0.49918287671862821 0.49918287671862799;
	setAttr -s 4 ".wl[1503].w[7:10]"  0.00049177302603505165 0.003357136508356515 
		0.49807554523280423 0.49807554523280423;
	setAttr -s 4 ".wl[1504].w[7:10]"  0.00039662005177158467 0.002736289984804741 
		0.49843354498171188 0.49843354498171188;
	setAttr -s 4 ".wl[1505].w[7:10]"  0.00085275313096455044 0.0053185744925339888 
		0.4969143361882507 0.4969143361882507;
	setAttr -s 4 ".wl[1506].w[7:10]"  0.00045807513862645272 0.003131346224694805 
		0.49820528931833941 0.49820528931833941;
	setAttr -s 4 ".wl[1507].w[7:10]"  0.00094392881465611971 0.0058416980909278063 
		0.49660718654720803 0.49660718654720803;
	setAttr -s 4 ".wl[1508].w[7:10]"  0.00041242730280867238 0.0028308691484020794 
		0.49837835177439477 0.49837835177439455;
	setAttr -s 4 ".wl[1509].w[7:10]"  0.00084566899764490988 0.0052626320215801879 
		0.49694584949038761 0.49694584949038739;
	setAttr -s 4 ".wl[1510].w[7:10]"  0.00054953040573738033 0.003725089398460548 
		0.49786269009790102 0.49786269009790102;
	setAttr -s 4 ".wl[1511].w[7:10]"  0.0005014263423587849 0.0034088416484587551 
		0.4980448660045913 0.4980448660045913;
	setAttr -s 4 ".wl[1512].w[7:10]"  0.0010186796941308515 0.0062640006002949652 
		0.49635865985278715 0.49635865985278704;
	setAttr -s 4 ".wl[1513].w[7:10]"  0.00089415574003936214 0.0055557657637896269 
		0.49677503924808547 0.49677503924808547;
	setAttr -s 4 ".wl[1514].w[7:10]"  0.00087470755075705331 0.0054288278713283244 
		0.4968482322889573 0.4968482322889573;
	setAttr -s 4 ".wl[1515].w[7:10]"  0.0013182069688859087 0.0077299961755569876 
		0.49547589842777856 0.49547589842777856;
	setAttr -s 4 ".wl[1516].w[7:10]"  0.0011854002076743004 0.0070056592650058055 
		0.49590447026365997 0.49590447026365997;
	setAttr -s 4 ".wl[1517].w[7:10]"  0.001445214706092327 0.0082912424338026751 
		0.49513177143005249 0.49513177143005249;
	setAttr -s 4 ".wl[1518].w[7:10]"  0.0012583303536806215 0.0073916343831424199 
		0.49567501763158844 0.49567501763158844;
	setAttr -s 4 ".wl[1519].w[7:10]"  0.0015085845702930477 0.0086192259557631174 
		0.49493609473697192 0.49493609473697192;
	setAttr -s 4 ".wl[1520].w[7:10]"  0.0011902521666820422 0.0070142159596724256 
		0.49589776593682278 0.49589776593682278;
	setAttr -s 4 ".wl[1521].w[7:10]"  0.0014186033439426197 0.0081357210753638387 
		0.49522283779034681 0.49522283779034681;
	setAttr -s 4 ".wl[1522].w[7:10]"  0.0014786812885782515 0.0084181684047049815 
		0.49505157515335835 0.49505157515335835;
	setAttr -s 4 ".wl[1523].w[7:10]"  0.0014007566617253533 0.0081656991486493864 
		0.49521677209481269 0.49521677209481269;
	setAttr -s 4 ".wl[1524].w[7:10]"  0.0013212151555869009 0.0077252253713778994 
		0.49547677973651755 0.49547677973651755;
	setAttr -s 4 ".wl[1525].w[7:10]"  0.0015852250348782967 0.0090110737125286312 
		0.49470185062629651 0.49470185062629651;
	setAttr -s 4 ".wl[1526].w[7:10]"  0.0014934563441511184 0.0085398032354940534 
		0.49498337021017741 0.49498337021017741;
	setAttr -s 4 ".wl[1527].w[7:10]"  0.0014554316504289356 0.0083257114786652719 
		0.49510942843545303 0.4951094284354528;
	setAttr -s 4 ".wl[1528].w[7:10]"  0.00051752312034342065 0.003488449595331154 
		0.49799701364216276 0.49799701364216276;
	setAttr -s 4 ".wl[1529].w[7:10]"  0.00053829669803361253 0.0036207404671952706 
		0.4979204814173856 0.4979204814173856;
	setAttr -s 4 ".wl[1530].w[7:10]"  0.00022720003283065618 0.0016951122457683754 
		0.49903884386070047 0.49903884386070047;
	setAttr -s 4 ".wl[1531].w[7:10]"  0.00060290654582185164 0.0040311869936414755 
		0.49768295323026834 0.49768295323026834;
	setAttr -s 4 ".wl[1532].w[7:10]"  0.00026444692638571997 0.0019617544024526619 
		0.49888689933558095 0.49888689933558072;
	setAttr -s 4 ".wl[1533].w[7:10]"  0.000658533029855702 0.0043854786490968994 
		0.49747799416052374 0.49747799416052374;
	setAttr -s 4 ".wl[1534].w[7:10]"  0.00032908337946106709 0.0024213127438907425 
		0.49862480193832415 0.49862480193832415;
	setAttr -s 4 ".wl[1535].w[7:10]"  0.00059165107440917978 0.0039653477202212344 
		0.49772150060268477 0.49772150060268477;
	setAttr -s 4 ".wl[1536].w[7:10]"  0.00031447294937975411 0.0023211332600821845 
		0.49868219689526916 0.49868219689526894;
	setAttr -s 4 ".wl[1537].w[7:10]"  0.00055302534167328381 0.0037186951962726168 
		0.49786413973102689 0.49786413973102711;
	setAttr -s 4 ".wl[1538].w[7:10]"  0.00027153565547628544 0.0020164016072944926 
		0.49885603136861462 0.49885603136861462;
	setAttr -s 4 ".wl[1539].w[7:10]"  0.00024140652967825113 0.0017986777583265895 
		0.49897995785599752 0.49897995785599752;
	setAttr -s 4 ".wl[1540].w[7:10]"  0.0012494298627040953 0.0073640322339944874 
		0.49569326895165072 0.49569326895165072;
	setAttr -s 4 ".wl[1541].w[7:10]"  0.0012787503902623072 0.0075240105706627517 
		0.49559861951953754 0.49559861951953754;
	setAttr -s 4 ".wl[1542].w[7:10]"  0.00095492493837511438 0.0058839950640843762 
		0.49658053999877022 0.49658053999877022;
	setAttr -s 4 ".wl[1543].w[7:10]"  0.0013577485101075033 0.0079538019855899828 
		0.49534422475215129 0.49534422475215129;
	setAttr -s 4 ".wl[1544].w[7:10]"  0.0010107563340657735 0.0062047833027657727 
		0.49639223018158424 0.49639223018158424;
	setAttr -s 4 ".wl[1545].w[7:10]"  0.0014025508278826127 0.0082003226914009533 
		0.49519856324035827 0.49519856324035827;
	setAttr -s 4 ".wl[1546].w[7:10]"  0.0010950397485083422 0.006688021120377188 
		0.49610846956555721 0.49610846956555721;
	setAttr -s 4 ".wl[1547].w[7:10]"  0.0013226890245691114 0.0077693477243755762 
		0.49545398162552762 0.49545398162552762;
	setAttr -s 4 ".wl[1548].w[7:10]"  0.0010710605741207412 0.0065553774115421556 
		0.49618678100716856 0.49618678100716856;
	setAttr -s 4 ".wl[1549].w[7:10]"  0.00128016705321025 0.0075361341181073831 
		0.49559184941434115 0.49559184941434115;
	setAttr -s 4 ".wl[1550].w[7:10]"  0.0010049188825393448 0.0061772702931985925 
		0.49640890541213101 0.49640890541213101;
	setAttr -s 4 ".wl[1551].w[7:10]"  0.00096485776444905631 0.0059436000698989107 
		0.49654577108282605 0.49654577108282605;
	setAttr -s 4 ".wl[1552].w[7:10]"  1.3717947276069261e-05 0.00013255534228247604 
		0.49992686335522069 0.49992686335522069;
	setAttr -s 4 ".wl[1553].w[7:10]"  1.8870081446725023e-05 0.0001816057481082145 
		0.49989976208522252 0.49989976208522252;
	setAttr -s 4 ".wl[1554].w[7:10]"  1.1995874199320454e-05 0.00011560437681810085 
		0.49993619987449128 0.49993619987449128;
	setAttr -s 4 ".wl[1555].w[7:10]"  7.9726338205354264e-06 7.7143267955528646e-05 
		0.49995744204911213 0.49995744204911191;
	setAttr -s 4 ".wl[1556].w[7:10]"  5.9106612745970675e-05 0.00053403369821713651 
		0.49970342984451843 0.49970342984451843;
	setAttr -s 4 ".wl[1557].w[7:10]"  4.1012722902567431e-05 0.00037411735253301139 
		0.49979243496228221 0.49979243496228221;
	setAttr -s 4 ".wl[1558].w[7:10]"  4.9287817740691721e-05 0.00044789569414075146 
		0.49975140824405911 0.49975140824405934;
	setAttr -s 4 ".wl[1559].w[7:10]"  8.4163753193751902e-05 0.00075352365894008303 
		0.49958115629393307 0.49958115629393307;
	setAttr -s 4 ".wl[1560].w[7:10]"  6.793755795746499e-05 0.00060907078239456794 
		0.499661495829824 0.499661495829824;
	setAttr -s 4 ".wl[1561].w[7:10]"  2.4813662580118739e-05 0.00022616507864951224 
		0.49987451062938526 0.49987451062938515;
	setAttr -s 4 ".wl[1562].w[7:10]"  1.9057238835185692e-05 0.00017436122085051278 
		0.4999032907701571 0.4999032907701571;
	setAttr -s 4 ".wl[1563].w[7:10]"  4.5656080615669913e-05 0.00041306732843123797 
		0.49977063829547658 0.49977063829547658;
	setAttr -s 4 ".wl[1564].w[7:10]"  4.1051393527657079e-05 0.00037577131069338438 
		0.49979158864788947 0.49979158864788947;
	setAttr -s 4 ".wl[1565].w[7:10]"  3.1923318684656178e-05 0.0002940420775210408 
		0.49983701730189717 0.49983701730189717;
	setAttr -s 4 ".wl[1566].w[7:10]"  3.8009262613047202e-05 0.00034898358205048564 
		0.49980650357766826 0.49980650357766826;
	setAttr -s 4 ".wl[1567].w[7:10]"  5.851389029395765e-05 0.00053152620074833998 
		0.49970497995447893 0.49970497995447882;
	setAttr -s 4 ".wl[1568].w[7:10]"  4.7181755975528963e-05 0.00042907803697339012 
		0.49976187010352552 0.49976187010352552;
	setAttr -s 4 ".wl[1569].w[7:10]"  1.9885572996064574e-05 0.00018304072262161709 
		0.49989853685219121 0.4998985368521911;
	setAttr -s 4 ".wl[1570].w[7:10]"  1.5560060685748049e-05 0.00014368423504546266 
		0.49992037785213439 0.49992037785213439;
	setAttr -s 4 ".wl[1571].w[7:10]"  3.1662953809782467e-05 0.00029016576332372383 
		0.49983908564143326 0.49983908564143326;
	setAttr -s 4 ".wl[1572].w[7:10]"  9.6854821302804498e-05 0.00085998895553548831 
		0.49952157811158093 0.49952157811158082;
	setAttr -s 4 ".wl[1573].w[7:10]"  5.5263950664200779e-05 0.00049881509208903898 
		0.49972296047862341 0.49972296047862341;
	setAttr -s 4 ".wl[1574].w[7:10]"  4.6022548827352963e-05 0.00041706705950174752 
		0.49976845519583529 0.49976845519583551;
	setAttr -s 4 ".wl[1575].w[7:10]"  6.847304994912488e-05 0.00061383837982034999 
		0.49965884428511531 0.49965884428511531;
	setAttr -s 4 ".wl[1576].w[7:10]"  5.3204148312490064e-05 0.00047763598007905233 
		0.49973457993580422 0.49973457993580422;
	setAttr -s 4 ".wl[1577].w[7:10]"  2.362370205272377e-05 0.00022225167394829384 
		0.49987706231199952 0.49987706231199952;
	setAttr -s 4 ".wl[1578].w[7:10]"  9.89402975095916e-06 9.3823409988660547e-05 
		0.49994814128013015 0.49994814128013015;
	setAttr -s 4 ".wl[1579].w[7:10]"  1.3461492113994525e-05 0.00012723481106502858 
		0.49992965184841048 0.49992965184841048;
	setAttr -s 4 ".wl[1580].w[7:10]"  3.7499008458764621e-05 0.0003500225575490233 
		0.49980623921699613 0.49980623921699613;
	setAttr -s 4 ".wl[1581].w[7:10]"  4.7866419691594275e-05 0.00044626273315259457 
		0.49975293542357807 0.49975293542357785;
	setAttr -s 4 ".wl[1582].w[7:10]"  2.922788064127453e-05 0.00027553159685237902 
		0.49984762026125318 0.49984762026125318;
	setAttr -s 4 ".wl[1583].w[7:10]"  2.3837268673445002e-05 0.00022545162381974989 
		0.4998753555537534 0.4998753555537534;
	setAttr -s 4 ".wl[1584].w[7:10]"  3.1978055601915532e-05 0.00030048989416864806 
		0.4998337660251147 0.4998337660251147;
	setAttr -s 4 ".wl[1585].w[7:10]"  3.760228863054138e-05 0.0003414900425017202 
		0.4998104538344339 0.4998104538344339;
	setAttr -s 4 ".wl[1586].w[7:10]"  1.796204128958189e-05 0.00016454094500968202 
		0.49990874850685035 0.49990874850685035;
	setAttr -s 4 ".wl[1587].w[7:10]"  2.2888999500029395e-05 0.00020896643180167782 
		0.49988407228434928 0.49988407228434906;
	setAttr -s 4 ".wl[1588].w[7:10]"  5.5495793539923255e-05 0.00049992028964343765 
		0.49972229195840817 0.49972229195840839;
	setAttr -s 4 ".wl[1589].w[7:10]"  6.8505793547796019e-05 0.0006163761064667064 
		0.49965755904999271 0.49965755904999271;
	setAttr -s 4 ".wl[1590].w[7:10]"  4.3468425255384222e-05 0.0003957959083573004 
		0.49978036783319379 0.49978036783319357;
	setAttr -s 4 ".wl[1591].w[7:10]"  3.6560727381038962e-05 0.00033402440800258071 
		0.49981470743230827 0.49981470743230816;
	setAttr -s 4 ".wl[1592].w[7:10]"  4.8426623778312871e-05 0.00043926127144617021 
		0.49975615605238771 0.49975615605238771;
	setAttr -s 4 ".wl[1593].w[7:10]"  7.3551009062693265e-05 0.00063438233100512117 
		0.49964603332996615 0.49964603332996604;
	setAttr -s 4 ".wl[1594].w[7:10]"  8.4736728859745314e-05 0.0007280838529344322 
		0.49959358970910295 0.49959358970910295;
	setAttr -s 4 ".wl[1595].w[7:10]"  0.00013284891994966031 0.0011240985998687159 
		0.49937152624009085 0.49937152624009085;
	setAttr -s 4 ".wl[1596].w[7:10]"  0.0001778908103124043 0.0014034648437851183 
		0.49920932217295128 0.49920932217295128;
	setAttr -s 4 ".wl[1597].w[7:10]"  0.00015066784118668213 0.001192738322751548 
		0.49932829691803093 0.49932829691803093;
	setAttr -s 4 ".wl[1598].w[7:10]"  0.000155686587447903 0.0012334814895823307 
		0.49930541596148487 0.49930541596148487;
	setAttr -s 4 ".wl[1599].w[7:10]"  5.0856280986806267e-05 0.00043824194261215784 
		0.4997554508882005 0.4997554508882005;
	setAttr -s 4 ".wl[1600].w[7:10]"  4.2286154601321926e-05 0.00036578302136131588 
		0.49979596541201882 0.49979596541201859;
	setAttr -s 4 ".wl[1601].w[7:10]"  2.148806553679212e-05 0.00019534219286968408 
		0.49989158487079677 0.49989158487079677;
	setAttr -s 4 ".wl[1602].w[7:10]"  2.7930346789075811e-05 0.00025289128082922591 
		0.49985958918619094 0.49985958918619072;
	setAttr -s 4 ".wl[1603].w[7:10]"  7.8503704496219354e-05 0.00069803142392696113 
		0.49961173243578844 0.49961173243578844;
	setAttr -s 4 ".wl[1604].w[7:10]"  0.00010019628004923501 0.0008555773026151409 
		0.4995221132086678 0.4995221132086678;
	setAttr -s 4 ".wl[1605].w[7:10]"  0.00011191525163118978 0.00094820913197716399 
		0.49946993780819582 0.49946993780819582;
	setAttr -s 4 ".wl[1606].w[7:10]"  8.2142351853107984e-05 0.00070233906999468958 
		0.49960775928907608 0.49960775928907608;
	setAttr -s 4 ".wl[1607].w[7:10]"  0.0015244809997459596 0.0087291220207563417 
		0.49487319848974881 0.49487319848974881;
	setAttr -s 4 ".wl[1608].w[7:10]"  0.0014622317588271075 0.0083996718776893874 
		0.49506904818174174 0.49506904818174174;
	setAttr -s 4 ".wl[1609].w[7:10]"  0.00020236271704156483 0.0015867479861148622 
		0.49910544464842177 0.49910544464842177;
	setAttr -s 4 ".wl[1610].w[7:10]"  0.0001427883827024802 0.0011311992666560348 
		0.49936300617532076 0.49936300617532076;
	setAttr -s 4 ".wl[1611].w[7:10]"  0.00012124622352010708 0.00096492090222855605 
		0.49945691643712564 0.49945691643712564;
	setAttr -s 4 ".wl[1612].w[7:10]"  0.001505474983686541 0.0085610405821873459 
		0.49496674221706305 0.49496674221706305;
	setAttr -s 4 ".wl[1613].w[7:10]"  0.0015487117417709063 0.0087814940259582988 
		0.49483489711613537 0.49483489711613537;
	setAttr -s 4 ".wl[1614].w[7:10]"  0.00010674780562513725 0.0008610113864934623 
		0.49951612040394072 0.49951612040394072;
	setAttr -s 4 ".wl[1615].w[7:10]"  0.0001480946330099031 0.0011811029633938097 
		0.49933540120179815 0.49933540120179815;
	setAttr -s 4 ".wl[1616].w[7:10]"  9.8654653233399075e-05 0.00079424902410066506 
		0.49955354816133296 0.49955354816133296;
	setAttr -s 4 ".wl[1617].w[7:10]"  0.0001853581274663304 0.0014657868695116512 
		0.49917442750151103 0.49917442750151103;
	setAttr -s 4 ".wl[1618].w[7:10]"  0.00012499202127269298 0.0010031241445140418 
		0.49943594191710661 0.49943594191710661;
	setAttr -s 4 ".wl[1619].w[7:10]"  0.00011091982767965826 0.00088983470156800513 
		0.49949962273537618 0.49949962273537618;
	setAttr -s 4 ".wl[1620].w[7:10]"  0.0004234531453474749 0.0029131503765982164 
		0.49833169823902723 0.49833169823902712;
	setAttr -s 4 ".wl[1621].w[7:10]"  0.00049723531917617817 0.0033888891870169206 
		0.49805693774690352 0.49805693774690341;
	setAttr -s 4 ".wl[1622].w[7:10]"  0.00042106249477299378 0.0028882649297343028 
		0.49834533628774635 0.49834533628774635;
	setAttr -s 4 ".wl[1623].w[7:10]"  0.0005563598467957959 0.0037647799980867445 
		0.49783943007755871 0.49783943007755871;
	setAttr -s 4 ".wl[1624].w[7:10]"  0.00045515283542409666 0.0031180282076530672 
		0.49821340947846149 0.49821340947846138;
	setAttr -s 4 ".wl[1625].w[7:10]"  0.00044328182096537344 0.0030316193593116388 
		0.49826254940986148 0.49826254940986148;
	setAttr -s 4 ".wl[1626].w[7:10]"  0.0012237433551297828 0.0072174777834895067 
		0.49577938943069039 0.49577938943069039;
	setAttr -s 4 ".wl[1627].w[7:10]"  0.0013194879089564139 0.0077304949636610946 
		0.49547500856369131 0.49547500856369131;
	setAttr -s 4 ".wl[1628].w[7:10]"  0.0012028657892327137 0.0070853792937130471 
		0.49585587745852711 0.49585587745852711;
	setAttr -s 4 ".wl[1629].w[7:10]"  0.0014035709030098095 0.0081738441196891626 
		0.49521129248865059 0.49521129248865048;
	setAttr -s 4 ".wl[1630].w[7:10]"  0.0012709863141394584 0.0074696354877408265 
		0.49562968909905991 0.49562968909905991;
	setAttr -s 4 ".wl[1631].w[7:10]"  0.0012358858125132441 0.0072616973002350878 
		0.49575120844362586 0.49575120844362586;
	setAttr -s 4 ".wl[1632].w[7:10]"  0.00052007936897823274 0.003504355597790072 
		0.49798778251661585 0.49798778251661585;
	setAttr -s 4 ".wl[1633].w[7:10]"  0.00056640870814863697 0.0037995476339918151 
		0.49781702182892984 0.49781702182892984;
	setAttr -s 4 ".wl[1634].w[7:10]"  0.00064262427022189039 0.0042833408649051172 
		0.4975370174324365 0.4975370174324365;
	setAttr -s 4 ".wl[1635].w[7:10]"  0.00062778840785167182 0.0041933825194044368 
		0.49758941453637195 0.49758941453637195;
	setAttr -s 4 ".wl[1636].w[7:10]"  0.00057082534362099099 0.0038329400912238774 
		0.49779811728257756 0.49779811728257756;
	setAttr -s 4 ".wl[1637].w[7:10]"  0.00053182585852129375 0.0035815648982729423 
		0.49794330462160286 0.49794330462160286;
	setAttr -s 4 ".wl[1638].w[7:10]"  0.0012549046794069359 0.0073936041356754516 
		0.49567574559245881 0.49567574559245881;
	setAttr -s 4 ".wl[1639].w[7:10]"  0.0013149728278541671 0.0077212052318100322 
		0.49548191097016792 0.49548191097016792;
	setAttr -s 4 ".wl[1640].w[7:10]"  0.0013950386047530462 0.0081574330575127797 
		0.4952237641688671 0.4952237641688671;
	setAttr -s 4 ".wl[1641].w[7:10]"  0.0013645646608981038 0.0079962727201144963 
		0.49531958130949366 0.49531958130949366;
	setAttr -s 4 ".wl[1642].w[7:10]"  0.0012983177804302202 0.0076363205047623962 
		0.49553268085740376 0.49553268085740376;
	setAttr -s 4 ".wl[1643].w[7:10]"  0.0012613074336816298 0.0074308951314959954 
		0.49565389871741117 0.49565389871741117;
	setAttr -s 4 ".wl[1644].w[11:14]"  0.00021123321289443826 0.0017071446936266952 
		0.49904081104673942 0.49904081104673942;
	setAttr -s 4 ".wl[1645].w[11:14]"  0.00030707742555450825 0.0024127942118882841 
		0.49864006418127865 0.49864006418127865;
	setAttr -s 4 ".wl[1646].w[11:14]"  0.00021636998756864153 0.001745078888694772 
		0.49901927556186831 0.49901927556186831;
	setAttr -s 4 ".wl[1647].w[11:14]"  9.5504299768039722e-05 0.00080749532857887177 
		0.49954850018582658 0.49954850018582658;
	setAttr -s 4 ".wl[1648].w[11:14]"  8.3233126631885283e-05 0.00072006254403650198 
		0.4995983521646658 0.4995983521646658;
	setAttr -s 4 ".wl[1649].w[11:14]"  0.00010846447970529179 0.00093749125541691487 
		0.49947702213243894 0.49947702213243894;
	setAttr -s 4 ".wl[1650].w[11:14]"  9.4039485724234988e-05 0.00081455102868206318 
		0.49954570474279686 0.49954570474279686;
	setAttr -s 4 ".wl[1651].w[11:14]"  9.4027265442552771e-05 0.00079679282566559465 
		0.49955458995444579 0.49955458995444602;
	setAttr -s 4 ".wl[1652].w[11:14]"  0.00010948709545171231 0.00098443338084929291 
		0.49945303976184952 0.49945303976184952;
	setAttr -s 4 ".wl[1653].w[11:14]"  0.00018773685734891097 0.0016438511950563923 
		0.49908420597379738 0.49908420597379738;
	setAttr -s 4 ".wl[1654].w[11:14]"  0.00010405295747420702 0.00093973911308813042 
		0.49947810396471887 0.49947810396471887;
	setAttr -s 4 ".wl[1655].w[11:14]"  2.5394399622036103e-05 0.0002419826493234937 
		0.49986631147552724 0.49986631147552724;
	setAttr -s 4 ".wl[1656].w[11:14]"  2.6581019441624483e-05 0.00025889681812917475 
		0.49985726108121459 0.49985726108121459;
	setAttr -s 4 ".wl[1657].w[11:14]"  4.2448780212799712e-05 0.00041336874856324064 
		0.499772091235612 0.499772091235612;
	setAttr -s 4 ".wl[1658].w[11:14]"  2.9070271875347601e-05 0.00028195866873028346 
		0.49984448552969718 0.49984448552969718;
	setAttr -s 4 ".wl[1659].w[11:14]"  2.8525532999769655e-05 0.00027034782666117088 
		0.49985056332016953 0.49985056332016953;
	setAttr -s 4 ".wl[1660].w[11:14]"  0.00011720779332619607 0.0010529877955341093 
		0.49941490220556983 0.49941490220556983;
	setAttr -s 4 ".wl[1661].w[11:14]"  6.9587946283035095e-05 0.00063991348498661129 
		0.49964524928436521 0.49964524928436521;
	setAttr -s 4 ".wl[1662].w[11:14]"  2.074257678054936e-05 0.00019880887753245577 
		0.49989022427284352 0.49989022427284352;
	setAttr -s 4 ".wl[1663].w[11:14]"  2.5180222667095905e-05 0.00024675412043466936 
		0.4998640328284491 0.4998640328284491;
	setAttr -s 4 ".wl[1664].w[11:14]"  1.8118216412485122e-05 0.00017720204622411227 
		0.49990233986868166 0.49990233986868166;
	setAttr -s 4 ".wl[1665].w[11:14]"  1.9762799117327582e-05 0.00019262988946449066 
		0.49989380365570912 0.49989380365570912;
	setAttr -s 4 ".wl[1666].w[11:14]"  2.3012047391303825e-05 0.00021959383498550806 
		0.49987869705881155 0.49987869705881155;
	setAttr -s 4 ".wl[1667].w[11:14]"  7.307938231827278e-05 0.00066962317693627222 
		0.49962864872037277 0.49962864872037277;
	setAttr -s 4 ".wl[1668].w[11:14]"  6.5925155255043552e-05 0.00063229441223037607 
		0.4996508902162573 0.4996508902162573;
	setAttr -s 4 ".wl[1669].w[11:14]"  5.4139869937721594e-06 5.5377622296645178e-05 
		0.49996960419535486 0.49996960419535474;
	setAttr -s 4 ".wl[1670].w[11:14]"  4.2263419177323472e-05 0.00041062180751744946 
		0.49977355738665263 0.49977355738665263;
	setAttr -s 4 ".wl[1671].w[11:14]"  8.6105199538291485e-06 8.7399777304556165e-05 
		0.49995199485137087 0.49995199485137076;
	setAttr -s 4 ".wl[1672].w[11:14]"  8.8220565055828017e-06 9.156057600944088e-05 
		0.49994980868374245 0.49994980868374245;
	setAttr -s 4 ".wl[1673].w[11:14]"  7.1428757378201407e-06 7.3609817768988058e-05 
		0.49995962365324659 0.49995962365324659;
	setAttr -s 4 ".wl[1674].w[11:14]"  8.0746828073639869e-06 8.2959442817743089e-05 
		0.49995448293718747 0.49995448293718747;
	setAttr -s 4 ".wl[1675].w[11:14]"  9.8873275890599828e-06 9.9979371863742218e-05 
		0.49994506665027361 0.49994506665027361;
	setAttr -s 4 ".wl[1676].w[11:14]"  4.469300180090191e-05 0.00043284206596385912 
		0.49976123246611764 0.49976123246611764;
	setAttr -s 4 ".wl[1677].w[11:14]"  0.0001270382013311964 0.0011156733633490867 
		0.49937864421765987 0.49937864421765987;
	setAttr -s 4 ".wl[1678].w[11:14]"  3.7442239041074158e-05 0.00034646446351182543 
		0.49980804664872358 0.49980804664872347;
	setAttr -s 4 ".wl[1679].w[11:14]"  3.8378814310663148e-05 0.00036336641384172717 
		0.4997991273859238 0.4997991273859238;
	setAttr -s 4 ".wl[1680].w[11:14]"  5.3882299493350411e-05 0.00051218354795232147 
		0.49971696707627722 0.49971696707627711;
	setAttr -s 4 ".wl[1681].w[11:14]"  3.5473980800554079e-05 0.00033728930523695193 
		0.49981361835698124 0.49981361835698124;
	setAttr -s 4 ".wl[1682].w[11:14]"  3.3797797449881752e-05 0.00031446210982713138 
		0.49982587004636148 0.49982587004636148;
	setAttr -s 4 ".wl[1683].w[11:14]"  0.000121112162525617 0.0010684133816710661 
		0.49940523722790164 0.49940523722790164;
	setAttr -s 4 ".wl[1684].w[11:14]"  0.00021119833259561783 0.0018066675630387997 
		0.49899106705218282 0.49899106705218282;
	setAttr -s 4 ".wl[1685].w[11:14]"  0.00010371947396617845 0.00095013200430702481 
		0.49947307426086324 0.49947307426086346;
	setAttr -s 4 ".wl[1686].w[11:14]"  6.3026749347949918e-05 0.00058886191923826015 
		0.49967405566570688 0.49967405566570688;
	setAttr -s 4 ".wl[1687].w[11:14]"  1.790719243886022e-05 0.00017423643559746045 
		0.49990392818598184 0.49990392818598184;
	setAttr -s 4 ".wl[1688].w[11:14]"  1.4897539559322073e-05 0.00014807310845787132 
		0.49991851467599147 0.49991851467599135;
	setAttr -s 4 ".wl[1689].w[11:14]"  1.9447568406882921e-05 0.00019434612928913306 
		0.49989310315115204 0.49989310315115204;
	setAttr -s 4 ".wl[1690].w[11:14]"  1.3494831589175347e-05 0.00013458156084949024 
		0.49992596180378052 0.49992596180378074;
	setAttr -s 4 ".wl[1691].w[11:14]"  1.5939952346691278e-05 0.0001557683319228485 
		0.49991414585786526 0.49991414585786526;
	setAttr -s 4 ".wl[1692].w[11:14]"  5.9833292081439724e-05 0.0005609981459390981 
		0.49968958428098975 0.49968958428098975;
	setAttr -s 4 ".wl[1693].w[11:14]"  0.00014854897083257135 0.0013178608652358399 
		0.49926679508196581 0.49926679508196581;
	setAttr -s 4 ".wl[1694].w[11:14]"  8.9498210492734939e-05 0.00081242075563987077 
		0.49954904051693372 0.49954904051693372;
	setAttr -s 4 ".wl[1695].w[11:14]"  2.561926444807873e-05 0.00024359636316702394 
		0.49986539218619253 0.49986539218619253;
	setAttr -s 4 ".wl[1696].w[11:14]"  2.3858681344285002e-05 0.00023196871103325697 
		0.49987208630381125 0.49987208630381125;
	setAttr -s 4 ".wl[1697].w[11:14]"  3.2579210399848356e-05 0.00031828337161584149 
		0.49982456870899217 0.49982456870899217;
	setAttr -s 4 ".wl[1698].w[11:14]"  2.1834970210043854e-05 0.00021309853376231885 
		0.49988253324801379 0.49988253324801379;
	setAttr -s 4 ".wl[1699].w[11:14]"  2.294569459323926e-05 0.00021924669116730672 
		0.49987890380711975 0.49987890380711975;
	setAttr -s 4 ".wl[1700].w[11:14]"  8.51303019728048e-05 0.00077586570957138639 
		0.49956950199422806 0.49956950199422784;
	setAttr -s 4 ".wl[1701].w[11:14]"  9.5803834951471343e-05 0.00079646637362628021 
		0.49955386489571113 0.49955386489571113;
	setAttr -s 4 ".wl[1702].w[11:14]"  0.0020627217267187234 0.011322197986560043 
		0.49330754014336048 0.4933075401433607;
	setAttr -s 4 ".wl[1703].w[11:14]"  0.002040263231750663 0.011209444001459226 
		0.49337514638339508 0.49337514638339508;
	setAttr -s 4 ".wl[1704].w[11:14]"  0.0022068745457642499 0.011997819198945723 
		0.492897653127645 0.492897653127645;
	setAttr -s 4 ".wl[1705].w[11:14]"  0.0021733472337139188 0.011836546293555308 
		0.49299505323636522 0.49299505323636544;
	setAttr -s 4 ".wl[1706].w[11:14]"  0.0018632492427682942 0.010403192986222864 
		0.4938667788855044 0.4938667788855044;
	setAttr -s 4 ".wl[1707].w[11:14]"  0.0018895048603567743 0.010531031748346898 
		0.49378973169564816 0.49378973169564816;
	setAttr -s 4 ".wl[1708].w[11:14]"  0.00046169811334275507 0.0033417712231237867 
		0.49809826533176677 0.49809826533176677;
	setAttr -s 4 ".wl[1709].w[11:14]"  0.00019911476724198004 0.001534691683581375 
		0.49913309677458839 0.49913309677458828;
	setAttr -s 4 ".wl[1710].w[11:14]"  0.00041739701576460497 0.0030499940266875618 
		0.49826630447877396 0.49826630447877385;
	setAttr -s 4 ".wl[1711].w[11:14]"  0.00025807052273397042 0.0019656655706336589 
		0.49888813195331622 0.49888813195331622;
	setAttr -s 4 ".wl[1712].w[11:14]"  0.00024441672126740996 0.0019165542063707157 
		0.49891951453618094 0.49891951453618094;
	setAttr -s 4 ".wl[1713].w[11:14]"  0.00025121817786696757 0.0019545722174501333 
		0.49889710480234145 0.49889710480234145;
	setAttr -s 4 ".wl[1714].w[11:14]"  0.0015527576578809484 0.0090413076413599844 
		0.49470296735037955 0.49470296735037955;
	setAttr -s 4 ".wl[1715].w[11:14]"  0.0011078002082141949 0.0067627074689762864 
		0.49606474616140483 0.49606474616140472;
	setAttr -s 4 ".wl[1716].w[11:14]"  0.0014855079849119203 0.0087106831951835502 
		0.49490190440995224 0.49490190440995224;
	setAttr -s 4 ".wl[1717].w[11:14]"  0.0012172224579142363 0.0073660803230012426 
		0.49570834860954227 0.49570834860954227;
	setAttr -s 4 ".wl[1718].w[11:14]"  0.0011749405240302712 0.0072861636433565495 
		0.49576944791630656 0.49576944791630656;
	setAttr -s 4 ".wl[1719].w[11:14]"  0.0011922855378804556 0.0073448690161199556 
		0.49573142272299986 0.49573142272299986;
	setAttr -s 4 ".wl[1720].w[11:14]"  0.0022328343975616767 0.012082076931015583 
		0.49284254433571134 0.49284254433571134;
	setAttr -s 4 ".wl[1721].w[11:14]"  0.0019130805239542463 0.010632490227817778 
		0.49372721462411401 0.49372721462411401;
	setAttr -s 4 ".wl[1722].w[11:14]"  0.0021869568232360456 0.011877914513343526 
		0.4929675643317103 0.49296756433171018;
	setAttr -s 4 ".wl[1723].w[11:14]"  0.0019785581098853205 0.010949681159799406 
		0.49353588036515766 0.49353588036515766;
	setAttr -s 4 ".wl[1724].w[11:14]"  0.0018553262071473486 0.010546467579568446 
		0.4937991031066421 0.4937991031066421;
	setAttr -s 4 ".wl[1725].w[11:14]"  0.0018699231996852153 0.010586865519043736 
		0.49377160564063555 0.49377160564063555;
	setAttr -s 4 ".wl[1726].w[11:14]"  0.00026430030204835662 0.0019750486461530504 
		0.49888032552589928 0.49888032552589928;
	setAttr -s 4 ".wl[1727].w[11:14]"  0.00033768007638366418 0.0024714290434052913 
		0.49859544544010553 0.49859544544010553;
	setAttr -s 4 ".wl[1728].w[11:14]"  0.00049658124081408662 0.0035275683536529242 
		0.49798792520276652 0.49798792520276652;
	setAttr -s 4 ".wl[1729].w[11:14]"  0.00054645179908849859 0.0038598718748465498 
		0.4977968381630325 0.4977968381630325;
	setAttr -s 4 ".wl[1730].w[11:14]"  0.00037911906378479263 0.00276115935539823 
		0.49842986079040846 0.49842986079040846;
	setAttr -s 4 ".wl[1731].w[11:14]"  0.00029586855171583341 0.0021974663915334141 
		0.49875333252837523 0.49875333252837545;
	setAttr -s 4 ".wl[1732].w[11:14]"  0.0012718742084371789 0.0075974029658983904 
		0.49556536141283219 0.49556536141283219;
	setAttr -s 4 ".wl[1733].w[11:14]"  0.0013912866564429946 0.0081995344200817399 
		0.49520458946173768 0.49520458946173768;
	setAttr -s 4 ".wl[1734].w[11:14]"  0.0015939535519429191 0.0092256973363927103 
		0.49459017455583221 0.49459017455583221;
	setAttr -s 4 ".wl[1735].w[11:14]"  0.0016755615409580369 0.0096455108578149748 
		0.49433946380061355 0.49433946380061344;
	setAttr -s 4 ".wl[1736].w[11:14]"  0.0014396535793478982 0.0084619543689039376 
		0.49504919602587416 0.49504919602587405;
	setAttr -s 4 ".wl[1737].w[11:14]"  0.0013155540493675231 0.0078305541799778451 
		0.49542694588532726 0.49542694588532726;
	setAttr -s 4 ".wl[1738].w[11:14]"  0.0001424614537167301 0.0011774022794547681 
		0.49934006813341425 0.49934006813341425;
	setAttr -s 4 ".wl[1739].w[11:14]"  8.0005363856226213e-05 0.00068681263284112886 
		0.49961659100165129 0.49961659100165129;
	setAttr -s 4 ".wl[1740].w[11:14]"  5.6246889685120799e-05 0.00051983388566177486 
		0.49971195961232656 0.49971195961232656;
	setAttr -s 4 ".wl[1741].w[11:14]"  0.00016374911125806172 0.0014436986813950795 
		0.4991962761036734 0.4991962761036734;
	setAttr -s 4 ".wl[1742].w[11:14]"  0.00015986924684044911 0.001413024217240855 
		0.49921355326795919 0.49921355326795941;
	setAttr -s 4 ".wl[1743].w[11:14]"  5.1799725978600223e-05 0.00048127998712871984 
		0.49973346014344638 0.49973346014344638;
	setAttr -s 4 ".wl[1744].w[11:14]"  2.0134468826540199e-05 0.00019489199762513773 
		0.49989248676677417 0.49989248676677417;
	setAttr -s 4 ".wl[1745].w[11:14]"  3.6530376812981429e-05 0.00035612011583297946 
		0.49980367475367699 0.49980367475367699;
	setAttr -s 4 ".wl[1746].w[11:14]"  3.8154306311415347e-05 0.00037108945056135408 
		0.49979537812156377 0.49979537812156355;
	setAttr -s 4 ".wl[1747].w[11:14]"  2.277326857678322e-05 0.00021930478414027994 
		0.49987896097364132 0.49987896097364154;
	setAttr -s 4 ".wl[1748].w[11:14]"  0.00016926569788539887 0.0014919649046654618 
		0.49916938469872463 0.49916938469872452;
	setAttr -s 4 ".wl[1749].w[11:14]"  9.4971016836406681e-05 0.00086193187073763946 
		0.49952154855621295 0.49952154855621295;
	setAttr -s 4 ".wl[1750].w[11:14]"  0.00010153409402994654 0.00091864435284272274 
		0.49948991077656363 0.49948991077656363;
	setAttr -s 4 ".wl[1751].w[11:14]"  2.4009947820648071e-05 0.00022928854411936677 
		0.49987335075403005 0.49987335075403005;
	setAttr -s 4 ".wl[1752].w[11:14]"  3.8230074151730327e-05 0.00035921100473872212 
		0.4998012794605548 0.4998012794605548;
	setAttr -s 4 ".wl[1753].w[11:14]"  3.7521765919524732e-05 0.00036629084904643637 
		0.49979809369251699 0.49979809369251699;
	setAttr -s 4 ".wl[1754].w[11:14]"  2.410864378574933e-05 0.00023526577114581646 
		0.49987031279253419 0.49987031279253419;
	setAttr -s 4 ".wl[1755].w[11:14]"  1.5884459931693678e-05 0.00015431326468221966 
		0.49991490113769305 0.49991490113769305;
	setAttr -s 4 ".wl[1756].w[11:14]"  2.2453058445471553e-05 0.00022008328596976367 
		0.49987873182779236 0.49987873182779236;
	setAttr -s 4 ".wl[1757].w[11:14]"  2.6373736173234863e-05 0.00025633850328750997 
		0.49985864388026968 0.49985864388026957;
	setAttr -s 4 ".wl[1758].w[11:14]"  2.3466781951110317e-05 0.00022958628387741593 
		0.49987347346708572 0.49987347346708572;
	setAttr -s 4 ".wl[1759].w[11:14]"  2.692108268737239e-05 0.00025575576472019725 
		0.49985866157629627 0.49985866157629627;
	setAttr -s 4 ".wl[1760].w[11:14]"  1.7766479515424244e-05 0.00017187849431795138 
		0.49990517751308333 0.49990517751308333;
	setAttr -s 4 ".wl[1761].w[11:14]"  9.9911424579077191e-05 0.00090292385816094435 
		0.49949858235863004 0.49949858235863004;
	setAttr -s 4 ".wl[1762].w[11:14]"  4.1269388158787502e-05 0.00038611210773614924 
		0.49978630925205259 0.49978630925205247;
	setAttr -s 4 ".wl[1763].w[11:14]"  0.00010393979700471484 0.00093852340408703177 
		0.49947876839945415 0.49947876839945415;
	setAttr -s 4 ".wl[1764].w[11:14]"  0.00010983578646168252 0.00099446422751350828 
		0.49944784999301256 0.49944784999301234;
	setAttr -s 4 ".wl[1765].w[11:14]"  2.315917861104187e-05 0.00023009663319593061 
		0.49987337209409638 0.4998733720940966;
	setAttr -s 4 ".wl[1766].w[11:14]"  6.4810160956462579e-05 0.00060042146938439438 
		0.49966738418482953 0.49966738418482953;
	setAttr -s 4 ".wl[1767].w[11:14]"  5.6883404171690904e-05 0.00054885731148608522 
		0.49969712964217111 0.49969712964217111;
	setAttr -s 4 ".wl[1768].w[11:14]"  1.8805602657041354e-05 0.0001814883583892942 
		0.49989985301947687 0.49989985301947687;
	setAttr -s 4 ".wl[1769].w[11:14]"  2.2244808092058447e-05 0.00022088808665779558 
		0.49987843355262523 0.49987843355262501;
	setAttr -s 4 ".wl[1770].w[11:14]"  6.0460538073487441e-06 6.1835878514344191e-05 
		0.49996605903383912 0.49996605903383912;
	setAttr -s 4 ".wl[1771].w[11:14]"  2.2527336651973018e-05 0.00022229013159523584 
		0.49987759126587639 0.49987759126587639;
	setAttr -s 4 ".wl[1772].w[11:14]"  3.5097702160354159e-06 3.649851937356331e-05 
		0.49997999585520525 0.49997999585520525;
	setAttr -s 4 ".wl[1773].w[11:14]"  1.6136027394532636e-05 0.0001588944589725479 
		0.49991248475681649 0.49991248475681649;
	setAttr -s 4 ".wl[1774].w[11:14]"  5.6185508337664231e-06 5.7855998753075368e-05 
		0.49996826272520656 0.49996826272520656;
	setAttr -s 4 ".wl[1775].w[11:14]"  7.8893869317396007e-06 8.1862287418279467e-05 
		0.499955124162825 0.499955124162825;
	setAttr -s 4 ".wl[1776].w[11:14]"  1.7664262417432767e-05 0.00017336142949548835 
		0.49990448715404351 0.49990448715404351;
	setAttr -s 4 ".wl[1777].w[11:14]"  8.4994104289007988e-06 8.8023962815506597e-05 
		0.4999517383133778 0.4999517383133778;
	setAttr -s 4 ".wl[1778].w[11:14]"  2.093387489352759e-05 0.00020115589117374559 
		0.4998889551169664 0.4998889551169664;
	setAttr -s 4 ".wl[1779].w[11:14]"  6.598910001576648e-06 6.7706966243221859e-05 
		0.49996284706187766 0.49996284706187766;
	setAttr -s 4 ".wl[1780].w[11:14]"  6.6470486322358236e-06 6.7835245345869668e-05 
		0.499962758853011 0.499962758853011;
	setAttr -s 4 ".wl[1781].w[11:14]"  6.8123223349919802e-05 0.00062889932022343743 
		0.4996514887282133 0.4996514887282133;
	setAttr -s 4 ".wl[1782].w[11:14]"  2.4264865785237099e-05 0.00024005524120754269 
		0.49986783994650358 0.49986783994650358;
	setAttr -s 4 ".wl[1783].w[11:14]"  5.8684383616920831e-05 0.00056508953159966821 
		0.49968811304239169 0.49968811304239169;
	setAttr -s 4 ".wl[1784].w[11:14]"  0.0001154514596613432 0.0010318172787316469 
		0.4994263656308035 0.4994263656308035;
	setAttr -s 4 ".wl[1785].w[11:14]"  4.5974400084440428e-05 0.00044526098145371377 
		0.49975438230923097 0.49975438230923097;
	setAttr -s 4 ".wl[1786].w[11:14]"  2.8902654673686697e-05 0.00028002202767101321 
		0.49984553765882761 0.49984553765882761;
	setAttr -s 4 ".wl[1787].w[11:14]"  4.7077916565215778e-05 0.00044799873574909968 
		0.49975246167384291 0.4997524616738428;
	setAttr -s 4 ".wl[1788].w[11:14]"  2.7296565266435586e-05 0.00025872972810004171 
		0.49985698685331675 0.49985698685331675;
	setAttr -s 4 ".wl[1789].w[11:14]"  2.7761322465258401e-05 0.00026234192975373307 
		0.49985494837389055 0.49985494837389055;
	setAttr -s 4 ".wl[1790].w[11:14]"  0.00010978525209681928 0.00098560553753243986 
		0.49945230460518536 0.49945230460518536;
	setAttr -s 4 ".wl[1791].w[11:14]"  6.3699803210449962e-05 0.00057799449914476878 
		0.49967915284882247 0.49967915284882236;
	setAttr -s 4 ".wl[1792].w[11:14]"  0.00019736291444925498 0.0017171095531276594 
		0.49904276376621159 0.49904276376621159;
	setAttr -s 4 ".wl[1793].w[11:14]"  0.00018134829652144792 0.0015658653511340544 
		0.4991263931761723 0.4991263931761723;
	setAttr -s 4 ".wl[1794].w[11:14]"  0.00018552595008898209 0.0015978941931136554 
		0.49910828992839867 0.49910828992839867;
	setAttr -s 4 ".wl[1795].w[11:14]"  9.5348469653450037e-05 0.00088957245550566741 
		0.49950753953742028 0.4995075395374205;
	setAttr -s 4 ".wl[1796].w[11:14]"  5.60994662668517e-05 0.00053405796423827289 
		0.49970492128474747 0.49970492128474747;
	setAttr -s 4 ".wl[1797].w[11:14]"  9.1436508669883772e-05 0.000841862772411651 
		0.49953335035945923 0.49953335035945923;
	setAttr -s 4 ".wl[1798].w[11:14]"  1.3966992616194983e-05 0.00013858392515996159 
		0.499923724541112 0.49992372454111189;
	setAttr -s 4 ".wl[1799].w[11:14]"  3.413331382373947e-05 0.00032561844506954791 
		0.49982012412055343 0.49982012412055332;
	setAttr -s 4 ".wl[1800].w[11:14]"  1.1328257548563192e-05 0.00011485315475733048 
		0.49993690929384704 0.49993690929384704;
	setAttr -s 4 ".wl[1801].w[11:14]"  1.3294015198232989e-05 0.00013114121904025928 
		0.49992778238288077 0.49992778238288077;
	setAttr -s 4 ".wl[1802].w[11:14]"  1.5416371084558843e-05 0.00015714586472853685 
		0.49991371888209346 0.49991371888209346;
	setAttr -s 4 ".wl[1803].w[11:14]"  1.8000127040416778e-05 0.00017958537649565121 
		0.499901207248232 0.499901207248232;
	setAttr -s 4 ".wl[1804].w[11:14]"  1.0105956090879323e-05 0.00010280523356453645 
		0.4999435444051723 0.4999435444051723;
	setAttr -s 4 ".wl[1805].w[11:14]"  1.7126725511276963e-05 0.00017118941049003734 
		0.49990584193199938 0.49990584193199938;
	setAttr -s 4 ".wl[1806].w[11:14]"  1.2230967402822002e-05 0.00012188617626265807 
		0.49993294142816724 0.49993294142816724;
	setAttr -s 4 ".wl[1807].w[11:14]"  1.1695900110820801e-05 0.00011585132012790566 
		0.49993622638988067 0.49993622638988056;
	setAttr -s 4 ".wl[1808].w[11:14]"  5.3074890511986749e-05 0.00050705223117379996 
		0.49971993643915713 0.49971993643915713;
	setAttr -s 4 ".wl[1809].w[11:14]"  3.141314763893348e-05 0.00030093639775362888 
		0.49983382522730374 0.49983382522730374;
	setAttr -s 4 ".wl[1810].w[11:14]"  8.921329249413708e-05 0.00082302207563668915 
		0.49954388231593461 0.49954388231593461;
	setAttr -s 4 ".wl[1811].w[11:14]"  0.0001305324103592141 0.001165110766428172 
		0.49935217841160634 0.49935217841160634;
	setAttr -s 4 ".wl[1812].w[11:14]"  8.0334770804638508e-05 0.00073230497328021513 
		0.49959368012795757 0.49959368012795757;
	setAttr -s 4 ".wl[1813].w[11:14]"  0.00013061229358599539 0.0011656129675960594 
		0.499351887369409 0.499351887369409;
	setAttr -s 4 ".wl[1814].w[11:14]"  2.4453593341802019e-05 0.00023269646189933124 
		0.49987142497237941 0.49987142497237941;
	setAttr -s 4 ".wl[1815].w[11:14]"  4.8183537512154912e-05 0.00044801648981606979 
		0.4997518999863359 0.4997518999863359;
	setAttr -s 4 ".wl[1816].w[11:14]"  2.1736392710920152e-05 0.00021137350181815723 
		0.49988344505273546 0.49988344505273546;
	setAttr -s 4 ".wl[1817].w[11:14]"  2.0062398585003848e-05 0.00019361500502119324 
		0.49989316129819689 0.49989316129819689;
	setAttr -s 4 ".wl[1818].w[11:14]"  2.8472957956686379e-05 0.00027831494482535727 
		0.49984660604860898 0.49984660604860898;
	setAttr -s 4 ".wl[1819].w[11:14]"  2.9801320298990325e-05 0.00029071468482722409 
		0.49983974199743697 0.49983974199743686;
	setAttr -s 4 ".wl[1820].w[11:14]"  1.9924204660738759e-05 0.00019444324986303907 
		0.49989281627273813 0.49989281627273813;
	setAttr -s 4 ".wl[1821].w[11:14]"  2.8517903190360367e-05 0.00027877918978934813 
		0.49984635145351014 0.49984635145351014;
	setAttr -s 4 ".wl[1822].w[11:14]"  2.1998410334887574e-05 0.00021030012393920137 
		0.49988385073286301 0.4998838507328629;
	setAttr -s 4 ".wl[1823].w[11:14]"  1.7830618872008114e-05 0.00017287770381658229 
		0.49990464583865568 0.49990464583865568;
	setAttr -s 4 ".wl[1824].w[11:14]"  7.6473935057890408e-05 0.00069973112572081786 
		0.49961189746961066 0.49961189746961066;
	setAttr -s 4 ".wl[1825].w[11:14]"  4.4496853754681645e-05 0.00041572003455080743 
		0.49976989155584728 0.49976989155584728;
	setAttr -s 4 ".wl[1826].w[11:14]"  0.00012754828733901476 0.001140832151753943 
		0.49936580978045347 0.49936580978045347;
	setAttr -s 4 ".wl[1827].w[11:14]"  0.00015238738632501595 0.0012933150809481646 
		0.49927714876636342 0.49927714876636342;
	setAttr -s 4 ".wl[1828].w[11:14]"  5.3118721846333218e-05 0.00047441429961447487 
		0.49973623348926965 0.49973623348926965;
	setAttr -s 4 ".wl[1829].w[11:14]"  5.4895488920702022e-05 0.00050076114605870936 
		0.49972217168251026 0.49972217168251026;
	setAttr -s 4 ".wl[1830].w[11:14]"  9.1328574578317668e-05 0.00079236763505489939 
		0.49955815189518338 0.49955815189518338;
	setAttr -s 4 ".wl[1831].w[11:14]"  7.6548487476823068e-05 0.00069824348669860208 
		0.4996126040129123 0.4996126040129123;
	setAttr -s 4 ".wl[1832].w[11:14]"  0.00018191574748156896 0.001445034292985564 
		0.49918652497976645 0.49918652497976645;
	setAttr -s 4 ".wl[1833].w[11:14]"  0.00032869248693845777 0.0025084170079660733 
		0.49858144525254777 0.49858144525254766;
	setAttr -s 4 ".wl[1834].w[11:14]"  0.0021801128913511098 0.01186132455579926 
		0.49297928127642482 0.49297928127642482;
	setAttr -s 4 ".wl[1835].w[11:14]"  0.00011542142195768917 0.00094578148306102209 
		0.49946939854749062 0.49946939854749062;
	setAttr -s 4 ".wl[1836].w[11:14]"  0.00013028530892358992 0.001060798942125217 
		0.49940445787447557 0.49940445787447557;
	setAttr -s 4 ".wl[1837].w[11:14]"  0.0019183114384832411 0.010656164345383108 
		0.4937127621080668 0.4937127621080668;
	setAttr -s 4 ".wl[1838].w[11:14]"  0.0019835086247005614 0.010958556248874035 
		0.49352896756321268 0.49352896756321268;
	setAttr -s 4 ".wl[1839].w[11:14]"  3.0886274378650716e-05 0.00029036004822520012 
		0.49983937683869806 0.49983937683869806;
	setAttr -s 4 ".wl[1840].w[11:14]"  8.3581778873214214e-05 0.00071883868898285457 
		0.49959878976607192 0.49959878976607192;
	setAttr -s 4 ".wl[1841].w[11:14]"  5.7659039248095969e-05 0.0005121650314779127 
		0.49971508796463698 0.49971508796463698;
	setAttr -s 4 ".wl[1842].w[11:14]"  6.8681931222542141e-05 0.00061986854741309801 
		0.49965572476068215 0.49965572476068215;
	setAttr -s 4 ".wl[1843].w[11:14]"  0.00015898695143457689 0.0013433373490787815 
		0.49924883784974333 0.49924883784974333;
	setAttr -s 4 ".wl[1844].w[11:14]"  3.0586289744545564e-05 0.00028832101314282978 
		0.49984054634855629 0.49984054634855629;
	setAttr -s 4 ".wl[1845].w[11:14]"  3.1534335401673157e-05 0.00030422523243831787 
		0.49983212021607998 0.49983212021607998;
	setAttr -s 4 ".wl[1846].w[11:14]"  0.00027963613894792878 0.0022173201094996746 
		0.49875152187577604 0.49875152187577626;
	setAttr -s 4 ".wl[1847].w[11:14]"  4.8943136496299617e-05 0.00046465708968713613 
		0.4997431998869083 0.4997431998869083;
	setAttr -s 4 ".wl[1848].w[11:14]"  0.00010794259015898183 0.00093797222240228661 
		0.49947704259371933 0.49947704259371933;
	setAttr -s 4 ".wl[1849].w[11:14]"  5.8490897640003815e-05 0.00053131573730894022 
		0.49970509668252561 0.4997050966825255;
	setAttr -s 4 ".wl[1850].w[11:14]"  0.00024892368825587277 0.002051282860945255 
		0.49884989672539942 0.49884989672539942;
	setAttr -s 4 ".wl[1851].w[11:14]"  0.00027886663006168155 0.0022150132679800783 
		0.49875306005097914 0.49875306005097914;
	setAttr -s 4 ".wl[1852].w[11:14]"  0.0022724821467034221 0.012280338916663249 
		0.49272358946831668 0.49272358946831668;
	setAttr -s 4 ".wl[1853].w[11:14]"  0.0021226891020257686 0.011579396579109544 
		0.49314895715943241 0.4931489571594323;
	setAttr -s 4 ".wl[1854].w[11:14]"  0.0019526983872057721 0.010808082127315873 
		0.49361960974273916 0.49361960974273916;
	setAttr -s 4 ".wl[1855].w[11:14]"  0.00033349295375796769 0.0025362235160890194 
		0.49856514176507655 0.49856514176507655;
	setAttr -s 4 ".wl[1856].w[11:14]"  0.00017386981598806263 0.0013984785314439588 
		0.49921382582628399 0.49921382582628399;
	setAttr -s 4 ".wl[1857].w[11:14]"  0.00015369180237924512 0.0012264817537786896 
		0.49930991322192103 0.49930991322192103;
	setAttr -s 4 ".wl[1858].w[11:14]"  0.0021513749737035351 0.011720560472439212 
		0.49306403227692858 0.49306403227692858;
	setAttr -s 4 ".wl[1859].w[11:14]"  0.00013793398072376885 0.0011425372823519275 
		0.49935976436846213 0.49935976436846213;
	setAttr -s 4 ".wl[1860].w[11:14]"  8.3856407934729824e-05 0.00070999234405144662 
		0.49960307562400696 0.49960307562400696;
	setAttr -s 4 ".wl[1861].w[11:14]"  0.00032136169678444383 0.0024738510122506935 
		0.49860239364548248 0.49860239364548248;
	setAttr -s 4 ".wl[1862].w[11:14]"  0.00010478454595262919 0.00086302096672956189 
		0.49951609724365897 0.49951609724365886;
	setAttr -s 4 ".wl[1863].w[11:14]"  0.00030259236363086464 0.0022589165486370647 
		0.49871924554386599 0.49871924554386599;
	setAttr -s 4 ".wl[1864].w[11:14]"  0.00027464149263481445 0.0021388012549831891 
		0.49879327862619099 0.49879327862619099;
	setAttr -s 4 ".wl[1865].w[11:14]"  0.00049270819373016246 0.0035498993805724625 
		0.49797869621284874 0.49797869621284863;
	setAttr -s 4 ".wl[1866].w[11:14]"  0.00014231582982802412 0.0011593856242533976 
		0.49934914927295931 0.49934914927295931;
	setAttr -s 4 ".wl[1867].w[11:14]"  0.00032239750172718582 0.0024063809688061504 
		0.49863561076473339 0.49863561076473339;
	setAttr -s 4 ".wl[1868].w[11:14]"  0.00014070267373527385 0.0011795337438391261 
		0.49933988179121275 0.49933988179121275;
	setAttr -s 4 ".wl[1869].w[11:14]"  0.00013991616735168144 0.0011649748728817346 
		0.49934755447988333 0.49934755447988333;
	setAttr -s 4 ".wl[1870].w[11:14]"  0.00026328207204484924 0.0020578464592645308 
		0.4988394357343453 0.4988394357343453;
	setAttr -s 4 ".wl[1871].w[11:14]"  0.00019384826741589568 0.0015184371476277278 
		0.49914385729247818 0.49914385729247818;
	setAttr -s 4 ".wl[1872].w[11:14]"  0.0002399152736197022 0.0018526507388826751 
		0.4989537169937488 0.4989537169937488;
	setAttr -s 4 ".wl[1873].w[11:14]"  0.00087877517285177243 0.0056829787153670833 
		0.49671912305589061 0.49671912305589061;
	setAttr -s 4 ".wl[1874].w[11:14]"  0.00052480668089863061 0.0035838989748322213 
		0.49794564717213463 0.49794564717213463;
	setAttr -s 4 ".wl[1875].w[11:14]"  0.001299110444179504 0.0077400844802930123 
		0.49548040253776382 0.49548040253776371;
	setAttr -s 4 ".wl[1876].w[11:14]"  0.00082690009417073077 0.0053896264516026847 
		0.49689173672711329 0.49689173672711329;
	setAttr -s 4 ".wl[1877].w[11:14]"  0.0015992024180759093 0.0092798211803495258 
		0.4945604882007873 0.4945604882007873;
	setAttr -s 4 ".wl[1878].w[11:14]"  0.00061413971357397929 0.0041496240527679222 
		0.49761811811682904 0.49761811811682904;
	setAttr -s 4 ".wl[1879].w[11:14]"  0.0013317437409544972 0.0079345804587595981 
		0.49536683790014296 0.49536683790014296;
	setAttr -s 4 ".wl[1880].w[11:14]"  0.00058429966289980084 0.0040564295728445351 
		0.49767963538212784 0.49767963538212784;
	setAttr -s 4 ".wl[1881].w[11:14]"  0.00059894590779620152 0.0041269369417750836 
		0.49763705857521434 0.49763705857521434;
	setAttr -s 4 ".wl[1882].w[11:14]"  0.0012084053627525013 0.0074761777315762758 
		0.49565770845283563 0.49565770845283563;
	setAttr -s 4 ".wl[1883].w[11:14]"  0.0010886759452778351 0.0067360233186260646 
		0.49608765036804803 0.49608765036804803;
	setAttr -s 4 ".wl[1884].w[11:14]"  0.0011785795990348414 0.0072120363995411465 
		0.49580469200071203 0.49580469200071203;
	setAttr -s 4 ".wl[1885].w[11:14]"  0.0021002359111222391 0.011530593929583617 
		0.49318458507964713 0.49318458507964713;
	setAttr -s 4 ".wl[1886].w[11:14]"  0.0016057809474938393 0.0092071970981643693 
		0.49459351097717091 0.49459351097717091;
	setAttr -s 4 ".wl[1887].w[11:14]"  0.0021301126317910791 0.011597093201049551 
		0.49313639708357965 0.49313639708357965;
	setAttr -s 4 ".wl[1888].w[11:14]"  0.0020115959695734785 0.011122928549815022 
		0.49343273774030583 0.49343273774030572;
	setAttr -s 4 ".wl[1889].w[11:14]"  0.0023605360621857391 0.012643516917226033 
		0.49249797351029406 0.49249797351029406;
	setAttr -s 4 ".wl[1890].w[11:14]"  0.0017087117518337438 0.009729489240270596 
		0.49428089950394788 0.49428089950394788;
	setAttr -s 4 ".wl[1891].w[11:14]"  0.0021266239759077099 0.011594893024285001 
		0.49313924149990368 0.49313924149990368;
	setAttr -s 4 ".wl[1892].w[11:14]"  0.0020424949369325994 0.011221093559023515 
		0.49336820575202195 0.49336820575202195;
	setAttr -s 4 ".wl[1893].w[11:14]"  0.0016722502156771629 0.0097362202129600735 
		0.49429576478568138 0.49429576478568138;
	setAttr -s 4 ".wl[1894].w[11:14]"  0.0016801266394690397 0.0097275482617257322 
		0.49429616254940262 0.49429616254940262;
	setAttr -s 4 ".wl[1895].w[11:14]"  0.0019600650729311555 0.011043103717684822 
		0.49349841560469188 0.4934984156046921;
	setAttr -s 4 ".wl[1896].w[11:14]"  0.0018817096696763672 0.010585562970542989 
		0.4937663636798903 0.4937663636798903;
	setAttr -s 4 ".wl[1897].w[11:14]"  0.0019333177921280326 0.010810841992197305 
		0.49362792010783735 0.49362792010783735;
	setAttr -s 4 ".wl[1898].w[11:14]"  0.00067183038906274957 0.0044396689040499876 
		0.49744425035344347 0.4974442503534437;
	setAttr -s 4 ".wl[1899].w[11:14]"  0.00076777332983357302 0.0049959632521603464 
		0.49711813170900304 0.49711813170900304;
	setAttr -s 4 ".wl[1900].w[11:14]"  0.00028595845967812071 0.0021209622260564445 
		0.49879653965713272 0.49879653965713272;
	setAttr -s 4 ".wl[1901].w[11:14]"  0.00093936957423160813 0.0059850246879731769 
		0.49653780286889759 0.49653780286889759;
	setAttr -s 4 ".wl[1902].w[11:14]"  0.00041353902389764183 0.0029795023545895862 
		0.49830347931075636 0.49830347931075636;
	setAttr -s 4 ".wl[1903].w[11:14]"  0.0010200863139314296 0.0064530712639643134 
		0.4962634212110521 0.4962634212110521;
	setAttr -s 4 ".wl[1904].w[11:14]"  0.00055216518249759415 0.0038922929110665901 
		0.4977777709532179 0.4977777709532179;
	setAttr -s 4 ".wl[1905].w[11:14]"  0.00081788870637477802 0.0053015541976626848 
		0.49694027854798128 0.49694027854798128;
	setAttr -s 4 ".wl[1906].w[11:14]"  0.00046350669685640735 0.0033207181947238779 
		0.49810788755420987 0.49810788755420987;
	setAttr -s 4 ".wl[1907].w[11:14]"  0.00071278771360540902 0.0046890565643604634 
		0.49729907786101707 0.49729907786101707;
	setAttr -s 4 ".wl[1908].w[11:14]"  0.00032903166067259522 0.0024240403246495131 
		0.49862346400733892 0.49862346400733892;
	setAttr -s 4 ".wl[1909].w[11:14]"  0.00027114725292800293 0.002025259006151814 
		0.49885179687046011 0.49885179687046011;
	setAttr -s 4 ".wl[1910].w[11:14]"  0.001695765252235328 0.0096322376962851061 
		0.49433599852573984 0.49433599852573984;
	setAttr -s 4 ".wl[1911].w[11:14]"  0.0018271571423602868 0.01024910110032619 
		0.49396187087865678 0.49396187087865678;
	setAttr -s 4 ".wl[1912].w[11:14]"  0.001312063313845145 0.0077980017739041618 
		0.49544496745612537 0.49544496745612537;
	setAttr -s 4 ".wl[1913].w[11:14]"  0.0020407868051374074 0.011260731692288643 
		0.493349240751287 0.493349240751287;
	setAttr -s 4 ".wl[1914].w[11:14]"  0.0014909484744214335 0.0087046541642350241 
		0.49490219868067181 0.49490219868067181;
	setAttr -s 4 ".wl[1915].w[11:14]"  0.0021025338032389955 0.011560609371180997 
		0.49316842841279007 0.49316842841278996;
	setAttr -s 4 ".wl[1916].w[11:14]"  0.0016730606002506665 0.0096271829639764366 
		0.49434987821788645 0.49434987821788645;
	setAttr -s 4 ".wl[1917].w[11:14]"  0.001865640815057011 0.010446303829332289 
		0.49384402767780539 0.49384402767780539;
	setAttr -s 4 ".wl[1918].w[11:14]"  0.001563423374567396 0.009086000091629633 
		0.49467528826690149 0.49467528826690149;
	setAttr -s 4 ".wl[1919].w[11:14]"  0.0017362014955456257 0.0098327597420614053 
		0.49421551938119657 0.49421551938119646;
	setAttr -s 4 ".wl[1920].w[11:14]"  0.0013647205185600383 0.0080822274107720006 
		0.49527652603533401 0.49527652603533401;
	setAttr -s 4 ".wl[1921].w[11:14]"  0.0012793547917215929 0.0076412360046547159 
		0.49553970460181179 0.49553970460181179;
	setAttr -s 4 ".wl[1922].w[11:14]"  2.1704505091054021e-05 0.00021623055162086561 
		0.499881032471644 0.499881032471644;
	setAttr -s 4 ".wl[1923].w[11:14]"  3.8242016114055015e-06 3.9731514706322556e-05 
		0.49997822214184112 0.49997822214184112;
	setAttr -s 4 ".wl[1924].w[11:14]"  4.2779798816371963e-06 4.4354955776437571e-05 
		0.49997568353217098 0.49997568353217098;
	setAttr -s 4 ".wl[1925].w[11:14]"  2.2837409059680563e-05 0.00022703439527815299 
		0.49987506409783106 0.49987506409783106;
	setAttr -s 4 ".wl[1926].w[11:14]"  0.00014457618272686185 0.0012856783133926 
		0.49928487275194028 0.49928487275194028;
	setAttr -s 4 ".wl[1927].w[11:14]"  4.8127957453788021e-05 0.00044867468905739706 
		0.49975159867674446 0.49975159867674435;
	setAttr -s 4 ".wl[1928].w[11:14]"  1.8828929805656805e-05 0.00018257135487234521 
		0.49989929985766085 0.49989929985766107;
	setAttr -s 4 ".wl[1929].w[11:14]"  3.2492297155689791e-05 0.00031747041052625089 
		0.49982501864615908 0.49982501864615908;
	setAttr -s 4 ".wl[1930].w[11:14]"  3.3953657440221165e-05 0.00033101180689218954 
		0.4998175172678338 0.4998175172678338;
	setAttr -s 4 ".wl[1931].w[11:14]"  2.1269492852637549e-05 0.00020522278513337557 
		0.49988675386100695 0.49988675386100695;
	setAttr -s 4 ".wl[1932].w[11:14]"  5.221893339832818e-05 0.00048434650899458631 
		0.49973171727880356 0.49973171727880356;
	setAttr -s 4 ".wl[1933].w[11:14]"  0.000148079572974208 0.0013136849885766512 
		0.49926911771922455 0.49926911771922455;
	setAttr -s 4 ".wl[1934].w[11:14]"  9.500370416391515e-05 0.00086617880991692877 
		0.49951940874295958 0.49951940874295958;
	setAttr -s 4 ".wl[1935].w[11:14]"  3.5245411160733619e-05 0.00033353378425233181 
		0.4998156104022935 0.4998156104022935;
	setAttr -s 4 ".wl[1936].w[11:14]"  1.4190487708883827e-05 0.00013879548078777525 
		0.49992350701575172 0.49992350701575161;
	setAttr -s 4 ".wl[1937].w[11:14]"  2.0044809763337364e-05 0.00019783375653170074 
		0.49989106071685252 0.49989106071685252;
	setAttr -s 4 ".wl[1938].w[11:14]"  2.0987247593813447e-05 0.00020675092300288315 
		0.49988613091470169 0.49988613091470169;
	setAttr -s 4 ".wl[1939].w[11:14]"  1.594298337963234e-05 0.00015529772461042005 
		0.49991437964600505 0.49991437964600505;
	setAttr -s 4 ".wl[1940].w[11:14]"  3.8117268690497417e-05 0.00035919400938946454 
		0.49980134436095985 0.49980134436096008;
	setAttr -s 4 ".wl[1941].w[11:14]"  9.7290483908141705e-05 0.00088527639847132327 
		0.49950871655881035 0.49950871655881035;
	setAttr -s 4 ".wl[1942].w[11:14]"  3.9626783652099656e-05 0.00038422386336983314 
		0.49978807467648911 0.499788074676489;
	setAttr -s 4 ".wl[1943].w[11:14]"  2.1871949028733554e-05 0.00021060625942304916 
		0.49988376089577397 0.49988376089577419;
	setAttr -s 4 ".wl[1944].w[11:14]"  5.501304073954662e-05 0.00050828238556579976 
		0.49971835228684736 0.49971835228684736;
	setAttr -s 4 ".wl[1945].w[11:14]"  0.00016821150569626986 0.0014774655528372444 
		0.49917716147073332 0.49917716147073321;
	setAttr -s 4 ".wl[1946].w[11:14]"  0.00017225400487486559 0.0015091383968249587 
		0.49915930379915008 0.49915930379915008;
	setAttr -s 4 ".wl[1947].w[11:14]"  8.3434259630416017e-05 0.0007824657568347884 
		0.49956704999176738 0.49956704999176738;
	setAttr -s 4 ".wl[1948].w[11:14]"  2.8804937084366962e-05 0.00028011065995741372 
		0.49984554220147909 0.49984554220147909;
	setAttr -s 4 ".wl[1949].w[11:14]"  9.9012618313227726e-06 9.962430925481725e-05 
		0.49994523721445688 0.49994523721445688;
	setAttr -s 4 ".wl[1950].w[11:14]"  1.4105767977790576e-05 0.00014355149331030185 
		0.49992117136935593 0.49992117136935593;
	setAttr -s 4 ".wl[1951].w[11:14]"  1.3331426130614005e-05 0.0001359238123916925 
		0.49992537238073881 0.49992537238073881;
	setAttr -s 4 ".wl[1952].w[11:14]"  8.525228110044685e-06 8.6132552836971441e-05 
		0.49995267110952657 0.49995267110952646;
	setAttr -s 4 ".wl[1953].w[11:14]"  2.6301207777524242e-05 0.00025684726609611003 
		0.49985842576306305 0.49985842576306327;
	setAttr -s 4 ".wl[1954].w[11:14]"  8.1299856389155847e-05 0.00076396502120303837 
		0.49957736756120391 0.49957736756120391;
	setAttr -s 4 ".wl[1955].w[11:14]"  0.00011537643834324571 0.0010354158763556129 
		0.49942460384265058 0.49942460384265058;
	setAttr -s 4 ".wl[1956].w[11:14]"  4.4575842539963245e-05 0.0004154342481339651 
		0.49976999495466318 0.49976999495466295;
	setAttr -s 4 ".wl[1957].w[11:14]"  1.9022758086193777e-05 0.00018360525214317056 
		0.49989868599488529 0.49989868599488529;
	setAttr -s 4 ".wl[1958].w[11:14]"  2.6337020626669652e-05 0.00025703011809539148 
		0.49985831643063905 0.49985831643063894;
	setAttr -s 4 ".wl[1959].w[11:14]"  2.5207289213405667e-05 0.00024649138763220243 
		0.49986415066157736 0.49986415066157713;
	setAttr -s 4 ".wl[1960].w[11:14]"  1.6979389652694968e-05 0.00016460065742239635 
		0.49990920997646249 0.49990920997646249;
	setAttr -s 4 ".wl[1961].w[11:14]"  4.1253692532320626e-05 0.00038620377225772565 
		0.49978627126760494 0.49978627126760494;
	setAttr -s 4 ".wl[1962].w[11:14]"  0.000112697747502699 0.0010135128819029845 
		0.49943689468529717 0.49943689468529717;
	setAttr -s 4 ".wl[1963].w[11:14]"  8.8390991366379273e-05 0.00077066590157543662 
		0.49957047155352913 0.49957047155352913;
	setAttr -s 4 ".wl[1964].w[11:14]"  4.5560560027413702e-05 0.00041307915427376841 
		0.49977068014284942 0.49977068014284942;
	setAttr -s 4 ".wl[1965].w[11:14]"  6.8627269995389685e-05 0.00062662989241275531 
		0.49965237141879593 0.49965237141879593;
	setAttr -s 4 ".wl[1966].w[11:14]"  0.00024797757220365932 0.0019299455804394621 
		0.4989110384236784 0.4989110384236784;
	setAttr -s 4 ".wl[1967].w[11:14]"  0.00012047757796898636 0.00098678501896161939 
		0.49944636870153469 0.49944636870153469;
	setAttr -s 4 ".wl[1968].w[11:14]"  0.00014737249246723371 0.0011881245385546621 
		0.49933225148448912 0.49933225148448901;
	setAttr -s 4 ".wl[1969].w[11:14]"  4.9538454584842485e-05 0.00044683102784144087 
		0.4997518152587867 0.49975181525878692;
	setAttr -s 4 ".wl[1970].w[11:14]"  9.4224251492923679e-05 0.00081716550718411839 
		0.49954430512066145 0.49954430512066145;
	setAttr -s 4 ".wl[1971].w[11:14]"  5.9661173792485987e-05 0.00054827159630035309 
		0.49969603361495357 0.49969603361495357;
	setAttr -s 4 ".wl[1972].w[11:14]"  2.4658610320703481e-05 0.00023620631490321872 
		0.49986956753738804 0.49986956753738804;
	setAttr -s 4 ".wl[1973].w[11:14]"  4.1342879735382319e-05 0.00039992224542809412 
		0.49977936743741824 0.49977936743741824;
	setAttr -s 4 ".wl[1974].w[11:14]"  0.0002172488267545734 0.0018063646395352414 
		0.49898819326685512 0.49898819326685512;
	setAttr -s 4 ".wl[1975].w[11:14]"  7.0871052043169111e-05 0.00064560935574083577 
		0.49964175979610798 0.49964175979610798;
	setAttr -s 4 ".wl[1976].w[11:14]"  0.00022178343202239816 0.0018394587343462073 
		0.49896937891681564 0.49896937891681564;
	setAttr -s 4 ".wl[1977].w[11:14]"  0.0022364115833985693 0.012097273334314864 
		0.49283315754114326 0.49283315754114326;
	setAttr -s 4 ".wl[1978].w[11:14]"  0.0020389183443218417 0.011198895815031659 
		0.49338109292032328 0.49338109292032328;
	setAttr -s 4 ".wl[1979].w[11:14]"  0.00036778554399225275 0.0027810942614145506 
		0.4984255600972966 0.4984255600972966;
	setAttr -s 4 ".wl[1980].w[11:14]"  0.00023383351139883725 0.0018189164390602435 
		0.4989736250247705 0.4989736250247705;
	setAttr -s 4 ".wl[1981].w[11:14]"  0.00011904738623655191 0.0009675956640118142 
		0.49945667847487585 0.49945667847487585;
	setAttr -s 4 ".wl[1982].w[11:14]"  0.0022275713746567668 0.012025237632602593 
		0.49287359549637033 0.49287359549637033;
	setAttr -s 4 ".wl[1983].w[11:14]"  0.0020027984940761134 0.01112637905812492 
		0.4934354112238995 0.4934354112238995;
	setAttr -s 4 ".wl[1984].w[11:14]"  0.00018684548473900259 0.0014869395259518269 
		0.49916310749465459 0.49916310749465459;
	setAttr -s 4 ".wl[1985].w[11:14]"  0.00034425148985253284 0.0026385649407596502 
		0.49850859178469398 0.49850859178469387;
	setAttr -s 4 ".wl[1986].w[11:14]"  0.00019365756226999661 0.0015436030377171909 
		0.49913136970000649 0.49913136970000638;
	setAttr -s 4 ".wl[1987].w[11:14]"  0.00015295658402493623 0.0012785848384063811 
		0.49928422928878419 0.49928422928878441;
	setAttr -s 4 ".wl[1988].w[11:14]"  0.00010147495414931969 0.00085002105974224442 
		0.49952425199305422 0.49952425199305422;
	setAttr -s 4 ".wl[1989].w[11:14]"  0.00012911777187223918 0.0010671600083385132 
		0.49940186110989465 0.49940186110989465;
	setAttr -s 4 ".wl[1990].w[11:14]"  0.00067184773618333461 0.0044621252560491159 
		0.4974330135038838 0.4974330135038838;
	setAttr -s 4 ".wl[1991].w[11:14]"  0.00091942483049721913 0.0059214485483075167 
		0.49657956331059777 0.49657956331059755;
	setAttr -s 4 ".wl[1992].w[11:14]"  0.00070327497436380084 0.0046684633166465999 
		0.49731413085449483 0.49731413085449483;
	setAttr -s 4 ".wl[1993].w[11:14]"  0.00061214047826652453 0.0042374196957109325 
		0.49757521991301129 0.49757521991301129;
	setAttr -s 4 ".wl[1994].w[11:14]"  0.00051395230089988154 0.0035619460681436598 
		0.49796205081547829 0.49796205081547817;
	setAttr -s 4 ".wl[1995].w[11:14]"  0.00058623520985309405 0.0040103978661292011 
		0.49770168346200888 0.49770168346200888;
	setAttr -s 4 ".wl[1996].w[11:14]"  0.0018224736805426778 0.010217527555973658 
		0.49397999938174181 0.49397999938174181;
	setAttr -s 4 ".wl[1997].w[11:14]"  0.0021435803941416915 0.011735382820654191 
		0.49306051839260207 0.49306051839260207;
	setAttr -s 4 ".wl[1998].w[11:14]"  0.0018381714087824432 0.010316185445844474 
		0.49392282157268652 0.49392282157268652;
	setAttr -s 4 ".wl[1999].w[11:14]"  0.0017030714962322758 0.0098979668265844045 
		0.49419948083859166 0.49419948083859166;
	setAttr -s 4 ".wl[2000].w[11:14]"  0.001580476967594445 0.0091782681869146746 
		0.49462062742274543 0.49462062742274543;
	setAttr -s 4 ".wl[2001].w[11:14]"  0.0016640273770269357 0.0095742895116317263 
		0.49438084155567069 0.49438084155567069;
	setAttr -s 4 ".wl[2002].w[11:14]"  0.00070332408661924809 0.0046211879033009736 
		0.49733774400503988 0.49733774400503988;
	setAttr -s 4 ".wl[2003].w[11:14]"  0.00085067989487196724 0.0054754701760905836 
		0.49683692496451876 0.49683692496451876;
	setAttr -s 4 ".wl[2004].w[11:14]"  0.0010124357493975272 0.0064042920993508442 
		0.49629163607562587 0.49629163607562576;
	setAttr -s 4 ".wl[2005].w[11:14]"  0.00092382857584165053 0.005909221315695456 
		0.4965834750542315 0.49658347505423139;
	setAttr -s 4 ".wl[2006].w[11:14]"  0.00075492397884619725 0.0049366437033875844 
		0.49715421615888311 0.49715421615888311;
	setAttr -s 4 ".wl[2007].w[11:14]"  0.0006801627245696894 0.0044933299496528051 
		0.49741325366288874 0.49741325366288874;
	setAttr -s 4 ".wl[2008].w[11:14]"  0.0017408562689013314 0.0098416029067874729 
		0.49420877041215544 0.49420877041215566;
	setAttr -s 4 ".wl[2009].w[11:14]"  0.0019346919253478802 0.010758290507594108 
		0.49365350878352898 0.49365350878352898;
	setAttr -s 4 ".wl[2010].w[11:14]"  0.0021109032243457758 0.011594871744173422 
		0.49314711251574045 0.49314711251574045;
	setAttr -s 4 ".wl[2011].w[11:14]"  0.0019896343291472105 0.011031567400158053 
		0.4934893991353474 0.4934893991353474;
	setAttr -s 4 ".wl[2012].w[11:14]"  0.001787449554719622 0.010076568321140861 
		0.49406799106206978 0.49406799106206978;
	setAttr -s 4 ".wl[2013].w[11:14]"  0.0017007286560985645 0.0096608200316127234 
		0.49431922565614433 0.49431922565614433;
	setAttr -s 4 ".wl[2014].w";
	setAttr ".wl[2014].w[0]" 0.39344144852909635;
	setAttr ".wl[2014].w[15]" 0.33885905219617357;
	setAttr ".wl[2014].w[16]" 0.026024607575668147;
	setAttr ".wl[2014].w[18]" 0.24167489169906195;
	setAttr -s 4 ".wl[2015].w";
	setAttr ".wl[2015].w[0]" 0.39482728454798383;
	setAttr ".wl[2015].w[15]" 0.24516781064077176;
	setAttr ".wl[2015].w[18]" 0.3352985968139876;
	setAttr ".wl[2015].w[19]" 0.024706307997256833;
	setAttr -s 4 ".wl[2016].w";
	setAttr ".wl[2016].w[0]" 0.41993066634812903;
	setAttr ".wl[2016].w[1]" 0.030242141192436321;
	setAttr ".wl[2016].w[15]" 0.32292838942401353;
	setAttr ".wl[2016].w[18]" 0.22689880303542115;
	setAttr -s 4 ".wl[2017].w";
	setAttr ".wl[2017].w[0]" 0.42038859112520405;
	setAttr ".wl[2017].w[1]" 0.030365753429342578;
	setAttr ".wl[2017].w[15]" 0.23471735666459476;
	setAttr ".wl[2017].w[18]" 0.31452829878085858;
	setAttr -s 4 ".wl[2018].w";
	setAttr ".wl[2018].w[0]" 0.47355288804538054;
	setAttr ".wl[2018].w[1]" 0.017631174142215256;
	setAttr ".wl[2018].w[15]" 0.31511368850571708;
	setAttr ".wl[2018].w[18]" 0.19370224930668714;
	setAttr -s 4 ".wl[2019].w";
	setAttr ".wl[2019].w[0]" 0.47523544653558858;
	setAttr ".wl[2019].w[1]" 0.017751514737596868;
	setAttr ".wl[2019].w[15]" 0.20291746176057002;
	setAttr ".wl[2019].w[18]" 0.30409557696624462;
	setAttr -s 4 ".wl[2020].w";
	setAttr ".wl[2020].w[0]" 0.43004549222602872;
	setAttr ".wl[2020].w[15]" 0.34103496111079007;
	setAttr ".wl[2020].w[16]" 0.015215823719499052;
	setAttr ".wl[2020].w[18]" 0.21370372294368217;
	setAttr -s 4 ".wl[2021].w";
	setAttr ".wl[2021].w[0]" 0.43223206509285966;
	setAttr ".wl[2021].w[15]" 0.21755827170781231;
	setAttr ".wl[2021].w[18]" 0.33584396683755668;
	setAttr ".wl[2021].w[19]" 0.014365696361771379;
	setAttr -s 4 ".wl[2022].w";
	setAttr ".wl[2022].w[0]" 0.37330207857886755;
	setAttr ".wl[2022].w[15]" 0.30116965347012042;
	setAttr ".wl[2022].w[16]" 0.027349358046111667;
	setAttr ".wl[2022].w[18]" 0.29817890990490048;
	setAttr -s 4 ".wl[2023].w";
	setAttr ".wl[2023].w[0]" 0.41128698047302681;
	setAttr ".wl[2023].w[1]" 0.034372854293978301;
	setAttr ".wl[2023].w[15]" 0.28151337561950157;
	setAttr ".wl[2023].w[18]" 0.27282678961349333;
	setAttr -s 4 ".wl[2024].w";
	setAttr ".wl[2024].w[0]" 0.4827172260614247;
	setAttr ".wl[2024].w[1]" 0.017806840311314725;
	setAttr ".wl[2024].w[15]" 0.25551532976331887;
	setAttr ".wl[2024].w[18]" 0.24396060386394172;
	setAttr -s 4 ".wl[2025].w";
	setAttr ".wl[2025].w[0]" 0.41248772916833693;
	setAttr ".wl[2025].w[15]" 0.28867500726833378;
	setAttr ".wl[2025].w[16]" 0.014259842599842179;
	setAttr ".wl[2025].w[18]" 0.28457742096348709;
	setAttr -s 4 ".wl[2026].w";
	setAttr ".wl[2026].w[0]" 0.40503514895561954;
	setAttr ".wl[2026].w[1]" 0.027132027611652707;
	setAttr ".wl[2026].w[15]" 0.34207083457451271;
	setAttr ".wl[2026].w[18]" 0.22576198885821488;
	setAttr -s 4 ".wl[2027].w";
	setAttr ".wl[2027].w[0]" 0.40561886389603663;
	setAttr ".wl[2027].w[1]" 0.027192619197586088;
	setAttr ".wl[2027].w[15]" 0.23110740586430362;
	setAttr ".wl[2027].w[18]" 0.33608111104207355;
	setAttr -s 4 ".wl[2028].w";
	setAttr ".wl[2028].w[0]" 0.44848909987793106;
	setAttr ".wl[2028].w[1]" 0.024536705853696193;
	setAttr ".wl[2028].w[15]" 0.32920139979028129;
	setAttr ".wl[2028].w[18]" 0.19777279447809148;
	setAttr -s 4 ".wl[2029].w";
	setAttr ".wl[2029].w[0]" 0.44981198470118194;
	setAttr ".wl[2029].w[1]" 0.024709135449848686;
	setAttr ".wl[2029].w[15]" 0.20648093735931766;
	setAttr ".wl[2029].w[18]" 0.31899794248965163;
	setAttr -s 4 ".wl[2030].w";
	setAttr ".wl[2030].w[0]" 0.46148119358823991;
	setAttr ".wl[2030].w[1]" 0.013411002611344144;
	setAttr ".wl[2030].w[15]" 0.34223272873240562;
	setAttr ".wl[2030].w[18]" 0.18287507506801037;
	setAttr -s 4 ".wl[2031].w";
	setAttr ".wl[2031].w[0]" 0.46373541133960788;
	setAttr ".wl[2031].w[1]" 0.013469402644960873;
	setAttr ".wl[2031].w[15]" 0.18930268919601828;
	setAttr ".wl[2031].w[18]" 0.3334924968194129;
	setAttr -s 4 ".wl[2032].w";
	setAttr ".wl[2032].w[0]" 0.4083292588254881;
	setAttr ".wl[2032].w[15]" 0.35339327631500028;
	setAttr ".wl[2032].w[16]" 0.021657038928915812;
	setAttr ".wl[2032].w[18]" 0.21662042593059588;
	setAttr -s 4 ".wl[2033].w";
	setAttr ".wl[2033].w[0]" 0.41027601945395614;
	setAttr ".wl[2033].w[15]" 0.2193746053068886;
	setAttr ".wl[2033].w[18]" 0.34988607846289571;
	setAttr ".wl[2033].w[19]" 0.020463296776259569;
	setAttr -s 4 ".wl[2034].w";
	setAttr ".wl[2034].w[0]" 0.38532286161986062;
	setAttr ".wl[2034].w[1]" 0.03108105011641955;
	setAttr ".wl[2034].w[15]" 0.29466258702728848;
	setAttr ".wl[2034].w[18]" 0.2889335012364313;
	setAttr -s 4 ".wl[2035].w";
	setAttr ".wl[2035].w[0]" 0.45055713973347405;
	setAttr ".wl[2035].w[1]" 0.027952946180210173;
	setAttr ".wl[2035].w[15]" 0.2662212960786795;
	setAttr ".wl[2035].w[18]" 0.25526861800763634;
	setAttr -s 4 ".wl[2036].w";
	setAttr ".wl[2036].w[0]" 0.46124401872181087;
	setAttr ".wl[2036].w[1]" 0.01238299012216088;
	setAttr ".wl[2036].w[15]" 0.26758537354618039;
	setAttr ".wl[2036].w[18]" 0.25878761760984786;
	setAttr -s 4 ".wl[2037].w";
	setAttr ".wl[2037].w[0]" 0.38116036982902357;
	setAttr ".wl[2037].w[15]" 0.29929885940808532;
	setAttr ".wl[2037].w[16]" 0.022336843173113873;
	setAttr ".wl[2037].w[18]" 0.29720392758977721;
	setAttr -s 4 ".wl[2038].w";
	setAttr ".wl[2038].w[0]" 0.43378223544380645;
	setAttr ".wl[2038].w[1]" 0.019306921945380664;
	setAttr ".wl[2038].w[15]" 0.19022558527711275;
	setAttr ".wl[2038].w[18]" 0.35668525733370016;
	setAttr -s 4 ".wl[2039].w";
	setAttr ".wl[2039].w[0]" 0.43228658543868986;
	setAttr ".wl[2039].w[1]" 0.019228357173480426;
	setAttr ".wl[2039].w[15]" 0.36369765138038201;
	setAttr ".wl[2039].w[18]" 0.18478740600744767;
	setAttr -s 4 ".wl[2040].w";
	setAttr ".wl[2040].w[0]" 0.38333771848082054;
	setAttr ".wl[2040].w[15]" 0.32385763264383333;
	setAttr ".wl[2040].w[16]" 0.027381918672766726;
	setAttr ".wl[2040].w[18]" 0.26542273020257939;
	setAttr -s 4 ".wl[2041].w";
	setAttr ".wl[2041].w[0]" 0.38461815060830001;
	setAttr ".wl[2041].w[15]" 0.26860785982821278;
	setAttr ".wl[2041].w[18]" 0.32075769903590601;
	setAttr ".wl[2041].w[19]" 0.026016290527581237;
	setAttr -s 4 ".wl[2042].w";
	setAttr ".wl[2042].w[0]" 0.41606022879454779;
	setAttr ".wl[2042].w[1]" 0.032826553633077689;
	setAttr ".wl[2042].w[15]" 0.30538624065228581;
	setAttr ".wl[2042].w[18]" 0.24572697692008877;
	setAttr -s 4 ".wl[2043].w";
	setAttr ".wl[2043].w[0]" 0.41609575456862335;
	setAttr ".wl[2043].w[1]" 0.032921335072596487;
	setAttr ".wl[2043].w[15]" 0.254121390367539;
	setAttr ".wl[2043].w[18]" 0.29686151999124122;
	setAttr -s 4 ".wl[2044].w";
	setAttr ".wl[2044].w[0]" 0.48217662142124651;
	setAttr ".wl[2044].w[1]" 0.017605455035239984;
	setAttr ".wl[2044].w[15]" 0.2893706212303801;
	setAttr ".wl[2044].w[18]" 0.21084730231313353;
	setAttr -s 4 ".wl[2045].w";
	setAttr ".wl[2045].w[0]" 0.48304372132802698;
	setAttr ".wl[2045].w[1]" 0.017696605980756546;
	setAttr ".wl[2045].w[15]" 0.22134950809175333;
	setAttr ".wl[2045].w[18]" 0.27791016459946322;
	setAttr -s 4 ".wl[2046].w";
	setAttr ".wl[2046].w[0]" 0.42391123371303685;
	setAttr ".wl[2046].w[15]" 0.32119630306843877;
	setAttr ".wl[2046].w[16]" 0.014789524144819786;
	setAttr ".wl[2046].w[18]" 0.24010293907370447;
	setAttr -s 4 ".wl[2047].w";
	setAttr ".wl[2047].w[0]" 0.42586970332684965;
	setAttr ".wl[2047].w[15]" 0.24376232496916284;
	setAttr ".wl[2047].w[18]" 0.31640951693785818;
	setAttr ".wl[2047].w[19]" 0.013958454766129282;
	setAttr -s 4 ".wl[2048].w";
	setAttr ".wl[2048].w[0]" 0.39862413318143963;
	setAttr ".wl[2048].w[15]" 0.34323902031617876;
	setAttr ".wl[2048].w[16]" 0.025450948248530884;
	setAttr ".wl[2048].w[18]" 0.23268589825385075;
	setAttr -s 4 ".wl[2049].w";
	setAttr ".wl[2049].w[0]" 0.41261756976681158;
	setAttr ".wl[2049].w[1]" 0.029229167426500936;
	setAttr ".wl[2049].w[15]" 0.3337795300060204;
	setAttr ".wl[2049].w[18]" 0.22437373280066719;
	setAttr -s 4 ".wl[2050].w";
	setAttr ".wl[2050].w[0]" 0.39968849146397545;
	setAttr ".wl[2050].w[1]" 0.024608937589714705;
	setAttr ".wl[2050].w[15]" 0.2369496447884043;
	setAttr ".wl[2050].w[18]" 0.33875292615790553;
	setAttr -s 4 ".wl[2051].w";
	setAttr ".wl[2051].w[0]" 0.41313488148578897;
	setAttr ".wl[2051].w[1]" 0.029334207767796484;
	setAttr ".wl[2051].w[15]" 0.23110020702527853;
	setAttr ".wl[2051].w[18]" 0.32643070372113608;
	setAttr -s 4 ".wl[2052].w";
	setAttr ".wl[2052].w[0]" 0.43206996243247264;
	setAttr ".wl[2052].w[1]" 0.028186757031061827;
	setAttr ".wl[2052].w[15]" 0.32769537949575245;
	setAttr ".wl[2052].w[18]" 0.21204790104071311;
	setAttr -s 4 ".wl[2053].w";
	setAttr ".wl[2053].w[0]" 0.464101256440968;
	setAttr ".wl[2053].w[1]" 0.020483551171012773;
	setAttr ".wl[2053].w[15]" 0.32398279257972734;
	setAttr ".wl[2053].w[18]" 0.19143239980829196;
	setAttr -s 4 ".wl[2054].w";
	setAttr ".wl[2054].w[0]" 0.43289039920200195;
	setAttr ".wl[2054].w[1]" 0.028342496434063603;
	setAttr ".wl[2054].w[15]" 0.22030755785534933;
	setAttr ".wl[2054].w[18]" 0.31845954650858505;
	setAttr -s 4 ".wl[2055].w";
	setAttr ".wl[2055].w[0]" 0.46577995085190727;
	setAttr ".wl[2055].w[1]" 0.020637005045378479;
	setAttr ".wl[2055].w[15]" 0.20046561144290315;
	setAttr ".wl[2055].w[18]" 0.31311743265981123;
	setAttr -s 4 ".wl[2056].w";
	setAttr ".wl[2056].w[0]" 0.47128230867583826;
	setAttr ".wl[2056].w[1]" 0.015296244790238065;
	setAttr ".wl[2056].w[15]" 0.32926223714950636;
	setAttr ".wl[2056].w[18]" 0.18415920938441738;
	setAttr -s 4 ".wl[2057].w";
	setAttr ".wl[2057].w[0]" 0.44548555218584351;
	setAttr ".wl[2057].w[15]" 0.34573898689065913;
	setAttr ".wl[2057].w[16]" 0.013231909448075077;
	setAttr ".wl[2057].w[18]" 0.19554355147542238;
	setAttr -s 4 ".wl[2058].w";
	setAttr ".wl[2058].w[0]" 0.47340222368671209;
	setAttr ".wl[2058].w[1]" 0.015397628176326584;
	setAttr ".wl[2058].w[15]" 0.19221509219758212;
	setAttr ".wl[2058].w[18]" 0.3189850559393792;
	setAttr -s 4 ".wl[2059].w";
	setAttr ".wl[2059].w[0]" 0.44772001106815501;
	setAttr ".wl[2059].w[1]" 0.01282754991081533;
	setAttr ".wl[2059].w[15]" 0.20044417210006529;
	setAttr ".wl[2059].w[18]" 0.33900826692096442;
	setAttr -s 4 ".wl[2060].w";
	setAttr ".wl[2060].w[0]" 0.41984797666556056;
	setAttr ".wl[2060].w[15]" 0.35067581427822081;
	setAttr ".wl[2060].w[16]" 0.017976936146316024;
	setAttr ".wl[2060].w[18]" 0.21149927290990253;
	setAttr -s 4 ".wl[2061].w";
	setAttr ".wl[2061].w[0]" 0.39897138151965977;
	setAttr ".wl[2061].w[15]" 0.34748936907625461;
	setAttr ".wl[2061].w[16]" 0.024641162583665003;
	setAttr ".wl[2061].w[18]" 0.2288980868204207;
	setAttr -s 4 ".wl[2062].w";
	setAttr ".wl[2062].w[0]" 0.42200315807830319;
	setAttr ".wl[2062].w[15]" 0.21460329204221432;
	setAttr ".wl[2062].w[18]" 0.34642655378157472;
	setAttr ".wl[2062].w[19]" 0.016966996097907763;
	setAttr -s 4 ".wl[2063].w";
	setAttr ".wl[2063].w[0]" 0.40062503554847395;
	setAttr ".wl[2063].w[15]" 0.2318953415994375;
	setAttr ".wl[2063].w[18]" 0.34413927922464943;
	setAttr ".wl[2063].w[19]" 0.023340343627439201;
	setAttr -s 4 ".wl[2064].w";
	setAttr ".wl[2064].w[0]" 0.37778568317533134;
	setAttr ".wl[2064].w[1]" 0.027725783636570578;
	setAttr ".wl[2064].w[15]" 0.29936945312175994;
	setAttr ".wl[2064].w[18]" 0.29511908006633808;
	setAttr -s 4 ".wl[2065].w";
	setAttr ".wl[2065].w[0]" 0.39442231531657485;
	setAttr ".wl[2065].w[1]" 0.029882500058058457;
	setAttr ".wl[2065].w[15]" 0.26118761735498303;
	setAttr ".wl[2065].w[18]" 0.31450756727038359;
	setAttr -s 4 ".wl[2066].w";
	setAttr ".wl[2066].w[0]" 0.39655208365614447;
	setAttr ".wl[2066].w[1]" 0.03359863519222879;
	setAttr ".wl[2066].w[15]" 0.28855007020729484;
	setAttr ".wl[2066].w[18]" 0.28129921094433197;
	setAttr -s 4 ".wl[2067].w";
	setAttr ".wl[2067].w[0]" 0.39421338959100355;
	setAttr ".wl[2067].w[1]" 0.029845018530198171;
	setAttr ".wl[2067].w[15]" 0.32029478190948435;
	setAttr ".wl[2067].w[18]" 0.25564680996931388;
	setAttr -s 4 ".wl[2068].w";
	setAttr ".wl[2068].w[0]" 0.42969900645203851;
	setAttr ".wl[2068].w[1]" 0.032255052400363853;
	setAttr ".wl[2068].w[15]" 0.27399327910482768;
	setAttr ".wl[2068].w[18]" 0.26405266204276984;
	setAttr -s 4 ".wl[2069].w";
	setAttr ".wl[2069].w[0]" 0.45279187169471702;
	setAttr ".wl[2069].w[1]" 0.026981477723442743;
	setAttr ".wl[2069].w[15]" 0.23217140887117629;
	setAttr ".wl[2069].w[18]" 0.28805524171066399;
	setAttr -s 4 ".wl[2070].w";
	setAttr ".wl[2070].w[0]" 0.47038038552461481;
	setAttr ".wl[2070].w[1]" 0.022683712639721847;
	setAttr ".wl[2070].w[15]" 0.2592502993959091;
	setAttr ".wl[2070].w[18]" 0.24768560243975421;
	setAttr -s 4 ".wl[2071].w";
	setAttr ".wl[2071].w[0]" 0.45224241005695148;
	setAttr ".wl[2071].w[1]" 0.026851954656663388;
	setAttr ".wl[2071].w[15]" 0.29889043843336832;
	setAttr ".wl[2071].w[18]" 0.22201519685301688;
	setAttr -s 4 ".wl[2072].w";
	setAttr ".wl[2072].w[0]" 0.47905110336555706;
	setAttr ".wl[2072].w[1]" 0.014316736821336015;
	setAttr ".wl[2072].w[15]" 0.25862576932500186;
	setAttr ".wl[2072].w[18]" 0.24800639048810513;
	setAttr -s 4 ".wl[2073].w";
	setAttr ".wl[2073].w[0]" 0.46955487604679752;
	setAttr ".wl[2073].w[1]" 0.012443740468341038;
	setAttr ".wl[2073].w[15]" 0.21846662058027758;
	setAttr ".wl[2073].w[18]" 0.29953476290458392;
	setAttr -s 4 ".wl[2074].w";
	setAttr ".wl[2074].w[0]" 0.43667798627561361;
	setAttr ".wl[2074].w[1]" 0.011814327463622447;
	setAttr ".wl[2074].w[15]" 0.27895130317407191;
	setAttr ".wl[2074].w[18]" 0.27255638308669206;
	setAttr -s 4 ".wl[2075].w";
	setAttr ".wl[2075].w[0]" 0.46806136454717123;
	setAttr ".wl[2075].w[1]" 0.012408607961180942;
	setAttr ".wl[2075].w[15]" 0.30862232595520533;
	setAttr ".wl[2075].w[18]" 0.21090770153644253;
	setAttr -s 4 ".wl[2076].w";
	setAttr ".wl[2076].w[0]" 0.39366876225491748;
	setAttr ".wl[2076].w[15]" 0.29538202381521483;
	setAttr ".wl[2076].w[16]" 0.018208830429601686;
	setAttr ".wl[2076].w[18]" 0.29274038350026615;
	setAttr -s 4 ".wl[2077].w";
	setAttr ".wl[2077].w[0]" 0.39622199430245347;
	setAttr ".wl[2077].w[15]" 0.2545885977929776;
	setAttr ".wl[2077].w[18]" 0.32772557770727107;
	setAttr ".wl[2077].w[19]" 0.021463830197297886;
	setAttr -s 4 ".wl[2078].w";
	setAttr ".wl[2078].w[0]" 0.37447894466467518;
	setAttr ".wl[2078].w[15]" 0.30103884315943502;
	setAttr ".wl[2078].w[16]" 0.02568900063900037;
	setAttr ".wl[2078].w[18]" 0.29879321153688937;
	setAttr -s 4 ".wl[2079].w";
	setAttr ".wl[2079].w[0]" 0.39426570449205367;
	setAttr ".wl[2079].w[15]" 0.33047868174937595;
	setAttr ".wl[2079].w[16]" 0.022707424852622754;
	setAttr ".wl[2079].w[18]" 0.25254818890594777;
	setAttr -s 4 ".wl[2080].w";
	setAttr ".wl[2080].w[0]" 0.42318633155133961;
	setAttr ".wl[2080].w[15]" 0.19875302781655682;
	setAttr ".wl[2080].w[18]" 0.35955615204757774;
	setAttr ".wl[2080].w[19]" 0.018504488584525875;
	setAttr -s 4 ".wl[2081].w";
	setAttr ".wl[2081].w[0]" 0.44950761931594718;
	setAttr ".wl[2081].w[1]" 0.015843986325142299;
	setAttr ".wl[2081].w[15]" 0.18252466819240978;
	setAttr ".wl[2081].w[18]" 0.35212372616650078;
	setAttr -s 4 ".wl[2082].w";
	setAttr ".wl[2082].w[0]" 0.44272055422055401;
	setAttr ".wl[2082].w[1]" 0.021824224647007721;
	setAttr ".wl[2082].w[15]" 0.19288578902681927;
	setAttr ".wl[2082].w[18]" 0.34256943210561897;
	setAttr -s 4 ".wl[2083].w";
	setAttr ".wl[2083].w[0]" 0.41889043233501305;
	setAttr ".wl[2083].w[1]" 0.023349181400412561;
	setAttr ".wl[2083].w[15]" 0.20732650335360658;
	setAttr ".wl[2083].w[18]" 0.35043388291096778;
	setAttr -s 4 ".wl[2084].w";
	setAttr ".wl[2084].w[0]" 0.42129573041206025;
	setAttr ".wl[2084].w[15]" 0.36447014498274694;
	setAttr ".wl[2084].w[16]" 0.019534969397659457;
	setAttr ".wl[2084].w[18]" 0.19469915520753342;
	setAttr -s 4 ".wl[2085].w";
	setAttr ".wl[2085].w[0]" 0.41790447666204772;
	setAttr ".wl[2085].w[1]" 0.023274546557624263;
	setAttr ".wl[2085].w[15]" 0.35681053644892458;
	setAttr ".wl[2085].w[18]" 0.20201044033140336;
	setAttr -s 4 ".wl[2086].w";
	setAttr ".wl[2086].w[0]" 0.44118242719102335;
	setAttr ".wl[2086].w[1]" 0.021679814138425825;
	setAttr ".wl[2086].w[15]" 0.35135009129557954;
	setAttr ".wl[2086].w[18]" 0.18578766737497135;
	setAttr -s 4 ".wl[2087].w";
	setAttr ".wl[2087].w[0]" 0.44746971054456092;
	setAttr ".wl[2087].w[1]" 0.015771153964748093;
	setAttr ".wl[2087].w[15]" 0.36000188025267527;
	setAttr ".wl[2087].w[18]" 0.17675725523801566;
	setAttr -s 4 ".wl[2088].w";
	setAttr ".wl[2088].w[0]" 0.38813080096131003;
	setAttr ".wl[2088].w[1]" 0.026798672401690964;
	setAttr ".wl[2088].w[15]" 0.266323236505635;
	setAttr ".wl[2088].w[18]" 0.31874729013136405;
	setAttr -s 4 ".wl[2089].w";
	setAttr ".wl[2089].w[0]" 0.40338576443544832;
	setAttr ".wl[2089].w[1]" 0.032217695467162341;
	setAttr ".wl[2089].w[15]" 0.25852593842954158;
	setAttr ".wl[2089].w[18]" 0.3058706016678478;
	setAttr -s 4 ".wl[2090].w";
	setAttr ".wl[2090].w[0]" 0.40332586034771784;
	setAttr ".wl[2090].w[1]" 0.032147422331137904;
	setAttr ".wl[2090].w[15]" 0.31303277073089009;
	setAttr ".wl[2090].w[18]" 0.25149394659025415;
	setAttr -s 4 ".wl[2091].w";
	setAttr ".wl[2091].w[0]" 0.38767228941223419;
	setAttr ".wl[2091].w[15]" 0.32326348949045819;
	setAttr ".wl[2091].w[16]" 0.026853131240141957;
	setAttr ".wl[2091].w[18]" 0.26221108985716551;
	setAttr -s 4 ".wl[2092].w";
	setAttr ".wl[2092].w[0]" 0.43325317577159583;
	setAttr ".wl[2092].w[1]" 0.030938755188741333;
	setAttr ".wl[2092].w[15]" 0.24378446120339478;
	setAttr ".wl[2092].w[18]" 0.29202360783626813;
	setAttr -s 4 ".wl[2093].w";
	setAttr ".wl[2093].w[0]" 0.47109031012358998;
	setAttr ".wl[2093].w[1]" 0.022183639433125985;
	setAttr ".wl[2093].w[15]" 0.22538367972938078;
	setAttr ".wl[2093].w[18]" 0.28134237071390328;
	setAttr -s 4 ".wl[2094].w";
	setAttr ".wl[2094].w[0]" 0.47035127710339569;
	setAttr ".wl[2094].w[1]" 0.022068009566252492;
	setAttr ".wl[2094].w[15]" 0.29280403406635824;
	setAttr ".wl[2094].w[18]" 0.2147766792639936;
	setAttr -s 4 ".wl[2095].w";
	setAttr ".wl[2095].w[0]" 0.43302056903822161;
	setAttr ".wl[2095].w[1]" 0.030821153938639365;
	setAttr ".wl[2095].w[15]" 0.301803410919954;
	setAttr ".wl[2095].w[18]" 0.23435486610318509;
	setAttr -s 4 ".wl[2096].w";
	setAttr ".wl[2096].w[0]" 0.48228728156408973;
	setAttr ".wl[2096].w[1]" 0.014363238703148401;
	setAttr ".wl[2096].w[15]" 0.21731417395053673;
	setAttr ".wl[2096].w[18]" 0.28603530578222514;
	setAttr -s 4 ".wl[2097].w";
	setAttr ".wl[2097].w[0]" 0.44845861259768866;
	setAttr ".wl[2097].w[1]" 0.011892067123882966;
	setAttr ".wl[2097].w[15]" 0.2303761603188553;
	setAttr ".wl[2097].w[18]" 0.30927315995957305;
	setAttr -s 4 ".wl[2098].w";
	setAttr ".wl[2098].w[0]" 0.44690045235982417;
	setAttr ".wl[2098].w[1]" 0.011890227635850455;
	setAttr ".wl[2098].w[15]" 0.31629690494357754;
	setAttr ".wl[2098].w[18]" 0.22491241506074786;
	setAttr -s 4 ".wl[2099].w";
	setAttr ".wl[2099].w[0]" 0.48109252320940876;
	setAttr ".wl[2099].w[1]" 0.014296858159593161;
	setAttr ".wl[2099].w[15]" 0.29668988574710475;
	setAttr ".wl[2099].w[18]" 0.20792073288389332;
	setAttr -s 4 ".wl[2100].w";
	setAttr ".wl[2100].w[0]" 0.40847364749972997;
	setAttr ".wl[2100].w[15]" 0.24983922086574706;
	setAttr ".wl[2100].w[18]" 0.32407241978283746;
	setAttr ".wl[2100].w[19]" 0.017614711851685439;
	setAttr -s 4 ".wl[2101].w";
	setAttr ".wl[2101].w[0]" 0.3877141447200908;
	setAttr ".wl[2101].w[15]" 0.2629721844788408;
	setAttr ".wl[2101].w[18]" 0.32480445403560126;
	setAttr ".wl[2101].w[19]" 0.024509216765467242;
	setAttr -s 4 ".wl[2102].w";
	setAttr ".wl[2102].w[0]" 0.38608302704737874;
	setAttr ".wl[2102].w[15]" 0.32741409829545159;
	setAttr ".wl[2102].w[16]" 0.025865047701648262;
	setAttr ".wl[2102].w[18]" 0.26063782695552151;
	setAttr -s 4 ".wl[2103].w";
	setAttr ".wl[2103].w[0]" 0.40645270537047939;
	setAttr ".wl[2103].w[15]" 0.32745540851600879;
	setAttr ".wl[2103].w[16]" 0.0186645843453918;
	setAttr ".wl[2103].w[18]" 0.24742730176812011;
	setAttr -s 4 ".wl[2104].w";
	setAttr ".wl[2104].w[0]" 0.43658929465576324;
	setAttr ".wl[2104].w[15]" 0.19250508491100313;
	setAttr ".wl[2104].w[18]" 0.35578431595869819;
	setAttr ".wl[2104].w[19]" 0.015121304474535418;
	setAttr -s 4 ".wl[2105].w";
	setAttr ".wl[2105].w[0]" 0.45878355102862378;
	setAttr ".wl[2105].w[1]" 0.018058650273667775;
	setAttr ".wl[2105].w[15]" 0.18598609072936545;
	setAttr ".wl[2105].w[18]" 0.337171707968343;
	setAttr -s 4 ".wl[2106].w";
	setAttr ".wl[2106].w[0]" 0.42673158514091575;
	setAttr ".wl[2106].w[1]" 0.025731218247990288;
	setAttr ".wl[2106].w[15]" 0.208873909814459;
	setAttr ".wl[2106].w[18]" 0.33866328679663504;
	setAttr -s 4 ".wl[2107].w";
	setAttr ".wl[2107].w[0]" 0.41114151185282255;
	setAttr ".wl[2107].w[15]" 0.21436227363283242;
	setAttr ".wl[2107].w[18]" 0.35279327647178832;
	setAttr ".wl[2107].w[19]" 0.021702938042556522;
	setAttr -s 4 ".wl[2108].w";
	setAttr ".wl[2108].w[0]" 0.40960402058780326;
	setAttr ".wl[2108].w[15]" 0.35732163574417264;
	setAttr ".wl[2108].w[16]" 0.022866797924197543;
	setAttr ".wl[2108].w[18]" 0.21020754574382661;
	setAttr -s 4 ".wl[2109].w";
	setAttr ".wl[2109].w[0]" 0.42574734265174463;
	setAttr ".wl[2109].w[1]" 0.025598741003944714;
	setAttr ".wl[2109].w[15]" 0.3465992394196819;
	setAttr ".wl[2109].w[18]" 0.20205467692462875;
	setAttr -s 4 ".wl[2110].w";
	setAttr ".wl[2110].w[0]" 0.45674645058748164;
	setAttr ".wl[2110].w[1]" 0.0179289070428784;
	setAttr ".wl[2110].w[15]" 0.34674932828337751;
	setAttr ".wl[2110].w[18]" 0.17857531408626254;
	setAttr -s 4 ".wl[2111].w";
	setAttr ".wl[2111].w[0]" 0.4343443007618929;
	setAttr ".wl[2111].w[15]" 0.36153756444023416;
	setAttr ".wl[2111].w[16]" 0.015980092506879394;
	setAttr ".wl[2111].w[18]" 0.18813804229099362;
	setAttr -s 4 ".wl[2112].w";
	setAttr ".wl[2112].w[0]" 0.2986450850359596;
	setAttr ".wl[2112].w[15]" 0.23665099701453049;
	setAttr ".wl[2112].w[18]" 0.36206057410632597;
	setAttr ".wl[2112].w[19]" 0.10264334384318392;
	setAttr -s 4 ".wl[2113].w";
	setAttr ".wl[2113].w[0]" 0.30922762606743548;
	setAttr ".wl[2113].w[15]" 0.19710404207318394;
	setAttr ".wl[2113].w[18]" 0.3864783116231702;
	setAttr ".wl[2113].w[19]" 0.10719002023621033;
	setAttr -s 4 ".wl[2114].w";
	setAttr ".wl[2114].w[0]" 0.39088019574148769;
	setAttr ".wl[2114].w[1]" 0.022520518272733953;
	setAttr ".wl[2114].w[15]" 0.30337488621263492;
	setAttr ".wl[2114].w[18]" 0.28322439977314351;
	setAttr -s 4 ".wl[2115].w";
	setAttr ".wl[2115].w[0]" 0.409722433804391;
	setAttr ".wl[2115].w[1]" 0.02177640778779041;
	setAttr ".wl[2115].w[15]" 0.24665475144363597;
	setAttr ".wl[2115].w[18]" 0.32184640696418265;
	setAttr -s 4 ".wl[2116].w";
	setAttr ".wl[2116].w[0]" 0.39321860950756238;
	setAttr ".wl[2116].w[15]" 0.30400092757618813;
	setAttr ".wl[2116].w[16]" 0.019956357196769855;
	setAttr ".wl[2116].w[18]" 0.28282410571947975;
	setAttr -s 4 ".wl[2117].w";
	setAttr ".wl[2117].w[0]" 0.41706485337510052;
	setAttr ".wl[2117].w[15]" 0.23908990811517095;
	setAttr ".wl[2117].w[18]" 0.32689255565153313;
	setAttr ".wl[2117].w[19]" 0.016952682858195411;
	setAttr -s 4 ".wl[2118].w";
	setAttr ".wl[2118].w[0]" 0.28748123248309104;
	setAttr ".wl[2118].w[15]" 0.23248841326691558;
	setAttr ".wl[2118].w[18]" 0.37591947438071605;
	setAttr ".wl[2118].w[19]" 0.10411087986927735;
	setAttr -s 4 ".wl[2119].w";
	setAttr ".wl[2119].w[0]" 0.29714651993025459;
	setAttr ".wl[2119].w[15]" 0.18916726793082819;
	setAttr ".wl[2119].w[18]" 0.40478273535520887;
	setAttr ".wl[2119].w[19]" 0.10890347678370822;
	setAttr -s 4 ".wl[2120].w";
	setAttr ".wl[2120].w[0]" 0.37736533544478229;
	setAttr ".wl[2120].w[15]" 0.30311874531662353;
	setAttr ".wl[2120].w[16]" 0.026158967164699936;
	setAttr ".wl[2120].w[18]" 0.29335695207389417;
	setAttr -s 4 ".wl[2121].w";
	setAttr ".wl[2121].w[0]" 0.37816885125372002;
	setAttr ".wl[2121].w[15]" 0.30404433898733829;
	setAttr ".wl[2121].w[16]" 0.023187144973725333;
	setAttr ".wl[2121].w[18]" 0.29459966478521632;
	setAttr -s 4 ".wl[2122].w";
	setAttr ".wl[2122].w[0]" 0.4126742530219355;
	setAttr ".wl[2122].w[15]" 0.21616770536854843;
	setAttr ".wl[2122].w[18]" 0.35163891602589797;
	setAttr ".wl[2122].w[19]" 0.019519125583618069;
	setAttr -s 4 ".wl[2123].w";
	setAttr ".wl[2123].w[0]" 0.40434919135931541;
	setAttr ".wl[2123].w[15]" 0.23002928324584077;
	setAttr ".wl[2123].w[18]" 0.34276625094375562;
	setAttr ".wl[2123].w[19]" 0.022855274451088117;
	setAttr -s 4 ".wl[2124].w";
	setAttr ".wl[2124].w[0]" 0.30639569829569274;
	setAttr ".wl[2124].w[15]" 0.2455857194971291;
	setAttr ".wl[2124].w[18]" 0.35588372231020898;
	setAttr ".wl[2124].w[19]" 0.09213485989696911;
	setAttr -s 4 ".wl[2125].w";
	setAttr ".wl[2125].w[0]" 0.29255898864752344;
	setAttr ".wl[2125].w[15]" 0.2408195574012999;
	setAttr ".wl[2125].w[18]" 0.37353519338508606;
	setAttr ".wl[2125].w[19]" 0.093086260566090387;
	setAttr -s 4 ".wl[2126].w";
	setAttr ".wl[2126].w[0]" 0.30718919491856189;
	setAttr ".wl[2126].w[15]" 0.17975253462589461;
	setAttr ".wl[2126].w[18]" 0.41454851019073086;
	setAttr ".wl[2126].w[19]" 0.098509760264812729;
	setAttr -s 4 ".wl[2127].w";
	setAttr ".wl[2127].w[0]" 0.32240058645272396;
	setAttr ".wl[2127].w[15]" 0.19169939836341451;
	setAttr ".wl[2127].w[18]" 0.38855945237838219;
	setAttr ".wl[2127].w[19]" 0.097340562805479341;
	setAttr -s 4 ".wl[2128].w";
	setAttr ".wl[2128].w[0]" 0.35058561383696646;
	setAttr ".wl[2128].w[15]" 0.25374730259178691;
	setAttr ".wl[2128].w[18]" 0.34640802147273758;
	setAttr ".wl[2128].w[19]" 0.049259062098508943;
	setAttr -s 4 ".wl[2129].w";
	setAttr ".wl[2129].w[0]" 0.34435593819994703;
	setAttr ".wl[2129].w[15]" 0.24784212319299567;
	setAttr ".wl[2129].w[18]" 0.36086617491334905;
	setAttr ".wl[2129].w[19]" 0.046935763693708304;
	setAttr -s 4 ".wl[2130].w";
	setAttr ".wl[2130].w[0]" 0.36719869048993437;
	setAttr ".wl[2130].w[15]" 0.17664219436500475;
	setAttr ".wl[2130].w[18]" 0.40857755520723393;
	setAttr ".wl[2130].w[19]" 0.047581559937826923;
	setAttr -s 4 ".wl[2131].w";
	setAttr ".wl[2131].w[0]" 0.37108792193531615;
	setAttr ".wl[2131].w[15]" 0.19257843443094144;
	setAttr ".wl[2131].w[18]" 0.38578483072892739;
	setAttr ".wl[2131].w[19]" 0.050548812904815145;
	setAttr -s 4 ".wl[2132].w";
	setAttr ".wl[2132].w[0]" 0.30577613048500552;
	setAttr ".wl[2132].w[15]" 0.21708842250313373;
	setAttr ".wl[2132].w[18]" 0.37208373749740814;
	setAttr ".wl[2132].w[19]" 0.10505170951445249;
	setAttr -s 4 ".wl[2133].w";
	setAttr ".wl[2133].w[0]" 0.39776053842515585;
	setAttr ".wl[2133].w[1]" 0.023412798586011474;
	setAttr ".wl[2133].w[15]" 0.27650491730653215;
	setAttr ".wl[2133].w[18]" 0.30232174568230058;
	setAttr -s 4 ".wl[2134].w";
	setAttr ".wl[2134].w[0]" 0.40446875231675616;
	setAttr ".wl[2134].w[15]" 0.27228975410232048;
	setAttr ".wl[2134].w[18]" 0.30595229887087588;
	setAttr ".wl[2134].w[19]" 0.017289194710047476;
	setAttr -s 4 ".wl[2135].w";
	setAttr ".wl[2135].w[0]" 0.29014436948533823;
	setAttr ".wl[2135].w[15]" 0.20867255357792153;
	setAttr ".wl[2135].w[18]" 0.3939081220551231;
	setAttr ".wl[2135].w[19]" 0.10727495488161723;
	setAttr -s 4 ".wl[2136].w";
	setAttr ".wl[2136].w[0]" 0.29881321080561929;
	setAttr ".wl[2136].w[15]" 0.24490565672238571;
	setAttr ".wl[2136].w[18]" 0.35554126299249528;
	setAttr ".wl[2136].w[19]" 0.10073986947949969;
	setAttr -s 4 ".wl[2137].w";
	setAttr ".wl[2137].w[0]" 0.31339245966205492;
	setAttr ".wl[2137].w[15]" 0.1922500269859323;
	setAttr ".wl[2137].w[18]" 0.38761324103838846;
	setAttr ".wl[2137].w[19]" 0.10674427231362439;
	setAttr -s 4 ".wl[2138].w";
	setAttr ".wl[2138].w[0]" 0.39358636172502004;
	setAttr ".wl[2138].w[15]" 0.3119861845717678;
	setAttr ".wl[2138].w[16]" 0.021640643654342699;
	setAttr ".wl[2138].w[18]" 0.27278681004886951;
	setAttr -s 4 ".wl[2139].w";
	setAttr ".wl[2139].w[0]" 0.41805381916618756;
	setAttr ".wl[2139].w[1]" 0.018472572449006647;
	setAttr ".wl[2139].w[15]" 0.23283597145511262;
	setAttr ".wl[2139].w[18]" 0.33063763692969317;
	setAttr -s 4 ".wl[2140].w";
	setAttr ".wl[2140].w[0]" 0.39254970821808138;
	setAttr ".wl[2140].w[15]" 0.31184017614261911;
	setAttr ".wl[2140].w[16]" 0.020667160330886749;
	setAttr ".wl[2140].w[18]" 0.27494295530841278;
	setAttr -s 4 ".wl[2141].w";
	setAttr ".wl[2141].w[0]" 0.42134620189539201;
	setAttr ".wl[2141].w[15]" 0.22511957606169009;
	setAttr ".wl[2141].w[18]" 0.33691943643311478;
	setAttr ".wl[2141].w[19]" 0.016614785609803163;
	setAttr -s 4 ".wl[2142].w";
	setAttr ".wl[2142].w[0]" 0.29096723510687245;
	setAttr ".wl[2142].w[15]" 0.24257189821325548;
	setAttr ".wl[2142].w[18]" 0.36368321209784221;
	setAttr ".wl[2142].w[19]" 0.10277765458202981;
	setAttr -s 4 ".wl[2143].w";
	setAttr ".wl[2143].w[0]" 0.30468320210648286;
	setAttr ".wl[2143].w[15]" 0.18674772202120937;
	setAttr ".wl[2143].w[18]" 0.3994415470806475;
	setAttr ".wl[2143].w[19]" 0.10912752879166028;
	setAttr -s 4 ".wl[2144].w";
	setAttr ".wl[2144].w[0]" 0.38924424890741938;
	setAttr ".wl[2144].w[15]" 0.30999869376276656;
	setAttr ".wl[2144].w[16]" 0.023772616021651958;
	setAttr ".wl[2144].w[18]" 0.27698444130816213;
	setAttr -s 4 ".wl[2145].w";
	setAttr ".wl[2145].w[0]" 0.36569125494601085;
	setAttr ".wl[2145].w[15]" 0.27426528279656576;
	setAttr ".wl[2145].w[18]" 0.32942719950300486;
	setAttr ".wl[2145].w[19]" 0.030616262754418533;
	setAttr -s 4 ".wl[2146].w";
	setAttr ".wl[2146].w[0]" 0.37965593537542175;
	setAttr ".wl[2146].w[15]" 0.31315492073549117;
	setAttr ".wl[2146].w[16]" 0.025601445478361611;
	setAttr ".wl[2146].w[18]" 0.28158769841072534;
	setAttr -s 4 ".wl[2147].w";
	setAttr ".wl[2147].w[0]" 0.39427084990565847;
	setAttr ".wl[2147].w[15]" 0.19385477171920865;
	setAttr ".wl[2147].w[18]" 0.38270585036034088;
	setAttr ".wl[2147].w[19]" 0.029168528014792014;
	setAttr -s 4 ".wl[2148].w";
	setAttr ".wl[2148].w[0]" 0.39697617137712016;
	setAttr ".wl[2148].w[15]" 0.25798569008300876;
	setAttr ".wl[2148].w[18]" 0.32477587598636543;
	setAttr ".wl[2148].w[19]" 0.020262262553505665;
	setAttr -s 4 ".wl[2149].w";
	setAttr ".wl[2149].w[0]" 0.4108388598198105;
	setAttr ".wl[2149].w[1]" 0.022790416791603482;
	setAttr ".wl[2149].w[15]" 0.23742106672436211;
	setAttr ".wl[2149].w[18]" 0.32894965666422393;
	setAttr -s 4 ".wl[2150].w";
	setAttr ".wl[2150].w[0]" 0.41294322907481185;
	setAttr ".wl[2150].w[15]" 0.21082784023479018;
	setAttr ".wl[2150].w[18]" 0.35549319401466734;
	setAttr ".wl[2150].w[19]" 0.020735736675730577;
	setAttr -s 4 ".wl[2151].w";
	setAttr ".wl[2151].w[0]" 0.39007284789730684;
	setAttr ".wl[2151].w[1]" 0.024769209812200144;
	setAttr ".wl[2151].w[15]" 0.26737579128348798;
	setAttr ".wl[2151].w[18]" 0.31778215100700502;
	setAttr -s 4 ".wl[2152].w";
	setAttr ".wl[2152].w[0]" 0.32674837407296142;
	setAttr ".wl[2152].w[15]" 0.24652116845881639;
	setAttr ".wl[2152].w[18]" 0.35538562224773862;
	setAttr ".wl[2152].w[19]" 0.071344835220483613;
	setAttr -s 4 ".wl[2153].w";
	setAttr ".wl[2153].w[0]" 0.28394867996832085;
	setAttr ".wl[2153].w[15]" 0.24036266749361326;
	setAttr ".wl[2153].w[18]" 0.37314443169506778;
	setAttr ".wl[2153].w[19]" 0.10254422084299809;
	setAttr -s 4 ".wl[2154].w";
	setAttr ".wl[2154].w[0]" 0.29694158877855481;
	setAttr ".wl[2154].w[15]" 0.25411508810638356;
	setAttr ".wl[2154].w[18]" 0.35741530537394811;
	setAttr ".wl[2154].w[19]" 0.09152801774111359;
	setAttr -s 4 ".wl[2155].w";
	setAttr ".wl[2155].w[0]" 0.29687179576307088;
	setAttr ".wl[2155].w[15]" 0.18079840081540952;
	setAttr ".wl[2155].w[18]" 0.41335079073079067;
	setAttr ".wl[2155].w[19]" 0.10897901269072896;
	setAttr -s 4 ".wl[2156].w";
	setAttr ".wl[2156].w[0]" 0.29796195541008641;
	setAttr ".wl[2156].w[15]" 0.20667899240927443;
	setAttr ".wl[2156].w[18]" 0.399178374249964;
	setAttr ".wl[2156].w[19]" 0.096180677930675154;
	setAttr -s 4 ".wl[2157].w";
	setAttr ".wl[2157].w[0]" 0.34548952128083243;
	setAttr ".wl[2157].w[15]" 0.18958894565513137;
	setAttr ".wl[2157].w[18]" 0.39011195122087572;
	setAttr ".wl[2157].w[19]" 0.074809581843160472;
	setAttr -s 4 ".wl[2158].w";
	setAttr ".wl[2158].w[0]" 0.31755523653411394;
	setAttr ".wl[2158].w[15]" 0.17766844701914683;
	setAttr ".wl[2158].w[18]" 0.40611449617027434;
	setAttr ".wl[2158].w[19]" 0.098661820276464876;
	setAttr -s 4 ".wl[2159].w";
	setAttr ".wl[2159].w[0]" 0.31696762379011223;
	setAttr ".wl[2159].w[15]" 0.21853159673776895;
	setAttr ".wl[2159].w[18]" 0.36981500041732973;
	setAttr ".wl[2159].w[19]" 0.09468577905478906;
	setAttr -s 4 ".wl[2160].w";
	setAttr ".wl[2160].w[0]" 0.36666998417225893;
	setAttr ".wl[2160].w[15]" 0.27750838619096008;
	setAttr ".wl[2160].w[18]" 0.32239136683150071;
	setAttr ".wl[2160].w[19]" 0.033430262805280282;
	setAttr -s 4 ".wl[2161].w";
	setAttr ".wl[2161].w[0]" 0.31568673629764465;
	setAttr ".wl[2161].w[15]" 0.24099535041497464;
	setAttr ".wl[2161].w[18]" 0.37284360159173346;
	setAttr ".wl[2161].w[19]" 0.070474311695647274;
	setAttr -s 4 ".wl[2162].w";
	setAttr ".wl[2162].w[0]" 0.34417886898399846;
	setAttr ".wl[2162].w[15]" 0.26317834006742769;
	setAttr ".wl[2162].w[18]" 0.34487222544377927;
	setAttr ".wl[2162].w[19]" 0.04777056550479461;
	setAttr -s 4 ".wl[2163].w";
	setAttr ".wl[2163].w[0]" 0.33457272494531826;
	setAttr ".wl[2163].w[15]" 0.17590997662178146;
	setAttr ".wl[2163].w[18]" 0.41588925779970576;
	setAttr ".wl[2163].w[19]" 0.073628040633194461;
	setAttr -s 4 ".wl[2164].w";
	setAttr ".wl[2164].w[0]" 0.35490971188237941;
	setAttr ".wl[2164].w[15]" 0.20793197515701128;
	setAttr ".wl[2164].w[18]" 0.39013221512436103;
	setAttr ".wl[2164].w[19]" 0.04702609783624833;
	setAttr -s 4 ".wl[2165].w";
	setAttr ".wl[2165].w[0]" 0.39007063693146238;
	setAttr ".wl[2165].w[15]" 0.21012119243125318;
	setAttr ".wl[2165].w[18]" 0.3672730206887837;
	setAttr ".wl[2165].w[19]" 0.032535149948500694;
	setAttr -s 4 ".wl[2166].w";
	setAttr ".wl[2166].w[0]" 0.37337727100085938;
	setAttr ".wl[2166].w[15]" 0.1752497562192801;
	setAttr ".wl[2166].w[18]" 0.40226621553526593;
	setAttr ".wl[2166].w[19]" 0.049106757244594519;
	setAttr -s 4 ".wl[2167].w";
	setAttr ".wl[2167].w[0]" 0.36139163309998079;
	setAttr ".wl[2167].w[15]" 0.22347566893534096;
	setAttr ".wl[2167].w[18]" 0.36466815901598182;
	setAttr ".wl[2167].w[19]" 0.050464538948696451;
	setAttr -s 4 ".wl[2168].w";
	setAttr ".wl[2168].w[0]" 0.3089153934499636;
	setAttr ".wl[2168].w[15]" 0.21844415856066099;
	setAttr ".wl[2168].w[18]" 0.36910332753260683;
	setAttr ".wl[2168].w[19]" 0.10353712045676865;
	setAttr -s 4 ".wl[2169].w";
	setAttr ".wl[2169].w[0]" 0.40206583821542963;
	setAttr ".wl[2169].w[1]" 0.019183920769922244;
	setAttr ".wl[2169].w[15]" 0.27527748928290113;
	setAttr ".wl[2169].w[18]" 0.30347275173174698;
	setAttr -s 4 ".wl[2170].w";
	setAttr ".wl[2170].w[0]" 0.28824033099523422;
	setAttr ".wl[2170].w[15]" 0.20714234018328179;
	setAttr ".wl[2170].w[18]" 0.39829154813535883;
	setAttr ".wl[2170].w[19]" 0.10632578068612521;
	setAttr -s 4 ".wl[2171].w";
	setAttr ".wl[2171].w[0]" 0.29833420928181625;
	setAttr ".wl[2171].w[15]" 0.21297946256836889;
	setAttr ".wl[2171].w[18]" 0.38222019430929405;
	setAttr ".wl[2171].w[19]" 0.10646613384052075;
	setAttr -s 4 ".wl[2172].w";
	setAttr ".wl[2172].w[0]" 0.30763029025608662;
	setAttr ".wl[2172].w[15]" 0.17863643552102312;
	setAttr ".wl[2172].w[18]" 0.40500842292637684;
	setAttr ".wl[2172].w[19]" 0.10872485129651348;
	setAttr -s 4 ".wl[2173].w";
	setAttr ".wl[2173].w[0]" 0.28904335102363765;
	setAttr ".wl[2173].w[15]" 0.25329326652105616;
	setAttr ".wl[2173].w[18]" 0.35726080615688555;
	setAttr ".wl[2173].w[19]" 0.10040257629842056;
	setAttr -s 4 ".wl[2174].w";
	setAttr ".wl[2174].w[0]" 0.3919525186137392;
	setAttr ".wl[2174].w[15]" 0.32079075565465109;
	setAttr ".wl[2174].w[16]" 0.023093471817785181;
	setAttr ".wl[2174].w[18]" 0.2641632539138245;
	setAttr -s 4 ".wl[2175].w";
	setAttr ".wl[2175].w[0]" 0.40375060276695057;
	setAttr ".wl[2175].w[15]" 0.26966064787926308;
	setAttr ".wl[2175].w[18]" 0.30924394860117432;
	setAttr ".wl[2175].w[19]" 0.017344800752611984;
	setAttr -s 4 ".wl[2176].w";
	setAttr ".wl[2176].w[0]" 0.42136675116153754;
	setAttr ".wl[2176].w[1]" 0.01824014103587334;
	setAttr ".wl[2176].w[15]" 0.218603858565139;
	setAttr ".wl[2176].w[18]" 0.34178924923745019;
	setAttr -s 4 ".wl[2177].w";
	setAttr ".wl[2177].w[0]" 0.39519383093659732;
	setAttr ".wl[2177].w[1]" 0.024989966893050216;
	setAttr ".wl[2177].w[15]" 0.27578830195327214;
	setAttr ".wl[2177].w[18]" 0.30402790021708037;
	setAttr -s 4 ".wl[2178].w";
	setAttr ".wl[2178].w[0]" 0.31842996742250451;
	setAttr ".wl[2178].w[15]" 0.25526990400854427;
	setAttr ".wl[2178].w[18]" 0.35613875821170704;
	setAttr ".wl[2178].w[19]" 0.07016137035724411;
	setAttr -s 4 ".wl[2179].w";
	setAttr ".wl[2179].w[0]" 0.32367179338134211;
	setAttr ".wl[2179].w[15]" 0.20441279409981125;
	setAttr ".wl[2179].w[18]" 0.39984330181488453;
	setAttr ".wl[2179].w[19]" 0.072072110703962133;
	setAttr -s 4 ".wl[2180].w";
	setAttr ".wl[2180].w[0]" 0.34352146430413782;
	setAttr ".wl[2180].w[15]" 0.17422259480469968;
	setAttr ".wl[2180].w[18]" 0.4076017638351333;
	setAttr ".wl[2180].w[19]" 0.074654177056029183;
	setAttr -s 4 ".wl[2181].w";
	setAttr ".wl[2181].w[0]" 0.33754011316654564;
	setAttr ".wl[2181].w[15]" 0.2182035047762437;
	setAttr ".wl[2181].w[18]" 0.37085292742131593;
	setAttr ".wl[2181].w[19]" 0.073403454635894608;
	setAttr -s 4 ".wl[2182].w";
	setAttr ".wl[2182].w[0]" 0.36189444051365238;
	setAttr ".wl[2182].w[15]" 0.28918893471892798;
	setAttr ".wl[2182].w[18]" 0.31683287121805748;
	setAttr ".wl[2182].w[19]" 0.032083753549362146;
	setAttr -s 4 ".wl[2183].w";
	setAttr ".wl[2183].w[0]" 0.38082429549507307;
	setAttr ".wl[2183].w[15]" 0.2300627867361745;
	setAttr ".wl[2183].w[18]" 0.35956397488118069;
	setAttr ".wl[2183].w[19]" 0.029548942887571764;
	setAttr -s 4 ".wl[2184].w";
	setAttr ".wl[2184].w[0]" 0.39649903786666879;
	setAttr ".wl[2184].w[15]" 0.1914795290311537;
	setAttr ".wl[2184].w[18]" 0.38147683516892716;
	setAttr ".wl[2184].w[19]" 0.0305445979332503;
	setAttr -s 4 ".wl[2185].w";
	setAttr ".wl[2185].w[0]" 0.3781535475423487;
	setAttr ".wl[2185].w[15]" 0.24426441565236773;
	setAttr ".wl[2185].w[18]" 0.34398830804167013;
	setAttr ".wl[2185].w[19]" 0.033593728763613467;
	setAttr -s 4 ".wl[2186].w";
	setAttr ".wl[2186].w[0]" 0.273830392128045;
	setAttr ".wl[2186].w[15]" 0.42048437334315775;
	setAttr ".wl[2186].w[16]" 0.14801217655214624;
	setAttr ".wl[2186].w[18]" 0.15767305797665099;
	setAttr -s 4 ".wl[2187].w";
	setAttr ".wl[2187].w[0]" 0.27668559764152795;
	setAttr ".wl[2187].w[15]" 0.4401064117346058;
	setAttr ".wl[2187].w[16]" 0.15167797533397173;
	setAttr ".wl[2187].w[18]" 0.13153001528989461;
	setAttr -s 4 ".wl[2188].w";
	setAttr ".wl[2188].w[0]" 0.39185141987460581;
	setAttr ".wl[2188].w[15]" 0.31332632086524126;
	setAttr ".wl[2188].w[16]" 0.023146191510995701;
	setAttr ".wl[2188].w[18]" 0.2716760677491572;
	setAttr -s 4 ".wl[2189].w";
	setAttr ".wl[2189].w[0]" 0.41412839693981962;
	setAttr ".wl[2189].w[1]" 0.021329770027383226;
	setAttr ".wl[2189].w[15]" 0.34786549209011947;
	setAttr ".wl[2189].w[18]" 0.21667634094267771;
	setAttr -s 4 ".wl[2190].w";
	setAttr ".wl[2190].w[0]" 0.39472157133986646;
	setAttr ".wl[2190].w[15]" 0.31506018932232011;
	setAttr ".wl[2190].w[16]" 0.020826241831603105;
	setAttr ".wl[2190].w[18]" 0.26939199750621035;
	setAttr -s 4 ".wl[2191].w";
	setAttr ".wl[2191].w[0]" 0.4212507175130174;
	setAttr ".wl[2191].w[15]" 0.35381705043717665;
	setAttr ".wl[2191].w[16]" 0.018253615224660041;
	setAttr ".wl[2191].w[18]" 0.20667861682514588;
	setAttr -s 4 ".wl[2192].w";
	setAttr ".wl[2192].w[0]" 0.25474037067519795;
	setAttr ".wl[2192].w[15]" 0.44430071776955804;
	setAttr ".wl[2192].w[16]" 0.15347019889730529;
	setAttr ".wl[2192].w[18]" 0.14748871265793873;
	setAttr -s 4 ".wl[2193].w";
	setAttr ".wl[2193].w[0]" 0.2556066769590154;
	setAttr ".wl[2193].w[15]" 0.46713286820809369;
	setAttr ".wl[2193].w[16]" 0.15706390057364181;
	setAttr ".wl[2193].w[18]" 0.12019655425924913;
	setAttr -s 4 ".wl[2194].w";
	setAttr ".wl[2194].w[0]" 0.38190104773098577;
	setAttr ".wl[2194].w[15]" 0.32120120547119646;
	setAttr ".wl[2194].w[16]" 0.027496579365822938;
	setAttr ".wl[2194].w[18]" 0.26940116743199494;
	setAttr -s 4 ".wl[2195].w";
	setAttr ".wl[2195].w[0]" 0.3844478821227858;
	setAttr ".wl[2195].w[15]" 0.32554676693670076;
	setAttr ".wl[2195].w[16]" 0.024449009550148709;
	setAttr ".wl[2195].w[18]" 0.26555634139036477;
	setAttr -s 4 ".wl[2196].w";
	setAttr ".wl[2196].w[0]" 0.41760053977033318;
	setAttr ".wl[2196].w[15]" 0.37705434903243906;
	setAttr ".wl[2196].w[16]" 0.020735192911011308;
	setAttr ".wl[2196].w[18]" 0.1846099182862164;
	setAttr -s 4 ".wl[2197].w";
	setAttr ".wl[2197].w[0]" 0.40885433601716259;
	setAttr ".wl[2197].w[15]" 0.36633288503635514;
	setAttr ".wl[2197].w[16]" 0.024306977065937343;
	setAttr ".wl[2197].w[18]" 0.20050580188054484;
	setAttr -s 4 ".wl[2198].w";
	setAttr ".wl[2198].w[0]" 0.29076839731692239;
	setAttr ".wl[2198].w[15]" 0.41087994032872027;
	setAttr ".wl[2198].w[16]" 0.12880423066724725;
	setAttr ".wl[2198].w[18]" 0.1695474316871102;
	setAttr -s 4 ".wl[2199].w";
	setAttr ".wl[2199].w[0]" 0.26712503387659675;
	setAttr ".wl[2199].w[15]" 0.44259453089433853;
	setAttr ".wl[2199].w[16]" 0.13394929031911956;
	setAttr ".wl[2199].w[18]" 0.15633114490994518;
	setAttr -s 4 ".wl[2200].w";
	setAttr ".wl[2200].w[0]" 0.26958536052369891;
	setAttr ".wl[2200].w[15]" 0.47602544605503194;
	setAttr ".wl[2200].w[16]" 0.13746325592161432;
	setAttr ".wl[2200].w[18]" 0.11692593749965488;
	setAttr -s 4 ".wl[2201].w";
	setAttr ".wl[2201].w[0]" 0.29694010629978584;
	setAttr ".wl[2201].w[15]" 0.43781854548573623;
	setAttr ".wl[2201].w[16]" 0.1326588845659987;
	setAttr ".wl[2201].w[18]" 0.13258246364847931;
	setAttr -s 4 ".wl[2202].w";
	setAttr ".wl[2202].w[0]" 0.35651671136499352;
	setAttr ".wl[2202].w[15]" 0.37783789440815047;
	setAttr ".wl[2202].w[16]" 0.055597825216443998;
	setAttr ".wl[2202].w[18]" 0.21004756901041208;
	setAttr -s 4 ".wl[2203].w";
	setAttr ".wl[2203].w[0]" 0.34881984920507597;
	setAttr ".wl[2203].w[15]" 0.40043701868861686;
	setAttr ".wl[2203].w[16]" 0.053288438324886687;
	setAttr ".wl[2203].w[18]" 0.19745469378142053;
	setAttr -s 4 ".wl[2204].w";
	setAttr ".wl[2204].w[0]" 0.37007594900443685;
	setAttr ".wl[2204].w[15]" 0.43962724962170135;
	setAttr ".wl[2204].w[16]" 0.051274772447246271;
	setAttr ".wl[2204].w[18]" 0.13902202892661536;
	setAttr -s 4 ".wl[2205].w";
	setAttr ".wl[2205].w[0]" 0.37766317122904108;
	setAttr ".wl[2205].w[15]" 0.4100230146337332;
	setAttr ".wl[2205].w[16]" 0.054541538578256506;
	setAttr ".wl[2205].w[18]" 0.15777227555896928;
	setAttr -s 4 ".wl[2206].w";
	setAttr ".wl[2206].w[0]" 0.27824682508744514;
	setAttr ".wl[2206].w[15]" 0.42660624266788411;
	setAttr ".wl[2206].w[16]" 0.14944507225274539;
	setAttr ".wl[2206].w[18]" 0.14570185999192542;
	setAttr -s 4 ".wl[2207].w";
	setAttr ".wl[2207].w[0]" 0.40272734933301296;
	setAttr ".wl[2207].w[1]" 0.023051655978240156;
	setAttr ".wl[2207].w[15]" 0.32926526686914331;
	setAttr ".wl[2207].w[18]" 0.24495572781960359;
	setAttr -s 4 ".wl[2208].w";
	setAttr ".wl[2208].w[0]" 0.40987668445273295;
	setAttr ".wl[2208].w[15]" 0.33455250453956614;
	setAttr ".wl[2208].w[16]" 0.019023377149377421;
	setAttr ".wl[2208].w[18]" 0.23654743385832339;
	setAttr -s 4 ".wl[2209].w";
	setAttr ".wl[2209].w[0]" 0.251120509621855;
	setAttr ".wl[2209].w[15]" 0.46101197239438663;
	setAttr ".wl[2209].w[16]" 0.15684422936454745;
	setAttr ".wl[2209].w[18]" 0.13102328861921095;
	setAttr -s 4 ".wl[2210].w";
	setAttr ".wl[2210].w[0]" 0.27697924322779094;
	setAttr ".wl[2210].w[15]" 0.41318541417928589;
	setAttr ".wl[2210].w[16]" 0.14525741611507112;
	setAttr ".wl[2210].w[18]" 0.16457792647785208;
	setAttr -s 4 ".wl[2211].w";
	setAttr ".wl[2211].w[0]" 0.28134437755461389;
	setAttr ".wl[2211].w[15]" 0.43905476066412702;
	setAttr ".wl[2211].w[16]" 0.15013858387045692;
	setAttr ".wl[2211].w[18]" 0.12946227791080209;
	setAttr -s 4 ".wl[2212].w";
	setAttr ".wl[2212].w[0]" 0.3889505336664057;
	setAttr ".wl[2212].w[15]" 0.30712761866775101;
	setAttr ".wl[2212].w[16]" 0.022211563285736959;
	setAttr ".wl[2212].w[18]" 0.28171028438010642;
	setAttr -s 4 ".wl[2213].w";
	setAttr ".wl[2213].w[0]" 0.42195792013590017;
	setAttr ".wl[2213].w[15]" 0.35661205767980692;
	setAttr ".wl[2213].w[16]" 0.018923953034389391;
	setAttr ".wl[2213].w[18]" 0.20250606914990352;
	setAttr -s 4 ".wl[2214].w";
	setAttr ".wl[2214].w[0]" 0.38904947938431261;
	setAttr ".wl[2214].w[15]" 0.31045693650331302;
	setAttr ".wl[2214].w[16]" 0.02134384446680836;
	setAttr ".wl[2214].w[18]" 0.27914973964556605;
	setAttr -s 4 ".wl[2215].w";
	setAttr ".wl[2215].w[0]" 0.42537599157640826;
	setAttr ".wl[2215].w[15]" 0.36338660326144073;
	setAttr ".wl[2215].w[16]" 0.017749391262706821;
	setAttr ".wl[2215].w[18]" 0.19348801389944431;
	setAttr -s 4 ".wl[2216].w";
	setAttr ".wl[2216].w[0]" 0.26384117971492865;
	setAttr ".wl[2216].w[15]" 0.42775446487687813;
	setAttr ".wl[2216].w[16]" 0.15041581243615099;
	setAttr ".wl[2216].w[18]" 0.15798854297204218;
	setAttr -s 4 ".wl[2217].w";
	setAttr ".wl[2217].w[0]" 0.26644293493134202;
	setAttr ".wl[2217].w[15]" 0.45627296784433102;
	setAttr ".wl[2217].w[16]" 0.15539370896718482;
	setAttr ".wl[2217].w[18]" 0.12189038825714218;
	setAttr -s 4 ".wl[2218].w";
	setAttr ".wl[2218].w[0]" 0.38629887228348697;
	setAttr ".wl[2218].w[15]" 0.3088100885053644;
	setAttr ".wl[2218].w[16]" 0.024423690728055274;
	setAttr ".wl[2218].w[18]" 0.28046734848309324;
	setAttr -s 4 ".wl[2219].w";
	setAttr ".wl[2219].w[0]" 0.37249741722613749;
	setAttr ".wl[2219].w[15]" 0.36131991496542709;
	setAttr ".wl[2219].w[16]" 0.033789033736678305;
	setAttr ".wl[2219].w[18]" 0.232393634071757;
	setAttr -s 4 ".wl[2220].w";
	setAttr ".wl[2220].w[0]" 0.37790252264776281;
	setAttr ".wl[2220].w[15]" 0.31478861100059147;
	setAttr ".wl[2220].w[16]" 0.026445785418450496;
	setAttr ".wl[2220].w[18]" 0.28086308093319517;
	setAttr -s 4 ".wl[2221].w";
	setAttr ".wl[2221].w[0]" 0.39925857192927744;
	setAttr ".wl[2221].w[15]" 0.40808309550799249;
	setAttr ".wl[2221].w[16]" 0.030424893455008869;
	setAttr ".wl[2221].w[18]" 0.16223343910772126;
	setAttr -s 4 ".wl[2222].w";
	setAttr ".wl[2222].w[0]" 0.4028125330665176;
	setAttr ".wl[2222].w[15]" 0.35348682132218545;
	setAttr ".wl[2222].w[16]" 0.022133388266928386;
	setAttr ".wl[2222].w[18]" 0.2215672573443685;
	setAttr -s 4 ".wl[2223].w";
	setAttr ".wl[2223].w[0]" 0.4150526047599134;
	setAttr ".wl[2223].w[1]" 0.022302455151400623;
	setAttr ".wl[2223].w[15]" 0.35412484063170135;
	setAttr ".wl[2223].w[18]" 0.20852009945698466;
	setAttr -s 4 ".wl[2224].w";
	setAttr ".wl[2224].w[0]" 0.41755914999791743;
	setAttr ".wl[2224].w[15]" 0.37907529778045318;
	setAttr ".wl[2224].w[16]" 0.021860337783676773;
	setAttr ".wl[2224].w[18]" 0.18150521443795262;
	setAttr -s 4 ".wl[2225].w";
	setAttr ".wl[2225].w[0]" 0.39504437292238814;
	setAttr ".wl[2225].w[15]" 0.34289862712420188;
	setAttr ".wl[2225].w[16]" 0.026527011590695197;
	setAttr ".wl[2225].w[18]" 0.23552998836271469;
	setAttr -s 4 ".wl[2226].w";
	setAttr ".wl[2226].w[0]" 0.32586450682734036;
	setAttr ".wl[2226].w[15]" 0.39946484075693778;
	setAttr ".wl[2226].w[16]" 0.089769049472063059;
	setAttr ".wl[2226].w[18]" 0.18490160294365887;
	setAttr -s 4 ".wl[2227].w";
	setAttr ".wl[2227].w[0]" 0.25171332636506882;
	setAttr ".wl[2227].w[15]" 0.44460259081306747;
	setAttr ".wl[2227].w[16]" 0.15246593247422091;
	setAttr ".wl[2227].w[18]" 0.15121815034764274;
	setAttr -s 4 ".wl[2228].w";
	setAttr ".wl[2228].w[0]" 0.27864956090918608;
	setAttr ".wl[2228].w[15]" 0.42012525175668536;
	setAttr ".wl[2228].w[16]" 0.1305297503461248;
	setAttr ".wl[2228].w[18]" 0.17069543698800366;
	setAttr -s 4 ".wl[2229].w";
	setAttr ".wl[2229].w[0]" 0.25245217322301178;
	setAttr ".wl[2229].w[15]" 0.47636334777677242;
	setAttr ".wl[2229].w[16]" 0.15719852865630671;
	setAttr ".wl[2229].w[18]" 0.11398595034390918;
	setAttr -s 4 ".wl[2230].w";
	setAttr ".wl[2230].w[0]" 0.26389507274901802;
	setAttr ".wl[2230].w[15]" 0.46702943730412577;
	setAttr ".wl[2230].w[16]" 0.13671086207354627;
	setAttr ".wl[2230].w[18]" 0.13236462787330996;
	setAttr -s 4 ".wl[2231].w";
	setAttr ".wl[2231].w[0]" 0.33826685577848503;
	setAttr ".wl[2231].w[15]" 0.42857723109171697;
	setAttr ".wl[2231].w[16]" 0.091211632118135794;
	setAttr ".wl[2231].w[18]" 0.1419442810116622;
	setAttr -s 4 ".wl[2232].w";
	setAttr ".wl[2232].w[0]" 0.28473981448762853;
	setAttr ".wl[2232].w[15]" 0.46005946290565974;
	setAttr ".wl[2232].w[16]" 0.13555601345506377;
	setAttr ".wl[2232].w[18]" 0.11964470915164804;
	setAttr -s 4 ".wl[2233].w";
	setAttr ".wl[2233].w[0]" 0.29786671154555905;
	setAttr ".wl[2233].w[15]" 0.42005547714500618;
	setAttr ".wl[2233].w[16]" 0.13002926353848257;
	setAttr ".wl[2233].w[18]" 0.15204854777095234;
	setAttr -s 4 ".wl[2234].w";
	setAttr ".wl[2234].w[0]" 0.37314555597380045;
	setAttr ".wl[2234].w[15]" 0.34805148592039759;
	setAttr ".wl[2234].w[16]" 0.0367460980269168;
	setAttr ".wl[2234].w[18]" 0.24205686007888538;
	setAttr -s 4 ".wl[2235].w";
	setAttr ".wl[2235].w[0]" 0.30868938236074756;
	setAttr ".wl[2235].w[15]" 0.42909789015415617;
	setAttr ".wl[2235].w[16]" 0.090343118581200019;
	setAttr ".wl[2235].w[18]" 0.17186960890389619;
	setAttr -s 4 ".wl[2236].w";
	setAttr ".wl[2236].w[0]" 0.3497317749581324;
	setAttr ".wl[2236].w[15]" 0.38116759671277367;
	setAttr ".wl[2236].w[16]" 0.054545688113562883;
	setAttr ".wl[2236].w[18]" 0.21455494021553118;
	setAttr -s 4 ".wl[2237].w";
	setAttr ".wl[2237].w[0]" 0.31769155551487721;
	setAttr ".wl[2237].w[15]" 0.4658417802242974;
	setAttr ".wl[2237].w[16]" 0.091078913253716282;
	setAttr ".wl[2237].w[18]" 0.12538775100710908;
	setAttr -s 4 ".wl[2238].w";
	setAttr ".wl[2238].w[0]" 0.35819961164229591;
	setAttr ".wl[2238].w[15]" 0.42684090864568897;
	setAttr ".wl[2238].w[16]" 0.051932442994226315;
	setAttr ".wl[2238].w[18]" 0.16302703671778887;
	setAttr -s 4 ".wl[2239].w";
	setAttr ".wl[2239].w[0]" 0.39663262965716001;
	setAttr ".wl[2239].w[15]" 0.38856695424881799;
	setAttr ".wl[2239].w[16]" 0.03418724164776954;
	setAttr ".wl[2239].w[18]" 0.18061317444625244;
	setAttr -s 4 ".wl[2240].w";
	setAttr ".wl[2240].w[0]" 0.37812806514758451;
	setAttr ".wl[2240].w[15]" 0.42842204466425121;
	setAttr ".wl[2240].w[16]" 0.052606417525233561;
	setAttr ".wl[2240].w[18]" 0.14084347266293076;
	setAttr -s 4 ".wl[2241].w";
	setAttr ".wl[2241].w[0]" 0.36770471073374245;
	setAttr ".wl[2241].w[15]" 0.39172410056619367;
	setAttr ".wl[2241].w[16]" 0.055644225818643679;
	setAttr ".wl[2241].w[18]" 0.1849269628814203;
	setAttr -s 4 ".wl[2242].w";
	setAttr ".wl[2242].w[0]" 0.28342280091601346;
	setAttr ".wl[2242].w[15]" 0.42183538737086934;
	setAttr ".wl[2242].w[16]" 0.14667503721365815;
	setAttr ".wl[2242].w[18]" 0.14806677449945915;
	setAttr -s 4 ".wl[2243].w";
	setAttr ".wl[2243].w[0]" 0.40697824253435866;
	setAttr ".wl[2243].w[15]" 0.33113778131189908;
	setAttr ".wl[2243].w[16]" 0.020400743889134501;
	setAttr ".wl[2243].w[18]" 0.24148323226460772;
	setAttr -s 4 ".wl[2244].w";
	setAttr ".wl[2244].w[0]" 0.24732901577270672;
	setAttr ".wl[2244].w[15]" 0.46798146560782045;
	setAttr ".wl[2244].w[16]" 0.15623947262313026;
	setAttr ".wl[2244].w[18]" 0.12845004599634269;
	setAttr -s 4 ".wl[2245].w";
	setAttr ".wl[2245].w[0]" 0.26515997172054062;
	setAttr ".wl[2245].w[15]" 0.44275531085734093;
	setAttr ".wl[2245].w[16]" 0.15356084739763029;
	setAttr ".wl[2245].w[18]" 0.13852387002448799;
	setAttr -s 4 ".wl[2246].w";
	setAttr ".wl[2246].w[0]" 0.26803989618736024;
	setAttr ".wl[2246].w[15]" 0.46077136173922523;
	setAttr ".wl[2246].w[16]" 0.15430313049280792;
	setAttr ".wl[2246].w[18]" 0.1168856115806066;
	setAttr -s 4 ".wl[2247].w";
	setAttr ".wl[2247].w[0]" 0.2643485777263494;
	setAttr ".wl[2247].w[15]" 0.42260741630437371;
	setAttr ".wl[2247].w[16]" 0.14778412473548477;
	setAttr ".wl[2247].w[18]" 0.16525988123379221;
	setAttr -s 4 ".wl[2248].w";
	setAttr ".wl[2248].w[0]" 0.38171803634467455;
	setAttr ".wl[2248].w[15]" 0.30146427500979761;
	setAttr ".wl[2248].w[16]" 0.023347716774922551;
	setAttr ".wl[2248].w[18]" 0.29346997187060525;
	setAttr -s 4 ".wl[2249].w";
	setAttr ".wl[2249].w[0]" 0.40951661546618612;
	setAttr ".wl[2249].w[15]" 0.33806376271748034;
	setAttr ".wl[2249].w[16]" 0.019085324329561264;
	setAttr ".wl[2249].w[18]" 0.23333429748677234;
	setAttr -s 4 ".wl[2250].w";
	setAttr ".wl[2250].w[0]" 0.42497326836559751;
	setAttr ".wl[2250].w[15]" 0.36678431494656927;
	setAttr ".wl[2250].w[16]" 0.018861261011426413;
	setAttr ".wl[2250].w[18]" 0.18938115567640684;
	setAttr -s 4 ".wl[2251].w";
	setAttr ".wl[2251].w[0]" 0.400120936164166;
	setAttr ".wl[2251].w[1]" 0.02460664467790373;
	setAttr ".wl[2251].w[15]" 0.33049132160094669;
	setAttr ".wl[2251].w[18]" 0.24478109755698346;
	setAttr -s 4 ".wl[2252].w";
	setAttr ".wl[2252].w[0]" 0.3158008039567155;
	setAttr ".wl[2252].w[15]" 0.40705380577185374;
	setAttr ".wl[2252].w[16]" 0.089732048339847673;
	setAttr ".wl[2252].w[18]" 0.18741334193158291;
	setAttr -s 4 ".wl[2253].w";
	setAttr ".wl[2253].w[0]" 0.31003012069095398;
	setAttr ".wl[2253].w[15]" 0.45517138013552361;
	setAttr ".wl[2253].w[16]" 0.090844597769922347;
	setAttr ".wl[2253].w[18]" 0.1439539014035999;
	setAttr -s 4 ".wl[2254].w";
	setAttr ".wl[2254].w[0]" 0.33051953015007385;
	setAttr ".wl[2254].w[15]" 0.45051768509717738;
	setAttr ".wl[2254].w[16]" 0.091292865957612213;
	setAttr ".wl[2254].w[18]" 0.12766991879513662;
	setAttr -s 4 ".wl[2255].w";
	setAttr ".wl[2255].w[0]" 0.33509345952877195;
	setAttr ".wl[2255].w[15]" 0.41013868775735807;
	setAttr ".wl[2255].w[16]" 0.090507027559756911;
	setAttr ".wl[2255].w[18]" 0.16426082515411305;
	setAttr -s 4 ".wl[2256].w";
	setAttr ".wl[2256].w[0]" 0.36888309668372676;
	setAttr ".wl[2256].w[15]" 0.34606051367287721;
	setAttr ".wl[2256].w[16]" 0.035639866674157902;
	setAttr ".wl[2256].w[18]" 0.24941652296923819;
	setAttr -s 4 ".wl[2257].w";
	setAttr ".wl[2257].w[0]" 0.38622497100390285;
	setAttr ".wl[2257].w[15]" 0.38960450885221781;
	setAttr ".wl[2257].w[16]" 0.03168099191869253;
	setAttr ".wl[2257].w[18]" 0.19248952822518664;
	setAttr -s 4 ".wl[2258].w";
	setAttr ".wl[2258].w[0]" 0.40252978097265002;
	setAttr ".wl[2258].w[15]" 0.40356451403991678;
	setAttr ".wl[2258].w[16]" 0.031731552494671404;
	setAttr ".wl[2258].w[18]" 0.16217415249276176;
	setAttr -s 4 ".wl[2259].w";
	setAttr ".wl[2259].w[0]" 0.38463340070216517;
	setAttr ".wl[2259].w[15]" 0.36702181486649654;
	setAttr ".wl[2259].w[16]" 0.03611336214021018;
	setAttr ".wl[2259].w[18]" 0.2122314222911281;
	setAttr -s 21 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 -0
		 0.99999999999999978 2.2204460492503121e-16 -0 0 0 -0 1 -0 -5.4491582185681535 -1.2099561838159529e-15 0.33987859393488401 1;
	setAttr ".pm[1]" -type "matrix" 0.11902179966517194 -0.94832950200624111 0.29411726713856451 -0
		 0.92697494311205464 2.7755575615628894e-16 -0.37512325286817694 0 0.35574044758343976 0.3172871816266678 0.87907768617371906 -0
		 -6.8060640447730263 0.10783912116483201 3.1019498453537606 1;
	setAttr ".pm[2]" -type "matrix" -0.032493468125590667 0.95155560116389448 -0.30575498756813491 -0
		 0.99944420808691625 0.028655626416012128 -0.01703320275879867 0 -0.0074464387923101893 -0.30613851924957941 -0.95195785493948593 0
		 -9.6210265938591135 -0.26796178682696359 0.11799271100348441 1;
	setAttr ".pm[3]" -type "matrix" -0.10949838929879632 0.95189885943234465 -0.28617942650087702 -0
		 0.95007361112851119 0.015598078646303221 -0.3116357382903539 0 -0.29218185463542995 -0.30601513255710966 -0.90608195129792513 0
		 -10.706809276442849 -0.098038864258253478 3.6031640527143907 1;
	setAttr ".pm[4]" -type "matrix" -0.31311346415092134 -0.10441438992348299 0.94395847034936653 -0
		 -0.36025455761109365 0.93271114505762565 -0.016327081972195141 0 -0.87873580346304592 -0.34517757033474616 -0.32966017753128268 0
		 4.2434054225082729 -11.779147514542743 0.18328437010568754 1;
	setAttr ".pm[5]" -type "matrix" -0.032780148209784095 -0.66512523436318505 0.74601198683175474 -0
		 -0.47218419311516657 0.66816760856773816 0.57497315992405063 0 -0.88089020295126641 -0.33340736265743637 -0.33596425534614449 0
		 5.5938519487133647 -8.796830146154905 -7.5479426253821034 1;
	setAttr ".pm[6]" -type "matrix" 0.74601198683175485 -0.45479082334673587 0.48644775927424677 -0
		 0.57497315992405063 0.80841714547943244 -0.12596659978675839 0 -0.33596425534614466 0.37366699866924791 0.86458139769210118 -0
		 -7.5479426253821034 -9.8255528372387282 2.5021337732958617 1;
	setAttr ".pm[7]" -type "matrix" 0.70448597425133797 -0.64590598793793907 -0.29411726713856468 -0
		 -0.53648397290854377 -0.75595468909840202 0.37512325286817705 0 -0.46463368248051873 -0.10647987028566804 -0.87907768617371917 -0
		 4.8234337060922403 6.1404418691957883 -3.6303281212711362 1;
	setAttr ".pm[8]" -type "matrix" 0.93632490307378058 -0.29353742080764478 -0.19269524765668364 -0
		 -0.29335462258927764 -0.95552841923980492 0.030141423831350062 0 -0.19297342119933822 0.028305875903679317 -0.98079561382581038 -0
		 3.9016260179552296 8.2094122361094364 -0.81844195034279432 1;
	setAttr ".pm[9]" -type "matrix" 0.98152727650055149 -0.04186815633318406 -0.18668492968279424 -0
		 -0.031826189826567035 -0.99789698501986746 0.056468583560959407 0 -0.18865656396355007 -0.04948398502058831 -0.98079561382581071 -0
		 3.3087595386982018 8.8453938588204473 -1.3316513003548018 1;
	setAttr ".pm[10]" -type "matrix" 0.98226436959005481 -0.017476992395205421 -0.18668492968279424 -0
		 -0.007031548336599548 -0.99837961537613951 0.0564685835609594 0 -0.18736932929869271 -0.054154393526369797 -0.9807956138258106 -0
		 5.0788019590806668 8.9558614474345095 -1.6971978870187348 1;
	setAttr ".pm[11]" -type "matrix" 0.84225299464722247 -0.45177973192555626 0.29411726713856445 -0
		 0.53648397290854366 0.75595468909840213 -0.37512325286817694 0 -0.052866244618377599 0.47373788306553016 0.87907768617371906 -0
		 -5.5065083054840578 -6.400771652941005 3.2613129967341488 1;
	setAttr ".pm[12]" -type "matrix" 0.6507194345629298 0.74638212805996618 0.13956337769908328 -0
		 -0.62331230476989063 0.63003267853385103 -0.46318527038530477 0 -0.43364269647319975 0.21441208662791697 0.87520367281182898 -0
		 4.3125378977054734 -7.1659455080069305 3.9700733759178837 1;
	setAttr ".pm[13]" -type "matrix" 0.14387546818535368 0.98579944684658916 0.086598500283803309 -0
		 -0.86331965859585924 0.1678092257197093 -0.4759403647992892 0 -0.48371377563221651 -0.0062860448861206536 0.87520367281182931 -0
		 7.0421117136472517 -4.9296062724129328 4.2744891041949407 1;
	setAttr ".pm[14]" -type "matrix" 0.11934672815588325 0.98906878336497994 0.086598500283803309 -0
		 -0.86722122265026191 0.14631513981712263 -0.4759403647992892 0 -0.4834084292432958 -0.018298131959686222 0.87520367281182931 -0
		 6.9455008458462677 -6.4477976805557802 4.0884825345060793 1;
	setAttr ".pm[15]" -type "matrix" -0.27932954083013128 -0.96019529660357505 -1.1759000965419119e-16 0
		 -0.96019529660357483 0.27932954083013128 3.4208002808492021e-17 0 1.2325951644078301e-32 1.2246467991473537e-16 -1 -0
		 4.6886333045962507 -2.0963855089020735 -0.30386923284970796 1;
	setAttr ".pm[16]" -type "matrix" -0.047944799368398842 -0.99884998684162951 -1.1759000965419119e-16 -0
		 -0.99884998684162929 0.047944799368398898 3.4208002808492021e-17 0 -2.8530833734542409e-17 1.1909487542724279e-16 -1 -0
		 2.8194233519588523 -1.4802709252696435 -0.30386923284970757 1;
	setAttr ".pm[17]" -type "matrix" 1 -1.2698175844150226e-15 -4.8746702605440892e-18 -0
		 1.3183898417423732e-15 0.99999999999999978 -3.4208002808492193e-17 0 4.8746702605441285e-18 3.4208002808492187e-17 1 -0
		 1.4634155678824088 -0.38665934138171248 0.30386923284970718 1;
	setAttr ".pm[18]" -type "matrix" -0.27932954083013117 -0.92619594191811239 0.25325103118062098 -0
		 0.96019529660357505 -0.26943881946708753 0.073673027252544349 0 0 0.2637495018736567 0.96459120888664418 -0
		 -4.651634883475638 2.0921520711358581 -0.256118514687523 1;
	setAttr ".pm[19]" -type "matrix" -0.055865694897517575 -0.96578617028523928 0.25325105610715093 -0
		 0.86545353467325814 0.079638343082102658 0.49461895801990857 -0 -0.4978646437086896 0.24680925346693777 0.83139400343514158 -0
		 -2.5420285577943234 1.1457839631535465 -1.4958276088521705 1;
	setAttr ".pm[20]" -type "matrix" 0.96735397954346003 0.0094970268402252821 -0.25325103107909713 -0
		 -0.12104074127100831 -0.86064000266357132 -0.49461896927616072 0 -0.22265537768936694 0.50912532081644013 -0.83139400436228872 -0
		 -1.5072127925376091 0.19960714001270663 0.10715133519858314 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 21 ".ma";
	setAttr -s 21 ".dpf[0:20]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4;
	setAttr -s 21 ".lw";
	setAttr -s 21 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 4;
	setAttr ".bm" 0;
	setAttr ".ucm" yes;
	setAttr -s 21 ".ifcl";
	setAttr -s 21 ".ifcl";
createNode tweak -n "tweak1";
createNode objectSet -n "skinCluster1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	setAttr -s 21 ".wm";
	setAttr -s 21 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 5.4491582185681553 -0.33987859393488407 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.70710678118654746 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0.3463803198695708 -0.36370622230242799
		 -0.12769938177315457 0 2.0235061283753679 4.4930861883848816e-16 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 -0.40173701482661595 0 0 2.0991336804737264
		 0.11516769444217495 -0.55144286349475469 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.99988051219709906 0.015458374056376274 9.4655241540818459e-19 6.1225023439599671e-17 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 -0.29991781757660857 0 0 1.6701197483486727
		 -0.022412921792497549 -0.16109101394102229 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0.006531320775175738 0.99997867069699631 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0.0029582065813437293 0.033851381954467838
		 -0.00076134297775614333 0 1.2754023665843861 0.11119272452583498 -0.026054209310318207 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48245802690457906 -0.51694704977912431 -0.50825069945995915 0.49161084863788579 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 -0.58092371047450753 -0.27044543982856978
		 0.5802577231264664 0 0.097993152295449193 0.40518695407372785 0.11703644683499803 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.35163678344814608 0.93613651383024343 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.55223104303847681 4.4408920985006262e-16
		 -4.6808343636699594e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.26548649906989619 -0.65537540296505636 0.26548649906989613 0.65537540296505647 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.99305798714378879 0.48762142368715317
		 -0.5283782759173753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.45894062473204406 0.88846693971726443 2.8102008353839217e-17 5.4402909693649615e-17 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 -0.30905218949160501 0.21750576920909526
		 0.24396787150298624 0 -0.86815636083539294 0.26164768849594733 0.41102852761173653 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.30709758194430758 0.9516780312500438 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0.026526884936977659 0.005061419459183536
		 -0.29811140026774019 0 -1.6204449608153177 0.51334394850804088 0.29486052711679411 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.015965166793684864 0.99987254860269559 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.9909129089091606 0.018437670344332465
		 0.3655465866639333 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.012419485958776851 0.99992287521014322 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.6006861650078306 -0.67833031464881477
		 -0.15936315138038804 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.45894062473204394 0.88846693971726443 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0.1726923488929106 -0.044015353668083179
		 -0.70741343420020431 0 1.0223972869332421 -0.49611474100631509 0.39449258542711874 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.30709758194430747 0.9516780312500438 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0.054380280348142136 0.0033913837203224865
		 -0.60266669669759088 0 1.1599945484980763 0.98352890491756506 -0.0062502672026289148 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.015965166793684867 0.99987254860269559 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.25889777981505097 1.6887081396482226
		 0.1860065696888607 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.012419485958778446 0.99992287521014322 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.36157217039139677
		 0.70326571742193122 0.036009361085176383 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.14107569492372635 0.98999881227291764 6.0619943830485503e-17 8.6383949112914984e-18 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.2916523184733042 -8.8817841970012523e-16
		 -2.8064646765769431e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.11729565599236053 0.99309703910812352 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.5033718397022513 1.2490009027033016e-16
		 -2.9813875739740733e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.68994753446606349 0.72385937839072034 4.2247101983173643e-17 4.432360353894942e-17 1
		 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0.26690728994286178 0 0 0 -0.40010427143280314
		 -0.70326599999999995 0.035124195029551775 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0.14107569492372651 0.98999881227291764 1 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 -0.0007192381033854407 0.44518329686284552
		 -0.11873845314816507 0 -2.2916480794997516 1.0474857785602865e-06 9.8607600000000006e-32 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1.2815702926508722e-08 -1.5136751219377993e-09 -0.1172956559923606 0.99309703910812341 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 -2.4149139824186348 -0.36926563640998122
		 -1.388676236684746 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.68994753446606349 -0.72385937839072023 5.4719769002267321e-09 1.2591625801931057e-08 1
		 1 1 yes;
	setAttr -s 21 ".m";
	setAttr -s 21 ".p";
	setAttr ".bp" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 3 ".s";
select -ne :defaultTextureList1;
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "root.s" "spine1.is";
connectAttr "spine1.s" "neck.is";
connectAttr "neck.s" "head.is";
connectAttr "head.s" "hat1.is";
connectAttr "hat1.s" "hat2.is";
connectAttr "hat2.s" "hat3.is";
connectAttr "spine1.s" "R_shoulder1.is";
connectAttr "R_shoulder1.s" "R_shoulder2.is";
connectAttr "R_shoulder2.s" "R_elbow.is";
connectAttr "R_elbow.s" "R_wrist.is";
connectAttr "spine1.s" "L_shoulder1.is";
connectAttr "L_shoulder1.s" "L_shoulder2.is";
connectAttr "L_shoulder2.s" "L_elbow.is";
connectAttr "L_elbow.s" "L_wrist.is";
connectAttr "root.s" "R_hip.is";
connectAttr "R_hip.s" "R_knee.is";
connectAttr "R_knee.s" "R_ankle.is";
connectAttr "root.s" "L_hip.is";
connectAttr "L_hip.s" "L_knee.is";
connectAttr "L_knee.s" "L_ankle.is";
connectAttr "skinCluster1.og[0]" "mordakTheFantasticShape.i";
connectAttr "groupId1.id" "mordakTheFantasticShape.iog.og[0].gid";
connectAttr "MordakTheFantastic:polySurface1SG1.mwc" "mordakTheFantasticShape.iog.og[0].gco"
		;
connectAttr "skinCluster1GroupId.id" "mordakTheFantasticShape.iog.og[7].gid";
connectAttr "skinCluster1Set.mwc" "mordakTheFantasticShape.iog.og[7].gco";
connectAttr "groupId3.id" "mordakTheFantasticShape.iog.og[8].gid";
connectAttr "tweakSet1.mwc" "mordakTheFantasticShape.iog.og[8].gco";
connectAttr "tweak1.vl[0].vt[0]" "mordakTheFantasticShape.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MordakTheFantastic:polySurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "MordakTheFantastic:polySurface1SG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MordakTheFantastic:polySurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "MordakTheFantastic:polySurface1SG1.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "PSD_blinn1_color.oc" "blinn1.c";
connectAttr "PSD_blinn1_color.ot" "blinn1.it";
connectAttr "blinn1.oc" "MordakTheFantastic:polySurface1SG.ss";
connectAttr "MordakTheFantastic:polySurface1SG.msg" "materialInfo1.sg";
connectAttr "blinn1.msg" "materialInfo1.m";
connectAttr "PSD_blinn1_color.msg" "materialInfo1.t" -na;
connectAttr "blinn1.oc" "MordakTheFantastic:polySurface1SG1.ss";
connectAttr "mordakTheFantasticShape.iog.og[0]" "MordakTheFantastic:polySurface1SG1.dsm"
		 -na;
connectAttr "groupId1.msg" "MordakTheFantastic:polySurface1SG1.gn" -na;
connectAttr "MordakTheFantastic:polySurface1SG1.msg" "materialInfo2.sg";
connectAttr "blinn1.msg" "materialInfo2.m";
connectAttr "PSD_blinn1_color.msg" "materialInfo2.t" -na;
connectAttr "place2dTexture1.o" "PSD_blinn1_color.uv";
connectAttr "place2dTexture1.ofu" "PSD_blinn1_color.ofu";
connectAttr "place2dTexture1.ofv" "PSD_blinn1_color.ofv";
connectAttr "place2dTexture1.rf" "PSD_blinn1_color.rf";
connectAttr "place2dTexture1.reu" "PSD_blinn1_color.reu";
connectAttr "place2dTexture1.rev" "PSD_blinn1_color.rev";
connectAttr "place2dTexture1.vt1" "PSD_blinn1_color.vt1";
connectAttr "place2dTexture1.vt2" "PSD_blinn1_color.vt2";
connectAttr "place2dTexture1.vt3" "PSD_blinn1_color.vt3";
connectAttr "place2dTexture1.vc1" "PSD_blinn1_color.vc1";
connectAttr "place2dTexture1.ofs" "PSD_blinn1_color.fs";
connectAttr "mordakTheFantasticShapeOrig.w" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "root.wm" "skinCluster1.ma[0]";
connectAttr "spine1.wm" "skinCluster1.ma[1]";
connectAttr "neck.wm" "skinCluster1.ma[2]";
connectAttr "head.wm" "skinCluster1.ma[3]";
connectAttr "hat1.wm" "skinCluster1.ma[4]";
connectAttr "hat2.wm" "skinCluster1.ma[5]";
connectAttr "hat3.wm" "skinCluster1.ma[6]";
connectAttr "R_shoulder1.wm" "skinCluster1.ma[7]";
connectAttr "R_shoulder2.wm" "skinCluster1.ma[8]";
connectAttr "R_elbow.wm" "skinCluster1.ma[9]";
connectAttr "R_wrist.wm" "skinCluster1.ma[10]";
connectAttr "L_shoulder1.wm" "skinCluster1.ma[11]";
connectAttr "L_shoulder2.wm" "skinCluster1.ma[12]";
connectAttr "L_elbow.wm" "skinCluster1.ma[13]";
connectAttr "L_wrist.wm" "skinCluster1.ma[14]";
connectAttr "R_hip.wm" "skinCluster1.ma[15]";
connectAttr "R_knee.wm" "skinCluster1.ma[16]";
connectAttr "R_ankle.wm" "skinCluster1.ma[17]";
connectAttr "L_hip.wm" "skinCluster1.ma[18]";
connectAttr "L_knee.wm" "skinCluster1.ma[19]";
connectAttr "L_ankle.wm" "skinCluster1.ma[20]";
connectAttr "root.liw" "skinCluster1.lw[0]";
connectAttr "spine1.liw" "skinCluster1.lw[1]";
connectAttr "neck.liw" "skinCluster1.lw[2]";
connectAttr "head.liw" "skinCluster1.lw[3]";
connectAttr "hat1.liw" "skinCluster1.lw[4]";
connectAttr "hat2.liw" "skinCluster1.lw[5]";
connectAttr "hat3.liw" "skinCluster1.lw[6]";
connectAttr "R_shoulder1.liw" "skinCluster1.lw[7]";
connectAttr "R_shoulder2.liw" "skinCluster1.lw[8]";
connectAttr "R_elbow.liw" "skinCluster1.lw[9]";
connectAttr "R_wrist.liw" "skinCluster1.lw[10]";
connectAttr "L_shoulder1.liw" "skinCluster1.lw[11]";
connectAttr "L_shoulder2.liw" "skinCluster1.lw[12]";
connectAttr "L_elbow.liw" "skinCluster1.lw[13]";
connectAttr "L_wrist.liw" "skinCluster1.lw[14]";
connectAttr "R_hip.liw" "skinCluster1.lw[15]";
connectAttr "R_knee.liw" "skinCluster1.lw[16]";
connectAttr "R_ankle.liw" "skinCluster1.lw[17]";
connectAttr "L_hip.liw" "skinCluster1.lw[18]";
connectAttr "L_knee.liw" "skinCluster1.lw[19]";
connectAttr "L_ankle.liw" "skinCluster1.lw[20]";
connectAttr "root.obcc" "skinCluster1.ifcl[0]";
connectAttr "spine1.obcc" "skinCluster1.ifcl[1]";
connectAttr "neck.obcc" "skinCluster1.ifcl[2]";
connectAttr "head.obcc" "skinCluster1.ifcl[3]";
connectAttr "hat1.obcc" "skinCluster1.ifcl[4]";
connectAttr "hat2.obcc" "skinCluster1.ifcl[5]";
connectAttr "hat3.obcc" "skinCluster1.ifcl[6]";
connectAttr "R_shoulder1.obcc" "skinCluster1.ifcl[7]";
connectAttr "R_shoulder2.obcc" "skinCluster1.ifcl[8]";
connectAttr "R_elbow.obcc" "skinCluster1.ifcl[9]";
connectAttr "R_wrist.obcc" "skinCluster1.ifcl[10]";
connectAttr "L_shoulder1.obcc" "skinCluster1.ifcl[11]";
connectAttr "L_shoulder2.obcc" "skinCluster1.ifcl[12]";
connectAttr "L_elbow.obcc" "skinCluster1.ifcl[13]";
connectAttr "L_wrist.obcc" "skinCluster1.ifcl[14]";
connectAttr "R_hip.obcc" "skinCluster1.ifcl[15]";
connectAttr "R_knee.obcc" "skinCluster1.ifcl[16]";
connectAttr "R_ankle.obcc" "skinCluster1.ifcl[17]";
connectAttr "L_hip.obcc" "skinCluster1.ifcl[18]";
connectAttr "L_knee.obcc" "skinCluster1.ifcl[19]";
connectAttr "L_ankle.obcc" "skinCluster1.ifcl[20]";
connectAttr "hat2.msg" "skinCluster1.ptt";
connectAttr "groupParts3.og" "tweak1.ip[0].ig";
connectAttr "groupId3.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "mordakTheFantasticShape.iog.og[7]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId3.msg" "tweakSet1.gn" -na;
connectAttr "mordakTheFantasticShape.iog.og[8]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "groupParts1.og" "groupParts3.ig";
connectAttr "groupId3.id" "groupParts3.gi";
connectAttr "root.msg" "bindPose1.m[0]";
connectAttr "spine1.msg" "bindPose1.m[1]";
connectAttr "neck.msg" "bindPose1.m[2]";
connectAttr "head.msg" "bindPose1.m[3]";
connectAttr "hat1.msg" "bindPose1.m[4]";
connectAttr "hat2.msg" "bindPose1.m[5]";
connectAttr "hat3.msg" "bindPose1.m[6]";
connectAttr "R_shoulder1.msg" "bindPose1.m[7]";
connectAttr "R_shoulder2.msg" "bindPose1.m[8]";
connectAttr "R_elbow.msg" "bindPose1.m[9]";
connectAttr "R_wrist.msg" "bindPose1.m[10]";
connectAttr "L_shoulder1.msg" "bindPose1.m[11]";
connectAttr "L_shoulder2.msg" "bindPose1.m[12]";
connectAttr "L_elbow.msg" "bindPose1.m[13]";
connectAttr "L_wrist.msg" "bindPose1.m[14]";
connectAttr "R_hip.msg" "bindPose1.m[15]";
connectAttr "R_knee.msg" "bindPose1.m[16]";
connectAttr "R_ankle.msg" "bindPose1.m[17]";
connectAttr "L_hip.msg" "bindPose1.m[18]";
connectAttr "L_knee.msg" "bindPose1.m[19]";
connectAttr "L_ankle.msg" "bindPose1.m[20]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[1]" "bindPose1.p[7]";
connectAttr "bindPose1.m[7]" "bindPose1.p[8]";
connectAttr "bindPose1.m[8]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[1]" "bindPose1.p[11]";
connectAttr "bindPose1.m[11]" "bindPose1.p[12]";
connectAttr "bindPose1.m[12]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[0]" "bindPose1.p[15]";
connectAttr "bindPose1.m[15]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[0]" "bindPose1.p[18]";
connectAttr "bindPose1.m[18]" "bindPose1.p[19]";
connectAttr "bindPose1.m[19]" "bindPose1.p[20]";
connectAttr "root.bps" "bindPose1.wm[0]";
connectAttr "spine1.bps" "bindPose1.wm[1]";
connectAttr "neck.bps" "bindPose1.wm[2]";
connectAttr "head.bps" "bindPose1.wm[3]";
connectAttr "hat1.bps" "bindPose1.wm[4]";
connectAttr "hat2.bps" "bindPose1.wm[5]";
connectAttr "hat3.bps" "bindPose1.wm[6]";
connectAttr "R_shoulder1.bps" "bindPose1.wm[7]";
connectAttr "R_shoulder2.bps" "bindPose1.wm[8]";
connectAttr "R_elbow.bps" "bindPose1.wm[9]";
connectAttr "R_wrist.bps" "bindPose1.wm[10]";
connectAttr "L_shoulder1.bps" "bindPose1.wm[11]";
connectAttr "L_shoulder2.bps" "bindPose1.wm[12]";
connectAttr "L_elbow.bps" "bindPose1.wm[13]";
connectAttr "L_wrist.bps" "bindPose1.wm[14]";
connectAttr "R_hip.bps" "bindPose1.wm[15]";
connectAttr "R_knee.bps" "bindPose1.wm[16]";
connectAttr "R_ankle.bps" "bindPose1.wm[17]";
connectAttr "L_hip.bps" "bindPose1.wm[18]";
connectAttr "L_knee.bps" "bindPose1.wm[19]";
connectAttr "L_ankle.bps" "bindPose1.wm[20]";
connectAttr "MordakTheFantastic:polySurface1SG.pa" ":renderPartition.st" -na;
connectAttr "MordakTheFantastic:polySurface1SG1.pa" ":renderPartition.st" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "PSD_blinn1_color.msg" ":defaultTextureList1.tx" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of mordak_001.ma
