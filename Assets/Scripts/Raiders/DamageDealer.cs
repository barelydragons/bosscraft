using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DamageDealer : Raider {
	public List<AudioClip> attackSoundOverride;
	protected override IEnumerator TargetRoutine() {
		while(true) {
			isAttacking = false;
			if(DistanceBetweenTarget == -1f || Target == null) {
				yield return new WaitForSeconds(AttackSpeed);
				continue;
			}
			if(DistanceBetweenTarget < Range && Alive && !Frozen) {
				isAttacking = true;
				Target.AddDamage((int)((float)Power * Random.Range(.9f,1.1f)));
				if(attackSoundOverride.Count > 0) {
					SoundManager.PlaySFX(this.gameObject,attackSoundOverride[Random.Range(0,attackSoundOverride.Count)]);
				} else if(AudioGroup != "") {
					SoundManager.PlaySFX(SoundManager.LoadFromGroup(AudioGroup), false, 0f, SoundManager.Instance.volumeSFX * .2f);
				}
				if(RaiderType == RaiderType.Ranged) {
					ParticleStream.Instance.RocketFire(damageCenter,Target.damageCenter);
				} else if(RaiderType == RaiderType.Wizard) {
                    ParticleStream.Instance.FireAttack(damageCenter, Target.damageCenter);
                } else if (RaiderType == RaiderType.Snowman) {
                    ParticleStream.Instance.SnowAttack(damageCenter, Target.damageCenter);
                }
			}
			yield return new WaitForSeconds(AttackSpeed);
		}
	}

	public override void MoveToTarget(RaycastHit target) {
		if(target.transform.tag == "boss") {
			Target = target.transform.GetComponent<RPGCharacter>();
			MoveToRange(target.transform.position);
		} else if(target.transform.tag == "raider") {
			Target = null;
			MoveToRange(target.transform.position);
		}
	}
	
	public override void ActivateAbility() {
//		if(RaiderType == RaiderType.Melee) { 
//			Leap l = (Leap)(AbilityBank.Instance.Abilities[abilityName]);
//			Vector3 targetPos = IsMoving ? this.MovementDestination : Target.collider.ClosestPointOnBounds(this.transform.position);
//			targetPos.y = 4.5f; // Constant
//			l.Activate(this,targetPos);
//		} else {
			Ability l = AbilityBank.Instance.Abilities[abilityName];
			l.Activate(this);
//		}
		TriggerUltimate(UltAnimationTime);
		HPBarHandler.Instance.BeginCooldown(this,AbilityBank.Instance.Abilities[abilityName].CooldownLength);
	}
}
