﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Helicopter : Raider {
    public LayerMask attackMask, healMask;
    public Transform attackA, attackB;
    public GameObject heli;
    public List<AudioClip> attackSoundOverride;
    private int mode = 1; // 1 = heal, -1 = attack.

    public Texture2D attackTex;
    public Texture2D healTex;

    public void ToggleMode() {
        mode *= -1;
        this.Target = null;
        if (mode == 1) {
            this.targetMask = healMask;
            this.RaiderType = RaiderType.Support;
            StopCoroutine("AttackRoutine");
            StartCoroutine("TargetRoutine");
            heli.renderer.material.SetTexture("_Texture", healTex);
        } else {
            this.RaiderType = RaiderType.Ranged;
            this.targetMask = attackMask;
            StartCoroutine("AttackRoutine");
            StopCoroutine("TargetRoutine");
            heli.renderer.material.SetTexture("_Texture", attackTex);
        }
    }

    private IEnumerator AttackRoutine() {
        while (true) {
            isAttacking = false;
            if (DistanceBetweenTarget == -1f || Target == null || Target == this || Target.isRaider()) {
                yield return new WaitForSeconds(AttackSpeed);
                continue;
            }
            if (DistanceBetweenTarget < Range && Alive && !Frozen) {
                isAttacking = true;
                Target.AddDamage((int)((float)Power * Random.Range(.9f, 1.1f)));
                if (attackSoundOverride.Count > 0) {
                    SoundManager.PlaySFX(attackSoundOverride[Random.Range(0, attackSoundOverride.Count)],false ,0f,.2f);
                } else if (AudioGroup != "") {
                    SoundManager.PlaySFX(SoundManager.LoadFromGroup(AudioGroup), false, 0f, SoundManager.Instance.volumeSFX * .2f);
                }
                ParticleStream.Instance.RocketFire(attackA, Target.damageCenter);
                ParticleStream.Instance.RocketFire(attackB, Target.damageCenter);
            }
            yield return new WaitForSeconds(AttackSpeed);
        }
    }
    
	protected override IEnumerator TargetRoutine() {
		while(true) {
			isAttacking = false;
			if(DistanceBetweenTarget == -1f || Target == null || !Target.isRaider()) {
				GetNewHealerTarget();
				yield return new WaitForSeconds(AttackSpeed);
				continue;
			}
			// Somehow the healer got out of range of his target, go back to healing stuff nearby
			if((!this.IsMoving && DistanceBetweenTarget > Range) || !Target.Alive) {
				GetNewHealerTarget();
			}
			if(Alive && ((DistanceBetweenTarget < Range && Target.Alive) || Target == this)) {
				if(!IsMoving && !Frozen) {
					isAttacking = true;
					Target.Heal((int)((float)Power * Random.Range(.8f,1.1f)));
					if(AudioGroup != "") {
						SoundManager.PlaySFX(SoundManager.LoadFromGroup(AudioGroup), false, 0f, SoundManager.Instance.volumeSFX * .2f);
					}
                    Transform s = Random.Range(0, 1f) < .5f ? attackA : attackB;
                    ParticleStream.Instance.HeliHeal(s, Target.transform);
                    ParticleStream.Instance.HeliHeal(s, Target.transform);
				}
			}
			yield return new WaitForSeconds(AttackSpeed);
		}
	}

	private void GetNewHealerTarget() {
		// Set to lowest HP person in range automatically, can be overridden by setting the target
		// This totally can be null!  This means the player has to move the healer in range of low HP people
		Target = RaiderPool.LowestHPRaiderInCollection(RaiderPool.RaidersInRange(this));
	}

    public override void Death() {
        base.Death();
        heli.transform.positionTo(.8f, new Vector3(transform.position.x, 0, transform.position.z));
    }

	public override void MoveToTarget(RaycastHit target) {
		Target = target.transform.GetComponent<RPGCharacter>();
		if(Target != this)
			MoveToRange(Target.transform.position);
	}
}