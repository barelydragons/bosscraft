﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaiderPool {
	// Static collections of helper methods, useful for targetting and what not.
	private static List<Healer> healers = new List<Healer>();
	private static List<Tank> tanks = new List<Tank>();
	private static List<DamageDealer> melee = new List<DamageDealer>();
	private static List<DamageDealer> ranged = new List<DamageDealer>();
	private static List<Raider> raiders = new List<Raider>();
	public static List<Raider> AllRaiders {
		get {return raiders;}
	}

	public static void FreezeAll() {
		foreach(Raider r in raiders)
			r.Frozen = true;
	}

	public static void UnFreezeAll() {
		foreach(Raider r in raiders)
			r.Frozen = false;
	}
	
	public static void Reset() {
		healers = new List<Healer>();
		tanks = new List<Tank>();
		melee = new List<DamageDealer>();
		ranged = new List<DamageDealer>();
		raiders = new List<Raider>();
	}

	public static void AddRaider(Raider r) {
		if(r.RaiderType == RaiderType.Melee)
			melee.Add(r as DamageDealer);
		else if(r.RaiderType == RaiderType.Ranged)
			ranged.Add(r as DamageDealer);
		else if(r.RaiderType == RaiderType.Support)
			healers.Add(r as Healer);
		else
			tanks.Add(r as Tank);
		raiders.Add(r);
	}

	// Type casting SHENANIGANS
	public static Raider LowestHPRaiderInCollection<T>(List<T> someRaiders) {
		if(someRaiders.Count == 0)
			return null;
		Raider lowest = null;
		for(int i = 0; i < someRaiders.Count; i++) {
			Raider r = (Raider)(object)someRaiders[i];
			if(r.Alive && (lowest == null || lowest.CurrentHP > r.CurrentHP)) {
				lowest = (Raider)(object)raiders[i];
			}
		}
		return lowest;
	}

	public static Raider LowestHP() {
		return LowestHPRaiderInCollection(raiders);
	}

	public static Raider LowestHP(RaiderType type) {
		if(type == RaiderType.Melee) {
			return LowestHPRaiderInCollection(melee);
		} else if(type == RaiderType.Ranged) {
			return LowestHPRaiderInCollection(ranged);
		} else if(type == RaiderType.Support) {
			return LowestHPRaiderInCollection(healers);
		} else if(type == RaiderType.Tank) {
			return LowestHPRaiderInCollection(tanks);
		} else {
			return LowestHP();
		}
	}

	// IDK if this is faster than sphere cast, since these are cached, it probably is.
	public static List<Raider> RaidersInRange(Raider aRaider) {
		List<Raider> raiderList = new List<Raider>();
		foreach(Raider r in raiders) {
			if(Vector3.Distance(r.transform.position,aRaider.transform.position) < aRaider.Range) {
				raiderList.Add(r);
			}
		}
		return raiderList;
	}
}
