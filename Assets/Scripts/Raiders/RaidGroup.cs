﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// In the end, this probalby shouldn't be a mono behavior as raid groups will be defined solely during run time.  But in the mean time this lets us build the groups in the inspector
// Extends IEnumerable for quality of life behaviors like iterators
public class RaidGroup : MonoBehaviour, ControlInterface, IEnumerable {
	public List<RPGCharacter> raiders = new List<RPGCharacter>();
	public float distApart = 3f;

	public bool IsClear {
		get { return !(raiders.Count > 0); }
	}
	// Use this for initialization
	void Start () {
		raiders = new List<RPGCharacter>(GetComponentsInChildren<RPGCharacter>());
	}

	public void Select() {
		foreach(RPGCharacter raider in this) {
			((Raider)raider).SelectedByController();
		}
	}

	public void Deselect() {
		foreach(RPGCharacter raider in this) {
			((Raider)raider).DeSelectedByController();
		}
	}

	public void Clear() {
		Deselect();
		raiders = new List<RPGCharacter>();
	}

	public void Add(RPGCharacter item) {
		raiders.Add(item);
	}

	public void Add(RaidGroup group) {
		this.Add(group.raiders);
	}

	public void Add(List<RPGCharacter> group) {

		raiders.AddRange(group);
	}

	public IEnumerator<RPGCharacter> GetEnumerator() {
		return this.raiders.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator() {
		return GetEnumerator();
	}
	
	public void SetGroup(RaidGroup grp) {
		raiders = new List<RPGCharacter>(grp.raiders);
		foreach(RPGCharacter raider in raiders) {
			raider.transform.parent = this.transform;
		}
	}

	private void MoveTo(Vector3 target,bool moveToTarget,RaycastHit ahit) {
		int posSpace = 0;
		int dir = Camera.main.WorldToScreenPoint(target).x > Screen.width/2 ? -1 : 1;
		for(int i = 0; i < raiders.Count; i++) {
			if(!raiders[i].Alive) 
				continue;
			Vector3 pos = target;
			pos.x += posSpace * distApart * dir;
			if(moveToTarget) {
				((Raider)raiders[i]).MoveToTarget(ahit);

			} else {
				raiders[i].MoveTo(pos);
			}
			posSpace++;
		}
	}

	public void MoveTo(Vector3 pos) {
		MoveTo(pos,false,new RaycastHit());
	}

	// Unused currently... move to target is a raider only function that is context called that does what this normally should do
	public void MoveToRange(Vector3 pos) {
		MoveTo(pos,false,new RaycastHit());
	}

	public void MoveTo(Vector3 target, RaycastHit ahit) {
		MoveTo (target,true,ahit);
	}

	public void MoveHandler(RaycastHit ground, bool ghit, RaycastHit raid, bool rhit, RaycastHit boss, bool bhit) {
		// If the boss hit isn't null, move people to the boss who do stuff to the boss (not healers...)
		if(bhit == false && rhit == false && ghit == true) {
			MoveTo(ground.point);
			return;
		}

		if(bhit == true) {  // Everyone but support moves to the boss on this kind of hit
			for (int i = 0; i <raiders.Count; i++) {
				if(!raiders[i].Alive || ((Raider)raiders[i]).RaiderType == RaiderType.Support)
					continue;
				((Raider)raiders[i]).MoveToTarget(boss);
			}
		}

		if(rhit == true) {
			for (int i = 0; i < raiders.Count; i++) {
				Raider r = ((Raider)raiders[i]);
				if(!r.Alive)
					continue;
				if(r.RaiderType == RaiderType.Support) {
					r.MoveToTarget(raid);
				} else if(bhit == false) {
					r.MoveToRange(raid.point);
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
