﻿using UnityEngine;
using System.Collections;

public class Tank : Raider {
	public bool TauntedBoss = false;

	protected override IEnumerator TargetRoutine() {
		while(true) {
			isAttacking = false;
			if(DistanceBetweenTarget == -1f || Target == null) {
				yield return new WaitForSeconds(AttackSpeed);
				continue;
			}
			if(DistanceBetweenTarget < Range && Alive && !Frozen) {
				Target.Taunt(this);
				isAttacking = true;
				Target.AddDamage((int)((float)Power * Random.Range(.8f,1.2f)));
			} else {
				Target.LoseAggro(this);
			}

			yield return new WaitForSeconds(AttackSpeed);
		}
	}

	public override void Death() {
		base.Death();
		if(Target != null) {
			Target.LoseAggro(this);
		}
	}
	
	public override void MoveToTarget(RaycastHit target) {
		StopCoroutine("WaitForRangeAndTaunt");
		if(target.transform.tag == "boss") {
			Target = target.transform.GetComponent<RPGCharacter>();
			MoveToRange(target.transform.position);
			StartCoroutine("WaitForRangeAndTaunt");
		}
	}

	IEnumerator WaitForRangeAndTaunt() {
		while(DistanceBetweenTarget >= Range) {
			yield return 0;
		}
		Target.Taunt(this);
	}
}
