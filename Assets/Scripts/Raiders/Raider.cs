﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Raider : RPGCharacter {
	[Header("Raider Specific Items")]
	[Tooltip("Optional")]
	public RPGCharacter inspectorTarget;
	public Material selectedMaterial,unselectedMaterial;
	public bool useTestTarget = true;
	public bool movementLookAtTarget;
	public Projector targetProjector; // for when targetted by the boss;
	public RaiderType RaiderType; // Definable by the class, but this is just easier to deal with as a programmer...
	public float UltAnimationTime = 1f;
	private Vector3 initialRot;
	protected string abilityName;
	public string AbilityName {
		get { return abilityName; }
	}

	protected override void Awake() {
		base.Awake();
		PoolRegister();
		if(PhotonNetwork.inRoom && RoomHandler.Instance.IsBossPlayer()) {
			this.sendUpdates = false;
		}
		targetProjector.material = unselectedMaterial;
		initialRot.x = transform.rotation.x;
		initialRot.z = transform.rotation.z;
		StartCoroutine("TargetRoutine");
		StartCoroutine("LookAtRoutine");
		if(useTestTarget) {
			Target = inspectorTarget;
		}
		RaidController.OnRaiderSelect += RaiderSelected;
		Game.OnBattleEnd += GameEnd;
	}

    public override bool isRaider() {
        return true;
    }

	void GameEnd(GameStateArgs args) {
		print ("Unregistering from events: " + this.name);
		this.Frozen = true;
		Game.OnBattleEnd -= GameEnd;
		RaidController.OnRaiderSelect -= RaiderSelected;
	}

	public virtual void ActivateAbility() {
		try { // If the raider potentially has no abilities.
			AbilityBank.Instance.Abilities[abilityName].Activate(this);
			HPBarHandler.Instance.BeginCooldown(this,AbilityBank.Instance.Abilities[abilityName].CooldownLength);
			TriggerUltimate(UltAnimationTime);
		} catch(KeyNotFoundException e) {
			print (e.ToString());
		}
	}

	protected override void Start() {
		base.Start();
		HPBarHandler.Instance.CreateHPBar(this);
	}

	public void SelectedByController() {
		targetProjector.material = selectedMaterial;
	}
	
	public void DeSelectedByController() {
		targetProjector.material = unselectedMaterial;
	}

	IEnumerator BossTargetHandler() {
		while(BossPool.MainBoss.Target == this) {
			yield return 0;
		}
	}

	IEnumerator LookAtRoutine() {
		while(true) {
			if((IsMoving || (Target != null  && Target != this)) && Alive && !Frozen) {
				Vector3 lookPos = IsMoving ? MovementDestination : Target.transform.position;
				if(MovementDestination == this.transform.position) {
					yield return 0;
					continue;
				}
				Quaternion lookRot = Quaternion.LookRotation(lookPos - this.transform.position);
				Quaternion newRot = Quaternion.Slerp(this.transform.rotation, lookRot, Time.deltaTime * 6);
				newRot.x = initialRot.x;
				newRot.z = initialRot.z;
				this.transform.rotation = newRot;
			}
			yield return 0;
		}
	}

	private void RaiderSelected(RaiderSelectedArgs args) {
		if(!Alive || this.renderer == null)
			return;
		if(args.Selected == this) {
			this.renderer.material.color = selectedColor;
			print ("SELECTED");
			targetProjector.material = selectedMaterial;
		} else {
			this.renderer.material.color = unselectedColor;
			print ("DESELECTED");
			targetProjector.material = unselectedMaterial;
		}
	}

	protected override void LoadFromXML() {
		RaiderData data = DataHandler.Raiders[XMLName];
		RaiderType = data.RaiderType;
		abilityName = data.AbilityName.Trim();
		LoadFromXML(data);
	}

	protected void PoolRegister() {
		RaiderPool.AddRaider(this);
	}

	protected override void Update() {
		base.Update();
		int groundMask = (1 << 8);
		if(!Physics.Raycast(this.transform.position,Vector3.down,50f,groundMask)) {
			if(this.Alive) {
				this.Death();
				this.rigidbody.constraints = RigidbodyConstraints.None;
			}
		}
	}

	public override void Death() {
		base.Death();
		targetProjector.enabled = false;
	}

	// Received a target from the raid controller, the raider right clicked another raider for example
	// Or right clicked a boss--is the behavior any different?
	public abstract void MoveToTarget(RaycastHit target);
}
