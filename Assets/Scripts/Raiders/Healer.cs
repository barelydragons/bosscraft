﻿ using UnityEngine;
using System.Collections;

public class Healer : Raider {
	protected override IEnumerator TargetRoutine() {
		while(true) {
			isAttacking = false;
			if(DistanceBetweenTarget == -1f || Target == null) {
				GetNewHealerTarget();
				yield return new WaitForSeconds(AttackSpeed);
				continue;
			}
			// Somehow the healer got out of range of his target, go back to healing stuff nearby
			if((!this.IsMoving && DistanceBetweenTarget > Range) || !Target.Alive) {
				GetNewHealerTarget();
			}
			if(Alive && ((DistanceBetweenTarget < Range && Target.Alive) || Target == this)) {
				if(!IsMoving && !Frozen) {
					isAttacking = true;
					Target.Heal((int)((float)Power * Random.Range(.8f,1.1f)));
					if(AudioGroup != "") {
						SoundManager.PlaySFX(SoundManager.LoadFromGroup(AudioGroup), false, 0f, SoundManager.Instance.volumeSFX * .2f);
					}
					ParticleStream.Instance.HealFire(transform,Target.transform);
				}
			}
			yield return new WaitForSeconds(AttackSpeed);
		}
	}

	private void GetNewHealerTarget() {
		// Set to lowest HP person in range automatically, can be overridden by setting the target
		// This totally can be null!  This means the player has to move the healer in range of low HP people
		Target = RaiderPool.LowestHPRaiderInCollection(RaiderPool.RaidersInRange(this));
	}

	public override void MoveToTarget(RaycastHit target) {
		Target = target.transform.GetComponent<RPGCharacter>();
		if(Target != this)
			MoveToRange(Target.transform.position);
	}
}