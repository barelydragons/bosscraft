﻿using UnityEngine;
using System.Collections;
using Photon.SocketServer;
using ExitGames.Client.Photon;

public interface ControlInterface {
	void MoveTo(Vector3 pos);
	
	void MoveToRange(Vector3 pos);
}

public class MovementParameters {
	private bool inRange;
	public bool InRange {
		get { return inRange; }
	}
	
	private Vector3 dest;
	public Vector3 Destination {
		get { return dest; }
	}
	
	public MovementParameters(Vector3 pos) {
		inRange = false;
		dest = pos;
	}
	
	public MovementParameters(Vector3 pos, bool GoToRange) {
		inRange = GoToRange;
		dest = pos;
	}

	public static byte[] SerializeMovementParameters(object customObject) {
		MovementParameters mp = (MovementParameters)customObject;
		int i = mp.InRange ? 1 : 0;
		byte[] bytes = new byte[4 * 4];
		int index = 0;
		Protocol.Serialize(i,bytes,ref index);
		Protocol.Serialize(mp.Destination.x,bytes,ref index);
		Protocol.Serialize(mp.Destination.y,bytes,ref index);
		Protocol.Serialize(mp.Destination.z,bytes,ref index);
		return bytes;
	}

	public static object DeserializeMovementParameters(byte[] bytes) {
		Vector3 dest = Vector3.zero;
		int range;
		int index = 0;
		Protocol.Deserialize(out range, bytes, ref index);
		Protocol.Deserialize(out dest.x, bytes, ref index);
		Protocol.Deserialize(out dest.y, bytes, ref index);
		Protocol.Deserialize(out dest.z, bytes, ref index);
		bool r = range == 1 ? true : false;
		return new MovementParameters(dest,r);
	}
}
