﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomView : MonoBehaviour {
	public GameObject roomPrefab;
	public UIGrid grid;
	public Transform startPoint;
	private Dictionary<string,UILabel> roomLabels;
	void Awake () {
		PhotonNetwork.Disconnect();
		PhotonNetwork.ConnectUsingSettings("v1.0");
	}
	
	void OnReceivedRoomListUpdate() {
		CreateRoomList();
	}

	void CreateRoomList() {
		foreach(RoomInfo r in PhotonNetwork.GetRoomList()) {
			GameObject lo = NGUITools.AddChild(grid.gameObject, roomPrefab) as GameObject;
			UILabel l = lo.GetComponent<UILabel>();
			l.text = r.name;
		}
		grid.Reposition();
	}
}
