﻿using UnityEngine;
using System.Collections;

public class HelloPhoton : MonoBehaviour {
	// Use this for initialization
	void Awake () {
		PhotonNetwork.ConnectUsingSettings("v1.0");
	}

	void OnReceivedRoomListUpdate() {
		PhotonNetwork.JoinOrCreateRoom("TestRoom",new RoomOptions(),null);
	}

	void OnJoinedRoom() {
		print("Connected to Room " + PhotonNetwork.room.name);
		print (PhotonNetwork.room.playerCount);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
