﻿using UnityEngine;
using System.Collections;

// Goes on a room button
public class JoinRoom : MonoBehaviour {
	UILabel txt;

	// Use this for initialization
	void Start () {
		txt = this.GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick () {
		if(PhotonNetwork.inRoom) {
			PhotonNetwork.LeaveRoom();
		}
		foreach(RoomInfo r in PhotonNetwork.GetRoomList()) {
			if(r.name == txt.text && r.playerCount >= 2) {
				return;
			}
		}
		PhotonNetwork.JoinRoom(txt.text);
	}
}
