﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

public class RaiderData : RPGCharacterData {
	[XmlElement("RaiderType")]
	public string RaiderTypeString;

	[XmlElement("AbilityName")]
	public string AbilityName;
	
	[XmlElement("RaiderFrame")]
	public string RaiderFrame;

	public RaiderType RaiderType {
		get { return (RaiderType)RaiderType.Parse( typeof(RaiderType), this.RaiderTypeString, true); }
	}

	public static RaiderData LoadFromText(string text) {
		XmlRootAttribute xRoot = new XmlRootAttribute("RPGCharacter");
		xRoot.IsNullable = true;
		XmlSerializer serializer = new XmlSerializer(typeof(RaiderData),xRoot);
		return serializer.Deserialize(new StringReader(text)) as RaiderData;
	}
}
