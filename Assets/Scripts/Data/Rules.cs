﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("RulesCollection")]
public class RuleObject {
	
	[XmlArray("Rules"), XmlArrayItem("Rule")]
	public List<Rule> theRules = new List<Rule>();
	
	public static RuleObject LoadRules() {
		TextAsset rulesAsset = (TextAsset)Resources.Load("GameData/Rules");
		XmlSerializer serializer = new XmlSerializer(typeof(RuleObject));
		return serializer.Deserialize(new StringReader(rulesAsset.text)) as RuleObject;
	}
}

public class Rules {
	private static Rules rules;
	public static Rules Instance {
		get {
			if(rules == null)
				return new Rules();
			return rules;
		}
	}
	
	public Dictionary<string,float> RuleDict;

	public Rules() {
		RuleDict = new Dictionary<string, float>();
		RuleObject ro = RuleObject.LoadRules();
		foreach(Rule rl in ro.theRules) {
			RuleDict.Add(rl.Name,rl.Value);
		}
		rules = this;
	}
}
