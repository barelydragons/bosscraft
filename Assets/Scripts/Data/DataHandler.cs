﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class DataHandler : MonoBehaviour {
	private static DataHandler instance;
	private AbilityContainer AbilityData;
	public static Dictionary<string,AbilityAction> AbilityActions;

	public static Dictionary<string,BossData> Bosses;
	public static Dictionary<string,RaiderData> Raiders;
	public static bool PlayTutorial = false;
	public static bool basicBossAI = false;
	// Use this for initialization
	public void Awake () {
		if(instance == null) // Ensure data gets loaded in every scene and that there's only ever one data handler--so we can start the editor in ANY scene
			instance = this;
		else {
			Destroy(this.gameObject);
			return;
		}
		TextAsset abilityAsset = (TextAsset)Resources.Load("GameData/Abilities");
		AbilityData = AbilityContainer.LoadFromText(abilityAsset.text);
		AbilityActions = new Dictionary<string, AbilityAction>();
		foreach(AbilityAction action in AbilityData.AbilityActions) {
			AbilityActions.Add(action.Name,action);
		}

		TextAsset[] bossXML = Resources.LoadAll<TextAsset>("GameData/Bosses");
		Bosses = new Dictionary<string, BossData>();
		foreach(TextAsset xml in bossXML) {
			BossData bd = BossData.LoadFromText(xml.text);
			bd.Name = bd.Name.Trim ();
			bd.Description = bd.Description.Trim();
//			print (bd.Name.Length);
			Bosses.Add(bd.Name.Trim(),bd);
		}

		TextAsset[] raidXML = Resources.LoadAll<TextAsset>("GameData/Raider");
		Raiders = new Dictionary<string, RaiderData>();
		foreach(TextAsset xml in raidXML) {
			RaiderData rd = RaiderData.LoadFromText(xml.text);
			rd.Name = rd.Name.Trim ();
			rd.Description = rd.Description.Trim();
			Raiders.Add(rd.Name.Trim(),rd);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
