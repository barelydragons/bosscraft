﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("RPGCharacter")]
public abstract class RPGCharacterData {
	[XmlElement("Name")]
	public string Name;
	
	[XmlElement("Description")]
	public string Description;

	[XmlElement("Range")]
	public float Range;

	[XmlElement("MaxHP")]
	public int MaxHP;

	[XmlElement("Mana")]
	public int Mana;

	[XmlElement("AttackSpeed")]
	public float AttackSpeed; // Attacks per second

	[XmlElement("Power")]
	public int Power;

	[XmlElement("MoveSpeed")]
	public float MoveSpeed = 20f;

	[XmlElement("DamageModifier")]
	public float DamageMod = 1f;
}
