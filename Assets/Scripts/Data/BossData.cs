﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

public class BossData : RPGCharacterData {
	[XmlArray("Abilities")]
	[XmlArrayItem("AbilityName")]
	public List<string> Abilities = new List<string>();

	[XmlElement("PowerPoints")]
	public int PowerPoints;

	public bool IsRestricted(string ability) {
		return Abilities.Contains(ability);
	}
	
	public static BossData LoadFromText(string text) {
		XmlRootAttribute xRoot = new XmlRootAttribute("RPGCharacter");
		xRoot.IsNullable = true;
		XmlSerializer serializer = new XmlSerializer(typeof(BossData),xRoot);
		return serializer.Deserialize(new StringReader(text)) as BossData;
	}
}
