﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

public class Rule {
	[XmlAttribute("name")]
	public string Name;

	[XmlElement("Value")]
	public float Value;
}
