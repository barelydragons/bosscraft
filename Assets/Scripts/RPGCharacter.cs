﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(PhotonView))]
[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(BoxCollider))]
public abstract class RPGCharacter : MonoBehaviour, ControlInterface {
	private static int characterIdCount = 0;
	public static Dictionary<int,RPGCharacter> IDLookup = new Dictionary<int, RPGCharacter>();
	private int cid;
	public int CharID {
		get { return cid; }
	}
	
	public Camera charCam;
	public Transform camRoot;

	[Header("Set in Inspector")]
	public float hpBarOffset = .08f;
	public Animator _anim;
	[Tooltip("Optional")]
	public Transform damageCenter; // Usually just the center, but for large bosses, could be from mouth.
	[Tooltip("Match groups in SoundManager prefab.")]
	public string AudioGroup = "";
	[Tooltip("Set to body of raider, for debugging.")]
	public Renderer mdlRenderer;
	public List<GameObject> disableOnDeath;

	[Header("Loaded From XML")]
	public float Range;
	public int MaxHP;
	public float DamageModifier = 1f;
	public int Mana;
	public float AttackSpeed = 1f; // Attacks per second
	public int Power;
	public float MoveSpeed = 5f;
	[Space(30)]
	public PhotonView photonView;
	protected bool sendUpdates = true;
	public AudioClip deathSound;
	public float DPS {
		get { return Power / AttackSpeed; }
	}

	private int waitCount = 0;

	private string animState;
	private bool blockStateChange;

	private bool invulnerable;
	public bool Invulnerable {
		get { return invulnerable; }
		set { invulnerable = value; }
	}
	private bool frozen;
	public bool Frozen {
		get { return frozen; }
		set { frozen = value; }
	}

	public Color selectedColor;
	public Color unselectedColor;
	public LayerMask targetMask;
	public bool UseInspector = false;
	public string XMLName;
	private Vector3 dest;
	public Vector3 MovementDestination {
		get {
			if(IsMoving)
				return dest;
			else
				return Vector3.zero;
		}
	}

	// If multiple tanks....
	private List<RPGCharacter> taunters = new List<RPGCharacter>();
	public List<RPGCharacter> Taunters {
		get { return taunters; }
	}

	private float distToTarget;
	public float DistanceBetweenTarget { // Calculated every frame, used in lots of routines and sort of expensive
		get { return distToTarget; }
	}

	private int currentHP;
	public int CurrentHP {
		get { return currentHP; }
	}

	private Animator charAnimator;
	public Animator Animator {
		get { return charAnimator; }
	}

	protected bool isAttacking;
	public bool IsAttacking {
		get { return isAttacking; }
	}
	private Vector3 curPos, lasPos;
	protected bool isMoving;
	public bool IsMoving {
		get { return isMoving; }
	}

	public bool IsIdle {
		get { return !isMoving && !isAttacking && Alive; }
	}

	public bool Alive {
		get { 
			return !(currentHP <= 0); 
		}
	}

	protected RPGCharacter target;
	public RPGCharacter Target {
		get { return target; }
		set { 
			if(PhotonNetwork.inRoom) {
				if(this.photonView == null || value == null)
					return;
				this.photonView.RPC ("SetTarget",PhotonTargets.All,value.CharID);
			} else {
				if(value == null)
					return;
				SetTarget(value.CharID);
			}
		}
	}

	private List<Ability> abilities;
	public List<Ability> Abilities {
		get { 
			if(abilities == null)
				return new List<Ability>(); 
			else
				return abilities; 
		}
	}

	protected virtual void Awake() {
		cid = characterIdCount;
		characterIdCount++;
		IDLookup.Add(cid,this);
		photonView = this.GetComponent<PhotonView>();
		if(photonView == null) {
			photonView = this.gameObject.AddComponent<PhotonView>();
		}
		charCam = this.GetComponentInChildren<Camera>();
		if(charCam != null) {
			camRoot = charCam.transform.parent;
			charCam.enabled = false;
		}
		abilities = new List<Ability>();
		charAnimator = GetComponent<Animator>(); // Slight speed improvement to keep this stored
	}

	protected virtual void Start() {
		if(!UseInspector) {
			LoadFromXML();
		}
		currentHP = MaxHP;
	}

    public virtual bool isRaider() {
        return false;
    }

	public void SelectColor() {
		if(Alive && mdlRenderer != null) {
			mdlRenderer.material.color = selectedColor;
		}
	}

	public void UnselectColor() {
		if(Alive && mdlRenderer != null) {
			mdlRenderer.material.color = unselectedColor;
		}
	}

	public void MoveTo(Vector3 pos) {
		MoveMaker(new MovementParameters(pos,false));
	}

	[RPC]
	public void OnlineMove(int rangeMove, float x, float y, float z) { // These primitives are serializable automatically
		// Moves character to pos, but only until the character is in range
		Vector3 dest = new Vector3(x,y,z);
		bool inRange = rangeMove == 1 ? true : false;
		StopCoroutine("Movement");
		if(currentHP > 0) {
			StartCoroutine("Movement", new MovementParameters(dest,inRange));
		}
	}

	public void OfflineMove(MovementParameters param) {
		StopCoroutine("Movement");
		if(currentHP > 0) {
			StartCoroutine("Movement", param);
		}
	}

	private void MoveMaker(MovementParameters param) {
		if(PhotonNetwork.inRoom) {
			int range = param.InRange ? 1 : 0;
			photonView.RPC("OnlineMove",PhotonTargets.All,range,param.Destination.x,param.Destination.y,param.Destination.z);
		} else {
			OfflineMove(param);
		}
	}

	public void MoveToRange(Vector3 pos) {
		MoveMaker(new MovementParameters(pos,true));
	}

	protected void Animate(string name) {
		AnimTrigger(name);
	}

	[RPC]
	public void RPCAnimTrigger(string name) {
		if(animState != name) {
			_anim.SetTrigger(name);
			animState = name; 		
		}
	}

	[RPC]
	public void AnimState(string state, bool val) {
		_anim.SetBool(state,val);
	}

	void AnimTrigger(string name) {
		if(PhotonNetwork.inRoom) {
			this.photonView.RPC("RPCAnimTrigger",PhotonTargets.All,name);
		} else if(_anim != null) {
			RPCAnimTrigger(name);
		}
	}

	void ChangeAnimState(string state, bool val) {
		if(_anim == null || _anim.GetBool(state) == val) {
			return;
		}
		if(PhotonNetwork.inRoom && !(this != BossPool.MainBoss && this.GetType() == typeof(BossMinion))) { // don't sync anything other than main boss
			this.photonView.RPC("AnimState",PhotonTargets.All,state,val);
		} else if(_anim != null) {
			AnimState(state,val);
		}
	}
	protected void TriggerIdle() {
		ChangeAnimState("Attack",false);
		ChangeAnimState("Ultimate",false);
		ChangeAnimState("Death",false);
		ChangeAnimState("Walk",false);
		ChangeAnimState("Idle",true);
	}

	protected void TriggerAttack() {
		ChangeAnimState("Attack",true);
		ChangeAnimState("Ultimate",false);
		ChangeAnimState("Death",false);
		ChangeAnimState("Walk",false);
		ChangeAnimState("Idle",false);
	}
	
	protected void TriggerWalk() {
		ChangeAnimState("Attack",false);
		ChangeAnimState("Ultimate",false);
		ChangeAnimState("Death",false);
		ChangeAnimState("Walk",true);
		ChangeAnimState("Idle",false);
	}
	
	protected void TriggerUltimate(float timeToBlock) {
		ChangeAnimState("Attack",false);
		ChangeAnimState("Ultimate",true);
		ChangeAnimState("Death",false);
		ChangeAnimState("Walk",false);
		ChangeAnimState("Idle",false);
		StartCoroutine(DontChangeState(timeToBlock));
	}
	
	protected void TriggerDeath() {
		SoundManager.PlaySFX(deathSound);
		ChangeAnimState("Attack",false);
		ChangeAnimState("Ultimate",false);
		ChangeAnimState("Death",true);
		ChangeAnimState("Walk",false);
		ChangeAnimState("Idle",false);
	}

	protected IEnumerator Movement(MovementParameters moveOpts) {
		dest = moveOpts.Destination;
		dest.y = this.transform.position.y;
		if(!this.rigidbody.isKinematic) {
			this.rigidbody.velocity = Vector3.zero; // Reset the speed...
		}
		while(Vector3.Distance(damageCenter.position,dest) >= .03f) {
			// Are we moving until we're in range of the target?
			if(distToTarget < this.Range && moveOpts.InRange) {
				break;
			}
			if(Frozen) {
				yield return 0;
				continue;
			}
			Vector3 v = Vector3.MoveTowards(transform.position,dest,(Time.deltaTime * MoveSpeed));

			rigidbody.MovePosition(v);
			yield return new WaitForFixedUpdate();
		}
	}

	protected float DistToTarget() {
		if(Target == this) {
			return 0f;
		} else if(Target == null) {
			return -1.0f;
		} else {
			return Vector3.Distance(Target.collider.ClosestPointOnBounds(damageCenter.position),damageCenter.position);
		}
	}

	[RPC]
	public void HandleTaunt(int id) {
		RPGCharacter c = IDLookup[id];
		if(!taunters.Contains(c)) {
			taunters.Add(c);
		}
		Target = c;
	}

	public void Taunt(RPGCharacter taunter) {
		if(PhotonNetwork.inRoom) {
			PhotonNetwork.RPC(this.photonView,"HandleTaunt",PhotonTargets.All,taunter.CharID);
		} else {
			HandleTaunt(taunter.CharID);
		}
	}

	[RPC]
	public void HandleAggroLoss(int id) {
		RPGCharacter taunter = IDLookup[id];
		if(taunters.Contains(taunter)) {
			taunters.Remove(taunter);
		}
	}

	[RPC]
	public void LoseAggro(RPGCharacter taunter) {
		if(PhotonNetwork.inRoom) {
			PhotonNetwork.RPC(this.photonView,"HandleAggroLoss",PhotonTargets.All,taunter.CharID);
		} else {
			HandleAggroLoss(taunter.CharID);
		}
	}

	[RPC]
	public void Damage(int damage) {
		if(currentHP <= 0 || invulnerable)
			return; // Already dead
		int realDamage = (int)((float)damage * (damage < 0 ? 1 : this.DamageModifier)); // if it's a heal... don't modify the value
		currentHP -= realDamage;
		if(FloatingCombatText.Instance != null) {
			if(damage < 0) {
				FloatingCombatText.Instance.NewHeal(this,-realDamage);
			} else {
				FloatingCombatText.Instance.NewDamage(this,realDamage);
			}
		}
		if(currentHP <= 0)
			Death();
		if(currentHP > MaxHP)
			currentHP = MaxHP;
	}

	public void AddDamage(int damage) {
		if(PhotonNetwork.inRoom) {
			if(this.sendUpdates) {
				photonView.RPC("Damage",PhotonTargets.All,damage);
			}
		} else {
			Damage (damage);
		}
	}

	[RPC]
	public void SetTarget(int charid) {
		if(IDLookup.ContainsKey(charid)) {
			this.target = IDLookup[charid];
		}
	}

	[RPC]
	public void SetMoving(bool movement) {
		this.isMoving = movement;
	}

	[RPC]
	public void SetAttacking(bool attacking) {
		this.isAttacking = attacking;
	}

	[RPC]
	public void DeathAndDestroy() {
		this.Death();
		Destroy (this.gameObject);
	}

	public virtual void Death() {
		StopCoroutine("Movement");
		HPBarHandler.Instance.UnregisterHPBar(this);
		foreach(GameObject go in this.disableOnDeath) {
			go.SetActive(false);
		}
		TriggerDeath();
		currentHP = 0;
	}

	// Healing is negative damage
	public void Heal(int healPoints) {
		AddDamage(-healPoints);
	}

	protected virtual void Update() {
		curPos = transform.position;

		isMoving = lasPos != curPos;
		if(!Alive || blockStateChange) {
		} else if(IsIdle) {
			TriggerIdle();
		} else if(isMoving) {
			TriggerWalk();
		} else if(isAttacking) {
			TriggerAttack();
		}
		this.distToTarget = DistToTarget();
		lasPos = curPos;
	}

	public IEnumerator DontChangeState(float time) {
		_anim.SetBool("WaitOnAbility",true);
		blockStateChange = true;
		waitCount++;
		yield return new WaitForSeconds(time);
		waitCount--;
		if(waitCount == 0) {
			blockStateChange = false;
			_anim.SetBool("WaitOnAbility",false);
		}
	}
	
	// What the character does when in range of his target
	protected abstract IEnumerator TargetRoutine();

	protected abstract void LoadFromXML();
	protected void LoadFromXML(RPGCharacterData data) {
		MaxHP = data.MaxHP;
		Mana = data.Mana;
		Range = data.Range;
		AttackSpeed = data.AttackSpeed;
		Power = data.Power;
		MoveSpeed = data.MoveSpeed;
		DamageModifier = data.DamageMod;
	}
}