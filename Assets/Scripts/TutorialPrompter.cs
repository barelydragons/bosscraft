﻿using UnityEngine;
using System.Collections;

public class TutorialPrompter : MonoBehaviour {
	class TutorialMessage {
		private string prompt;
		public string Prompt {
			get { return prompt; }
			set { prompt = value; }
		}
		private float length;
		public float TimeLength {
			get { return length; }
		}
		
		public TutorialMessage(string aPrompt) {
			prompt = aPrompt;
			length = aPrompt.Length * .04f;
		}
		
		public void UpdatePrompt(string newPrompt) {
			prompt = newPrompt;
			length = newPrompt.Length * .04f;
		}
	}
	public UILabel tutorialPrompt;
	public Game game;

	// Use this for initialization
	void Start () {
		if(DataHandler.PlayTutorial == true) {
			StartCoroutine(WaitToBegin());
		} else {
			Destroy(this.gameObject);
		}
	}

    IEnumerator WaitToBegin() {
        while (BossPool.MainBoss == null)
            yield return 0;
		StartCoroutine(TutorialRoutine());
    }

	void FreezeGame () {
		BossPool.MainBoss.Frozen = true;
		RaiderPool.FreezeAll();
		if(RaidController.Instance != null) {
			RaidController.Instance.blockInput = true;
		}
		if(BossController.Instance != null) {
			BossController.Instance.blockInput = true;
		}
	}

	void UnfreezeGame () {
		BossPool.MainBoss.Frozen = false;
		RaiderPool.UnFreezeAll();
		if(RaidController.Instance != null) {
			RaidController.Instance.blockInput = false;
		}
		if(BossController.Instance != null) {
			BossController.Instance.blockInput = false;
		}
	}

	IEnumerator TutorialRoutine() {
//		game.PauseOn();
		FreezeGame();
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.Invulnerable = true;
		}
		while(Game.Instance.loading)
			yield return 0;
		TutorialMessage startPrompt = new TutorialMessage(
			"Welcome to Bosscraft. A competitive boss battling game."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		game.tutorialWait = false;
//		game.PauseOff();
		FreezeGame();
		startPrompt.UpdatePrompt(
			"Players must battle each other to the death. One controls a team of 5 toys, and the other a fearsome boss."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"If the health of the boss is zero, the toys win.  If all the toys die, the boss wins."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"Toys can move freely and have abilities to aid them. But they are quite fragile."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"The boss is very powerful, with a huge health pool and a large variety of abilities. But the boss cannot move freely."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"The toys can force the boss to attack a specific toy--the superhero. This is called aggro. The boss must attack the toy with aggro."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"Select the superhero by left clicking or dragging a box around her with the mouse."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		RaidController.Instance.blockInput = false;
		RaiderPool.UnFreezeAll();
		RaidGroup rg = RaidController.Instance.ActiveGroup;
		while(true) {
			if(rg.raiders.Count > 0) {
				bool finish = false;
				foreach(RPGCharacter r in rg.raiders) {
					if(((Raider)r).RaiderType == RaiderType.Tank) {
						finish = true;
						break;
					}
				}
				if(finish)
					break;
			}
			yield return 0;
		}
		startPrompt.UpdatePrompt(
			"She can move around by clicking on the arena with the right mouse button, try moving her and the other toys around."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 4f);
		FreezeGame();
		startPrompt.UpdatePrompt(
			"By default, the boss attacks the nearest player.  If the superhero is in close range though, she forces the boss to attack her."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"See who the boss makes a move for!"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);
		UnfreezeGame();
		yield return new WaitForSeconds(3f);
		FreezeGame();
		startPrompt.UpdatePrompt(
			"The boss player must use his abilities to force the toys to move to his advantage."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 1.5f);
		startPrompt.UpdatePrompt(
			"The four icons on the bottom are the bosses abilities.  They are selected with 1/2/3/4"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);
		startPrompt.UpdatePrompt(
			"Each ability has 3 modes, a weak, normal, and strong mode. The outline of the ability shows its charge."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"Hold control and shift to select weak and strong mode respectively. First select an ability then hit it again to activate it."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"The weak mode is activated by holding down Control. The strong by holding down shift."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);
		startPrompt.UpdatePrompt(
			"Try activating some abilities now.  Use the mouse to move the targetting reticle for Area of Effect abilities"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		UnfreezeGame();
		startPrompt.UpdatePrompt(
			"Some moves don't need to be targetted.  Others affect only whoever the boss is targetting."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(15f);
		FreezeGame();
		startPrompt.UpdatePrompt(
			"Some abilities spawn minions of the boss to help them destroy the toys."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"Abilities cooldown after using them, strong versions have longer cooldowns and weak versions are shorter."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"Experiment with abilities and cooldowns to find the strategy that works best."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"Each toy has a special role, there's the tank who forces the boss to attack her."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"The healer who doesn't attack the boss, but heals the wounds of his allies."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"The robot can only attack from close range, but can fire lasers from a distance."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"A wizard who slings spells at the boss and whose special ability buffs the attack/heal speed of the other toys."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"With a toy or toys selected, right click to command them to attack the boss or heal a fellow toy."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"Keeping the toys alive is important, move toys out of the way of boss abilities and use the healer to heal any damage."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"If the boss summons minions, be sure to take them out."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"Toys also have special abilities that can be activated with the space bar, try using them now."
			);
		StartCoroutine("Play",startPrompt);
		UnfreezeGame();
		yield return new WaitForSeconds(startPrompt.TimeLength + 2f);
		startPrompt.UpdatePrompt(
			"There are all sorts of different strategies to try. Mix and match your team and find out how to showdown!"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 6f);
		startPrompt.UpdatePrompt(
			"And that's how to play, damage is now enabled; who will win? YOU ARE NOT PREPARED!"
			);
		StartCoroutine("Play",startPrompt);
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.Invulnerable = false;
		}
		Destroy (this.gameObject);
	}
	
	IEnumerator Play(TutorialMessage aMessage) {
		int count = aMessage.Prompt.Length;
		float timeLimit = aMessage.TimeLength;
		float time = 0.0f;
		while(time <= timeLimit) {
			int numofchars = (int)((float)count * (time/timeLimit));
			time += Time.deltaTime;
			tutorialPrompt.text = aMessage.Prompt.Substring(0,numofchars);
			yield return 0;
		}
		tutorialPrompt.text = aMessage.Prompt;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
