﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {
	public string levelName;

	void OnPress () {
		if(Game.Instance != null) {
			Game.Instance.TriggerEndPrematurely();
		}
        Application.LoadLevel(levelName);	
	}
}
