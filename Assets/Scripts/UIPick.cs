﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIPick : MonoBehaviour {
    public static UIPick Instance;
    public bool snow, monkey, army, heli, wiz, rob, super, lego;
    public bool jack, bear;
    public int raidersSelected = 0;
    public bool bossSelected;
	// Use this for initialization
	void Awake () {
        if (Instance != null) {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
        Game.OnBattleEnd += GameEnd;
	}

    void GameEnd(GameStateArgs args) {
        raidersSelected = 0;
        snow = false;
        monkey = false;
        army = false;
        heli = false;
        wiz = false;
        rob = false;
        super = false;
        lego = false;
        jack = false;
        bear = false;
        bossSelected = false;
    }

    public void Tutorial() {
        snow = false;
        monkey = false;
        army = true;
        heli = false;
        wiz = true;
        rob = true;
        super = true;
        lego = true;
        jack = false;
        bear = true;
    }

    public void Set(string key, bool status) {
        if (key == "Snow") {
            snow = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Monkey") {
            monkey = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Army") {
            army = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Helicopter") {
            heli = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Wizard") {
            wiz = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Robot") {
            rob = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Superhero") {
            super = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Lego") {
            lego = status;
            if (status) {
                raidersSelected++;
            } else {
                raidersSelected--;
            }
        }
        if (key == "Jack") {
            jack = status;
        }
        if (key == "Bear") {
            bear = status;
        }
        bossSelected = bear || jack;
    }
}
