﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioBank : MonoBehaviour {
	public List<AudioClip> clips;
	private static Dictionary<string,AudioClip> audioDict;
	public static AudioBank Instance;
	// Use this for initialization
	void Start () {
        audioDict = new Dictionary<string, AudioClip>();
		foreach(AudioClip c in clips) {
			audioDict.Add(c.name,c);
		}
	}

	public static AudioClip Fetch(string name) {
		return AudioBank.audioDict[name];
	}
}
