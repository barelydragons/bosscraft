﻿using UnityEngine;
using System.Collections;

public class WalkTrail : MonoBehaviour {
    public RPGCharacter toTrack;
    private Component[] psyses;
	// Use this for initialization
	void Start () {
        psyses = this.GetComponentsInChildren(typeof(ParticleSystem));
	}

    void Set(bool opt) {
        foreach (ParticleSystem p in psyses) {
            p.enableEmission = opt;
        }
    }

	// Update is called once per frame
	void Update () {
        Set(toTrack.IsMoving);
	}
}
