﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleStream : MonoBehaviour {
	protected class FireArgs {
		public Transform p1, p2;    
		public GameObject o, explosionType;
		public bool explode;
        public Vector3 offset;
		public FireArgs(Transform p1_, Transform p2_, GameObject o_) {
			p1 = p1_;
			p2 = p2_;
			o = o_;
			explode = false;
		}
		public FireArgs(Transform p1_, Transform p2_, GameObject o_, bool expl, GameObject explosion) {
			p1 = p1_;
			p2 = p2_;
			o = o_;
            explosionType = explosion;
			explode = expl;
            offset = Random.insideUnitSphere * 5;
		}
	}
	private static ParticleStream inst;
	public static ParticleStream Instance {
		get { return inst; }
	}
	public List<GameObject> healingShot;
	public List<GameObject> rocket;
	public List<GameObject> fireAttacks, snowAttakcs;
    public GameObject healthPack;
	public GameObject beanieBaby;
	public GameObject fireExplosion, snowExplosion;
	public float shotSpeed = 20f; // m/s

	// Use this for initialization
	void Start () {
		inst = this;
	}

	private GameObject RandFromCollection(List<GameObject> collection) {
		return (GameObject)collection[Random.Range(0,collection.Count)];
	}

	private void Fire(Transform p1, Transform p2, GameObject inst) {
		StartCoroutine("FireRoutine",new FireArgs(p1,p2,(GameObject)Instantiate(inst,p1.position,Quaternion.identity)));
	}
	
	private void Fire(Transform p1, Transform p2, GameObject inst, bool explode, GameObject explosionType) {
        StartCoroutine("FireRoutine", new FireArgs(p1, p2, (GameObject)Instantiate(inst, p1.position, Quaternion.identity), explode, explosionType));
	}

	private IEnumerator FireRoutine(FireArgs args) {
		float t = 1f;
		while( t > .05f) {
			if(args.p2 == null) // If target is destroyed on the particles journey there
				break;
            Vector3 tar = args.p2.position + args.offset;
            t = Vector3.Distance(args.o.transform.position, tar);
			args.o.transform.position = Vector3.MoveTowards(args.o.transform.position,args.p2.position + args.offset,Time.deltaTime * shotSpeed);
            args.o.transform.LookAt(tar);
			yield return 0;
		}
		if(args.explode) {
			Destroy (Instantiate(args.explosionType,args.o.transform.position,Quaternion.identity),5f);
		}
		Destroy (args.o);
	}

	public void HealFire(Transform pnt1, Transform pnt2) {
		GameObject rand = RandFromCollection(healingShot);
		Fire (pnt1,pnt2,rand);
	}
	
	public void RocketFire(Transform pnt1, Transform pnt2) {
		GameObject rand = RandFromCollection(rocket);
		Fire (pnt1,pnt2,rand,true,fireExplosion);
	}
	
	public void FireAttack(Transform pnt1, Transform pnt2) {
		GameObject rand = RandFromCollection(fireAttacks);
		Fire (pnt1,pnt2,rand,true,fireExplosion);
	}

    public void SnowAttack(Transform pnt1, Transform pnt2) {
        GameObject rand = RandFromCollection(snowAttakcs);
        Fire(pnt1, pnt2, rand, true, snowExplosion);
    }
	
	public void BeanieBabyAirDrop(Transform pnt1, Transform pnt2) {
		Fire (pnt1,pnt2,beanieBaby,true, fireExplosion);
	}

    public void HeliHeal(Transform pnt1, Transform pnt2) {
        Fire(pnt1, pnt2, healthPack);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
