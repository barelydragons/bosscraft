﻿using UnityEngine;
using System.Collections;

public class GameInitializer : MonoBehaviour {
	public GameObject raidController, bossController;
	public PhotonView pview;
	public RoomHandler Instance;
	// Use this for initialization
	void Start () {
		RoomHandler gri = RoomHandler.Instance;
		if(PhotonNetwork.inRoom) {
			print (PhotonNetwork.player.ID);
			if(gri.raidPlayer.ID == PhotonNetwork.player.ID) {
				PhotonNetwork.Instantiate("MainCamRaidController",Vector3.zero,Quaternion.identity,0);
				print ("RC Instantiated");
			} else {
				PhotonNetwork.Instantiate("BossController",Vector3.zero,Quaternion.identity,0);
				print ("BC Instantiated");
			}
		} else {
			Create();
		}
	}

	void Create() {
		Instantiate(raidController);
		Instantiate(bossController);
	}
	
	// Update is called once per frame
	void Update () {
	}
}
