﻿using UnityEngine;
using System.Collections;

public enum GameState {
	Setup,
	Paused,
	Battle,
	Postgame,
	Invalid
}


public class GameStateArgs {
	private GameState _leavingState;
	public GameState LeavingState {
		get { return _leavingState; }
	}
	private GameState _nextState;
	public GameState NextState {
		get { return _nextState; }
	}
	private bool _raidAlive;
	public bool RaidAlive {
		get { return _raidAlive; }
	}
	private bool _bossAlive;
	public bool BossAlive {
		get { return _bossAlive; }
	}

	public GameStateArgs(GameState leavingState, GameState nextState, bool raidStatus, bool bossStatus) {
		_leavingState = leavingState;
		_nextState = nextState;
		_raidAlive = raidStatus;
		_bossAlive = bossStatus;
	}
}

public class Game : MonoBehaviour {
	public static bool InSetup = true;
	public static Game Instance;
	public BossMinion mainBoss;
	public RaidController raidControl;
	public GameObject gameOver;
	public UILabel endGameText;
	public bool paused, loading;
	public bool tutorialWait;
	public AudioClip raidVictory, bossVictory;
	public delegate void GameStateEventHandler(GameStateArgs args);
	public static event GameStateEventHandler OnSetupBegin;
	public static event GameStateEventHandler OnSetupEnd;
	public static event GameStateEventHandler OnBattleBegin;
	public static event GameStateEventHandler OnBattleEnd;
	public static event GameStateEventHandler OnPostGameBegin;
	public static event GameStateEventHandler OnPostGameEnd;
	public static event GameStateEventHandler OnPause;
	public static event GameStateEventHandler OnUnpause;
	public static GameState CurrentState = GameState.Invalid;
	public GameObject explosion;
	public UILabel vsLabel;
	public GameObject bossLabel, raidLabel;
	public bool bossReady, raidReady;
	// Use this for initialization
	void Start () {
		Instance = this;
		OnSetupBegin += StateChange;
		OnSetupEnd += StateChange;
		OnBattleBegin += StateChange;
		OnBattleEnd += StateChange;
		OnBattleEnd += EndGame;
		OnPostGameBegin += StateChange;
		OnPostGameEnd += StateChange;
		OnPause += StateChange;
		OnUnpause += StateChange;
		Time.timeScale = 1f;
		paused = false;
		gameOver.SetActive(false);
		StartCoroutine("GameRoutine");

		vsLabel.alpha = 0;
	}

	void EndGame(GameStateArgs end) {

		OnSetupBegin -= StateChange;
		OnSetupEnd -= StateChange;
		OnBattleBegin -= StateChange;
		OnBattleEnd -= StateChange;
		OnBattleEnd -= EndGame;
		OnPostGameBegin -= StateChange;
		OnPause -= StateChange;
		OnUnpause -= StateChange;
	}

	// Superfluous really, but seems nice to have
	void StateChange(GameStateArgs args) {
		CurrentState = args.NextState;
	}

	// Really shouldn't be handled by game.
	// TODO: Refactor into an input manager class and serve an input event
	void Pause() {
		if(Input.GetButtonDown("Pause")) {
			if (paused) {
				PauseOff();
			} else {
				PauseOn();
			}
		}
	}

	public void PauseOff() {
		Time.timeScale = 1f;
		paused = false;
		gameOver.SetActive(false);
		OnUnpause(CreateGSArgs(GameState.Battle));
	}

	public void PauseOn() {
		if(!PhotonNetwork.inRoom)
			Time.timeScale = 0f;
		paused = true;
		gameOver.SetActive(true);
		OnPause(CreateGSArgs(GameState.Paused));
	}

	GameStateArgs CreateGSArgs(GameState nextState) {
		if(RaidController.Instance == null) {
			return new GameStateArgs(Game.CurrentState,nextState,true,true);
		}
		return new GameStateArgs(Game.CurrentState,nextState,!RaidController.Instance.RaidDead,mainBoss.Alive);
	}

	IEnumerator VSRoutine() {
		bossLabel.transform.position = new Vector3(-Screen.width,0);
		raidLabel.transform.position = new Vector3(Screen.width,0);
		vsLabel.alpha = 0f;

		GoEaseType ease = GoEaseType.QuintInOut;
		bossLabel.transform.localPositionTo(1.5f,new Vector3(-280f,0)).easeType = ease;
		raidLabel.transform.localPositionTo(1.5f,new Vector3(280f,0)).easeType = ease;
		Go.to (vsLabel,.8f, new GoTweenConfig().floatProp("alpha",1f));
		yield return new WaitForSeconds(4f);
		bossLabel.transform.localPositionTo(1.5f,new Vector3(-Screen.width,0)).easeType = ease;
		raidLabel.transform.localPositionTo(1.5f,new Vector3(Screen.width,0)).easeType = ease;
		Go.to (vsLabel,.8f, new GoTweenConfig().floatProp("alpha",0f));
		yield return new WaitForSeconds(1.5f);
	}
	
	IEnumerator WinRoutine() {
		bossLabel.transform.position = new Vector3(-Screen.width,0);
		raidLabel.transform.position = new Vector3(Screen.width,0);
		vsLabel.alpha = 0f;
		BossMinion mb = BossPool.MainBoss;
		
		GoEaseType ease = GoEaseType.QuintInOut;
		bossLabel.transform.localPositionTo(1.5f,new Vector3(-280f,0)).easeType = ease;
		raidLabel.transform.localPositionTo(1.5f,new Vector3(280f,0)).easeType = ease;
		Go.to (vsLabel,.8f, new GoTweenConfig().floatProp("alpha",1f));
		yield return new WaitForSeconds(1.5f);
		bool bossWon = mainBoss.Alive;
		if(bossWon) {
			SoundManager.PlaySFX(this.gameObject,bossVictory);
		} else {
			SoundManager.PlaySFX(this.gameObject,raidVictory);
		}
		mb.charCam.enabled = true;
		if(bossWon) {
			// play boss win animation
		} else {
			// Start boss explode routine
//			mb.charCam.enabled = true;
		}
		GameObject loser = !mainBoss.Alive ? bossLabel : raidLabel;
		GameObject winner = !mainBoss.Alive ? raidLabel : bossLabel;
		StartCoroutine(ExplodeAndShake(loser));
		yield return new WaitForSeconds(2f);
		int extraSplosionCount = 8;
		for(int i = 0; i < extraSplosionCount; i++) {
			GameObject particle = (GameObject)NGUITools.AddChild(vsLabel.gameObject,explosion);
			Vector2 shake = Random.insideUnitCircle * 200f;
			particle.transform.localPosition += new Vector3(shake.x,shake.y);
			particle.transform.localPosition += new Vector3(Random.Range(-loser.collider.bounds.extents.x,loser.collider.bounds.extents.x),
			                                                Random.Range(-loser.collider.bounds.extents.y,loser.collider.bounds.extents.y));
			
//			yield return new WaitForSeconds(.05f);
			Destroy (particle,3f);
		}
		vsLabel.text = "Winner";
		vsLabel.transform.localPositionTo(.1f,new Vector3(0,-150f,0),true);
		winner.transform.localPositionTo(.2f,new Vector3(0,100)).easeType = ease;
		yield return new WaitForSeconds(1f);
		yield return new WaitForSeconds(1.5f);
	}

	IEnumerator ExplodeAndShake(GameObject loser) {
		float shakeTime = 2f;
		float t = 0;
		float shakeStep = .1f;
		float shakeFactor = 125f;
		Vector3 startP = loser.transform.localPosition;
		while (t < shakeTime) {
			yield return new WaitForSeconds(shakeStep);
			t += shakeStep;
			Vector2 shake = Random.insideUnitCircle * shakeFactor * t / shakeTime;
			loser.transform.localPositionTo(shakeStep,new Vector3(startP.x + shake.x,startP.y + shake.y,startP.z)).easeType = GoEaseType.BounceInOut;
			GameObject particle = (GameObject)NGUITools.AddChild(loser,explosion);
			particle.transform.localPosition += new Vector3(shake.x,shake.y);
			particle.transform.localPosition += new Vector3(Random.Range(-loser.collider.bounds.extents.x,loser.collider.bounds.extents.x),
			                                                Random.Range(-loser.collider.bounds.extents.y,loser.collider.bounds.extents.y));
			Destroy (particle,3f);
		}
		int extraSplosionCount = 8;
		TweenRotation tr = loser.GetComponent<TweenRotation>();
		TweenAlpha ta = loser.GetComponent<TweenAlpha>();
		tr.enabled = true;
		tr.PlayForward();
		ta.enabled = true;
		ta.PlayForward();
		for(int i = 0; i < extraSplosionCount; i++) {
			GameObject particle = (GameObject)NGUITools.AddChild(loser,explosion);
			Vector2 shake = Random.insideUnitCircle * shakeFactor;
			particle.transform.localPosition += new Vector3(shake.x,shake.y);
			particle.transform.localPosition += new Vector3(Random.Range(-loser.collider.bounds.extents.x,loser.collider.bounds.extents.x),
			                                                Random.Range(-loser.collider.bounds.extents.y,loser.collider.bounds.extents.y));

			yield return new WaitForSeconds(.05f);
			Destroy (particle,3f);
		}
		Destroy(loser.gameObject,.2f);
	}

	// Update is called once per frame
	IEnumerator GameRoutine () {
        loading = true;
        while (BossPool.MainBoss == null) {
            yield return 0;
        }
        mainBoss = BossPool.MainBoss;
		yield return new WaitForSeconds(5f);
		while(RaidController.Instance == null) {
			yield return 0;
		}
		loading = false;
		LoadScreen.Instance.Stop();
		RaidController.Instance.animation.Play();

		while(bossReady == false || raidReady == false) {
			yield return 0;
		}
		vsLabel.text = "VS";
		StartCoroutine(VSRoutine());
		yield return new WaitForSeconds(5f); // For camera to fly in and other stuff
		if(OnSetupBegin != null) {
			OnSetupBegin(new GameStateArgs(GameState.Invalid,GameState.Setup,true,true));
		}
		while(tutorialWait)
			yield return 0;
		float t = 0f;
		while(t <= Rules.Instance.RuleDict["PrepPhaseTime"]) {
			t += Time.deltaTime;
			yield return 0;
		}
		if(OnSetupEnd != null) {
			OnSetupEnd(CreateGSArgs(GameState.Battle));
		}
		InSetup = false;
		if(OnBattleBegin != null) {
			OnBattleBegin(CreateGSArgs(GameState.Battle));
		}
		while(true) {
			Pause();
			if (!mainBoss.Alive || RaidController.Instance.RaidDead) {
				RaiderPool.Reset();
				break;
//				if(!mainBoss.Alive) {
//					endGameText.text = "The raiders have vanquished the dragon!";
//				} else {
//					endGameText.text = "The raid was decimated by the dragon!";
//				}
//				Time.timeScale = 0f;
			}
			yield return 0;
		}
		if(OnBattleEnd != null) {
			OnBattleEnd(CreateGSArgs(GameState.Postgame));
		}
		yield return StartCoroutine(WinRoutine());
		gameOver.SetActive(true);
	}

	public void TriggerEndPrematurely() {
		StopCoroutine("GameRoutine");
		RaiderPool.Reset();
		if(OnBattleEnd != null) {
			OnBattleEnd(CreateGSArgs(GameState.Postgame));
		}
		gameOver.SetActive(true);
	}
}
