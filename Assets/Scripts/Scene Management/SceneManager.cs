﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {
    public GameObject raiderSelectPrefab;
    public UIPanel mainMenuRoot;
	public static SceneManager Instance;
	// Use this for initialization
	void Awake () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadQueued() {
        LoadBattle();
    }

	public void LoadLevel(string aLevel) {
		Application.LoadLevel(aLevel);
	}

	public void LoadFromPopup() {
		string popupStr = UIPopupList.current.value;
		if(popupStr == "Competitive") {
			LoadBattle();
		} else if(popupStr == "Boss AI") {
			LoadAIBattle();
		}
	}

	public void LoadTutorial() {
		Time.timeScale = 1.0f;
		DataHandler.PlayTutorial = true;
		DataHandler.basicBossAI = false;
        UIPick p = UIPick.Instance;
        p.Tutorial();
		LoadBattle();
	}

	public void LoadBalance() {
		Time.timeScale = 1.0f;
		LoadLevel("optScene");
	}

	public void LoadAIBattle() {
		DataHandler.PlayTutorial = false;
		DataHandler.basicBossAI = true;
        //Instantiate(raiderSelectPrefab);
        NGUITools.AddChild(mainMenuRoot.gameObject,raiderSelectPrefab);
	}

	public void LoadBattle() {
		Time.timeScale = 1.0f;
		LoadLevel("battle");
	}
	
	public void LoadMenu() {
		Time.timeScale = 1.0f;
		LoadLevel("mainMenu");
	}
}
