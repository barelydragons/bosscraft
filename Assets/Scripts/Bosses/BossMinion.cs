﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PriorityType {
	Tank,
	Healer,
	Nearest,
	Damage
}

public class BossMinion : RPGCharacter {
	private PriorityType priority;
	public PriorityType Priority {
		get { return priority; }
		set { priority = value; }
	}
    public AudioClip attackAudio;
	[Header("Boss Specific Items")]
	public bool IsMainBoss;
	public BossData data;
	public UISlider bossHealth;
	public UISprite behaviorRose;
	public GameObject bossExplosion;
	private Vector3 initialRot;
	private List<RPGCharacter> allRaiders;
	private bool rightDown, leftDown, upDown, botDown;
	private BossController bc;
	private BossController bController {
		get {
			if(bc == null) {
				bc = GameObject.FindObjectOfType(typeof(BossController)) as BossController;
			}
			return bc;
		}
	}
	public GameObject bossModel;
	
	protected override void Awake() {
		base.Awake();
        if (IsMainBoss) {
            bossHealth = GameObject.FindGameObjectWithTag("bossHPBar").GetComponent<UISlider>();
			BossPool.MainBoss = this;
        } else {
            BossPool.AddMinion(this);
        }
		if(PhotonNetwork.inRoom && !RoomHandler.Instance.IsBossPlayer()) {
			this.sendUpdates = false;
		}
		Game.OnBattleBegin += Begin;
		Game.OnBattleEnd += GameEnd;
		allRaiders = new List<RPGCharacter>();
		foreach(GameObject o in GameObject.FindGameObjectsWithTag("raider")) {
			allRaiders.Add(o.GetComponent<Raider>());
		}
		priority = PriorityType.Nearest;
		initialRot.x = transform.rotation.x;
		initialRot.z = transform.rotation.z;
		if(!IsMainBoss)
			Begin (null);
	}

	void GameEnd(GameStateArgs args) {
		this.Frozen = true;
		Game.OnBattleBegin -= Begin;
		Game.OnBattleEnd -= GameEnd;
	}

	void Begin(GameStateArgs args) {
		StartCoroutine("DamageRoutine");
		StartCoroutine("TargetRoutine");
		StartCoroutine("LookAtRoutine");
		StartCoroutine("MoveToRoutine");
	}

	protected override void Start() {
		base.Start();
		bc = GameObject.FindObjectOfType(typeof(BossController)) as BossController;
		if(!IsMainBoss) {
			HPBarHandler.Instance.CreateHPBar(this);
		}
		if(AudioGroup != "") {
			StartCoroutine("AudioRoutine");
		}
	}

	protected override IEnumerator TargetRoutine () {
		float priorityTimer = 0f;
		while(true) {
			if(Target == null || !Target.Alive) {
				PickNewTarget();
				priorityTimer = 0f;
			} else if(priority == PriorityType.Nearest && priorityTimer > 1.5f) { // Don't chase a target if it runs really far away, so check who the closest is while in near mode every once in awhile
				PickNewTarget();
				priorityTimer = 0f;
			} else if(priorityTimer > 1.5f) {
				priorityTimer = 0f;
			}
			priorityTimer += Time.deltaTime;
			yield return 0;
		}
	}

	Raider ClosestOfType(RaiderType rType) {
		Raider closest = null;
		float closestDist = -1f;
		foreach(Raider r in allRaiders) {
			if(!r.Alive) continue;
			if(r.RaiderType == rType) {
				float dist = Vector3.Distance(transform.position,r.transform.position);
				if(dist < closestDist || closestDist < 0f) {
					closest = r;
					closestDist = dist;
				}
			}
		}

		return closest;
	}

	Raider ClosestRaider() {
		Raider closest = null;
		float closestDist = -1f;
		foreach(Raider r in allRaiders) {
			if(r == null)
				continue;
			if(!r.Alive) continue;
			float dist = Vector3.Distance(transform.position,r.transform.position);
			if(dist < closestDist || closestDist < 0f) {
				closest = r;
				closestDist = dist;
			}
		}
		
		return closest;
	}

	public void PickNewTarget(PriorityType aType) {
		priority = aType;
		PickNewTarget();
	}

	public void PickNewTarget() {
		if(Taunters.Count > 0) {
			Target = Taunters[0];
			return;
		} else {
			if(bController != null) {
				Target = bController.target;
			}
		}
	}

	void Explode() {
		Instantiate(bossExplosion,transform.position,Quaternion.identity);
		this.bossModel.SetActive(false);
	}

	public override void Death() {
		base.Death();
		Invoke ("Explode",5f);
        if (!IsMainBoss) {
            BossPool.RemoveMinion(this);
            HPBarHandler.Instance.UnregisterHPBar(this);
            DestroyImmediate(this.gameObject);
		}
	}

	IEnumerator MoveToRoutine () {
		while(true) {
			if(Target != null && Target.Alive && DistanceBetweenTarget >= Range && !Frozen) {
				MoveToRange(Target.transform.position);
			} else {

			}
			yield return 0;
		}
	}
	
	IEnumerator LookAtRoutine() {
		while(true) {
			if(Target != null && !Frozen) {
				Quaternion newRot = Quaternion.Slerp(this.transform.rotation, (Quaternion.LookRotation(Target.transform.position - this.transform.position)), Time.deltaTime * 2);;
				newRot.x = initialRot.x;
				newRot.z = initialRot.z;
				this.transform.rotation = newRot;
			}
			yield return 0;
		}
	}

	protected override void Update() {
		base.Update();
		if(IsMainBoss) {
			bossHealth.value = (float)CurrentHP/(float)MaxHP;
		}
		PickNewTarget();
	}

	// Triggers a special attack animation, blocks other animations for Time.
	public void TriggerAttackAnimation(string trigger, float time) {
		if(_anim != null) {
			_anim.SetTrigger(trigger);
			StartCoroutine(DontChangeState(time));
		}
	}

	protected IEnumerator DamageRoutine() {
		while(true) {
//			print (string.Format("Dist to target {0}: {1:f}, with Range: {2:f}",this.Target == null ? "Null" : this.Target.name,DistanceBetweenTarget,Range));
			this.isAttacking = false;
			if(DistanceBetweenTarget == -1f || this.Target == null) {
				//print ("Not attacking.");
				yield return 0;
				continue;
			}
			if(DistanceBetweenTarget < this.Range && Alive && !Frozen) {
                if (attackAudio != null) {
                    SoundManager.PlaySFX(this.gameObject, attackAudio);
                }
				Target.AddDamage((int)((float)Power * Random.Range(.9f,1.1f)));
				this.isAttacking = true;
				TriggerAttack();
//				if(bossModel != null) {
//					bossModel.animation.CrossFade("Bite");
//				}
			}
			yield return new WaitForSeconds(AttackSpeed);
		}
	}

	// Boss data is really only used in the construction phase...
	protected override void LoadFromXML() {
		data = DataHandler.Bosses[XMLName];
		LoadFromXML(data);
	}
	
	protected IEnumerator AudioRoutine() {
		int startHP = CurrentHP;
		int audrange = Random.Range(4500,6500);
		while(true) {
			while(startHP - CurrentHP <= audrange) { // A growl effect
				yield return 0;
			}
			SoundManager.PlaySFX(SoundManager.LoadFromGroup(AudioGroup));
			startHP = CurrentHP;
			audrange = Random.Range(4500,6500);
			yield return 0;
		}
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if(stream.isWriting) {
			stream.SendNext(rigidbody.position);
		} else {
			rigidbody.position = (Vector3)stream.ReceiveNext();
		}
	}
}
