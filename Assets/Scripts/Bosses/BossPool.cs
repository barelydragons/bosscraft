﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossPool {
    private static List<BossMinion> minions;
    public static List<BossMinion> Minions {
        get {
            if (minions == null)
                minions = new List<BossMinion>();
            return minions; 
        }
    }

    public static void AddMinion(BossMinion m) {
        if (minions == null)
            minions = new List<BossMinion>();
        minions.Add(m);
    }
    public static void RemoveMinion(BossMinion m) {
        if (minions == null)
            minions = new List<BossMinion>();
        minions.Remove(m);
    }

    public static List<BossMinion> EveryBossActor() {
        List<BossMinion> ret = new List<BossMinion>();
        ret.AddRange(BossPool.Minions);
        ret.Add(mainBoss);
        return ret;
    }

	private static BossMinion mainBoss;
	public static BossMinion MainBoss {
		get { 
            if(mainBoss == null) {
                GameObject go = GameObject.FindGameObjectWithTag("boss") as GameObject;
                if (go == null) {
                    return null;
                }
				mainBoss = go.GetComponent<BossMinion>();
			}
			return mainBoss;
		}
		set { 
            mainBoss = value; 
        }
	}
}
