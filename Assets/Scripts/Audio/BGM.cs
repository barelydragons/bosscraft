﻿using UnityEngine;
using System.Collections;

public class BGM : MonoBehaviour {
	public AudioClip clip;
	// Use this for initialization
	void Start () {
		SoundManager.Instance.crossDuration = 0f;
		SoundManager.Play (clip,true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
