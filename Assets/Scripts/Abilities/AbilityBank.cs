﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilityBank : MonoBehaviour {
	public List<Ability> AbilityGameObjects; // These are literally IN the initialization scene, linked through inspector NOT instantiated.
	public CooldownGroupHandler cdGroup;
	public Dictionary<string,Ability> Abilities;

	private static AbilityBank instance;
	public static AbilityBank Instance {
		get { return instance; }
	}
	// Use this for initialization
	void Awake () {
		if(instance != null) {
			Destroy (this.gameObject);
			return;
        }
        instance = this;
        Abilities = new Dictionary<string, Ability>();
		/*GameObject[] objs = GameObject.FindGameObjectsWithTag("ability");
		AbilityGameObjects = new List<Ability>();
		foreach(GameObject o in objs) {
			Ability abi = o.GetComponent<Ability>();
			if(abi == null) {
				print (o.name);
			}
			AbilityGameObjects.Add(abi);
		}

		Abilities = new Dictionary<string, Ability>();
		instance = this;
		foreach(Ability o in AbilityGameObjects) {
			Abilities.Add(o.AbilityPowerName,o);
		}*/
	}

    public void RegisterAbility(Ability abi) {
        if (Abilities == null) {
            Abilities = new Dictionary<string, Ability>();
        }
        Abilities.Add(abi.AbilityPowerName,abi);//.Add(abi);
    }

	public List<Ability> AvailableBossAbilities() {
		List<Ability> ret = new List<Ability>();
		foreach(Ability abi in Abilities.Values) {
			if(abi.AbilityData.CooldownGroup > 0) {
				if(abi.AbilityAvailable) {
					ret.Add(abi);
				}
			}
		}
		return ret;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
