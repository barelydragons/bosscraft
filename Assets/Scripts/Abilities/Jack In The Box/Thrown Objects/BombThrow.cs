﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BombThrow : Ability {
	public bool IsActive = false;
	public AudioClip BreathLoop;
	public float speed = 5.0f;
	public AnimationClip clip;
    public GameObject explosion;
	public Transform bossDamagePoint;
	public GameObject bombObj;
	private Dictionary<Raider,Raider> targets;
	public AudioClip sfx;

	// Use this for initialization
	protected override void Start () {
		base.Start();
		targets = new Dictionary<Raider,Raider>();
	}

	IEnumerator DamageRoutine(Vector3 tar){
		//BossPool.MainBoss.TriggerAttackAnimation("BombThrow",clip.length);
		BossPool.MainBoss.Frozen = true;
		Vector3 pos = new Vector3(BossPool.MainBoss.transform.position.x, BossPool.MainBoss.transform.position.y + 10f, BossPool.MainBoss.transform.position.z);
		GameObject bomb = (GameObject)Instantiate(bombObj, pos, Quaternion.identity);
		bomb.transform.LookAt(tar);
		bomb.transform.positionTo (this.AbilityData.Duration, tar);
		yield return new WaitForSeconds(this.AbilityData.Duration);
		//SoundManager.PlaySFX(this.gameObject,sfx);
		Collider[] coll = Physics.OverlapSphere(tar,this.AbilityData.Radius);
		foreach(Collider c in coll) {
			Debug.Log(c.tag);
			if(c.CompareTag("raider")) {
				RPGCharacter ch = c.GetComponent<RPGCharacter>();
				ch.AddDamage((int)this.AbilityData.Damage);
			}
		}
        Destroy(Instantiate(explosion, bomb.transform.position, Quaternion.identity), 8f);
		Destroy (bomb);
		BossPool.MainBoss.Frozen = false;
	}

	public override bool IsAoeTargeted() {
		return true;
	}

	public override void Activate(RPGCharacter character){
		if(character == null) {
			Activate(Vector3.zero);
			return;
		}
		Activate(character.transform.position);
	}

	public override void Activate(Vector3 tar){
		if(!AbilityAvailable)
			return;
		tar.y = 0f;
		transform.position = tar;
		StopCoroutine("DamageRoutine");
		StartCoroutine(DamageRoutine(tar));
		StartCoroutine("Cooldown");
	}
}
