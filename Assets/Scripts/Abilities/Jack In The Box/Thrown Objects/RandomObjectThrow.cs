﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RandomObjectThrow : Ability {
	public bool IsActive = false;
	public AudioClip hitSound;
	public AnimationClip clip;
	public Transform bossDamagePoint;
	public List<GameObject> randomObjects;
	private RPGCharacter _target;
	
	// Use this for initialization
	protected override void Start () {
		base.Start();
	}
	
	IEnumerator DamageRoutine(Vector3 tar){
		BossPool.MainBoss.TriggerAttackAnimation("Throw",.3f);
		BossPool.MainBoss.Frozen = true;
		Vector3 pos = new Vector3(BossPool.MainBoss.transform.position.x, BossPool.MainBoss.transform.position.y + 10f, BossPool.MainBoss.transform.position.z);
		GameObject thrownObject = (GameObject)Instantiate(randomObjects[Random.Range(0, randomObjects.Count)], pos, Quaternion.identity);
        Destroy(thrownObject, this.AbilityData.Duration);
		thrownObject.transform.LookAt(tar);
		thrownObject.transform.positionTo (this.AbilityData.Duration, tar);
		yield return new WaitForSeconds(this.AbilityData.Duration);
		_target.AddDamage((int)this.AbilityData.Damage);
		SoundManager.PlaySFX(this.gameObject, hitSound);
		BossPool.MainBoss.Frozen = false;
		Deactivate();
	}
	
	public override bool IsAoeTargeted() {
		return false;
	}
	
	public void Deactivate() {
		
	}
	
	public override void Activate(RPGCharacter target){
		if(target == null) {
			Activate(Vector3.zero);
			return;
		}
		_target = target;
		StopCoroutine("DamageRoutine");
		StartCoroutine(DamageRoutine(target.gameObject.transform.position));
		StartCoroutine("Cooldown");
	}
	
	public override void Activate(Vector3 tar){

	}

	public override bool IsSelectTargeted() {
		return true;
	}
}
