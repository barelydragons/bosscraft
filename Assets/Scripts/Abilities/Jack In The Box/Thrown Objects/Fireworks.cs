﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Fireworks : Ability {

	public bool IsActive = false;
	public AudioClip hitSound;
	public AnimationClip clip;
	public Transform bossDamagePoint;
	public List<GameObject> fireworkObjects;
    public GameObject exploud;
	public GameObject fireworkParticles;
	public List<Color> colors;

	// Use this for initialization
	protected override void Start () {
		base.Start();
	}

    IEnumerator Explode(Vector3 p) {
        yield return new WaitForSeconds(this.AbilityData.Duration);
        Destroy(Instantiate(exploud, p, Quaternion.identity), 8f);
    }

	IEnumerator DamageRoutine(){
		//BossPool.MainBoss.TriggerAttackAnimation("BombThrow",clip.length);
		BossPool.MainBoss.Frozen = true;
		Vector3 pos = new Vector3(BossPool.MainBoss.transform.position.x, BossPool.MainBoss.transform.position.y + 10f, BossPool.MainBoss.transform.position.z);
		List<GameObject> fireworks = new List<GameObject>();
		List<GameObject> particles = new List<GameObject>();
		foreach(Raider r in RaiderPool.AllRaiders){
			if(r.Alive){
                GameObject fireworkObject = fireworkObjects[Random.Range(0, fireworkObjects.Count)];
				//yield return new WaitForSeconds(Random.Range (0f, 0.25f));
				GameObject fire = (GameObject)Instantiate(fireworkObject, pos, Quaternion.identity);
				fireworks.Add (fire);
				particles.Add ((GameObject)Instantiate(fireworkParticles, r.transform.position, Quaternion.identity));
				fire.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
				fire.transform.LookAt(r.gameObject.transform.position);
				fire.transform.positionTo (this.AbilityData.Duration, r.gameObject.transform.position);
                StartCoroutine(Explode(r.gameObject.transform.position));
			}
		}
		BossPool.MainBoss.Frozen = false;
		yield return new WaitForSeconds(this.AbilityData.Duration);
		foreach(Raider r in RaiderPool.AllRaiders){
			if(r.Alive){
				r.AddDamage((int)this.AbilityData.Damage);
				SoundManager.PlaySFX(this.gameObject, hitSound);
			}
		}
		foreach(GameObject f in fireworks){
			Destroy (f);
		}
		foreach(GameObject game in particles){
			game.particleSystem.startColor = colors[Random.Range (0, colors.Count)];
			game.particleSystem.Emit (1000);
			StartCoroutine(explosion (game));
		}
		yield return new WaitForSeconds(1.0f);
		Deactivate();
	}

	public IEnumerator explosion(GameObject part){
		yield return new WaitForSeconds(2.0f);
		Destroy (part);
	}

	public override bool IsAoeTargeted() {
		return false;
	}
	
	public void Deactivate() {
		
	}
	
	public override void Activate(RPGCharacter target){
		if(target == null) {
			Activate(Vector3.zero);
			return;
		}
		StopCoroutine("DamageRoutine");
		StartCoroutine(DamageRoutine());
		StartCoroutine("Cooldown");
	}
	
	public override void Activate(Vector3 tar){
		
	}
	
	public override bool IsSelectTargeted() {
		return true;
	}
}
