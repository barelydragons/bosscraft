﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RayCannon : Ability {
    public Transform cannons;
    public AudioClip cannonSfx;
    public GameObject cannonBallPrefab;
    public float cannonPower = 200f;
    public List<Transform> cannonSpawns;

    public GameObject clown;
    public AudioClip hitSound;
    public AnimationClip open, close;
    public Transform bossDamagePoint;
    public List<GameObject> randomObjects;
    public WSP_LaserBeamWS laser;
    private RPGCharacter _target;
    public GameObject rayGunBase;
    public GameObject rayGunTop;
    private bool cannoning = false;
    private bool raygunning = false;
    public override bool IsAoeTargeted() {
        return false;
    }

    public override void Activate(RPGCharacter character) {
        _target = character;
        Activate();
    }

    public override void Activate(Vector3 tar) {
        Activate();
    }

    void Activate() {
        if (!AbilityAvailable)
            return;
        StartCoroutine("Cooldown");
        cannoning = false;
        raygunning = false;
        StartCoroutine(RayCannonRoutine());
    }

    IEnumerator RayCannonRoutine() {
        BossPool.MainBoss.Frozen = true;
        StartCoroutine(Cannon());
        StartCoroutine(RayGun());
        while (cannoning == false || raygunning == false) {
            yield return null;
        }
        BossPool.MainBoss.Frozen = false; 
    }

    IEnumerator Cannon() {
		cannons.gameObject.SetActive(true);
		cannons.scaleFrom(.3f, Vector3.zero);
		yield return new WaitForSeconds(.3f);
		foreach(Transform t in cannonSpawns) {
			print (t.name);
		}
		float steps = this.AbilityData.Duration / (float)cannonSpawns.Count;
		for(int i = 0; i < cannonSpawns.Count; i++) {
            RaiderPool.AllRaiders[Random.Range(0, RaiderPool.AllRaiders.Count)].AddDamage((int)this.AbilityData.Damage);
			Transform spawn = cannonSpawns[Random.Range(0,cannonSpawns.Count)];
			float shotPower = Random.Range(cannonPower * .8f, cannonPower * 1.2f);
            GameObject ball = (GameObject)Instantiate(cannonBallPrefab, spawn.position, Quaternion.identity);
            SoundManager.PlaySFX(cannonSfx,false,0f,1f,Random.Range(.8f,1f));
			Vector3 dir = ball.transform.position - transform.position;
			dir = dir.normalized;
			ball.rigidbody.AddForce(shotPower * dir);
			Destroy (ball, 5f);
			yield return new WaitForSeconds(steps);
		}
		Vector3 localS = cannons.localScale;
		cannons.scaleTo(.3f, Vector3.zero);
		yield return new WaitForSeconds(.3f);
		cannons.localScale = localS;
		cannons.gameObject.SetActive(false);
    }

    IEnumerator RayGun() {
        BossPool.MainBoss._anim.SetBool("WaitOnAbility", true);
        BossPool.MainBoss._anim.SetTrigger("CloseBox");
        yield return new WaitForSeconds(close.length);
        clown.SetActive(false);
        BossPool.MainBoss._anim.SetTrigger("OpenBox");
        yield return new WaitForSeconds(open.length);
        rayGunBase.SetActive(true);
        rayGunBase.transform.positionFrom(.5f, new Vector3(0, -5f), true);
        rayGunBase.transform.LookAt(_target.transform);
        yield return new WaitForSeconds(0.5f);
        LaserOn();
        laser.CurrentTarget = _target.damageCenter;
        float timer = 0;
        while (timer < this.AbilityData.Duration) {
            timer += this.AbilityData.Speed;
            laser.FireLaser();
            _target.AddDamage((int)this.AbilityData.Damage);
            yield return new WaitForSeconds(this.AbilityData.Speed);
        }
        LaserOff();
        _target.AddDamage((int)this.AbilityData.Damage);
        SoundManager.PlaySFX(this.gameObject, hitSound);
        Vector3 p = rayGunBase.transform.position;
        rayGunBase.transform.positionTo(.5f, new Vector3(0, -5f), true);
        BossPool.MainBoss.TriggerAttackAnimation("CloseBox", close.length);
        yield return new WaitForSeconds(close.length);
        clown.SetActive(true);
        BossPool.MainBoss._anim.SetTrigger("OpenBox");
        yield return new WaitForSeconds(open.length);
        rayGunBase.transform.position = p;
        rayGunBase.SetActive(false);
        BossPool.MainBoss.Frozen = false;
        BossPool.MainBoss._anim.SetBool("WaitOnAbility", false);
    }

    void LaserOn() {
        laser.LaserCanFire = true;
        laser.LaserBeamActive = true;
    }

    void LaserOff() {
        laser.LaserCanFire = false;
        laser.LaserBeamActive = false;
    }

    public override bool IsSelectTargeted() {
        return true;
    }
}
