﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cannon : Ability {
	public Transform cannons;
    public AudioClip sfx;
	public GameObject cannonBallPrefab;
	public float cannonPower = 200f;
	public List<Transform> cannonSpawns;
	void Awake () {
		cannons.gameObject.SetActive(false);
	}

	public override bool IsAoeTargeted ()
	{
		return false;
	}
	public override void Activate (RPGCharacter character)
	{
		StartCoroutine(CannonAbility());
	}
	public override void Activate (Vector3 tar)
	{
		StartCoroutine(CannonAbility());
	}

	IEnumerator CannonAbility() {
		StartCoroutine("Cooldown");
		BossPool.MainBoss.Frozen = true; 
		cannons.gameObject.SetActive(true);
		cannons.scaleFrom(.3f, Vector3.zero);
		yield return new WaitForSeconds(.3f);
		foreach(Transform t in cannonSpawns) {
			print (t.name);
		}
		float steps = this.AbilityData.Duration / (float)cannonSpawns.Count;
		for(int i = 0; i < cannonSpawns.Count; i++) {
            RaiderPool.AllRaiders[Random.Range(0, RaiderPool.AllRaiders.Count)].AddDamage((int)this.AbilityData.Damage);
			Transform spawn = cannonSpawns[Random.Range(0,cannonSpawns.Count)];
			float shotPower = Random.Range(cannonPower * .8f, cannonPower * 1.2f);
            GameObject ball = (GameObject)Instantiate(cannonBallPrefab, spawn.position, Quaternion.identity);
            SoundManager.PlaySFX(sfx,false,0f,1f,Random.Range(.8f,1f));
			Vector3 dir = ball.transform.position - transform.position;
			dir = dir.normalized;
			ball.rigidbody.AddForce(shotPower * dir);
			Destroy (ball, 5f);
			yield return new WaitForSeconds(steps);
		}
		Vector3 localS = cannons.localScale;
		cannons.scaleTo(.3f, Vector3.zero);
		yield return new WaitForSeconds(.3f);
		cannons.localScale = localS;
		cannons.gameObject.SetActive(false);
		BossPool.MainBoss.Frozen = false; 
	}
}
