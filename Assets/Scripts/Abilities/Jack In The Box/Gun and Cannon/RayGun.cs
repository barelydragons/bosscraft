﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RayGun : Ability {

	public bool IsActive = false;
    public GameObject clown;
	public AudioClip hitSound;
	public AnimationClip open, close;
	public Transform bossDamagePoint;
	public List<GameObject> randomObjects;
	public WSP_LaserBeamWS laser;
	private RPGCharacter _target;
	public GameObject rayGunBase;
	public GameObject rayGunTop;

	// Use this for initialization
	protected override void Start () {
        LaserOff();
        rayGunBase.SetActive(false);
		base.Start();
	}

	IEnumerator DamageRoutine(){
        print("LASERFIRE");
		BossPool.MainBoss.Frozen = true;
        BossPool.MainBoss._anim.SetBool("WaitOnAbility",true);
        BossPool.MainBoss._anim.SetTrigger("CloseBox");
        yield return new WaitForSeconds(close.length);
        clown.SetActive(false);
        BossPool.MainBoss._anim.SetTrigger("OpenBox");
        yield return new WaitForSeconds(open.length);
		rayGunBase.SetActive(true);
        rayGunBase.transform.positionFrom(.5f, new Vector3(0, -5f), true);
		rayGunBase.transform.LookAt(_target.transform);
		yield return new WaitForSeconds(0.5f);
		LaserOn();
		laser.CurrentTarget = _target.damageCenter;
		float timer = 0;
		while(timer < this.AbilityData.Duration) {
			timer += this.AbilityData.Speed;
			laser.FireLaser();
			_target.AddDamage((int)this.AbilityData.Damage);
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
		LaserOff();
		_target.AddDamage((int)this.AbilityData.Damage);
        SoundManager.PlaySFX(this.gameObject, hitSound);
        Vector3 p = rayGunBase.transform.position;
        rayGunBase.transform.positionTo(.5f, new Vector3(0, -5f), true);
        BossPool.MainBoss.TriggerAttackAnimation("CloseBox", close.length);
        yield return new WaitForSeconds(close.length);
        clown.SetActive(true);
        BossPool.MainBoss._anim.SetTrigger("OpenBox");
        yield return new WaitForSeconds(open.length);
        rayGunBase.transform.position = p;
        rayGunBase.SetActive(false);
		BossPool.MainBoss.Frozen = false;
        BossPool.MainBoss._anim.SetBool("WaitOnAbility", false);
	}

	void LaserOn() {
		laser.LaserCanFire = true;
		laser.LaserBeamActive = true;
	}
	
	void LaserOff() {
		laser.LaserCanFire = false;
		laser.LaserBeamActive = false;
	}

	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter target){
		if(target == null) {

		}
		_target = target;
		StopCoroutine("DamageRoutine");
        StartCoroutine("DamageRoutine");
		StartCoroutine("Cooldown");
	}
	
	public override void Activate(Vector3 tar){
		
	}
	
	public override bool IsSelectTargeted() {
		return true;
	}
}
