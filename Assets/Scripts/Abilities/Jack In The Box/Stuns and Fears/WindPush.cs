﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindPush : Ability {
	public List<ParticleSystem> particles;
	public AnimationClip clip;
	public AudioClip sfx;
	public float particleRate;
	private Dictionary<Raider,Raider> targets;

	protected override void Start ()
	{
		base.Start();
        partSet(false);
		targets = new Dictionary<Raider,Raider>();
	}


    void partSet(bool on) {
        foreach (ParticleSystem psys in particles) {
            psys.enableEmission = on;
        }
    }

	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter target) {
		if(!this.AbilityAvailable)
			return;
		StartCoroutine(PushBack ());
		StartCoroutine("Cooldown");
	}
	
	IEnumerator PushBack() {
		SoundManager.PlaySFX(this.gameObject,sfx);
		//BossPool.MainBoss.TriggerAttackAnimation("Stomp",clip.length);
        float time = 0f;
        partSet(true);
		while(time < this.AbilityData.Duration) {
			foreach(Raider r in targets.Values) {
				r.rigidbody.AddForce((r.transform.position - BossPool.MainBoss.transform.position).normalized * this.AbilityData.Damage);
			}
			time += this.AbilityData.Speed;
			yield return new WaitForSeconds(this.AbilityData.Speed);
        }
        partSet(false);
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.rigidbody.velocity = Vector3.zero;
		}
	}
	
	public override void Activate(Vector3 position) {
		
	}

	public void OnTriggerEnter(Collider other) {
		if(other.CompareTag("raider")) {
			Raider r = other.GetComponent<Raider>();
			targets[r] = r;
		}
	}

	public void OnTriggerExit(Collider other){
		if(other.CompareTag("raider")) {
			Raider r = other.GetComponent<Raider>();
			targets.Remove(r);
		}
	}
	
}
