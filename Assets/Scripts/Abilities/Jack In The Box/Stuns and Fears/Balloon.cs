﻿using UnityEngine;
using System.Collections;

public class Balloon : Ability {
    private RPGCharacter tar;
    public AudioClip inflate, pop;
    public override bool IsAoeTargeted() {
        return false;
    }

    public override void Activate(RPGCharacter character) {
        if (!AbilityAvailable)
            return;
        tar = character;
        StartCoroutine(BalloonRoutine());
        StartCoroutine("Cooldown");
    }

    IEnumerator BalloonRoutine() {
        print("I AM BALLOON ABILITY. HEAR MY HELIUM BLAST.");
        Vector3 startP = tar.transform.position;
        tar.Frozen = true;
        float up = this.AbilityData.Duration * .8f;
        float down = this.AbilityData.Duration * .2f;
        SoundManager.PlaySFX(this.gameObject, inflate);
        GoEaseType easeTemp = Go.defaultEaseType;
        Go.defaultEaseType = GoEaseType.CubicInOut;
        tar.transform.positionTo(up, new Vector3(0, 30f), true);
        yield return new WaitForSeconds(this.AbilityData.Duration);
        tar.transform.positionTo(down, startP);
        SoundManager.PlaySFX(this.gameObject, pop);
        yield return new WaitForSeconds(down);
        tar.Frozen = false;
        Go.defaultEaseType = easeTemp;
        tar.AddDamage((int)this.AbilityData.Damage);
    }

    public override void Activate(Vector3 tar) {
        throw new System.NotImplementedException();
    }

    public override bool IsSelectTargeted() {
        return true;
    }
}
