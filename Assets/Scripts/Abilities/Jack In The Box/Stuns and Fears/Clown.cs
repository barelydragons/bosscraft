﻿using UnityEngine;
using System.Collections;

public class Clown : Ability {
	public ParticleSystem particles;
    public GameObject jack;//, clown;
	public AudioClip sfx;
	public AnimationClip clip;
	public Transform bossDamagePoint;
	
	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter target) {
		if(!this.AbilityAvailable)
			return;
		StartCoroutine(Scream());
		StartCoroutine("Cooldown");
	}
	
	IEnumerator Scream() {
		SoundManager.PlaySFX(this.gameObject,sfx);
        float time = 0f;
        BossMinion boss = BossPool.MainBoss;
        boss.Frozen = true;
        boss._anim.SetBool("WaitOnAbility", true);
        boss._anim.SetTrigger("CloseBox");
        yield return new WaitForSeconds(.2f);
        boss._anim.SetTrigger("OpenBox");
        jack.SetActive(false);
       // clown.SetActive(true);

       // clown.transform.positionFrom(.5f, new Vector3(0, -5f), true);
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.Frozen = true;
        }
        while (time <= this.AbilityData.Duration) {
            time += .1f;
            foreach (Raider r in RaiderPool.AllRaiders) {
                particles.transform.position = r.transform.position;
                particles.Emit(80);
            }
            yield return new WaitForSeconds(.1f);
        }
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.Frozen = false;
        }
       // Vector3 p = clown.transform.position;
        // clown.transform.positionTo(.5f, new Vector3(0, -5f), true);
        boss._anim.SetTrigger("CloseBox");
        yield return new WaitForSeconds(.5f);
        //clown.transform.position = p;
        //clown.SetActive(false);
        jack.SetActive(true);
        boss.Frozen = false;
        boss._anim.SetBool("WaitOnAbility", false);
        boss._anim.SetTrigger("OpenBox");
	}
	public override void Activate(Vector3 position) {
		
	}
	
	public override bool IsSelectTargeted() {
		return true;
	}
	void Update(){
	}
}

