﻿using UnityEngine;
using System.Collections;

public class Bounce : Ability {
    public GameObject clown;
    public GameObject bounceSplosion;
    public AudioClip leap, land;
    public override bool IsAoeTargeted() {
        return true;
    }

    public override void Activate(RPGCharacter character) {
        if (!AbilityAvailable)
            return;
        StartCoroutine(BounceRoutine(character.transform.position));
    }

    public override void Activate(Vector3 tar) {
        if (!AbilityAvailable)
            return;
        StartCoroutine(BounceRoutine(tar));
    }

    IEnumerator BounceRoutine(Vector3 tar) {
        StartCoroutine("Cooldown");
        clown.SetActive(false);
        BossMinion boss = BossPool.MainBoss;
        GoEaseType easeTemp = Go.defaultEaseType;
        Go.defaultEaseType = GoEaseType.CubicInOut;
        boss.Frozen = true;
        boss._anim.SetBool("WaitOnAbility", true);
        boss._anim.SetTrigger("CloseBox");
        yield return new WaitForSeconds(.2f);
        Vector3 startP = boss.transform.position;
        tar.y = startP.y;
        SoundManager.PlaySFX(this.gameObject, leap);
        boss.transform.positionTo(.8f, new Vector3(0, 50f, 0), true);
        yield return new WaitForSeconds(.8f);
        boss.transform.positionTo(1f, tar);
        yield return new WaitForSeconds(1f);
        Collider[] colls = Physics.OverlapSphere(transform.position, this.AbilityData.Radius);
        foreach (Collider item in colls) {
            if (item.tag == "raider") {
                RPGCharacter raider = item.GetComponent<RPGCharacter>();
                raider.AddDamage((int)this.AbilityData.Damage);
            }
        }
        SoundManager.PlaySFX(this.gameObject, land);
        Vector3 exp = boss.transform.position;
        exp.y = 0;
        Destroy(Instantiate(bounceSplosion, exp, Quaternion.identity), 15f);
        boss.Frozen = false;
        clown.SetActive(true);
        Go.defaultEaseType = easeTemp;
        boss._anim.SetBool("WaitOnAbility", false);
        boss._anim.SetTrigger("OpenBox");
    }
}
