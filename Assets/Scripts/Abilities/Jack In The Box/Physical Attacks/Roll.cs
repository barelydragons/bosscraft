﻿using UnityEngine;
using System.Collections;

public class Roll : Ability {
    public GameObject clown;
    public GameObject spikes;

    protected override void Start() {
        base.Start();
        spikes.SetActive(false);
    }
    public override bool IsAoeTargeted() {
        return false;
    }
    public override void Activate(RPGCharacter character) {
        if (!this.AbilityAvailable)
            return;
        StartCoroutine("Cooldown");
        StopCoroutine(RollRoutine());
        StartCoroutine(RollRoutine());
    }
    public override void Activate(Vector3 tar) {
        if (!this.AbilityAvailable) {
            return;
        }
        StartCoroutine("Cooldown");
        StopCoroutine(RollRoutine());
        StartCoroutine(RollRoutine());
    }

    IEnumerator RollRoutine() {
        BossMinion boss = BossPool.MainBoss;
        boss.Frozen = true;
        spikes.SetActive(true);
        spikes.transform.scaleFrom(.5f, Vector3.zero);
        boss._anim.SetBool("WaitOnAbility", true);
        boss._anim.SetTrigger("CloseBox");
        boss._anim.SetBool("Roll", true);
        yield return new WaitForSeconds(.2f);
        clown.SetActive(false);
        boss.rigidbody.velocity = Vector3.zero;
        foreach(RPGCharacter c in RaiderPool.AllRaiders) {
            Vector3 p = c.transform.position;
            Vector3 relativeP = p - transform.position;
            relativeP.y = boss.transform.position.y;
            Quaternion rotation = Quaternion.LookRotation(relativeP);
            boss.transform.eularAnglesTo(.15f, rotation.eulerAngles);
            yield return new WaitForSeconds(.05f);
            boss.transform.positionTo(this.AbilityData.Speed, p);
            yield return new WaitForSeconds(this.AbilityData.Speed);
            c.AddDamage((int)this.AbilityData.Damage);
            boss.rigidbody.velocity = Vector3.zero;
        }
        clown.SetActive(true);
        boss.Frozen = false;
        Vector3 lScale = spikes.transform.localScale;
        spikes.transform.scaleTo(.5f, Vector3.zero);
        yield return new WaitForSeconds(.5f);
        spikes.transform.localScale = lScale;
        spikes.SetActive(false);
        boss._anim.SetBool("WaitOnAbility", false);
        boss._anim.SetTrigger("OpenBox");
        boss._anim.SetBool("Roll", false);
    }
}
