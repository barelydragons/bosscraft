﻿using UnityEngine;
using System.Collections;

public class BoxingGlove : Ability {
    public AnimationClip open, close;
    public GameObject clown, glove, box;
    public AudioClip sfx;
    protected override void Start() {
        base.Start();

        glove.SetActive(false);
    }
    public override bool IsAoeTargeted() {
        return false;
    }

    public override void Activate(RPGCharacter character) {
        ActivateAbi(character);
    }

    public override void Activate(Vector3 tar) {
        //Activate();
    }

    void ActivateAbi(RPGCharacter tar) {
        if (!AbilityAvailable)
            return;
        StartCoroutine("Cooldown");
        StartCoroutine(BoxRoutine(tar));
    }

    IEnumerator BoxRoutine(RPGCharacter tar) {
        BossPool.MainBoss.Frozen = true;
        BossPool.MainBoss._anim.SetBool("WaitOnAbility", true);
        BossPool.MainBoss._anim.SetTrigger("CloseBox");
        yield return new WaitForSeconds(close.length);
        clown.SetActive(false);
        BossPool.MainBoss._anim.SetTrigger("OpenBox");
        yield return new WaitForSeconds(open.length);
        glove.SetActive(true);
        glove.transform.positionFrom(this.AbilityData.Speed, new Vector3(0, -20f), true);
        yield return new WaitForSeconds(this.AbilityData.Speed);
        tar.AddDamage((int)this.AbilityData.Damage);
        SoundManager.PlaySFX(this.gameObject, sfx);
        Vector3 p = glove.transform.position;
        glove.transform.positionTo(close.length, new Vector3(0, -5f), true);
        BossPool.MainBoss.TriggerAttackAnimation("CloseBox", close.length);
        yield return new WaitForSeconds(close.length);
        glove.SetActive(false);
        clown.SetActive(true);
        BossPool.MainBoss._anim.SetTrigger("OpenBox");
        yield return new WaitForSeconds(open.length);
        glove.transform.position = p;
        BossPool.MainBoss.Frozen = false;
        BossPool.MainBoss._anim.SetBool("WaitOnAbility", false);
    }
}
