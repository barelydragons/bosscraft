﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilityTrack : MonoBehaviour {
	public List<UISprite> abilityTracks;
	public List<AbilityPowerFiller> fillers;
	public List<BossAbilityTooltip> tooltips; 

	public void SetAbility(int num, string name) {
		if(abilityTracks.Count == 0) {
			abilityTracks = new List<UISprite>(GetComponentsInChildren<UISprite>());
		}
		abilityTracks[num].spriteName = AbilityBank.Instance.Abilities[name].AbilityData.IconName;
	}
}
