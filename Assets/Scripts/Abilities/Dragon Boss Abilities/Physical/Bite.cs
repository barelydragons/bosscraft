using UnityEngine;
using System.Collections;

// One shot ability, happens instantly
public class Bite : Ability {
	public AnimationClip clip;
	public AudioClip sfx;
	public override bool IsAoeTargeted() {
		return false;
	}

	public override void Activate(RPGCharacter character) {
		if(!AbilityAvailable)
			return;
		// Animation stuff later, but other things as well....
		character.AddDamage((int)this.AbilityData.Damage);
		SoundManager.PlaySFX(this.gameObject,sfx);
		BossPool.MainBoss.TriggerAttackAnimation("Kick",clip.length);
		StartCoroutine("Cooldown");
	}

	public override void Activate(Vector3 tar) {
		// null for this class
		throw new System.NotImplementedException();
	}
}
