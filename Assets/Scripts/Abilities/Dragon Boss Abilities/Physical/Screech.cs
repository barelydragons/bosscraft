using UnityEngine;
using System.Collections;

public class Screech : Ability {
	public ParticleSystem particles;
	public AudioClip sfx;
	public AnimationClip clip;
	public Transform bossDamagePoint;
	
	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter target) {
		if(!this.AbilityAvailable)
			return;
		StartCoroutine(Scream());
		StartCoroutine("Cooldown");
	}
	
	IEnumerator Scream() {
		SoundManager.PlaySFX(this.gameObject,sfx);
		BossPool.MainBoss.TriggerAttackAnimation("Roar",clip.length);
		float time = 0f;
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.Frozen = true;
		}
		while(time < this.AbilityData.Duration) {
			particles.Emit(200);
			time += this.AbilityData.Speed;
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.Frozen = false;
		}
	}
	
	public override void Activate(Vector3 position) {
		
	}
	
	public override bool IsSelectTargeted() {
		return true;
	}
}
