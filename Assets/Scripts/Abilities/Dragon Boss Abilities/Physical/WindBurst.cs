using UnityEngine;
using System.Collections;

public class WindBurst : Ability {
	public ParticleSystem particles;
	public RPGCharacter _target;
	public Transform bossDamagePoint;
	public AnimationClip clip;
	public AudioClip sfx;
	
	public override bool IsAoeTargeted() {
		return false;
	}

    protected override void Start() {
        base.Start();
        bossDamagePoint = BossPool.MainBoss.damageCenter;
    }
	
	public override void Activate(RPGCharacter target) {
		if(!this.AbilityAvailable)
			return;
		_target = target;
		StartCoroutine(Scream());
		StartCoroutine("Cooldown");
	}
	
	IEnumerator Scream() {
		SoundManager.PlaySFX(this.gameObject,sfx);
		BossPool.MainBoss.TriggerAttackAnimation("Stomp",clip.length);
		float time = 0f;
		while(time < this.AbilityData.Duration) {
			foreach(Raider r in RaiderPool.AllRaiders) {
				r.rigidbody.AddForce((r.transform.position - BossPool.MainBoss.transform.position).normalized * this.AbilityData.Damage);
			}
			particles.Emit(200);
			time += this.AbilityData.Speed;
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
		foreach(Raider r in RaiderPool.AllRaiders) {
			r.rigidbody.velocity = Vector3.zero;
		}
	}
	
	public override void Activate(Vector3 position) {
		
	}
	
	public override bool IsSelectTargeted() {
		return true;
	}
	
	void Update() {
		transform.position = bossDamagePoint.position;
	}
}
