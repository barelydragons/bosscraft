using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireBreath : Ability {
	public bool IsActive = false;
	public AudioClip BreathLoop;
	public AnimationClip clip;
	public ParticleSystem particles;
	public Transform bossDamagePoint;
	private float fireRate;

	private Dictionary<Raider,Raider> targets;
	
	// Use this for initialization
	protected override void Start () {
		base.Start();
		fireRate = particles.emissionRate;
		particles.emissionRate = 0;
		collider.enabled = false;
		targets = new Dictionary<Raider,Raider>();
	}
	
	private IEnumerator DamageRoutine() {
		BossPool.MainBoss.TriggerAttackAnimation("FireBreath",clip.length);
		BossPool.MainBoss.Frozen = true;
		particles.emissionRate = fireRate;
		SoundManager.PlaySFXLoop(this.gameObject,BreathLoop,true,1f,1f,this.AbilityData.Duration);
		float t = 0;
		while(t < clip.length) {
			t += this.AbilityData.Speed;
			foreach(Raider r in targets.Values) {
				r.AddDamage((int)this.AbilityData.Damage);
			}
			yield return new WaitForSeconds(this.AbilityData.Speed);
		} 
		BossPool.MainBoss.Frozen = false;
		Deactivate();
	}
	
	public void Deactivate() {
		collider.enabled = false;
		particles.emissionRate = 0;
	}
	
	public override void Activate(Vector3 tar) {
		if(!AbilityAvailable)
			return;
		collider.enabled = true;
		StopCoroutine("DamageRoutine");
		StartCoroutine("DamageRoutine");
		StartCoroutine("Cooldown");
	}
	
	public override void Activate(RPGCharacter character) {
		if(character == null) {
			Activate(Vector3.zero);
			return;
		}
		Activate(character.transform.position);
	}
	
	public override bool IsAoeTargeted() {
		return false;
	}

	public void OnTriggerEnter(Collider other) {
		if(other.tag == "raider") {
			Raider r = other.GetComponent<Raider>();
			targets[r] = r;
		}
	}
}
