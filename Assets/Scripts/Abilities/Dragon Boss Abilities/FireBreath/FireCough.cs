using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireCough : Ability {
	public bool IsActive = false;
	public ParticleSystem particles;
	public Transform bossDamagePoint;
	
	private Dictionary<Raider,Raider> targets;
	
	// Use this for initialization
	protected override void Start () {
		base.Start();
        bossDamagePoint = BossPool.MainBoss.damageCenter;
		transform.position = bossDamagePoint.position;
		//		transform.parent = bossDamagePoint;
		targets = new Dictionary<Raider,Raider>();
		//		Activate (Vector3.zero);
	}
	
	void Update() {
		transform.position = bossDamagePoint.position;
		transform.rotation = bossDamagePoint.rotation;
	}
	
	public override void Activate(Vector3 tar) {
		if(!AbilityAvailable)
			return;
		foreach(Raider r in targets.Values) {
			r.AddDamage((int)this.AbilityData.Damage);
		}
		particles.Emit(500);
		StartCoroutine("Cooldown");
	}
	
	public override void Activate(RPGCharacter character) {
		if(character == null) {
			Activate(Vector3.zero);
			return;
		}
		Activate(character.transform.position);
	}
	
	public override bool IsAoeTargeted() {
		return false;
	}
	
	public void OnTriggerEnter(Collider other) {
		if(other.tag == "raider") {
			Raider r = other.GetComponent<Raider>();
			targets[r] = r;
		}
	}

	public void OnTriggerExit(Collider other) {
		if(other.tag == "raider") {
			Raider r = (Raider)other.GetComponent(typeof(Raider));
			if(targets.ContainsKey(r))
				targets.Remove(r);
		}
	}
}
