using UnityEngine;
using System.Collections;

public class MinionAbility : Ability {
	public GameObject whelpPrefab, poofparticles;
	public AnimationClip clip;
	public AudioClip sfx;
	public override bool IsAoeTargeted() {
		return true;
	}
	
	public override void Activate(RPGCharacter character) {
		return;
	}
	
	public override void Activate(Vector3 tar) {
		print ("Activating");
		if(!AbilityAvailable)
			return;
		SoundManager.PlaySFX(this.gameObject,sfx);
		BossPool.MainBoss.TriggerAttackAnimation("Summon",clip.length);
		// Animation stuff later, but other things as well....
		tar.y = 4.5f;
		if(PhotonNetwork.inRoom) {
			PhotonNetwork.Instantiate("Whelp",tar,Quaternion.identity,0);
		} else {
			Instantiate(whelpPrefab,tar,Quaternion.identity);
			Destroy(Instantiate(poofparticles,tar,Quaternion.identity),3f);
		}
		StartCoroutine("Cooldown");
	}
}
