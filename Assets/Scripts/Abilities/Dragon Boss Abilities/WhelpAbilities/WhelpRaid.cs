using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WhelpRaid : Ability {
	public GameObject whelpPrefab;
	public Transform whelpSpawnPoint;
	public AnimationClip clip;
	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter character) {
		Activate(Vector3.zero);
	}
	
	public override void Activate(Vector3 tar) {
		if(!AbilityAvailable)
			return;
		StartCoroutine("WhelpRaidGo");
		StartCoroutine("Cooldown");
	}
	
	IEnumerator WhelpRaidGo() {
		BossPool.MainBoss.TriggerAttackAnimation("Summon",clip.length);
		List<Raider> raiders = RaiderPool.AllRaiders;
		List<WhelpRaider> whelplings = new List<WhelpRaider>();
		for(int i = 0; i < raiders.Count; i++) {
			if(raiders[i].Alive == false)
				continue;
			float zOffset = Random.Range(-30f,30f);
			Vector3 pos = whelpSpawnPoint.position;
			pos.z += zOffset;
			pos.y = 60;
			WhelpRaider whelpling = ((GameObject)Instantiate(whelpPrefab,pos,Quaternion.identity)).GetComponent<WhelpRaider>();
			whelplings.Add(whelpling);
			whelpling.Attack(raiders[i],this.AbilityData.Damage);
			yield return new WaitForSeconds(1f);
		}
	
		yield return new WaitForSeconds(this.AbilityData.Duration);

		foreach(WhelpRaider wr in whelplings) {
			wr.Retreat(whelpSpawnPoint.position);
		}
	}
}
