﻿using UnityEngine;
using System.Collections;

public class WhelpRaider : MonoBehaviour {
	private bool retreating = false;
	private RPGCharacter _tar;
	public void Attack(RPGCharacter target, float damage) {
		retreating = false;
		_tar = target;
		this.transform.positionTo(1.5f,target.transform.position + new Vector3(0,8f));
		StartCoroutine("AttackRoutine",damage);
	}

	IEnumerator AttackRoutine(float damage) {
		float getInRangeTime = 1.5f;
		this.transform.positionTo(getInRangeTime,_tar.transform.position + new Vector3(0,15f));
		yield return new WaitForSeconds(getInRangeTime);
		while(retreating == false) {
			_tar.AddDamage((int)damage);
			ParticleStream.Instance.BeanieBabyAirDrop(transform,_tar.transform);
			yield return new WaitForSeconds(2f);
		}
	}

	IEnumerator RetreatRoutine(Vector3 returnPosition) {
		this.transform.positionTo(1.5f,returnPosition);
		yield return new WaitForSeconds(1.5f);
		Destroy (this.gameObject);
	}

	public void Retreat(Vector3 whereToGo) {
		retreating = true;
		StartCoroutine("RetreatRoutine",whereToGo);
	}
}
