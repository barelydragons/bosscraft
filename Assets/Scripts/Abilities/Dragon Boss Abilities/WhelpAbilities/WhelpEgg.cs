using UnityEngine;
using System.Collections;

public class WhelpEgg : Ability {
	public GameObject whelpPrefab;
	public GameObject eggPrefab;
	public GameObject poofparticles;
	public AnimationClip clip;
	public AudioClip sfx, hatch;
	private RPGCharacter egg;
	public override bool IsAoeTargeted() {
		return true;
	}
	
	public override void Activate(RPGCharacter character) {
		return;
	}
	
	public override void Activate(Vector3 tar) {
		if(!AbilityAvailable)
			return;
		// Animation stuff later, but other things as well....
		tar.y = 4.5f;
		SoundManager.PlaySFX(this.gameObject,sfx);
		BossPool.MainBoss.TriggerAttackAnimation("Summon",clip.length);
		GameObject eggObj;
		if(PhotonNetwork.inRoom && RoomHandler.Instance.IsBossPlayer()) {
			eggObj = (GameObject)PhotonNetwork.Instantiate("WhelpEgg",tar,Quaternion.identity,0);
			print ("Photon instantiate.");
		} else if(!PhotonNetwork.inRoom) {
			eggObj = (GameObject)Instantiate(eggPrefab,tar,Quaternion.identity);
		} else {;
			StartCoroutine("Cooldown"); /// The other player is handling the egg, so just return after starting the cooldown
			return;
		}
		egg = eggObj.GetComponent<RPGCharacter>();
		egg.Frozen = true;
		StartCoroutine("EggHatch");
		StartCoroutine("Cooldown");
	}
	
	IEnumerator EggHatch() {
		yield return new WaitForSeconds(this.AbilityData.Duration);
		if(egg.Alive) {
			SoundManager.PlaySFX(this.gameObject,sfx);
			if(PhotonNetwork.inRoom) {
				PhotonNetwork.Instantiate("Whelp",this.egg.transform.position,Quaternion.identity,0);
			} else {
				Instantiate(whelpPrefab,this.egg.transform.position,Quaternion.identity);
			}
		}
		if(egg != null) {
			if(PhotonNetwork.inRoom) {
				PhotonNetwork.Instantiate("whelpHatch",this.egg.transform.position,Quaternion.identity,0);
			} else {
				Destroy(Instantiate(poofparticles,egg.transform.position,Quaternion.identity),3f);
			}
			if(PhotonNetwork.inRoom) {
				egg.photonView.RPC("DeathAndDestroy",PhotonTargets.All);
			} else {
				egg.Death();
			}
		}
	}
}
