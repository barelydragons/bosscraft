using UnityEngine;
using System.Collections;

public class Immolate : Ability {
	private RPGCharacter _target;
	public AudioClip burnSound;
	public ParticleSystem particles;

	public override bool IsAoeTargeted() {
		return false;
	}

	public override void Activate(RPGCharacter target) {
		if(!this.AbilityAvailable)
			return;
		_target = target;
		StartCoroutine(LightTheBastardsOnFire());
		StartCoroutine("Cooldown");
	}

	IEnumerator LightTheBastardsOnFire() {
		float time = 0f;
		particles.emissionRate = 135f;
		SoundManager.PlaySFX(this.gameObject,burnSound);
		while(time < this.AbilityData.Duration) {
			time += this.AbilityData.Speed;
			_target.AddDamage((int)this.AbilityData.Damage);
			transform.position = _target.transform.position - new Vector3(0,2f);
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
		particles.emissionRate = 0f;
	}

	public override void Activate(Vector3 position) {

	}

	public override bool IsSelectTargeted() {
		return true;
	}
}
