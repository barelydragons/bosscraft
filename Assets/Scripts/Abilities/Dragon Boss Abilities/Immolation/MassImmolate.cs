using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MassImmolate : Ability {
	private List<Raider> _targets;
	public AudioClip immolationAudio, flop;
	public ParticleSystem particles;
	public AnimationClip clip;
	
	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter target) {
		if(!this.AbilityAvailable)
			return;
		_targets = RaiderPool.AllRaiders;
		StartCoroutine(LightTheBastardsOnFire());
		StartCoroutine("Cooldown");
	}
	
	IEnumerator LightTheBastardsOnFire() {
		float time = 0f;
		BossPool.MainBoss.TriggerAttackAnimation("ImmolateArea",clip.length);
		SoundManager.PlaySFXLoop(this.gameObject,flop);
		particles.emissionRate = 400f;
		yield return .5f; // Give particles time to ramp up
		SoundManager.PlaySFXLoop(this.gameObject,immolationAudio,true,1f,1f,this.AbilityData.Duration);
		while(time < this.AbilityData.Duration) {
			time += this.AbilityData.Speed;
			foreach(Raider r in _targets) {
				r.AddDamage((int)this.AbilityData.Damage);
			}
			yield return new WaitForSeconds(this.AbilityData.Speed);

		}
		particles.emissionRate = 0f;
	}
	
	public override void Activate(Vector3 position) {
		
	}
}
