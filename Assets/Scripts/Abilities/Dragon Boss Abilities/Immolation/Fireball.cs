using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Fireball : Ability {
	public bool IsActive = false;
	public ParticleSystem particles;

	// Use this for initialization
	protected override void Start () {
		base.Start();
		particles.emissionRate = 0;

	}

	private IEnumerator DamageRoutine() {
		particles.emissionRate = 200;
		float timeElapsed = this.AbilityData.Duration;
		float damageTick = 0f;
		while(timeElapsed >= 0) {
			timeElapsed -= Time.deltaTime;
			damageTick += Time.deltaTime;
			if(damageTick > this.AbilityData.Speed) {
				Collider[] colls = Physics.OverlapSphere(transform.position,this.AbilityData.Radius);
				foreach(Collider item in colls) {
					if(item.tag == "raider") {
						RPGCharacter raider = item.GetComponent<RPGCharacter>();
						raider.AddDamage((int)this.AbilityData.Damage);
					}
				}
				damageTick = 0f;
			}
			yield return 0;
		}
		Deactivate();
	}

	public void Deactivate() {
		particles.emissionRate = 0;
	}

	public override void Activate(Vector3 tar) {
		if(!AbilityAvailable)
			return;
		StopCoroutine("DamageRoutine");
		tar.y = 0; // Should be a ray, challenge for another day though
		// Will place the ability at position
		transform.position = tar;
		IsActive = true;
		StartCoroutine("DamageRoutine");
		StartCoroutine("Cooldown");
	}
	
	public override void Activate(RPGCharacter character) {
		Activate(character.transform.position);
	}

	public override bool IsAoeTargeted() {
		return true;
	}
}
