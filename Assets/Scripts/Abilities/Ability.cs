﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AbilityStrength {
	Weak,
	Normal,
	Strong,
	Inactive
}

public class ActivatedAbilityArgs {
	public Ability ability;
	public ActivatedAbilityArgs(Ability abi) {
		ability = abi;
	}
}

public abstract class Ability : MonoBehaviour {
	public string AnimationName;
	public string AbilityPowerName; // Name of ability, looked up for balancing in XML
	private string StrongAbilityName, WeakAbilityName;
	public Ability StrongAbility {
		get {
			if(StrongAbilityName == null) {
				StrongAbilityName = abilityAction.StrongAbility;
			}
			return AbilityBank.Instance.Abilities[StrongAbilityName];
		}
	}
	public Ability WeakAbility {
		get {
			if(WeakAbilityName == null) {
				WeakAbilityName = abilityAction.WeakAbility;
			}
			if(AbilityBank.Instance.Abilities.ContainsKey(WeakAbilityName) == false) {
				print (WeakAbilityName);
			}
			return AbilityBank.Instance.Abilities[WeakAbilityName];
		}
	}
	public UISprite abilityCooldownOverlay;
	public delegate void AbilityEventHandler(ActivatedAbilityArgs args);
	public static event AbilityEventHandler ActivatedAbility;
	public static event AbilityEventHandler NewAbility;

	private bool coolingDown;
	public bool AbilityAvailable {
		get {
			if(this.abilityAction.CooldownGroup > 0) {
				return AbilityBank.Instance.cdGroup.IsStrengthAvailable(abilityAction.CooldownGroup,this.abilityAction.AbilityStrengthLevel);
			} else {
				return coolingDown;
			}
		}
	}

	public AbilityStrength AbilityPower {
		get {
			return this.AbilityData.AbilityStrengthLevel;
		}
	}

	private float cdElapsed;
	public float CooldownPercent {
		get {
			float total = this.CooldownLength;
			if(this.abilityAction.CooldownGroup > 0) {
				return AbilityBank.Instance.cdGroup.CDPercent(this.abilityAction.CooldownGroup);
			} else {
				return cdElapsed / total;
			}
		}
	}

	private AbilityAction abilityAction;
	public AbilityAction AbilityData {
		get {
			if(abilityAction == null)
				abilityAction = DataHandler.AbilityActions[AbilityPowerName];
			return abilityAction; 
		}
	}

	private float timeUntilAvailable;
	public float CooldownLength {
		get { return abilityAction.Cooldown; }
	}

	private bool isActive = false;
	public bool Active {
		get { return isActive; }
	}

	protected virtual void PlaySound() {
		SoundManager.PlaySFX(this.gameObject,AudioBank.Fetch(this.AbilityData.AudioName));
	}

	protected virtual void Start() {
        if (AbilityBank.Instance == null) {
            print("RABBLE RABBLE");
        }
        AbilityBank.Instance.RegisterAbility(this);
        StartCoroutine("WaitForLoad");

	}

    IEnumerator WaitForLoad() {
        while (BossPool.MainBoss == null) {
            yield return 0;
        }
        abilityAction = DataHandler.AbilityActions[AbilityPowerName];
        if (NewAbility != null && BossPool.MainBoss.XMLName == abilityAction.BossOwner) {
            NewAbility(new ActivatedAbilityArgs(this));
        }
    }

	public Ability GetAbilityStrength(AbilityStrength strength) {
		StrongAbilityName = abilityAction.StrongAbility;
		WeakAbilityName = abilityAction.WeakAbility;
		bool hasStrength = StrongAbilityName != "" || WeakAbilityName != "";
		if(strength == AbilityStrength.Strong && hasStrength) {
			return AbilityBank.Instance.Abilities[StrongAbilityName];
		} else if(strength == AbilityStrength.Weak && hasStrength) {
			return AbilityBank.Instance.Abilities[WeakAbilityName];
		}
		return this;
	}

	public virtual bool IsSelectTargeted() {
		return false;
	}

	public abstract bool IsAoeTargeted(); // If it is, it's placed at a spot as opposed to a place
	public abstract void Activate(RPGCharacter character);
	public abstract void Activate(Vector3 tar);

	public void SelectOverlayOff() {
		AbilityBank.Instance.cdGroup.IconOverlayDeactivate(AbilityData.CooldownGroup);
//		abilityCooldownOverlay.spriteName = abilityCooldownOverlay.spriteName.Substring(0,abilityCooldownOverlay.spriteName.Length - "_select".Length);
	}
	
	public void SelectOverlayOn() {
		AbilityBank.Instance.cdGroup.IconOverlayActivate(AbilityData.CooldownGroup);
//		abilityCooldownOverlay.spriteName = string.Format("{0}{1}",abilityCooldownOverlay.spriteName,"_select");
	}

	protected IEnumerator Cooldown() {
		if(this.abilityAction.CooldownGroup > 0) {
			BossController.Instance.StartCoroutine("GlobalCooldown");
			CooldownGroupHandler.Instance.StartCoroutine("GCD");
//			AbilityBank.Instance.cdGroup.StartGroupCooldown(this.abilityAction.CooldownGroup,this.abilityAction.Cooldown);
			if(ActivatedAbility != null)
				ActivatedAbility(new ActivatedAbilityArgs(this));
			yield break;
		} else {
			// This is a unique cooldown, for raider abilities.
			coolingDown = true;
			cdElapsed = 0f;
			while(cdElapsed < this.abilityAction.Cooldown) {
				cdElapsed += Time.deltaTime;
				yield return 0;
			}
			coolingDown = false;
		}
	}
}
