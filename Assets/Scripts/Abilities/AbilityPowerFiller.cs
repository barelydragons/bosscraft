﻿using UnityEngine;
using System.Collections;

public class AbilityPowerFiller : MonoBehaviour {
	public UISprite border, borderBg;
	private Color white, blue, green, red;

	private void Awake() {
		white = Color.white;
		blue = Color.blue;
		green = Color.green;
		red = Color.red;
	}

	public float Fill {
		set {
			float v = value;
			if(v <= 1.0) {
				border.color = Color.Lerp(white,blue,v);
				borderBg.color = Color.black;
				border.fillAmount = v;
			} else if(v <= 2.0) {
				border.color = Color.Lerp(blue,green,v - 1f);
				borderBg.color = Color.blue;
				border.fillAmount = v - 1f;
			} else {
				border.color = Color.Lerp(green,red,v - 2f);
				borderBg.color = Color.green;
				border.fillAmount = v - 2f;
			}
		}
	}
}
