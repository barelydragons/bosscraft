﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CooldownGroup {
	public int group_;
	public List<float> times;
    private int abilityLevel = 0;
    public int AbilityLevel {
        get { return abilityLevel; }
    }

	public AbilityStrength AvailablePower {
		get {
			AbilityStrength ret = AbilityStrength.Inactive;
			if(elapsed > times[2]) {
				ret = AbilityStrength.Strong;
			} else if(elapsed > times[1]) {
				ret = AbilityStrength.Normal;
			} else if(elapsed > times[0]) {
				ret = AbilityStrength.Weak;
			}
			return ret;
		}
	}
	public bool AbilityAvailable(AbilityStrength str) {
        if (str == AbilityStrength.Strong) {
            return PowerAvailable(3);
        } else if (str == AbilityStrength.Normal) {
            return PowerAvailable(2);
        } else {
            return PowerAvailable(1);
        }
	}

	public float TotalCooldown {
		get {
			float ret = 0;
			foreach(float t in times) {
				ret += t;
			}
			return ret;
		}
	}
	
	private float elapsed;
	
	public void AddAbility(AbilityStrength str, float time) {
		if(str == AbilityStrength.Weak) {
			times[0] = time;
		} else if(str == AbilityStrength.Normal) {
			times[1] = time;
		} else if(str == AbilityStrength.Strong) {
			times[2] = time;
		}
	}

	public CooldownGroup(int id) {
		this.group_ = id;
		times = new List<float>();
		times.Add(0f);
		times.Add(0f);
		times.Add(0f);
		elapsed = 0f;
	}

	public CooldownGroup(int id, float time1, float time2, float time3) {
		this.group_ = id;
		times = new List<float>();
		times.Add(time1);
		times.Add(time2);
		times.Add(time3);
		elapsed = 0f;
	}

	public void IncrementTime(float aTime) {
		elapsed += aTime;
        float t = times[Mathf.Min(2, abilityLevel)];// abilityLevel == 0 ? 0f : times[abilityLevel - 1];
        if (elapsed >= t) {
            if (abilityLevel < 3) {
                elapsed = 0;
                abilityLevel++;
            } else {
                elapsed = t;
                abilityLevel = 3;
            }
        }
	}

    public bool PowerAvailable(int powerLevel) {
        return abilityLevel >= powerLevel;
    }

	public void ActivateAbility(AbilityStrength str) {
        if (str == AbilityStrength.Normal) {
            if (abilityLevel == 2) {
                TimeToPowerLevel(0);
            } else {
                TimeToPowerLevel(1);
            }
        } else if (str == AbilityStrength.Strong) {
            TimeToPowerLevel(0);
        } else {
            TimeToPowerLevel(abilityLevel - 1);
        }
	}

    public void TimeToPowerLevel(int level) {
        elapsed = 0f; // Change this current cooldown percentage * times[level - 1] if cooldown stays relative when it's used
        abilityLevel = level;
    }

	public float AdditiveCooldown(int time) {
		float tot = 0;
		for(int i = 0; i < time + 1; i++) {
			tot += times[i];
		}
		return tot;
	}

	// Returns percent progress towards the next available cooldown
	public float CurrentCooldownPercentage() {
        if (abilityLevel >= 3) {
            return 1f;
        }
        return elapsed / times[abilityLevel];
	}

    public float PowerNodePercentage() {
        float CDP = CurrentCooldownPercentage();
        // 3 fills all 3 power nodes.
        if (elapsed >= AdditiveCooldown(2))
            return 3f;
        if (elapsed >= AdditiveCooldown(1))
            return 2f + CDP;
        if (elapsed >= AdditiveCooldown(0))
            return 1f + CDP;
        return CDP;
    }

    public float FillPercentage() {
        float ret = this.abilityLevel + this.CurrentCooldownPercentage();
        return ret;
    }

	public override int GetHashCode() {
		return group_;
	}
}
// Handles the cooldowns of abilities and their groups.
// All the firebreath abilities are 1 group
// 0 is a special reserved value for abilities that don't belong to a group
// 4 ability groups
public class CooldownGroupHandler : MonoBehaviour {
	private Dictionary<int,CooldownGroup> coolDownGroups;
	public static CooldownGroupHandler Instance;
	public List<UISprite> cooldownOverlays;
	public List<UISprite> abilityIcons;
	public List<AbilityPowerFiller> abilityPower;
	
	public void Awake() {
		Instance = this;
        coolDownGroups = new Dictionary<int, CooldownGroup>();
		foreach(AbilityPowerFiller pf in abilityPower) {
			pf.Fill = 0f;
		}
		foreach(UISprite s in cooldownOverlays) {
			s.fillAmount = 0f;
		}
		Ability.ActivatedAbility += AbilityActivated;
		Game.OnBattleBegin += GameBegin;
		Game.OnBattleEnd += GameEnd;
		Ability.NewAbility += RegisterAbility;
	}

	void GameEnd(GameStateArgs args) {
		Ability.ActivatedAbility -= AbilityActivated;
		Ability.NewAbility -= RegisterAbility;
		Game.OnBattleEnd -= GameEnd;
		Game.OnBattleBegin -= GameBegin;
	}

	public void GameBegin(GameStateArgs args) {
		StartCoroutine("CooldownTracking");
	}

	IEnumerator CooldownTracking() {
		while(true) {
			foreach(CooldownGroup cdg in coolDownGroups.Values) {
				cdg.IncrementTime(Time.deltaTime);
                abilityPower[cdg.group_ - 1].Fill = cdg.FillPercentage();// +cdg.AbilityLevel;
				if(BossController.Instance.GlobalCooling) {
					continue;
				}
				cooldownOverlays[cdg.group_ - 1 ].fillAmount = cdg.CurrentCooldownPercentage();
			}
			yield return 0;
		}
	}

	public void RegisterAbility(ActivatedAbilityArgs args) {
		if(coolDownGroups == null) {
			coolDownGroups = new Dictionary<int, CooldownGroup>();
		}
		int cdGroupNum = args.ability.AbilityData.CooldownGroup;
		if(cdGroupNum == 0)
			return;
		CooldownGroup CDG = null;
		if(coolDownGroups.ContainsKey(cdGroupNum)) {
			CDG = coolDownGroups[cdGroupNum];
		} else if(CDG == null) {
			CDG = new CooldownGroup(cdGroupNum);
			coolDownGroups.Add(cdGroupNum,CDG);
		}
		CDG.AddAbility(args.ability.AbilityPower,args.ability.CooldownLength);
	}

	public void AbilityActivated(ActivatedAbilityArgs args) {
		int cdGroupNum = args.ability.AbilityData.CooldownGroup;
		CooldownGroup CDG = coolDownGroups[cdGroupNum];
		CDG.ActivateAbility(args.ability.AbilityPower);
	}

	public float CDPercent(int group) {
		return coolDownGroups[group].CurrentCooldownPercentage();
	}

	public void IconOverlayDeactivate(int icon) {
		UISprite abilityCooldownOverlay = abilityIcons[icon - 1];
		abilityCooldownOverlay.color = Color.white;
//		abilityCooldownOverlay.spriteName = abilityCooldownOverlay.spriteName.Substring(0,abilityCooldownOverlay.spriteName.Length - "_select".Length);
	}

	public void IconOverlayActivate(int icon) {
		UISprite abilityCooldownOverlay = abilityIcons[icon - 1];
		abilityCooldownOverlay.color = Color.green;
//		abilityCooldownOverlay.spriteName = string.Format("{0}{1}",abilityCooldownOverlay.spriteName,"_select");
	}

	public bool IsStrengthAvailable(int group, AbilityStrength abiStr) {
		if(coolDownGroups.ContainsKey(group)) {
			return coolDownGroups[group].AbilityAvailable(abiStr);
		}
		return false;
	}

	public CooldownGroup CDGroup(int group) {
		if(coolDownGroups.ContainsKey(group)) {
			return coolDownGroups[group];
		}
		return null;
	}

	public IEnumerator GCD() {
		float t = 0;
		float lim = Rules.Instance.RuleDict["GCD"];
		while(t <= lim) {
			foreach(UISprite s in cooldownOverlays) {
				s.fillAmount = t / lim;
			}
			t += Time.deltaTime;
			yield return 0;
		}
	}
}
