﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

public class AbilityAction {
	// Corresponds to RPG character values.
	[XmlAttribute("name")]
	public string Name;

	[XmlElement("Cooldown")]
	public float Cooldown;

	[XmlElement("Duration")]
	public float Duration;

	[XmlElement("CooldownGroup")]
	public int CooldownGroup;

	[XmlElement("Damage")]
	public float Damage;

	[XmlElement("Heal")]
	public float Heal;

	[XmlElement("ManaCost")]
	public float ManaCost;

	[XmlElement("Radius")] // For aoe abilities only,
	public float Radius;

	[XmlElement("Speed")] // How often it deals damage per second, etc. (for aoe and things with duration > 0...)
	public float Speed;

	[XmlElement("IconName")] // Name of icon in GUI atlas
	public string IconName;

	[XmlElement("StrongAbility")] // Name of strong ability, if applicable
	public string StrongAbility;

	[XmlElement("WeakAbility")]
	public string WeakAbility;

	[XmlElement("Tooltip")]
	public string Tooltip = "";

	[XmlElement("AudioClipName")]
	public string AudioName;

	[XmlElement("PowerLevel")]
	public int PowerLevel;

    [XmlElement("BossName")] // blank if no raid ability
    public string BossOwner;

	public AbilityStrength AbilityStrengthLevel {
		get {
			if(this.PowerLevel == 0) {
				return AbilityStrength.Weak;
			} else if(this.PowerLevel == 1) {
				return AbilityStrength.Normal;
			} else if(this.PowerLevel == 2) {
				return AbilityStrength.Strong;
			} else {
				return AbilityStrength.Inactive;
			}
		}
	}
}