﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

// Code is from http://wiki.unity3d.com/index.php?title=Saving_and_Loading_Data:_XmlSerializer
[XmlRoot("AbilityCollection")]
public class AbilityContainer {
	[XmlArray("Abilities"), XmlArrayItem("Ability")]
	public List<AbilityAction> AbilityActions = new List<AbilityAction>();

	public static AbilityContainer Load(string path) {
		XmlSerializer serializer = new XmlSerializer(typeof(AbilityContainer));
		using(FileStream stream = new FileStream(path, FileMode.Open)) {
			return serializer.Deserialize(stream) as AbilityContainer;
		}
	}

	public static AbilityContainer LoadFromText(string text) {
		XmlSerializer serializer = new XmlSerializer(typeof(AbilityContainer));
		return serializer.Deserialize(new StringReader(text)) as AbilityContainer;
	}

	public void Save(string path) {
		XmlSerializer serializer = new XmlSerializer(typeof(AbilityContainer));
		using(FileStream stream = new FileStream(path, FileMode.Create)) {
			serializer.Serialize(stream,this);
		}
	}


}
