using UnityEngine;
using System.Collections;

public class RocketBarrage : Ability {
	public RPGCharacter armyMan;
	public AudioClip clip;

	public override bool IsAoeTargeted() {
		return false;
	}

	public override void Activate(RPGCharacter character) {
		if(AbilityAvailable || armyMan.Target == null)
			return;
		StartCoroutine("Barrage");
		StartCoroutine("Cooldown");
	}
	
	public void Activate(RPGCharacter character, Vector3 target) {
		Activate(character);
	}
	
	public override void Activate(Vector3 tar) {
		throw new System.NotImplementedException();
	}

	IEnumerator Barrage() {
		float t = 0;
		while(t < this.AbilityData.Duration) {
			if(armyMan.Target == null) {
				yield return new WaitForSeconds(this.AbilityData.Speed);
				continue;
			}
			t += this.AbilityData.Speed;
			ParticleStream.Instance.RocketFire(this.transform,armyMan.Target.transform);
			SoundManager.PlaySFX(this.gameObject,clip);
			armyMan.Target.AddDamage((int)this.AbilityData.Damage);
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
	}
}
