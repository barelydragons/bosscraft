﻿using UnityEngine;
using System.Collections;

public class LaserEyes : Ability {
	private RPGCharacter _target;
	public RPGCharacter user;
	public WSP_LaserBeamWS laser1, laser2;
	public AnimationClip clip;
	public override void Activate(Vector3 position) {
		
	}
	
	public override bool IsSelectTargeted() {
		return true;
	}
	
	public override bool IsAoeTargeted() {
		return false;
	}

	void LaserOn() {
		laser1.LaserCanFire = true;
		laser1.LaserBeamActive = true;
		laser2.LaserCanFire = true;
		laser2.LaserBeamActive = true;
	}

	void LaserOff() {
		laser1.LaserCanFire = false;
		laser1.LaserBeamActive = false;
		laser2.LaserCanFire = true;
		laser2.LaserBeamActive = true;
	}
	
	public override void Activate(RPGCharacter target) {
		print ("Active");
        if (user == null) {
            user = GameObject.Find("robot").GetComponent<RPGCharacter>();
        }
		if(AbilityAvailable)
			return;
		print ("GO");
		_target = user.Target;
		if(_target == null)
			return;
		laser1.CurrentTarget = _target.damageCenter;
		laser2.CurrentTarget = _target.damageCenter;
		LaserOn();
		StartCoroutine(LightTheBastardsOnFire());
		StartCoroutine("Cooldown");
	}

	IEnumerator LightTheBastardsOnFire() {
		print ("LASER EYES");
		float timer = 0;
		while(timer < clip.length) {
			timer += this.AbilityData.Speed;
			laser1.FireLaser();
			laser2.FireLaser();
			_target.AddDamage((int)this.AbilityData.Damage);
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
		LaserOff();
	}
}
