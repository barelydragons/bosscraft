﻿using UnityEngine;
using System.Collections;

public class MonkeyFear : Ability {
    public ParticleSystem psys;
    public override bool IsAoeTargeted() {
        return false;
    }

    public override void Activate(RPGCharacter character) {
        Activate(Vector3.zero);
    }

    public override void Activate(Vector3 tar) {
        if (AbilityAvailable)
            return;
        SoundManager.PlaySFX(this.gameObject, AudioBank.Fetch(this.AbilityData.AudioName));
        StartCoroutine(FearRoutine());
        StartCoroutine(Cooldown());
    }

    IEnumerator FearRoutine() {
        if (Vector3.Distance(this.transform.position, BossPool.MainBoss.transform.position) > this.AbilityData.Radius) {
            yield return 0;
        }
        psys.Emit(1);
        BossPool.MainBoss.Frozen = true;
        BossController.Instance.TriggerGCD(this.AbilityData.Duration);//.MainBoss.("GlobalCooldownOverride", this.AbilityData.Duration);
        print("Freezing boss");
        yield return new WaitForSeconds(this.AbilityData.Duration);
        print("Unfreezing boss");
        BossPool.MainBoss.Frozen = false;
        yield return 0;
    }
}
