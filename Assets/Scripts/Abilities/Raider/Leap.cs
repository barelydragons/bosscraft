using UnityEngine;
using System.Collections;

public class Leap : Ability {
	private RPGCharacter owner;
	public AudioClip landing;
	public ParticleSystem pSys;

	public override bool IsAoeTargeted() {
		return false;
	}
	
	public override void Activate(RPGCharacter character) {
		// null for this class
		throw new System.NotImplementedException();
	}

	public void Activate(RPGCharacter character, Vector3 target) {
		if(AbilityAvailable)
			return;
		// Animation stuff later, but other things as well....
		owner = character;
		character.AddDamage((int)this.AbilityData.Damage);
		StartCoroutine("LeapRoutine",target);
		StartCoroutine("Cooldown");
	}

	float NextY(float startingHeight, float apexHeight, float t) {
		float B = (apexHeight - startingHeight) / 4f;
		float A = -B;
		float C = startingHeight;
		return A * t * t + B * t + C;
	}

	IEnumerator LeapRoutine(Vector3 target) {
		owner.Frozen = true;
		float tarHeight = owner.transform.position.y + 90f;
		Vector3 start = owner.transform.position;
		float leapTime = this.AbilityData.Duration;
		float timer = 0f;
		while(timer <= leapTime) {
			float elapsed = timer / leapTime;
			float nextX = Mathf.Lerp(start.x,target.x,elapsed);
			float nextZ = Mathf.Lerp (start.z,target.z,elapsed);
//			float nextY = owner.transform.position.y;
			float nextY = NextY (start.y,tarHeight,timer);//start.y - timer * ( 3 * start.y - 4 * tarHeight + start.y) + 2 * timer * timer *(start.y - 2 * tarHeight +start.y);
//			if(elapsed >= .5f) {
//
//			} else {
//
//			}
			owner.rigidbody.MovePosition(new Vector3(nextX,nextY,nextZ));
			timer += Time.deltaTime;
			yield return 0;
		}
		SoundManager.PlaySFX(this.gameObject,landing,false,0f,5f);
		owner.rigidbody.MovePosition(target);
		owner.rigidbody.velocity = Vector3.zero;
		owner.transform.position = new Vector3(owner.transform.position.x,4.5f,owner.transform.position.z); // 4.5 is the y plane of raiders
		pSys.gameObject.transform.position = target - new Vector3(0,5f);
		pSys.Emit(50);
		owner.Frozen = false;
	}
	
	public override void Activate(Vector3 tar) {
		// null for this class
		throw new System.NotImplementedException();
	}
}
