using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BubbleShield : Ability {
	// Use this for initialization
	public GameObject sphere;
	private List<Raider> invincibleRaiders;
	private Vector3 startScale;
	public AudioClip clip;
	protected override void Start () {
		base.Start();

        this.transform.parent = null;
		startScale = sphere.transform.localScale;
		sphere.SetActive(false);
//		Activate(this.transform.position);
	}
	
	public override bool IsAoeTargeted() {
		return false;	
	}
	
	public override void Activate(RPGCharacter character) {
		Activate(character.transform.position);	
	}
	
	public override void Activate(Vector3 pos) {
		if(AbilityAvailable)
			return;
		SoundManager.PlaySFX(this.gameObject,clip);
		transform.position = pos;
		sphere.transform.scaleFrom(.5f,new Vector3(0.1f,0.1f,0.1f));
		sphere.SetActive(true);
		StartCoroutine(InvincibleRoutine());
		StartCoroutine("Cooldown");
	}
	
	IEnumerator InvincibleRoutine() {
		invincibleRaiders = new List<Raider>();
		yield return new WaitForSeconds(this.AbilityData.Duration);
		foreach(Raider r in invincibleRaiders) {
			r.Invulnerable = false;
		}
		invincibleRaiders = new List<Raider>();
		sphere.transform.scaleTo(.5f,new Vector3(0.1f,0.1f,0.1f));
		SoundManager.PlaySFX(this.gameObject,clip);
		yield return new WaitForSeconds(.5f);
		sphere.transform.localScale = startScale;
		sphere.SetActive(false);
	}

	void OnTriggerEnter(Collider c) {
		if(c.transform.tag == "raider") {
			Raider r = c.GetComponent<Raider>();
			r.Invulnerable = true;
			invincibleRaiders.Add(r);
		}
	}
	
	void OnTriggerExit(Collider c) {
		if(c.transform.tag == "raider") {
			Raider r = c.GetComponent<Raider>();
			r.Invulnerable = false;
			// Not going to bother removing them from the collection here as I would rather set false to false, then iterate through the list of raiders multiple times.
		}
	}
}
