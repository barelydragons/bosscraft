﻿using UnityEngine;
using System.Collections;

public class HeliToggle : Ability {
    public Helicopter heli;
    public override bool IsAoeTargeted() {
        return false;
    }

    public override void Activate(RPGCharacter character) {
        if (heli == null) {
            heli = GameObject.Find("Helicopter(Clone)").GetComponent<Helicopter>();
        }
        heli.ToggleMode();
    }

    public override void Activate(Vector3 tar) {
        if (heli == null) {
            heli = GameObject.Find("Helicopter(Clone)").GetComponent<Helicopter>();
        }
        heli.ToggleMode();
    }
}
