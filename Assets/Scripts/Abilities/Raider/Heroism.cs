using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Heroism : Ability {
	private RPGCharacter owner;
	public AudioClip clip;
	public ParticleSystem pSys;
//	private Transform target;
	
	public override bool IsAoeTargeted() {
		return false;
	}
	
	
	protected override void Start() {
		base.Start();
		this.gameObject.SetActive(false);
	}

	public override void Activate(RPGCharacter character) {
		if(AbilityAvailable)
			return;
		SoundManager.PlaySFX(this.gameObject,clip);
		this.gameObject.SetActive(true);
		this.transform.position = character.transform.position;
		this.transform.parent = character.transform;
		StartCoroutine("HeroismRoutine");
		StartCoroutine("Cooldown");
	}
	
	public void Activate(RPGCharacter character, Vector3 target) {
		Activate(character);
	}
	
	float NextY(float startingHeight, float apexHeight, float t) {
		float B = (apexHeight - startingHeight) / 4f;
		float A = -B;
		float C = startingHeight;
		return A * t * t + B * t + C;
	}
	
	IEnumerator HeroismRoutine() {
		List<Raider> raiders = RaiderPool.AllRaiders;
		float modifier = this.AbilityData.Damage;
		foreach(Raider r in raiders) {
			r.AttackSpeed /= modifier;
		}
		pSys.gameObject.SetActive(true);
		pSys.emissionRate = 24f;
//		float time = 0f;
		yield return new WaitForSeconds(this.AbilityData.Duration);
//		while(time < this.AbilityData.Duration) {
//			time += Time.deltaTime;
//
//			yield return 0;
     //		}
		
		foreach(Raider r in raiders) {
			r.AttackSpeed *= modifier; // hopefully rounding errors are minimized.
		}
		pSys.emissionRate = 0f;
		pSys.gameObject.SetActive(false);
	}

	public override void Activate(Vector3 tar) {
		throw new System.NotImplementedException();
	}
}
