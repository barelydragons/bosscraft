using UnityEngine;
using System.Collections;

public class HealerPulse : Ability {
	public ParticleSystem particleSys;
	// Use this for initialization
	protected override void Start () {
		base.Start();
	}

	public override bool IsAoeTargeted() {
		return false;	
	}
	
	public override void Activate(RPGCharacter character) {
		Activate(character.transform.position);	
	}

	public override void Activate(Vector3 pos) {
		if(AbilityAvailable)
			return;
		transform.position = pos;
		StartCoroutine(HealRoutine());
		StartCoroutine("Cooldown");
	}

	IEnumerator HealRoutine() {
		int bursts = 3; // TODO: Make this not a magic number
		for(int i = 0; i < bursts; i++) {
			particleSys.Emit(200);
			Collider[] colls = Physics.OverlapSphere(transform.position,this.AbilityData.Radius);
			foreach(Collider item in colls) {
				if(item.tag == "raider") {
					RPGCharacter raider = item.GetComponent<RPGCharacter>();
					raider.Heal((int)this.AbilityData.Heal);
				}
			}
			yield return new WaitForSeconds(this.AbilityData.Speed);
		}
	}


	
	// Update is called once per frame
	void Update () {
	
	}
}
