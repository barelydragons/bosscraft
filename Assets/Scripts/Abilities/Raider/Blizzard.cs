﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Child of snowman
public class Blizzard : Ability {
    private Component[] psyses;
    void Awake() {
        psyses = this.GetComponentsInChildren(typeof(ParticleSystem));
        foreach (ParticleSystem p in psyses) {
            p.enableEmission = false;
        }
    }

    public override bool IsAoeTargeted() {
        return false;
    }

    public override void Activate(RPGCharacter character) {
        Activate(character.transform.position);	
    }

    public override void Activate(Vector3 tar) {
        if (AbilityAvailable)
            return;
        StartCoroutine("Cooldown");
        StartCoroutine(BlizzardRoutine());
    }

    private IEnumerator BlizzardRoutine() {
        foreach (ParticleSystem p in psyses) {
            p.enableEmission = true;
        }
        float timer = 0f;
        while (timer < this.AbilityData.Duration) {
            timer += this.AbilityData.Speed;
            foreach (BossMinion b in BossPool.EveryBossActor()) {
                if (b == null || !b.Alive)
                    continue;
                float dist = Vector3.Distance(b.damageCenter.position, transform.position);
                if (dist <= this.AbilityData.Radius) {
                    b.AddDamage((int)this.AbilityData.Damage);
                }
            }
            yield return new WaitForSeconds(this.AbilityData.Speed);
        }
        foreach (ParticleSystem p in psyses) {
            p.enableEmission = false;
        }
    }
}
