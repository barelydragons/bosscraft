﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSpawner : MonoBehaviour {
    public static GameSpawner Instance;
    public GameObject snowman, monkey, army, heli, wizard, robot, superhero, lego;
    public GameObject jack, bear;

    // Check UI Pick object to set which actually spawn. That game object persists through scenes so this can stay and link stuff in scene.

    private List<GameObject> toSpawn;
    private List<string> toSpawnNames;

    public List<Transform> raidSpawnPoints;
    public Transform bossSpawnPoint;

	// Use this for initialization
	void Awake () {
        Instance = this;
        StartCoroutine("SpawnRoutine");
    }

    IEnumerator SpawnRoutine() {
        while (BossController.Instance == null || RaidController.Instance == null) {
            yield return 0;
        }
        toSpawn = new List<GameObject>();
        toSpawnNames = new List<string>();
        if (UIPick.Instance.snow) {
            toSpawn.Add(snowman);
            toSpawnNames.Add("SnowMan");
        }
        if (UIPick.Instance.monkey) {
            toSpawn.Add(monkey);
            toSpawnNames.Add("MonkeyTank");
        }
        if (UIPick.Instance.army) {
            toSpawn.Add(army);
            toSpawnNames.Add("armyMan");
        }
        if (UIPick.Instance.heli) {
            toSpawn.Add(heli);
            toSpawnNames.Add("Helicopter");
        }
        if (UIPick.Instance.wiz) {
            toSpawn.Add(wizard);
            toSpawnNames.Add("wizard");
        }
        if (UIPick.Instance.rob) {
            toSpawn.Add(robot);
            toSpawnNames.Add("robot");
        }
        if (UIPick.Instance.super) {
            toSpawn.Add(superhero);
            toSpawnNames.Add("Superhero");
        }
        if (UIPick.Instance.lego) {
            toSpawn.Add(lego);
            toSpawnNames.Add("DrPhilGoode");
        }
        for (int i = 0; i < 5; i++) {
            Transform t = raidSpawnPoints[i];
            if (PhotonNetwork.inRoom && RoomHandler.Instance.raidPlayer == PhotonNetwork.player) {
                PhotonNetwork.Instantiate(toSpawn[i].name, t.position, Quaternion.identity, 0);
            } else if(!PhotonNetwork.inRoom) {
                Instantiate(toSpawn[i], t.position, Quaternion.identity);
            }
        }

        if (UIPick.Instance.jack) {
            if (PhotonNetwork.inRoom && RoomHandler.Instance.bossPlayer == PhotonNetwork.player) {
                print("Spawning Jack");
                PhotonNetwork.Instantiate(jack.name, bossSpawnPoint.position, Quaternion.identity, 0);
            } else if (!PhotonNetwork.inRoom) {
                GameObject b = (GameObject)Instantiate(jack, bossSpawnPoint.position, Quaternion.identity);
                b.transform.eularAnglesTo(.5f, new Vector3(0, 180f, 0), true);
            }
        } else {
            if (PhotonNetwork.inRoom && RoomHandler.Instance.bossPlayer == PhotonNetwork.player) {
                print("Spawning Bear");
                PhotonNetwork.Instantiate(bear.name, bossSpawnPoint.position, Quaternion.identity, 0);
            } else if (!PhotonNetwork.inRoom) {
                GameObject b = (GameObject)Instantiate(bear, bossSpawnPoint.position, Quaternion.identity);
                b.transform.eularAnglesTo(.5f, new Vector3(0, 180f, 0), true);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
