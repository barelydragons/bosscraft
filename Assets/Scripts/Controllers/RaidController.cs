﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaiderSelectedArgs {
	private RPGCharacter selectedCharacter;
	public RPGCharacter Selected {
		get { return selectedCharacter; }
	}

	public RaiderSelectedArgs(RPGCharacter selected) {
		selectedCharacter = selected;
	}
}

public class RaidController : MonoBehaviour {
	public delegate void RaiderSelectHandler(RaiderSelectedArgs args);
	public static event RaiderSelectHandler OnRaiderSelect;
	public static RaidController Instance;
	public List<RaidGroup> raidGroups = new List<RaidGroup>(5);
	public LayerMask GroundMask, RaiderMask, BossMask;
	public UISprite marqueeBoundingBox;
	public Camera nguiCamera;
	private PhotonView photonView;
	
	public bool IsRaidPlayer {
		get { 
			return this.photonView.isMine || (!PhotonNetwork.inRoom);
		}
	}

	public RaidGroup ActiveGroup; // This is just a null raid group, these are mono behaviours so they can be defined visually though it's not technically going to matter in the end

	private List<bool> raidGroupDown;
	public bool RaidDead {
		get {
			foreach(Raider r in RaiderPool.AllRaiders) {
				if(r == null)
					continue;
				if(r.Alive) {
					return false;
				}
			}
			return true;
		}
	}

	private bool moveDown, selectDown, marqueeOn;
	public bool blockInput = false;
	private Vector3 marqueeStart, marqueeEnd;

	void Awake() {
		photonView = this.GetComponent<PhotonView>();
		Game.OnUnpause += PauseToggle;
		Game.OnPause += PauseToggle;
		Game.OnBattleEnd += EndGame;

		RaidGroup[] rg = GameObject.FindObjectsOfType<RaidGroup>();
		raidGroups = new List<RaidGroup>(rg);
		marqueeBoundingBox = GameObject.Find("boundingBox").GetComponent<UISprite>();
		Instance = this;
		if(PhotonNetwork.inRoom) {
			if(this.GetComponent<PhotonView>().owner != PhotonNetwork.player) {
				this.enabled = false;
			}
		}
		Game.Instance.raidReady = true;
	}

	void EndGame(GameStateArgs args) {
		Game.OnUnpause -= PauseToggle;
		Game.OnPause -= PauseToggle;
		Game.OnBattleEnd -= EndGame;
	}

	// Use this for initialization
	void Start () {
		raidGroupDown = new List<bool>(new bool[] {false,false,false,false,false});
	}

	void actionInput(float moveRaider) {
		// Move group or raider
		if(moveRaider == 1f && !moveDown && ActiveGroup.IsClear == false) {
			moveDown = true;
			Ray clickRay = camera.ScreenPointToRay(Input.mousePosition);
			RaycastHit ghit,rhit,bhit;
			int groundMask = (1 << 8);
			int raidMask = (1 << 9);
			int bossMask = (1 << 10);
			bool groundHit = Physics.Raycast(clickRay, out ghit, 100f, groundMask);
			bool raidHit = Physics.Raycast(clickRay, out rhit, 100f, raidMask);
			bool bossHit = Physics.Raycast(clickRay, out bhit, 100f, bossMask);
			ActiveGroup.MoveHandler(ghit,groundHit,rhit,raidHit,bhit,bossHit);
		} else if(moveRaider == 0) {
			moveDown = false;
		}
	}

	void BeginMarquee() {
		marqueeOn = true;
		marqueeStart = Input.mousePosition;
		marqueeEnd = marqueeStart;
		Vector3 pos = Input.mousePosition;
			// Since the screen can be of different than expected size, we want to convert
			// mouse coordinates to view space, then convert that to world position.
		pos.x = Mathf.Clamp01(pos.x / Screen.width);
		pos.y = Mathf.Clamp01(pos.y / Screen.height);
		marqueeBoundingBox.transform.position = UICamera.mainCamera.ViewportToWorldPoint(pos);
		marqueeBoundingBox.width = 10;
		marqueeBoundingBox.height = 10;
		//marqueeBoundingBox.transform.position = nguiCamera.WorldToViewportPoint(Camera.main.ScreenToWorldPoint(marqueeStart));
	}

	void UpdateMarquee() {
		marqueeEnd = Input.mousePosition;
		float width = marqueeStart.x - marqueeEnd.x;
		float height = marqueeStart.y - marqueeEnd.y;
		Vector3 localScale = marqueeBoundingBox.transform.localScale;
		
		if((Mathf.Abs(width) <= 10 && Mathf.Abs(height) < 10) || !marqueeOn) {
			marqueeBoundingBox.enabled = false;
			return;
		} else {
			marqueeBoundingBox.enabled = true;
		}

		if (width > 0) {
			localScale.x = -1f;
		} else {
			localScale.x = 1f;
		}
		if (height < 0) {
			localScale.y = -1f;
		} else {
			localScale.y = 1f;
		}
		marqueeBoundingBox.transform.localScale = localScale;
		marqueeBoundingBox.width = (int)Mathf.Abs(width);
		marqueeBoundingBox.height = (int)Mathf.Abs(height);
	}

	bool InRange(float val, float v1, float v2) {
		if(v2 < v1) {
			float t = v1;
			v1 = v2;
			v2 = t;
		}
		return (v1 < val && val < v2);
	}

	void MarqueeOff(bool additive) {
		marqueeOn = false;
		marqueeEnd = Input.mousePosition;
		
		float width = marqueeStart.x - marqueeEnd.x;
		float height = marqueeStart.y - marqueeEnd.y;

		if(!(Mathf.Abs(width) < .5f && Mathf.Abs(height) < .5f)) {
			if(!additive) {
				Deselect();
			}
			foreach(RPGCharacter r in RaiderPool.AllRaiders) {
				if(r == null)
					continue;
				Vector3 p = Camera.main.WorldToScreenPoint(r.transform.position);
				if(InRange (p.x,marqueeStart.x,marqueeEnd.x) && InRange (p.y,marqueeStart.y,marqueeEnd.y)) {
					if(!additive || !ActiveGroup.raiders.Contains(r)) {
						ActiveGroup.Add(r);
						((Raider)r).SelectedByController();
					}
				}
			}
			ActiveGroup.Select();
		}
	}

	void PauseToggle(GameStateArgs args) {
		if(args.NextState == GameState.Paused)
			blockInput = true;
		else
			blockInput = false;
	}

	// This is the big nasty input handler function!  Yay.... it had to be somewhere
	void Update () {
		if(blockInput)
			return;
		actionInput(Input.GetAxisRaw("MoveRaider"));
		bool additiveSelect = Input.GetAxisRaw("AdditiveSelect") == 1f ? true : false;

		bool raidDown = Input.GetButtonDown("SelectRaider");
		bool raidUp = Input.GetButtonUp("SelectRaider");
		bool selectRaider = raidDown || raidUp;
		if(!marqueeOn && raidDown) {
			BeginMarquee();
		} else if(marqueeOn && raidUp) {
			MarqueeOff(additiveSelect);
		} else {
			UpdateMarquee();
		}

		if(selectRaider && !selectDown) {
			selectDown = true;
			Ray clickRay = camera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(clickRay, out hit, 300f, RaiderMask)) {
				if(hit.collider.tag == "raider") {
					RPGCharacter raider = hit.collider.GetComponent<RPGCharacter>();
					if(!additiveSelect) {
						Deselect();
						ActiveGroup.Add(raider);
						((Raider)raider).SelectedByController();
					} else {
						if(!ActiveGroup.raiders.Contains(raider)) {
							ActiveGroup.Add(raider);
							((Raider)raider).SelectedByController();
						} else {
							ActiveGroup.raiders.Remove(raider);
							((Raider)raider).DeSelectedByController();
						}
					}
					ActiveGroup.Select();
				} else if(!additiveSelect) {
					Deselect();
				}
			}
		} else if(!selectRaider) {
			selectDown = false;
		}

		// Select or assign raid groups
		for(int i = 0; i < 5; i++) {
			float select = Input.GetAxisRaw(string.Format("Raid{0:D}",i + 1));
			if(select == 1f && !raidGroupDown[i]) {
				if(!additiveSelect) {
					Deselect();
					ActiveGroup.Add(RaiderPool.AllRaiders[i]);
				} else {
					// This seems expensive...
					foreach(RPGCharacter c in raidGroups[i]) {
						if(!ActiveGroup.raiders.Contains(c)) {
							ActiveGroup.Add(c);
						}
					}
				}
				ActiveGroup.Select();
			} else if(select != 1f) {
				raidGroupDown[i] = false;
			}
		}

		if(ActiveGroup.raiders.Count == 1 && Input.GetButtonDown("RaiderActive")) {
			Raider r = (Raider)ActiveGroup.raiders[0];
			if(r.RaiderType == RaiderType.Melee && r.Alive) {// leap move
				AbilityActivator(r);
			} else if(r.Alive) {
				AbilityActivator(r);
			}
		}
	}

	void AbilityActivator(Raider r) {
		if(PhotonNetwork.inRoom) {
			this.photonView.RPC("ActivateRaiderAbility",PhotonTargets.All,r.CharID);
		} else {
			ActivateRaiderAbility(r.CharID);
		}
	}

	[RPC]
	public void ActivateRaiderAbility(int charid) {
		Raider r = (Raider)RPGCharacter.IDLookup[charid];
		r.ActivateAbility();
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if(stream.isWriting) {
			stream.SendNext(transform.position);
		} else {
			transform.position = (Vector3)stream.ReceiveNext();
		}
	}

	void Deselect() {
		ActiveGroup.Clear();
		if(OnRaiderSelect != null) {
			OnRaiderSelect(new RaiderSelectedArgs(null));
		}
	}
}