﻿using UnityEngine;
using System.Collections;

public class CamPivot : MonoBehaviour {
	public float speed;
	public Transform pivotPoint;

	public void Start() {
		Game.OnBattleBegin += Begin;
		Game.OnBattleEnd += GameEnd;
	}

	void Begin(GameStateArgs args) {
		StartCoroutine(Pivot());
	}

	IEnumerator Pivot() {
		while(true) {
            float mod = 0f;
            if (Input.GetKey(KeyCode.A)) {
                mod = 1f;
            }
            if (Input.GetKey(KeyCode.D)) {
                mod = -1f;
            }
			this.transform.RotateAround(pivotPoint.position, Vector3.up, mod * speed * Time.deltaTime);
			yield return 0;
		}
	}

	void GameEnd(GameStateArgs args) {
		Game.OnBattleBegin -= Begin;
		Game.OnBattleEnd -= GameEnd;
	}

}
