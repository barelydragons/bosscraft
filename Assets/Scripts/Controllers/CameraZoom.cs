﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour {
	public float maxFOV;
	public float minFOV;
	public float zoomSpeed = 5f;
	private bool letZoom = false;
	// Use this for initialization
	void Start () {
		Game.OnBattleBegin += Begin;
	}

	void Begin(GameStateArgs args) {
		letZoom = true;
	}

	// Update is called once per frame
	void Update () {
		if(letZoom == false)
			return;
		if (Input.GetAxis("Mouse ScrollWheel") < 0) {
			Camera.main.fieldOfView += zoomSpeed;
			if(Camera.main.fieldOfView > maxFOV) {
				Camera.main.fieldOfView = maxFOV;
			}
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0) {
			Camera.main.fieldOfView -= zoomSpeed;
			if(Camera.main.fieldOfView < minFOV) {
				Camera.main.fieldOfView = minFOV;
			}
		}
	}

	void OnDestroy() {
		Game.OnBattleBegin -= Begin;
	}
}
