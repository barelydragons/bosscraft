﻿using UnityEngine;
using System.Collections;

public class HideUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Backslash)) {
			UICamera.mainCamera.enabled = !UICamera.mainCamera.enabled;
		}
	}
}
