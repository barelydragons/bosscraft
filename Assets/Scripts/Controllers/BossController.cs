using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BossController : MonoBehaviour {
	public UISprite abilityStrength;
	public RPGCharacter target;
	public AbilityTrackHandler abiTrackHandler;
	private List<RPGCharacter> raiders;
	private int targetIndex;
	public Projector aoeProjector;
	public BossMinion MainBoss {
		get { 
			return BossPool.MainBoss;
		}
	}
	public List<BossMinion> Minions;
	public List<string> AbilityNames; // Can be up to 16 abilities
	private int currentAbilityTrack;
	private bool leftBump, rightBump;
	private Ability selectedAbility;
	private PhotonView photonView;

	public UILabel hpLabel;
	public static BossController Instance;
	private bool aoeTargetting, selectTargetting;
	private int activeAbility;
	public bool blockInput = true;
	private bool GCD = false;
    private bool pregame = true;
	public bool GlobalCooling {
		get { return GCD; }
	}

	public bool IsBossPlayer {
		get { 
			return photonView.isMine || (!PhotonNetwork.inRoom);
		}
	}

	void Awake () {
		Instance = this;
		Game.OnSetupEnd += PauseToggle;
		Game.OnUnpause += PauseToggle;
		Game.OnPause += PauseToggle;
		Game.OnBattleEnd += GameEnd;
		if(PhotonNetwork.inRoom) {
			this.transform.Rotate(new Vector3(90f,0,0));
		}
		abiTrackHandler = GameObject.Find("abilityTrackHandler").GetComponent<AbilityTrackHandler>();
		Game.Instance.bossReady = true;
		photonView = this.GetComponent<PhotonView>();
		if(PhotonNetwork.inRoom) {
			if(!IsBossPlayer) {
				blockInput = true;
			}
		}
	}

	void GameEnd(GameStateArgs args) {
		Game.OnBattleEnd -= GameEnd;
		Game.OnPause -= PauseToggle;
		Game.OnUnpause -= PauseToggle;
		Game.OnSetupEnd -= PauseToggle;
	}

    IEnumerator PreGame() {
        while (BossPool.MainBoss == null) {
            yield return 0;
        }
		// God damn this is dumb.
		while (BossPool.MainBoss.data == null) {
			yield return 0;
		}
		for(int i = 0; i < BossPool.MainBoss.data.Abilities.Count; i++) {
			AbilityNames[i] = BossPool.MainBoss.data.Abilities[i].Trim();
		}
//		AbilityNames = BossPool.MainBoss.data.Abilities;
        pregame = false;
        for (int i = 0; i < AbilityNames.Count; i++) {
            abiTrackHandler.SetAbilityNumber(i, AbilityNames[i]);
            AbilityBank.Instance.Abilities[AbilityNames[i]].abilityCooldownOverlay = abiTrackHandler.CoolDownIcon(i);
            AbilityBank.Instance.Abilities[AbilityNames[i]].abilityCooldownOverlay.fillAmount = 1f;
            Ability a = AbilityBank.Instance.Abilities[AbilityNames[i]];
            abiTrackHandler.AddTooltip(i, a.AbilityData.Tooltip, a.AbilityData.PowerLevel);
            abiTrackHandler.AddTooltip(i, a.StrongAbility.AbilityData.Tooltip, a.StrongAbility.AbilityData.PowerLevel);
            abiTrackHandler.AddTooltip(i, a.WeakAbility.AbilityData.Tooltip, a.WeakAbility.AbilityData.PowerLevel);
        }
        currentAbilityTrack = 0;
        leftBump = false;
        rightBump = false;
        raiders = new List<RPGCharacter>();
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("raider")) {
            raiders.Add(o.GetComponent<RPGCharacter>());
        }
        targetIndex = 0;
        target = raiders[targetIndex];
        aoeTargetting = false;
        selectTargetting = false;
        if (DataHandler.basicBossAI) {
            print("AI Game starting.");
            StartCoroutine(SimpleBossAI());
        }
    }

	void Start () {
        StartCoroutine("PreGame");
	}

	IEnumerator SimpleBossAI() {
		while(true) {
			if(blockInput || Game.InSetup || pregame) {
				yield return 0;
				continue;
			}
			List<Ability> possibleAbilities = AbilityBank.Instance.AvailableBossAbilities();

			Ability nextAbility = null; 
			if(possibleAbilities.Count > 0) {
				nextAbility = possibleAbilities[UnityEngine.Random.Range(0,possibleAbilities.Count)];
			}
			if(!BossPool.MainBoss.Target.Alive) {
				foreach(Raider r in RaiderPool.AllRaiders) {
					if(r.Alive) {
						this.target = r;
						break;
					}
				}
			}

			if(nextAbility != null && !MainBoss.Frozen) {
				try {
					RPGCharacter randRaider = RaiderPool.AllRaiders[UnityEngine.Random.Range(0,RaiderPool.AllRaiders.Count)];
					if(nextAbility.IsAoeTargeted()) {
						nextAbility.Activate(randRaider.transform.position);
					} else {
						nextAbility.Activate(randRaider);
					}
				} catch(Exception e) {
					print (e.ToString());
				}
			}
			yield return new WaitForSeconds(5f);
		}
	}

	void PauseToggle(GameStateArgs args) {
		if(args.NextState == GameState.Battle) {
			if(IsBossPlayer) {
				blockInput = false;
			}
		} else {
			blockInput = true;
		}
	}

	int GetAbility() {
		if(Input.GetButtonDown("Abi1")) {
			return 1;
		} else if(Input.GetButtonDown("Abi2")) {
			return 2;
		} else if(Input.GetButtonDown("Abi3")) {
			return 3;
		} else if(Input.GetButtonDown("Abi4")) {
			return 4;
		}
		return 0;
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		return;
	}

	void DisplayAbilityTrack(int abilityMod) {
		// Displays boss ability shelf based on the modifier values... (holding left trigger is shelf two, right is three, left + right = 4)
		// Ability mod is 0, 4, 8, 12
		if(abilityMod != currentAbilityTrack) {
			// swapping ability tracks...
			abiTrackHandler.SelectTrack(abilityMod);
			currentAbilityTrack = abilityMod;
		} // else do nothing!
	}

	[RPC]
	public void HandleNextTarget(int next) {
		for(int i = 0; i < raiders.Count; i++) {
			targetIndex += next;
			if(targetIndex < 0) {
				targetIndex = raiders.Count - 1;
			} else if(targetIndex >= raiders.Count) {
				targetIndex = 0;
			}
			target = raiders[targetIndex];
			if(target.Alive)
				break;
		}
	}

	void NextTarget (int next) {
		if(PhotonNetwork.inRoom) {
			this.photonView.RPC("HandleNextTarget",PhotonTargets.All,next);
		} else {
			HandleNextTarget(next);
		}
	}

	// Update is called once per frame
	void Update () {
        if (target == null) {
            HPBarHandler.Instance.NoTarget();
		} else if(target.Alive) {
            HPBarHandler.Instance.Target(target);
		} else { // Find a new target...
			NextTarget(1);
		}
		if(blockInput || DataHandler.basicBossAI || pregame) {
			return;
		}
		int inputAbility = GetAbility();
		float leftAxis = Input.GetAxis("LeftBossModifier");
		float rightAxis = Input.GetAxis("RightBossModifier");
		AbilityStrength abiStr = AbilityStrength.Normal;
		BossAbilityTooltip.currentAbilityPower = 1;
		if(leftAxis > 0.5f || Input.GetKey(KeyCode.LeftControl)) {
			abiStr = AbilityStrength.Weak;
			BossAbilityTooltip.currentAbilityPower = 0;
		} else if(rightAxis > 0.5f || Input.GetKey (KeyCode.LeftShift)) {
			abiStr = AbilityStrength.Strong;
			BossAbilityTooltip.currentAbilityPower = 2;
		}

		for(int i = 0; i < AbilityNames.Count; i++) {
            if (!AbilityBank.Instance.Abilities.ContainsKey(AbilityNames[i]))
                print(AbilityNames[i]);
			Ability a = AbilityBank.Instance.Abilities[AbilityNames[i]];
			if(abiStr == AbilityStrength.Weak) {
				a = a.GetAbilityStrength(AbilityStrength.Weak);
			} else if(abiStr == AbilityStrength.Strong) {
				a = a.GetAbilityStrength(AbilityStrength.Strong);
			}
			abiTrackHandler.SetAbilityNumber(i,a.AbilityData.Name);
		}

		if(inputAbility > 0 && !GCD) { // Abutton was pressed
			Ability nextAbility = AbilityBank.Instance.Abilities[AbilityNames[inputAbility - 1]];
			nextAbility = nextAbility.GetAbilityStrength(abiStr);
			if(selectedAbility == nextAbility) { // There was a queued ability, and it's the same as the one the player selected now.  Fire it.
				if(aoeTargetting) {
					HandleAbilityActivation(selectedAbility.name,null,transform.position);
				} else if(selectTargetting) {
					HandleAbilityActivation(selectedAbility.name,target,Vector3.zero);
				} else if(MainBoss.Target != null) {
					HandleAbilityActivation(selectedAbility.name,MainBoss.Target,Vector3.zero);
				} else {
					HandleAbilityActivation(selectedAbility.name,null,Vector3.zero);
				}
				selectedAbility.SelectOverlayOff();
				selectedAbility = null;
				aoeTargetting = false;
				selectTargetting = false;
			} else if(nextAbility.AbilityAvailable == true) {
				if(selectedAbility != null) {
					selectedAbility.SelectOverlayOff();
				}
				selectedAbility = nextAbility;
				selectedAbility.SelectOverlayOn();
				aoeTargetting = selectedAbility.IsAoeTargeted();
				selectTargetting = selectedAbility.IsSelectTargeted();
			} else if(selectedAbility != null) {
				selectedAbility.SelectOverlayOff();
				selectedAbility = null;
				aoeTargetting = false;
				selectTargetting = false;
			}
		}

		if(aoeTargetting) {
			aoeProjector.enabled = true;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			int groundMask = (1 << 8);
			Physics.Raycast(ray, out hit, float.PositiveInfinity, groundMask);
			float y = transform.position.y;
			transform.position = hit.point;
			transform.position = new Vector3(transform.position.x,y,transform.position.z);
		} else {
			aoeProjector.enabled = false;
		}
		ProjectorUpdate();

		if(Input.GetButtonDown("QuickSwitch")) {
			NextTarget(1);
		} else {
			bool leftBumper = (Input.GetAxisRaw("BossTargetSelectLeft") == 1f) || (Input.GetKey(KeyCode.A));
			bool rightBumper = (Input.GetAxisRaw("BossTargetSelectRight") == 1f) || (Input.GetKey (KeyCode.D));
			if(leftBumper && !leftBump) {
				NextTarget(-1);
				leftBump = true;
			} else if(rightBumper && !rightBump) {
				NextTarget(1);
				rightBump = true;
			}

			if(!rightBumper && rightBump) {
				rightBump = false;
			}
			if(!leftBumper && leftBump) {
				leftBump = false;
			}
		}
	}

	public IEnumerator GlobalCooldown(){
		GCD = true;
		yield return new WaitForSeconds(Rules.Instance.RuleDict["GCD"]);
		GCD = false;
	}

    public void TriggerGCD(float time) {
        StartCoroutine(GlobalCooldownOverride(time));
    }

    public IEnumerator GlobalCooldownOverride(float time) {
        GCD = true;
        yield return new WaitForSeconds(time);
        GCD = false;
    }

	void HandleAbilityActivation(string abilityName, RPGCharacter tar, Vector3 posTar) {
		bool useTarget = tar != null;
		if(PhotonNetwork.inRoom) {
			if(useTarget) {
				photonView.RPC("RemoteAbility",PhotonTargets.All,abilityName,tar.CharID,posTar.x,posTar.y,posTar.z,useTarget);
			} else {
				photonView.RPC("RemoteAbility",PhotonTargets.All,abilityName,-1,posTar.x,posTar.y,posTar.z,useTarget);

			}
		} else {
			if(useTarget) {
				RemoteAbility(abilityName,tar.CharID,posTar.x,posTar.y,posTar.z,useTarget);
			} else {
				RemoteAbility(abilityName,-1,posTar.x,posTar.y,posTar.z,useTarget);
			}
		}
	}

	[RPC]
	public void RemoteAbility(string name, int targetId, float x, float y, float z, bool useTarget) {
        if (MainBoss.Frozen)
            return;
        Ability abi = AbilityBank.Instance.Abilities[name];
		Vector3 tarPos;
		RPGCharacter tar;
		if(useTarget) {
			tar = RPGCharacter.IDLookup[targetId];
			abi.Activate(tar);
		} else {
			tarPos = new Vector3(x,y,z);
			abi.Activate(tarPos);
		}
	}

	[RPC]
	public void NetworkedAOEControl(bool turnOn, float x, float y, float z) {
//		if(!IsBossPlayer) {
			aoeProjector.enabled = turnOn;
			transform.position = new Vector3(x,y,z);
//		}
	}

	public void ProjectorUpdate() {
		if(PhotonNetwork.inRoom) {
			this.photonView.RPC("NetworkedAOEControl",PhotonTargets.All, aoeProjector.enabled, transform.position.x, transform.position.y, transform.position.z);
		}
	}
}
