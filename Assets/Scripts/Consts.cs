﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

public enum RaiderType {
	[XmlEnumAttribute("Tank")]
	Tank,
	[XmlEnumAttribute("Support")]
	Support,
	[XmlEnumAttribute("Melee")]
	Melee,
	[XmlEnumAttribute("Ranged")]
	Ranged,
	[XmlEnumAttribute("Wizard")]
	Wizard,
    [XmlEnumAttribute("Snowman")]
    Snowman,
    [XmlEnumAttribute("Monkey")]
    Monkey
}

public class Consts {
	public static string[] AttackTags = new string[] {
		"boss"
	};
}
