﻿using UnityEngine;
using System.Collections;

public class CharacterSelectFrame : MonoBehaviour {
    public UISprite glow;
    public string charStr;
    public string tooltip;
    public bool selected = false;
    public static Color SEL_COL = new Color(0,1f,12f/255f);
    public static Color HOVER_COL = new Color(0, 149f / 255f, 1f);
    public static Color ERR_COL = new Color(1f,0,0);
	// Use this for initialization
	void Start () {
        glow.alpha = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnHover(bool isOver) {
        if (isOver) {
            glow.alpha = 255f;
            if (!selected && UIPick.Instance.raidersSelected >= 5) {
                glow.color = ERR_COL;
            } else if (!selected) {
                glow.color = HOVER_COL;
            }
        } else if(!selected) {
            glow.alpha = 0f;
        }
    }

    void OnClick() {
        print(UIPick.Instance.raidersSelected);
        if (!CharSelectHandler.Instance.showRaid) { // this is bad bad bad bad bad bad bad bad bad ....
            selected = !selected;
            if (selected) {
                glow.color = SEL_COL;
                if (PhotonNetwork.inRoom) { 
                    RoomHandler.Instance.photonView.RPC("Pick", PhotonTargets.All, charStr, selected);
                } else {
                    UIPick.Instance.Set(charStr, selected);
                }
            } else {
                glow.alpha = 0;
                if (PhotonNetwork.inRoom) {
                    RoomHandler.Instance.photonView.RPC("Pick", PhotonTargets.All, charStr, selected);
                } else {
                    UIPick.Instance.Set(charStr, selected);
                }
            }
            CharSelectHandler.Instance.BossChoice(charStr);                                              
            return;
        }
        if (selected == false && UIPick.Instance.raidersSelected >= 5) {
            return;
        }
        
        selected = !selected;
        
        if (selected) {
            glow.color = SEL_COL;
            if (PhotonNetwork.inRoom) {
                RoomHandler.Instance.photonView.RPC("Pick", PhotonTargets.All, charStr, selected);
            } else {
                UIPick.Instance.Set(charStr, selected);
            }
        } else {
            glow.alpha = 0;
            if (PhotonNetwork.inRoom) {
                RoomHandler.Instance.photonView.RPC("Pick", PhotonTargets.All, charStr, selected);
            } else {
                UIPick.Instance.Set(charStr, selected);
            }
        }
    }
    void OnTooltip(bool show) {
        if(show) {
            UITooltip.ShowText(tooltip);
            print(tooltip);
        } else {
            UITooltip.ShowText(null);
            print("Bye tip");
        }
    }
}
