﻿using UnityEngine;
using System.Collections;

public class CharSelectHandler : MonoBehaviour {
    public static CharSelectHandler Instance;
    public UILabel raidCount;
    public UIButton raidConfirm;
    public UITweener bossIn, raidOut;
    public BossSelectFrame jack, bear;
    public bool showRaid = true;
    public bool ready = false;
    public UIPlayTween frameScale;
    void Awake() {
        Instance = this;
    }

	// Update is called once per frame
	void Update () {
        raidCount.text = string.Format("{0:D} / 5 Raiders Selected", UIPick.Instance.raidersSelected);
        raidConfirm.isEnabled = (UIPick.Instance.raidersSelected >= 5 && showRaid) || (!showRaid && UIPick.Instance.bossSelected);
	}

    void Load() {
        SceneManager.Instance.LoadQueued();
    }

    public void ShowBoss() {
        showRaid = false;
        raidOut.PlayForward();
        bossIn.PlayForward();
    }

    public void ConfirmRaider() {
        print("Confirming...");
        if (showRaid) { 
            raidOut.PlayForward();
            if (PhotonNetwork.inRoom) {
                ready = true;
                RoomHandler.Instance.photonView.RPC("PlayerReady", PhotonTargets.All, null);
                return;
            }
            bossIn.PlayForward();
            showRaid = false;
        } else {
            if (PhotonNetwork.inRoom) {
                RoomHandler.Instance.photonView.RPC("PlayerReady", PhotonTargets.All, null);
                return;
            }
            frameScale.Play(true);
            Invoke("Load", 1f);
        }
    }

    public void BossChoice(string choice) {
        BossSelectFrame fr = bear;
        if (choice == "Bear") {
            fr = jack;
        }
        if (fr.selected) {
            fr.Toggle();
        }
    }
}
