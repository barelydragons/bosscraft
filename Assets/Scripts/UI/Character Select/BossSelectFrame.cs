﻿using UnityEngine;
using System.Collections;

public class BossSelectFrame : MonoBehaviour {
    public UISprite glow;
    public string charStr;
    public string tooltip;
    public bool selected = false;
	// Use this for initialization
	void Start () {
        glow.alpha = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Toggle() {
        selected = !selected;
        if (selected) {
            glow.alpha = 255f;
        } else {
            glow.alpha = 0f;
        }
        UIPick.Instance.Set(charStr, selected);
    }

    void OnClick() {
        Toggle();
        CharSelectHandler.Instance.BossChoice(charStr);
    }

    void OnHover(bool isOver) {
        if (isOver && !selected) {
            glow.alpha = .5f;
        } else if (!selected) {
            glow.alpha = 0f;
        }
    }

    void OnTooltip(bool show) {
        if (show) {
            UITooltip.ShowText(tooltip);
            print(tooltip);
        } else {
            UITooltip.ShowText(null);
            print("Bye tip");
        }
    }
}
