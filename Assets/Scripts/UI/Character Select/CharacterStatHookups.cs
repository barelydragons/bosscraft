﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CharacterStatHookups : MonoBehaviour {
	public UILabel charName;
	public UILabel swapLabel;
	public UILabel description;
	public StatHookup HP;
	public StatHookup Aspd;
	public StatHookup MoveSpeed;
	public StatHookup Range;
	public StatHookup Power;

	// Boss Only
	public StatHookup PP;
	public GameObject PPGO;

	private void SetWithData() {
		RPGCharacterData someData = raidSelected ? raiderData[raidSelectIndex] : bossData[bossSelectIndex];
		charName.text = someData.Name;
		description.text = someData.Description;
		HP.value.text = someData.MaxHP.ToString();
		Aspd.value.text = someData.AttackSpeed.ToString();
		MoveSpeed.value.text = someData.MoveSpeed.ToString();
		Power.value.text = someData.Power.ToString();
		Range.value.text = someData.Range.ToString();
		if(!raidSelected) {
			BossData d = (BossData)someData;
			PP.value.text = d.PowerPoints.ToString();
		}
	}

	private List<RPGCharacterData> raiderData;
	private List<RPGCharacterData> bossData;
	private int raidSelectIndex;
	private int bossSelectIndex;
	private bool raidSelected = true;

	// Use this for initialization
	void Start () {
		raiderData = new List<RPGCharacterData>();
		bossData = new List<RPGCharacterData>();
		foreach(RPGCharacterData r in DataHandler.Raiders.Values) {
			raiderData.Add(r);
		}
		foreach(RPGCharacterData r in DataHandler.Bosses.Values) {
			bossData.Add(r);
		}
		raidSelectIndex = 0;
		bossSelectIndex = 0;
		SetWithData();
		PPGO.transform.position -= new Vector3(0,60,0);
		PPGO.SetActive(false);
	}

	public void Swap() {
		raidSelected = !raidSelected;
		SetWithData();
		if(raidSelected) {
			PPGO.transform.position -= new Vector3(0,60,0);
			PPGO.SetActive(false);
			swapLabel.text = "Swap to Bosses";
		} else {
			PPGO.transform.position += new Vector3(0,60,0);
			PPGO.SetActive(true);
			swapLabel.text = "Swap to Raiders";
		}
	}

	public void Next() {
		if(raidSelected) {
			raidSelectIndex++;
			if(raidSelectIndex >= raiderData.Count)
				raidSelectIndex = 0;
		} else {
			bossSelectIndex++;
			if(bossSelectIndex >= bossData.Count)
				bossSelectIndex = 0;
		}
		SetWithData();
	}
	
	public void Previous() {
		if(raidSelected) {
			raidSelectIndex--;
			if(raidSelectIndex < 0)
				raidSelectIndex = raiderData.Count - 1;
		} else {
			bossSelectIndex++;
			if(bossSelectIndex < 0)
				bossSelectIndex = bossData.Count - 1;
		}
		SetWithData();
	}

	public void SaveAndContinue() {
		ReverseSet();
		SoundManager.Instance.crossDuration = 2f;
		Application.LoadLevel("battle");
	}
	
	public void BossAI() {
		DataHandler.basicBossAI = true;
		SaveAndContinue();
	}

	public void Tutorial() {
		DataHandler.PlayTutorial = true;
		SaveAndContinue();
	}
	
	public void ReverseSet() {
		RPGCharacterData d = DataHandler.Raiders[raiderData[raidSelectIndex].Name];
		if(!raidSelected)
			d = DataHandler.Bosses[bossData[bossSelectIndex].Name];
		d.Name = charName.text;
		d.Description = description.text;
		d.MaxHP = int.Parse(HP.value.text);
		d.AttackSpeed = float.Parse(Aspd.value.text);
		d.MoveSpeed = int.Parse(MoveSpeed.value.text);
		d.Power = int.Parse(Power.value.text);
		d.Range = int.Parse(Range.value.text);
		if(!raidSelected) {
			((BossData)d).PowerPoints = int.Parse(PP.value.text);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
