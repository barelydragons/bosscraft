﻿using UnityEngine;
using System.Collections;

public class FloatingCombatText : MonoBehaviour {
	public static FloatingCombatText Instance;
	public GameObject textPrefab;
	public Camera nguiCam;

	public class FloatingTextData {
		public UILabel l;
		public RPGCharacter c;
		public int amount;
		public Color col; 
		public Vector3 screenPos;
		public FloatingTextData(UILabel l_, RPGCharacter c_,int amount_,Color col_, Vector3 screenPos_) {
			c = c_;
			l = l_;
			amount = amount_;
			col = col_;
			screenPos = screenPos_;
		}

	}
	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	public void NewHeal(RPGCharacter c, int amount) {
		FloatingText(c,amount,Color.green);
	}

	public void NewDamage(RPGCharacter c, int amount) {
		FloatingText(c,amount,Color.red);
	}

	void FloatingText(RPGCharacter c, int amount, Color col) {
		if(Camera.main == null) 
			return;
		Vector3 screenPos = Camera.main.WorldToScreenPoint(c.transform.position);
		screenPos.x = Mathf.Clamp01(screenPos.x / Screen.width);
		screenPos.y = Mathf.Clamp01(screenPos.y / Screen.height);
		screenPos.z = 0;
		// 0,0 is center of ngui screen, top left of regular screen
		//			screenPos.x -= Screen.width / 2.0f;
		//			screenPos.y -= Screen.height / 2.0f;
		UILabel lbl = ((GameObject)Instantiate(textPrefab)).GetComponent<UILabel>();
		lbl.color = col;
		lbl.text = amount.ToString();
//		lbl.transform.positionTo(2f,new Vector3(0,.15f),true);
		StartCoroutine("WaitAndTween",new FloatingTextData(lbl,c,amount,col,screenPos));
		Destroy (lbl.gameObject,2.3f);
	}

	IEnumerator WaitAndTween(FloatingTextData d) {
		UILabel lbl = d.l;
		Vector3 screenPos = d.screenPos;
		lbl.transform.position = nguiCam.ViewportToWorldPoint(screenPos + new Vector3(0,.048f));
		yield return 0;
		float duration = 2f;
		float offset = .04f + .04f * ((float)d.amount / 250f);
		lbl.transform.position = nguiCam.ViewportToWorldPoint(screenPos + new Vector3(0,.048f));
		lbl.transform.positionTo(duration,new Vector3(Random.Range(-.025f,.025f),offset),true);
		lbl.transform.scaleTo(duration,lbl.transform.localScale * .5f);
		Go.to (lbl,duration,new GoTweenConfig().colorProp("color",new Color(lbl.color.r,lbl.color.g,lbl.color.b,0f)).setEaseType(GoEaseType.QuadIn));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
