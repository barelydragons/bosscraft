﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HPBarHandler : MonoBehaviour {
	private static HPBarHandler instance;
	public static HPBarHandler Instance {
		get { return instance; }
	}
	public GameObject hpBarPrefab;
	public Camera nguiCam;
	private Dictionary<RPGCharacter, RaiderHPBar> hpBars;
	// Use this for initialization
	void Awake () {
		instance = this;
		hpBars = new Dictionary<RPGCharacter, RaiderHPBar>();
		Game.OnBattleEnd += DestroyBars;
	}

	void DestroyBars(GameStateArgs args) {
		Game.OnBattleEnd -= DestroyBars;
		foreach(RPGCharacter c in hpBars.Keys) {
			Destroy(hpBars[c].gameObject);
		}
		hpBars = null;
	}

    public void Target(RPGCharacter tar) {
        NoTarget();
        if (hpBars == null) {
            return;
        }
        hpBars[tar].Arrow(true);
    }

    public void NoTarget() {
        if (hpBars == null) {
            return;
        }
        foreach (RaiderHPBar bar in hpBars.Values) {
            bar.Arrow(false);
        }
    }

	public void BeginCooldown(Raider r,float t) {
		hpBars[r].BeginCooldown(t);
	}

	public void CreateHPBar(RPGCharacter character) {
		// Creates and links an HP Bar to a given rpg character
		GameObject barSprite = (GameObject)GameObject.Instantiate(hpBarPrefab);
		RaiderHPBar hpBar = barSprite.GetComponent<RaiderHPBar>();
		hpBar.TrackRaider(character);
		hpBars.Add(character,hpBar);
	}

	public void UnregisterHPBar(RPGCharacter character) {
		if(hpBars == null || !hpBars.ContainsKey(character)) {
			return;
		}
		GameObject o = hpBars[character].gameObject;
		hpBars.Remove(character);
		Destroy(o);
	}
	
	// Update is called once per frame
	void Update () {
		if(hpBars == null) {
			return;
		}
		foreach(RPGCharacter c in hpBars.Keys) {
            if (c == null) {
                hpBars[c].gameObject.SetActive(false);
                continue;
            }
			if(Camera.main == null) {
				continue;
			}
			Vector3 screenPos = Camera.main.WorldToScreenPoint(c.transform.position);
			screenPos.x = Mathf.Clamp01(screenPos.x / Screen.width);
			screenPos.y = Mathf.Clamp01(screenPos.y / Screen.height);
			screenPos.z = 0;

			hpBars[c].hpBar.transform.position = nguiCam.ViewportToWorldPoint(screenPos + new Vector3(0,c.hpBarOffset));
			hpBars[c].hpBar.fillAmount = (float)c.CurrentHP / (float)c.MaxHP;
		}
	}
}
