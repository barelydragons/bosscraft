﻿using UnityEngine;
using System.Collections;

public class LoadScreen : MonoBehaviour {
	public Texture2D[] loadScreens;
	private Texture2D screen;
	public static LoadScreen Instance;
	private bool go;
	// Use this for initialization
	void Start () {
		Instance = this;
		NewScreen();
		go = true;
	}

	public void NewScreen() {
		screen = loadScreens[Random.Range(0,loadScreens.Length)];
	}

	public void Stop() {
		go = false;
	}

	public void Go() {
		go = true;
	}

	public void NewScreenAndGo() {
		NewScreen();
		Go();
	}
	
	// Update is called once per frame
	void OnGUI () {
		if(go) {
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),screen);
		}
	}
}
