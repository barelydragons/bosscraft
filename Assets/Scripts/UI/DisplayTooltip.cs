﻿using UnityEngine;
using System.Collections;

public class DisplayTooltip : MonoBehaviour {
	public string tooltip;
	void OnTooltip(bool show) {
		if(show) {
			UITooltip.ShowText(tooltip);
		} else {
			UITooltip.ShowText(null);
		}
	}
}
