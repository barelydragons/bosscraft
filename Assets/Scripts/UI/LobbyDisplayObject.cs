﻿using UnityEngine;
using System.Collections;

public class LobbyDisplayObject : MonoBehaviour {
	public UILabel bossPlayerName, raidPlayerName;
	public UILabel displayNameLabel;
	public static string LocalPlayerName;
	public static LobbyDisplayObject Instance;

	// Use this for initialization
	void Start () {
		Instance = this;
		LocalPlayerName = System.Environment.UserName; // BREAKS WEBPLAYER, PROBABLY
		displayNameLabel.text = "Display Name: " + LocalPlayerName;
	}

	public void SetBossName(string name) {
		this.bossPlayerName.text = name;
	}

	public void SetRaidName(string name) {
		this.raidPlayerName.text = name;
	}

	// Update is called once per frame
	void Update () {
		
	}
}