﻿using UnityEngine;
using System.Collections;

public class RaiderHPBar : MonoBehaviour {
	public UISprite cooldown, hpBar, targetArrow;
	private RPGCharacter raider;
	private bool coolingDown = false;

    void Awake() {
        Arrow(false);
    }

    public void Arrow(bool on) {
        targetArrow.gameObject.SetActive(on);
    }

	public void TrackRaider(RPGCharacter r) {
		this.raider = r;
	}

	public void BeginCooldown(float time) {
		if(coolingDown == false) {
			StartCoroutine("Cooldown",time);
		}
	}

	IEnumerator Cooldown(float time) {
		Raider cast = (Raider)raider;
		time = AbilityBank.Instance.Abilities[cast.AbilityName].CooldownLength;
		coolingDown = true;
		while(AbilityBank.Instance.Abilities[cast.AbilityName].AbilityAvailable) {
			cooldown.fillAmount = AbilityBank.Instance.Abilities[cast.AbilityName].CooldownPercent;
			yield return 0;
		}
		cooldown.fillAmount = 1;
		coolingDown = false;
	}
}
