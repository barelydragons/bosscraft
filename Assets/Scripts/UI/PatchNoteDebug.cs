﻿using UnityEngine;
using System.Collections;

public class PatchNoteDebug : MonoBehaviour {
	public TextAsset patchFile;
	// Use this for initialization
	void Start () {
		this.GetComponent<UILabel>().text = patchFile.text;
	}
}
