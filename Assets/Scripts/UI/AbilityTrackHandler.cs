﻿using UnityEngine;
using System.Collections;

// Master ability icon handler
public class AbilityTrackHandler : MonoBehaviour {
	public AbilityTrack track1; // there's only 1 track
	private GameObject activeObject;
	// Use this for initialization
	void Start () {
		activeObject = track1.gameObject;
	}

	public UISprite CoolDownIcon(int num) {
		return track1.abilityTracks[num % 4];
	}

	public void AddTooltip(int num, string tooltip, int power) {
		track1.tooltips[num % 4].tips[power] = tooltip;
	}

	public void SetAbilityNumber(int num,string name) {
		track1.SetAbility(num % 4,name);
	}

	public void SelectTrack(int num) {
		// 0 = 0, 4 = 1, 8 = 2, 12 = 3, for multiple ability tracks
		int val = num / 4 + 1;
		// C# trickery, access a variable via a string :O
		GameObject selectedTrack = ((AbilityTrack)this.GetType().GetField(string.Format("track{0:D}",val)).GetValue(this)).gameObject;
		activeObject.animation.Play("AbilityTrackExit");
		selectedTrack.animation.Play("AbilityTrackEntrance");
		activeObject = selectedTrack;
	}
}
