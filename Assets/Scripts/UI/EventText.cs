﻿using UnityEngine;
using System.Collections;

public class EventText : MonoBehaviour {
	public UILabel lbl;
	public UILabel timerLbl;
	private string prevText;
	private float setupTime;
	// Use this for initialization
	void Awake () {
		Game.OnPause += OnPause;
		Game.OnUnpause += OnUnpause;
		Game.OnBattleBegin += SetText;
		Game.OnBattleEnd += SetText;
		Game.OnSetupBegin += SetText;
		Game.OnBattleEnd += GameEnd;
		setupTime = Rules.Instance.RuleDict["PrepPhaseTime"];
	}

	void GameEnd(GameStateArgs args) {
		Game.OnPause -= OnPause;
		Game.OnUnpause -= OnUnpause;
		Game.OnBattleBegin -= SetText;
		Game.OnBattleEnd -= SetText;
		Game.OnBattleEnd -= GameEnd;
		Game.OnSetupBegin -= SetText;
	}

	IEnumerator setupTimer() {
		while(setupTime > 0f) {
			if(Time.timeScale != 0f) {
				timerLbl.transform.scaleFrom(1f,timerLbl.transform.localScale * 1.8f);
				setupTime -= 1f;
				timerLbl.text = setupTime.ToString("0");
			}
			yield return new WaitForSeconds(1f);
		}
		timerLbl.text = "";
	}

	void OnPause(GameStateArgs args) {
		prevText = lbl.text;
		lbl.text = "Paused";
	}
	
	void OnUnpause(GameStateArgs args) {
		lbl.text = prevText;
	}

	// Update is called once per frame
	void SetText (GameStateArgs args) {
		if(args.NextState == GameState.Battle) {
			lbl.text = "";
		} else if(args.NextState == GameState.Setup) {
			lbl.text = "Prepare To Fight";
			StartCoroutine(setupTimer());
		} else if(args.NextState == GameState.Postgame) {
			if(!args.BossAlive) {
				lbl.text = "Raiders Win";
			} else if(!args.RaidAlive) {
				lbl.text = "Boss Wins";
			}
		}
	}
}
