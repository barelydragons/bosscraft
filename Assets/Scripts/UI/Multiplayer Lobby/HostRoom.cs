﻿using UnityEngine;
using System.Collections;

public enum HostingState {
	NotHosting,
	SettingUp,
	WaitingForSecondPlayer,
	Ready
}

public class HostRoom : MonoBehaviour {
	public TweenAlpha roomViewTween, scrollTween;
	public HostingState hostState = HostingState.NotHosting;
	public UIInput input;
	public UIButton but;
	public bool RoomSettingUp = false;
	public bool RoomSetup = false;
	public UILabel roomLabel;
	public bool roomCreated = false;
	// Use this for initialization
	void Start () {
		but.isEnabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(input.value != "") {
			but.isEnabled = true;
		} else {
			if(!PhotonNetwork.inRoom){
				but.isEnabled = false;
			}
		}
	}

	void OnJoinedRoom() {
		hostState = HostingState.WaitingForSecondPlayer;
		print (PhotonNetwork.room.name);
		PhotonNetwork.player.name = LobbyDisplayObject.LocalPlayerName;
		if(PhotonNetwork.room.playerCount == 2) {
			roomLabel.text = "In Room: " + PhotonNetwork.room.name;
			RoomHandler.Instance.tweenPlayer.Play(true);
			roomViewTween.enabled = true;
			roomViewTween.PlayForward();
			scrollTween.enabled = true;
			scrollTween.PlayForward();
		} else {
			roomLabel.text = "Hosting Room: " + PhotonNetwork.room.name;
		}
		but.gameObject.GetComponentInChildren<UILabel>().text = "Start";
	}

	void OnClick() {
		if(!roomCreated){
			hostState = HostingState.SettingUp;
			PhotonNetwork.CreateRoom(input.value,new RoomOptions() { isVisible = true, maxPlayers = 2 },TypedLobby.Default);
			roomLabel.text = "Setting up " + input.value;
			roomCreated = true;
		}
	}
}
