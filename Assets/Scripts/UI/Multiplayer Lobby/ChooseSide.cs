﻿using UnityEngine;
using System.Collections;

public class ChooseSide : MonoBehaviour {
	public bool BossOccupied, RaidOccupied;
	int BossID, RaidID;
	// Use this for initialization
	void Start () {
		BossID = 1;
		RaidID = 2;
	}

	public void ChooseBoss() {
		if(!BossOccupied){
			if(2 == PhotonNetwork.player.ID){
				BossID = 2;
				if(2 == RaidID){
					RaidOccupied = false;
					RaidID = 1;
				}
			}
			else{
				BossID = 1;
				if(1 == RaidID){
					RaidOccupied = false;
					RaidID = 2;
				}
			}
			BossOccupied = true;
		}
		else{
			Debug.Log ("Other Player is already the boss");
		}
	}
	
	public void ChooseRaid() {
		if(!RaidOccupied){
			if(2 == PhotonNetwork.player.ID){
				if(2 == BossID){
					BossOccupied = false;
					BossID = 1;
				}
				RaidID = 2;
			}
			else{
				RaidID = 1;
				if(1 == BossID){
					BossOccupied = false;
					BossID = 2;
				}
			}
			RaidOccupied = true;	
		}
		else{
			Debug.Log ("Other Player is already the Raid");
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
