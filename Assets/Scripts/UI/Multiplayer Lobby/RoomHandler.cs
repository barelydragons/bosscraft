﻿using UnityEngine;
using System.Collections;

public class RoomHandler : MonoBehaviour {
    public GameObject raiderSelectPrefab;
    public UIPanel rootUI;
	public static RoomHandler Instance;
	public PhotonPlayer bossPlayer, raidPlayer;
	public PhotonView photonView;
	public UIPlayTween tweenPlayer;
    int ready = 0;
	int playerCount = 0;
	
	// Use this for initialization
	void Awake () {
		Instance = this;
		photonView = this.GetComponent<PhotonView>();
		PhotonNetwork.automaticallySyncScene = true;
		PhotonNetwork.ConnectUsingSettings("1.0");
		DontDestroyOnLoad(this.gameObject);
	}
	
	void OnPhotonPlayerConnected() {
		playerCount = PhotonNetwork.room.playerCount;
		if(playerCount == 2 && tweenPlayer != null) {
			tweenPlayer.Play(true);
		}
	}

	PhotonPlayer PlayerWithId(int playerid) {
		foreach(PhotonPlayer p in PhotonNetwork.playerList) {
			if(p.ID == playerid) {
				return p;
			}
		}
		return null;
	}

	public bool IsBossPlayer() {
		return PhotonNetwork.player == bossPlayer;
	}

	public bool IsRaidPlayer() {
		return PhotonNetwork.player == raidPlayer;
	}

	public void ChooseSide(bool bossSide) {
		if(bossSide) {
			this.photonView.RPC("SetBoss",PhotonTargets.All,PhotonNetwork.player.ID,LobbyDisplayObject.LocalPlayerName);
		} else {
			this.photonView.RPC("SetRaider",PhotonTargets.All,PhotonNetwork.player.ID,LobbyDisplayObject.LocalPlayerName);
		}
	}

    [RPC]
    public void Pick(string name, bool stat) {
        UIPick.Instance.Set(name, stat);
    }

	[RPC]
	public void SetBoss(int playerid, string name) {
		if(bossPlayer == null) {
			bossPlayer = PlayerWithId(playerid);
			LobbyDisplayObject.Instance.SetBossName(name);
		}
	}

    [RPC]
    public void PlayerReady() {
        ready++;
        if (ready == 2) {
            PhotonNetwork.LoadLevel("battle");
        }
    }

	[RPC]
	public void SetRaider(int playerid, string name) {
		if(raidPlayer == null) {
			raidPlayer = PlayerWithId(playerid);
			LobbyDisplayObject.Instance.SetRaidName(name);
		}
	}

	public void ChooseBoss() {
		ChooseSide(true);
	}
	
	public void ChooseRaid() {
		ChooseSide(false);
	}

	public void OnPressStart() {
		if(2 == playerCount && RolesFilled()){
            this.photonView.RPC("StartPhotonGame", PhotonTargets.All, null);// StartPhotonGame("battle");
		}
		else{
			Debug.Log("Can't start without a second player");
		}
	}

	void OnPhotonPlayerDisconnected() {
		if(tweenPlayer != null) {
			tweenPlayer.Play(false);
		}
		if(bossPlayer.ID == PhotonNetwork.player.ID) {
			raidPlayer = null;
		} else {
			bossPlayer = null;
		}
		playerCount = PhotonNetwork.room.playerCount;
		if(Application.loadedLevelName == "battle") {
			print ("Player disconnected.");
			Game.Instance.TriggerEndPrematurely();
			Application.LoadLevel("mainMenu");
			PhotonNetwork.Disconnect();
			Destroy(this.gameObject);
		}
	}

	bool RolesFilled(){
		if(bossPlayer != null && raidPlayer != null){
			return true;
		}
		else{
			return false;
		}
	}

    [RPC]
    public void StartPhotonGame() {
        NGUITools.AddChild(rootUI.gameObject, raiderSelectPrefab);
        if (PhotonNetwork.player == bossPlayer) {
            CharSelectHandler.Instance.ShowBoss();
        }
	}
}
