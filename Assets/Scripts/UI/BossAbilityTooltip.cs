﻿using UnityEngine;
using System.Collections;

public class BossAbilityTooltip : MonoBehaviour {
	public string[] tips; // Will store tooltips for weak/med/strong
	public static int currentAbilityPower = 1; // 0 = weak, 1 = normal, 2 = strong.

	void Awake() {
		tips = new string[3];
	}

	void OnHover(bool isOver) {
		if(isOver) {
			UITooltip.ShowText(tips[currentAbilityPower]);
		} else {
			UITooltip.ShowText(null);
		}
	}
}
