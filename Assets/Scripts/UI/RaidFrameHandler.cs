﻿using UnityEngine;
using System.Collections;

public class RaidFrameHandler : MonoBehaviour {
	public static RaidFrameHandler Instance;
	public RaiderHPBar tankFrame,healerFrame,mageFrame,melee1Frame,melee2Frame;
	private bool melee1 = false;
	// Use this for initialization
	void Awake () {
		Instance = this;
//		foreach(Raider r in RaiderPool.AllRaiders) {
//			RaiderFrame frameToTrack = melee2Frame;
//			if(r.RaiderType == RaiderType.Tank) {
//				frameToTrack = tankFrame;
//			} else if(r.RaiderType == RaiderType.Ranged) {
//				frameToTrack = mageFrame;
//			} else if(r.RaiderType == RaiderType.Support) {
//				frameToTrack = healerFrame;
//			} else if(melee1 == false) {
//				melee1 = true;
//				frameToTrack = melee1Frame;
//			}
//			frameToTrack.TrackRaider(r);
//		}
	}

	public void SyncRaider(RPGCharacter raider, RaiderHPBar hpBar) {
		foreach(Raider r in RaiderPool.AllRaiders) {
			if(r == raider) {
				if(r.RaiderType == RaiderType.Tank) {
					tankFrame = hpBar;
				} else if(r.RaiderType == RaiderType.Ranged) {
					mageFrame = hpBar;
				} else if(r.RaiderType == RaiderType.Support) {
					healerFrame = hpBar;
				} else if(melee1 == false) {
					melee1 = true;
					melee1Frame = hpBar;
				} else {
					melee2Frame = hpBar;
				}
				hpBar.TrackRaider(r);
			}
			return;
		}
	}

	public void BeginCooldown(Raider r,float t) {
		bool melee1 = false;
		RaiderHPBar frameToTrack = melee2Frame;
		if(r.RaiderType == RaiderType.Tank) {
			frameToTrack = tankFrame;
		} else if(r.RaiderType == RaiderType.Ranged) {
			frameToTrack = mageFrame;
		} else if(r.RaiderType == RaiderType.Support) {
			frameToTrack = healerFrame;
		} else if(melee1 == false) {
			melee1 = true;
			melee1Frame.BeginCooldown(t); // They share a cooldown...
		}
		frameToTrack.BeginCooldown(t);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
