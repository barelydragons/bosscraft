﻿using UnityEngine;
using System.Collections;

public class ToggleMute : MonoBehaviour {
	public UILabel text;
	private int muted = 0;
	// Use this for initialization
	void Awake () {
		if(PlayerPrefs.HasKey("muteAudio")) {
			muted = PlayerPrefs.GetInt("muteAudio");
		}
		SetMute();
	}

	void SetMute() {
        if (muted == 1) {
            print(muted);
			AudioListener.volume = 0;
			text.text = "Unmute";
			SoundManager.Instance.muted = true;
		} else {
			AudioListener.volume = 1;
			text.text = "Mute";
			SoundManager.Instance.muted = false;
		}
	}

	void MuteToggle() {
		muted = Mathf.Abs(muted - 1);
		PlayerPrefs.SetInt("muteAudio",muted);
		SetMute();
	}
	
	public void OnClick() {
		MuteToggle();
	}
}
