﻿using UnityEditor;

public class ModelScaleFactor : AssetPostprocessor{

	void OnPostProcessModel(){
		(assetImporter as ModelImporter).globalScale = 1.0f;
	}
}
