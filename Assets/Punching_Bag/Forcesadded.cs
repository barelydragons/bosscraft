﻿using UnityEngine;
using System.Collections;

public class Forcesadded : MonoBehaviour {
	
	float count = 0;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		count = count + Time.deltaTime; 

		if (3 < count && count < 3.5)
		{
			rigidbody.AddForce (new Vector3 (5 , 0 , 10 ));
		}

		if (6 < count && count < 6.5 )
		{
			rigidbody.AddForce (new Vector3 (-10 , 0 , -5 ));

		}		                   

		if (9 < count && count < 9.5 )
		{
			rigidbody.AddForce (new Vector3 (0 , -50 , 0 ));
			count = 0;
		}		

	}
}
